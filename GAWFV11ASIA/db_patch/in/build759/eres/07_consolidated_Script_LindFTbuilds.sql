/* Consolidated Script for code merge of Lind FT builds released between builds 1 to 32 */

/* LE build #01 Scripts */

set define off;

DECLARE
  v_item_exists number := 0; 
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'UI_FLX_PAGEVER'
and COLUMN_NAME = 'PAGE_ATTACHDIFF';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_ATTACHDIFF CLOB)';
  dbms_output.put_line('Col PAGE_ATTACHDIFF added');
else
  dbms_output.put_line('Col PAGE_ATTACHDIFF already exists');
end if;
 
END;
/
 
COMMENT ON COLUMN
ERES.UI_FLX_PAGEVER.PAGE_ATTACHDIFF IS
'This column stores the attachment difference of current and previous version';

-->agent -dropdown
-->dose -free text
-->Dose rationale --free text
-->Route of administration--dropdown
-->manufacturer--lookup


declare
v_CodeExists number := 0;
begin
	
	---1---
	-- inserting the second set of drug/device repeating fields--(part of the first set already exists , so will not be adding those again:
	--the fields that already existed are codelst_subtyp = 'dose' , codelst_subtyp = 'doseRationale' , codelst_subtyp = 'route'
	--so we are just going to have these fields as is and then the sequence starts from 2, 3 ..and so on.
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent1', 'Agent1', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr1', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---2------
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent2', 'Agent2', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose2', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl2', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn2', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr2', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--- 3-----

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent3', 'Agent3', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose3', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl3', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn3', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr3', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--4--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent4', 'Agent4', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose4', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl4', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn4', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr4', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--5--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent5', 'Agent5', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose5', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl5', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn5', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr5', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--6--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent6', 'Agent6', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose6', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl6', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn6', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr6', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--7--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent7', 'Agent7', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose7', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl7', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn7', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr7', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--8--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent8', 'Agent8', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose8', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl8', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn8', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr8', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
		
	END IF;

--9--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent9', 'Agent9', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose9', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl9', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn9', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr9', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;



--10--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent10', 'Agent10', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose10', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl10', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn10', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr10', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


COMMIT;
END;
/

-->device -dropdown
-->manufacturer -lookup
-->risk assessment attachment --checkbox


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent1', 'Device 1', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--change this to lookup !!!!!!!!!!!-----------------------------------------
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr1', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt1', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---2---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent2', 'Device 2', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr2', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt2', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---3---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent3', 'Device 3', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr3', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt3', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---4---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent4', 'Device 4', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr4', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt4', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---5---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent5', 'Device 5', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr5', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt5', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	

COMMIT;
END;
/

-->sponsor name -lookup (Sponsor List in 9.2)
-->funding type -dropdown


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName1', 'Sponsor Name1', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType1', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---2---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName2', 'Sponsor Name 2', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType2', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---3---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName3', 'Sponsor Name 3', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType3', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---4---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName4', 'Sponsor Name 4', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType4', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---5---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName5', 'Sponsor Name 5', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType5', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	

COMMIT;
END;
/

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "industry">Industry</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--setting the lookup field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' 
where codelst_subtyp in ('sponsorName1', 'sponsorName2', 'sponsorName3', 'sponsorName4', 'sponsorName5'); 

	
--changing the name of the field that says Grant Serial#/Contract# to COEUS#
update er_codelst set codelst_desc = ' COEUS#' where  codelst_subtyp = 'grant' ;

COMMIT;

---adding the dropdown options to the device agent dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "adapthence">Adapter henceforth</OPTION>
<OPTION value = "affyGene">Affymetrix Gene Chip U133A</OPTION>
<OPTION value = "bipapVision">BIPAP Vision Ventilatory Support System</OPTION>
<OPTION value = "biocomp">Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl</OPTION>
<OPTION value = "bioSeal">Bio-Seal Track Plug</OPTION>
<OPTION value = "block">B-lock solution</OPTION>
<OPTION value = "caphosol">Caphosol</OPTION>
<OPTION value = "cavSpine">Cavity SpineWand</OPTION>
<OPTION value = "cliniMACS">CliniMACS device</OPTION>
<OPTION value = "cryocare">Cryocare</OPTION>
<OPTION value = "daVinci">da Vinci Robotic Surgical System</OPTION>
<OPTION value = "exAblate">ExAblate device</OPTION>
<OPTION value = "galilMed">Galil Medical Cryoprobes</OPTION>
<OPTION value = "goldFid">Gold Fiducial Marker</OPTION>
<OPTION value = "imageGuide">Image Guide Microscope - a high-resolution microendoscope (HRME)</OPTION>
<OPTION value = "imagio">Imagio Breast Imaging System</OPTION>
<OPTION value = "indocyanine">Indocyanine green fluorescence lymphography system</OPTION>
<OPTION value = "infrared">Infrared camera</OPTION>
<OPTION value = "inSpectra">InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300</OPTION>
<OPTION value = "integrated">Integrated BRACAnalysis</OPTION>
<OPTION value = "intensity">Intensity Modulated Radiotherapy</OPTION>
<OPTION value = "iodine125">Iodine-125 radioactive seeds</OPTION>
<OPTION value = "louis3D">LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System</OPTION>
<OPTION value = "lowCoh">Low Coherence Enhanced Backscattering (LEBS)</OPTION>
<OPTION value = "mdaApp">MDA Applicator</OPTION>
<OPTION value = "mirena">Mirena (Levonorgestrel Contraceptive IUD)</OPTION>
<OPTION value = "pemFlex">PEMFlex Solo II High Resolution PET Scanner</OPTION>
<OPTION value = "pointSpectro">Point spectroscopy probe(POS) and PS2</OPTION>
<OPTION value = "prostateImm">Prostate Immobilization Treatment Device</OPTION>
<OPTION value = "senoRxCon">SenoRx Contura MLB applicator</OPTION>
<OPTION value = "SERI">SERI Surgical Scaffold</OPTION>
<OPTION value = "SIR">SIR-spheres</OPTION>
<OPTION value = "sonablate">Sonablate 500 (Sonablate)</OPTION>
<OPTION value = "spectra">Spectra Optia Apheresis System</OPTION>
<OPTION value = "vapotherm">The Vapotherm 2000i Respiratory Therapy Device</OPTION>
<OPTION value = "strattice">Strattice Tissue Matrix</OPTION>
<OPTION value = "syntheCel">SyntheCelTM Dura Replacement</OPTION>
<OPTION value = "ISLLC">The ISLLC Implantable Drug Delivery System (IDDS)</OPTION>
<OPTION value = "visica">The Visica 2 Treatment System</OPTION>
<OPTION value = "theraSphere">TheraSphere</OPTION>
<OPTION value = "transducer">Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier</OPTION>
<OPTION value = "truFreeze">truFreezeTM System</OPTION>
<OPTION value = "UB500">UB500</OPTION>
<OPTION value = "viOptix">ViOptix continuous transcutaneous tissue oximetry device</OPTION>
<OPTION value = "vendys">VENDYS-Model 6000 B/C</OPTION>
<OPTION value = "veridex">Veridex CellSearch assay</OPTION>
<OPTION value = "visica">Visica 2 Treatment System</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN('deviceAgent1' , 'deviceAgent2', 'deviceAgent3', 'deviceAgent4', 'deviceAgent5');

--setting up the look up field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' where 
codelst_subtyp in ('devMnftr1', 'devMnftr2', 'devMnftr3', 'devMnftr4', 'devMnftr5');

COMMIT;

---adding the dropdown options to the drug agent dropdown. There are 10 of these dropdowns on the screen.


UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib>Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN('drugAgent1',
'drugAgent2',
'drugAgent3',
'drugAgent4',
'drugAgent5',
'drugAgent6',
'drugAgent7',
'drugAgent8',
'drugAgent9',
'drugAgent10');

--setting up the lookup field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' 
where codelst_subtyp in ('drugMnftr1', 'drugMnftr2', 'drugMnftr3', 'drugMnftr4', 'drugMnftr5','drugMnftr6', 'drugMnftr7','drugMnftr8', 'drugMnftr9', 'drugMnftr10'); 

COMMIT;

---adding the dropdown options to the route of admn dropdown. There are 10 of these dropdowns on the screen.
--2----
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Aural">Aural</OPTION>
<OPTION value = "Epidural">Epidural</OPTION>
<OPTION value = "Inhalation">Inhalation</OPTION>
<OPTION value = "Intraarterial">Intraarterial</OPTION>
<OPTION value = "Intradermal">Intradermal</OPTION>
<OPTION value = "Intralesional">Intralesional</OPTION>
<OPTION value = "Intramucosal">Intramucosal</OPTION>
<OPTION value = "Intramuscular">Intramuscular</OPTION>
<OPTION value = "Intranasal">Intranasal</OPTION>
<OPTION value = "Intraocular">Intraocular</OPTION>
<OPTION value = "Intraosseous">Intraosseous</OPTION>
<OPTION value = "Intraperitoneal">Intraperitoneal</OPTION>
<OPTION value = "Intrapleural">Intrapleural</OPTION>
<OPTION value = "Intrathecal">Intrathecal</OPTION>
<OPTION value = "Intratumoral">Intratumoral</OPTION>
<OPTION value = "Intravenous">Intravenous</OPTION>
<OPTION value = "Intravesicular">Intravesicular</OPTION>
<OPTION value = "Ocular">Ocular</OPTION>
<OPTION value = "Oral">Oral</OPTION>
<OPTION value = "Rectal">Rectal</OPTION>
<OPTION value = "Subcutaneous">Subcutaneous</OPTION>
<OPTION value = "Sublingual">Sublingual</OPTION>
<OPTION value = "Topical">Topical</OPTION>
<OPTION value = "Transdermal">Transdermal</OPTION>
<OPTION value = "Vaginal">Vaginal</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'routeAdmn2',
'routeAdmn3',
'routeAdmn4',
'routeAdmn5',
'routeAdmn6',
'routeAdmn7',
'routeAdmn8',
'routeAdmn9',
'routeAdmn10');

COMMIT;

/* LE build #02 Scripts */

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'role', 'Collaborators', 'Collaborators', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'New Protocol' where OBJECT_NAME = 'irb_menu' and OBJECT_SUBTYPE = 'new_menu';

-- Fixed Bug # 20707 
update ER_OBJECT_SETTINGS set object_sequence= 7  where object_name='irb_new_tab' and object_subtype='irb_approv_tab';

-- Fixed Bug # 20516
update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'Attachments' where object_name='irb_new_tab' and object_subtype='irb_upload_tab' and fk_account=0;

commit;

-- Added these 3 fields in Subject selection.
-->Inclusion Criteria - textarea
-->Inclusion Criteria N/A - textarea
-->Exclusion Criteria - textarea
-->Exclusion Criteria N/A - textarea

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteria', 'Inclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteriaNa', 'Inclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteria', 'Exclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteriaNa', 'Exclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
COMMIT;
END;
/

update er_codelst set codelst_desc='MDACC' where codelst_type='INDIDEHolder' and codelst_subtyp='organization';

commit;

declare
v_CodeExists number := 0;
v_PkCode number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';	
IF (v_CodeExists = 0) THEN
	INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
	VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs_ids', 'Department Chair User ID','N',1, 'readonly-input');
	COMMIT;
	dbms_output.put_line('One row inserted');
ELSE
	dbms_output.put_line('Row already exists');
END IF;

SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs';
IF (v_CodeExists = 0) THEN
	INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
	VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs', 'Department Chair',1, 'lookup', '{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}');
	COMMIT;
	dbms_output.put_line('One row inserted');
ELSE
	dbms_output.put_line('Row already exists');
END IF;

	
END;
/

create or replace PROCEDURE        "SP_AUTOGEN_STUDY" (p_account number, p_studynum out varchar2 )
as
V_STUDYNO VARCHAR2(100);
v_studynumber varchar2(100)  := null ;
v_sql varchar2(4000);
V_LAST_ID NUMBER;
v_Count number;
BEGIN
    WHILE V_STUDYNUMBER is null
    LOOP
    SELECT STUDYNUM_AUTOGEN_SQL,(STUDYNUM_LASTID + 1) AS STUDYNUM_LASTID INTO V_SQL,V_LAST_ID FROM ER_ACCOUNT_SETTINGS WHERE FK_ACCOUNT = P_ACCOUNT;
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNO;
    SELECT COUNT(*) INTO V_COUNT FROM ER_STUDY WHERE STUDY_NUMBER = V_STUDYNO || V_LAST_ID;
    UPDATE ER_ACCOUNT_SETTINGS SET STUDYNUM_LASTID = V_LAST_ID  WHERE FK_ACCOUNT = P_ACCOUNT;
    if(V_COUNT = 0) then
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNUMBER;
    v_studynumber:=RPAD(v_studynumber,6,'0');
    P_STUDYNUM := V_STUDYNUMBER || V_LAST_ID;
    END IF;
    end LOOP ;
End;
/

/* LE build #03 Scripts */

declare
v_CodeExists number := 0;
v_PkCode number := 0;
begin
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
end;
/

CREATE OR REPLACE PROCEDURE ERES."SP_AUTOGEN_PATIENT" (p_account number, p_patientid out varchar2 )
as
v_patientid varchar2(100);
v_sql varchar2(4000);
v_last_id number;
Begin
    select patientid_autogen_sql,(patientid_lastid + 1) as patientid_lastid into v_sql,v_last_id from er_account_settings where fk_account = p_account and
      PATIENTID_AUTOGEN_SQL is not null;
    update er_account_settings set patientid_lastid = v_last_id  where fk_account = p_account;
    execute immediate v_sql into v_patientid;
    p_patientid := v_patientid || v_last_id;
End ;
/

create or replace PROCEDURE        "SP_AUTOGEN_STUDY" (p_account number, p_studynum out varchar2 )
as
V_STUDYNO VARCHAR2(100);
v_studynumber varchar2(100)  := null ;
v_sql varchar2(4000);
V_LAST_ID NUMBER;
v_Count number;
BEGIN
    WHILE V_STUDYNUMBER is null
    LOOP
    SELECT STUDYNUM_AUTOGEN_SQL,(STUDYNUM_LASTID + 1) AS STUDYNUM_LASTID INTO V_SQL,V_LAST_ID FROM ER_ACCOUNT_SETTINGS WHERE FK_ACCOUNT = P_ACCOUNT and
      STUDYNUM_AUTOGEN_SQL is not null;
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNO;
    SELECT COUNT(*) INTO V_COUNT FROM ER_STUDY WHERE STUDY_NUMBER = V_STUDYNO || V_LAST_ID;
    UPDATE ER_ACCOUNT_SETTINGS SET STUDYNUM_LASTID = V_LAST_ID  WHERE FK_ACCOUNT = P_ACCOUNT;
    if(V_COUNT = 0) then
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNUMBER;
    v_studynumber:=RPAD(v_studynumber,6,'0');
    P_STUDYNUM := V_STUDYNUMBER || V_LAST_ID;
    END IF;
    end LOOP ;
End;
/

/* LE build #04 Scripts */

--For issue with updation in seq #5
--crcAppealed

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAppealed';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAppealed', 'CRC Appealed', 'departmental,Study','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcApproved

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcApproved', 'CRC Approved', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvContngs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvContngs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvContngs', 'CRC Approved With Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvMinorCon

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvMinorCon';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvMinorCon', 'CRC Approved With Minor Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvContngs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvContgncs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvContgncs', 'CRC Approved With Major Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcDeferred

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDeferred';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcDeferred', 'CRC Deferred', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcDisapproved

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDisapproved';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcDisapproved', 'CRC Disapproved', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcInRev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcInRev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcInRev', 'CRC In Review', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcReturn

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcReturn';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcReturn', 'CRC Modifications Required', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcRejected

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcRejected';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcRejected', 'CRC Rejected', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSentReviewer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSentReviewer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSentReviewer', 'CRC Sent To Reviewers', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSentReviewer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSentReviewer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSentReviewer', 'CRC Sent To Reviewers', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcTempHold

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcTempHold';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcTempHold', 'CRC Temporary Hold', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcPbhsrcRejctd

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcPbhsrcRejctd';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcPbhsrcRejctd', 'CRC/PBHSRC Rejected', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcResubmitted

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcResubmitted';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcResubmitted', 'HRPP Resubmitted', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSubmitted

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSubmitted';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSubmitted', 'HRPP Submitted', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medAncRevCmp

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevCmp';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'medAncRevCmp', 'Medical or Ancillary Review Completed', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medAncRevPend

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevPend';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'medAncRevPend', 'Medical or Ancillary Review Pending', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_work_start

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_work_start';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_work_start', 'IRB: Application Work Started', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--app_CHR

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'app_CHR', 'IRB: Approved', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_defer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_defer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_defer', 'IRB: Deferred', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--rej_CHR

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'rej_CHR';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'rej_CHR', 'IRB: Disapproved', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_human

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_human';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_human', 'IRB: Human Research, Not Engaged', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_in_rev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_in_rev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_in_rev', 'IRB: In Review', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_init_subm

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_init_subm', 'IRB: Initial Submission', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_add_info

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_add_info';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_add_info', 'IRB: Modifications Required', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_not_human

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_not_human';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_not_human', 'IRB: Not Human Research', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--pi_resp_rev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'pi_resp_rev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'pi_resp_rev', 'IRB: PI Response Sent to Reviewers', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_ammend

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_ammend';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_ammend', 'IRB: Submission - Amendment', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_renew

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_renew';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_renew', 'IRB: Submission - Renewal', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_temp_hold

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_temp_hold';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_temp_hold', 'IRB: Temporary Hold', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_terminated

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_terminated';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_terminated', 'IRB: Terminated', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_withdrawn

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_withdrawn';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_withdrawn', 'IRB: Withdrawn', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For issue with update in seq #6

--dsmbInt

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbInt';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'dsmbInt', 'Data Safety and Monitoring Board', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--expdBiostat

declare
v_CodeExists number := 0;
begin
	--need to review custom col1 update
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expdBiostat';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'expdBiostat', 'Are you requesting an expedited biostatistical review for this protocol?', 'checkbox','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

DECLARE
v_ItemExists number := 0;
v_LkpId number := 100090;
BEGIN

select count(*) into v_ItemExists from ER_LKPLIB where LKPTYPE_NAME like 'Sponsors%';
if (v_ItemExists > 0) then
  dbms_output.put_line('Sponsors Lookup already exists');
  return;
end if;
select count(*) into v_ItemExists from ER_LKPLIB where PK_LKPLIB = v_LkpId;
if (v_ItemExists > 0) then
  dbms_output.put_line('Lookup ID '||v_LkpId||' already exists');
  return;
end if;

Insert into ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_VERSION,LKPTYPE_DESC,
LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) values (v_LkpId,'Sponsors','1.0','Sponsors Lookup',null,null,null);

Insert into ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,
LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,
LKPVIEW_FILTER,LKPVIEW_KEYWORD) values (v_LkpId,'Sponsors','Sponsors Lookup',v_LkpId,null,null,null,null,null,'CustLkp'||v_LkpId);

Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,
LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values 
((select NVL(max(PK_LKPCOL),0)+1 from ER_LKPCOL),v_LkpId,'custom002','Sponsor_Number','varchar2',null,'ER_LKPDATA','Sponsor_Number');

Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,
FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values 
((select NVL(max(PK_LKPVIEWCOL),0)+1 from ER_LKPVIEWCOL), (select NVL(max(PK_LKPCOL),0) from ER_LKPCOL),'Y',1,v_LkpId,'50%','Y');

Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,
LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values 
((select NVL(max(PK_LKPCOL),0)+1 from ER_LKPCOL),v_LkpId,'custom001','Sponsor_Name','varchar2',null,'ER_LKPDATA','Sponsor_Name');

Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,
FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values 
((select NVL(max(PK_LKPVIEWCOL),0)+1 from ER_LKPVIEWCOL), (select NVL(max(PK_LKPCOL),0) from ER_LKPCOL),'Y',2,v_LkpId,'50%','Y');

COMMIT;

dbms_output.put_line('Lookup ID '||v_LkpId||' structure is created for Sponsors');

END;
/

-- 04

DECLARE
v_ItemExists number := 0;
v_LkpId number := 100090;
BEGIN

select count(*) into v_ItemExists from ERES.ER_LKPDATA where FK_LKPLIB = v_LkpId;
if (v_ItemExists > 0) then
  dbms_output.put_line('Lookup data for ID='||v_LkpId||' already exists');
  return;
end if;
select count(*) into v_ItemExists from ERES.ER_LKPLIB where LKPTYPE_NAME like 'Sponsors%' and PK_LKPLIB <> v_LkpId;
if (v_ItemExists > 0) then
  dbms_output.put_line('Another Sponsors Lookup already exists');
  return;
end if;
select count(*) into v_ItemExists from ERES.ER_LKPLIB where LKPTYPE_NAME like 'Sponsors%';
if (v_ItemExists = 0) then
  dbms_output.put_line('Sponsors Lookup must exist before lookup data can be inserted');
  return;
end if;
select count(*) into v_ItemExists from ERES.ER_LKPLIB where PK_LKPLIB = v_LkpId;
if (v_ItemExists = 0) then
  dbms_output.put_line('Lookup ID='||v_LkpId||'must exist before lookup data can be inserted');
  return;
end if;

Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arrien Pharmaceuticals','3501',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arrowhead Cancer Center','3976',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arthritis Foundation','5053',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arthur Vining Davis Foundation','3537',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Asante','3701',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ASCENTA THERAPEUTICS INC','80',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Asian Fund for Cancer Research','5006',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Assn Francaise Contre les Myopathies AFM','855',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Assoc. of Univ Radiologists AUR','5114',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Association of American Cancer Institute','3575',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ASSURANCE BI0SCIENCES INC','399',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Astellas Pharma US Inc','680',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Astex Pharmaceuticals, Inc','597',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ASTRA TECH','126',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AstraZeneca','38',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Asuragen','5016',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'A-T CHILDRENS PROJ','800',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ATOS MEDICAL INC','760',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Atterocor, Inc.','3646',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Augusta Health Cancer Center','3608',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AVALON PHARMACEUTICALS INC','99',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AVAX TECHNOLOGIES INC','204',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AVEO PHARMACEUTICALS INC','318',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AVILA THERAPEUTICS INC','250',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AVON FOUNDATION','101',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Banner MD Anderson Cancer Ctr','3671',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baptist Hospital of Miami','3804',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baptist Medical Center','3776',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baptist Memorial Hospital - Desoto','3753',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Basilea Pharmaceutica Ltd','413',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baton Rouge General Med Ctr - Mid-City','3609',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BAXTER HEALTHCARE CORP','807',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bay Area Cancer Care Center','3983',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BAYER HEALTHCARE PHARMACEUTICALS','698',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bayer MaterialScience LLC','5092',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baylor College of Medicine','5',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Baylor Institute for Immunology Research','5054',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BAYLOR RESEARCH INSTITUTE','6',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bayshore Medical Center','3610',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BC Cancer Agency, Centre for the North','3611',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BCCA-Vancouver Cancer Centre','3732',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Beckman Research Inst','5181',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BELLICUM PHARMACEUTICALS INC','203',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BEN AND CATHERINE IVY FOUNDATION','224',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Benaroya Res Inst at Virginia Mason','5182',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Berg Pharma LLC','3536',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BERLEX LAB INC','127',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BETH ISRAEL DEACONESS MEDICAL CENTER','67',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIDMC','3556',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIO TEX  INC','319',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOCARTIS','831',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOCEPT, INC.','702',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BioChem Pharmaceuticals','907',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Biocompatibles, Inc.','5121',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOCRYST PHARMACEUTICALS INC','103',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOENVISION','788',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOFORTUNA LTD','819',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Biogen Idec, Inc','197',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BioLineRx Ltd','3532',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOLOGENE PHARMACEUTICAL LIMITED','475',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BioMarin Pharmaceuticals Inc.,','3579',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BioMed Valley Discoveries','5035',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BioMerieux SA','5026',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'bioMERIEUX, INC','847',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIONICHE THERAPEUTICS LTD','446',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIONOVO INC','81',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIONUMERIK PHARMACEUTICALS','450',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIO-PATH HOLDINGS INC','320',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOSPHERE MEDICAL INC','128',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'bioTheranostics, Inc','848',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIOVIEW INC','655',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BIPAR SCIENCES INC','163',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BLACK HILLS CTR FOR INDIAN HEALTH','171',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bladder Cancer Advocacy Network (BCAN)','5174',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Blanchard Valley Regional Cancer Center','3612',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bluebird Bio','5088',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BOEHRINGER INGELHEIM PHARM INC','208',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BOOZ ALLEN HAMILTON','95',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Boston Medical Center','3756',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Boston Strategics','5046',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BOSTON UNIVERSITY','867',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Boynton Beach Radiation Oncology','3581',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BRAIN TUMOR FUNDERS COLLABORATIVE BTFC','321',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Brains Together for a Cure','5095',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Brandon C Gromada Head Neck Cancer Fndn','5188',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BREAST CANCER RELIEF FNDTN','772',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BREAST CANCER RESEARCH FOUNDATION','51',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BRIGHAM AND WOMEN''S HOSPITAL','451',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Bristol-Myers Squibb','3',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'British Columbia Cancer Agency','5167',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Brown Fndtn Inst Molecular Med','5199',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BSA Health System, LLC','5050',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Buffalo Medical Group','4061',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'BURROUGHS WELLCOME FUND','462',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Butler Hospital','5012',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'C4 IMAGING LLC','100',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CALISTOGA PHARMACEUTICALS INC','463',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Calithera Biosciences, Inc.','5180',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Callisto  Pharmaceuticals, Inc.','182',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANADIAN INST OF HEALTH RESEARCH (CIHR)','766',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer Care Manitoba','4701',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer Center of Santa Barbara','3830',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer Centers of Southwest Oklahoma','3614',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer InCITe, LLC','5038',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANCER INTERNATIONAL RESEARCH GRP (CIRG)','129',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer Prevention Pharmaceuticals, INC','5044',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANCER PREVENTION RESEARCH INST OF TX','30',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANCER RESEARCH INSTITUTE','322',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cancer Sci Institute of Singapore','3567',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANCER TREATMENT RESEARCH FOUNDATION','751',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CANCERVAX CORP','172',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cape Radiation Oncology','4068',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Capital Region Medical Center','4472',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CARACAL INC','551',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Care Progress, LLC','3519',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CAREFUSION CORPORATION','323',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CARING FOR CARCINOID FOUNDATION','324',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Carl & Marie Jo Anderson Charitable Fndt','468',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Carle Clinic Association','3839',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Carolinas HealthCare Fndtn','5175',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Carolinas Medical Center - Union','3615',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Catamaran Health','3546',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Catawba Valley Medical Center','3783',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CDx Laboratories Inc.','241',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CELATOR PHARMACEUTICALS INC','218',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Celek Pharmaceuticals, LLC','897',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CELGENE','700',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cell Medica','5130',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cell Point','603',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CELL THERAPEUTICS, INC.','444',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CELLDEX THERAPEUTICS INC','469',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cellerant Therapeutics, Inc.','3689',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Centegra Sage Cancer Center','4478',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Center for Cancer Care Goshen Hospital','4059',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CENTERS FOR DISEASE CONTROL PREVENTION','105',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CENTERS FOR MEDICARE & MEDICAID SERVICES','102',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CENTOCOR','445',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CentraCare Health System','3821',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Central Maine Medical Center','4465',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Central Michigan Community Hospital','4376',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Centre Hospitalier Universitaire DeSherb','4009',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CEPHALON','59',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CEPHEID INC','154',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CEPTYR INC','503',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cerexa, Inc.','5171',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cerulean Pharma Inc.','5205',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHARLES A DANA FOUNDATION','131',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHARLES W AND JUDY SPENCE TATE CHAR FNDN','325',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Charlotte-Mecklenburg Hospital','5150',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ChemGenex Pharmaceuticals, Inc.','132',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHEMOKINE THERAPEUTICS CORP','341',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cherry Tree Cancer Center','3975',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHIANG CHING KUO FNDTN','756',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHILDRENS BRAIN TUMOR FOUNDATION','205',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Children''s Hosp of Philadelphia','3592',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHILDRENS HOSPITAL - LOS ANGELES','612',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Childrens Hospital Colorado','3585',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Children''s Hospital Corporation','5117',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHILDRENS LEUKEMIA RESEARCH ASSOC INC','326',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Childrens Oncology Group','504',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Children''s Tumor Foundation','5068',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHIMERIX INC','627',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHIRON THERAPEUTICS','104',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cholangiocarcinoma Foundation','5223',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Christ Hospital Cancer Center','4056',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Christiana Care Health Services','841',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHRISTUS St. Frances Cabrini Hospital','3613',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHROMA THERAPEUTICS LTD','505',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHUGAI PHARMACEUTICAL CO LTD','327',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CHUNGNAM NATIONAL UNIVERSITY KOREA','330',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CINCINNATI CHILDRENS HOSP MEDICAL CTR','775',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cincinnati Children''s Hosptial Med Ctr','5104',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CIPHERGEN BIOSYSTEMS  INC','454',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CIRCADIAN TECHNOLOGIES LTD','506',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'City of Hope National Med Ctr','4051',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CJW Medical Center','3639',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CLAYTON FOUNDATION FOR RESEARCH','141',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cleveland Clinic Foundation','4705',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cleveland Clinic Lerner College','5093',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CLEVELAND FOUNDATION','142',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Clinical Financial Services, LLC','3673',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Clinipace, Inc.','5168',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CLL GLOBAL RESEARCH FOUNDATION','52',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Clovis Oncology','3698',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COHEN-REINACH FAMILY CHARITABLE FNDTN','78',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cold Spring Harbor Laboratory','3558',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COLLABORATIVE EPENDYMOMA RSRCH NETWORK','507',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Collaborative Medical Research, LLC','614',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COLOPLAST INC','133',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Columbia St. Mary''s Hospital  - Ozaukee','3622',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COLUMBIA UNIVERSITY','868',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Columbia University Medical Center','3710',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COMBINATORX INC','748',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COMMUNITIES FNDTN OF TX','238',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Community Medical Center','4476',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Company Alliance Cardiovas','5184',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Comprehensive Cancer Centers of Nevada','3815',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COMPUGEN LTD','623',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CONCERN FOUNDATION','331',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cone Health Cancer Center','3782',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CONFORMA THERAPEUTICS CORP','332',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CONQUER CANCER FOUNDATION','68',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Convergen LifeSciences Inc','508',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COOK MEDICAL INC','342',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CORE SCIENCE SOLUTIONS LLC','509',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cornell University','3555',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CORNERSTONE SYSTEMS NORTHWEST INC','786',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Corp. Centro Internaional','5139',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COUGAR BIOTECHNOLOGY INC','192',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COUNCIL FOR RESPONSIBLE NUTRITION','143',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COVANCE INC','883',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'COVIDIEN','514',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Creatv MicroTech, Inc.','3520',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Creighton University','5091',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CRITICAL CARE INNOVATIONS INC','134',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Critical Outcome Technologies, Inc.','5169',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Crohns & Colitis Foundation of America','875',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CSSSTR, Volet Centre Hospitalier','3616',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CUBIST PHARMACEUTICALS INC','515',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CURAGEN CORPORATION','144',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CURE CHILDHOOD CANCER','762',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Cure GBM, LLC','5210',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CureSearch for Children''s Cancer','898',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CURETECH LTD','135',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CURIS INC','333',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CYCLACEL PHARMACEUTICALS INC','145',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CYLENE PHARMACEUTICALS INC','240',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CYTOGEN CORP','604',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'CytRx Corp','621',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DAIICHI RADIOISOTOPE LABORATORIES','334',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DAIICHI SANKYO CO LTD','522',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DAINIPPON SUMITOMO PHARMA CO LTD','761',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DAMON RUNYON CANCER RESEARCH FNDTN','774',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'D''Amour Center for Cancer Care','3750',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dana Foundation','510',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DANA-FARBER CANCER INSTITUTE','343',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dani''s Foundation','3509',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DANISH COUNCIL FOR INDEPENDENT RESEARCH','706',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Danville Regional Medical Center','3797',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DARTMOUTH MEDICAL SCHOOL','335',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'David C Pratt Cancer Center','3748',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dayton Physicians, LLC','3831',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DECIPHERA PHARMACEUTICALS LLC','523',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DEEP BREEZE LTD','111',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DEKK-TEC INC','155',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DelMar Pharmaceuticals','5049',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DELTA-FLY PHARMA INC','715',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DENDREON','402',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DENISE TERRILL CHARITY','136',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dental Innovation Foundation','859',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Department of Education','3505',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Depomed','5074',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DEPT OF HEALTH AND HUMAN SERVICES','1',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DePuy Synthes','746',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DESMOID TUMOR RESEARCH FNDTN','251',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DIAGNOSTIC PRODUCTS CORP','176',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DIASORIN','336',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DIGITAL SCIENCE TECH','401',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dixie Regional Medical Center','3617',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DNA SEQ Alliance','5197',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DNAtrix INC','337',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Doctors Cancer Fndtn','5097',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Doctors Medical Center','4375',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Doctor''s Regional Cancer Center 10714','4065',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dompe S.p.a.','846',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dong-A Pharmaceutical Co Ltd','3530',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dongwoo Syntech Company Ltd','527',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Door County Cancer Center','4021',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dot Decimal Inc','5229',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Dr Miriam and Sheldon G Adelson','3535',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DR SCHOLL FOUNDATION','156',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DR SUSAN LOVE RESEARCH FNDTN','431',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DREXEL UNIVERSITY','137',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Drs Martin Dorothy Spatz Charitable Fndt','2583',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'DUKE UNIVERSITY MEDICAL CENTER','138',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Durata Therapeutics, Inc.','843',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EARL C SAMS FNDTN INC','780',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Eastern Cooperative Oncology Group ECOG','887',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ECOG-ACRIN Medical Research Fndtn, INC','5140',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EcoNugenics,Inc.','3504',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EDAP TMS SA','616',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EDWARD & MARGARET ROBERTS FOUNDATION','338',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Edwards Comprehensive Cancer Center','3618',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EISAI INC','157',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EKR THERAPEUTICS','344',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'El Paso Cancer Treatment Center','3814',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Elan Pharmaceuticals Inc.','243',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Elekta Limited','5045',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ELEOS INC','158',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ELI LILLY AND COMPANY','7',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ellison Medical Fndtn','5073',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Elmhurst Memorial Hospital','4064',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ELSA U PARDEE FOUNDATION','139',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EMD SERONO INC','146',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EMERALD FOUNDATION INC','339',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emergency Medicine Foundation','856',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emergent BioSolutions Inc.','864',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emerson Hospital','3619',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emily''s Power for a Cure Foundation','5200',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EMMES Corporation','3513',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EMORY UNIVERSITY','140',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emory University Hospital Midtown','3787',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Emory University Winship Cancer Center','3736',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ENDOCARE INC','528',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Endocare, Inc.','5133',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Endocrine Fellows Foundation','3503',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ENDOREX','164',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Englewood Hospital','4004',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Enterprise Advisory Services, Inc.','5084',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Entertainment Industry Foundation','5176',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ENZON PHARMACEUTICALS INC','149',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EPIZYME','842',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Erasmus MC','5060',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Erlanger Medical Ctr','3650',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Erler Lab, Biotech Res Innov Ct (BRIC)','5238',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Esperance Pharmaceuticals, Inc','3515',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Essentia Health Cancer Center','4462',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ETHICON INC','340',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Eunice Kennedy Shriver Nat Inst of Child','555',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Euro Diagnostica AB','5075',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EUROPEAN COMMISSION','403',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EUSA PHARMA INC','207',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Eutropics Pharmaceuticals','5144',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EVENTUS DIAGNOSTICS LTD','787',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Eventus Diagnostics, Inc','3559',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'EVORX TECHNOLGIES INC','713',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Exelixis','54',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Exempla St. Joseph Hospital','3816',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Exemplar Genetics','874',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Exosome Diagnostics','3540',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FANCONI ANEMIA RESEARCH FUND INC','757',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FAVRILLE INC','797',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Federal University of Rio Grande do Sul','5025',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FEINSTEIN INSTITUTE FOR MED RESEARCH','529',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Finley Hospital','3988',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'First Health of the Carolinas','3838',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FKD Therapies Oy','866',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Fletcher Allen Health Care','3716',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FLIGHT ATTENDANT MEDICAL RESEARCH INST','490',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Florida International University','5228',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FNDTN FOR ANESTHESIA EDUC AND RESEARCH','764',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Fndtn for Informed Med Decision Making','618',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOCUS SURGERY INC','648',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Focused Ultrasound Surgery Fndtn','3512',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOGARTY INTERNATIONAL CENTER DHHS NIH','353',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ford Foundation Fellowship Program','5129',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Forest Research Institute, Inc.','5170',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Foundation for Gynecologic Oncology','5107',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOUNDATION FOR RESEARCH','455',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Foundation For Surgical Fellowships','5143',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOUNDATION FOR THE NATL INST OF HEALTH','530',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOUNDATION FOR WOMENS CANCER','166',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FOUNDATION MEDICINE INC','167',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FRED HUTCHINSON CANCER RESEARCH CTR','517',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FRIEDREICH''S ATAXIA RESEARCH ALLIANCE','346',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FRIENDS FOR AN EARLIER BREAST CANCER TST','516',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FRONTIER SCI AND TECH RESEARCH FNDTN','151',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FUDAN UNIVERSITY','258',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FujiFilm Corporation','5159',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'FUJIREIBIO DIAGNOSTICS INC','347',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GABRIELLES ANGEL FDN FOR CANCER RESEARCH','350',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Galaxybiotech','5017',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Galderma R&D SNC','3634',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GALENA BIOPHARMA INC','531',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GALIL MEDICAL','767',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GAMIDA CELL LTD','348',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gaston Memorial Hospital','3994',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GATEWAY FOR CANCER RESEARCH','168',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gay & Lesbian Med Assoc Lesbian','5108',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GE HEALTHCARE','259',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Geisinger Health System','3682',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genalyte Inc.','5069',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENENTECH INC','44',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genesis Healthcare Partners - Escondido','3722',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genesis Medical Center','4453',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genesys Hurley Cancer Institute','3620',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENEVA FNDTN','795',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENITOPE CORPORATION','189',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENMAB INC','260',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GenomeDx Biosciences Inc.','5098',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genomic Health Inc.','5028',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genprex, INC','5158',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gen-Probe, Inc.','244',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENSOLVE INC','349',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GENTA INCORPORATED','607',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gentium S.P.A.','3570',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Genzyme','40',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GEORGE AND MARY JOSEPHINE HAMMAN FNDTN','532',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'George Washington Univ Med Ctr','3527',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GEORGETOWN UNIV MEDICAL CENTER','518',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GEORGETOWN UNIVERSITY','569',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Georgia Health Sci Ctr','5022',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GEORGIA INSTITUTE OF TECHNOLOGY','824',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Georgia Regents Univ','5119',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Georgia State University','873',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GERON CORPORATION','173',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gertner Institute','829',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GILEAD SCIENCES INC','533',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GILSON-LONGENBAUGH FOUNDATION','301',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GlaxoSmithKline','4',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GLENN FOUNDATION FOR MEDICAL RESEARCH','351',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Glipper Oncology Research, Inc.','5201',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GLOUCESTER PHARMACEUTICALS INC','261',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GOLDHIRSH FOUNDATION','535',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GOLFERS AGAINST CANCER FOUNDATION','662',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Good Samaritan Hospital','3696',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Google Inc','5113',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Government of Canada','3566',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Grady Health Sys-Grady Mem Hosp','3712',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Grand View Hospital','3811',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Grants Default Sponsor','837',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Greater Baltimore Medical Ctr','3793',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Grifols Inc.','3596',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Grossmont Hospital','3799',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GROUP HEALTH COOPERATIVE','600',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GSK Biologicals','560',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Gulf Coast Consortia','3687',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Guthrie Health Care System','3835',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'GYNECOLOGIC ONCOLOGY GROUP','534',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'H LEE MOFFITT CANCER CENTER','170',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hahnemann University Hospital','4703',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HAMAMATSU CORPORATION','352',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HANA BIOSCIENCES INC','109',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Harrington Cancer Center','3795',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Harrington Discovery Institute','5047',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Harris County Hospital District','3683',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HARRIS CTY PUBLIC HEALTH AND ENV SVC','773',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Harvard Medical School','5023',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Harvard University','246',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HEALTH DISCOVERY CORPORATION','476',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Health Outcome Solutions','3661',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HEALTH RES AND SVC ADMN - DHHS','602',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Health Research, Inc','857',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HEARST FNDTN','752',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Heartland Regional Medical Center','4479',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HELEN HAY WHITNEY FNDTN','808',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HELIX BIOPHARMA CORP','536',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hennepin County Medical Ctr','3715',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Henry Ford Health Systems','3594',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HENRY M JACKSON FOUNDATION','187',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Highland Hospital','3828',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Highland Park Hospital','4477',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hill Radiation Oncology','4023',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hillcrest Medical Center','3755',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HIRSHBERG FNDTN - PANCREATIC CANCER RES','233',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hispanic Assoc of Colleges and Univ','3561',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hitachi Chemical Co., Ltd.','5136',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOFFMAN-LA ROCHE INC','686',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Holy Cross Hospital','4000',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hongda Group Limited, LLC','5109',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOPE FOUNDATION','98',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOPE STREET KIDS','520',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hospira�s Global Clinical R&D & Medical','3658',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hospital Maisonneuve - Rosemont','3757',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hotel Dieu de Quebec','3758',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Houston Chapter Oncology Nursing Society','5172',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Houston Endowment Inc','537',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOUSTON GALVESTON AREA COUNCIL','488',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOUSTON PHARMACEUTICAL INC','262',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOWARD HUGHES MEDICAL INSTITUTE','174',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Howard University Hospital','3727',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HOWMEDICA OSTEONICS CORP','753',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HTG Molecular Diagnostics, Inc','5149',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HUNTER COLLEGE','538',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Hutcheson Fuller Cancer Center','3695',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'HYUNDAI HOPE ON WHEELS','632',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ICHIGAYA TRS INC','354',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ICON Clinical Research, Inc','3511',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Idera Pharmaceuticals','5141',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IDM INC','467',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Igenica, Inc.','5135',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ignyta Inc.','5222',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Illumina Inc.','5132',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IMCLONE SYSTEMS INC','521',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Immune Design Corp.','5128',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IMMUNOCORE LIMITED','394',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ImmunoGen, Inc.','248',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IMMUNOMEDICS  INC','456',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IMPALA INC','3636',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IMPERIAL COLLEGE of LONDON','494',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INC Research Inc','838',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INCYTE CORPORATION','75',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INDIAN AMERICAN CANCER NETWORK IACAN','539',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INDIANA UNIVERSITY','181',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Indiana University Health','3718',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INEX PHARMACEUTICALS','209',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INFINITY PHARMACEUTICALS INC','165',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'InGeneron Incorporated','3573',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Innovive Pharmaceuticals','610',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INST OF CANCER RES - UK','263',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Inst of Health and Biomedical Innovation','5062',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INST OF INTERNATIONAL EDUCATION','770',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Inst of Museum and Lib Svc, St Lib Pgm','5080',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'InstantLabs','836',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Institut Gustave Roussy Tr�sorerie','3553',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INSTITUT MERIEUX','834',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Institute for Radiation Therapy','4014',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INSTITUTE FOR RESEARCH IN BIOMEDICINE','865',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Institute for Systems Biology','540',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Institute of Medicine','5102',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INSYS THERAPEUTICS INC','541',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTEGRATED THERAPEUTICS GROUP','73',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Intel(r) Parallel Computing Ctr','5221',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Intelligent Automation, Inc.','3539',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTELLIKINE INC','620',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Intermountain Medical Center','4011',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTERNATIONAL EPIDEMIOLOGY INSTITUTE LTD','358',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTERNATIONAL MYELOMA FOUNDATION','383',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTL ASSOC FOR THE STUDY OF LUNG CANCER','457',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTL ATOMIC ENERGY AGENCY','87',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Intl Soc Chemotherapy Infection Cancer','3516',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTROGEN RESEARCH INSTITUTE','473',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INTROGEN THERAPEUTICS INC','543',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'InVentiv Clinical','3693',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'INVITROGEN','183',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IOSI Oncology Institute of','3819',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Iowa City Cancer Treatment Center','3583',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'IPSEN PHARMA SAS','404',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ISIS PHARMACEUTICALS INC','264',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ISOTHERAPEUTICS GROUP LLC','651',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ITO-EN','360',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Jackson County Memorial Hospital - Altus','3603',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Jackson Laboratory','5156',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JAMES EWING FNDTN - SOC OF SUR ONCOLOGY','806',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JAMES MCDONNELL FOUNDATION','186',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JANE COFFIN CHILDS MEM FUND MED RSRCH','791',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Janssen Biotech, Inc','904',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JANSSEN PHARMACEUTICALS INC','217',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JANSSEN RESEARCH AND DEVELOPMENT LLC','710',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Jarrow Formulas, Inc','5207',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Jazz Pharmaceuticals, Inc.','3668',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JFK Medical Center','3803',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JMI LABORATORIES','384',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOAN SCARANGELLO FOUNDATION','405',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOANNA M NICOLAY MELANOMA FDNTN','646',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOHN G MARIE STELLA KENEDY MEM FDN','385',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'John S Dunn Research Foundation','544',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOHN WAYNE CANCER INSTITUTE','265',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOHNS HOPKINS UNIVERSITY','501',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JOHNSON & JOHNSON','687',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Juravinski Cancer Centre','3817',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'JW PHARMACEUTICAL','669',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KADOORIE CHARITABLE FOUNDATION','386',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KAISER FNDTN RESEARCH INST','779',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kaiser Permanente Med Ctr Los Angeles','3740',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KALOBIOS PHARMACEUTICALS INC','83',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kansas State University','619',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kantonsspital St Gallen','3770',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Karmanos Cancer Institute','3725',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Karyopharm Therapeutics, Inc.','5030',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KATZ FOUNDATION','406',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KELLOGG FOUNDATION','545',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KERYX BIOPHARMACEUTICALS INC','159',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KHALIFA BIN ZAYED AL NAHYAN FNDTN','635',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kimberly-Clark Corporation','5033',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KineMed Inc.','3541',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KINEX PHARMACEUTICALS LLC','355',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'King Faisal Specialis Hosp & Res Centre','5216',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KIOR INC','892',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Klinik fur Radio-Onkologie','3769',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KODAK','147',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Korea Institute of Radiological','5024',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KOSAN BIOSCIENCES INC','212',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kuakini Medical Center','3826',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'KUDOS PHARMACEUTICALS','581',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kuwait University','5166',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kyoto Prefectural Univ of Med','5152',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Kyowa Hakko Kirin Pharma Inc','219',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'La Jolla Pharmaceutical Company','881',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LAB DISCOVERIES LTD','570',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LADIES LEUKEMIA LEAGUE','387',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lahey Clinic Northshore Peabody','4467',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LAM FOUNDATION','266',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lancaster General Health Campus','3785',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Landauer Inc.','3597',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lantheus Medical Imaging, Inc.','3506',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LAURI STRAUSS LEUKEMIA FOUNDATION','664',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lawrence Berkeley National Laboratory','548',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LDS Hospital','3728',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Leidos Biomedical Research','5145',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Leo Jenkins Cancer Services','3824',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Leonard Ruditski & Adm Manager','5183',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Leukemia & Lymphoma Society','42',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Leukemia Research Foundation','888',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lewis Gale Medical Center','4473',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LEXICON PHARMACEUTICALS INC','549',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LIDDY SHRIVER SARCOMA INITIATIVE FUND','388',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LIFE BIOSYSTEMS','400',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Life Science Law','3550',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Life Sciences Research Fndtn','5090',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Life Technologies Corp','3543',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lifecell Corporation','599',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LIGAND PHARMACEUTICALS','359',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LIMB PRESERVATION FNDTN','650',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lincoln Memorial University','3525',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LipoMedics Limited','3522',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Livestrong Foundation','5214',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Loma Linda University Medical Center','3741',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Long Island Jewish Medical Center','3743',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LORUS THERAPEUTICS','785',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Los Alamitos Medical Ctr','3986',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Louisiana State Univ','3514',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LOUISIANA STATE UNIV HSC-SHREVEPORT','392',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Lovelace Medical Center - Downtown','3747',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Loyola University','3746',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LSU Agricultural Center','5071',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUDWIG INST FOR CANCER RSCH','749',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUITPOLD PHARMACEUTICALS INC','148',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUNDBECK FNDTN','724',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUNG CANCER RESEARCH FOUNDATION','220',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUNGEVITY FOUNDATION','550',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LUSTGARTEN FNDTN - PANCREATIC CANCER RES','221',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LYMPHOMA RESEARCH FNDTN','267',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LYNNE COHEN FNDTN FOR OVARIAN CANCER','563',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'LYNNTECH INC','231',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'M D ANDERSON CANCER CENTER ORLANDO','255',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MAB GENERATION INC','407',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MALAYSIAN PALM OIL BOARD','268',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MARCH OF DIMES BIRTH DEFECTS FND','269',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MARCUS FOUNDATION','74',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MARSHA RIVKIN CTR OVARIAN CANCER RSRCH','654',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MARVAL BIOSCIENCES INC','784',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mary K Chapman Fndtn','5057',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MASSACHUSETTS GENERAL HOSPITAL','46',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MATHERS FOUNDATION','565',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MATHEW LARSON PEDI BRAIN TUMOR FNDTN','633',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mayo Clinic Arizona','5072',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MAYO CLINIC JACKSONVILLE','357',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mayo Clinic Phoenix','3991',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MAYO CLINIC ROCHESTER','566',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mayo-Albert Lea Medical Center','4075',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MCGILL UNIV','823',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MCNEIL CONSUMER & SPECIALTY PHARMACEUT','393',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MD Anderson Cancer Center','5064',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Chronic Lymphocytic Leukemia ResFund','731',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Keck Centr Canc Gene Therapy Dev Awd','745',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA KHALIFA BIN ZAYED AL NAHYAN FNDTN','712',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Leukemia SPORE Career Developmnt Awd','736',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Melanoma & Skin Cancer Research Prog','738',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Multidisciplinary Research Program','739',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MDA Roy & Phyllis Gough Huffington Fund','742',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mebiopharm','609',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDAREX, INC.','701',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDICAL COLLEGE OF WISCONSIN','117',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Medical College of Wisconsin','3809',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDICAL PROGNOSIS INSTITUTE','422',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Medical Univ- South Carolina Charleston','3739',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDICAL UNIVERSITY OF SOUTH CAROLINA','270',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Medical University of Vienna','5153',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MedImmune LLC','556',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDIVATION INC','567',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Medpace Inc.','5160',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MedStar Health Research Inst','3551',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEDTRONIC INC','408',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEHTA FAMILY FOUNDATION','271',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEI Pharma, Inc.','3688',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Melanoma International Foundation','899',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MELANOMA RSRCH ALLIANCE','213',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEMGEN LLC','153',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Memorial Hermann Health System','5014',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Memorial Hospital of Carbondale','4460',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Memorial Medical Center','3775',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MEMORIAL SLOAN KETTERING CANCER CTR','524',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Menorah Medical Center','3694',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MERCATOR MEDSYSTEMS INC','634',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MERCK','41',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mercy Philadelphia Hospital','3738',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mercy Regional Cancer Center','4076',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Merrimack Pharmaceuticals, Inc','849',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mesoblast, Inc','3588',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'METAVivor Research and Support Inc','839',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Methodist Central Hospital','3754',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Methodist Hosp Physician Org OB/GYN','3672',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'METHODIST HOSPITAL','818',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'METHODIST HOSPITAL RESEARCH INSTITUTE','684',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'METHYLGENE INC','420',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MetroHealth Medical Center','3766',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Metronomx Therapeutics, LLC','5042',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MGI PHARMA INC','55',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MICHAEL AND SUSAN DELL FOUNDATION','582',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MICHIGAN STATE UNIV','624',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Microdermis Corporation','5173',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MICROMET INC','812',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MIKE HOGG FUND','583',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MILLENNIUM','107',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MILLIPEDE FOUNDATION','768',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MiRagen Therapeutics','249',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mirna Therapeutics','5066',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mission Health','3772',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MIYAGI CLINIC SENDAI MED IMAGING  INC','114',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MOGAM BIOTECHNOLOGY RESEARCH INSTITUTE','584',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Molecular Carninogenesis, NKI','3554',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Molecular Health AG','3647',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MOLECULAR INSIGHT PHARMACEUTICALS','421',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MOLECULIN','409',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MOLMED','272',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Monash University','5193',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Montefiore Medical Center - Moses Div','3642',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Montreal General Hospital','3745',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MOREHOUSE SCHOOL OF MED','653',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MorphoSys AG','3549',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MORPHOTEK','215',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Morris Cancer Center','4072',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Morton Plant Hosp-Lykes Cancer Ctr','4459',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mountain States Tumor Institute','3731',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Mountainside Hospital','4010',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MPN Research Foundation','3676',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MSHA-Johnson City Medical Center','4466',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MT SINAI MEDICAL CENTER','661',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MULTIPLE MYELOMA RESEARCH FOUNDATION','190',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MUSCULAR DYSTROPHY ASSOCIATION','789',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MUSCULOSKELETAL TRANSPLANT FOUNDATION','585',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MYELORX LLC','629',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MYREXIS INC','798',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'MYRIAD GENETICS INC','188',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NABI BIOPHARMACEUTICALS','375',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NanCogenics,Inc','3679',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nano3D Biosciences','896',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NanoBioMagnetics, Inc','605',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NanoCarrier Co.,Ltd.','5011',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NANOMATERIALS AND NANOFABRIC LABS','414',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NANOSPECTRA BIOSCIENCES INC','118',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL ALOPECIA AREATA FOUNDATION','329',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL BRAIN TUMOR FOUNDATION','423',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL BRAIN TUMOR SOCIETY','89',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Cancer Centre Singapore','3623',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL CANCER INSTITUTE','9',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL CENTER FOR RESEARCH RESOURCES','572',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL CHILDHOOD CANCER FOUNDATION','110',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL COMPREHENSIVE CANCER NETWORK','670',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL EYE INSTITUTE','10',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Foundation for Cancer Research','198',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL HEART, LUNG, AND BLOOD INST','11',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL HUMAN GENOME RESEARCH INST','12',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Institute of Diabetes','5100',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL INSTITUTE ON AGING','13',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL INSTITUTES OF HEALTH','8',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL KIDNEY FOUNDATION','496',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL LIBRARY OF MEDICINE','28',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Marrow Donor Program','564',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Office','5052',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL SAFETY ASSOCIATES','356',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL SCIENCE FOUNDATION','449',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATIONAL SOCIETY OF GENETIC COUNSELORS','755',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National Stem Cell Fndtn','5127',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'National University of Singapore','3526',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Natl Aeronautics and Space Admn NASA','3563',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL CNTR FOR COMPLMNTY ALTRN  MED','29',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL COMM ON CERTIFICATION OF PHYSICIAN','525',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Natl Committee for Quality AssuranceNCQA','5009',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Natl Ctr Advancing Translational Sci','5065',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL CTR CHRON DISEASE PREV HLTH PROMO','108',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL HISTORICAL PUBLN & RECORDS COMMSN','273',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST ALCOHOL ABUSE AND ALCOHOLISM','14',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST ALLERGY & INFECTIOUS DISEASES','15',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST ARTH & MUSC & SKIN DISEASES','16',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST BIOMED IMAG AND BIOENG','17',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST CHLD HEALTH HUMAN DEVELPMNT','18',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST DEAFNESS AND COMM DISORDERS','19',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST DENTAL CRANIOFACIAL RESEARCH','20',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST MINORITY HEALTH & DISPARITIES','671',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF DIAB & DGST & KID DISEASES','21',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF ENVIRON HLTH SCIENCES','23',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF GENERAL MEDICINE SCIENCES','24',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF MENTAL HEALTH','25',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF NEURO DISORDERS AND STROKE','26',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST OF NURSING RESEARCH','27',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL INST ON DRUG ABUSE','22',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Natl Lung Cancer Partnership','3631',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL MULTIPLE SCLEROSIS SOC','660',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NATL ORGANIZATION RARE DISORDERS (NORD)','763',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Naval Medical Center','3721',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nektar Therapeutics','3697',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NELIA AND AMADEO BARLETTA FOUNDATION','416',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nemours Children�s Clinic','5087',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NEOPROBE CORPORATION','424',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NEREUS PHARMACEUTICAL INC','410',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nerium Biotechnology','252',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NERVIANO MEDICAL SCIENCES','274',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New Chapter Inc','606',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New Hanover Radiation Oncology Center','3796',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New York Community Trust','5179',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New York Medical College','906',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New York Methodist Hospital','3832',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New York University','5040',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'New York University Medical Center','3708',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NewLink Genetics Corporation','5211',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nexus Oncology Ltd','3529',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NIH-DIVISION OF INTRAMURAL RESEARCH','890',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NITTOBO MEDICAL CO LTD','415',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Non-Profit Org Adv Clin Res Org NPO-ACRO','3589',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nordion Incorporated','3560',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Norris Cancer Center','3802',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Norristown Regional Cancer Center','4458',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North American NeuroEndocrine Tumor Soc','586',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North Colorado Medical Center Cancer Ins','3808',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North Mississippi Medical Center','3822',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North Shore University Hospital','3761',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North Star Lodge Cancer Center','3806',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'North Wilmington Silverside','3978',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northeast Alabama Regional Medical Ctr','4469',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northeast Georgia Medical Center','4463',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northeast Ohio Medical University','5177',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northeast Radiation Oncology Ctr @ Mercy','4071',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northern California Inst for Res and Edu','5004',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NORTHERN ILLINOIS UNIVERSITY','573',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NorthShore University Medical System','4055',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Northwest Biotherapeutics','5010',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NORTHWESTERN UNIVERSITY','697',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Novartis','2',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Novel Anti-Infective Technologies, LLC','5195',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Novella Clinical','3604',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Novocure','5206',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NOXXON Pharma AG','559',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NPM PHARMA INC','723',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NRG Oncology Foundation, Inc','5231',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NS PHARMA INC','587',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NSA, LLC','5142',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NSABP','85',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Nuvo Research AG','3684',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'NuVue Therapeutics','5529',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OBI Pharma, Inc','3645',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ochsner Clinic Foundation Hospital','3778',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ockham','5112',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oconomowoc Memorial Hospital','3997',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ODYSSEY HEALTHCARE','411',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Office of the Director','862',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ohio Medical School','4077',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ohio State University','3572',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OKLAHOMA MEDICAL RESEARCH FNDTN','720',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Olympus Medical Systems, Corporation','3574',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Omega Optics Inc','3538',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oncoceutics, Inc','5164',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCOGENEX TECHNOLOGIES INC','275',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCOLIN THERAPEUTICS','471',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCOLIX INC','588',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCOLOGY NURSING SOC','214',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oncology PCU Clinical Operations','3627',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oncology/Hematology Care, Inc.','4073',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OncoMed Pharmaceuticals, Inc.','3547',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCONOVA THERAPEUTICS  INC','495',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OncoPep, Inc.','850',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OncoPlex Diagnostics','5126',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONCOTHYREON INC','362',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ONLINE COLLABORATIVE ONC GROUP','608',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Onyx Pharmaceuticals','193',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OPSONA THERAPEUTICS LTD','821',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Optimer Biotechnologies, INC','5161',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Optimer Pharmaceuticals, Inc','886',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OREGON HEALTH SCI UNIVERSITY','374',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ORTHO BIOTECH','695',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Osiris Therapeutics','253',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oswego County Radiation Oncology','4018',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OTSUKA PHARMACEUTICAL','699',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ottumwa Regional Health Center','4470',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Outcome Science, Inc.','5116',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Outreach Strategists, LLC','861',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Outrun the Sun, Inc.','3508',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OVARIAN CANCER RESEARCH FUND','693',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Oxford Immunotec Ltd.','5082',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'OXiGENE INC','222',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ozmosis Research, Inc.','5003',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PancreatRX, LLC','5125',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PAREXEL INTERNATIONAL LLC','716',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Partners HealthCare','5005',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Paterson Inst  for Cancer Research','3568',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Patient-Centered Outcomes Res Inst PCORI','5115',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PDL BioPHARMA INC','418',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Peace Health Southwest Medical Center','3767',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PEDIATRIC CANCER RESEARCH FOUNDATION','589',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PENN STATE UNIV','802',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Penrose Hospital','3706',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PEREGRINE PHARMACEUTICALS INC','439',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Pfizer Inc','39',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMACEUTICAL RESEARCH ASSOC INC','412',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMACIA','447',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMACYCLICS INC','179',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PharmaJet','894',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMAMAR USA, INC.','474',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMANET INC','440',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PharmaReview Corporation','5124',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHARMATECH INC','276',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Phelps County Regional Medical Ctr.','4474',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHI BETA PSI SORORITY','491',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHILIPS HEALTHCARE','708',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHILIPS INTELLECTUAL PROPERTY AND STNDS','574',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHILIPS RADIATION ONCOLOGY SYS','590',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHOENIX BIOTECHNOLOGY INC','458',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Phosplatin Therapeutics','5165',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PHOTOCURE ASA','601',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PhRMA Foundation','5067',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Piedmont Hospital','3703',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Pikeville Medical Center','4012',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Pink Ribbons Project','903',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PIQUR Therapeutics AG','5217',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Piramal Enterprises Limited','3590',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PKD FNDTN','793',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PLANTACOR INC','417',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PLASTIC SURGERY EDUCATIONAL FOUNDATION','277',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PLEXXIKON INC.','470',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Pluristem Therapeutics Ltd','3587',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PLx Pharm Inc.','5189',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'POLARIS HEALTH DIRECTIONS INC','379',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'POLARIS PHARMACEUTICALS INC','778',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'POLYMED THERAPEUTICS INC','872',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Population Health Research Institute','3578',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PORTUGUESE FNDTN FOR SCIENCE & TECH','216',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PPD DEVELOPMENT','441',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PRA INTERNATIONAL','840',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Presbyterian Hospital','3768',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Presbyterian/ St. Luke''s Medical Center','3779',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PREVENT CANCER FOUNDATION (PCF)','827',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PRIMERADX INC','442',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Princess Margaret Cancer Centre','5147',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Princess Margaret Hospital','4706',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PRISM','854',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PRO NEURON BIOTECHNOLOGIES INC','526',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROACTA INC','425',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROCTER AND GAMBLE CO','278',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ProCure Proton Therapy Center','4029',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Progenics Pharmaceuticals, Inc','5208',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Prologue Research International, Inc.','558',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ProMedica Flower Hospital','3763',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Promedior, Inc.','3548',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROMETHEUS THERAPEUTICS INC','592',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROSTATE CANCER FOUNDATION','47',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROSTRAKAN GROUP PLC','279',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Proteolix Inc.','615',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROTEONOMIX INC','835',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROTOX THERAPEUTICS INC','178',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PROVECTUS PHARMACEUTICALS INC','493',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PULMOTECT INC','160',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Puma Biotechnology, Inc','5032',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'PURDUE UNIVERSITY','363',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'QUADW','593',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'QUEENS UNIV','626',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'QUINTESSENCE BIOSCIENCES INC','373',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'QUINTILES INC','801',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RADIADYNE','229',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RADIANT CREATIVE GROUP','419',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Radiant Research, Inc','3649',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RadiantCare Radiation Oncology','3702',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Radiation Oncology Associates','4455',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RADIATION ONCOLOGY DEPT','691',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Radiation Therapy Oncology Group (RTOG)','5204',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RADIOLOGICAL SOCIETY OF NORTH AMER','703',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RADIOMEDIX, INC.','465',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rally Foundation','3500',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RAS Radiation Oncology Center-Sacramento','3734',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RAYMOND BEVERLY SACKLER FUND ARTS SCI','594',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Real Imaging Ltd.','5063',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Reata Discovery Inc.','211',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'REATA PHARMACEUTICALS INC','113',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Regensburg Germany','5162',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RESEARCH FNDTN for SUNY','705',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Research Fndtn for SUNY @ Stony Brook','5013',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Resverlogix Corp','3606',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RETINA RESEARCH FOUNDATION','484',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rex Health Care, Inc','3773',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RGK FNDTN','429',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rheumatology Research Foundation','5123',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rhode Island Hospital','4057',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RICE UNIVERSITY','71',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RIGEL PHARML INC','280',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RITA MEDICAL SYSTEMS INC','380',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RIVIERA COUNTRY CLUB AND SPORTS CTR','636',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROBERT and KAY ONSTEAD FNDTN','754',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROBERT DUNCAN FOUNDATION','652',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROBERT WOOD JOHNSON FNDTN','595',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROCHE','92',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rochester General Hospital','3723',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rocky Mountain Cancer Center-Thornton','4063',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROLL INTERNATIONAL','485',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Roper Hospital','3774',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ROSETTA GENOMICS LTD','372',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Roswell Park Cancer Institute','747',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Royal Adelaide Hospital','3643',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RSNA RSRCH AND EDUC FNDTN','809',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RTI INTERNATIONAL','256',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rush-Copley Medical Center','3677',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rutgers Univ','5089',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Rutland Regional Medical Center','3704',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'RUZIC RESEARCH FOUNDATION','486',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'S*BIO PTE LTD','56',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SAIC-FREDERICK, INC.','90',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Saint Frances Hospital','4060',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Saint Louis University','5094',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Saint Nicholas Hospital','4022',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SALIENT PHARMACEUTICALS','426',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Samsung Advanced Institute of Technology','860',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Samsung Medical Center','3624',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sands Cancer Center','4019',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sanford Health','3820',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sanofi S.A.','688',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Santech Inc','3518',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SARAH CANNON RESEARCH INST','825',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SARANNE AND J LIVINGSTON KOSBERG','576',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SARCOMA ALLNCE FOR RSCH THROUGH COLABRTN','694',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SARCOMA FOUNDATION OF AMERICA','281',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Saskatoon Cancer Centre','3792',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Schering-Plough','119',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SCOTT AND WHITE MEMORIAL HOSP','617',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SCRI Services','3680',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Scripps Research Inst','5018',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Seattle Biomedical Research Inst','5105',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Seattle Cancer Ctr Alliance @ Northwest','3999',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SEATTLE GENETICS INC','64',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SENEX BIOTECHNOLOGY, INC','711',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Seno Medical Instruments, Inc','5070',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sentara Norfolk General Hospital','3771',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SEOUL NATIONAL UNIVERSITY','871',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Seoul National University Hosp','5036',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SHAPE PHARMACEUTICALS INC','443',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sharp Memorial Hospital','4464',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Shell Oil Company','3655',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SHIRE PHARMACEUTICALS DEV INC','811',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SICEL TECHNOLOGIES','257',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sidney Kimmel Comprehensive Cancer Ctr','3711',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SIEMENS','304',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SIGNPATH PHARMA INC','427',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SIGNUM BIOSCIENCES INC','596',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SILICON VALLEY COMMUNITY FOUNDATION','487',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sinai - Grace Hospital','3985',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SIR FOUNDATION','282',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sirenas Marine Discovery  LLC','3691',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sirtex Technology Pty. Ltd.','3600',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SKIP VIRAGH FOUNDATION','647',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sky Ridge Cancer Center','4062',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Slidell Radiation Center','4454',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SMART PERSONALIZED MEDICINE','283',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SMDC Medical Center','3834',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SOCIAL AND SCIENTIFIC SYS INC','810',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Society for Immunotherapy of Cancer','5191',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Society of Interventional Radiology','5218',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SOCIETY OF SURGICAL ONCOLOGY','303',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SOLIGENIX, INC.','328',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SOLVAY PHARMACEUTICALS INC','191',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Solving Kids'' Cancer','901',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SonaCare Medica, LLC','3678',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sookmyung Womens University','3653',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Soricimed Biopharma Inc.','3584',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'South Florida Radiation Oncology','3582',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'South Nassau Communities Hospital-Oceans','3790',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Southeast Missouri Hospital','3628',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Southern Research Institute','3665',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Southwest Cancer Center','3987',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SOUTHWEST ONCOLOGY GROUP','460',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SPARTA PHARMACEUTICALS INC','656',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Spartanburg Regional-Gibbs Cancer Center','3812',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SPECTRUM PHARMACEUTICALS INC','562',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SRI International','5224',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Agnes Hospital Cancer Center','4452',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Anthony''s Hospital','3630',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Anthony''s Medical Center','4028',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ST BALDRICKS FNDTN','437',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Elizabeth Healthcare - Edgewood','4457',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Francis Health Center','3833',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Francis Hospital','3765',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Francis Hospital - San Francisco','3829',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Francis Hospital and Medical Center','3805',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St John Macomb Hospital','4066',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St John Medical Center','3800',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St John''s Cancer Center','3836',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Joseph Hospital','3699',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Joseph''s Hospital','3744',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ST JUDE CHILDRENS RESEARCH HOSPITAL','284',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Luke''s Cancer Institute','3837',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Luke''s Hospital-Chesterfield','3788',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Lukes Mountain States Tumor Inst-Namp','4069',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Mary Medical Center','3629',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Mary of Nazareth Hospital','3807',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Mary''s Medical Center','4008',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Vincent Hospital','3990',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Vincent Hospital- IN','3777',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St Vincent''s Medical Center','3659',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Elizabeth Medical Ctr','3528',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Joseph''s Regional Medical Ctr','3651',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Jude Medical Center','4480',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Louis University Hospital','3759',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Luke''s Episcopal Health System','3605',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'St. Vincent Hospital - MA','3794',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STANDARD IMAGING INC','370',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STANFORD AND JOAN ALEXANDER FNDTN','161',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Stanford University','206',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STCube Inc.','5077',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STELLAR MICRO DEVICES','364',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STEMLINE THERAPEUTICS INC','382',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Strang Cancer Prevention Center','630',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'STRUCTURAL GENOMIX INC','466',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUMITOMO HEAVY INDUSTRIES LTD','631',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Summa Health System at Lake Medina','3810',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUN NUCLEAR CORPORATION','578',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sun YatSen Univ','3601',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sun Yat-Sen University Cancer Ctr','5203',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNBEAM FNDTN','579',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNESIS PHARMACEUTICALS INC','696',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNOVION PHARMACEUTICALS INC','833',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNWIN BIOPHARMA(XIAMEN) CO. LTD','885',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNY at Stony Brook','3660',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNY Downstate Medical Center','3709',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SUNY Upstate Medical University','3749',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Susan B. Allen Cancer Center','4070',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Susan G. Komen for the Cure','43',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Sutter East Bay Hosp','3598',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Swedish Cancer Institute - First Hill','3742',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Swiss Inst for Experimental Cancer Res','5198',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Symphogen A/S','5079',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SYNDAX PHARMACEUTICALS INC','232',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Synta Pharmaceuticals','5106',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'SYSMEX','361',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'T.R.U.E.RESEARCH FOUNDATION','464',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'T2 BIOSYSTEMS INC','882',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tacoma/St Joseph ROC','3980',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tacoma/Valley ROC','3981',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TAIHO PHARMACEUTICAL CO LTD','169',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TAIWAN HOPAX CHEM MFG CO LTD','210',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TAIWAN INST FOR NUCLEAR ENERGY RESEARCH','672',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TALON THERAPEUTICS INC','719',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tammy Walker Cancer Center','4074',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TAP PHARMACEUTICAL PRODUCTS','667',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Targazyme','5146',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TARGET DISCOVERY INC','368',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TELEFAX MEDICAL','776',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Telik, Inc.','184',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Temple University','254',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Temple University Hospital','4702',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TENNESSEE TECHNOLOGICAL UNIVERSITY','477',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Terumo BCT, Inc','3690',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TETRALOGIC PHARMACEUTICALS','645',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Teva Pharmaceuticals','858',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEX FED OF BUSINESS & PROFESSIONAL WOMEN','489',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS A & M RSCH FOUNDATION','479',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS A&M HEALTH SCIENCE CENTER','709',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas A&M Inst for Genomic Medicine','3625',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS A&M UNIVERSITY SYSTEM','813',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS BIOMEDICAL RESEARCH','870',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS CANCER COUNCIL','500',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Children''s Hospital','3523',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS ENGINEERING EXPERIMENT STATION','480',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Gulf Foundation dba (TIBER)','5027',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Heart Institute','5034',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS HIGHER EDUCATION COORDINATING BRD','796',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Molecular','5111',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS NEUROFIBROMATOSIS FOUNDATION','381',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Oncology - Round Rock','3674',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Oncology-Dallas Presbyterian Hospi','3789',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Texas Society of Allied Hlth Professions','4078',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXAS SOUTHERN UNIV','432',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TEXRAD LIMITED','728',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TG Therapeutics, Inc','5055',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE BROAD INSTITUTE','783',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Cure Starts Now','5056',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE DAISY FOUNDATION','781',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Donald W. Reynolds Fndtn','3564',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE GABRIEL INST','679',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE GERBER FOUNDATION','519',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Greenwall Foundation','5083',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Harold Leever Regional Cancer Center','3633',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Hospital of Central Connecticut','3780',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Hospital of Fox Chase Cancer Center','3524',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Netherlands Cancer Institute','3662',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE PITTSBURGH FOUNDATION','376',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'The Regional Cancer Center','4006',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE SLANE CO','428',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THE SYNAPSE GROUP INC','285',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Theorem Clinical Research','5051',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THERAKOS INC','511',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THERAVANCE INC','722',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THIRD NERVE LLC','657',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'THRESHOLD PHARMACEUTICALS INC','512',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TIBOTEC THERAPEUTICS','765',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TM Chemicals','5031',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TomoWave Laboratories, Inc.','3580',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TOPOTARGET A/S','649',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TORQUIN THERAPEUTICS','371',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TOSOH BIOSCIENCE INC','365',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TRACON PHARMACEUTICALS INC','759',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TRAGARA PHARMACEUTICALS INC','513',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TRANSGENOMIC INC','815',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Transgenomic Inc','830',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Translational Genomics Research Inst','613',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Translational Molecular Pathology','5061',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Translational Research in Oncology','3667',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Transpire, Inc.','644',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TRIAD BURDEN OF ILLNESS INC','116',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Trinitas Comprehensive Cancer Center','3825',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TRIOLOGICAL SOC','433',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TROVAGENE INC','721',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tx Business and Pro Women''s Fndtn','3685',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tx Emerging Tech Fund','5202',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'TX STATE LIBRARY AND ARCHIVES COMMSN','286',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tx State Univ San Marcos','5236',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Tx Tech Univ Health Sci Ctr - El Paso','880',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'U3 Pharma GmbH','5058',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UC Davis Medical Ctr','5019',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UCB PHARMA','816',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UCLA JONSSON COMPREHENSIVE CANCER CTR','659',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UCSD SUBCONTRACTOR','542',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UCSF-Mt Zion Comprehensive Cancer Center','4058',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Un of Tx Southwestern Med Ctr - Dallas','31',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Union Hospital Cancer Center','3979',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'United Biosource Corp','5185',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNITED NEGRO COLLEGE FUND','622',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'United Soybean  Board','3654',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNITED THERAPEUTICS CORP','287',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNITING AGAINST LUNG CANCER','288',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Unity Point Health Trinity','3813',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ  of Bergen','5101',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ Hosp Chagrin Highlands Hlth Ctr','3652',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV NC - CHAPEL HILL','289',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF ALABAMA BIRMINGHAM','546',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF ARIZONA','461',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of California at Los Angeles','4704',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF CALIFORNIA Irvine','853',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF CALIFORNIA SAN FRANCISCO','305',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF CALIFORNIA SANTA BARBARA','879',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Cambridge','5021',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF CHICAGO','434',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Chicago Medical Center','3707',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF COLORADO','682',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Florida','905',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Florida Proton Therapy Inst 730','3989',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF HOUSTON','683',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Illinois Medical Center','3719',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Innsbruck','5178',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Kentucky Medical Center','4054',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Louisville School of Medicine','3717',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF MARYLAND - BALTIMORE','162',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Massachusetts','3569',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF MASSACHUSETTS MEDICAL SCHOOL','497',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Memphis','5122',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF MINNESOTA','435',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Mississippi-Medical Mall','3735',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF NEBRASKA','718',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF OKLAHOMA HEALTH SCIENCE CTR','547',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF PITTSBURGH MEDICAL CTR','637',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of South Florida','3599',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF SOUTHERN CALIFORNIA','150',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Sydney RNS Kolling Inst','5232',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Texas at San Antonio','3576',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV OF TEXAS HEALTH SCIENCE CTR TYLER','771',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Univ of Toronto','5163',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV TENNESSEE-HEALTH SCIENCE CENTER','290',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIV TX MED BRANCH - GALV','291',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Universitatsklinikum Essen','3591',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Universit� Laval - Ferdinand-Vandry','5048',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University Hospitals Case Medical Center','4707',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY MEDICAL CENTER GOTTINGEN','828',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY MEDICAL CENTRE GRONINGEN','481',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Alabama Medical Center','3730',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Buenos Aires School of Med','5226',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of California Davis','561',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF CALIFORNIA RIVERSIDE','673',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF CALIFORNIA SAN DIEGO','674',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF CINCINNATI','666',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Florida Medical Center','3752',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF GEORGIA','367',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF GEORGIA','869',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Illinois at Chicago','5001',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF IOWA','675',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Kansas Medical Center','3737',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Kentucky Research Fndtn','876',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Louisville Res Foundation','643',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Maryland Medical Center','3637',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF MEDICINE AND DENT OF NJ','498',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF MIAMI','112',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF MICHIGAN','77',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Michigan Medical Center','3724',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Minnesota Med Ctr Fairview','3714',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Missouri','5187',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Nebraska Medical Center','3726',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF NEW MEXICO','552',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of North Carolina Hospitals','3751',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of North Texas Hlth Sci Cntr','5227',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Northern Colorado','5002',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Oslo','900',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF PENNSYLVANIA','677',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF PITTSBURGH','235',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Pittsburgh Medical Ctr','3720',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF PUERTO RICO','805',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Rochester','228',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Sao Paulo','3733',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of South Carolina','5000',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Tennessee Medical Center','3764',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Texas at Dallas (UTD)','3565',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF TEXAS AUSTIN','88',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF TEXAS SYSTEM','665',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Toledo College of Medicine','3621',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Utah','889',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Utah Huntsman Cancer Hosp','3729',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF VERMONT','366',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF VIRGINIA','482',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Washington','3586',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNIVERSITY OF WISCONSIN MADISON','676',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'University of Wisconsin Med Center','3713',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UNL Nutrition and Health Sci','5237',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UPMC Beacon Hospital','3626',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UPMC-Heritage Valley Health CC Beaver','3641',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Urology Care Foundation','5099',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US AGENCY FOR INTERNATIONAL DEVELOPMENT','714',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US ARMY INSTITUTE OF SURG RSCH','33',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US BIOTEST INC','378',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US CANCER PAIN RELIEF COMMITTEE INC','658',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Def Advanced Res Proj Agency (DARPA)','34',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Department of Defense','32',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Department of the Army','35',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Department Veterans Affairs','3577',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US DEPT OF ENERGY','799',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Environmental Protection Agency','5081',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US FOOD AND DRUG ADMINISTRATION','225',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US Oncology','4067',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'USA Medical Research Acq Activity','36',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'USAF MEDICAL CENTER, KEESLER','37',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'US-Israel Binational  Sci Fndtn (BSF)','3552',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'USON-Onc Assoc of OR-Williamette Valley','3781',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'USON-Tex Oncology-Medical City Dallas','3760',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UT HEALTH SCIENCE CTR HOUSTON','48',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UT HEALTH SCIENCE CTR SAN ANTONIO','638',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UT Sch of Public Health-Dallas Reg Campu','5235',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UT SYSTEM PATIENT SAFETY COMMITTEE','642',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC','729',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Breast Cancer Research Program','730',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Development Office Gifts','732',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Goodwin Fund','733',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC IRG Basic Research','734',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC IRG Program','877',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Kleberg Fund','735',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Leukemia SPORE Development Award','737',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC LONGWELL FAMILY FOUNDATION','292',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Melanoma Spore Res Dev Award','5154',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Other Internal Funding','740',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Prostate Cancer Research Program','741',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC Topfer Fund','743',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC TX FED BUS & PROF WOMEN','293',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'UTMDACC University Cancer Foundation UCF','744',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'V FOUNDATION','294',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vaccine and Gene Therapy Inst of Florida','5234',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vall d�Hebron Institut d�Oncologia VHIO','3517',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Valley Anesthesiology Foundation','5148',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Valley Health System','5120',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Van Andel Research Institute','5096',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Van Elslander Cancer Center','3977',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vanderbilt University','5138',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VANDERBILT UNIVERSITY MEDICAL CENTER','472',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vanquish Oncology Inc.','851',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VARIAN MEDICAL SYSTEMS INC','448',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VCU Massey Cancer Center','3638',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VEEDA ONCOLOGY','377',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vegenics Proprietary Limited','641',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VENNER CAPITAL SA','483',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VentiRx Pharmaceuticals Inc.','3681',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Verastem','5157',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VERIDEX LLC','640',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VERIMED RESEARCH CORP.','577',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Veristat, LLC','5155',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VERNALIS R&D LIMITED','568',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vertex Pharmaceuticals','3675',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VERTO INSTITUTE','814',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vestan, Inc.','5151',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Veterans Medical Research Foundation','5212',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Via Christi Cancer Center','3818',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VIACELL INC','668',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Viamet Pharmaceuticals, Inc.','3593',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VICAL INC','295',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vicus Therapeutics','3557',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vietnam Educ Fndtn VEF','3686',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VION PHARMACEUTICALS INC','502',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VioQuest','611',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VIRGINIA COMMONWEALTH UNIV','571',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Virginia Mason Medical Center','4708',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VIRGINIA POLYTECHNIC INST AND STATE UNIV','180',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Virtua','3823',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Vision RT Ltd','3562',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VITOL CHARITABLE FNDTN','777',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'VUCOMP INC','478',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'W M KECK FNDTN','438',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Wake Forest Univ. Baptist Med. Ctr.','4052',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Washington Adventist Hospital','3801',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Washington State University','5186',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WASHINGTON UNIVERSITY IN ST LOUIS','575',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Wayne Radiation Oncology Center','3786',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WAYNE STATE UNIVERSITY','390',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Weill Cornell Med College','5059',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Weisberg Cancer Center','3984',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WELCH FOUNDATION','389',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Wellcome Trust Sanger Inst','3542',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WellStar Health System','3700',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Wesley Medical Center','4030',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Westchester Medical Center','3784',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Wheeling Hospital','4005',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WILEX AG','769',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'William Beaumont Hospital','3640',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'William Lawrence & Blanche Hughes Fndtn','689',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Windsor Regional Hospital','3762',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WINDY HILL MEDICAL INC','236',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Winthrop University Hospital','3827',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Women''s Cancer and Surgical Care, P.C.','3669',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'World Molecular Imaging Society','3531',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'WYETH AYERST RESEARCH LAB','678',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'XBIOTECH USA','430',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Xcovery Holding Company','3656',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Xencor','239',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'XENEX HEALTHCARE SERVICES LLC','639',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'YALE UNIVERSITY','499',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'YAUPON THERAPEUTICS','391',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'YM BIOSCIENCES INC','681',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Yonsei Univ College of Med','5020',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Z TECH INC','436',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Zalicus, Inc','5039',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ZARROW FOUNDATION','580',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Zhejian University','863',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ZYMOGENETICS','884',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'.CLS Therapeutics Limited','902',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'20/20 Gene Systems','3595',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'21st Century Oncology, Lakes Park','3992',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'21st Century Oncology,Cape Coral','3993',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'21st Century Oncology-NB1 Office','3974',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'72 Hammersmith Road','5215',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AA ChemBio LLC','5137',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AAVP Biosciences','5007',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AAWR RESEARCH AND EDUCATION FOUNDATION','76',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AB Science','554',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Abbott Laboratories','60',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AbbVie','3533',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ABIOGEN PHARMA S P A','297',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ABRAXIS BIOSCIENCE LLC','685',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Academic and Community Cancer Res United','5041',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Academy of Nutrition and Dietetics Fndtn','3632',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Accademia delle Scienze di Medicina Pall','5209',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACCELERATE BRAIN CANCER CURE','72',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Acceleron Pharma Inc.','5003',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACCESS PHARMACEUTICALS INC','50',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AccuGenomics, Inc.','5086',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACCURAY INC','120',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Acerta Pharma, LLC','5076',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACETYLON PHARMACEUTICALS INC','758',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACORN RESEARCH LLC','725',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACT BIOTECH INC','121',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACT ONCOLOGY LLC','717',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Actelion Clinical Research, Inc.','3670',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACTELION PHARMACEUTICALS LTD','459',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Actinium Pharmaceuticals','844',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACTION TO CURE KIDNEY CANCER (ACKC)','782',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ACTIVE BIOTECH RESEARCH AB','306',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Adaptive Biotechnologies Corp.','5037',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ADHEREX TECHNOLOGIES INC','152',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Adient Medical Inc.','895',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ADMA BIOLOGICS INC','61',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ADNEXUS','298',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Advanced Accelerator Applications SA','3692',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ADVANCED RADIATION THERAPY LLC','726',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AEGERA THERAPEUTICS INC','69',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AFFIMED THERAPEUTICS AG','692',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Agency for Healthcare Research & Qual','557',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AGENDIA','299',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Agendia Inc.','5196',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Agenus','5134',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Agios Pharmaceuticals, Inc.','5213',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AGNES SCOTT COLLEGE','66',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AGOURON PHARMACEUTICALS','317',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ahn-Gook Pharmaceutical Co., Ltd','5103',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AiCuris GmbH & Co. KG','237',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Air Force Research Laboratory','3521',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AKC CANINE HEALTH FOUNDATION','663',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Akron General Medical Center','3798',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Albany College of Pharmacy & Health Sci','5029',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Albert Einstein College of Medicine','628',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Albert Einstein Medical Center','4053',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alegent Health - Immanuel Med Ctr','3602',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALERE INC','94',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alere Wellbeing Inc','5220',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALEXION PHARMACEUTICALS  INC','226',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALEX''S LEMONADE STAND FOUNDATION','223',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALFRED MANN INST BIOMED DEV PURDUE UNIV','300',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alice Kleberg Reynolds Foundation','878',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Allan Blair Memorial Clinic','3791',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Allegheny General Hospital','3705',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alle-Kiski Medical Center','4456',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALLIANCE  FOR CANCER GENE THERAPY','57',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALLIANCE FOR CLINICAL TRIALS IN ONCOLOGY','832',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alliance for Clinical Trials Onc Fndtn','5008',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALLIANCE OF CARDIOVASCULAR RESEARCHERS','307',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALLOS THERAPEUTICS INC','91',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALNYLAM PHARMACEUTICALS INC','122',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Alpha Oncology, Inc.','3635',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ALTOR BIOSCIENCE CORPORATION','308',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AM BIOTECHNOLOGIES LLC','804',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMAG pharmaceutical INC','309',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amarillo Pharmaceuticals, LLC','5233',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMBIT BIOSCIENCES CORPORATION','492',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER ASSN FOR LAB ANIMALS SCI','200',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Assoc for Study of Liver Disease','3545',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Assoc for the Advancement of Sci','3663',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER ASSOCIATION FOR CANCER RESEARCH','704',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer College of Emergency Physicians','3510',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER COLLEGE OF PROSTHODONTISTS EDU FND','227',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Institute of Ultrasound in Medicine','845',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Liver Foundation','3544',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER SOC BLOOD MARROW TRANSPLANTATION','310',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Soc of Breast Surgeons Fndtn','3657',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER SOC OF CYTOPATHOLOGY FNDTN','201',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amer Soc of Regional Anesthesia Pain Med','5131',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER SOC THERAPEUTIC RADIOLOGY ONCOLOGY','86',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER SOCIETY FOR BONE MINERAL RESEARCH','707',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMER UNIV OF BEIRUT','727',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN ACADEMY OF DERMATOLOGY','115',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN ACADEMY OF OTOLARYNGOLOGY','79',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Assoc of Neurological Surgeons','3534',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN ASSOC OF PHYSICISTS IN MEDICINE','452',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Association of Anatomist','893',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Association of Immunologists','5230',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN ASTHMA FOUNDATION','311',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN BIOSCIENCE INC','242',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN BRAIN TUMOR ASSOCIATION','84',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN CANCER SOCIETY, NATIONAL','49',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Coll of Laboratory Animal Med','690',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN COLLEGE OF RADIOLOGY','53',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American College of Surgeons','598',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN GASTROENTEROLOGICAL ASSOCIATION','312',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN GERIATRICS SOCIETY','58',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Heart Association','106',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN INSTITUTE FOR CANCER RESEARCH','62',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Italian Cancer Foundation','5225',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN LUNG ASSOC','345',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Nursing Foundation','5194',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Pain Society','3571',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Skin Association','123',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN SOC FOR RADIATION ONC (ASTRO)','195',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'American Soc of Colon and Rectal Surgeon','5078',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN SOCIETY OF CLINCAL ONCOLOGY','70',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN SOCIETY OF HEALTH SYSTEM PHAR','313',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN SOCIETY OF HEMATOLOGY','196',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN STEM CELL, INC.','245',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMERICAN THYROID ASSOC','395',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMGEN INC','45',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amgen, formerly Biovex Inc.','453',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMI - ACADEMY OF MOLECULAR IMAGING','230',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMINO UP CHEMICAL CO LTD','314',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AMPLIMED CORPORATION','124',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amplimmune, Inc.','5118',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Amschwand Sarcoma Cancer Foundation','396',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANESTHESIA PATIENT SAFETY FNDTN (APSF)','820',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANGIMMUNE LLC','553',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Angioblast Systems Inc','96',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Angiochem Inc.','5192',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANGIODYNAMICS','63',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Angiosarcoma Awareness, Inc.','5043',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANHUI JINCHAN BIOCHEMISTRY CO','397',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ann & Robert H. Lurie Children�s Hosp','3648',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANORMED INC','125',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Ansun BioPharma, Inc.','5085',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANTIGENICS LLC','93',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ANZAI MEDICAL CO LTD','315',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AOI PHARMACEUTICALS','199',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'AOSPINE','97',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'APERIO TECHNOLOGIES INC','202',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'APLASTIC ANEMIA & MDS INTL FOUNDATION','316',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'APOCELL INC','247',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Aptium Myeloma Consortium','625',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Aptiv Solutions, Inc','5110',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARCHER BIOSCIENCES INC','302',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Archimage, Inc','5190',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARCHIMEDES DEVELOPMENT LIMITED INC','234',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Argos Therapeutics, Inc.','3664',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARIAD PHARMACEUTICALS INC','65',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arizona Onc Services','4003',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arizona Oncology Services','3607',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arkival Technology Corporation','3507',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARMO BioSciences','5219',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARNO THERAPEUTICS INC','398',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arnold P. Gold Foundation Research Inst','5015',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Arog Pharmaceuticals, LLC','852',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'Aroostook Medical Center','4468',null,null,null,null,null,null,null,null,null,null,null,null,null);
Insert into ER_LKPDATA (PK_LKPDATA,FK_LKPLIB,CUSTOM001,CUSTOM002,CUSTOM003,CUSTOM004,CUSTOM005,CUSTOM006,CUSTOM007,CUSTOM008,CUSTOM009,CUSTOM010,CUSTOM011,CUSTOM012,CUSTOM013,CUSTOM014,CUSTOM015) values (SEQ_ER_LKPDATA.nextval,v_LkpId,'ARRAY BIOPHARMA INC','82',null,null,null,null,null,null,null,null,null,null,null,null,null);

dbms_output.put_line('Lookup data for Sponsors are inserted');

END;
/

--05

update ER_CODELST set CODELST_DESC='CRC Appealed' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAppealed';
update ER_CODELST set CODELST_DESC='CRC Approved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcApproved';
update ER_CODELST set CODELST_DESC='CRC Approved With Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvContngs';
update ER_CODELST set CODELST_DESC='CRC Approved With Major Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvContgncs';
update ER_CODELST set CODELST_DESC='CRC Approved With Minor Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvMinorCon';
update ER_CODELST set CODELST_DESC='CRC Deferred' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcDeferred';
update ER_CODELST set CODELST_DESC='CRC Disapproved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcDisapproved';
update ER_CODELST set CODELST_DESC='CRC In Review' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcInRev';
update ER_CODELST set CODELST_DESC='CRC Modifications Required' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcReturn';
update ER_CODELST set CODELST_DESC='CRC Rejected' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcRejected';
update ER_CODELST set CODELST_DESC='CRC Sent To Reviewers' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcSentReviewer';
update ER_CODELST set CODELST_DESC='CRC Temporary Hold' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcTempHold';
update ER_CODELST set CODELST_DESC='CRC/PBHSRC Rejected' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcPbhsrcRejctd';
update ER_CODELST set CODELST_DESC='HRPP Resubmitted' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcResubmitted';
update ER_CODELST set CODELST_DESC='HRPP Submitted' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcSubmitted';
update ER_CODELST set CODELST_DESC='Medical or Ancillary Review Completed' where CODELST_TYPE='studystat' and CODELST_SUBTYP='medAncRevCmp';
update ER_CODELST set CODELST_DESC='Medical or Ancillary Review Pending' where CODELST_TYPE='studystat' and CODELST_SUBTYP='medAncRevPend';

update ER_CODELST set CODELST_DESC='IRB: Application Work Started' where  CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_work_start';
update ER_CODELST set CODELST_DESC='IRB: Approved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='app_CHR';
update ER_CODELST set CODELST_DESC='IRB: Deferred' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_defer';
update ER_CODELST set CODELST_DESC='IRB: Disapproved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='rej_CHR';
update ER_CODELST set CODELST_DESC='IRB: Human Research, Not Engaged' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_human';
update ER_CODELST set CODELST_DESC='IRB: In Review' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_in_rev';
update ER_CODELST set CODELST_DESC='IRB: Initial Submission' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_init_subm';
update ER_CODELST set CODELST_DESC='IRB: Modifications Required' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_add_info';
update ER_CODELST set CODELST_DESC='IRB: Not Human Research' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_not_human';
update ER_CODELST set CODELST_DESC='IRB: PI Response Sent to Reviewers' where CODELST_TYPE='studystat' and CODELST_SUBTYP='pi_resp_rev';
update ER_CODELST set CODELST_DESC='IRB: Submission - Amendment' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_ammend';
update ER_CODELST set CODELST_DESC='IRB: Submission - Renewal' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_renew';
update ER_CODELST set CODELST_DESC='IRB: Temporary Hold' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_temp_hold';
update ER_CODELST set CODELST_DESC='IRB: Terminated' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_terminated';
update ER_CODELST set CODELST_DESC='IRB: Withdrawn' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_withdrawn';

commit;

--06

update er_codelst set codelst_custom_col1= '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "mdaDsmb">MD ANDERSON DSMB</OPTION>
<OPTION value = "mdaExtdsmb">MD ANDERSON  External DSMB </OPTION>
<OPTION value = "indpDsmb">Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.</OPTION>
<OPTION value = "intDsmb">Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.</OPTION>
<OPTION value = "notAppl">Not Applicable</OPTION>
</SELECT>' where codelst_subtyp='dsmbInt' and codelst_desc='Data Safety and Monitoring Board'; 

commit;

--07

-->What criteria are you using for classifying Adverse Events
-->Other : Displayed if 'What critera are you using for classifying adverse events' is 'Other'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvents', 'What criteria are you using for classifying Adverse Events?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvntsOth', 'Other');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--08

---adding the dropdown options to the What critera are you using for classifying adverse events dropdown.

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "CTC2.0">CTC2.0</OPTION>
<OPTION value = "CTCAE3.0">CTCAE3.0</OPTION>
<OPTION value = "CTCAE3.M10">CTCAE3.M10</OPTION>
<OPTION value = "CTCAE4.03">CTCAE4.03</OPTION>
<OPTION value = "Other">Other</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL = 'textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';
COMMIT;

--09

UPDATE ER_CODELST
SET CODELST_DESC = 'Are you requesting an expedited biostatistical review for this protocol?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'expdBiostat';
COMMIT;

/* LE build #05 Scripts */

--01

declare
v_CodeExists number := 0;
v_CodeSeq number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
	IF (v_CodeExists > 0) THEN
		SELECT CODELST_SEQ INTO v_CodeSeq FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_HIDE='Y' WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_SEQ=v_CodeSeq WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators' AND codelst_desc = 'Collaborators';
		commit;
		dbms_output.put_line('Collaborators codelst row fixed');
	END IF;
end;
/

--02

-->Are fresh biopsies needed?
-->If yes, please describe : Displayed if 'Are fresh biopsies needed' is 'Yes'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyNeed', 'Are fresh biopsies needed?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyYes';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyYes', 'If yes, please describe' , 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--03

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';

COMMIT;

--04

--Biomarker and Correlative Studies : �Methodology � How?� to �Methodology�

update er_codelst set codelst_desc = 'Methodology' where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'methodology' ;
 
-- In �Did an MDACC Biostatistician participate in the study design?�  remove double hyphens from values
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes Completely</OPTION>
<OPTION value = "YesPart">Yes Partially</OPTION>
<OPTION value = "NoSponsrDes">No Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'
where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'proDes2';

--Make first field into �Sponsor Name 1� (currently Sponsor Name1)
update er_codelst set codelst_desc = 'Sponsor Name 1' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName1' ;
update er_codelst set codelst_desc = 'Sponsor Name 2' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName2' ;
update er_codelst set codelst_desc = 'Sponsor Name 3' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName3' ;
update er_codelst set codelst_desc = 'Sponsor Name 4' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName4' ;
update er_codelst set codelst_desc = 'Sponsor Name 5' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName5' ;

--Device 1, Device 2, Device 3, etc. turns into Device-1, Device-2, etc. to stay consistent with Agent
update er_codelst set codelst_desc = 'Device-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent1' ;
update er_codelst set codelst_desc = 'Device-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent2' ;
update er_codelst set codelst_desc = 'Device-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent3' ;
update er_codelst set codelst_desc = 'Device-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent4' ;
update er_codelst set codelst_desc = 'Device-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent5' ;

--For Agent 1, Agent 2, etc. fields should be relabeled into Agent-1, Agent-2, etc
update er_codelst set codelst_desc = 'Agent-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent1' ;
update er_codelst set codelst_desc = 'Agent-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent2' ;
update er_codelst set codelst_desc = 'Agent-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent3' ;
update er_codelst set codelst_desc = 'Agent-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent4' ;
update er_codelst set codelst_desc = 'Agent-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent5' ;
update er_codelst set codelst_desc = 'Agent-6' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent6' ;
update er_codelst set codelst_desc = 'Agent-7' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent7' ;
update er_codelst set codelst_desc = 'Agent-8' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent8' ;
update er_codelst set codelst_desc = 'Agent-9' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent9' ;
update er_codelst set codelst_desc = 'Agent-10' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent10' ;

COMMIT;

--05

-->funding provided checkbox


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk1', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--2--
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk2', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--3--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk3', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--4--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk4', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--5--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk5', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	
COMMIT;
END;
/

/* LE build #06 Scripts */

--02

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'prot_priority';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'prot_priority', 'Protocol Prioritization', 'N', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

--04

update er_codelst set codelst_custom_col='hidden-input' where codelst_subtyp='rplSubject'; 
commit;

/* LE build #07 Scripts */

--01

CREATE OR REPLACE FUNCTION ERES."F_NON_FUTURE_DATE" (p_date DATE ) RETURN DATE
AS
v_today DATE := trunc(sysdate);
BEGIN
 if (p_date > v_today) then
   return v_today;
 end if;
return trunc(p_date);
END ;
/

--02

---hiding some of the dropdown options to match the values given in the config doc -bug id: 20981 

update er_codelst set codelst_hide = 'Y' where pk_codelst = '7801' and codelst_type = 'study_division'  and codelst_subtyp = 'CANCER';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7802' and codelst_type = 'study_division'  and codelst_subtyp = 'PEDS';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7803' and codelst_type = 'study_division'  and codelst_subtyp = 'MEDICINE';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7805' and codelst_type = 'study_division'  and codelst_subtyp = 'SURGERY';

---changing the label of the field from Blindin Notes to Blinding Description
update er_codelst set codelst_desc = 'Blinding Description' where codelst_subtyp = 'blndTrtGrp' and codelst_type = 'studyidtype';

COMMIT;

/* LE build #01 Scripts */

-- Missing entries insert

--studyCond2

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'studyCond2', 'Administrative Department', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--doubleBlind

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'doubleBlind';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'blinding', 'doubleBlind', 'Double-Blind', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--spnsrshp1

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'spnsrshp1', 'Indicate if the sponsor/supporter/granting agency will receive data', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #3

--offStdyCri

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'offStdyCri';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'offStdyCri', 'Off Study Criteria', 'checkbox','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--studyscope_3

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'studyscope', 'studyscope_3', 'Multi-Center, MDACC lead','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #4

--pilotFeasibilty

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'phase' AND CODELST_SUBTYP = 'pilotFeasibilty';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc,codelst_hide)
        values (seq_er_codelst.nextval, 'phase', 'pilotFeasibilty', 'Feasibility with N/A','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq#8

--randomized
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'randomized';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','randomized','Adaptive Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--other
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'other';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','other','Other','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--none
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'none';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','none','None','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--blckRndmztn
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'blckRndmztn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','blckRndmztn','Block Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--lstRndmztn
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'lstRndmztn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','lstRndmztn','Simple Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--PocockRndm
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'PocockRndm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','PocockRndm','Stratified Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #10

--vp_cr
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'vp_cr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','vp_cr','VP of Clinical Research Administration Determination','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crc_cc
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'crc_cc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','crc_cc','Clinical Research Committee','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_cc
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'irb_cc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','irb_cc','Institutional Review Board','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--ancillary
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'ancillary';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','ancillary','Ancillary Review Response','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medical
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'medical';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','medical','Medical Review Response','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "DICRC">DICRC</OPTION>
<OPTION value = "dept">Department</OPTION>
<OPTION value = "fedFund">Federal Funding</OPTION>
<OPTION value = "foundation">Foundation</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privRecint">Private Industry /ReCINT</OPTION>
<OPTION value = "RPCCC">RPCCC</OPTION>
</SELECT>' where CODELST_TYPE='studyidtype' and 
CODELST_SUBTYP in ('fundingType1', 'fundingType2', 'fundingType3',
'fundingType4', 'fundingType5');

-- Study Division
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='study_division' and
CODELST_SUBTYP in ('cc', 'cardio', 'med', 'neuro', 'surg', 'other',
'study_divisi_31', 'pulmonary');
commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_6', 'Anesthesiology & Critical Care', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_7', 'Basic Science', 7);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_8', 'Cancer Medicine', 8);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_9', 'Cancer Prevention & Population Sciences', 9);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_10', 'Diagnostic Imaging', 10);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_11';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_11', 'EVP Physician in Chief Area', 11);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_12';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_12', 'Internal Medicine', 12);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_13';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_13', 'Pathology/Lab Medicine', 13);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_14';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_14', 'Pediatrics', 14);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_15';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_15', 'Radiation Oncology', 15);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'sur';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'sur', 'Surgery', 20);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	commit;
end;
/
-- End of Study Division

-- TArea = Department
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='tarea' and
CODELST_SUBTYP in ('lip', 'aging', 'alds', 'allergy', 'alzheimer', 'blood',
'brain', 'tareaBreast', 'cancer', 'dental', 'diabetes', 'ent', 'esm', 'eye',
'gastro', 'heart', 'hypertension', 'impotence', 'infertility', 'lipid', 'liver',
'lung', 'mentalhealth', 'obesity', 'other', 'pain', 'pediatric', 'prostate',
'renal', 'reproductive', 'seizures', 'std', 'urogenital');
---- Fix QA items
update ER_CODELST set CODELST_DESC='Genetics',CODELST_HIDE='N',CODELST_SEQ=130,CODELST_CUSTOM_COL1='study_divisio_7' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_36');
update ER_CODELST set CODELST_DESC='Genitourinary Medical Oncology',CODELST_HIDE='N',CODELST_SEQ=135,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_37');
update ER_CODELST set CODELST_DESC='Genomic Medicine',CODELST_HIDE='N',CODELST_SEQ=140,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_38');

commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_14';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_14','Anesthesiology & Perioperative Medicine','N',1,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_15';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_15','Behavioral Science','N',15,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_16';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_16','Bioinformatics & Computational Biology','N',20,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_17';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_17','Biostatistics','N',25,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_18';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_18','Breast Medical Oncology','N',35,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_20';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_20','Cancer Biology','N',40,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_21';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_21','Cancer System Imaging','N',45,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_22';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_22','Cardiology','N',50,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_23';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_23','Clinical Cancer Prevention','N',55,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_24';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_24','Critical Care','N',60,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_25';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_25','Dermatology','N',70,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_26';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_26','Diagnostic Radiology','N',75,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_27';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_27','Emergency Medicine','N',80,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_28';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_28','Endocrine Neoplasia and HD','N',85,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_29';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_29','Epidemiology','N',90,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_30';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_30','Experimental Radiation Oncology','N',95,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_31';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_31','Experimental Therapeutics','N',100,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_32';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_32','Gastroenterology Hepat & Nutr','N',105,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_33';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_33','Gastrointestinal Medical Oncology','N',115,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_34';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_34','General Internal Medicine','N',120,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_35';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_35','General Oncology','N',125,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_36';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_36','Genetics','N',130,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_37';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_37','Genitourinary Medical Oncology','N',135,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_38';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_38','Genomic Medicine','N',140,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_39';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_39','Gynecologic Oncology & Reproductive Medicine','N',150,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_40';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_40','Head & Neck Surgery','N',160,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_41';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_41','Health Disparities Research','N',165,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_42';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_42','Health Services Research','N',170,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_43';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_43','Hematopathology','N',175,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_44';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_44','Imaging Physics','N',185,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_45';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_45','Immunology','N',190,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_46';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_46','Infectious Diseases','N',195,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_47';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_47','Interventional Radiology','N',200,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_48';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_48','Investigational Cancer Therapeutics','N',205,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_49';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_49','Laboratory Medicine','N',210,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_50';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_50','Leukemia','N',215,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_51';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_51','Lymphoma/Myeloma','N',220,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_52';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_52','Melanoma Medical Oncology','N',225,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_53';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_53','Molecular Carcinogenesis','N',230,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_54';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_54','Molecular Pathology','N',235,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_55';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_55','Molecular and Cellular Oncology','N',240,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_56';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_56','Neuro-Oncology','N',245,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_57';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_57','Neurosurgery','N',255,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_58';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_58','Nuclear Medicine','N',265,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_59';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_59','Orthopaedic Oncology','N',275,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_61';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_61','Pain Medicine','N',280,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_62';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_62','Palliative Care & Rehabilitation Medicine','N',285,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_63';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_63','Pathology','N',290,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_64';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_64','Pediatrics','N',295,'study_divisi_14');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_65';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_65','Plastic Surgery','N',300,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_67';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_67','Proton Therapy','N',305,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_68';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_68','Psychiatry','N',310,'study_divisi_11');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_69';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_69','Pulmonary Medicine','N',315,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_70';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_70','Radiation Oncology','N',320,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_71';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_71','Radiation Physics','N',325,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_73';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_73','Respiratory Care','N',330,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_74';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_74','Sarcoma Medical Oncology - Cytokine & Supportive Oncology','N',335,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_75';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_75','Stem Cell Transplantation','N',340,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_76';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_76','Surgical Oncology','N',345,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_77';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_77','Symptom Research','N',350,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_78';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_78','Systems Biology','N',355,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_79';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_79','Thoracic & Cardiovascular Surgery','N',360,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_80';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_80','Thoracic/Head & Neck Medical Oncology','N',370,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_81';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_81','Urology','N',380,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_83';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_83','Veterinary Medicine & Surgery','N',385,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_82';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_82','Veterinary Sciences','N',390,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
-- End of TArea = Department
commit;
end;
/

update ER_CODELST set CODELST_DESC='Administrative Department' where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='studyCond2';
commit;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyAdmDiv';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) values (seq_er_codelst.nextval,null,'studyidtype','studyAdmDiv','Administrative Division','N',1,'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
end;
/

update ER_CODELST set CODELST_DESC='Cooperative Group / Other Externally Peer Reviewed' where CODELST_TYPE='research_type' and CODELST_SUBTYP='coop';
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='research_type' and CODELST_SUBTYP='none';

--Bug 21213 : Field: Blinding Change drop down choice Double - Blinded to Double - Blind 
update er_codelst set codelst_desc = 'Double-Blind' where codelst_type = 'blinding' and codelst_subtyp = 'doubleBlind' ;

commit;

--02

update er_codelst set codelst_custom_col = 'dropdown'  where codelst_subtyp = 'spnsrshp1' ;
---Indicate if the sponsor/supporter/granting agency will receive data -- Should be a dropdown instead of check box.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';

---adding the dropdown options to the funding type dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privFound">Private Foundation</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
<OPTION value = "coopGrp">Cooperative group</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--adding the options to the checkbox
UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Personnel"},
{data: "option2", display:"Patient Care"},
{data: "option3", display:"Startup Costs"},
{data: "option4", display:"Study Agent / Device"},
{data: "option5", display:"Participant Remunerations"},
{data: "option6", display:"Ancillary Services"},
]}'
WHERE CODELST_TYPE='studyidtype'  AND CODELST_SUBTYP IN(
'fundProvChk1',
'fundProvChk2',
'fundProvChk3',
'fundProvChk4',
'fundProvChk5');

COMMIT;

--03

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL='checkbox',
CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Death"},
{data: "option2", display:"Disease Progression"},
{data: "option3", display:"Disease Relapse"},
{data: "option4", display:"Drug Resistant"},
{data: "option5", display:"Intercurrent Illness"},
{data: "option6", display:"Lost to Follow-Up"},
{data: "option7", display:"New Malignancy"},
{data: "option8", display:"Not Able to Comply with Study Requirements"},
{data: "option9", display:"Patient Decision to Discontinue Study"},
{data: "option10", display:"Patient Non-Compliance"},
{data: "option11", display:"Patient Withdrawal of Consent"},
{data: "option12", display:"Physician Discretion"},
{data: "option13", display:"Pregnancy"},
{data: "option14", display:"Toxicity"},
{data: "option15", display:"Treatment Completion"},
{data: "option16", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='offStdyCri';

--for bug id 20984
update er_codelst set codelst_seq = '1' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_single' ;
update er_codelst set codelst_seq = '2' where CODELST_TYPE='studyscope' and codelst_subtyp = 'studyscope_3' ;
update er_codelst set codelst_seq = '5' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_multi' ;

COMMIT;

--04

--bug 21141
--updating subtypes to Velos supported subtypes
update er_codelst set codelst_subtyp = 'phaseIII' where codelst_type = 'phase' and (codelst_desc = 'Phase III');
update er_codelst set codelst_subtyp = 'phaseI/II' where codelst_type = 'phase' and (codelst_desc = 'Phase I/II');
update er_codelst set codelst_subtyp = 'phaseIV' where codelst_type = 'phase' and (codelst_desc = 'Phase IV');
update er_codelst set codelst_subtyp = 'phaseIV/V' where codelst_type = 'phase' and (codelst_desc = 'Phase IV/V');
update er_codelst set codelst_subtyp = 'phaseII' where codelst_type = 'phase' and (codelst_desc = 'Phase II');
update er_codelst set codelst_subtyp = 'phaseII/III' where codelst_type = 'phase' and (codelst_desc = 'Phase II/III');
update er_codelst set codelst_subtyp = 'phaseI' where codelst_type = 'phase' and (codelst_desc = 'Phase I');

update er_codelst set codelst_subtyp = 'phaseZero' where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
commit;

--hiding Not applicable
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseNA';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Phase 0';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','phaseZero','Phase 0','N',5, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero  inserted');
	else
		update er_codelst set codelst_subtyp = 'phaseZero', codelst_seq = 5 where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Pilot';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilot','Pilot','N',40, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot inserted');
	else
		update er_codelst set codelst_subtyp = 'pilot', codelst_seq = 40 where codelst_type = 'phase' and (codelst_desc = 'Pilot');
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','other','Other','N',45, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:other inserted');
	else
		update er_codelst set codelst_subtyp = 'other', codelst_seq = 45 where codelst_type = 'phase' and (codelst_desc = 'Other');
		dbms_output.put_line('Code-list item Type:phase Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Current Practice';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','currPractice','Current Practice','N',50, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice inserted');
	else
		update er_codelst set codelst_subtyp = 'currPractice', codelst_seq = 50 where codelst_type = 'phase' and (codelst_desc = 'Current Practice');
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Compassionate';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','compassionate','Compassionate','N',55, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate inserted');
	else
		update er_codelst set codelst_subtyp = 'compassionate', codelst_seq = 55 where codelst_type = 'phase' and (codelst_desc = 'Compassionate');
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Lab';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','lab','Lab','N',60, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:Lab inserted');
	else
		update er_codelst set codelst_subtyp = 'lab', codelst_seq = 60 where codelst_type = 'phase' and (codelst_desc = 'Lab');
		dbms_output.put_line('Code-list item Type:phase Subtype:lab already exists');
	end if;
	
	commit;

end;
/

--updating code-item descriptions and sequence
update er_codelst set codelst_desc = 'Phase 1', codelst_seq = 10 where codelst_type = 'phase' and codelst_subtyp = 'phaseI';
update er_codelst set codelst_desc = 'Phase 2', codelst_seq = 15 where codelst_type = 'phase' and codelst_subtyp = 'phaseII';
update er_codelst set codelst_desc = 'Phase 3', codelst_seq = 20 where codelst_type = 'phase' and codelst_subtyp = 'phaseIII';
update er_codelst set codelst_desc = 'Phase 1/2', codelst_seq = 25 where codelst_type = 'phase' and codelst_subtyp = 'phaseI/II';
update er_codelst set codelst_desc = 'Phase 2-3', codelst_seq = 30 where codelst_type = 'phase' and codelst_subtyp = 'phaseII/III';
update er_codelst set codelst_desc = 'Phase 4', codelst_seq = 35 where codelst_type = 'phase' and codelst_subtyp = 'phaseIV';
update er_codelst set codelst_seq = 40 where codelst_type = 'phase' and codelst_subtyp = 'pilot';
update er_codelst set codelst_desc = 'Feasability with N/A', codelst_seq = 65 where codelst_type = 'phase' and codelst_subtyp = 'pilotFeasibilty';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIIIorIV';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIV/V';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseV';
commit;

--05

--bug 20896
update er_codelst set codelst_hide = 'Y' where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'PI';

commit;

--06

--bug 21139
--updating code subtype
update er_codelst set codelst_subtyp = 'ancillary' where codelst_type = 'study_type' and codelst_desc = 'Ancillary';
update er_codelst set codelst_subtyp = 'diagnostic' where codelst_type = 'study_type' and codelst_desc = 'Diagnostic';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'behavioral';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'companion';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'compassionUse';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'earlyDetection';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'epidemiological';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'studyTypeNone';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'study';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'prevention';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'qol';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'screening';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'standardofCare';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'therapeutic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'tissueBankLab';
commit;

update er_codelst set codelst_desc = 'Observational (OBS)', codelst_seq = 10 where codelst_type = 'study_type' and codelst_subtyp = 'observational';
update er_codelst set codelst_desc = 'Ancillary or Correlative (ANC/COR)', codelst_seq = 15 where codelst_type = 'study_type' and codelst_subtyp = 'correlative';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'study_type' and codelst_desc = 'Interventional (INT)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'study_type','interventional','Interventional (INT)','N',5, null);
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional  inserted');
	else
		update er_codelst set codelst_subtyp = 'interventional', codelst_seq = 5 where codelst_type = 'study_type' and (codelst_desc = 'Interventional (INT)');
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional already exists');
	end if;
	commit;
end;
/

update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp not in ('interventional','observational','correlative');
commit;

--07

--bug 21140
update er_codelst set codelst_desc = 'Diagnostic (DIA)', codelst_seq = 5 where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_desc = 'Basic Science (BAS)', codelst_seq = 10 where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
update er_codelst set codelst_desc = 'Health Services Research (HSR)', codelst_seq = 20 where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
update er_codelst set codelst_desc = 'Prevention (PRE)', codelst_seq = 25 where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
update er_codelst set codelst_desc = 'Screening (SCR)', codelst_seq = 30 where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
update er_codelst set codelst_desc = 'Supportive Care (SUP)', codelst_seq = 35 where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
update er_codelst set codelst_desc = 'Treatment (TRE)', codelst_seq = 40 where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
update er_codelst set codelst_desc = 'Other (OTH)', codelst_seq = 50 where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';

commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Epidemiologic Trial (EPI)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','epidemiologic','Epidemiologic Trial (EPI)','N',15, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic  inserted');
	else
		update er_codelst set codelst_subtyp = 'epidemiologic', codelst_seq = 15 where codelst_type = 'studyPurpose' and (codelst_desc = 'Epidemiologic Trial (EPI)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Observational Trial (Obs)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','observational','Observational Trial (Obs)','N',45, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational  inserted');
	else
		update er_codelst set codelst_subtyp = 'observational', codelst_seq = 45 where codelst_type = 'studyPurpose' and (codelst_desc = 'Observational Trial (Obs)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Outcome Trial (Out)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','outcomeTrial','Outcome Trial (Out)','N',55, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial  inserted');
	else
		update er_codelst set codelst_subtyp = 'outcomeTrial', codelst_seq = 55 where codelst_type = 'studyPurpose' and (codelst_desc = 'Outcome Trial (Out)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial already exists');
	end if;
	commit;
end;
/

--08

update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Bayesian Model Averaging, Continuous Reassessment Method (BMA-CRM)';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_subtyp = 'blckRndmztn' and codelst_desc = 'Block Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'EffTox Dose-finding';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Equal Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'List Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Pocock-Simon Stratified Randomization';

commit;

update er_codelst set codelst_seq = 10 where codelst_type = 'randomization' and codelst_subtyp = 'other';
update er_codelst set codelst_seq = 9 where codelst_type = 'randomization' and codelst_desc = 'Stratified Randomization';

commit;

--09

--bug 20987
update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib>Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent1';

commit;

--10

--updating the code-item descriptions 
update er_codelst set codelst_desc = 'VP of Clinical Resaerch Adminsitration Determination', codelst_seq = 13 where codelst_type = 'rev_board' and codelst_subtyp = 'vp_cr';
update er_codelst set codelst_desc = 'Clinical Research Committee', codelst_seq = 2 where codelst_type = 'rev_board' and codelst_subtyp = 'crc_cc';
update er_codelst set codelst_desc = 'Institutional Review Board', codelst_seq = 6 where codelst_type = 'rev_board' and codelst_subtyp = 'irb_cc';
update er_codelst set codelst_desc = 'Ancillary Review Response', codelst_seq = 1 where codelst_type = 'rev_board' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_desc = 'Medical Review Response', codelst_seq = 7 where codelst_type = 'rev_board' and codelst_subtyp = 'medical';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Data and Safety Monitoring Board';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_dataSafety','Data and Safety Monitoring Board','N',3, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_dataSafety', codelst_seq = 3 where codelst_type = 'rev_board' and (codelst_desc = 'Data and Safety Monitoring Board');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'EPAAC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_EPAAC','EPAAC','N',4, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_EPAAC', codelst_seq = 4 where codelst_type = 'rev_board' and (codelst_desc = 'EPAAC');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Exeuctive Session - IRB3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ExecSession','Exeuctive Session - IRB3','N',5, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ExecSession', codelst_seq = 5 where codelst_type = 'rev_board' and (codelst_desc = 'Exeuctive Session - IRB3');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Office of Protocol Research';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ProtRes','Office of Protocol Research','N',8, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ProtRes', codelst_seq = 8 where codelst_type = 'rev_board' and (codelst_desc = 'Office of Protocol Research');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'PI Initiated';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PIInit','PI Initiated','N',9, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PIInit', codelst_seq = 9 where codelst_type = 'rev_board' and (codelst_desc = 'PI Initiated');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Psychosocial Behavior Health Services Research Committee';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PBHSRC','Psychosocial Behavior Health Services Research Committee','N',10, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PBHSRC', codelst_seq = 10 where codelst_type = 'rev_board' and (codelst_desc = 'Psychosocial Behavior Health Services Research Committee');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Response To Compliance Audit/Monitoring Report';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_CompAudit','Response To Compliance Audit/Monitoring Report','N',11, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_CompAudit', codelst_seq = 11 where codelst_type = 'rev_board' and (codelst_desc = 'Response To Compliance Audit/Monitoring Report');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Sponsor Request';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_SponsorReq','Sponsor Request','N',12, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_SponsorReq', codelst_seq = 12 where codelst_type = 'rev_board' and (codelst_desc = 'Sponsor Request');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq already exists');
	end if;
	commit;
end;
/

--Hiding code-items
update er_codelst set codelst_hide = 'Y' where codelst_type = 'rev_board' and codelst_subtyp not in ('vp_cr','crc_cc', 'irb_cc','ancillary','medical','rev_dataSafety',
'rev_EPAAC','rev_ExecSession', 'rev_ProtRes', 'rev_PIInit', 'rev_PBHSRC', 'rev_CompAudit', 'rev_SponsorReq');
commit;

/* LE build #09 Scripts */

--01

DECLARE
   CURSOR c_nonsystem_nologin IS SELECT * FROM ER_USER WHERE USR_TYPE = 'N' and USR_LOGNAME is null;
   r_er_user er_user%ROWTYPE;
BEGIN
    OPEN c_nonsystem_nologin;
    LOOP
        FETCH c_nonsystem_nologin INTO r_er_user;
        EXIT WHEN c_nonsystem_nologin%NOTFOUND;
        BEGIN
            --dbms_output.put_line(r_er_user.usr_lastname);
            update ER_USER set USR_LOGNAME = 'nonsys-'||r_er_user.PK_USER where PK_USER = r_er_user.PK_USER;
        END;
    END LOOP;
    COMMIT;
    CLOSE c_nonsystem_nologin;
END;
/

--02

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and CODELST_SUBTYP='pilotFeasibilty';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilotFeasibilty','Feasability with N/A','N', 65, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('New row inserted');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyscope' and CODELST_SUBTYP='studyscope_3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyscope','studyscope_3','Multi-Center, MDACC lead','N', 3, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('Row already exists');
	end if;
END;
/

update ER_CODELST set CODELST_CUSTOM_COL1='interventional' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('diagnostic','prevention','screening','supportiveCare','treatment');
update ER_CODELST set CODELST_CUSTOM_COL1='interventional,correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('healthSvcRsrch');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('epidemiologic','observational','other','outcomeTrial');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('basicScience');

update ER_CODELST set CODELST_CUSTOM_COL1='irb' where codelst_type='studystat' and 
CODELST_SUBTYP in ('irb_add_info');

commit;

/* LE build #10 Scripts */

--01

update ERES.ER_CODELST set CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes -- Completely</OPTION>
<OPTION value = "YesPart">Yes -- Partially</OPTION>
<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'
where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='proDes2';

update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 1' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt1';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 2' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt2';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 3' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt3';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 4' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt4';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 5' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt5';

update ERES.ER_CODELST set CODELST_DESC='Multi-Center' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='std_cen_multi';
update ERES.ER_CODELST set CODELST_DESC='Multi-Center, MDACC lead' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='studyscope_3';

update ERES.ER_CODELST set CODELST_CUSTOM_COL1=
'<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='biosafety3';

update ERES.ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studySiteType' and CODELST_SUBTYP='responsible';

update ERES.ER_CODELST set CODELST_DESC='Feasibility with N/A' where
CODELST_TYPE='phase' and CODELST_SUBTYP='pilotFeasibilty';

update ER_CODELST set CODELST_DESC='Phase 5', CODELST_HIDE='Y' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_5';

update ER_CODELST set CODELST_DESC='Phase 1/2', CODELST_HIDE='N' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phaseI/II';

update ER_CODELST set CODELST_SUBTYP='phaseII/III', CODELST_DESC='Phase 2-3', 
CODELST_HIDE='N',  CODELST_SEQ = 30 where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_3';

commit;

/* LE build #11 Scripts */

--Missing entries inserts

--For seq #1

--invAntcpAEs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invAntcpAEs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'invAntcpAEs', 'If investigational, provide list of anticipated AEs', 'textarea','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--repSAEs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSAEs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'repSAEs', 'Report of SAEs and Unanticipated Problems', 'textarea','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--blinded

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'blinded';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc)
        values (seq_er_codelst.nextval, 'blinding', 'blinded', 'Blinded');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #3

--dept_chair

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'dept_chair';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'studyvercat', 'dept_chair', 'Protocol Prioritization', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set CODELST_HIDE='N' where
CODELST_TYPE='randomization' and CODELST_SUBTYP='blckRndmztn';

update ER_CODELST set CODELST_DESC = 'If investigational, provide list of anticipated AEs' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='invAntcpAEs';

update ER_CODELST set CODELST_DESC = 'Report of SAEs and Unanticipated Problems' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='repSAEs';

update ER_CODELST set CODELST_HIDE = 'Y' where
CODELST_TYPE='blinding' and CODELST_SUBTYP='blinded';

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'doubleBlind';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'blinding', 'doubleBlind', 'Double-Blind', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	commit;
end;
/

--02

-- Fixed Bug 21352 - Configuration: Study Drug: Agents: a value is missing.
--  Fixed option <OPTION value = "Rigosertib">Rigosertib</OPTION> for all drugAgents.

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent1';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent2';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent3';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent4';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent5';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent6';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent7';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent8';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent9';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent10';

commit;

--03

-- Fixed Bug # 21272

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization
--Updates: Jerd Phichitkul 2015-02-12 13:21:59 PST (Currently 'Department Chair Review' category is passing the document to Click
--instead of the 'Protocol Prioritization' category. )
--1. Hide 'Protocol Prioritization' category.
--2. Rename 'Department Chair Review' to 'Protocol Prioritization'.

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='prot_priority';

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='inf_consent';

--Hiding this after Umer's confirmation
update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='studyvercat_7';

update er_codelst
set codelst_desc='Protocol Prioritization', codelst_seq=8
where codelst_type='studyvercat'
and codelst_subtyp='dept_chair';


update er_codelst
set codelst_seq= 1
where codelst_type='studyvercat'
and codelst_subtyp='protocol';

update er_codelst
set codelst_seq= 7
where codelst_type='studyvercat'
and codelst_subtyp='inv_brochure';


commit;

--04

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization

-- New ones
-- External Collaborator Signature
-- Toxicity Form
-- Sponsor Provided Files
-- Other Documents

set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'collab_sign';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'collab_sign', 'External Collaborator Signature', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'other_docs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'other_docs', 'Other Documents', 'N', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'toxic_form';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'toxic_form', 'Toxicity Form', 'N', 3);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'spons_file';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'spons_file', 'Sponsor Provided Files', 'N', 4);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--06

update er_codelst set CODELST_CUSTOM_COL1 = '{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. ---- Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review.----"},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation? ----- Please attached appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review.-----"}
]}' where codelst_type = 'studyidtype' and codelst_subtyp = 'expdBiostat';

commit;

/* LE build #12 Scripts */

--Missing entries insert

--For seq #1

--role_20

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_20';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'role', 'role_20', 'Co-PI', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set CODELST_SUBTYP='CoPi' where
CODELST_TYPE='role' and CODELST_SUBTYP='role_20' and CODELST_DESC='Co-PI';

commit;

/* LE build #13 Scripts */

--01

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

commit;

--02

delete ER_STUDY_INDIDE where nvl(FK_STUDY, 0) = 0;

commit;

--03

update er_codelst set codelst_desc = 'COEUS Number' where codelst_type = 'studyidtype' and codelst_subtyp = 'grant';

update er_codelst set codelst_desc = 'If gene, use www.genecard.org to pull name from list' where codelst_type = 'studyidtype' and codelst_subtyp = 'ifGeneList';

commit;

--NCI program codes

Update er_codelst set codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CCR';
Update er_codelst set codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CDP';
Update er_codelst set codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CIP';
Update er_codelst set codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CTEP';
Update er_codelst set codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCB';
Update er_codelst set codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCCPS';
Update er_codelst set codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCEG';
Update er_codelst set codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCP';
Update er_codelst set codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DEA';
Update er_codelst set codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DTP';
Update er_codelst set codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'N/A';
Update er_codelst set codelst_seq = 14, codelst_desc='OSB/SPORE' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'OSB/SPOREs';
Update er_codelst set codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'RRP';
Update er_codelst set codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'TRP';
commit;

declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='CCT/CTB';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode' ,'CCTCTB', 'CCT/CTB', 'NCI',2); 
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'CCTCTB' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='CCT/CTB';
	end if;
	commit;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='OD';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','OD', 'OD', 'NCI',13); 
	else
		Update er_codelst set codelst_seq = 13, codelst_subtyp = 'OD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='OD';
	end if;
	commit;
end;
/

--NIH program codes

Update er_codelst set codelst_desc='NIH Office of the Director (OD)',codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'OD';
Update er_codelst set codelst_desc='National Eye Institute (NEI)',codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NEI';
Update er_codelst set codelst_desc='National Heart, Lung, and Blood Institute (NHLBI)',codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHLBI';
Update er_codelst set codelst_desc='National Human Genome Research Institute (NHGRI)',codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHGRI';
Update er_codelst set codelst_desc='National Institute on Aging (NIA)',codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIA';
Update er_codelst set codelst_desc='National Institute on Alcohol Abuse and Alcoholism (NIAAA)',codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAAA';
Update er_codelst set codelst_desc='National Institute of Allergy and Infectious Diseases (NIAID)',codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAID';
Update er_codelst set codelst_desc='National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS)',codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAMS';
Update er_codelst set codelst_desc='National Institute of Biomedical Imaging and Bioengineering (NIBIB)',codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIBIB';
Update er_codelst set codelst_desc='Eunice Kennedy Shriver�National Institute of Child Health and Human Development (NICHD)',codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NICHD';
Update er_codelst set codelst_desc='National Institute on Deafness and Other Communication Disorders (NIDCD)',codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCD';
Update er_codelst set codelst_desc='National Institute of Dental and Craniofacial Research (NIDCR)',codelst_seq = 13 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCR';
Update er_codelst set codelst_desc='National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK)',codelst_seq = 14 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDDK';
Update er_codelst set codelst_desc='National Institute on Drug Abuse (NIDA)�',codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDA';
Update er_codelst set codelst_desc='National Institute of Environmental Health Sciences (NIEHS)',codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIEHS';
Update er_codelst set codelst_desc='National Institute of General Medical Sciences (NIGMS)',codelst_seq = 17 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIGMS';
Update er_codelst set codelst_desc='National Institute of Mental Health (NIMH)',codelst_seq = 18 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIMH';
Update er_codelst set codelst_desc='National Institute of Neurological Disorders and Stroke (NINDS)�',codelst_seq = 20 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINDS';
Update er_codelst set codelst_desc='National Institute of Nursing Research (NINR)',codelst_seq = 21 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINR';
Update er_codelst set codelst_desc='National Library of Medicine (NLM)',codelst_seq = 22 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NLM';
Update er_codelst set codelst_desc='Center for Information Technology (CIT)',codelst_seq = 23 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CIT';
Update er_codelst set codelst_desc='Center for Scientific Review (CSR)',codelst_seq = 24 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CSR';
Update er_codelst set codelst_desc='Fogarty International Center (FIC)',codelst_seq = 25 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'FIC';
Update er_codelst set codelst_desc='NIH Clinical Center (CC)',codelst_seq = 28 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CC';
commit;


declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Cancer Institute (NCI)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCI', 'National Cancer Institute (NCI)', 'NIH',2);
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'NCI' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Cancer Institute (NCI)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NIMHD', 'National Institute on Minority Health and Health Disparities (NIMHD)', 'NIH',19);
	else 
		Update er_codelst set codelst_seq = 19, codelst_subtyp = 'NIMHD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCCIH', 'National Center for Complementary and Integrative Health (NCCIH)', 'NIH',26);
	else
		Update er_codelst set codelst_seq = 26, codelst_subtyp = 'NCCIH' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	end if;
	
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCATS', 'National Center for Advancing Translational Sciences (NCATS)', 'NIH',27);
	else
		Update er_codelst set codelst_seq = 27, codelst_subtyp = 'NCATS' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	end if;
	commit;
end;
/

Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCCAM';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCMHD';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCRR';
commit;

/* LE build #14 Scripts */

--01

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

update ER_CODELST set CODELST_DESC='VP of Clinical Research Administration Determination' where
CODELST_TYPE='rev_board' and CODELST_SUBTYP='vp_cr';

commit;

--02

Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCCAM';
Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCMHD';
Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCRR';
commit;

--Study drug agent
update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">Ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>' where codelst_type = 'studyidtype' and codelst_subtyp like 'drugAgent%'; 

commit;


update er_codelst set codelst_custom_col1 = '{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review."},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation. Please attach appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review."}
]}' where codelst_type = 'studyidtype' and codelst_subtyp like 'expdBiostat'; 

commit;

/* LE build #15 Scripts */

--01

declare
v_exists number := 0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='adapthence'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'adapthence','Adapter henceforth',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='affyGene'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'affyGene','Affymetrix Gene Chip U133A',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bipapVision'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bipapVision','BIPAP Vision Ventilatory Support System',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='biocomp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'biocomp','Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bioSeal'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bioSeal','Bio-Seal Track Plug',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='block'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'block','B-lock solution',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='caphosol'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'caphosol','Caphosol',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cavSpine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cavSpine','Cavity SpineWand',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cliniMACS'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cliniMACS','CliniMACS device',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cryocare'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cryocare','Cryocare',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='daVinci'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'daVinci','da Vinci Robotic Surgical System',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='exAblate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'exAblate','ExAblate device',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='galilMed'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'galilMed','Galil Medical Cryoprobes',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='goldFid'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'goldFid','Gold Fiducial Marker',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imageGuide'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imageGuide','Image Guide Microscope - a high-resolution microendoscope (HRME)',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imagio'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imagio','Imagio Breast Imaging System',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='indocyanine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'indocyanine','Indocyanine green fluorescence lymphography system',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='infrared'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'infrared','Infrared camera',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='inSpectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'inSpectra','InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='integrated'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'integrated','Integrated BRACAnalysis',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='intensity'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'intensity','Intensity Modulated Radiotherapy',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='iodine125'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'iodine125','Iodine-125 radioactive seeds',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='louis3D'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'louis3D','LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='lowCoh'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'lowCoh','Low Coherence Enhanced Backscattering (LEBS)',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mdaApp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mdaApp','MDA Applicator',25);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mirena'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mirena','Mirena (Levonorgestrel Contraceptive IUD)',26);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pemFlex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pemFlex','PEMFlex Solo II High Resolution PET Scanner',27);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pointSpectro'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pointSpectro','Point spectroscopy probe(POS) and PS2',28);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='prostateImm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'prostateImm','Prostate Immobilization Treatment Device',29);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='senoRxCon'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'senoRxCon','SenoRx Contura MLB applicator',30);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SERI'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SERI','SERI Surgical Scaffold',31);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SIR'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SIR','SIR-spheres',32);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='sonablate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'sonablate','Sonablate 500 (Sonablate)',33);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='spectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'spectra','Spectra Optia Apheresis System',34);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vapotherm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vapotherm','The Vapotherm 2000i Respiratory Therapy Device',35);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='strattice'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'strattice','Strattice Tissue Matrix',36);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='syntheCel'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'syntheCel','SyntheCelTM Dura Replacement',37);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='ISLLC'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'ISLLC','The ISLLC Implantable Drug Delivery System (IDDS)',38);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='visica'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'visica','The Visica 2 Treatment System',39);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='theraSphere'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'theraSphere','TheraSphere',40);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='transducer'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'transducer','Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier',41);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='truFreeze'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'truFreeze','truFreezeTM System',42);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='UB500'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'UB500','UB500',43);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='viOptix'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'viOptix','ViOptix continuous transcutaneous tissue oximetry device',44);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vendys'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vendys','VENDYS-Model 6000 B/C',45);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='veridex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'veridex','Veridex CellSearch assay',46);
	end if;
	commit;
end;
/

update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent5';
commit;

--02

update er_studyid set studyid_id ='panobinostat' where studyid_id = 'panobinostat (LBH-589)' and fk_codelst_idtype in (select pk_codelst from er_codelst 
where codelst_type ='studyidtype' and codelst_subtyp in ('drugAgent1', 'drugAgent2', 'drugAgent3', 'drugAgent4', 'drugAgent5',
'drugAgent6','drugAgent7','drugAgent8','drugAgent9','drugAgent10'));

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abiraterone';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abiraterone','abiraterone',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abraxane';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abraxane','abraxane',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-199';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-199','ABT-199',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-888';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-888','ABT-888 (veliparib)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AC220';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AC220','AC220',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AG-013736';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AG-013736','AG-013736',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AMG-337';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AMG-337','AMG-337',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AP-24534';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AP-24534','AP-24534',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ARRY-614';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ARRY-614','ARRY-614',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AZD-1208';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AZD-1208','AZD-1208',10);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='birinipant';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'birinipant','birinipant',11);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BKM-120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BKM-120','BKM-120',12);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BMS-833923';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BMS-833923','BMS-833923',13);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='bortezomib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'bortezomib','bortezomib',14);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Bosutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Bosutinib','Bosutinib',15);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CC-223';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CC-223','CC-223',16);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='cediranib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'cediranib','cediranib (AZD-2171)',17);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CMX-001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CMX-001','CMX-001',18);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CPX-351';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CPX-351','CPX-351',19);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='crenolanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'crenolanib','Crenolanib',20);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CWP-23229';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CWP-23229','CWP-232291',21);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dasatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dasatinib','dasatinib',22);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='denileukin';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'denileukin','denileukin diftitox',23);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='E-75acetate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'E-75acetate','E-75 acetate',24);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='eflornithine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'eflornithine','eflornithine',25);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='entinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'entinostat','Entinostat',26);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='erlotinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'erlotinib','erlotinib',27);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='everolimus';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'everolimus','everolimus (RAD-001)',28);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GDC-0941';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GDC-0941','GDC-0941',29);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GS-1101';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GS-1101','GS-1101',30);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-1120212';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-1120212','GSK-1120212',31);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dabrafenib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dabrafenib','GSK-2118436 (dabrafenib)',32);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-525762';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-525762','GSK-525762',33);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ibrutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ibrutinib','ibrutinib',34);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ICL-670';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ICL-670','ICL-670',35);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='INCB-018424';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'INCB-018424','INCB-018424',36);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='KB-004';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'KB-004','KB-004',37);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lacosamide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lacosamide','lacosamide',38);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Lapatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Lapatinib','Lapatinib',39);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LDE-225';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LDE-225','LDE-225',40);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LEE-011';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LEE-011','LEE-011',41);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lenalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lenalidomide','lenalidomide(CC-5013)',42);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2784544';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2784544','LY-2784544',43);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2940680';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2940680','LY-2940680',44);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-3009120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-3009120','LY-3009120',45);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='masatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'masatinib','masatinib',46);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MEK-162';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MEK-162','MEK-162',47);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-1775';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-1775','MK-1775',48);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-2206';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-2206','MK-2206',49);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MRX-34';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MRX-34','MRX-34',50);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='olaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'olaparib','olaparib',51);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Omacetaxine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Omacetaxine','Omacetaxine',52);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='panobinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'panobinostat','panobinostat (LBH-589)',53);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pazopanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pazopanib','pazopanib',54);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PF-0449913';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PF-0449913','PF-0449913',55);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PKC-412';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PKC-412','PKC-412',56);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pomalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pomalidomide','pomalidomide',57);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib','Ponatinib',58);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib-AP';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib-AP','Ponatinib(AP-24534)',59);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='posaconazole';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'posaconazole','posaconazole',60);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pracinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pracinostat','pracinostat',61);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PRI0724';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PRI0724','PRI0724',62);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Rigosertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Rigosertib','Rigosertib',63);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='rucaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'rucaparib','rucaparib',64);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ruxolitinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ruxolitinib','Ruxolitinib',65);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='S-1';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'S-1','S-1',66);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='SAR-302503';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'SAR-302503','SAR-302503',67);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='topiramate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'topiramate','topiramate',68);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='volasertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'volasertib','volasertib',69);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='vorinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'vorinostat','vorinostat',70);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='VST-1001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'VST-1001','VST-1001',71);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='XL-184';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'XL-184','XL-184',72);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent5';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent6';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent7';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent8';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent9';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent10';
commit;

--03

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesComp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq)
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesComp','Yes -- Completely',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesPart';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesPart','Yes -- Partially',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoSponsrDes';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoSponsrDes','No -- Sponsor provided biostatistical design',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoProsStat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoProsStat','No prospective statistical design in this study',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindProDes2"}' where codelst_type='studyidtype' and codelst_subtyp='proDes2';

commit;

--04

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='coopGrp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE)
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'coopGrp','Cooperative Group',1,'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='consrtm';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'consrtm','Consortium',2, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='nciEtctn';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'nciEtctn','NCI ETCTN',3, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='indstrStdy';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'indstrStdy','Industry Study',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='MDACCInvIni';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'MDACCInvIni','MDACC Investigator Initiated',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindTemplateTyp"}' where codelst_type='studyidtype' and codelst_subtyp='spnsrdProt';

commit;

--05

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaDsmb','MD ANDERSON DSMB',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaExtdsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaExtdsmb','MD ANDERSON  External DSMB ',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='indpDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'indpDsmb','Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='intDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'intDsmb','Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='notAppl'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'notAppl','Not Applicable',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDSMB"}' where codelst_type='studyidtype' and codelst_subtyp='dsmbInt';

commit;

--06

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='NCI'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'NCI','NCI',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='nih'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'nih','NIH (other than NCI)',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='dod'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'dod','DOD',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='peerRevFund'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'peerRevFund','Other peer reviewed funding (e.g. NSF, ACS, etc.)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privInd'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privInd','Private Industry',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privFound'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privFound','Private Foundation',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='deptFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'deptFunds','Departmental Funds',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='donorFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'donorFunds','Donor Funds',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='unfunded'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'unfunded','Unfunded',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='coopGrp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'coopGrp','Cooperative group',10);
	END IF;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindFundingType"}' where codelst_type='studyidtype' and codelst_subtyp like 'fundingType%';

commit;

--07

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'bayesianComp';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'spnsrshp1';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,327,930,'07_misc_codelist.sql',sysdate,'v9.3.0 #727-le15');

commit;

--08

declare
v_exists number :=0;
begin
		select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Aural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Aural','Aural',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Epidural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Epidural','Epidural',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Inhalation'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Inhalation','Inhalation',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraarterial'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraarterial','Intraarterial',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intradermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intradermal','Intradermal',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intralesional'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intralesional','Intralesional',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramucosal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramucosal','Intramucosal',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramuscular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramuscular','Intramuscular',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intranasal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intranasal','Intranasal',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraocular','Intraocular',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraosseous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraosseous','Intraosseous',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraperitoneal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraperitoneal','Intraperitoneal',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrapleural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrapleural','Intrapleural',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrathecal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrathecal','Intrathecal',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intratumoral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intratumoral','Intratumoral',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravenous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravenous','Intravenous',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravesicular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravesicular','Intravesicular',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Ocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Ocular','Ocular',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Oral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Oral','Oral',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Rectal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Rectal','Rectal',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Subcutaneous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Subcutaneous','Subcutaneous',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Sublingual'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Sublingual','Sublingual',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Topical'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Topical','Topical',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Transdermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Transdermal','Transdermal',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Vaginal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Vaginal','Vaginal',25);
	end if;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindRouteAdmin"}' where codelst_type='studyidtype' and codelst_subtyp like 'route%';

commit;

/* LE build #16 Scripts */

--01

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'knownBiomrkr';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'prot_req_ind';

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='gene'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'gene','Gene',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='microrna'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'microrna','Micro RNA',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='snp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'snp','SNP-Small Nucleotide Polymorphism',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='other'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'other','Other',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindBiomrkrType"}' where codelst_type='studyidtype' and codelst_subtyp='biomrkrType';

commit;

--03

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_MOD_STAT_SUBTYP' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_MOD_STAT_SUBTYP VARCHAR(15))';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_MOD_STAT_SUBTYP IS 
'This column stores the codelst subtyp of the status of the respective module mentioned in the PAGE_MOD_TABLE column of this table';

/* LE build #17 Scripts */

--01

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MAJORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MAJORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MAJORVER added');
else
  dbms_output.put_line('Col STUDYVER_MAJORVER already exists');
end if;

END;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MINORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MINORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MINORVER added');
else
  dbms_output.put_line('Col STUDYVER_MINORVER already exists');
end if;

END;
/

--02

update er_codelst set codelst_custom_col = 'textarea' where 
codelst_type ='studyidtype' and codelst_subtyp in ('dose','doseRationale');

commit;

/* LE build #18 Scripts */

--01

update er_codelst set codelst_hide = 'N', codelst_desc='Informed Consent Report' where codelst_type = 'studyvercat' and codelst_subtyp = 'inf_consent';
commit;

/* LE build #19 Scripts */

--05

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPONSES' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPONSES CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPONSES IS 
'This column stores the form responses archives in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPDIFF' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPDIFF CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPDIFF IS 
'This column stores the form responses diff in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_COMMENTS' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_COMMENTS CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_COMMENTS IS 
'This column stores the version comments';

--06

DECLARE
  v_table_exists number := 0;  
BEGIN
  select count(*) into v_table_exists from sys.user_tables where table_name = 'ER_RESUBM_DRAFT';

  if (v_table_exists = 0) then
      execute immediate 'CREATE TABLE ERES.ER_RESUBM_DRAFT AS '
      || 'SELECT * FROM ERES.UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = ''ABC''';
  end if;
end;
/

COMMENT ON TABLE 
ERES.ER_RESUBM_DRAFT IS 
'This table stores re-submission draft data.';

/* LE build #20 Scripts */

--01

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'studCalSchEvnts';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'studCalSchEvnts', 'Study Calendar/Schedule of Events', 'N', 11);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

/* LE build #22 Scripts */

--04

update er_codelst set codelst_desc='Data Safety and Monitoring Plan' where codelst_type = 'studyidtype' and codelst_subtyp='dataSftyMntrPln'; 

commit;

declare
v_id varchar(1000);
begin
	select 'alternateId'|| PK_CODELST into v_id from er_codelst where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev_id';
	
	update er_codelst set 
	codelst_custom_col1='{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"'|| v_id ||'"}]}' 
	where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev'; 

commit;
end;
/

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;

--05

create or replace
FUNCTION F_DISPOPTION(optn VARCHAR2, cdlstID NUMBER) RETURN CLOB 
IS
v_json json ;
a    dbms_utility.uncl_array;
a1    dbms_utility.uncl_array;
len  pls_integer;
v_json_value json_value;
v_json_list json_list;
v_json_list2 json_list;
v_ccc1 VARCHAR2(4000) := '';
v_ccc12 VARCHAR2(4000) := '';
i number := 0;
j number := 0;
begin 
  select codelst_custom_col1 INTO v_ccc1 from er_codelst where codelst_type = 'studyidtype' and codelst_custom_col = 'checkbox' 
  and pk_codelst =cdlstID and codelst_custom_col1 is not null;
 v_ccc12:=optn;
  v_json := json(v_ccc1);
  v_json_list := json_list(v_json.get('chkArray'));
  dbms_utility.comma_to_table(v_ccc12, len, a);
  v_ccc12 := '';
  for j in 1..a.count loop
  for i in 1..v_json_list.count
  loop
    v_json := json(v_json_list.get(i));
    if (v_json.get('data').get_string() = a(j)) then
    a1(i):= v_json.get('display').get_string();
    v_ccc12 :=v_ccc12||a1(i)||', ';
    end if;
  end loop;
  end loop;
  v_ccc12:=(substr(v_ccc12,0,length(v_ccc12)-2));
BEGIN
  RETURN v_ccc12;
  END;
END F_DISPOPTION;
/

/* LE build #23 Scripts */

--01

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;


update er_codelst set codelst_desc = 'Data Safety and Monitoring Plan' where codelst_type ='section' and codelst_subtyp ='section_6_5';

commit;

/* LE build #24 Scripts */

--Missing entries insert

-prtlBlnd

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'prtlBlnd';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc,codelst_hide)
        values (seq_er_codelst.nextval, 'blinding', 'prtlBlnd', 'Partial Blind','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--05

DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_codelst WHERE CODELST_DESC = 'Resubmission Memo Response' and Codelst_Type='studyvercat';
  IF (v_record_exists = 0) THEN
    INSERT INTO ER_CODELST 
		(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ)
	VALUES(
		SEQ_ER_CODELST.nextval,  'studyvercat', 'resubMemoResp', 'Resubmission Memo Response', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyvercat'));
    COMMIT;
  END IF;

END;
/

--06

update er_codelst set codelst_desc = 'Partial Blind' where codelst_type = 'blinding' and codelst_subtyp ='prtlBlnd';

update er_codelst set codelst_desc = 'Triple Blind' where codelst_type = 'blinding' and codelst_subtyp ='bindingTriple';

commit;

/* LE build #26 Scripts */

declare
v_id NUMBER :=0;
begin
	select pk_codelst into v_id from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_default';
	
	if (v_id > 0) then
		update er_user set fk_codelst_theme = v_id where fk_codelst_theme in (select pk_codelst from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_irb');
	end if;

	update er_codelst set codelst_hide = 'Y' where codelst_type = 'theme' and codelst_subtyp = 'th_irb';
	commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,7,'07_consolidated_Script_LindFTbuilds.sql',sysdate,'v10 #759');

commit;