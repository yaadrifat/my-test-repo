set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;
v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;
begin
   SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
	
   IF (v_exists = 0) THEN
       v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

       Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
       values (v_PK_WORKFLOW,'Study Initiation','Study Initiation','STUDY_INITIATION');
       COMMIT;
   ELSE
       SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
   END IF;

   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Protocol Definition" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'protocolDefinition';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'protocolDefinition','Protocol Definition');
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'protocolDefinition';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, 'Protocol Definition', v_PK_WORKFLOW, v_PK_ACTIVITY, 1, 0, 'SELECT PKG_WORKFLOW_RULES.F_studyInit_protDef(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;

   -------------------------------------------------------------------------------------------------------
   --Adding activity named "CRC Review" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'CRC_Review';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'CRC_Review','CRC Review');
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'CRC_Review';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, 'CRC Review', v_PK_WORKFLOW, v_PK_ACTIVITY, 2, 0, 'SELECT PKG_WORKFLOW_RULES.F_studyInit_crcReview(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "IRB Review" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'IRB_Review';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'IRB_Review','IRB Review');
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'IRB_Review';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, 'IRB Review', v_PK_WORKFLOW, v_PK_ACTIVITY, 3, 0, 'SELECT PKG_WORKFLOW_RULES.F_studyInit_irbReview(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,6,'06_workflow_Creation_script.sql',sysdate,'v10 #759');

commit;