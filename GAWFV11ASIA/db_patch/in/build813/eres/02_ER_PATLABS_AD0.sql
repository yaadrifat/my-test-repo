create or replace TRIGGER "ERES"."ER_PATLABS_AD0" AFTER DELETE ON ER_PATLABS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
    
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := oldUsr ;
      END;

  Audit_Trail.record_transaction (raid, 'ER_PATLABS', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'ACCESSION_NUM', :OLD.ACCESSION_NUM);
       Audit_Trail.column_delete (raid, 'CMS_APPROVED_LAB', :OLD.CMS_APPROVED_LAB);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CUSTOM001', :OLD.CUSTOM001);
       Audit_Trail.column_delete (raid, 'CUSTOM002', :OLD.CUSTOM002);
       Audit_Trail.column_delete (raid, 'CUSTOM003', :OLD.CUSTOM003);
       Audit_Trail.column_delete (raid, 'CUSTOM004', :OLD.CUSTOM004);
       Audit_Trail.column_delete (raid, 'CUSTOM005', :OLD.CUSTOM005);
       Audit_Trail.column_delete (raid, 'CUSTOM006', :OLD.CUSTOM006);
       Audit_Trail.column_delete (raid, 'CUSTOM007', :OLD.CUSTOM007);
       Audit_Trail.column_delete (raid, 'CUSTOM008', :OLD.CUSTOM008);
       Audit_Trail.column_delete (raid, 'CUSTOM009', :OLD.CUSTOM009);
       Audit_Trail.column_delete (raid, 'CUSTOM010', :OLD.CUSTOM010);
       Audit_Trail.column_delete (raid, 'ER_GRADE', :OLD.ER_GRADE);
       Audit_Trail.column_delete (raid, 'ER_NAME', :OLD.ER_NAME);
       Audit_Trail.column_delete (raid, 'FDA_LICENSED_LAB', :OLD.FDA_LICENSED_LAB);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_ABFLAG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_ABFLAG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_STDPHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STDPHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_TSTSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_TSTSTAT', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_EVENT_ID', :OLD.FK_EVENT_ID);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'FK_SPECIMEN', :OLD.FK_SPECIMEN);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_TESTGROUP', :OLD.FK_TESTGROUP);
       Audit_Trail.column_delete (raid, 'FK_TESTID', :OLD.FK_TESTID);
       Audit_Trail.column_delete (raid, 'FK_TEST_METHOD', :OLD.FK_TEST_METHOD);
       Audit_Trail.column_delete (raid, 'FK_TEST_OUTCOME', :OLD.FK_TEST_OUTCOME);
       Audit_Trail.column_delete (raid, 'FK_TEST_REASON', :OLD.FK_TEST_REASON);
       Audit_Trail.column_delete (raid, 'FK_TEST_SPECIMEN', :OLD.FK_TEST_SPECIMEN);
       Audit_Trail.column_delete (raid, 'FK_TIMING_OF_TEST', :OLD.FK_TIMING_OF_TEST);
       Audit_Trail.column_delete (raid, 'FT_TEST_SOURCE', :OLD.FT_TEST_SOURCE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'NOTES', :OLD.NOTES);
       Audit_Trail.column_delete (raid, 'PK_PATLABS', :OLD.PK_PATLABS);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'TEST_DATE', :OLD.TEST_DATE);
       Audit_Trail.column_delete (raid, 'TEST_LLN', :OLD.TEST_LLN);
       Audit_Trail.column_delete (raid, 'TEST_RANGE', :OLD.TEST_RANGE);
       Audit_Trail.column_delete (raid, 'TEST_RESULT', :OLD.TEST_RESULT);
       Audit_Trail.column_delete (raid, 'TEST_RESULT_TX', :OLD.TEST_RESULT_TX);
       Audit_Trail.column_delete (raid, 'TEST_TYPE', :OLD.TEST_TYPE);
       Audit_Trail.column_delete (raid, 'TEST_ULN', :OLD.TEST_ULN);
       Audit_Trail.column_delete (raid, 'TEST_UNIT', :OLD.TEST_UNIT);
COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,412,2,'02_ER_PATLABS_AD0.sql',sysdate,'v11 #813');
commit; 