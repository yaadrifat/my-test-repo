 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'MILEAPNDX_DESC'
      and table_name = 'ER_MILEAPNDX';

  if (v_column_exists > 0) then
      execute immediate 'alter table ER_MILEAPNDX modify MILEAPNDX_DESC  varchar2(500 byte)';
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,412,1,'01_ALTER_ER_MILEAPNDX.sql',sysdate,'v11 #813');
commit; 