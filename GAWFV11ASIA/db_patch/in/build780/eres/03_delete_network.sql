set define off;
create or replace
PROCEDURE "DELETE_NETWORK" (p_netId in number,p_userId in varchar2, p_ipAdd in varchar2,p_delType in varchar2)
IS

BEGIN
  --delete Network data
if (p_deltype='NA') then
FOR i IN (SELECT pk_nwsites  FROM ER_NWSITES START WITH pk_nwsites = p_netId CONNECT BY PRIOR pk_nwsites = FK_NWSITES order by pk_nwsites desc)
LOOP
delete from sch_docs where PK_DOCS in(select MD_MODELEMENTPK   from er_moredetails where FK_MODPK=p_netId and MD_MODNAME='Network_doc');
delete from er_moredetails where fk_modpk = i.pk_nwsites;

FOR j IN (select pk_nwusers from er_nwusers where fk_nwsites= i.pk_nwsites order by pk_nwusers desc)
LOOP
delete from er_moredetails where fk_modpk=j.pk_nwusers;
END LOOP;
delete from er_nwusers where fk_nwsites= i.pk_nwsites;
delete from er_nwsites where pk_nwsites=i.pk_nwsites;
COMMIT;
END LOOP;
else

delete from sch_docs where PK_DOCS in(select MD_MODELEMENTPK   from er_moredetails where FK_MODPK=p_netId and MD_MODNAME='Network_doc');
delete  from  er_nwusers where PK_NWUSERS=p_netId;
delete  from  er_nwusers where PK_NWUSERS=p_netId;
COMMIT;
END If;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,379,3,'03_delete_network.sql',sysdate,'v10.1 #780');

commit;

