set define off;
Declare
	v_invNumber number:=0;
	check_seq number:=0;
Begin

	select count(*) into check_seq from user_sequences where sequence_name='SEQ_ER_INVOICE_ID';

	if(check_seq=0) Then
		Begin
			SELECT MAX(to_number(invnum)) into v_invNumber
			FROM
			(SELECT ERES.F_IS_NUMBER(SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',
				(SELECT LENGTH(study_number) FROM er_study WHERE PK_study = FK_STUDY
				))+1)) is_number,
				SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',
				(SELECT LENGTH(study_number) FROM er_study WHERE PK_study = FK_STUDY
				))+1) invnum
				FROM er_invoice
				)
			WHERE is_number>0;
			Exception WHEN NO_DATA_FOUND THEN
			v_invNumber:=0;
		end;
		
		EXECUTE IMMEDIATE 'CREATE SEQUENCE "ERES"."SEQ_ER_INVOICE_ID" MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH '||to_char(v_invNumber+1)||' NOCACHE NOORDER NOCYCLE';
	End if;
commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,3,'03_invoice_auto.sql',sysdate,'v11 #828');

commit;	
/