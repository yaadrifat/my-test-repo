declare
begin
	delete  from  er_repxsl where pk_repxsl in (110,159,184,148,500,501);
	delete  from er_report where pk_report in (148,500,501);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,10,'10_del_er_repxsl.sql',sysdate,'v10.1 #782');

commit;