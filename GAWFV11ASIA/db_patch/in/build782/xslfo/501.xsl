<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:key name="studyNum" match="ROW" use="concat(STUDY_NUMBER,' ')"/>
  <xsl:key name="studyTitle" match="ROW" use="concat(STUDY_TITLE,' ')"/>
  <xsl:key name="studyPrinv" match="ROW" use="concat(STUDY_PRINV,' ')"/>
  <xsl:key name="meetingDate" match="ROW" use="concat(MEETING_DATE,' ')"/>
  <xsl:key name="submissionStatus" match="ROW" use="concat(SUBMISSION_STATUS,' ')"/>
  <xsl:key name="submissionStatusDate" match="ROW" use="concat(SUBMISSION_STATUS_DATE,' ')"/>
  <xsl:template match="/">
  <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="eIRB" page-height="11in" page-width="8.5in" 
          margin-top="0.7in" margin-bottom="0.7in" margin-left="0.7in" margin-right="0.7in">
        <fo:region-body/>
        <fo:region-after/>
      </fo:simple-page-master>
    </fo:layout-master-set>
      <xsl:apply-templates select="ROWSET"/>
  </fo:root>
  </xsl:template>
  <xsl:template match="ROWSET">
  <fo:page-sequence master-reference="eIRB" initial-page-number="1" font-family="serif">
    <fo:static-content flow-name="xsl-region-after">
      <fo:block text-align="center" font-family="serif" font-size="10pt">
      Mail-Stop 1234
      </fo:block>
      <fo:block text-align="center" font-family="serif" font-size="10pt">
      Phone: (800) 555-1234  &#160;Fax: (800) 555-1234  &#160;humansubjects@example.com
      </fo:block>
    </fo:static-content>
    <fo:flow flow-name="xsl-region-body">
      <fo:block font-size="16pt"
                font-weight="bold"
                line-height="15pt"
                space-after.optimum="16pt"
                text-align="center">
              XYZ Medical Center
      </fo:block>
      <fo:block text-align="left"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                 Human Research Protection Program
      </fo:block>
      <fo:block text-align="left"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                
                  <xsl:value-of select="//SUBMISSION_STATUS_DATE"/>
                
      </fo:block>
      <fo:block text-align="left"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
        <fo:table width="500pt" table-layout="fixed">
          <fo:table-column column-width="150pt" column-number="1"></fo:table-column>
          <fo:table-column column-width="350pt" column-number="2"></fo:table-column>
          <fo:table-body start-indent="0pt" text-align="start">
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Project Number:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:for-each select="ROW[count(. | key('studyNum', concat(STUDY_NUMBER,' '))[1])=1]">
                  <xsl:variable name="s_num" select="STUDY_NUMBER" />
                  <xsl:value-of select="$s_num"/>
                </xsl:for-each>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Project Title:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:for-each select="ROW[count(. | key('studyTitle', concat(STUDY_TITLE,' '))[1])=1]">
                  <xsl:variable name="s_title" select="STUDY_TITLE" />
                  <xsl:value-of select="$s_title"/>
                </xsl:for-each>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Sponsor:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>N/A</fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Protocol Number:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>N/A</fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Primary Investigator:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:for-each select="ROW[count(. | key('studyPrinv', concat(STUDY_PRINV,' '))[1])=1]">
                  <xsl:variable name="s_prinv" select="STUDY_PRINV" />
                  <xsl:value-of select="$s_prinv"/>
                </xsl:for-each>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Meeting Date:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:for-each select="ROW[count(. | key('meetingDate', concat(MEETING_DATE,' '))[1])=1]">
                  <xsl:variable name="m_date" select="MEETING_DATE" />
                  <xsl:value-of select="$m_date"/>
                </xsl:for-each>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-weight="bold">Status:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
               
                  <xsl:value-of select="//SUBMISSION_STATUS"/>
                
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          </fo:table-body>
        </fo:table>
      </fo:block>
      <fo:block text-align="left"
                space-before.optimum="12pt"
                space-after.optimum="6pt">
                Dear Investigator:
      </fo:block>
      <fo:block text-align="left"
                text-indent="0.5in"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                Your research proposal involving human subject participants has 
                been reviewed by the XYZ Medical Center Human Subjects Committee (HSC). 
                Please be advised that you not commence your proposed research 
                until you have received notice of approval from the HSC Office. 
                In an effort to continue the review of your application, the 
                Committee requests that you respond to the following issues:
      </fo:block>
      <fo:block text-align="left"
                font-weight="bold"
                text-decoration="underline"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                HSC REVIEW
      </fo:block>
      <xsl:apply-templates select="ROW"/>
      <fo:block text-align="left"
                font-weight="bold"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                When answering the provisos, please submit two (2) copies of your responses to the HSC office. 
                If only HSC provisos are listed, please submit just one (1) copy.
      </fo:block>
      <fo:block text-align="left"
                text-indent="0.5in"
                space-before.optimum="6pt"
                space-after.optimum="6pt">
                Please respond to the above issues by means of a letter, and 
				submit any revised documents (highlighted and clean copies, if appropriate) 
                with your letter, within 45 days of the date of this letter to avoid your 
				file being administratively terminated. 
                If you have any questions regarding the human subject protection process, 
				please do not hesitate to contact our office.
      </fo:block>
      <fo:block text-align="center"
                space-before.optimum="6pt"
                space-after.optimum="32pt">
                Verty truly yours,
      </fo:block>
      <fo:block text-align="center"
                space-before.optimum="6pt"
                space-after.optimum="0pt">
                John Dole, M.D.
      </fo:block>
      <fo:block text-align="center"
                space-before.optimum="0pt"
                space-after.optimum="16pt">
                IRB Administrator
      </fo:block>
    </fo:flow>
  </fo:page-sequence>
  </xsl:template>
  <xsl:template match="ROW">
    <fo:block text-align="left"><xsl:value-of select="PROVISO"/></fo:block>
  </xsl:template>
</xsl:stylesheet>