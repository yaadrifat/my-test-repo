set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'TM' and OBJECT_SUBTYPE = 'config_menu';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'config_menu', 'top_menu', 13, 
    1, 'Configurations', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'M' and OBJECT_SUBTYPE = 'study_screen';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'study_screen', 'config_menu', 1, 
    1, 'Manage Study Screen', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

end;
/
