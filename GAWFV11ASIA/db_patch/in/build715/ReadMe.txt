/////*********This readMe is specific to v9.3.0 build #715**********////

1) Lind FT UI configuration
To turn on Lind FT eIRB mode, add this line to
EIRB_MODE=LIND
from configBundle_custom.properties.

This will hide Reviewer's UI and only show PI's UI in eIRB.

-- CSS Chnages --
Few CSS, JSP and JS files has been changed to apply UI look as it was done in LindFT


*********************************************************************************************
