/////*********This readMe is specific to v11 build #832(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. Regular bug fixing .

Note: 1. Purge audit tables with respect to bug #26126.
			a) audit_row
			b) audit_column
	  2. Following files are included in folder '<wildfly server>/conf' for bugs(27488,27500,28268)
			a) antisamy-highsecurity.xml
			b) antisamy.xsd
		 These files are referenced to eResearch security patch also.