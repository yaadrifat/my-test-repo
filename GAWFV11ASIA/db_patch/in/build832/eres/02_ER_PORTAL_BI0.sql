set define off;
create or replace TRIGGER "ERES"."ER_PORTAL_BI0" BEFORE INSERT ON ER_PORTAL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PORTAL',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_PORTAL', :NEW.PK_PORTAL);
       Audit_Trail.column_insert (raid, 'PORTAL_AUDITUSER', :NEW.PORTAL_AUDITUSER);
       Audit_Trail.column_insert (raid, 'PORTAL_BGCOLOR', :NEW.PORTAL_BGCOLOR);
       Audit_Trail.column_insert (raid, 'PORTAL_CONSENTING_FORM', :NEW.PORTAL_CONSENTING_FORM);
       Audit_Trail.column_insert (raid, 'PORTAL_CREATED_BY', :NEW.PORTAL_CREATED_BY);
       Audit_Trail.column_insert (raid, 'PORTAL_CREATELOGINS', :NEW.PORTAL_CREATELOGINS);
       Audit_Trail.column_insert (raid, 'PORTAL_DEFAULTPASS', :NEW.PORTAL_DEFAULTPASS);
       Audit_Trail.column_insert (raid, 'PORTAL_DESC', :NEW.PORTAL_DESC);
       Audit_Trail.column_insert (raid, 'PORTAL_DISP_TYPE', :NEW.PORTAL_DISP_TYPE);
      -- Audit_Trail.column_insert (raid, 'PORTAL_FOOTER', :NEW.PORTAL_FOOTER);
      -- Audit_Trail.column_insert (raid, 'PORTAL_HEADER', :NEW.PORTAL_HEADER);
       Audit_Trail.column_insert (raid, 'PORTAL_LEVEL', :NEW.PORTAL_LEVEL);
       Audit_Trail.column_insert (raid, 'PORTAL_NAME', :NEW.PORTAL_NAME);
       Audit_Trail.column_insert (raid, 'PORTAL_NOTIFICATION_FLAG', :NEW.PORTAL_NOTIFICATION_FLAG);
       Audit_Trail.column_insert (raid, 'PORTAL_SELFLOGOUT', :NEW.PORTAL_SELFLOGOUT);
       Audit_Trail.column_insert (raid, 'PORTAL_STATUS', :NEW.PORTAL_STATUS);
       Audit_Trail.column_insert (raid, 'PORTAL_TEXTCOLOR', :NEW.PORTAL_TEXTCOLOR);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;

/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,2,'02_ER_PORTAL_BI0.sql',sysdate,'v11 #832');

commit;
/