declare 
v_exists number:=0;
begin
select count(*) into v_exists from user_tables where table_name='ER_REVIEW_BOARD_LOGIC';
if v_exists=0 then
  execute immediate('create table er_review_board_logic(pk_review_board_logic number primary key,logic_display_query varchar2(4000), fk_reviewboard number,  logic_name varchar2(50),display_name varchar2(100), display_category varchar2(100))');
end if;
end;
/
declare 
v_seq_exists number:=0;
begin
select count(*) into v_seq_exists from user_sequences where sequence_name='SEQ_REVIEW_BOARD_LOGIC';
if v_seq_exists=0 then
  execute immediate('CREATE SEQUENCE  SEQ_REVIEW_BOARD_LOGIC  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL');
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,401,1,'01_REVIEW_LOGICTBL.sql',sysdate,'v11 #802');
commit; 

