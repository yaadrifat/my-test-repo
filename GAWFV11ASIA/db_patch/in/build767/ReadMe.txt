/////*********This readMe is specific to v10 build #767	  **********////

1. 04_data_patch.sql is a data patch released to fix the bug no: 24056.
2. Since v10 is not delivered to any client we will not be including this patch in the final package. Also there won't be any Track patch entry for this patch.