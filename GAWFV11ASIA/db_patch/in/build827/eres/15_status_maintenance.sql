declare
valid_current number;
begin
for nullrecords in (select fk_study from er_studystat  where FK_CODELST_STUDYSTAT is null group by fk_study)
loop
select count(*) into valid_current from er_studystat where fk_study=nullrecords.fk_study and current_stat=1 and FK_CODELST_STUDYSTAT is not null;
if valid_current=0 then
update er_studystat set current_stat=1 where pk_studystat in (select max(pk_studystat) from er_studystat where fk_study=nullrecords.fk_study and FK_CODELST_STUDYSTAT is not null);
commit;
end if;

end loop;
end;
/
delete from er_studystat where FK_CODELST_STUDYSTAT is null;
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,15,'15_status_maintenance.sql',sysdate,'v11 #827');
commit;