DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from er_browserconf
    where FK_BROWSER = (select pk_browser from er_browser where browser_module='irbPend')
	and BROWSERCONF_COLNAME='PK_SUBMISSION';
      --and owner = 'SCOTT --*might be required if you are using all/dba views

  if (v_column_exists > 0) then
      update er_browserconf set BROWSERCONF_SETTINGS='{"key":"PK_SUBMISSION", "label":"Submission ID", "format":"form_submlink","sortable":true, "resizeable":true,"hideable":true}'
	  where PK_BROWSERCONF= (select PK_BROWSERCONF from er_browserconf where FK_BROWSER = (select pk_browser from er_browser where browser_module='irbPend')
	and BROWSERCONF_COLNAME='PK_SUBMISSION');
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,4,'04_er_browserconf.sql',sysdate,'v11 #827');
commit;