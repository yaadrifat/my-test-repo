declare 
v_exists number;
begin
select count(*) into v_exists from user_tab_cols where table_name='ER_STUDYAPNDX' and column_name='STAMP_REF';
if v_exists =0 then
  execute immediate('alter table er_studyapndx add STAMP_REF number');
end if;
end;
/
comment on column ER_STUDYAPNDX.STAMP_REF is 'Stores reference of PK_STATUS_HISTORY. To track which status added stamp in the document.';

declare 
v_exists number;
begin
select count(*) into v_exists from user_tab_cols where table_name='ER_STUDYAPNDX' and (column_name='PRESTAMP_FILEOBJ' or column_name='PRESTAMP_FILEURI');
if v_exists =0 then
  execute immediate('alter table er_studyapndx add (PRESTAMP_FILEOBJ BLOB, PRESTAMP_FILEURI VARCHAR2(300))');
end if;
end;
/
comment on column ER_STUDYAPNDX.PRESTAMP_FILEOBJ is 'Stores the backup of file blob before stamping';
comment on column ER_STUDYAPNDX.PRESTAMP_FILEURI is 'Stores the backup of file name before stamping';
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,5,'05_StudyApndx_alter.sql',sysdate,'v11 #827');
commit;