create or replace TRIGGER "ERES"."ER_STUDYFORMS_AU_IRB" 
AFTER UPDATE OF FORM_COMPLETED
ON ERES.ER_STUDYFORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

v_lf_submission_type varchar2(20);
 v_form_status varchar2(20);
v_ret  number;
v_formcount number;
 BEGIN

/* Whenever a form is answered check if it is for eIRB ongoing study*/

begin
    select nvl(lf_submission_type,' ')
    into v_lf_submission_type
    from er_linkedforms where
    fk_formlib = :new.fk_formlib and nvl(lf_isirb,0) = 1 and
    lf_submission_type in ('irb_ongo_amd','irb_ongo_prob','irb_ongo_cont','irb_ongo_clos');


if (v_lf_submission_type != ' ') then

    -- get form status
    if (nvl(:new.form_completed,0) <> nvl(:old.form_completed,0)) then
        begin
             select nvl(codelst_subtyp,'')
             into v_form_status
             from er_codelst where pk_codelst = :new.form_completed;
        exception when no_data_found then
                v_form_status :='';
        end;
select count(*) into v_formcount from audit_column where raid in (select raid from audit_row where rid =:old.rid) and COLUMN_NAME='FORM_COMPLETED' and OLD_VALUE = (select pk_codelst from er_codelst where codelst_type='fillformstat' and codelst_subtyp='complete');
if(v_formcount<1) then
        if v_form_status = 'complete' then
             pkg_eirb.sp_submit_ongoing(:new.fk_study ,v_lf_submission_type ,:new.creator ,:new.ip_add ,
            v_ret , :new.form_completed );

        end if; --for form status
end if; -- work only for one completed status
    end if;

end if; --for v_lf_submission_type

exception when no_data_found then
    v_lf_submission_type := '';
end;


END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,10,'10_ER_STUDYFORMS_AU_IRB.sql',sysdate,'v11 #827');
commit;