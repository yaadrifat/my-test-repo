DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Study Submission History(All)';

		IF (row_check = 1) then
			UPDATE er_report SET rep_sql_clob = 'SELECT 
  review_board_name ,
  pk_submission,
  st.pk_submission_status ,
  study_number,
  study_title,
  F_CODELST_DESC(submission_type) submission_type_desc,
  submission_type,
  F_CODELST_DESC(st.submission_status) submission_status_desc,
  st.submission_status,
  TO_CHAR(sb.created_on,pkg_dateutil.f_get_dateformat) submission_date  
  FROM er_submission sub,
  er_submission_board sb,
  er_study s,
  er_submission_status st, er_review_board rb
  WHERE sub.fk_study         = s.pk_study
  AND sub.pk_submission      = sb.fk_submission
  AND st.fk_submission_board = sb.pk_submission_board 
  and st.fk_submission_board is not null
  and pk_review_board= sb.fk_review_board
  AND fk_review_board       IN
  (SELECT pk_review_board
  FROM er_review_board erb
  WHERE erb.fk_account = :sessAccId
  AND '',''
    || erb.board_group_access
    || '','' LIKE ''%,'' || (select er_user.FK_GRP_DEFAULT from er_user where pk_user= :sessUserId) || '',%''
  )
  AND ( EXISTS
  (SELECT *
  FROM er_studyteam t
  WHERE t.fk_study                  = s.pk_study
  AND t.fk_user                     = :sessUserId
  AND NVL(t.study_team_usr_type,''D'')=''D''
  )
  OR pkg_superuser.F_Is_Superuser(:sessUserId, pk_study) = 1 ) 
  AND s.fk_account = :sessAccId 
  AND s.pk_study IN (:studyId)  
  order by study_number, submission_status_date desc'
  WHERE rep_name = 'Study Submission History(All)';
    
		END IF;
	END IF;
  COMMIT;
  END;
  /
DECLARE 
  table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Study Submission History(Current)';

		IF (row_check = 1) then
			UPDATE er_report SET rep_sql_clob = 'SELECT 
  review_board_name ,
  pk_submission,
  st.pk_submission_status ,
  study_number,
  study_title,
  F_CODELST_DESC(submission_type) submission_type_desc,
  submission_type,
  F_CODELST_DESC(st.submission_status) submission_status_desc,
  st.submission_status,
  TO_CHAR(sb.created_on,pkg_dateutil.f_get_dateformat) submission_date  
  FROM er_submission sub,
  er_submission_board sb,
  er_study s,
  er_submission_status st, er_review_board rb
  WHERE sub.fk_study         = s.pk_study
  AND sub.pk_submission      = sb.fk_submission
  AND st.fk_submission_board = sb.pk_submission_board 
  AND is_current             = 1 
  and st.fk_submission_board is not null
  and pk_review_board= sb.fk_review_board
  AND fk_review_board       IN
  (SELECT pk_review_board
  FROM er_review_board erb
  WHERE erb.fk_account = :sessAccId
  AND '',''
    || erb.board_group_access
    || '','' LIKE ''%,'' || (select er_user.FK_GRP_DEFAULT from er_user where pk_user= :sessUserId) || '',%''
  )
  AND ( EXISTS
  (SELECT *
  FROM er_studyteam t
  WHERE t.fk_study                  = s.pk_study
  AND t.fk_user                     = :sessUserId
  AND NVL(t.study_team_usr_type,''D'')=''D''
  )
  OR pkg_superuser.F_Is_Superuser(:sessUserId, pk_study) = 1 ) 
  AND s.fk_account = :sessAccId 
  AND s.pk_study IN (:studyId)  
  order by study_number, submission_status_date desc'
  WHERE rep_name = 'Study Submission History(Current)';
    
		END IF;
	END IF;
  COMMIT;
  END;
  /
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,8,'08_er_report.sql',sysdate,'v11 #827');
commit;