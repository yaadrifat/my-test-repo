/////*********This readMe is specific to v11 build #789**********////


*********************************************************************************************************************************************************************
VELOS PM team and MAYO has done gap analysis of Network tab enhancement and came up with  change request and new
implementation. there were around 14 items identified. Check the "ACC-39863 -- Network Tab Nov 12 2017 v0.8.docx" for Changes from Gap Analysis section.  In  build#788 we have released 7 items (1,4,6,8,10,12 &13) we are releasing another 5 items in this build  They are listed below.

A.
1. --2.	Manage Networks tab always displays collapsed
2. --7. Multiple Person roles in a Network-organization
3. --3. The UI is unpredictable when creating networks and adding organizations to networks using the drag and drop feature
4. --9. Person-organization status history
5. --11.Organization CTEP ID display

The remaining items 2 (5 & 14) items will be covered in the next build.



B. Velos eSample Fixes and Enhancements for v11
---------------------------------------------------------------------
This eSample enhancement request came from PM team as points wise in a document.  Team has worked on this document and covered  many of these items.
To identify the items released in this build we have highlighted the items in the document with YELLOW color.

In Build#787  we have covered the following items. 
1. Navigation between eSample and Patient tabs:  A & B     2. Specimens > Search:  A C D & E    3. Preparation Area: A B & C    4. Specimens > Label:  A  5. Specimens: B    6.Add Multiple Specimens: A 


In this BUILD#789 we have covered following items:  
2.Specimens > Search:   B & G     3.Preparation Area: D   7.Specimen > Select Location (everywhere the Select Location lookup shows up): A B C   

ITEMS NOT COVERED YET:
2.Specimens > Search: F   5. Specimens: A & C   6.Add Multiple Specimens:  B  7.Specimen > Select Location (everywhere the Select Location lookup shows up): D    8.Storage: A 
