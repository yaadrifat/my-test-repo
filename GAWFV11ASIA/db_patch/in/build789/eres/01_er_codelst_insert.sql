set define off;
DECLARE 
	table_check number;
	v_row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_CODELST';
  
  IF (table_check > 0) then
  SELECT count(*) into v_row_check FROM ER_CODELST where CODELST_TYPE = 'networkuserstat'  and CODELST_SUBTYP = 'Active';
        
      if v_row_check = 0 then 
       INSERT INTO ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) VALUES(SEQ_ER_CODELST.nextval,NULL,'networkuserstat','Active','Active','N',1);
       end if;
    
  END IF;
  
END;
/
set define off;
DECLARE 
	table_check number;
	v_row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_CODELST';
  
  IF (table_check > 0) then
  SELECT count(*) into v_row_check FROM ER_CODELST where CODELST_TYPE = 'networkuserstat'  and CODELST_SUBTYP = 'Inactive';
        
      if v_row_check = 0 then 
       INSERT INTO ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) VALUES(SEQ_ER_CODELST.nextval,NULL,'networkuserstat','Inactive','Inactive','N',2);
       end if;
    
  END IF;
  
END;
/
set define off;
DECLARE 
	table_check number;
	v_row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_CODELST';
  
  IF (table_check > 0) then
  SELECT count(*) into v_row_check FROM ER_CODELST where CODELST_TYPE = 'networkuserstat'  and CODELST_SUBTYP = 'Pending';
        
      if v_row_check = 0 then 
       INSERT INTO ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) VALUES(SEQ_ER_CODELST.nextval,NULL,'networkuserstat','Pending','Pending','N',3);
       end if;
    
  END IF;
  
END;
/

INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,1,'01_er_codelst_insert.sql',sysdate,'v11 #789');

	commit;
