create or replace PROCEDURE "DELETE_NETWORK" (p_netId in number,p_userId in varchar2, p_ipAdd in varchar2,p_delType in varchar2,p_studyId in number)
IS

BEGIN
  --delete Network data
IF(p_studyId<=0) then
if (p_deltype='NA') then
FOR i IN (SELECT pk_nwsites  FROM ER_NWSITES START WITH pk_nwsites = p_netId CONNECT BY PRIOR pk_nwsites = FK_NWSITES order by pk_nwsites desc)
LOOP
delete from sch_docs where PK_DOCS in(select MD_MODELEMENTPK   from er_moredetails where (FK_MODPK=i.pk_nwsites or FK_MODPK in (select pk_nwusers from er_nwusers where fk_nwsites=i.pk_nwsites) or FK_MODPK in (select fk_user from er_nwusers where  pk_nwusers in ( select pk_nwusers from er_nwusers where fk_nwsites=i.pk_nwsites )) or FK_MODPK in (select fk_site from ER_NWSITES where pk_nwsites=i.pk_nwsites)) and (MD_MODNAME='Org_doc' or MD_MODNAME='Network_User_doc' or MD_MODNAME='Org_level_doc' or MD_MODNAME='User_doc'));
update er_moredetails set LAST_MODIFIED_BY = p_userId where fk_modpk = i.pk_nwsites;
delete from er_moredetails where fk_modpk = i.pk_nwsites or fk_modpk  in (select fk_site from ER_NWSITES where pk_nwsites=i.pk_nwsites)  ;

FOR j IN (select pk_nwusers from er_nwusers where fk_nwsites= i.pk_nwsites order by pk_nwusers desc)
LOOP
delete from er_moredetails where fk_modpk=j.pk_nwusers or fk_modpk in (select fk_user from er_nwusers where pk_nwusers =j.pk_nwusers);
END LOOP;
delete from er_nwusers where fk_nwsites= i.pk_nwsites;
update er_nwsites set LAST_MODIFIED_BY = p_userId where pk_nwsites=i.pk_nwsites;
delete from er_nwsites where pk_nwsites=i.pk_nwsites;
COMMIT;
END LOOP;
else
delete from sch_docs where PK_DOCS in(select MD_MODELEMENTPK   from er_moredetails where (FK_MODPK=p_netId or FK_MODPK in (select fk_user from er_nwusers where pk_nwusers = p_netId )) and ( MD_MODNAME='Network_User_doc' or MD_MODNAME='User_doc'));
delete from er_moredetails where fk_modpk=p_netId or FK_MODPK in (select fk_user from er_nwusers where pk_nwusers = p_netId );
update er_nwusers set LAST_MODIFIED_BY = p_userId where PK_NWUSERS=p_netId ;
delete from er_status_history where STATUS_MODPK = p_netId and STATUS_MODTABLE='er_nwusers';
delete  from  er_nwusers where PK_NWUSERS=p_netId;
delete  from  er_nwusers where PK_NWUSERS=p_netId;
COMMIT;
END If;
else
FOR i IN (SELECT pk_nwsites  FROM ER_NWSITES START WITH pk_nwsites = p_netId CONNECT BY PRIOR pk_nwsites = FK_NWSITES order by pk_nwsites desc)
LOOP
 Delete from er_status_history where STATUS_MODPK = i.pk_nwsites and STATUS_PARENTMODPK = p_studyId;
END LOOP;
END IF;
END;
/


INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,7,'07_sp_deletenetwork.sql',sysdate,'v11 #789');

	commit;
