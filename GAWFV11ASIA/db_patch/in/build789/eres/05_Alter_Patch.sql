DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'ER_SITE'
		AND COLUMN_NAME = 'CTEP_ID';
    
  IF(v_column_exists = 0) THEN
    execute immediate 'alter table er_site add CTEP_ID varchar2(20 BYTE)';
  END IF;
Commit;  
END;
/
INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,5,'05_Alter_Patch.sql',sysdate,'v11 #789');

	commit;
