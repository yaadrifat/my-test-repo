DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_ER_FORMQUERYSTATUS_FK' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_ER_FORMQUERYSTATUS_FK ON ERES.ER_FORMQUERYSTATUS
      (FK_FORMQUERY,FK_CODELST_QUERYSTATUS)
       LOGGING
       NOPARALLEL';
  	dbms_output.put_line ('Index created IDX_ER_FORMQUERYSTATUS_FK');
	ELSE
		dbms_output.put_line ('Index not created IDX_ER_FORMQUERYSTATUS_FK');
	END IF;
  
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_ER_FORMQUERY_FK' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_ER_FORMQUERY_FK ON ERES.ER_FORMQUERY
      (FK_FIELD, CREATOR)
       LOGGING
       NOPARALLEL';
  	dbms_output.put_line ('Index created IDX_ER_FORMQUERYSTATUS_FK');
	ELSE
		dbms_output.put_line ('Index not created IDX_ER_FORMQUERYSTATUS_FK');
	END IF;

  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_ER_PATFORMS_RECORD_TYPE' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_ER_PATFORMS_RECORD_TYPE ON ERES.ER_PATFORMS
      (RECORD_TYPE)
       LOGGING
       NOPARALLEL';
  	dbms_output.put_line ('Index created IDX_ER_PATFORMS_RECORD_TYPE');
	ELSE
		dbms_output.put_line ('Index not created IDX_ER_PATFORMS_RECORD_TYPE');
	END IF;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,341,1,'01_indexes.sql',sysdate,'v9.3.0 #742');

commit;
