DECLARE
v_count_exist number;
BEGIN
select count(*) into v_count_exist from er_repxsl where fk_report=(select pk_report from er_report where rep_name='Routine Care Events (Per Study)')
and repxsl_name='Routine Care Events (Per Study)';
IF v_count_exist > 0 then
delete from er_repxsl where fk_report=(select pk_report from er_report where rep_name='Routine Care Events (Per Study)')
and repxsl_name='Routine Care Events (Per Study)';
END IF;
COMMIT;
END;

/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,410,1,'01_delete_repxsl_272.sql',sysdate,'v11 #811');

commit;