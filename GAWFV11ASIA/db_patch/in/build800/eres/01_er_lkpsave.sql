SET DEFINE OFF;

DECLARE
  V_PK_LKPLIB NUMBER :=0;
  v_cnt       NUMBER :=0;
  v_LKPCOL NUMBER :=0;
  v_FK_LKPVIEW NUMBER :=0;
  v_max_cnt NUMBER :=0;
  v_seq NUMBER :=0;
BEGIN
 BEGIN
 SELECT PK_LKPLIB INTO V_PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI';

 exception when NO_DATA_FOUND then
      V_PK_LKPLIB := 0;
 end; 
    DELETE FROM er_lkpdata WHERE FK_LKPLIB in (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' );
	DELETE FROM ER_LKPVIEWCOL WHERE FK_LKPVIEW in (select PK_LKPVIEW from  ER_LKPVIEW WHERE FK_LKPLIB in (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' ));
	DELETE FROM ER_LKPCOL where FK_LKPLIB in (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' );
	DELETE FROM ER_LKPVIEW where PK_LKPVIEW in (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' );
	DELETE FROM ER_LKPLIB where PK_LKPLIB in (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' );
	commit;
	
	SELECT COUNT(*) INTO v_cnt FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI';
	SELECT SEQ_ER_LKPLIB.nextval INTO v_seq FROM dual;  
	IF (v_cnt=0) THEN
	Insert into ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_VERSION,LKPTYPE_DESC,LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) values (v_seq,'CTCAE','5.0','CTCAE v5.0','NCI',null,null);
	END IF;

	SELECT COUNT(*) INTO v_cnt FROM ER_LKPVIEW WHERE FK_LKPLIB=v_seq;

	IF (v_cnt=0) THEN
	Insert into ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,LKPVIEW_FILTER,LKPVIEW_KEYWORD) values (v_seq,'CTCAE v5.0',null,v_seq,'Grade','CTCAE',null,null,null,'CustLkp2000001');
	END IF;

	SELECT COUNT(*) INTO v_cnt FROM ER_LKPCOL WHERE FK_LKPLIB=v_seq;

	IF (v_cnt=0) THEN
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom001','CTCAE Term','varchar2',200,'ER_LKPDATA','adv_lkp_ctcaeterm');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom002','Grade','varchar2',500,'ER_LKPDATA','adv_lkp_grade');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom003','Grade Description','varchar2',500,'ER_LKPDATA','adv_lkp_gdesc');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom006','Definition','varchar2',500,'ER_LKPDATA','adv_lkp_defn');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom010','MedDra Code','varchar2',100,'ER_LKPDATA','adv_lkp_meddra');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom009','MedDRA SOC','varchar2',100,'ER_LKPDATA','adv_lkp_meddras');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom004','Short Name','varchar2',500,'ER_LKPDATA','adv_lkp_tshort');
	Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'custom007','Navigational Note','varchar2',500,'ER_LKPDATA','adv_lkp_navn');
	END IF;

	SELECT COUNT(*) INTO v_cnt FROM ER_LKPVIEWCOL WHERE FK_LKPVIEW in (select PK_LKPVIEW from  ER_LKPVIEW WHERE FK_LKPLIB=v_seq);

	IF (v_cnt=0) THEN
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_tshort' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_navn' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_defn' and fk_lkplib=v_seq),'Y',9,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_grade' and fk_lkplib=v_seq),'Y',4,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_gdesc' and fk_lkplib=v_seq),'Y',4,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_ctcaeterm' and fk_lkplib=v_seq),'Y',3,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_meddras' and fk_lkplib=v_seq),'Y',2,v_seq,'10%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='adv_lkp_meddra' and fk_lkplib=v_seq),'Y',1,v_seq,'10%','Y');
	END IF;

	commit; 
    SELECT PK_LKPLIB into v_LKPCOL FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI' ;
	if v_LKPCOL>0 then
	update er_study set STUDY_ADVLKP_VER = (SELECT PK_LKPLIB FROM ER_LKPLIB WHERE LKPTYPE_NAME='CTCAE' and LKPTYPE_TYPE='NCI')
where STUDY_ADVLKP_VER = V_PK_LKPLIB;
end if;
END;
/
commit;

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,399,1,'01_er_lkpsave.sql',sysdate,'v11 #800');

	commit; 
