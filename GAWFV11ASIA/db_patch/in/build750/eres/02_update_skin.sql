set define off;
update er_user 
set fk_codelst_skin=(select pk_codelst from er_codelst where codelst_type='skin' and codelst_subtyp='vel_default') where fk_codelst_skin is not null;
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,2,'02_update_skin.sql',sysdate,'v10 #750');

commit;