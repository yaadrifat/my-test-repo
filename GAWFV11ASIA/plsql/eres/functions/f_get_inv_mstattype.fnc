CREATE OR REPLACE FUNCTION        "F_GET_INV_MSTATTYPE" (V_VALUE CHAR)
RETURN VARCHAR2 AS V_RETVAL VARCHAR2(50);
BEGIN
  CASE V_VALUE
    WHEN 'A' THEN RETURN 'All Receivables';
    WHEN 'OR' THEN RETURN 'Outstanding receivables';
    WHEN 'UI' THEN RETURN 'Un-invoiced receivables';
    WHEN 'P' THEN RETURN 'Patient';
    WHEN 'M' THEN RETURN 'Milestone';
    WHEN 'I' THEN RETURN 'Invoice';
    WHEN 'B' THEN RETURN 'Budget';
    WHEN 'u' THEN RETURN 'User';
    ELSE RETURN NULL;
  END CASE;
END;
/


