CREATE OR REPLACE FUNCTION        "F_GET_SCHPROTOCOL" (p_pk_sch_events1 NUMBER ) RETURN VARCHAR IS

/*Function to get the calendar name linked with a schedule record
*Author: Sonia Abrol, on 08/22/06
*/
v_cal_name VARCHAR2(100);

BEGIN

	 IF p_pk_sch_events1 > 0 THEN

		BEGIN

		 	SELECT NAME
			INTO v_cal_name
			FROM event_assoc WHERE LPAD(event_id ,10,0) IN (SELECT session_id FROM sch_events1 WHERE LPAD(event_id,10,0) = p_pk_sch_events1);

		EXCEPTION WHEN NO_DATA_FOUND THEN

			v_cal_name := '';

		END;

	ELSE
		v_cal_name := '';
	END IF;

	RETURN v_cal_name;
END   ;
/


