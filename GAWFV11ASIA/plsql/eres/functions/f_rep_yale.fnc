CREATE OR REPLACE FUNCTION        "F_REP_YALE" (p_pk_person NUMBER,p_param4 VARCHAR, p_schdate DATE)
RETURN NUMBER
IS
v_count NUMBER;
BEGIN

IF p_param4 = '1 Month' THEN
 SELECT COUNT(*) INTO v_count
 FROM ER_FORMSLINEAR
 WHERE fk_form = 434 AND ID=p_pk_person AND
 DECODE(INSTR(col2,'[VELSEP1]'),0,col2,SUBSTR(col2,1,INSTR(col2,'[VELSEP1]')-1))='Visit Completed' AND
 (SELECT COUNT(*) FROM ER_FORMSLINEAR WHERE fk_form = 434 AND ID=p_pk_person) > 0 ;
END IF; 
 IF p_param4 = '6 Month' THEN
 SELECT COUNT(*) INTO v_count
 FROM ER_FORMSLINEAR
 WHERE fk_form = 588 AND ID=p_pk_person AND
 DECODE(INSTR(col2,'[VELSEP1]'),0,col2,SUBSTR(col2,1,INSTR(col2,'[VELSEP1]')-1))='Visit Completed' AND
 (SELECT COUNT(*) FROM ER_FORMSLINEAR WHERE fk_form = 588 AND ID=p_pk_person) > 0; 
 
-- and
-- to_date(col3,'mm/dd/yyyy') >= p_schdate - 10;
END IF;

IF v_count > 0 THEN
   RETURN v_count;
END IF;

--if p_param4 = '1 Month' then
 SELECT COUNT(*) INTO v_count
 FROM ER_FORMSLINEAR
 WHERE fk_form = 435 AND ID=p_pk_person AND
 DECODE(INSTR(col7,'[VELSEP1]'),0,col7,SUBSTR(col7,1,INSTR(col7,'[VELSEP1]')-1)) IN ('Interview Completed','Mailed Interview Received','Too ill','Deceased','Refused','Lost to Follow-up') AND
 DECODE(INSTR(col4,'[VELSEP1]'),0,col4,SUBSTR(col4,1,INSTR(col4,'[VELSEP1]')-1))=p_param4 AND ID=p_pk_person ;
--else
-- select count(*) into v_count
-- from er_formslinear
-- where fk_form = 435 and id=p_pk_person and
-- decode(instr(col7,'[VELSEP1]'),0,col7,substr(col7,1,instr(col7,'[VELSEP1]')-1)) in ('Interview Completed','Mailed Interview Received','Too ill','Deceased','Refused','Lost to Follow-up') and
-- decode(instr(col4,'[VELSEP1]'),0,col4,substr(col4,1,instr(col4,'[VELSEP1]')-1))=p_param4 and id=p_pk_person; 
 
-- and
-- to_date(col1,'mm/dd/yyyy') >= p_schdate - 10;
--end if;

RETURN v_count;
END ;
/


CREATE SYNONYM ESCH.F_REP_YALE FOR F_REP_YALE;


CREATE SYNONYM EPAT.F_REP_YALE FOR F_REP_YALE;


GRANT EXECUTE, DEBUG ON F_REP_YALE TO EPAT;

GRANT EXECUTE, DEBUG ON F_REP_YALE TO ESCH;

