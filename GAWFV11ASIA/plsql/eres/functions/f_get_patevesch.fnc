CREATE OR REPLACE FUNCTION      "F_GET_PATEVESCH" (p_date DATE, p_pat VARCHAR,p_study NUMBER)
RETURN VARCHAR
IS
v_ret VARCHAR2(4000);
v_pat VARCHAR2(20);
 v_visit  VARCHAR2(50);
 v_event VARCHAR2(1000);
v_pat1 VARCHAR2(20);
v_counter NUMBER := 0;
v_sql VARCHAR2(1000);
 v_cur Gk_Cv_Types.GenericCursorType;
 v_visit_old VARCHAR2(50);
BEGIN
	 v_ret := TO_CHAR(p_date,'dd') || 'X@*@XX@@@XX@@@X';
	 v_sql := 'SELECT PATPROT_PATSTDID AS pat_studyid,(select visit_name from sch_protocol_visit where pk_protocol_visit = fk_visit) as visit,description AS event_name FROM sch_events1 z, person, ER_PATPROT WHERE ACTUAL_SCHDATE = to_date(''' ||p_date || ''')  AND pk_person IN (' || p_pat ||') AND isconfirmed = (SELECT pk_codelst FROM sch_codelst WHERE codelst_type=''eventstatus'' AND codelst_subtyp=''ev_notdone'') AND pk_patprot = fk_patprot AND PATIENT_ID = PK_PERSON AND patprot_stat=1  and er_patprot.fk_study=' || p_study
	 || ' order by PATPROT_PATSTDID , fk_patprot, fk_visit,z.event_sequence';
 OPEN v_cur FOR v_sql;
		 LOOP
	  	 FETCH v_cur INTO  v_pat, v_visit, v_event  ;
		 EXIT WHEN v_cur %NOTFOUND;
 	 	 v_counter := v_counter + 1;
		 IF v_counter = 1 THEN
		  	 v_pat1 :=v_pat;
			 v_visit_old := v_visit;

			 v_ret := v_ret || 'Pt. Study # ' || v_pat || 'X@*@X X@@@X Visit: ' || v_visit || 'X@@@XEvent(s):X@@@X';
		 END IF;

		 IF v_pat1 = v_pat THEN
		 	IF v_visit_old <> v_visit THEN
			   v_ret := v_ret || 'X@*@XX@@@X  Visit: ' || v_visit  || 'X@@@XEvent(s):X@@@X';
			END IF;

	 	 	v_ret := v_ret || '-  ' || v_event || 'X@@@X';
		 ELSE
	 	 	v_ret := v_ret || 'X@@@XPt. Study # ' || v_pat || 'X@*@XX@@@X  Visit: ' || v_visit || 'X@@@XEvent(s):X@@@X-  ' || v_event || 'X@@@X' ;
		  	v_pat1 := v_pat;
		 END IF;

		  v_visit_old := v_visit;

		 END LOOP;
	 CLOSE v_cur ;
  	 RETURN v_ret ;
END ;
/


CREATE SYNONYM ESCH.F_GET_PATEVESCH FOR F_GET_PATEVESCH;


CREATE SYNONYM EPAT.F_GET_PATEVESCH FOR F_GET_PATEVESCH;


GRANT EXECUTE, DEBUG ON F_GET_PATEVESCH TO EPAT;

GRANT EXECUTE, DEBUG ON F_GET_PATEVESCH TO ESCH;

