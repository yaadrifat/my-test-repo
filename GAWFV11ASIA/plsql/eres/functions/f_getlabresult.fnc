CREATE OR REPLACE FUNCTION        "F_GETLABRESULT" (p_testname VARCHAR, p_testdate DATE, p_per NUMBER, p_study NUMBER)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(1000);
v_counter NUMBER := 0 ;
BEGIN
  FOR i IN (SELECT test_result || ' ' || test_unit AS test_result FROM er_patlabs,er_labtest WHERE pk_labtest = fk_testid AND LOWER(labtest_shortname) = p_testname AND fk_per = p_per AND fk_study = p_study AND test_date = p_testdate)
  LOOP
  	  v_counter := v_counter + 1;
	  IF v_counter > 1 THEN
	  	 v_retvalues := v_retvalues || 'X@@@X';
	  END IF;
  	  v_retvalues := v_retvalues || i.test_result;
  END LOOP;

  RETURN v_retvalues ;
END ;
/


CREATE SYNONYM ESCH.F_GETLABRESULT FOR F_GETLABRESULT;


CREATE SYNONYM EPAT.F_GETLABRESULT FOR F_GETLABRESULT;


GRANT EXECUTE, DEBUG ON F_GETLABRESULT TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETLABRESULT TO ESCH;

