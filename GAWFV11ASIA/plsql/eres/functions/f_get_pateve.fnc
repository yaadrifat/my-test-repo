CREATE OR REPLACE FUNCTION      "F_GET_PATEVE" (p_date DATE, p_site VARCHAR,p_study NUMBER)
RETURN VARCHAR
IS
v_ret VARCHAR2(4000);
v_pat VARCHAR2(20);
--v_study_number VARCHAR2(20);
v_study_number VARCHAR2(100);
 v_visit  VARCHAR2(50);
 v_event VARCHAR2(1000);
v_pat1 VARCHAR2(20);
v_counter NUMBER := 0;
v_sql VARCHAR2(1000);
 v_cur Gk_Cv_Types.GenericCursorType;
 v_visit_old VARCHAR2(50);
BEGIN
	 v_ret := TO_CHAR(p_date,'dd') || 'X@*@XX@@@XX@@@X';
	 IF p_study = 0  THEN
	 			v_sql := 'SELECT study_number,PATPROT_PATSTDID AS pat_studyid,(select visit_name from sch_protocol_visit where pk_protocol_visit = fk_visit) as visit,description AS event_name FROM sch_events1, person, ER_PATPROT z ,ER_STUDY WHERE ACTUAL_SCHDATE = TO_DATE(''' ||p_date || ''')  AND fk_site IN (' || p_site||') AND isconfirmed = (SELECT pk_codelst FROM sch_codelst WHERE codelst_type=''eventstatus'' AND codelst_subtyp=''ev_notdone'') AND pk_patprot = fk_patprot AND PATIENT_ID = PK_PERSON AND patprot_stat=1  AND pk_study = z.fk_study order by PATPROT_PATSTDID , fk_patprot, fk_visit,sch_events1.event_sequence';
	 ELSE
	 			v_sql := 'SELECT study_number,PATPROT_PATSTDID AS pat_studyid,(select visit_name from sch_protocol_visit where pk_protocol_visit = fk_visit) as visit,description AS event_name FROM sch_events1, person, ER_PATPROT z,ER_STUDY WHERE ACTUAL_SCHDATE = TO_DATE(''' ||p_date || ''')  AND fk_site IN (' || p_site||') AND isconfirmed = (SELECT pk_codelst FROM sch_codelst WHERE codelst_type=''eventstatus'' AND codelst_subtyp=''ev_notdone'') AND pk_patprot = fk_patprot AND PATIENT_ID = PK_PERSON AND patprot_stat=1  AND pk_study = z.fk_study AND z.fk_study=' || p_study || ' order by PATPROT_PATSTDID , fk_patprot, fk_visit,sch_events1.event_sequence' ;
	END IF;
 OPEN v_cur FOR v_sql;
		 LOOP
	  	 FETCH v_cur INTO  v_study_number,v_pat, v_visit, v_event  ;
		 EXIT WHEN v_cur %NOTFOUND;
 	 	 v_counter := v_counter + 1;
		 IF v_counter = 1 THEN
		  	 v_pat1 :=v_pat;
			  v_visit_old := v_visit;

			 v_ret := v_ret || 'Study # ' || v_study_number || 'X@*@X X@@@XPt. Study # ' || v_pat || 'X@*@X X@@@X Visit: ' || v_visit || 'X@@@XEvent(s):X@@@X';
		 END IF;
		 IF v_pat1 = v_pat THEN

		 	IF v_visit_old <> v_visit THEN
			   v_ret := v_ret || 'X@*@XX@@@X  Visit: ' || v_visit  || 'X@@@XEvent(s):X@@@X';
			END IF;

	 	 	v_ret := v_ret || '-  ' || v_event || 'X@@@X';
		 ELSE
	 	 	v_ret := v_ret || 'X@@@XStudy # ' || v_study_number || 'X@*@XX@@@XPt. Study # ' || v_pat || 'X@*@XX@@@X  Visit: ' || v_visit || 'X@@@XEvent(s):X@@@X-  ' || v_event || 'X@@@X' ;
		  	v_pat1 := v_pat;
		 END IF;

		  v_visit_old := v_visit;

		 END LOOP;
	 CLOSE v_cur ;
  	 RETURN v_ret ;
END ;
/


CREATE SYNONYM ESCH.F_GET_PATEVE FOR F_GET_PATEVE;


CREATE SYNONYM EPAT.F_GET_PATEVE FOR F_GET_PATEVE;


GRANT EXECUTE, DEBUG ON F_GET_PATEVE TO EPAT;

GRANT EXECUTE, DEBUG ON F_GET_PATEVE TO ESCH;

