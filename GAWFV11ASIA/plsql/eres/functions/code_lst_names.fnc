CREATE OR REPLACE FUNCTION        "CODE_LST_NAMES" (p_id varchar2)
return varchar
is
   V_STR      varchar2 (2001) default p_id || ',';
   V_SQLSTR   varchar2 (4000);
   V_PARAMS   varchar2 (2001) default p_id || ',';
   V_POS      number         := 0;
   V_CNT      number         := 0;
   v_namelst varchar2(2000) ;
   v_name varchar2(70) ;
Begin
   LOOP
       V_CNT := V_CNT + 1;
       V_POS := instr (V_STR, ',');
       V_PARAMS := substr (V_STR, 1, V_POS - 1);
        exit when V_PARAMS Is null;
        select codelst_desc
          into v_name
         from er_codelst
        where pk_codelst = v_params ;
        if v_namelst is null then
          v_namelst :=  v_name ;
        else
          v_namelst :=  v_namelst ||';' || v_name  ;
        end if ;
      V_STR := substr (V_STR, V_POS + 1);
   dbms_output.put_line( 'v_namelst = '  || v_namelst ) ;
   END LOOP ;
   dbms_output.put_line( to_char(length(v_namelst))) ;
  return v_namelst ;
end ;
/


CREATE SYNONYM ESCH.CODE_LST_NAMES FOR CODE_LST_NAMES;


CREATE SYNONYM EPAT.CODE_LST_NAMES FOR CODE_LST_NAMES;


GRANT EXECUTE, DEBUG ON CODE_LST_NAMES TO EPAT;

GRANT EXECUTE, DEBUG ON CODE_LST_NAMES TO ESCH;

