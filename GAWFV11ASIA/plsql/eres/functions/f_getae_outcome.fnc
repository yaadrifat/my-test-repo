CREATE OR REPLACE FUNCTION        "F_GETAE_OUTCOME" (p_adveve NUMBER, p_ae_outdate VARCHAR2)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(4000);
v_counter NUMBER := 0 ;
BEGIN
  FOR i IN (SELECT advinfo_value,codelst_desc, codelst_subtyp  FROM sch_advinfo, sch_codelst WHERE fk_adverse = p_adveve AND pk_codelst = fk_codelst_info  AND fk_codelst_info IN (SELECT pk_codelst FROM sch_codelst WHERE codelst_type = 'outcome') ORDER BY codelst_seq)
  LOOP
  	  IF i.advinfo_value = 1 THEN
	  	  v_counter := v_counter + 1;
		  IF v_counter > 1 THEN
		  	 v_retvalues := v_retvalues || 'X@@@X';
		  END IF;
	  	  v_retvalues := v_retvalues || i.codelst_desc ;
		  IF i.codelst_subtyp = 'al_death' THEN
		  	  v_retvalues := v_retvalues || ' - ' || p_ae_outdate ;
		  END IF;
	  END IF;
  END LOOP;
  RETURN v_retvalues ;
END ;
/


CREATE SYNONYM ESCH.F_GETAE_OUTCOME FOR F_GETAE_OUTCOME;


CREATE SYNONYM EPAT.F_GETAE_OUTCOME FOR F_GETAE_OUTCOME;


GRANT EXECUTE, DEBUG ON F_GETAE_OUTCOME TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETAE_OUTCOME TO ESCH;

