CREATE OR REPLACE FUNCTION        "F_GETAE_OUTCOME_ADD_INFO" (p_adveve NUMBER, p_ae_outdate DATE, p_codelst_type VARCHAR2,p_ae_outnotes VARCHAR2)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(4000) default null;

BEGIN

  FOR i IN (SELECT advinfo_value,(select codelst_desc from sch_codelst where pk_codelst = fk_codelst_info) as codelst_desc,(select codelst_subtyp from sch_codelst where pk_codelst = fk_codelst_info) as codelst_subtyp
     FROM sch_advinfo, sch_codelst
   WHERE fk_adverse = p_adveve AND
   fk_codelst_info = pk_codelst and
   trim(codelst_type) =p_codelst_type and
   advinfo_value = 1)
  LOOP
    IF i.codelst_subtyp = 'al_death' THEN
          v_retvalues := v_retvalues || '[' || i.codelst_desc || ' - ' || TO_CHAR(p_ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT) || '],';
         ELSE
          v_retvalues := v_retvalues || '[' || i.codelst_desc || '],';
    END IF;
  END LOOP;
  if v_retvalues is null then
      return '';
  else
    SELECT substr(v_retvalues,1,length(v_retvalues)-1) || nvl2(p_ae_outnotes,', [Notes: ' || p_ae_outnotes || ']','') INTO v_retvalues FROM dual;
    return v_retvalues;
  end if;
END ;
/


CREATE SYNONYM ESCH.F_GETAE_OUTCOME_ADD_INFO FOR F_GETAE_OUTCOME_ADD_INFO;


CREATE SYNONYM EPAT.F_GETAE_OUTCOME_ADD_INFO FOR F_GETAE_OUTCOME_ADD_INFO;


GRANT EXECUTE, DEBUG ON F_GETAE_OUTCOME_ADD_INFO TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETAE_OUTCOME_ADD_INFO TO ESCH;

