CREATE OR REPLACE FUNCTION        "F_GENCSV_VAL" (p_flddatatype VARCHAR2, p_mapcolname VARCHAR2,p_dispordataval NUMBER,p_delimiter VARCHAR2)
RETURN VARCHAR2
-- p_dispordataval 1 - Display value 2 - Data value
IS
v_retval VARCHAR2(4000);
BEGIN
	CASE p_flddatatype
	WHEN 'ET' THEN 	v_retval := '''"'' || ' || p_mapcolname || '||''"''' || '||''' || p_delimiter ||'''||';
	WHEN 'ED' THEN v_retval := p_mapcolname || '||''' || p_delimiter ||'''||';
	WHEN 'EN' THEN v_retval := p_mapcolname || '||''' || p_delimiter ||'''||';
	WHEN 'MD' THEN
		 IF p_dispordataval = 1 THEN
--		 	v_retval := SUBSTR(p_mapcolname ,1,INSTR(p_mapcolname,'[VELSEP1]')-1) || '''' || p_delimiter ||'''||';
		 	v_retval := SUBSTR(p_mapcolname ,1,INSTR(p_mapcolname,'[VELSEP1]')-1) ;
		 ELSE
--		 	 v_retval := SUBSTR(p_mapcolname,INSTR(p_mapcolname,'[VELSEP1]')+9) || '||''' || p_delimiter ||'''||';
		 	 v_retval :=SUBSTR(p_mapcolname,INSTR(p_mapcolname,'[VELSEP1]')+9) ;
		 END IF;
	WHEN 'MR' THEN
		 IF p_dispordataval = 1 THEN
--		 	v_retval := SUBSTR(p_mapcolname,1,INSTR( p_mapcolname,'[VELSEP1]')-1) || '||''' || p_delimiter ||'''||';
		 	v_retval := SUBSTR(p_mapcolname,1,INSTR( p_mapcolname,'[VELSEP1]')-1) ;
		 ELSE
--		 	 v_retval := SUBSTR(p_mapcolname,INSTR( p_mapcolname,'[VELSEP1]')+9) || '||''' || p_delimiter ||'''||';
		 	 v_retval := SUBSTR(p_mapcolname,INSTR( p_mapcolname,'[VELSEP1]')+9) ;
		 END IF;
	WHEN 'MC' THEN
		 IF p_dispordataval = 1 THEN
--		 	v_retval := '''"''' || SUBSTR( p_mapcolname,1,INSTR(p_mapcolname ,'[VELSEP1]')-1) || '||''"''' || '||''' || p_delimiter ||'''||';
		 	v_retval := '"' || SUBSTR( p_mapcolname,1,INSTR(p_mapcolname ,'[VELSEP1]')-1) || '"' ;
		 ELSE
--		 	v_retval :=  '''"''' || REPLACE(SUBSTR(REPLACE(p_mapcolname ,'[VELSEP1],','[VELSEP1]'),INSTR(REPLACE(p_mapcolname ,'[VELSEP1],','[VELSEP1]'),'[VELSEP1]')+9),'[VELCOMMA]','') || '||''"''' || '||''' || p_delimiter ||'''||';
		 	v_retval :=  '"' || REPLACE(SUBSTR(REPLACE(p_mapcolname ,'[VELSEP1],','[VELSEP1]'),INSTR(REPLACE(p_mapcolname ,'[VELSEP1],','[VELSEP1]'),'[VELSEP1]')+9),'[VELCOMMA]','') || '"' ;
		 END IF;
	END CASE;

 RETURN v_retval ;
END ;
/


