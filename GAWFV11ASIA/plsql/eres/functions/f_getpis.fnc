CREATE OR REPLACE FUNCTION        "F_GETPIS" (p_study NUMBER)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(4000);
BEGIN
  FOR i IN (SELECT ' * ' || usr_firstname || ' ' || usr_lastname AS pi,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_siteid) AS site_name , 0 AS seq
FROM ER_STUDY, ER_USER
WHERE pk_study = p_study AND pk_user = study_prinv
UNION
SELECT usr_firstname || ' ' || usr_lastname AS pi,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_siteid) AS site_name, 1 AS seq
 FROM ER_STUDYTEAM, ER_USER
WHERE fk_study =  p_study  AND fk_codelst_tmrole = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'role' AND codelst_subtyp = 'role_prin')
AND Pk_user = fk_user AND fk_user NOT IN (SELECT study_prinv FROM ER_STUDY WHERE pk_study = p_study)
ORDER BY seq)
  LOOP
  	  v_retvalues := v_retvalues || i.pi || ' (' || i.site_name || ')X@@@X' ;
  END LOOP;
  v_retvalues := SUBSTR(v_retvalues,1,LENGTH(v_retvalues)-5);
  RETURN v_retvalues ;
END ;
/


CREATE SYNONYM ESCH.F_GETPIS FOR F_GETPIS;


CREATE SYNONYM EPAT.F_GETPIS FOR F_GETPIS;


GRANT EXECUTE, DEBUG ON F_GETPIS TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETPIS TO ESCH;

