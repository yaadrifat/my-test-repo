SELECT pk_bgtcal, LINEITEM_NAME,
BUDGET_NAME,
BGTCAL_PROTTYPE,
PROT_CALENDAR,
BUDGET_VERSION,
BUDGET_DESC,
STUDY_NUMBER,
STUDY_TITLE,
SITE_NAME,
CATEGORY,
BGTSECTION_NAME,
CPT_CODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
NVL(TOTAL_COST,0) AS TOTAL_COST,
DECODE(LINE_ITEM_INDIRECTS_FLAG,100,'Yes','') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,100,'Yes','') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,'Discount',2,'Markup','') AS BUDGET_DISCOUNT_FLAG,
TOTAL_COST_AFTER,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
TOTAL_COST_DISCOUNT,
PERPAT_INDIRECT,
TOTAL_COST_INDIRECT,
BUDGET_CURRENCY,
UNIT_COST,
NUMBER_OF_UNIT,
case when BGTSECTION_PATNO is null then '1' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
DECODE(COST_CUSTOMCOL,'research','No','soc','Yes','Other') AS standard_of_care,
category_subtyp,
NVL(LINEITEM_SPONSORAMOUNT, 0) AS SPONSOR_AMOUNT,
LINEITEM_VARIANCE AS  L_VARIANCE,FK_CODELST_COST_TYPE,COST_TYPE_DESC,
lineitem_direct_perpat,total_cost_per_pat, total_cost_all_pat,pk_budgetsec,nvl(budgetsec_fkvisit,0)budgetsec_fkvisit,pk_lineitem,lineitem_inpersec,
nvl(bgtcal_excldsocflag,0) as bgtcal_excldsocflag,budgetsection_sequence,lineitem_seq ,lower(LINEITEM_NAME) as l_LINEITEM_NAME,BGTSECTION_TYPE,
SUBCOST_ITEM_FLAG,
NVL(FK_EVENT, 0) as FK_EVENT
FROM erv_budget
WHERE pk_budget = ~1
AND pk_bgtcal in (select pk_bgtcal from sch_bgtcal where fk_budget = ~1)
and (PROT_CALENDAR is not null and trim(PROT_CALENDAR) != 'No Calendar')
union
SELECT s.fk_bgtcal AS pk_bgtcal, ' ' as LINEITEM_NAME, NULL, NULL, (select distinct PROT_CALENDAR from erv_budget where pk_bgtcal = s.fk_bgtcal) as PROT_CALENDAR ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,BGTSECTION_NAME,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,0 ,NULL ,NULL ,NULL ,NULL ,
case when BGTSECTION_PATNO is null then '1' else BGTSECTION_PATNO end AS BGTSECTION_PATNO,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,NULL ,
0,0 , 0 ,pk_budgetsec,0,NULL ,0 ,NULL,bgtsection_sequence as budgetsection_sequence ,NULL as lineitem_seq ,null as l_LINEITEM_NAME, BGTSECTION_TYPE,0,
0
from sch_bgtsection s where s.fk_bgtcal in (select pk_bgtcal from sch_bgtcal where fk_budget = ~1)
AND NVL (bgtsection_delflag, 'N') = 'N' AND not exists (select * from erv_budget v where pk_bgtcal in (select pk_bgtcal from sch_bgtcal where fk_budget = ~1)
and v.pk_budgetsec = s.pk_budgetsec )
ORDER BY pk_bgtcal,budgetsection_sequence,budgetsec_fkvisit,lineitem_seq,l_LINEITEM_NAME