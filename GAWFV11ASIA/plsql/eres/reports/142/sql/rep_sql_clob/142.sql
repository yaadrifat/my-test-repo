select stor.pk_storage,STORAGE_ID,STORAGE_NAME,
(SELECT p.STORAGE_ID FROM ER_STORAGE p WHERE p.pk_STORAGE = stor.fk_STORAGE) PSTORE_ID,
(SELECT count(*) FROM ER_STORAGE p WHERE p.fk_STORAGE = stor.pk_storage) StoreChildCnt,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,spec.pk_specimen,
SPEC_ID,(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
(SELECT study_number FROM ER_STUDY WHERE pk_study = FK_STUDY) STUDY_NUMBER,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
SPEC_ORIGINAL_QUANTITY || ' '||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY
from er_storage stor, er_specimen spec
where spec.fk_storage = stor.pk_storage
and ((':specTyp'='NonStudy' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or (':specTyp'='Study' AND spec.fk_study in (:studyId)))
AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND stor.pk_storage in (:storageId)
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
order by storage_id
