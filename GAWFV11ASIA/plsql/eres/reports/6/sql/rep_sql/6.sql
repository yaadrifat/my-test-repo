
select
       STUDY_NUMBER,
       STUDY_TITLE,
        to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
       TA,
       STUDY_AUTHOR
  from erv_study
 where fk_account = ~1
   and STUDY_PUBFLAG = 'Y'
   and created_on between '~2' and '~3'
 order by TA
