SELECT
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = fk_study) AS study_title,
(SELECT study_actualdt FROM ER_STUDY WHERE pk_study = fk_study) AS study_actualdt,
(SELECT study_samplsize FROM ER_STUDY WHERE pk_study = fk_study) AS study_samplsize,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
TO_CHAR(MIN(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolldate,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE fk_study = x.fk_study AND patstudystat_date < '~3' AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'patStatus' AND codelst_subtyp = 'enrolled')) AS total_count,
SUM(active) AS active_cnt,
SUM(offstudy) AS offstudy_cnt,
SUM(followup) AS followup_cnt,
SUM(offtreat) AS offtreat_cnt
FROM(
SELECT fk_study,fk_site,patstudystat_date,
DECODE(status,'enrolled',1,0) AS enrolled,
DECODE(status,'active',1,0) AS active,
DECODE(status,'offstudy',1,0) AS offstudy,
DECODE(status,'followup',1,0) AS followup,
DECODE(status,'offtreat',1,0) AS offtreat
FROM
(SELECT
fk_study,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) AS status,fk_site,patstudystat_date
FROM ER_PATSTUDYSTAT a, ER_PER
WHERE pk_per = fk_per AND
fk_study = ~1 AND
fk_site IN (SELECT fk_site FROM ER_STUDYSITES WHERE fk_study = ~1) AND
patstudystat_date BETWEEN '~2' AND '~3' AND
patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_study = a.fk_study AND fk_per = pk_per AND patstudystat_date BETWEEN '~2' AND '~3')
)
) x
GROUP BY fk_study,fk_site
