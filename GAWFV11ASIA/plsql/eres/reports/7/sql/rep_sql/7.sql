select  distinct c.USR_LASTNAME,
  c.USR_FIRSTNAME, er_study.pk_study,
  er_study.STUDY_TITLE, to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
  er_study.STUDY_NUMBER,   a.CODELST_DESC tarea,
  b.CODELST_DESC team_role, f.codelst_desc study_stat,
 d.site_name
 from  ER_STUDYTEAM,   er_user c,
  er_study,   er_codelst a,
  er_codelst b, er_codelst f,
  er_site d ,er_studystat e
 where  c.fk_siteid = ~1
 and  trunc(er_study.created_on) between '~2' and '~3'
 and  ER_STUDYTEAM.FK_USER = c.pk_user
 and c.fk_siteid = d.pk_site
 and  er_study.pk_study = ER_STUDYTEAM.FK_STUDY
 and  er_study.FK_CODELST_TAREA = a.pk_codelst
 and  ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst
and e.fk_study = er_study.pk_study
and e.FK_CODELST_STUDYSTAT = f.pk_codelst
and f.codelst_type = 'studystat' and e.STUDYSTAT_ENDT is null
 and study_actualdt is null
 and study_verparent is null
union
select  distinct c.USR_LASTNAME, c.USR_FIRSTNAME,
  er_study.pk_study,     er_study.STUDY_TITLE,
  to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
  er_study.STUDY_NUMBER,  a.CODELST_DESC tarea,
  b.CODELST_DESC team_role,
 f.codelst_desc  study_stat,
 d.site_name
 from  ER_STUDYTEAM,
  er_user c,  er_study,
  er_codelst a, er_codelst b,
er_codelst f,  er_site d ,
er_studystat e
 where  c.fk_siteid = ~1
 and  trunc(er_study.created_on) between '~2' and '~3'
 and  ER_STUDYTEAM.FK_USER = c.pk_user
 and c.fk_siteid = d.pk_site
 and  er_study.pk_study = ER_STUDYTEAM.FK_STUDY
 and  er_study.FK_CODELST_TAREA = a.pk_codelst
 and  ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst
and e.fk_study = er_study.pk_study
and e.FK_CODELST_STUDYSTAT = f.pk_codelst
and f.codelst_type = 'studystat' and e.STUDYSTAT_ENDT is null
 and study_actualdt is not null
 and study_verparent is null
 order by tarea, pk_study    
