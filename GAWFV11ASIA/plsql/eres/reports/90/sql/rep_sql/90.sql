select     protocolid,
PERSON_CODE , PAT_STUDYID , FK_PER , STUDY_NUMBER, SITE_NAME,
STUDY_TITLE ,to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt  ,
PROTOCOL_NAME, PATPROT_ENROLDT,
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
EVENT_NAME,MONTH,
to_char(EVENT_SCHDATE,'YYYY') Year ,
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,
EVENT_STATUS ,  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,
( select     max(codelst_desc)
from     ESCH.SCH_EVENTCOST A,
ESCH.SCH_CODELST B
where      A.FK_CURRENCY = B.PK_CODELST
and     A.fk_event = fk_assoc
) cost_currency,
to_char(ESCH.tot_cost(fk_assoc),'9999999999.99') evecost,
trim(to_char(EVENTCOST_VALUE,0999999999.99)) supply_cost,
b.codelst_desc supply_desc ,
FK_PATPROT
from     erv_patsch,
ESCH.sch_eventcost ,
ESCH.sch_codelst a,
ESCH.sch_codelst b
where protocolid = ~1
and EVENT_SCHDATE between '~2' and '~3'
and     event_status = 'Done'
and     fk_assoc = sch_eventcost.fk_event
and     FK_CURRENCY = a.pk_codelst
and     FK_COST_DESC = b.pk_codelst
and     FK_SITE in (~4)
Order by supply_desc, person_code,   EVENT_SCHDATE