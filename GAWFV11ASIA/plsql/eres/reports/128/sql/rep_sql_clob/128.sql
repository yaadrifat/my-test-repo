SELECT spec_id, spec_alternate_id,
spec_original_quantity AS qty,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_quantity_units) AS units,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_type AND codelst_type = 'specimen_type') AS sp_type,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study,
TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patid,
DECODE(fk_specimen,NULL,'' ,'Yes') AS child_yes,
(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = a.fk_specimen) AS parent,
storage_name, storage_id,
(SELECT storage_name FROM ER_STORAGE WHERE b.fk_storage = pk_storage) AS storage_loc
FROM ER_SPECIMEN a, ER_STORAGE b
WHERE pk_storage (+) = a.fk_storage
AND ((':specTyp'='NonStudy' AND fk_study is null AND a.fk_account = :sessAccId) or (':specTyp'='Study' AND fk_study in (:studyId)))
AND (a.fk_per is not null AND exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
AND usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
order by patid, study, spec_id
