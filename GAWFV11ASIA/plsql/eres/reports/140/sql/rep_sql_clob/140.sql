select s.pk_storage PPK_Storage, s.Storage_id PStorage_id, s.storage_name PStorage_name,
s.STORAGE_ALTERNALEID PAlt_ID,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.STORAGE_UNIT_CLASS) PUnit_class,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.FK_CODELST_STORAGE_TYPE) PStorage_type,
'Occupied' PStatus, pkg_inventory.f_get_storageSpecCount(s.pk_storage)specCnt,
pkg_inventory.f_get_storagecapacity(s.pk_storage)+1 capacity
from er_storage s, er_storage_status ss
WHERE s.pk_storage = ss.fk_storage
AND ss.FK_CODELST_STORAGE_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp='Occupied')
AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1
WHERE ss1.FK_STORAGE = s.PK_STORAGE
AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) from er_storage_status ss2
where ss2.FK_STORAGE = s.PK_STORAGE))
AND pkg_inventory.f_get_storageSpecCount(s.pk_storage)<> pkg_inventory.f_get_storagecapacity(s.pk_storage)+1
and s.pk_storage in (:storageId)
order by s.storage_name,s.Storage_id
