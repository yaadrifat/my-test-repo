CREATE OR REPLACE TRIGGER "ER_FLDACTIONINFO_AU0" AFTER UPDATE OF
PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE ON ER_FLDACTIONINFO REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu FOR Audit Data After Update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction (raid, 'ER_FLDACTIONINFO', :old.rid, 'U', usr);
   if nvl(:old.PK_FLDACTIONINFO,0) !=
      NVL(:new.PK_FLDACTIONINFO,0) then
      audit_trail.column_update
       (raid, 'PK_FLDACTIONINFO',
       :old.PK_FLDACTIONINFO, :new.PK_FLDACTIONINFO);
   end if;

   if nvl(:old.FK_FLDACTION,0) !=
      NVL(:new.FK_FLDACTION,0) then
      audit_trail.column_update
       (raid, 'FK_FLDACTION',
       :old.FK_FLDACTION, :new.FK_FLDACTION);
   end if;

   if nvl(:old.INFO_TYPE,' ') !=
      NVL(:new.INFO_TYPE,' ') then
      audit_trail.column_update
       (raid, 'INFO_TYPE',
       :old.INFO_TYPE, :new.INFO_TYPE);
   end if;

   if nvl(:old.INFO_VALUE,' ') !=
      NVL(:new.INFO_VALUE,' ') then
      audit_trail.column_update
       (raid, 'INFO_VALUE',
       :old.INFO_VALUE, :new.INFO_VALUE);
   end if;

   if nvl(:old.RID,0) !=
      NVL(:new.RID,0) then
      audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
   end if;

   if nvl(:old.LAST_MODIFIED_BY,0) !=
      NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
           Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	   into old_modby from er_user  where pk_user = :old.last_modified_by ;
	   Exception When NO_DATA_FOUND then
	   old_modby := null;
	End ;
	Begin
	   Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	   into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
	   Exception When NO_DATA_FOUND then
	   new_modby := null;
        End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    end if;

    if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
       NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
       audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

   if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
   end if;

end;
/


