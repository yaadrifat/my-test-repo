CREATE  OR REPLACE TRIGGER ER_FLDLIB_BI0 BEFORE INSERT ON ER_FLDLIB        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_FLDLIB',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_CATLIB', :NEW.FK_CATLIB);
       Audit_Trail.column_insert (raid, 'FK_LOOKUP', :NEW.FK_LOOKUP);
       Audit_Trail.column_insert (raid, 'FLD_ALIGN', :NEW.FLD_ALIGN);
       Audit_Trail.column_insert (raid, 'FLD_BOLD', :NEW.FLD_BOLD);
       Audit_Trail.column_insert (raid, 'FLD_CHARSNO', :NEW.FLD_CHARSNO);
       Audit_Trail.column_insert (raid, 'FLD_COLCOUNT', :NEW.FLD_COLCOUNT);
       Audit_Trail.column_insert (raid, 'FLD_COLOR', :NEW.FLD_COLOR);
       Audit_Trail.column_insert (raid, 'FLD_DATATYPE', :NEW.FLD_DATATYPE);
       Audit_Trail.column_insert (raid, 'FLD_DECIMAL', :NEW.FLD_DECIMAL);
       Audit_Trail.column_insert (raid, 'FLD_DEFRESP', :NEW.FLD_DEFRESP);
       Audit_Trail.column_insert (raid, 'FLD_DESC', :NEW.FLD_DESC);
       Audit_Trail.column_insert (raid, 'FLD_DISPLAY_WIDTH', :NEW.FLD_DISPLAY_WIDTH);
       Audit_Trail.column_insert (raid, 'FLD_EXPLABEL', :NEW.FLD_EXPLABEL);
       Audit_Trail.column_insert (raid, 'FLD_FONT', :NEW.FLD_FONT);
       Audit_Trail.column_insert (raid, 'FLD_FONTSIZE', :NEW.FLD_FONTSIZE);
       Audit_Trail.column_insert (raid, 'FLD_FORMAT', :NEW.FLD_FORMAT);
       Audit_Trail.column_insert (raid, 'FLD_HIDELABEL', :NEW.FLD_HIDELABEL);
       Audit_Trail.column_insert (raid, 'FLD_HIDERESPLABEL', :NEW.FLD_HIDERESPLABEL);
       Audit_Trail.column_insert (raid, 'FLD_INSTRUCTIONS', :NEW.FLD_INSTRUCTIONS);
       Audit_Trail.column_insert (raid, 'FLD_ISREADONLY', :NEW.FLD_ISREADONLY);
       Audit_Trail.column_insert (raid, 'FLD_ISUNIQUE', :NEW.FLD_ISUNIQUE);
       Audit_Trail.column_insert (raid, 'FLD_ISVISIBLE', :NEW.FLD_ISVISIBLE);
       Audit_Trail.column_insert (raid, 'FLD_ITALICS', :NEW.FLD_ITALICS);
       Audit_Trail.column_insert (raid, 'FLD_KEYWORD', :NEW.FLD_KEYWORD);
       Audit_Trail.column_insert (raid, 'FLD_LENGTH', :NEW.FLD_LENGTH);
       Audit_Trail.column_insert (raid, 'FLD_LIBFLAG', :NEW.FLD_LIBFLAG);
       Audit_Trail.column_insert (raid, 'FLD_LINESNO', :NEW.FLD_LINESNO);
       Audit_Trail.column_insert (raid, 'FLD_LINKEDFORM', :NEW.FLD_LINKEDFORM);
       Audit_Trail.column_insert (raid, 'FLD_LKPDATAVAL', :NEW.FLD_LKPDATAVAL);
       Audit_Trail.column_insert (raid, 'FLD_LKPDISPVAL', :NEW.FLD_LKPDISPVAL);
       Audit_Trail.column_insert (raid, 'FLD_LKPTYPE', :NEW.FLD_LKPTYPE);
       Audit_Trail.column_insert (raid, 'FLD_NAME', :NEW.FLD_NAME);
       Audit_Trail.column_insert (raid, 'FLD_NAME_FORMATTED', :NEW.FLD_NAME_FORMATTED);
       Audit_Trail.column_insert (raid, 'FLD_OVERRIDE_DATE', :NEW.FLD_OVERRIDE_DATE);
       Audit_Trail.column_insert (raid, 'FLD_OVERRIDE_FORMAT', :NEW.FLD_OVERRIDE_FORMAT);
       Audit_Trail.column_insert (raid, 'FLD_OVERRIDE_MANDATORY', :NEW.FLD_OVERRIDE_MANDATORY);
       Audit_Trail.column_insert (raid, 'FLD_OVERRIDE_RANGE', :NEW.FLD_OVERRIDE_RANGE);
       Audit_Trail.column_insert (raid, 'FLD_REPEATFLAG', :NEW.FLD_REPEATFLAG);
       Audit_Trail.column_insert (raid, 'FLD_RESPALIGN', :NEW.FLD_RESPALIGN);
       Audit_Trail.column_insert (raid, 'FLD_SAMELINE', :NEW.FLD_SAMELINE);
       Audit_Trail.column_insert (raid, 'FLD_SORTORDER', :NEW.FLD_SORTORDER);
       Audit_Trail.column_insert (raid, 'FLD_SYSTEMID', :NEW.FLD_SYSTEMID);
       Audit_Trail.column_insert (raid, 'FLD_TODAYCHECK', :NEW.FLD_TODAYCHECK);
       Audit_Trail.column_insert (raid, 'FLD_TYPE', :NEW.FLD_TYPE);
       Audit_Trail.column_insert (raid, 'FLD_UNDERLINE', :NEW.FLD_UNDERLINE);
       Audit_Trail.column_insert (raid, 'FLD_UNIQUEID', :NEW.FLD_UNIQUEID);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_FIELD', :NEW.PK_FIELD);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
