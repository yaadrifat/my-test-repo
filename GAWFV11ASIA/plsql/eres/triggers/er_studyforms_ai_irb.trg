CREATE OR REPLACE TRIGGER "ER_STUDYFORMS_AI_IRB"
AFTER INSERT
ON ER_STUDYFORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

v_lf_submission_type varchar2(20);
v_ret   number;

BEGIN

/* Whenever a form is answered check if it is for eIRB ongoing study*/

begin
    select nvl(lf_submission_type,' ')
    into v_lf_submission_type
    from er_linkedforms where
    fk_formlib = :new.fk_formlib and nvl(lf_isirb,0) = 1 and
    lf_submission_type in ('irb_ongo_amd','irb_ongo_prob','irb_ongo_cont','irb_ongo_clos');


if (v_lf_submission_type != ' ') then
      pkg_eirb.sp_submit_ongoing(:new.fk_study ,v_lf_submission_type ,:new.creator ,:new.ip_add ,
v_ret,:new.form_completed   );

end if; --for v_lf_submission_type

exception when no_data_found then
    v_lf_submission_type := '';
end;


END;
/


