CREATE OR REPLACE TRIGGER ER_PORTAL_POPLEVEL_AU0 AFTER UPDATE OF PK_PORTAL_POPLEVEL,FK_PORTAL,PP_OBJECT_TYPE,PP_OBJECT_ID,RID,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD ON ER_PORTAL_POPLEVEL FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_PORTAL', :old.rid, 'U', usr);
  if nvl(:old.PK_PORTAL_POPLEVEL,0) !=
     NVL(:new.PK_PORTAL_POPLEVEL,0) then
     audit_trail.column_update
       (raid, 'PK_PORTAL_POPLEVEL',
       :old.PK_PORTAL_POPLEVEL, :new.PK_PORTAL_POPLEVEL);
  end if;

  if nvl(:old.FK_PORTAL,0) !=
     NVL(:new.FK_PORTAL,0) then
     audit_trail.column_update
       (raid, 'FK_PORTAL',
       :old.FK_PORTAL, :new.FK_PORTAL);
  end if;

  if nvl(:old.PP_OBJECT_TYPE,' ') !=
     NVL(:new.PP_OBJECT_TYPE,' ') then
     audit_trail.column_update
       (raid, 'PP_OBJECT_TYPE',
       :old.PP_OBJECT_TYPE, :new.PP_OBJECT_TYPE);
  end if;

  if nvl(:old.PP_OBJECT_ID,0) !=
     NVL(:new.PP_OBJECT_ID,0) then
     audit_trail.column_update
       (raid, 'PP_OBJECT_ID',
       :old.PP_OBJECT_ID, :new.PP_OBJECT_ID);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) !=
     NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
   end if;

   if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
      audit_trail.column_update
      (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

    if nvl(:old.ip_add,' ') !=
       NVL(:new.ip_add,' ') then
       audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
    end if;

end;
/


