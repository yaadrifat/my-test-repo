CREATE  OR REPLACE TRIGGER ER_CRFFORMS_BI0 BEFORE INSERT ON ER_CRFFORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_CRFFORMS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CRFFORMS_FILLDATE', :NEW.CRFFORMS_FILLDATE);
      -- Audit_Trail.column_insert (raid, 'CRFFORMS_XML', :NEW.CRFFORMS_XML);
       Audit_Trail.column_insert (raid, 'FK_CRF', :NEW.FK_CRF);
       Audit_Trail.column_insert (raid, 'FK_FORMLIB', :NEW.FK_FORMLIB);
       Audit_Trail.column_insert (raid, 'FK_FORMLIBVER', :NEW.FK_FORMLIBVER);
       Audit_Trail.column_insert (raid, 'FK_SCHEVENT1', :NEW.FK_SCHEVENT1);
       Audit_Trail.column_insert (raid, 'FORM_COMPLETED', :NEW.FORM_COMPLETED);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'ISVALID', :NEW.ISVALID);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NOTIFICATION_SENT', :NEW.NOTIFICATION_SENT);
       Audit_Trail.column_insert (raid, 'PK_CRFFORMS', :NEW.PK_CRFFORMS);
       Audit_Trail.column_insert (raid, 'PROCESS_DATA', :NEW.PROCESS_DATA);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       
COMMIT;
END;
/