CREATE OR REPLACE TRIGGER "ER_STUDYTEAM_AI0" 
AFTER INSERT
ON ER_STUDYTEAM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  v_new_fk_study NUMBER;
  v_new_fk_user NUMBER;
  v_new_creator NUMBER;
  v_new_ip_add VARCHAR2(15);

BEGIN

 v_new_fk_study := :NEW.fk_study ;
 v_new_fk_user := :NEW.fk_user ;
 v_new_creator := :NEW.creator;
 v_new_ip_add := :NEW.ip_add;


Pkg_Studystat.sp_grant_studyteam_access( v_new_fk_study, v_new_fk_user,v_new_creator,v_new_ip_add);

END;
/


