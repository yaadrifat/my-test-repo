CREATE  OR REPLACE TRIGGER ER_STUDYSITES_APNDX_AD0 AFTER DELETE ON ER_STUDYSITES_APNDX        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STUDYSITES_APNDX', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'APNDX_DESCRIPTION', :OLD.APNDX_DESCRIPTION);
       --Audit_Trail.column_delete (raid, 'APNDX_FILE', :OLD.APNDX_FILE);
       Audit_Trail.column_delete (raid, 'APNDX_FILESIZE', :OLD.APNDX_FILESIZE);
       Audit_Trail.column_delete (raid, 'APNDX_NAME', :OLD.APNDX_NAME);
       Audit_Trail.column_delete (raid, 'APNDX_TYPE', :OLD.APNDX_TYPE);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_STUDYSITES', :OLD.FK_STUDYSITES);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_ON', :OLD.LAST_MODIFIED_ON);
       Audit_Trail.column_delete (raid, 'PK_STUDYSITES_APNDX', :OLD.PK_STUDYSITES_APNDX);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
