CREATE OR REPLACE TRIGGER "ER_MILEACHIEVED_AD1" 
 AFTER DELETE ON ER_MILEACHIEVED
 REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
DECLARE
 BEGIN
   --update er_milestone to update milestone ach count
   -- for only those milestones where 'count' in milestone rule is <= 1

        update er_milestone
        set milestone_achievedcount= milestone_achievedcount -1
        where pk_milestone = :old.fk_milestone and nvl(milestone_count,0) <= 1;

  END ;
/


