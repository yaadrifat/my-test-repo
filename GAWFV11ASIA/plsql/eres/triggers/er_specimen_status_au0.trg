CREATE OR REPLACE TRIGGER ER_SPECIMEN_STATUS_AU0 AFTER UPDATE ON ER_SPECIMEN_STATUS FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);


  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
  		audit_trail.record_transaction(raid, 'ER_SPECIMEN_STATUS', :OLD.rid, 'U', usr);
  END IF;

  IF NVL(:OLD.PK_SPECIMEN_STATUS,0) !=
     NVL(:NEW.PK_SPECIMEN_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'PK_SPECIMEN_STATUS',
       :OLD.PK_SPECIMEN_STATUS, :NEW.PK_SPECIMEN_STATUS);
  END IF;

  IF NVL(:OLD.FK_SPECIMEN,0) !=
     NVL(:NEW.FK_SPECIMEN,0) THEN
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :OLD.FK_SPECIMEN, :NEW.FK_SPECIMEN);
  END IF;

  IF NVL(:OLD.SS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.SS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'SS_DATE',
       to_char(:OLD.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.FK_CODELST_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STATUS',
       :OLD.FK_CODELST_STATUS, :NEW.FK_CODELST_STATUS);
  END IF;


  IF NVL(:OLD.SS_QUANTITY,0) !=
     NVL(:NEW.SS_QUANTITY,0) THEN
     audit_trail.column_update
       (raid, 'SS_QUANTITY',
       :OLD.SS_QUANTITY, :NEW.SS_QUANTITY);
  END IF;

  IF NVL(:OLD.SS_QUANTITY_UNITS,0) !=
     NVL(:NEW.SS_QUANTITY_UNITS,0) THEN
     audit_trail.column_update
       (raid, 'SS_QUANTITY_UNITS',
       :OLD.SS_QUANTITY_UNITS, :NEW.SS_QUANTITY_UNITS);
  END IF;


  IF NVL(:OLD.SS_ACTION,0) !=
     NVL(:NEW.SS_ACTION,0) THEN
     audit_trail.column_update
       (raid, 'SS_ACTION',
       :OLD.SS_ACTION, :NEW.SS_ACTION);
  END IF;


  IF NVL(:OLD.FK_STUDY,0) !=
     NVL(:NEW.FK_STUDY,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.FK_STUDY, :NEW.FK_STUDY);
  END IF;


  IF NVL(:OLD.FK_USER_RECEPIENT,0) !=
     NVL(:NEW.FK_USER_RECEPIENT,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER_RECEPIENT',
       :OLD.FK_USER_RECEPIENT, :NEW.FK_USER_RECEPIENT);
  END IF;

  IF NVL(:OLD.SS_TRACKING_NUMBER,' ') !=
     NVL(:NEW.SS_TRACKING_NUMBER,' ') THEN
     audit_trail.column_update
       (raid, 'SS_TRACKING_NUMBER',
       :OLD.SS_TRACKING_NUMBER, :NEW.SS_TRACKING_NUMBER);
  END IF;

   IF NVL(:OLD.SS_STATUS_BY,0) !=
     NVL(:NEW.SS_STATUS_BY,0) THEN
     audit_trail.column_update
       (raid, 'SS_STATUS_BY',
       :OLD.SS_STATUS_BY, :NEW.SS_STATUS_BY);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;



  IF NVL(:OLD.ss_proc_type,0) !=
     NVL(:NEW.ss_proc_type,0) THEN
     audit_trail.column_update
       (raid, 'SS_PROC_TYPE',
       :OLD.ss_proc_type, :NEW.ss_proc_type);
  END IF;


  IF NVL(:OLD.ss_hand_off_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ss_hand_off_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'SS_HAND_OFF_DATE',
       to_char(:OLD.ss_hand_off_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.ss_hand_off_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.ss_proc_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ss_proc_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'SS_PROC_DATE',
       to_char(:OLD.ss_proc_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.ss_proc_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

END;
/


