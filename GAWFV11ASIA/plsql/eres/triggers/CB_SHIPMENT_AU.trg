create or replace
TRIGGER CB_SHIPMENT_AU
AFTER UPDATE ON CB_SHIPMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
pragma autonomous_transaction;
 outstr varchar2(100);
 workflowType VARCHAR2(100);
 entityType_CT NUMBER;
 entityType NUMBER;

BEGIN

 select pk_codelst into entityType_CT from er_codelst where codelst_type='order_type' and codelst_subtyp='CT';

 select ord.order_type into entityType from ER_ORDER ord where ord.PK_ORDER = :new.FK_ORDER_ID;

  workflowType := null;
  if(entityType_CT = entityType) then
      workflowType := 'CTORDER';
  end if;

  if(workflowType is not null) then
       SP_WORKFLOW(:new.FK_ORDER_ID,'CTORDER',1,outstr);
  end if;
COMMIT;  
END;
