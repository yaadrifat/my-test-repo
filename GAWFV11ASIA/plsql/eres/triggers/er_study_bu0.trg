CREATE OR REPLACE TRIGGER "ER_STUDY_BU0" 
BEFORE UPDATE
ON ER_STUDY REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.last_modified_by is not null
      )
BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/


