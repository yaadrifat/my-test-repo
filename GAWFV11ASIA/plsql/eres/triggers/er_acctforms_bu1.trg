CREATE OR REPLACE TRIGGER "ER_ACCTFORMS_BU1"
BEFORE UPDATE OF ACCTFORMS_XML
ON ER_ACCTFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  datestr Varchar2(20);
  v_form_xml  sys.xmlType;
  i   NUMBER := 1;
 begin
  :new.NOTIFICATION_SENT := 0; --reset the notification flag
  v_form_xml := :new.ACCTFORMS_XML;
          datestr := v_form_xml.extract('/rowset/er_def_date_01/text()').getStringVal();
       if datestr is not null then
       :new.ACCTFORMS_FILLDATE  := to_date(datestr,PKG_DATEUTIL.f_get_dateformat);
    end if ;
 if :new.ACCTFORMS_FILLDATE is null then
   :new.ACCTFORMS_FILLDATE  := sysdate;
     END IF;
 end;
/


