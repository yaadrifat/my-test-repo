CREATE OR REPLACE TRIGGER ER_SPECIMEN_AI0 AFTER INSERT ON ER_SPECIMEN
FOR EACH ROW
DECLARE
v_occupied_stat_code number;

begin

select pk_Codelst into v_occupied_stat_code from er_Codelst where codelst_type='storage_stat' and codelst_subtyp='Occupied';

if  (:new.FK_STORAGE is not null) then

insert into ER_STORAGE_STATUS (PK_STORAGE_STATUS,
FK_STORAGE,
SS_START_DATE,
FK_CODELST_STORAGE_STATUS,
FK_USER,
SS_TRACKING_NUMBER,
FK_STUDY,
CREATOR,
CREATED_ON,
IP_ADD,
SS_NOTES) values (

seq_er_storage_stat.nextval, :new.FK_STORAGE, to_Date(TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT), v_occupied_stat_code, :new.CREATOR, '', :new.FK_STUDY, :new.CREATOR,
TO_DATE(TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT), :new.IP_ADD, '');


end if;

end ER_SPECIMEN_AI0;
/


