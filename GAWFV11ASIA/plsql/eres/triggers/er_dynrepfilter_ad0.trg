CREATE  OR REPLACE TRIGGER ER_DYNREPFILTER_AD0 AFTER DELETE ON ER_DYNREPFILTER        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_DYNREPFILTER', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'DATERANGE_FROM', :OLD.DATERANGE_FROM);
       Audit_Trail.column_delete (raid, 'DATERANGE_TO', :OLD.DATERANGE_TO);
       Audit_Trail.column_delete (raid, 'DATERANGE_TYPE', :OLD.DATERANGE_TYPE);
      -- Audit_Trail.column_delete (raid, 'DYNREPFILTER_FILTER', :OLD.DYNREPFILTER_FILTER);
       Audit_Trail.column_delete (raid, 'DYNREPFILTER_NAME', :OLD.DYNREPFILTER_NAME);
       Audit_Trail.column_delete (raid, 'FK_DYNREP', :OLD.FK_DYNREP);
       Audit_Trail.column_delete (raid, 'FK_FORM', :OLD.FK_FORM);
       Audit_Trail.column_delete (raid, 'FK_PARENTFILTER', :OLD.FK_PARENTFILTER);
       Audit_Trail.column_delete (raid, 'FORM_TYPE', :OLD.FORM_TYPE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_DYNREPFILTER', :OLD.PK_DYNREPFILTER);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
