CREATE OR REPLACE TRIGGER "ER_FORMFLD_AU1" 
AFTER UPDATE OF RECORD_TYPE
ON ER_FORMFLD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.record_type='D'
      )
BEGIN

/*

sets the record_type to 'D' for all fields, field responses and repeated fields corresponding to this field

the form_xslrefresh is set to 1 at JB level to prevent mutation

*/



update er_fldlib

set record_type='D'

where pk_field = :old.fk_field;





delete er_fldresp

where fk_field = :old.fk_field ;







delete er_fldresp

where fk_field in ( select fk_field from er_repformfld where fk_formfld = :old.pk_formfld ) ;



delete er_repformfld

where fk_formfld = :old.pk_formfld ;





END;
/


