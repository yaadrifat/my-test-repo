CREATE  OR REPLACE TRIGGER ER_FORMQUERYSTATUS_BI0 BEFORE INSERT ON ER_FORMQUERYSTATUS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_FORMQUERYSTATUS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'ENTERED_BY', :NEW.ENTERED_BY);
       Audit_Trail.column_insert (raid, 'ENTERED_ON', :NEW.ENTERED_ON);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_QUERYSTATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_QUERYSTATUS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_QUERYTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_QUERYTYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_FORMQUERY', :NEW.FK_FORMQUERY);
       Audit_Trail.column_insert (raid, 'FORMQUERY_TYPE', :NEW.FORMQUERY_TYPE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_FORMQUERYSTATUS', :NEW.PK_FORMQUERYSTATUS);
       Audit_Trail.column_insert (raid, 'QUERY_NOTES', :NEW.QUERY_NOTES);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/