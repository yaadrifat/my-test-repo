CREATE OR REPLACE TRIGGER ER_PATLABS_BU_TIME
BEFORE UPDATE
ON ER_PATLABS REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
BEGIN

    if  ( trunc(:old.test_date)  = trunc(:new.test_date) and  to_char(:old.test_date,'hh24:mi:ss') <> '00:00:00' ) then

        :new.test_date := :old.test_date;

    end if;

END;
/


