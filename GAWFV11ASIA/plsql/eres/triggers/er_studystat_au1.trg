CREATE OR REPLACE TRIGGER ER_STUDYSTAT_AU1
AFTER UPDATE
OF FK_CODELST_STUDYSTAT
ON ER_STUDYSTAT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare v_checkactive number(10);
        v_checkclose number(10);
BEGIN

--update study start date if the status is active
---This is being done now at java code level.
/*select count(*) into v_checkactive
	from er_codelst
	where :new.fk_codelst_studystat = pk_codelst
	and codelst_type='studystat'
	and codelst_subtyp='active';

if (v_checkactive = 1) then
	update er_study
	set study_actualdt = :new.studystat_date
	where pk_study = :new.fk_study
	and study_actualdt is null;
end if;*/

--update study end date if the status is permanent closure
select count(*) into v_checkclose
	from er_codelst
	where :new.fk_codelst_studystat = pk_codelst
	and codelst_type='studystat'
	and codelst_subtyp='prmnt_cls';

if (v_checkclose = 1) then
	update er_study
	set study_end_date = :new.studystat_date,
	last_modified_by = NVL(:new.last_modified_by, :new.creator)
	where pk_study = :new.fk_study
	and study_end_date is null;
end if;

END;
/


