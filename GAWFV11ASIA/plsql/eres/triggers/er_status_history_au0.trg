CREATE OR REPLACE TRIGGER ER_STATUS_HISTORY_AU0
  AFTER UPDATE OF
   fk_codelst_stat,
  status_date,
  status_end_date,
  status_notes,
  status_custom1,
  status_iscurrent,
  record_type,
  last_modified_by,
  last_modified_date,
  ip_add
  ON er_status_history
  FOR EACH ROW
DECLARE
   raid NUMBER(10);
   usr VARCHAR2(100);

   old_modby VARCHAR2(100);
   new_modby VARCHAR2(100);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_STATUS_HISTORY', :OLD.rid, 'U', usr);

  IF NVL(:OLD.fk_codelst_stat,0) !=
     NVL(:NEW.fk_codelst_stat,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM er_codelst WHERE pk_codelst =  :OLD.fk_codelst_stat ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL ;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM er_codelst WHERE pk_codelst =  :NEW.fk_codelst_stat ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'fk_codelst_stat',
       old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.status_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.status_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'STATUS_DATE',
       to_char(:OLD.status_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.status_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.status_end_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.status_end_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'STATUS_END_DATE',
       to_char(:OLD.status_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.status_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.status_notes,' ') !=
     NVL(:NEW.status_notes,' ') THEN
     audit_trail.column_update
       (raid, 'STATUS_NOTES',
       :OLD.status_notes, :NEW.status_notes);
  END IF;
  IF NVL(:OLD.status_custom1,' ') !=
     NVL(:NEW.status_custom1,' ') THEN
     audit_trail.column_update
       (raid, 'STATUS_CUSTOM1',
       :OLD.status_custom1, :NEW.status_custom1);
  END IF;
  IF NVL(:OLD.record_type,' ') !=
     NVL(:NEW.record_type,' ') THEN
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :OLD.record_type, :NEW.record_type);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  --Added for july-august Enhancement #S4
  if nvl(:old.STATUS_ISCURRENT ,0 ) !=
     NVL(:new.STATUS_ISCURRENT ,0) then
     audit_trail.column_update
       (raid, 'STATUS_ISCURRENT',
       :old.STATUS_ISCURRENT, :new.STATUS_ISCURRENT);
  end if;

 IF NVL(:OLD.LAST_MODIFIED_BY,0) !=
 NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        BEGIN
                SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
                INTO old_modby FROM er_user  WHERE pk_user = :OLD.last_modified_by ;
                EXCEPTION WHEN NO_DATA_FOUND THEN
                        old_modby := NULL;
        END ;
        BEGIN
                SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
                INTO new_modby   FROM er_user   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
                EXCEPTION WHEN NO_DATA_FOUND THEN
                        new_modby := NULL;
         END ;
        audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 END IF;

END;
/


