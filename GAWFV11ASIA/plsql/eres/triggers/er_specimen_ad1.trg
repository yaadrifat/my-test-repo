CREATE OR REPLACE TRIGGER ER_SPECIMEN_AD1
 AFTER DELETE ON ER_SPECIMEN
 REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
DECLARE
   v_available_stat_code number;
 BEGIN

    select pk_Codelst into v_available_stat_code from er_Codelst where codelst_type='storage_stat' and codelst_subtyp='Available';

if  (:old.FK_STORAGE is not null) then
	insert into ER_STORAGE_STATUS (PK_STORAGE_STATUS,
	FK_STORAGE,
	SS_START_DATE,
	FK_CODELST_STORAGE_STATUS,
	FK_USER,
	SS_TRACKING_NUMBER,
	FK_STUDY,
	CREATOR,
	CREATED_ON,
	IP_ADD,
	SS_NOTES) values (

	seq_er_storage_stat.nextval, :old.FK_STORAGE, to_date(TO_CHAR(sysdate,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT), v_available_stat_code, :old.CREATOR, '', :old.FK_STUDY, :new.CREATOR,
	sysdate, :old.IP_ADD, '');

end if;


  END ER_SPECIMEN_AD1;
/


