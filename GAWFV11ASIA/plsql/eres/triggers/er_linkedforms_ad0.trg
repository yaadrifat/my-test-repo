CREATE  OR REPLACE TRIGGER ER_LINKEDFORMS_AD0 AFTER DELETE ON ER_LINKEDFORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_LINKEDFORMS', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_CALENDAR', :OLD.FK_CALENDAR);
       Audit_Trail.column_delete (raid, 'FK_CRF', :OLD.FK_CRF);
       Audit_Trail.column_delete (raid, 'FK_EVENT', :OLD.FK_EVENT);
       Audit_Trail.column_delete (raid, 'FK_FORMLIB', :OLD.FK_FORMLIB);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'LF_DATACNT', :OLD.LF_DATACNT);
       Audit_Trail.column_delete (raid, 'LF_DISPLAYTYPE', :OLD.LF_DISPLAYTYPE);
       Audit_Trail.column_delete (raid, 'LF_DISPLAY_INPAT', :OLD.LF_DISPLAY_INPAT);
       Audit_Trail.column_delete (raid, 'LF_DISPLAY_INSPEC', :OLD.LF_DISPLAY_INSPEC);
       Audit_Trail.column_delete (raid, 'LF_ENTRYCHAR', :OLD.LF_ENTRYCHAR);
       Audit_Trail.column_delete (raid, 'LF_HIDE', :OLD.LF_HIDE);
       Audit_Trail.column_delete (raid, 'LF_ISIRB', :OLD.LF_ISIRB);
       Audit_Trail.column_delete (raid, 'LF_LNKFROM', :OLD.LF_LNKFROM);
       Audit_Trail.column_delete (raid, 'LF_SEQ', :OLD.LF_SEQ);
       Audit_Trail.column_delete (raid, 'LF_SUBMISSION_TYPE', :OLD.LF_SUBMISSION_TYPE);
       Audit_Trail.column_delete (raid, 'PK_LF', :OLD.PK_LF);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
