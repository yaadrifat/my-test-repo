CREATE  OR REPLACE TRIGGER ER_STORAGE_KIT_BI0 BEFORE INSERT ON ER_STORAGE_KIT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STORAGE_KIT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'DEF_PROCESSING_TYPE', :NEW.DEF_PROCESSING_TYPE);
       Audit_Trail.column_insert (raid, 'DEF_SPECIMEN_TYPE', :NEW.DEF_SPECIMEN_TYPE);
       Audit_Trail.column_insert (raid, 'DEF_SPEC_PROCESSING_SEQ', :NEW.DEF_SPEC_PROCESSING_SEQ);
       Audit_Trail.column_insert (raid, 'DEF_SPEC_QUANTITY', :NEW.DEF_SPEC_QUANTITY);
       Audit_Trail.column_insert (raid, 'DEF_SPEC_QUANTITY_UNIT', :NEW.DEF_SPEC_QUANTITY_UNIT);
       Audit_Trail.column_insert (raid, 'FK_STORAGE', :NEW.FK_STORAGE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'KIT_SPEC_DISPOSITION', :NEW.KIT_SPEC_DISPOSITION);
       Audit_Trail.column_insert (raid, 'KIT_SPEC_ENVT_CONS', :NEW.KIT_SPEC_ENVT_CONS);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_STORAGE_KIT', :NEW.PK_STORAGE_KIT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
