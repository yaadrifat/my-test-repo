CREATE OR REPLACE TRIGGER "ER_FORMFLD_AI1" 
AFTER INSERT
ON ER_FORMFLD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_out_status NUMBER(10);
v_repeat_no NUMBER (10);
i NUMBER;
v_newfld NUMBER;
v_fld_tocopy NUMBER(10);
v_new_repformfld NUMBER(10);
v_new_repfldvalidate NUMBER;
v_fld_systemid VARCHAR2(50) ;
v_fld_oldsystemid VARCHAR2(50) ;
v_repformfld_xsl VARCHAR(4000);
v_formfld_xsl   VARCHAR2(4000);
v_formfld_xsl_init   VARCHAR2(4000);
v_formfld_javascr VARCHAR2(4000) ;
v_formfld_javascr_init VARCHAR2(4000) ;
v_fk_formsec   NUMBER ;
v_formfld_seq NUMBER ;
v_formsec_fmt CHAR(1);
v_formfld_xsl_1   VARCHAR2(4000);
v_formfld_xsl_2   VARCHAR2(4000);
v_formfld_xsl_3   VARCHAR2(4000);
v_pos_1  NUMBER ;
v_pos_2  NUMBER ;
v_pos_3  NUMBER ;
v_pos_endtd NUMBER;
len NUMBER ;
v_strlen NUMBER ;
v_fld_type CHAR(1);
v_fld_datatype VARCHAR2(2);
v_fldval_op1 CHAR(2);
v_fldval_val1 VARCHAR2(100);
v_fldval_logop1 VARCHAR2(10);
v_fldval_op2 CHAR(2);
v_fldval_val2 VARCHAR2(100);
v_fldval_logop2 VARCHAR2(10);
v_fldval_javascr VARCHAR2(4000);
record_type CHAR(1);
creator NUMBER;
created_on DATE;
ip_add VARCHAR2(15);
v_fld_align VARCHAR2(20);

BEGIN
 --Find the Format, field repeat number of the Section
   SELECT formsec_fmt,formsec_repno
   INTO v_formsec_fmt, v_repeat_no
   FROM ER_FORMSEC
   WHERE  pk_formsec = :NEW.fk_formsec ;

   i := 1 ;

  -- Select the values from ER_FORMFLD to the variables
	v_fk_formsec := :NEW.FK_FORMSEC ;
	v_formfld_seq := :NEW.FORMFLD_SEQ ;
	v_formfld_xsl_init := :NEW.FORMFLD_XSL ;

	v_formfld_javascr_init :=  :NEW.FORMFLD_JAVASCR;

	SELECT fld_systemid , fld_type, fld_datatype,fld_align
	INTO v_fld_oldsystemid , v_fld_type, v_fld_datatype,v_fld_align
	FROM ER_FLDLIB
	WHERE pk_field = :NEW.fk_field ;

  WHILE i <= v_repeat_no LOOP
	BEGIN


	    v_fld_systemid  := 0 ;
	    v_newfld := 0 ;
	    v_formfld_xsl_1 := '';
	    v_formfld_xsl_2 := '';
	    v_formfld_xsl_3  := '';
		--First copy the field to the ER_FLDLIB
	     Pkg_Form.SP_REPEAT_FIELD(:NEW.fk_field, :NEW.creator, :NEW.ip_add, v_newfld);
          --p(' after the repeat flag ' || v_newfld) ;
		 IF v_newfld > 0 THEN
		   --We get the the FLD_SYSTEMID from the ER_FLDLIB table for using in the XSL

			 SELECT FLD_SYSTEMID
			 INTO v_fld_systemid
			 FROM ER_FLDLIB
			 WHERE PK_FIELD = v_newfld ;



			 v_formfld_xsl_1 := REPLACE(v_formfld_xsl_init, v_fld_oldsystemid , v_fld_systemid);

                v_formfld_javascr := REPLACE(v_formfld_javascr_init, v_fld_oldsystemid ,v_fld_systemid);

                --we change the xsl and remove the <label> tags if the section format is tabular

                IF v_formsec_fmt = 'T'   THEN

			   		--modified by sonia abrol, 03/16/05 to use a function to cut the label

			 		 SELECT Pkg_Form.f_cut_fieldlabel(v_formfld_xsl_1,v_fld_type, v_fld_datatype,v_fld_align,v_newfld)
			 		 INTO v_formfld_xsl  FROM dual;

			 /* --modified by Sonika on Nov 21, 03 search only for '<td width=''15%'' instead of '<td width=''15%'' ><label' as align has also been added
                 --modified by Sonika on April 19, 04 search only for <td
				  v_pos_1 := INSTR( v_formfld_xsl_1, '<td') ;
		     	  v_formfld_xsl_2 := SUBSTR(v_formfld_xsl_1 , 0 ,v_pos_1-1);
			      len := LENGTH(v_formfld_xsl_1);
				  v_pos_2 := INSTR(v_formfld_xsl_1 , '</label>');

                   --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
       		       --Modified by Sonika Talwar on April 22, 04 to remove help icon addition incase of Tabular section

                    --find </td> end tag of label
   		          v_pos_endtd := INSTR(v_formfld_xsl_1 , '</td>',v_pos_2);
                    --# of characters between </label> and </td>
			     v_strlen := LENGTH('</td>');
			     v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;

	              v_formfld_xsl_3 := SUBSTR( v_formfld_xsl_1 , v_pos_2+v_strlen , len-1 ) ;
	              v_formfld_xsl := v_formfld_xsl_2 || v_formfld_xsl_3 ;
				  */


			    ELSE
			     v_formfld_xsl := v_formfld_xsl_1 ;
			END IF ;

			 -- The new form field id generated from the seq_er_repformfld sequence.
             SELECT seq_er_repformfld.NEXTVAL
             INTO v_new_repformfld
             FROM dual ;
		    --p(' after the repeat flag 2 '  ) ;
			--Enter all the respective data from existing ER_FORMFLD to ER_REPFORMFLD
              --p('The new pk for repformfld' || v_new_repformfld ) ;
		 	INSERT INTO ER_REPFORMFLD
			( PK_REPFORMFLD , FK_FORMSEC , FK_FIELD , FK_FORMFLD,
			  REPFORMFLD_XSL,REPFORMFLD_JAVASCR, REPFORMFLD_SEQ , REPFORMFLD_SET  ,RECORD_TYPE,
			  CREATOR , CREATED_ON , IP_ADD)
			VALUES ( v_new_repformfld ,v_fk_formsec , v_newfld , :NEW.pk_formfld,
			v_formfld_xsl ,v_formfld_javascr ,v_formfld_seq , i ,
    	          'N' , :NEW.creator , SYSDATE , :NEW.ip_add ) ;

-------------Get all field values for the fldvalidate record whose data is to be inserted into er_repfldvalidate


              BEGIN
			 SELECT REPLACE( FLDVALIDATE_JAVASCR , v_fld_oldsystemid ,v_fld_systemid)
			   INTO v_fldval_javascr
			   FROM ER_FLDVALIDATE
			   WHERE fk_fldlib = v_newfld ;

     		   -- The new repeated form field validation id generated from the seq_er_repfldvalidate sequence.
               SELECT seq_er_repfldvalidate.NEXTVAL
               INTO v_new_repfldvalidate
               FROM dual ;

			--insert repeated field validation javascript
			INSERT INTO ER_REPFLDVALIDATE(
			 PK_REPFLDVALIDATE  , FK_REPFORMFLD, FK_FIELD ,
			 REPFLDVALIDATE_JAVASCR,
			 RECORD_TYPE, CREATOR, CREATED_ON, IP_ADD)
			VALUES (v_new_repfldvalidate, v_new_repformfld , v_newfld,
			 v_fldval_javascr ,
			  'N' , :NEW.creator , SYSDATE , :NEW.ip_add ) ;

        	    EXCEPTION WHEN NO_DATA_FOUND THEN
			    P('no data');
              END;
		END IF;

      END ;--end of insert begin
	 i := i+1;
  END LOOP;


END ;
/


