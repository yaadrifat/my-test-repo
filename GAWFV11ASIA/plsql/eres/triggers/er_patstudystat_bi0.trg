CREATE  OR REPLACE TRIGGER ER_PATSTUDYSTAT_BI0 BEFORE INSERT ON ER_PATSTUDYSTAT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PATSTUDYSTAT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CURRENT_STAT', :NEW.CURRENT_STAT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STAT', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'INFORM_CONSENT_VER', :NEW.INFORM_CONSENT_VER);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NEXT_FOLLOWUP_ON', :NEW.NEXT_FOLLOWUP_ON);
       Audit_Trail.column_insert (raid, 'PATSTUDYSTAT_DATE', :NEW.PATSTUDYSTAT_DATE);
       Audit_Trail.column_insert (raid, 'PATSTUDYSTAT_ENDT', :NEW.PATSTUDYSTAT_ENDT);
       Audit_Trail.column_insert (raid, 'PATSTUDYSTAT_NOTE', :NEW.PATSTUDYSTAT_NOTE);
       Audit_Trail.column_insert (raid, 'PATSTUDYSTAT_REASON', :NEW.PATSTUDYSTAT_REASON);
       Audit_Trail.column_insert (raid, 'PK_PATSTUDYSTAT', :NEW.PK_PATSTUDYSTAT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SCREENED_BY', :NEW.SCREENED_BY);
       Audit_Trail.column_insert (raid, 'SCREENING_OUTCOME', :NEW.SCREENING_OUTCOME);
       Audit_Trail.column_insert (raid, 'SCREEN_NUMBER', :NEW.SCREEN_NUMBER);
COMMIT;
END;
/