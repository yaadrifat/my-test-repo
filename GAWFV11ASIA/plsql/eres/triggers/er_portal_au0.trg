CREATE OR REPLACE TRIGGER ER_PORTAL_AU0 AFTER UPDATE OF PK_PORTAL,PORTAL_NAME,FK_ACCOUNT,PORTAL_CREATED_BY,PORTAL_DESC,PORTAL_LEVEL,PORTAL_NOTIFICATION_FLAG,PORTAL_STATUS,PORTAL_DISP_TYPE,PORTAL_BGCOLOR,PORTAL_TEXTCOLOR,
PORTAL_AUDITUSER,RID,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,portal_consenting_form ON ER_PORTAL FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_PORTAL', :old.rid, 'U', usr);
  if nvl(:old.PK_PORTAL,0) !=
     NVL(:new.PK_PORTAL,0) then
     audit_trail.column_update
       (raid, 'PK_PORTAL',
       :old.PK_PORTAL, :new.PK_PORTAL);
  end if;
  if nvl(:old.PORTAL_NAME,' ') !=
     NVL(:new.PORTAL_NAME,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_NAME',
       :old.PORTAL_NAME, :new.PORTAL_NAME);
  end if;

  if nvl(:old.FK_ACCOUNT,0) !=
     NVL(:new.FK_ACCOUNT,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.FK_ACCOUNT, :new.FK_ACCOUNT);
  end if;

  if nvl(:old.PORTAL_CREATED_BY,0) !=
     NVL(:new.PORTAL_CREATED_BY,0) then
     audit_trail.column_update
       (raid, 'PORTAL_CREATED_BY',
       :old.PORTAL_CREATED_BY, :new.PORTAL_CREATED_BY);
  end if;

  if nvl(:old.PORTAL_DESC,' ') !=
     NVL(:new.PORTAL_DESC,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_DESC',
       :old.PORTAL_DESC, :new.PORTAL_DESC);
  end if;

  if nvl(:old.PORTAL_LEVEL,' ') !=
     NVL(:new.PORTAL_LEVEL,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_LEVEL',
       :old.PORTAL_LEVEL, :new.PORTAL_LEVEL);
  end if;

  if nvl(:old.PORTAL_NOTIFICATION_FLAG,0) !=
     NVL(:new.PORTAL_NOTIFICATION_FLAG,0) then
     audit_trail.column_update
       (raid, 'PORTAL_NOTIFICATION_FLAG',
       :old.PORTAL_NOTIFICATION_FLAG, :new.PORTAL_NOTIFICATION_FLAG);
  end if;

  if nvl(:old.PORTAL_STATUS,' ') !=
     NVL(:new.PORTAL_STATUS,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_STATUS',
       :old.PORTAL_STATUS, :new.PORTAL_STATUS);
  end if;


  if nvl(:old.PORTAL_DISP_TYPE,' ') !=
     NVL(:new.PORTAL_DISP_TYPE,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_DISP_TYPE',
       :old.PORTAL_DISP_TYPE, :new.PORTAL_DISP_TYPE);
  end if;

  if nvl(:old.PORTAL_BGCOLOR,' ') !=
     NVL(:new.PORTAL_BGCOLOR,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_BGCOLOR',
       :old.PORTAL_BGCOLOR, :new.PORTAL_BGCOLOR);
  end if;

  if nvl(:old.PORTAL_TEXTCOLOR,' ') !=
     NVL(:new.PORTAL_TEXTCOLOR,' ') then
     audit_trail.column_update
       (raid, 'PORTAL_TEXTCOLOR',
       :old.PORTAL_TEXTCOLOR, :new.PORTAL_TEXTCOLOR);
  end if;

  if nvl(:old.PORTAL_AUDITUSER,0) !=
     NVL(:new.PORTAL_AUDITUSER,0) then
     audit_trail.column_update
       (raid, 'PORTAL_AUDITUSER',
       :old.PORTAL_AUDITUSER, :new.PORTAL_AUDITUSER);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) !=
     NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from ER_USER  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from ER_USER   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
   end if;

   if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
      audit_trail.column_update
      (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

    if nvl(:old.ip_add,' ') !=
       NVL(:new.ip_add,' ') then
       audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
    end if;

    if nvl(:old.fk_study, 0) !=
       NVL(:new.fk_study, 0) then
       audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
    end if;


    if nvl(:old.portal_selflogout, 0) !=
       NVL(:new.portal_selflogout, 0) then
       audit_trail.column_update
       (raid, 'PORTAL_SELFLOGOUT',
       :old.portal_selflogout, :new.portal_selflogout);
    end if;

	 if nvl(:old.portal_createlogins, 0) !=
       NVL(:new.portal_createlogins, 0) then
       audit_trail.column_update
       (raid, 'PORTAL_CREATELOGINS',
       :old.portal_createlogins, :new.portal_createlogins);
    end if;

     if nvl(:old.portal_defaultpass,' ') !=
       NVL(:new.portal_defaultpass,' ') then
       audit_trail.column_update
       (raid, 'PORTAL_DEFAULTPASS',
       :old.portal_defaultpass, :new.portal_defaultpass);
    end if;


    if nvl(:old.portal_consenting_form, 0) !=
       NVL(:new.portal_consenting_form, 0) then
       audit_trail.column_update
       (raid, 'PORTAL_CONSENTING_FORM',
       :old.portal_consenting_form, :new.portal_consenting_form);
    end if;


end;
/


