CREATE OR REPLACE TRIGGER ER_LINKEDFORMS_AU0
  after update of
  pk_lf,
  fk_formlib,
  lf_displaytype,
  lf_entrychar,
  fk_account,
  fk_study,
  lf_lnkfrom,
  fk_calendar,
  fk_event,
  fk_crf,
  record_type,
  last_modified_by,
  last_modified_date,
  ip_add,
  rid,
  lf_isirb,
  lf_submission_type
  ON ER_LINKEDFORMS   for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_LINKEDFORMS', :old.rid, 'U', usr);

  if nvl(:old.pk_lf,0) !=
     NVL(:new.pk_lf,0) then
     audit_trail.column_update
       (raid, 'PK_LF',
       :old.pk_lf, :new.pk_lf);
  end if;
  if nvl(:old.fk_formlib,0) !=
     NVL(:new.fk_formlib,0) then
     audit_trail.column_update
       (raid, 'FK_FORMLIB',
       :old.fk_formlib, :new.fk_formlib);
  end if;
  if nvl(:old.lf_displaytype,' ') !=
     NVL(:new.lf_displaytype,' ') then
     audit_trail.column_update
       (raid, 'LF_DISPLAYTYPE',
       :old.lf_displaytype, :new.lf_displaytype);
  end if;
  if nvl(:old.lf_entrychar,' ') !=
     NVL(:new.lf_entrychar,' ') then
     audit_trail.column_update
       (raid, 'LF_ENTRYCHAR',
       :old.lf_entrychar, :new.lf_entrychar);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.fk_account, :new.fk_account);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.lf_lnkfrom,' ') !=
     NVL(:new.lf_lnkfrom,' ') then
     audit_trail.column_update
       (raid, 'LF_LNKFROM',
       :old.lf_lnkfrom, :new.lf_lnkfrom);
  end if;
  if nvl(:old.fk_calendar,0) !=
     NVL(:new.fk_calendar,0) then
     audit_trail.column_update
       (raid, 'FK_CALENDAR',
       :old.fk_calendar, :new.fk_calendar);
  end if;
  if nvl(:old.fk_event,0) !=
     NVL(:new.fk_event,0) then
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :old.fk_event, :new.fk_event);
  end if;
  if nvl(:old.fk_crf,0) !=
     NVL(:new.fk_crf,0) then
     audit_trail.column_update
       (raid, 'FK_CRF',
       :old.fk_crf, :new.fk_crf);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.lf_isirb,' ') !=
     NVL(:new.lf_isirb,' ') then
     audit_trail.column_update
       (raid, 'LF_ISIRB',
       :old.lf_isirb, :new.lf_isirb);
  end if;
  if nvl(:old.lf_submission_type,' ') !=
     NVL(:new.lf_submission_type,' ') then
     audit_trail.column_update
       (raid, 'LF_SUBMISSION_TYPE',
       :old.lf_submission_type, :new.lf_submission_type);
  end if;

 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/


