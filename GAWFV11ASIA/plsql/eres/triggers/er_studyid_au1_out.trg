CREATE OR REPLACE TRIGGER ERES.ER_STUDYTEAM_AU1_OUT
AFTER UPDATE
OF FK_CODELST_TMROLE
  ,STUDYTEAM_STATUS
ON ERES.ER_STUDYTEAM REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pkmsg NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       ER_STUDYTEAM_AU1_OUT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/23/2011    Kanwal         1. Created this trigger.

******************************************************************************/
BEGIN
SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);

  PKG_MSG_QUEUE.SP_POPULATE_STUDYTEAM_MSG(
      :NEW.PK_STUDYTEAM,
      fkaccount,
      'U',
      to_char(:NEW.fk_study), :NEW.FK_USER,  :NEW.FK_CODELST_TMROLE);
   EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ER_STUDYTEAM_AU1_OUT' || SQLERRM);

END ER_STUDYTEAM_AU1_OUT;
/