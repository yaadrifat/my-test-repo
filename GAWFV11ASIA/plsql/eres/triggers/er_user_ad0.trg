CREATE  OR REPLACE TRIGGER ER_USER_AD0 AFTER DELETE ON ER_USER        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_USER', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_JOBTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_JOBTYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SKIN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SKIN', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SPL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SPL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_THEME;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_THEME', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_GRP_DEFAULT', :OLD.FK_GRP_DEFAULT);
       Audit_Trail.column_delete (raid, 'FK_LOGINMODULE', :OLD.FK_LOGINMODULE);
       Audit_Trail.column_delete (raid, 'FK_PERADD', :OLD.FK_PERADD);
       Audit_Trail.column_delete (raid, 'FK_SITEID', :OLD.FK_SITEID);
       Audit_Trail.column_delete (raid, 'FK_TIMEZONE', :OLD.FK_TIMEZONE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_USER', :OLD.PK_USER);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'USER_CODE', :OLD.USER_CODE);
       Audit_Trail.column_delete (raid, 'USER_HIDDEN', :OLD.USER_HIDDEN);
       Audit_Trail.column_delete (raid, 'USR_ANS', :OLD.USR_ANS);
       Audit_Trail.column_delete (raid, 'USR_ATTEMPTCNT', :OLD.USR_ATTEMPTCNT);
       Audit_Trail.column_delete (raid, 'USR_CODE', :OLD.USR_CODE);
       Audit_Trail.column_delete (raid, 'USR_CURRNTSESSID', :OLD.USR_CURRNTSESSID);
       Audit_Trail.column_delete (raid, 'USR_ES', :OLD.USR_ES);
       Audit_Trail.column_delete (raid, 'USR_ESDAYS', :OLD.USR_ESDAYS);
       Audit_Trail.column_delete (raid, 'USR_ESREMIND', :OLD.USR_ESREMIND);
       Audit_Trail.column_delete (raid, 'USR_ESXPIRY', :OLD.USR_ESXPIRY);
       Audit_Trail.column_delete (raid, 'USR_FIRSTNAME', :OLD.USR_FIRSTNAME);
       Audit_Trail.column_delete (raid, 'USR_LASTNAME', :OLD.USR_LASTNAME);
       Audit_Trail.column_delete (raid, 'USR_LOGGEDINFLAG', :OLD.USR_LOGGEDINFLAG);
       Audit_Trail.column_delete (raid, 'USR_LOGINMODULEMAP', :OLD.USR_LOGINMODULEMAP);
       Audit_Trail.column_delete (raid, 'USR_LOGNAME', :OLD.USR_LOGNAME);
       Audit_Trail.column_delete (raid, 'USR_MIDNAME', :OLD.USR_MIDNAME);
       Audit_Trail.column_delete (raid, 'USR_PAHSEINV', :OLD.USR_PAHSEINV);
       Audit_Trail.column_delete (raid, 'USR_PWD', :OLD.USR_PWD);
       Audit_Trail.column_delete (raid, 'USR_PWDDAYS', :OLD.USR_PWDDAYS);
       Audit_Trail.column_delete (raid, 'USR_PWDREMIND', :OLD.USR_PWDREMIND);
       Audit_Trail.column_delete (raid, 'USR_PWDXPIRY', :OLD.USR_PWDXPIRY);
       Audit_Trail.column_delete (raid, 'USR_SECQUES', :OLD.USR_SECQUES);
       Audit_Trail.column_delete (raid, 'USR_SESSTIME', :OLD.USR_SESSTIME);
       Audit_Trail.column_delete (raid, 'USR_SITEFLAG', :OLD.USR_SITEFLAG);
       Audit_Trail.column_delete (raid, 'USR_STAT', :OLD.USR_STAT);
       Audit_Trail.column_delete (raid, 'USR_TYPE', :OLD.USR_TYPE);
       Audit_Trail.column_delete (raid, 'USR_WRKEXP', :OLD.USR_WRKEXP);
COMMIT;
END;
/
