CREATE OR REPLACE TRIGGER "ER_DYNREP_BU_LM" BEFORE UPDATE ON ER_DYNREP
FOR EACH ROW
begin :new.last_modified_date := sysdate;
end;
/


