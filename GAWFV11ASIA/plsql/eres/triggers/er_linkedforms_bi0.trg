CREATE  OR REPLACE TRIGGER ER_LINKEDFORMS_BI0 BEFORE INSERT ON ER_LINKEDFORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_LINKEDFORMS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_CALENDAR', :NEW.FK_CALENDAR);
       Audit_Trail.column_insert (raid, 'FK_CRF', :NEW.FK_CRF);
       Audit_Trail.column_insert (raid, 'FK_EVENT', :NEW.FK_EVENT);
       Audit_Trail.column_insert (raid, 'FK_FORMLIB', :NEW.FK_FORMLIB);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'LF_DATACNT', :NEW.LF_DATACNT);
       Audit_Trail.column_insert (raid, 'LF_DISPLAYTYPE', :NEW.LF_DISPLAYTYPE);
       Audit_Trail.column_insert (raid, 'LF_DISPLAY_INPAT', :NEW.LF_DISPLAY_INPAT);
       Audit_Trail.column_insert (raid, 'LF_DISPLAY_INSPEC', :NEW.LF_DISPLAY_INSPEC);
       Audit_Trail.column_insert (raid, 'LF_ENTRYCHAR', :NEW.LF_ENTRYCHAR);
       Audit_Trail.column_insert (raid, 'LF_HIDE', :NEW.LF_HIDE);
       Audit_Trail.column_insert (raid, 'LF_ISIRB', :NEW.LF_ISIRB);
       Audit_Trail.column_insert (raid, 'LF_LNKFROM', :NEW.LF_LNKFROM);
       Audit_Trail.column_insert (raid, 'LF_SEQ', :NEW.LF_SEQ);
       Audit_Trail.column_insert (raid, 'LF_SUBMISSION_TYPE', :NEW.LF_SUBMISSION_TYPE);
       Audit_Trail.column_insert (raid, 'PK_LF', :NEW.PK_LF);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
