CREATE OR REPLACE TRIGGER "ER_MOREDETAILS_AU0"
AFTER UPDATE
ON ER_MOREDETAILS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction(raid, 'ER_MOREDETAILS', :old.rid, 'U', usr);

  if nvl(:old.PK_MOREDETAILS,0) != NVL(:new.PK_MOREDETAILS,0) then
     audit_trail.column_update(raid, 'PK_MOREDETAILS',:old.PK_MOREDETAILS, :new.PK_MOREDETAILS);
  end if;

  if nvl(:old.FK_MODPK,0) !=
     NVL(:new.FK_MODPK,0) then
     audit_trail.column_update
       (raid, 'FK_MODPK',
       :old.FK_MODPK, :new.FK_MODPK);
  end if;

  if nvl(:old.MD_MODNAME,' ') !=
     NVL(:new.MD_MODNAME,' ') then
     audit_trail.column_update
       (raid, 'MD_MODNAME',
       :old.MD_MODNAME, :new.MD_MODNAME);
  end if;

  if nvl(:old.MD_MODELEMENTPK,0) !=
     NVL(:new.MD_MODELEMENTPK,0) then
     audit_trail.column_update
       (raid, 'MD_MODELEMENTPK',
       :old.MD_MODELEMENTPK, :new.MD_MODELEMENTPK);
  end if;


  if nvl(:old.MD_MODELEMENTDATA,' ') !=
     NVL(:new.MD_MODELEMENTDATA,' ') then
     audit_trail.column_update
       (raid, 'MD_MODELEMENTDATA',
       :old.MD_MODELEMENTDATA, :new.MD_MODELEMENTDATA);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
     Begin
	Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	into old_modby from er_user  where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	old_modby := null;
     End ;
     Begin
	Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
     Exception When NO_DATA_FOUND then
	new_modby := null;
     End ;
     audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') != NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;

end;
/


