CREATE OR REPLACE TRIGGER "ER_PORTAL_BU_LM" BEFORE UPDATE ON ER_PORTAL
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
begin
 :new.last_modified_date := sysdate;
end;
/


