CREATE OR REPLACE TRIGGER ER_STUDYTEAM_AU0
AFTER UPDATE
ON ER_STUDYTEAM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_codetype varchar2(200) ;
  new_codetype varchar2(200) ;
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_study varchar2(1000) ;
  new_study varchar2(1000) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYTEAM', :old.rid, 'U', usr);
  if nvl(:old.pk_studyteam,0) !=
     NVL(:new.pk_studyteam,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYTEAM',
       :old.pk_studyteam, :new.pk_studyteam);
  end if;
  if nvl(:old.fk_codelst_tmrole,0) !=
     NVL(:new.fk_codelst_tmrole,0) then
	select trim(codelst_desc) into old_codetype
	from er_codelst where pk_codelst =  :old.fk_codelst_tmrole ;
	select trim(codelst_desc) into new_codetype
	from er_codelst where pk_codelst =  :new.fk_codelst_tmrole ;
     audit_trail.column_update
       (raid, 'FK_CODELST_TMROLE',
       old_codetype, new_codetype);
  end if;
  if nvl(:old.fk_user,0) !=
     NVL(:new.fk_user,0) then
      Begin
       select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
       into old_modby
       from er_user
       where pk_user = :old.fk_user;
       Exception When no_data_found then
        old_modby := NULL;
      End ;
     Begin
      select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
      into new_modby
      from er_user
      where pk_user = :new.fk_user ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
     End ;
     audit_trail.column_update
       (raid, 'FK_USER',
       old_modby, new_modby);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
    Begin
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
     Exception When NO_DATA_FOUND then
      old_study := null ;
     End ;
     Begin
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
     Exception When NO_DATA_FOUND then
      new_study := null ;
     End ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
     End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
     End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
    if nvl(:old.study_team_rights,0) !=
     NVL(:new.study_team_rights,0) then
     audit_trail.column_update
       (raid, 'STUDY_TEAM_RIGHTS',
       :old.study_team_rights, :new.study_team_rights);
  end if;

  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
    if nvl(:old.study_team_usr_type,' ') !=
     NVL(:new.study_team_usr_type,' ') then
     audit_trail.column_update
       (raid, 'study_team_usr_type',
       :old.study_team_usr_type, :new.study_team_usr_type);
  end if;
      if nvl(:old.studyteam_status,0) !=
     NVL(:new.studyteam_status,0) then
     audit_trail.column_update
       (raid, 'STUDYTEAM_STATUS',
       :old.studyteam_status, :new.studyteam_status);
  end if;
end;
/


