CREATE  OR REPLACE TRIGGER ER_STORAGE_AD0 AFTER DELETE ON ER_STORAGE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STORAGE', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_CAPACITY_UNITS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CAPACITY_UNITS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_CHILD_STYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CHILD_STYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_STORAGE_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STORAGE_TYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_STORAGE', :OLD.FK_STORAGE);
       Audit_Trail.column_delete (raid, 'FK_STORAGE_KIT', :OLD.FK_STORAGE_KIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STORAGE', :OLD.PK_STORAGE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STORAGE_ALTERNALEID', :OLD.STORAGE_ALTERNALEID);
       Audit_Trail.column_delete (raid, 'STORAGE_AVAIL_UNIT_COUNT', :OLD.STORAGE_AVAIL_UNIT_COUNT);
       Audit_Trail.column_delete (raid, 'STORAGE_CAP_NUMBER', :OLD.STORAGE_CAP_NUMBER);
       Audit_Trail.column_delete (raid, 'STORAGE_COORDINATE_X', :OLD.STORAGE_COORDINATE_X);
       Audit_Trail.column_delete (raid, 'STORAGE_COORDINATE_Y', :OLD.STORAGE_COORDINATE_Y);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM1_CELLNUM', :OLD.STORAGE_DIM1_CELLNUM);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM1_NAMING', :OLD.STORAGE_DIM1_NAMING);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM1_ORDER', :OLD.STORAGE_DIM1_ORDER);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM2_CELLNUM', :OLD.STORAGE_DIM2_CELLNUM);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM2_NAMING', :OLD.STORAGE_DIM2_NAMING);
       Audit_Trail.column_delete (raid, 'STORAGE_DIM2_ORDER', :OLD.STORAGE_DIM2_ORDER);
       Audit_Trail.column_delete (raid, 'STORAGE_ID', :OLD.STORAGE_ID);
       Audit_Trail.column_delete (raid, 'STORAGE_ISTEMPLATE', :OLD.STORAGE_ISTEMPLATE);
       Audit_Trail.column_delete (raid, 'STORAGE_KIT_CATEGORY', :OLD.STORAGE_KIT_CATEGORY);
       Audit_Trail.column_delete (raid, 'STORAGE_LABEL', :OLD.STORAGE_LABEL);
       Audit_Trail.column_delete (raid, 'STORAGE_NAME', :OLD.STORAGE_NAME);
       Audit_Trail.column_delete (raid, 'STORAGE_NOTES', :OLD.STORAGE_NOTES);
       Audit_Trail.column_delete (raid, 'STORAGE_TEMPLATE_TYPE', :OLD.STORAGE_TEMPLATE_TYPE);
       Audit_Trail.column_delete (raid, 'STORAGE_UNIT_CLASS', :OLD.STORAGE_UNIT_CLASS);
COMMIT;
END;
/
