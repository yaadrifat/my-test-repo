CREATE OR REPLACE TRIGGER "ER_PATSTUDYSTAT_AU_REV" 
AFTER UPDATE
ON ER_PATSTUDYSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

/* **********************************
   **
   ** Author: Sonia Sahni 05/06/2004
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata
   *************************************
*/

v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_per NUMBER;
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);
v_module_name VARCHAR2(20);
v_sponsor_account NUMBER;
v_count NUMBER;
v_module_commmon VARCHAR2(20);

BEGIN

 -- get account information and site code
 v_per := :NEW.FK_PER;

  PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(:NEW.FK_STUDY, v_sponsor_study , v_site_code, v_sponsor_account, v_err);

  IF LENGTH(trim(v_site_code)) >  0 THEN

  IF v_sponsor_study > 0 THEN
  --insert data for patient status
 v_tabname := 'er_patstudystat';
 v_module_name := 'pat_enr';

 v_pk := :NEW.PK_PATSTUDYSTAT ;

 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_name,       :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
 INTO v_key FROM dual;

   INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk, RP_SITECODE,FK_STUDY,RP_MODULE)
   VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_name);

  END IF; -- end of if for v_count


    --for screened_by
    v_tabname := 'er_user';
	v_pk := :NEW.SCREENED_BY ;
	 v_module_commmon := 'pat_common';

	IF :NEW.SCREENED_BY IS NOT NULL AND :NEW.SCREENED_BY > 0 THEN
	  SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_commmon, :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
	  INTO v_count FROM dual;

      IF v_count <= 0 THEN

	  SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	  INTO v_key FROM dual;

	  INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	  VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_commmon );

	  END IF; -- for v_count
	END IF ; -- end of null check for :NEW.SCREENED_BY


   END IF; -- for if v_sponsor_study > 0
 END IF; -- for v_site_code


END;
/


