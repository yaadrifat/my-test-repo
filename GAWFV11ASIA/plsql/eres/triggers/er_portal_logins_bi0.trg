CREATE  OR REPLACE TRIGGER ER_PORTAL_LOGINS_BI0 BEFORE INSERT ON ER_PORTAL_LOGINS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PORTAL_LOGINS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_PORTAL', :NEW.FK_PORTAL);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_PORTAL_LOGIN', :NEW.PK_PORTAL_LOGIN);
       Audit_Trail.column_insert (raid, 'PL_ID', :NEW.PL_ID);
       Audit_Trail.column_insert (raid, 'PL_ID_TYPE', :NEW.PL_ID_TYPE);
       Audit_Trail.column_insert (raid, 'PL_LOGIN', :NEW.PL_LOGIN);
       Audit_Trail.column_insert (raid, 'PL_LOGOUT_TIME', :NEW.PL_LOGOUT_TIME);
       Audit_Trail.column_insert (raid, 'PL_PASSWORD', :NEW.PL_PASSWORD);
       Audit_Trail.column_insert (raid, 'PL_STATUS', :NEW.PL_STATUS);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
