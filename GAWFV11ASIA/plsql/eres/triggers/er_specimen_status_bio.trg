create or replace TRIGGER "ER_SPECIMEN_STATUS_BIO" BEFORE INSERT ON ER_SPECIMEN_STATUS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN

   BEGIN
   --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_SPECIMEN_STATUS',erid, 'I',usr);

insert_data:= :NEW.PK_SPECIMEN_STATUS||'|'||:NEW.FK_SPECIMEN||'|'||TO_CHAR(:NEW.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.FK_CODELST_STATUS||'|'||
:NEW.SS_QUANTITY||'|'||:NEW.SS_QUANTITY_UNITS||'|'||:NEW.SS_ACTION||'|'||:NEW.FK_STUDY||'|'||:NEW.FK_USER_RECEPIENT||'|'||
:NEW.SS_TRACKING_NUMBER||'|'||:NEW.SS_STATUS_BY||'|'||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
:NEW.SS_PROC_TYPE||'|'||
TO_CHAR(:NEW.SS_HAND_OFF_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
TO_CHAR(:NEW.SS_PROC_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT);

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/


