CREATE OR REPLACE TRIGGER "ER_FLDLIB_BI1" 
BEFORE INSERT
ON ER_FLDLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.FLD_SYSTEMID is null and new.FLD_LIBFLAG = 'F'
      )
declare
  sysid number(10);
  v_site_code varchar2(10);
 begin
 /*create system id of field
 *use site_code for making the system id unique across sites.
 *In case, there is no site_code specified, the systemid will also include a combination of ac_usrcreator and pk_Account.
 */

  Select nvl(site_code,ac_usrcreator || :new.fk_Account)
  into v_site_code
  from er_account
  Where  pk_account = :new.fk_Account;

  select seq_fldsystemid.nextval into sysid from dual;

  :new.FLD_SYSTEMID := substr('fld' || lower(v_site_code) || '_' || sysid || '_' || :new.pk_field ,1,50);
end;
/


