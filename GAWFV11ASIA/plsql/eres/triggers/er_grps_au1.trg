CREATE OR REPLACE TRIGGER "ERES"."ER_GRPS_AU1" AFTER UPDATE 
OF GRP_SUPUSR_FLAG
ON ER_GRPS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_GRPS_AU1', pLEVEL  => Plog.LFATAL);
BEGIN
	--User was a super user of all studies before and he is nomore a super user
	--check if he is budget creator OR inspite of being a superuser, he was a physical member of study team
	--hence his access to all budgets should be revoked.
	IF (NVL(:OLD.GRP_SUPUSR_FLAG,0)=1 AND NVL(:NEW.GRP_SUPUSR_FLAG,0)=0) THEN
		DELETE FROM esch.SCH_BGTUSERS WHERE PK_BGTUSERS IN (SELECT PK_BGTUSERS 
		FROM ER_USER usr, esch.SCH_BUDGET bgt, esch.SCH_BGTUSERS bgtUsr
		WHERE FK_GRP_DEFAULT = :NEW.PK_GRP AND bgtUsr.FK_BUDGET = bgt.PK_BUDGET AND bgtUsr.FK_USER = usr.PK_USER
		AND BGTUSERS_TYPE ='G' AND bgt.FK_STUDY IS NOT NULL AND NVL(bgt.BUDGET_RIGHTSCOPE,'X')='S'
		AND (usr.PK_USER <> bgt.creator
		AND usr.PK_USER not in (SELECT tm.fk_user FROM ER_STUDYTEAM tm WHERE tm.fk_study = bgt.FK_STUDY AND tm.study_team_usr_type <> 'X')));
	END IF;
END;
/

ALTER TRIGGER "ERES"."ER_GRPS_AU1" ENABLE
 