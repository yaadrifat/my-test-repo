CREATE  OR REPLACE TRIGGER ER_STORAGE_KIT_AD0 AFTER DELETE ON ER_STORAGE_KIT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STORAGE_KIT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'DEF_PROCESSING_TYPE', :OLD.DEF_PROCESSING_TYPE);
       Audit_Trail.column_delete (raid, 'DEF_SPECIMEN_TYPE', :OLD.DEF_SPECIMEN_TYPE);
       Audit_Trail.column_delete (raid, 'DEF_SPEC_PROCESSING_SEQ', :OLD.DEF_SPEC_PROCESSING_SEQ);
       Audit_Trail.column_delete (raid, 'DEF_SPEC_QUANTITY', :OLD.DEF_SPEC_QUANTITY);
       Audit_Trail.column_delete (raid, 'DEF_SPEC_QUANTITY_UNIT', :OLD.DEF_SPEC_QUANTITY_UNIT);
       Audit_Trail.column_delete (raid, 'FK_STORAGE', :OLD.FK_STORAGE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'KIT_SPEC_DISPOSITION', :OLD.KIT_SPEC_DISPOSITION);
       Audit_Trail.column_delete (raid, 'KIT_SPEC_ENVT_CONS', :OLD.KIT_SPEC_ENVT_CONS);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STORAGE_KIT', :OLD.PK_STORAGE_KIT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
