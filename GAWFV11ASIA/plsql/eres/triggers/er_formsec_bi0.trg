CREATE  OR REPLACE TRIGGER ER_FORMSEC_BI0 BEFORE INSERT ON ER_FORMSEC        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_FORMSEC',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_FORMLIB', :NEW.FK_FORMLIB);
       Audit_Trail.column_insert (raid, 'FORMSEC_FMT', :NEW.FORMSEC_FMT);
       Audit_Trail.column_insert (raid, 'FORMSEC_NAME', :NEW.FORMSEC_NAME);
       Audit_Trail.column_insert (raid, 'FORMSEC_OFFLINE', :NEW.FORMSEC_OFFLINE);
       Audit_Trail.column_insert (raid, 'FORMSEC_REPNO', :NEW.FORMSEC_REPNO);
       Audit_Trail.column_insert (raid, 'FORMSEC_SEQ', :NEW.FORMSEC_SEQ);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_FORMSEC', :NEW.PK_FORMSEC);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
