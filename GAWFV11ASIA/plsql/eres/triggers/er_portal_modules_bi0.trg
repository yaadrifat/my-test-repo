CREATE  OR REPLACE TRIGGER ER_PORTAL_MODULES_BI0 BEFORE INSERT ON ER_PORTAL_MODULES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PORTAL_MODULES',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_ID', :NEW.FK_ID);
       Audit_Trail.column_insert (raid, 'FK_PORTAL', :NEW.FK_PORTAL);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_PORTAL_MODULES', :NEW.PK_PORTAL_MODULES);
       Audit_Trail.column_insert (raid, 'PM_ALIGN', :NEW.PM_ALIGN);
       Audit_Trail.column_insert (raid, 'PM_FORM_AFTER_RESP', :NEW.PM_FORM_AFTER_RESP);
       Audit_Trail.column_insert (raid, 'PM_FROM', :NEW.PM_FROM);
       Audit_Trail.column_insert (raid, 'PM_FROM_UNIT', :NEW.PM_FROM_UNIT);
       Audit_Trail.column_insert (raid, 'PM_TO', :NEW.PM_TO);
       Audit_Trail.column_insert (raid, 'PM_TO_UNIT', :NEW.PM_TO_UNIT);
       Audit_Trail.column_insert (raid, 'PM_TYPE', :NEW.PM_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
