CREATE  OR REPLACE TRIGGER ER_PATTXARM_BI0 BEFORE INSERT ON ER_PATTXARM        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PATTXARM',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_STUDYTXARM', :NEW.FK_STUDYTXARM);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NOTES', :NEW.NOTES);
       Audit_Trail.column_insert (raid, 'PK_PATTXARM', :NEW.PK_PATTXARM);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'TX_DRUG_INFO', :NEW.TX_DRUG_INFO);
       Audit_Trail.column_insert (raid, 'TX_END_DATE', :NEW.TX_END_DATE);
       Audit_Trail.column_insert (raid, 'TX_START_DATE', :NEW.TX_START_DATE);
COMMIT;
END;
/
