CREATE OR REPLACE TRIGGER "ERES"."ER_PATSTUDYSTAT_BI1" 
BEFORE INSERT ON ER_PATSTUDYSTAT 
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW 

DECLARE
  statCount NUMBER;
  currrentStat Number;
BEGIN
currrentStat :=:NEW.CURRENT_STAT;

if currrentStat=1 then
select count(*) into statCount from ER_PATSTUDYSTAT  where FK_PER= :NEW.FK_PER and FK_STUDY= :NEW.FK_STUDY and CURRENT_STAT = 1;

if statCount > 0 then
  update ER_PATSTUDYSTAT set CURRENT_STAT=0 where FK_PER = :NEW.FK_PER and FK_STUDY = :NEW.FK_STUDY;
end if;

end if;
END;
/