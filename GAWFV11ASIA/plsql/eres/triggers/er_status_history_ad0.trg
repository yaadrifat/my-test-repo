CREATE  OR REPLACE TRIGGER ER_STATUS_HISTORY_AD0 AFTER DELETE ON ER_STATUS_HISTORY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STATUS_HISTORY', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STAT', codeList_desc);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STATUS', :OLD.PK_STATUS);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STATUSENDDATE', :OLD.STATUSENDDATE);
       Audit_Trail.column_delete (raid, 'STATUS_CUSTOM1', :OLD.STATUS_CUSTOM1);
       Audit_Trail.column_delete (raid, 'STATUS_DATE', :OLD.STATUS_DATE);
       Audit_Trail.column_delete (raid, 'STATUS_END_DATE', :OLD.STATUS_END_DATE);
       Audit_Trail.column_delete (raid, 'STATUS_ENTERED_BY', :OLD.STATUS_ENTERED_BY);
       Audit_Trail.column_delete (raid, 'STATUS_ISCURRENT', :OLD.STATUS_ISCURRENT);
       Audit_Trail.column_delete (raid, 'STATUS_MODPK', :OLD.STATUS_MODPK);
       Audit_Trail.column_delete (raid, 'STATUS_MODTABLE', :OLD.STATUS_MODTABLE);
       Audit_Trail.column_delete (raid, 'STATUS_NOTES', :OLD.STATUS_NOTES);
COMMIT;
END;
/