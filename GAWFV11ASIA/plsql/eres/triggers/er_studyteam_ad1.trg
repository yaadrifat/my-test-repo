CREATE OR REPLACE TRIGGER "ER_STUDYTEAM_AD1" 
AFTER DELETE
ON ER_STUDYTEAM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE


 v_old_fk_study NUMBER;
 v_old_fk_user NUMBER;
 v_old_pk NUMBER;--JM;

/* Author : Sonia Sahni
Purpose : Remove the deleted user from the study budget access rights
*/
BEGIN


 v_old_fk_study :=  :OLD.fk_study ;
 v_old_fk_user := :OLD.fk_user ;

 --JM: 08Nov2006
 v_old_pk := :OLD.PK_STUDYTEAM;


 DELETE FROM ER_STATUS_HISTORY WHERE status_modtable ='er_studyteam' AND
  status_modpk IN (v_old_pk );


Pkg_Studystat.sp_revoke_studyteam_access(v_old_fk_study , v_old_fk_user);


END;
/


