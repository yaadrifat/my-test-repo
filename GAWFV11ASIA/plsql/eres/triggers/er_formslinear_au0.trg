CREATE OR REPLACE TRIGGER "ER_FORMSLINEAR_AU0" 
AFTER UPDATE
ON ER_FORMSLINEAR 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
usrName VARCHAR2(500);
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_FL_TRIGGER', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/13/2006             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:
      Sysdate:         3/13/2006
      Date and Time:   3/13/2006, 9:29:57 AM, and 3/13/2006 9:29:57 AM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:       (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   tmpVar := 0;
   SELECT Getuser(:NEW.last_modified_by)INTO usrName FROM dual;

IF NVL(:OLD.COL1,' ') != NVL(:NEW.COL1,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col1'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col1'),:OLD.COL1,:NEW.COL1,usrName);
  END IF;

IF NVL(:OLD.COL2,' ') != NVL(:NEW.COL2,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col2'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col2'),:OLD.COL2,:NEW.COL2,usrName);
  END IF;

IF NVL(:OLD.COL3,' ') != NVL(:NEW.COL3,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col3'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col3'),:OLD.COL3,:NEW.COL3,usrName);
  END IF;

IF NVL(:OLD.COL4,' ') != NVL(:NEW.COL4,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col4'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col4'),:OLD.COL4,:NEW.COL4,usrName);
  END IF;

IF NVL(:OLD.COL5,' ') != NVL(:NEW.COL5,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col5'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col5'),:OLD.COL5,:NEW.COL5,usrName);
  END IF;

IF NVL(:OLD.COL6,' ') != NVL(:NEW.COL6,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col6'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col6'),:OLD.COL6,:NEW.COL6,usrName);
  END IF;

IF NVL(:OLD.COL7,' ') != NVL(:NEW.COL7,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col7'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col7'),:OLD.COL7,:NEW.COL7,usrName);
  END IF;

IF NVL(:OLD.COL8,' ') != NVL(:NEW.COL8,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col8'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col8'),:OLD.COL8,:NEW.COL8,usrName);
  END IF;

IF NVL(:OLD.COL9,' ') != NVL(:NEW.COL9,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col9'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col9'),:OLD.COL9,:NEW.COL9,usrName);
  END IF;

IF NVL(:OLD.COL10,' ') != NVL(:NEW.COL10,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col10'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col10'),:OLD.COL10,:NEW.COL10,usrName);
  END IF;

IF NVL(:OLD.COL11,' ') != NVL(:NEW.COL11,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col11'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col11'),:OLD.COL11,:NEW.COL11,usrName);
  END IF;

IF NVL(:OLD.COL12,' ') != NVL(:NEW.COL12,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col12'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col12'),:OLD.COL12,:NEW.COL12,usrName);
  END IF;

IF NVL(:OLD.COL13,' ') != NVL(:NEW.COL13,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col13'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col13'),:OLD.COL13,:NEW.COL13,usrName);
  END IF;

IF NVL(:OLD.COL14,' ') != NVL(:NEW.COL14,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col14'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col14'),:OLD.COL14,:NEW.COL14,usrName);
  END IF;

IF NVL(:OLD.COL15,' ') != NVL(:NEW.COL15,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col15'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col15'),:OLD.COL15,:NEW.COL15,usrName);
  END IF;

IF NVL(:OLD.COL16,' ') != NVL(:NEW.COL16,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col16'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col16'),:OLD.COL16,:NEW.COL16,usrName);
  END IF;

IF NVL(:OLD.COL17,' ') != NVL(:NEW.COL17,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col17'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col17'),:OLD.COL17,:NEW.COL17,usrName);
  END IF;

IF NVL(:OLD.COL18,' ') != NVL(:NEW.COL18,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col18'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col18'),:OLD.COL18,:NEW.COL18,usrName);
  END IF;

IF NVL(:OLD.COL19,' ') != NVL(:NEW.COL19,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col19'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col19'),:OLD.COL19,:NEW.COL19,usrName);
  END IF;

IF NVL(:OLD.COL20,' ') != NVL(:NEW.COL20,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col20'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col20'),:OLD.COL20,:NEW.COL20,usrName);
  END IF;

IF NVL(:OLD.COL21,' ') != NVL(:NEW.COL21,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col21'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col21'),:OLD.COL21,:NEW.COL21,usrName);
  END IF;

IF NVL(:OLD.COL22,' ') != NVL(:NEW.COL22,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col22'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col22'),:OLD.COL22,:NEW.COL22,usrName);
  END IF;

IF NVL(:OLD.COL23,' ') != NVL(:NEW.COL23,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col23'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col23'),:OLD.COL23,:NEW.COL23,usrName);
  END IF;

IF NVL(:OLD.COL24,' ') != NVL(:NEW.COL24,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col24'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col24'),:OLD.COL24,:NEW.COL24,usrName);
  END IF;

IF NVL(:OLD.COL25,' ') != NVL(:NEW.COL25,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col25'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col25'),:OLD.COL25,:NEW.COL25,usrName);
  END IF;

IF NVL(:OLD.COL26,' ') != NVL(:NEW.COL26,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col26'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col26'),:OLD.COL26,:NEW.COL26,usrName);
  END IF;

IF NVL(:OLD.COL27,' ') != NVL(:NEW.COL27,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col27'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col27'),:OLD.COL27,:NEW.COL27,usrName);
  END IF;

IF NVL(:OLD.COL28,' ') != NVL(:NEW.COL28,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col28'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col28'),:OLD.COL28,:NEW.COL28,usrName);
  END IF;

IF NVL(:OLD.COL29,' ') != NVL(:NEW.COL29,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col29'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col29'),:OLD.COL29,:NEW.COL29,usrName);
  END IF;

IF NVL(:OLD.COL30,' ') != NVL(:NEW.COL30,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col30'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col30'),:OLD.COL30,:NEW.COL30,usrName);
  END IF;

IF NVL(:OLD.COL31,' ') != NVL(:NEW.COL31,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col31'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col31'),:OLD.COL31,:NEW.COL31,usrName);
  END IF;

IF NVL(:OLD.COL32,' ') != NVL(:NEW.COL32,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col32'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col32'),:OLD.COL32,:NEW.COL32,usrName);
  END IF;

IF NVL(:OLD.COL33,' ') != NVL(:NEW.COL33,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col33'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col33'),:OLD.COL33,:NEW.COL33,usrName);
  END IF;

IF NVL(:OLD.COL34,' ') != NVL(:NEW.COL34,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col34'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col34'),:OLD.COL34,:NEW.COL34,usrName);
  END IF;

IF NVL(:OLD.COL35,' ') != NVL(:NEW.COL35,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col35'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col35'),:OLD.COL35,:NEW.COL35,usrName);
  END IF;

IF NVL(:OLD.COL36,' ') != NVL(:NEW.COL36,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col36'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col36'),:OLD.COL36,:NEW.COL36,usrName);
  END IF;

IF NVL(:OLD.COL37,' ') != NVL(:NEW.COL37,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col37'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col37'),:OLD.COL37,:NEW.COL37,usrName);
  END IF;

IF NVL(:OLD.COL38,' ') != NVL(:NEW.COL38,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col38'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col38'),:OLD.COL38,:NEW.COL38,usrName);
  END IF;

IF NVL(:OLD.COL39,' ') != NVL(:NEW.COL39,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col39'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col39'),:OLD.COL39,:NEW.COL39,usrName);
  END IF;

IF NVL(:OLD.COL40,' ') != NVL(:NEW.COL40,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col40'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col40'),:OLD.COL40,:NEW.COL40,usrName);
  END IF;

IF NVL(:OLD.COL41,' ') != NVL(:NEW.COL41,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col41'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col41'),:OLD.COL41,:NEW.COL41,usrName);
  END IF;

IF NVL(:OLD.COL42,' ') != NVL(:NEW.COL42,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col42'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col42'),:OLD.COL42,:NEW.COL42,usrName);
  END IF;

IF NVL(:OLD.COL43,' ') != NVL(:NEW.COL43,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col43'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col43'),:OLD.COL43,:NEW.COL43,usrName);
  END IF;

IF NVL(:OLD.COL44,' ') != NVL(:NEW.COL44,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col44'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col44'),:OLD.COL44,:NEW.COL44,usrName);
  END IF;

IF NVL(:OLD.COL45,' ') != NVL(:NEW.COL45,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col45'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col45'),:OLD.COL45,:NEW.COL45,usrName);
  END IF;

IF NVL(:OLD.COL46,' ') != NVL(:NEW.COL46,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col46'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col46'),:OLD.COL46,:NEW.COL46,usrName);
  END IF;

IF NVL(:OLD.COL47,' ') != NVL(:NEW.COL47,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col47'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col47'),:OLD.COL47,:NEW.COL47,usrName);
  END IF;

IF NVL(:OLD.COL48,' ') != NVL(:NEW.COL48,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col48'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col48'),:OLD.COL48,:NEW.COL48,usrName);
  END IF;

IF NVL(:OLD.COL49,' ') != NVL(:NEW.COL49,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col49'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col49'),:OLD.COL49,:NEW.COL49,usrName);
  END IF;

IF NVL(:OLD.COL50,' ') != NVL(:NEW.COL50,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col50'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col50'),:OLD.COL50,:NEW.COL50,usrName);
  END IF;

IF NVL(:OLD.COL51,' ') != NVL(:NEW.COL51,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col51'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col51'),:OLD.COL51,:NEW.COL51,usrName);
  END IF;

IF NVL(:OLD.COL52,' ') != NVL(:NEW.COL52,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col52'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col52'),:OLD.COL52,:NEW.COL52,usrName);
  END IF;

IF NVL(:OLD.COL53,' ') != NVL(:NEW.COL53,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col53'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col53'),:OLD.COL53,:NEW.COL53,usrName);
  END IF;

IF NVL(:OLD.COL54,' ') != NVL(:NEW.COL54,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col54'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col54'),:OLD.COL54,:NEW.COL54,usrName);
  END IF;

IF NVL(:OLD.COL55,' ') != NVL(:NEW.COL55,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col55'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col55'),:OLD.COL55,:NEW.COL55,usrName);
  END IF;

IF NVL(:OLD.COL56,' ') != NVL(:NEW.COL56,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col56'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col56'),:OLD.COL56,:NEW.COL56,usrName);
  END IF;

IF NVL(:OLD.COL57,' ') != NVL(:NEW.COL57,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col57'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col57'),:OLD.COL57,:NEW.COL57,usrName);
  END IF;

IF NVL(:OLD.COL58,' ') != NVL(:NEW.COL58,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col58'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col58'),:OLD.COL58,:NEW.COL58,usrName);
  END IF;

IF NVL(:OLD.COL59,' ') != NVL(:NEW.COL59,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col59'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col59'),:OLD.COL59,:NEW.COL59,usrName);
  END IF;

IF NVL(:OLD.COL60,' ') != NVL(:NEW.COL60,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col60'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col60'),:OLD.COL60,:NEW.COL60,usrName);
  END IF;

IF NVL(:OLD.COL61,' ') != NVL(:NEW.COL61,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col61'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col61'),:OLD.COL61,:NEW.COL61,usrName);
  END IF;

IF NVL(:OLD.COL62,' ') != NVL(:NEW.COL62,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col62'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col62'),:OLD.COL62,:NEW.COL62,usrName);
  END IF;

IF NVL(:OLD.COL63,' ') != NVL(:NEW.COL63,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col63'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col63'),:OLD.COL63,:NEW.COL63,usrName);
  END IF;

IF NVL(:OLD.COL64,' ') != NVL(:NEW.COL64,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col64'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col64'),:OLD.COL64,:NEW.COL64,usrName);
  END IF;

IF NVL(:OLD.COL65,' ') != NVL(:NEW.COL65,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col65'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col65'),:OLD.COL65,:NEW.COL65,usrName);
  END IF;

IF NVL(:OLD.COL66,' ') != NVL(:NEW.COL66,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col66'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col66'),:OLD.COL66,:NEW.COL66,usrName);
  END IF;

IF NVL(:OLD.COL67,' ') != NVL(:NEW.COL67,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col67'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col67'),:OLD.COL67,:NEW.COL67,usrName);
  END IF;

IF NVL(:OLD.COL68,' ') != NVL(:NEW.COL68,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col68'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col68'),:OLD.COL68,:NEW.COL68,usrName);
  END IF;

IF NVL(:OLD.COL69,' ') != NVL(:NEW.COL69,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col69'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col69'),:OLD.COL69,:NEW.COL69,usrName);
  END IF;

IF NVL(:OLD.COL70,' ') != NVL(:NEW.COL70,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col70'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col70'),:OLD.COL70,:NEW.COL70,usrName);
  END IF;

IF NVL(:OLD.COL71,' ') != NVL(:NEW.COL71,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col71'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col71'),:OLD.COL71,:NEW.COL71,usrName);
  END IF;

IF NVL(:OLD.COL72,' ') != NVL(:NEW.COL72,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col72'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col72'),:OLD.COL72,:NEW.COL72,usrName);
  END IF;

IF NVL(:OLD.COL73,' ') != NVL(:NEW.COL73,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col73'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col73'),:OLD.COL73,:NEW.COL73,usrName);
  END IF;

IF NVL(:OLD.COL74,' ') != NVL(:NEW.COL74,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col74'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col74'),:OLD.COL74,:NEW.COL74,usrName);
  END IF;

IF NVL(:OLD.COL75,' ') != NVL(:NEW.COL75,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col75'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col75'),:OLD.COL75,:NEW.COL75,usrName);
  END IF;

IF NVL(:OLD.COL76,' ') != NVL(:NEW.COL76,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col76'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col76'),:OLD.COL76,:NEW.COL76,usrName);
  END IF;

IF NVL(:OLD.COL77,' ') != NVL(:NEW.COL77,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col77'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col77'),:OLD.COL77,:NEW.COL77,usrName);
  END IF;

IF NVL(:OLD.COL78,' ') != NVL(:NEW.COL78,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col78'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col78'),:OLD.COL78,:NEW.COL78,usrName);
  END IF;

IF NVL(:OLD.COL79,' ') != NVL(:NEW.COL79,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col79'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col79'),:OLD.COL79,:NEW.COL79,usrName);
  END IF;

IF NVL(:OLD.COL80,' ') != NVL(:NEW.COL80,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col80'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col80'),:OLD.COL80,:NEW.COL80,usrName);
  END IF;

IF NVL(:OLD.COL81,' ') != NVL(:NEW.COL81,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col81'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col81'),:OLD.COL81,:NEW.COL81,usrName);
  END IF;

IF NVL(:OLD.COL82,' ') != NVL(:NEW.COL82,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col82'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col82'),:OLD.COL82,:NEW.COL82,usrName);
  END IF;

IF NVL(:OLD.COL83,' ') != NVL(:NEW.COL83,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col83'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col83'),:OLD.COL83,:NEW.COL83,usrName);
  END IF;

IF NVL(:OLD.COL84,' ') != NVL(:NEW.COL84,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col84'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col84'),:OLD.COL84,:NEW.COL84,usrName);
  END IF;

IF NVL(:OLD.COL85,' ') != NVL(:NEW.COL85,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col85'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col85'),:OLD.COL85,:NEW.COL85,usrName);
  END IF;

IF NVL(:OLD.COL86,' ') != NVL(:NEW.COL86,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col86'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col86'),:OLD.COL86,:NEW.COL86,usrName);
  END IF;

IF NVL(:OLD.COL87,' ') != NVL(:NEW.COL87,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col87'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col87'),:OLD.COL87,:NEW.COL87,usrName);
  END IF;

IF NVL(:OLD.COL88,' ') != NVL(:NEW.COL88,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col88'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col88'),:OLD.COL88,:NEW.COL88,usrName);
  END IF;

IF NVL(:OLD.COL89,' ') != NVL(:NEW.COL89,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col89'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col89'),:OLD.COL89,:NEW.COL89,usrName);
  END IF;

IF NVL(:OLD.COL90,' ') != NVL(:NEW.COL90,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col90'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col90'),:OLD.COL90,:NEW.COL90,usrName);
  END IF;

IF NVL(:OLD.COL91,' ') != NVL(:NEW.COL91,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col91'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col91'),:OLD.COL91,:NEW.COL91,usrName);
  END IF;

IF NVL(:OLD.COL92,' ') != NVL(:NEW.COL92,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col92'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col92'),:OLD.COL92,:NEW.COL92,usrName);
  END IF;

IF NVL(:OLD.COL93,' ') != NVL(:NEW.COL93,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col93'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col93'),:OLD.COL93,:NEW.COL93,usrName);
  END IF;

IF NVL(:OLD.COL94,' ') != NVL(:NEW.COL94,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col94'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col94'),:OLD.COL94,:NEW.COL94,usrName);
  END IF;

IF NVL(:OLD.COL95,' ') != NVL(:NEW.COL95,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col95'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col95'),:OLD.COL95,:NEW.COL95,usrName);
  END IF;

IF NVL(:OLD.COL96,' ') != NVL(:NEW.COL96,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col96'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col96'),:OLD.COL96,:NEW.COL96,usrName);
  END IF;

IF NVL(:OLD.COL97,' ') != NVL(:NEW.COL97,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col97'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col97'),:OLD.COL97,:NEW.COL97,usrName);
  END IF;

IF NVL(:OLD.COL98,' ') != NVL(:NEW.COL98,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col98'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col98'),:OLD.COL98,:NEW.COL98,usrName);
  END IF;

IF NVL(:OLD.COL99,' ') != NVL(:NEW.COL99,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col99'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col99'),:OLD.COL99,:NEW.COL99,usrName);
  END IF;

IF NVL(:OLD.COL100,' ') != NVL(:NEW.COL100,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col100'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col100'),:OLD.COL100,:NEW.COL100,usrName);
  END IF;

IF NVL(:OLD.COL101,' ') != NVL(:NEW.COL101,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col101'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col101'),:OLD.COL101,:NEW.COL101,usrName);
  END IF;

IF NVL(:OLD.COL102,' ') != NVL(:NEW.COL102,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col102'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col102'),:OLD.COL102,:NEW.COL102,usrName);
  END IF;

IF NVL(:OLD.COL103,' ') != NVL(:NEW.COL103,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col103'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col103'),:OLD.COL103,:NEW.COL103,usrName);
  END IF;

IF NVL(:OLD.COL104,' ') != NVL(:NEW.COL104,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col104'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col104'),:OLD.COL104,:NEW.COL104,usrName);
  END IF;

IF NVL(:OLD.COL105,' ') != NVL(:NEW.COL105,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col105'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col105'),:OLD.COL105,:NEW.COL105,usrName);
  END IF;

IF NVL(:OLD.COL106,' ') != NVL(:NEW.COL106,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col106'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col106'),:OLD.COL106,:NEW.COL106,usrName);
  END IF;

IF NVL(:OLD.COL107,' ') != NVL(:NEW.COL107,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col107'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col107'),:OLD.COL107,:NEW.COL107,usrName);
  END IF;

IF NVL(:OLD.COL108,' ') != NVL(:NEW.COL108,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col108'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col108'),:OLD.COL108,:NEW.COL108,usrName);
  END IF;

IF NVL(:OLD.COL109,' ') != NVL(:NEW.COL109,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col109'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col109'),:OLD.COL109,:NEW.COL109,usrName);
  END IF;

IF NVL(:OLD.COL110,' ') != NVL(:NEW.COL110,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col110'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col110'),:OLD.COL110,:NEW.COL110,usrName);
  END IF;

IF NVL(:OLD.COL111,' ') != NVL(:NEW.COL111,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col111'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col111'),:OLD.COL111,:NEW.COL111,usrName);
  END IF;

IF NVL(:OLD.COL112,' ') != NVL(:NEW.COL112,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col112'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col112'),:OLD.COL112,:NEW.COL112,usrName);
  END IF;

IF NVL(:OLD.COL113,' ') != NVL(:NEW.COL113,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col113'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col113'),:OLD.COL113,:NEW.COL113,usrName);
  END IF;

IF NVL(:OLD.COL114,' ') != NVL(:NEW.COL114,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col114'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col114'),:OLD.COL114,:NEW.COL114,usrName);
  END IF;

IF NVL(:OLD.COL115,' ') != NVL(:NEW.COL115,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col115'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col115'),:OLD.COL115,:NEW.COL115,usrName);
  END IF;

IF NVL(:OLD.COL116,' ') != NVL(:NEW.COL116,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col116'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col116'),:OLD.COL116,:NEW.COL116,usrName);
  END IF;

IF NVL(:OLD.COL117,' ') != NVL(:NEW.COL117,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col117'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col117'),:OLD.COL117,:NEW.COL117,usrName);
  END IF;

IF NVL(:OLD.COL118,' ') != NVL(:NEW.COL118,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col118'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col118'),:OLD.COL118,:NEW.COL118,usrName);
  END IF;

IF NVL(:OLD.COL119,' ') != NVL(:NEW.COL119,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col119'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col119'),:OLD.COL119,:NEW.COL119,usrName);
  END IF;

IF NVL(:OLD.COL120,' ') != NVL(:NEW.COL120,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col120'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col120'),:OLD.COL120,:NEW.COL120,usrName);
  END IF;

IF NVL(:OLD.COL121,' ') != NVL(:NEW.COL121,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col121'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col121'),:OLD.COL121,:NEW.COL121,usrName);
  END IF;

IF NVL(:OLD.COL122,' ') != NVL(:NEW.COL122,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col122'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col122'),:OLD.COL122,:NEW.COL122,usrName);
  END IF;

IF NVL(:OLD.COL123,' ') != NVL(:NEW.COL123,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col123'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col123'),:OLD.COL123,:NEW.COL123,usrName);
  END IF;

IF NVL(:OLD.COL124,' ') != NVL(:NEW.COL124,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col124'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col124'),:OLD.COL124,:NEW.COL124,usrName);
  END IF;

IF NVL(:OLD.COL125,' ') != NVL(:NEW.COL125,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col125'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col125'),:OLD.COL125,:NEW.COL125,usrName);
  END IF;

IF NVL(:OLD.COL126,' ') != NVL(:NEW.COL126,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col126'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col126'),:OLD.COL126,:NEW.COL126,usrName);
  END IF;

IF NVL(:OLD.COL127,' ') != NVL(:NEW.COL127,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col127'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col127'),:OLD.COL127,:NEW.COL127,usrName);
  END IF;

IF NVL(:OLD.COL128,' ') != NVL(:NEW.COL128,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col128'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col128'),:OLD.COL128,:NEW.COL128,usrName);
  END IF;

IF NVL(:OLD.COL129,' ') != NVL(:NEW.COL129,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col129'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col129'),:OLD.COL129,:NEW.COL129,usrName);
  END IF;

IF NVL(:OLD.COL130,' ') != NVL(:NEW.COL130,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col130'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col130'),:OLD.COL130,:NEW.COL130,usrName);
  END IF;

IF NVL(:OLD.COL131,' ') != NVL(:NEW.COL131,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col131'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col131'),:OLD.COL131,:NEW.COL131,usrName);
  END IF;

IF NVL(:OLD.COL132,' ') != NVL(:NEW.COL132,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col132'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col132'),:OLD.COL132,:NEW.COL132,usrName);
  END IF;

IF NVL(:OLD.COL133,' ') != NVL(:NEW.COL133,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col133'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col133'),:OLD.COL133,:NEW.COL133,usrName);
  END IF;

IF NVL(:OLD.COL134,' ') != NVL(:NEW.COL134,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col134'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col134'),:OLD.COL134,:NEW.COL134,usrName);
  END IF;

IF NVL(:OLD.COL135,' ') != NVL(:NEW.COL135,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col135'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col135'),:OLD.COL135,:NEW.COL135,usrName);
  END IF;

IF NVL(:OLD.COL136,' ') != NVL(:NEW.COL136,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col136'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col136'),:OLD.COL136,:NEW.COL136,usrName);
  END IF;

IF NVL(:OLD.COL137,' ') != NVL(:NEW.COL137,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col137'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col137'),:OLD.COL137,:NEW.COL137,usrName);
  END IF;

IF NVL(:OLD.COL138,' ') != NVL(:NEW.COL138,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col138'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col138'),:OLD.COL138,:NEW.COL138,usrName);
  END IF;

IF NVL(:OLD.COL139,' ') != NVL(:NEW.COL139,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col139'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col139'),:OLD.COL139,:NEW.COL139,usrName);
  END IF;

IF NVL(:OLD.COL140,' ') != NVL(:NEW.COL140,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col140'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col140'),:OLD.COL140,:NEW.COL140,usrName);
  END IF;

IF NVL(:OLD.COL141,' ') != NVL(:NEW.COL141,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col141'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col141'),:OLD.COL141,:NEW.COL141,usrName);
  END IF;

IF NVL(:OLD.COL142,' ') != NVL(:NEW.COL142,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col142'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col142'),:OLD.COL142,:NEW.COL142,usrName);
  END IF;

IF NVL(:OLD.COL143,' ') != NVL(:NEW.COL143,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col143'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col143'),:OLD.COL143,:NEW.COL143,usrName);
  END IF;

IF NVL(:OLD.COL144,' ') != NVL(:NEW.COL144,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col144'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col144'),:OLD.COL144,:NEW.COL144,usrName);
  END IF;

IF NVL(:OLD.COL145,' ') != NVL(:NEW.COL145,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col145'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col145'),:OLD.COL145,:NEW.COL145,usrName);
  END IF;

IF NVL(:OLD.COL146,' ') != NVL(:NEW.COL146,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col146'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col146'),:OLD.COL146,:NEW.COL146,usrName);
  END IF;

IF NVL(:OLD.COL147,' ') != NVL(:NEW.COL147,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col147'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col147'),:OLD.COL147,:NEW.COL147,usrName);
  END IF;

IF NVL(:OLD.COL148,' ') != NVL(:NEW.COL148,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col148'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col148'),:OLD.COL148,:NEW.COL148,usrName);
  END IF;

IF NVL(:OLD.COL149,' ') != NVL(:NEW.COL149,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col149'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col149'),:OLD.COL149,:NEW.COL149,usrName);
  END IF;

IF NVL(:OLD.COL150,' ') != NVL(:NEW.COL150,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col150'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col150'),:OLD.COL150,:NEW.COL150,usrName);
  END IF;

IF NVL(:OLD.COL151,' ') != NVL(:NEW.COL151,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col151'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col151'),:OLD.COL151,:NEW.COL151,usrName);
  END IF;

IF NVL(:OLD.COL152,' ') != NVL(:NEW.COL152,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col152'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col152'),:OLD.COL152,:NEW.COL152,usrName);
  END IF;

IF NVL(:OLD.COL153,' ') != NVL(:NEW.COL153,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col153'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col153'),:OLD.COL153,:NEW.COL153,usrName);
  END IF;

IF NVL(:OLD.COL154,' ') != NVL(:NEW.COL154,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col154'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col154'),:OLD.COL154,:NEW.COL154,usrName);
  END IF;

IF NVL(:OLD.COL155,' ') != NVL(:NEW.COL155,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col155'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col155'),:OLD.COL155,:NEW.COL155,usrName);
  END IF;

IF NVL(:OLD.COL156,' ') != NVL(:NEW.COL156,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col156'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col156'),:OLD.COL156,:NEW.COL156,usrName);
  END IF;

IF NVL(:OLD.COL157,' ') != NVL(:NEW.COL157,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col157'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col157'),:OLD.COL157,:NEW.COL157,usrName);
  END IF;

IF NVL(:OLD.COL158,' ') != NVL(:NEW.COL158,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col158'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col158'),:OLD.COL158,:NEW.COL158,usrName);
  END IF;

IF NVL(:OLD.COL159,' ') != NVL(:NEW.COL159,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col159'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col159'),:OLD.COL159,:NEW.COL159,usrName);
  END IF;

IF NVL(:OLD.COL160,' ') != NVL(:NEW.COL160,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col160'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col160'),:OLD.COL160,:NEW.COL160,usrName);
  END IF;

IF NVL(:OLD.COL161,' ') != NVL(:NEW.COL161,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col161'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col161'),:OLD.COL161,:NEW.COL161,usrName);
  END IF;

IF NVL(:OLD.COL162,' ') != NVL(:NEW.COL162,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col162'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col162'),:OLD.COL162,:NEW.COL162,usrName);
  END IF;

IF NVL(:OLD.COL163,' ') != NVL(:NEW.COL163,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col163'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col163'),:OLD.COL163,:NEW.COL163,usrName);
  END IF;

IF NVL(:OLD.COL164,' ') != NVL(:NEW.COL164,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col164'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col164'),:OLD.COL164,:NEW.COL164,usrName);
  END IF;

IF NVL(:OLD.COL165,' ') != NVL(:NEW.COL165,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col165'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col165'),:OLD.COL165,:NEW.COL165,usrName);
  END IF;

IF NVL(:OLD.COL166,' ') != NVL(:NEW.COL166,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col166'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col166'),:OLD.COL166,:NEW.COL166,usrName);
  END IF;

IF NVL(:OLD.COL167,' ') != NVL(:NEW.COL167,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col167'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col167'),:OLD.COL167,:NEW.COL167,usrName);
  END IF;

IF NVL(:OLD.COL168,' ') != NVL(:NEW.COL168,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col168'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col168'),:OLD.COL168,:NEW.COL168,usrName);
  END IF;

IF NVL(:OLD.COL169,' ') != NVL(:NEW.COL169,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col169'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col169'),:OLD.COL169,:NEW.COL169,usrName);
  END IF;

IF NVL(:OLD.COL170,' ') != NVL(:NEW.COL170,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col170'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col170'),:OLD.COL170,:NEW.COL170,usrName);
  END IF;

IF NVL(:OLD.COL171,' ') != NVL(:NEW.COL171,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col171'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col171'),:OLD.COL171,:NEW.COL171,usrName);
  END IF;

IF NVL(:OLD.COL172,' ') != NVL(:NEW.COL172,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col172'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col172'),:OLD.COL172,:NEW.COL172,usrName);
  END IF;

IF NVL(:OLD.COL173,' ') != NVL(:NEW.COL173,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col173'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col173'),:OLD.COL173,:NEW.COL173,usrName);
  END IF;

IF NVL(:OLD.COL174,' ') != NVL(:NEW.COL174,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col174'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col174'),:OLD.COL174,:NEW.COL174,usrName);
  END IF;

IF NVL(:OLD.COL175,' ') != NVL(:NEW.COL175,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col175'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col175'),:OLD.COL175,:NEW.COL175,usrName);
  END IF;

IF NVL(:OLD.COL176,' ') != NVL(:NEW.COL176,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col176'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col176'),:OLD.COL176,:NEW.COL176,usrName);
  END IF;

IF NVL(:OLD.COL177,' ') != NVL(:NEW.COL177,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col177'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col177'),:OLD.COL177,:NEW.COL177,usrName);
  END IF;

IF NVL(:OLD.COL178,' ') != NVL(:NEW.COL178,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col178'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col178'),:OLD.COL178,:NEW.COL178,usrName);
  END IF;

IF NVL(:OLD.COL179,' ') != NVL(:NEW.COL179,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col179'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col179'),:OLD.COL179,:NEW.COL179,usrName);
  END IF;

IF NVL(:OLD.COL180,' ') != NVL(:NEW.COL180,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col180'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col180'),:OLD.COL180,:NEW.COL180,usrName);
  END IF;

IF NVL(:OLD.COL181,' ') != NVL(:NEW.COL181,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col181'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col181'),:OLD.COL181,:NEW.COL181,usrName);
  END IF;

IF NVL(:OLD.COL182,' ') != NVL(:NEW.COL182,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col182'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col182'),:OLD.COL182,:NEW.COL182,usrName);
  END IF;

IF NVL(:OLD.COL183,' ') != NVL(:NEW.COL183,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col183'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col183'),:OLD.COL183,:NEW.COL183,usrName);
  END IF;

IF NVL(:OLD.COL184,' ') != NVL(:NEW.COL184,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col184'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col184'),:OLD.COL184,:NEW.COL184,usrName);
  END IF;

IF NVL(:OLD.COL185,' ') != NVL(:NEW.COL185,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col185'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col185'),:OLD.COL185,:NEW.COL185,usrName);
  END IF;

IF NVL(:OLD.COL186,' ') != NVL(:NEW.COL186,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col186'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col186'),:OLD.COL186,:NEW.COL186,usrName);
  END IF;

IF NVL(:OLD.COL187,' ') != NVL(:NEW.COL187,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col187'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col187'),:OLD.COL187,:NEW.COL187,usrName);
  END IF;

IF NVL(:OLD.COL188,' ') != NVL(:NEW.COL188,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col188'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col188'),:OLD.COL188,:NEW.COL188,usrName);
  END IF;

IF NVL(:OLD.COL189,' ') != NVL(:NEW.COL189,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col189'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col189'),:OLD.COL189,:NEW.COL189,usrName);
  END IF;

IF NVL(:OLD.COL190,' ') != NVL(:NEW.COL190,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col190'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col190'),:OLD.COL190,:NEW.COL190,usrName);
  END IF;

IF NVL(:OLD.COL191,' ') != NVL(:NEW.COL191,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col191'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col191'),:OLD.COL191,:NEW.COL191,usrName);
  END IF;

IF NVL(:OLD.COL192,' ') != NVL(:NEW.COL192,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col192'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col192'),:OLD.COL192,:NEW.COL192,usrName);
  END IF;

IF NVL(:OLD.COL193,' ') != NVL(:NEW.COL193,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col193'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col193'),:OLD.COL193,:NEW.COL193,usrName);
  END IF;

IF NVL(:OLD.COL194,' ') != NVL(:NEW.COL194,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col194'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col194'),:OLD.COL194,:NEW.COL194,usrName);
  END IF;

IF NVL(:OLD.COL195,' ') != NVL(:NEW.COL195,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col195'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col195'),:OLD.COL195,:NEW.COL195,usrName);
  END IF;

IF NVL(:OLD.COL196,' ') != NVL(:NEW.COL196,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col196'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col196'),:OLD.COL196,:NEW.COL196,usrName);
  END IF;

IF NVL(:OLD.COL197,' ') != NVL(:NEW.COL197,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col197'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col197'),:OLD.COL197,:NEW.COL197,usrName);
  END IF;

IF NVL(:OLD.COL198,' ') != NVL(:NEW.COL198,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col198'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col198'),:OLD.COL198,:NEW.COL198,usrName);
  END IF;

IF NVL(:OLD.COL199,' ') != NVL(:NEW.COL199,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col199'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col199'),:OLD.COL199,:NEW.COL199,usrName);
  END IF;

IF NVL(:OLD.COL200,' ') != NVL(:NEW.COL200,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col200'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col200'),:OLD.COL200,:NEW.COL200,usrName);
  END IF;

IF NVL(:OLD.COL201,' ') != NVL(:NEW.COL201,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col201'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col201'),:OLD.COL201,:NEW.COL201,usrName);
  END IF;

IF NVL(:OLD.COL202,' ') != NVL(:NEW.COL202,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col202'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col202'),:OLD.COL202,:NEW.COL202,usrName);
  END IF;

IF NVL(:OLD.COL203,' ') != NVL(:NEW.COL203,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col203'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col203'),:OLD.COL203,:NEW.COL203,usrName);
  END IF;

IF NVL(:OLD.COL204,' ') != NVL(:NEW.COL204,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col204'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col204'),:OLD.COL204,:NEW.COL204,usrName);
  END IF;

IF NVL(:OLD.COL205,' ') != NVL(:NEW.COL205,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col205'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col205'),:OLD.COL205,:NEW.COL205,usrName);
  END IF;

IF NVL(:OLD.COL206,' ') != NVL(:NEW.COL206,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col206'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col206'),:OLD.COL206,:NEW.COL206,usrName);
  END IF;

IF NVL(:OLD.COL207,' ') != NVL(:NEW.COL207,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col207'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col207'),:OLD.COL207,:NEW.COL207,usrName);
  END IF;

IF NVL(:OLD.COL208,' ') != NVL(:NEW.COL208,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col208'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col208'),:OLD.COL208,:NEW.COL208,usrName);
  END IF;

IF NVL(:OLD.COL209,' ') != NVL(:NEW.COL209,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col209'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col209'),:OLD.COL209,:NEW.COL209,usrName);
  END IF;

IF NVL(:OLD.COL210,' ') != NVL(:NEW.COL210,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col210'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col210'),:OLD.COL210,:NEW.COL210,usrName);
  END IF;

IF NVL(:OLD.COL211,' ') != NVL(:NEW.COL211,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col211'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col211'),:OLD.COL211,:NEW.COL211,usrName);
  END IF;

IF NVL(:OLD.COL212,' ') != NVL(:NEW.COL212,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col212'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col212'),:OLD.COL212,:NEW.COL212,usrName);
  END IF;

IF NVL(:OLD.COL213,' ') != NVL(:NEW.COL213,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col213'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col213'),:OLD.COL213,:NEW.COL213,usrName);
  END IF;

IF NVL(:OLD.COL214,' ') != NVL(:NEW.COL214,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col214'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col214'),:OLD.COL214,:NEW.COL214,usrName);
  END IF;

IF NVL(:OLD.COL215,' ') != NVL(:NEW.COL215,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col215'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col215'),:OLD.COL215,:NEW.COL215,usrName);
  END IF;

IF NVL(:OLD.COL216,' ') != NVL(:NEW.COL216,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col216'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col216'),:OLD.COL216,:NEW.COL216,usrName);
  END IF;

IF NVL(:OLD.COL217,' ') != NVL(:NEW.COL217,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col217'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col217'),:OLD.COL217,:NEW.COL217,usrName);
  END IF;

IF NVL(:OLD.COL218,' ') != NVL(:NEW.COL218,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col218'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col218'),:OLD.COL218,:NEW.COL218,usrName);
  END IF;

IF NVL(:OLD.COL219,' ') != NVL(:NEW.COL219,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col219'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col219'),:OLD.COL219,:NEW.COL219,usrName);
  END IF;

IF NVL(:OLD.COL220,' ') != NVL(:NEW.COL220,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col220'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col220'),:OLD.COL220,:NEW.COL220,usrName);
  END IF;

IF NVL(:OLD.COL221,' ') != NVL(:NEW.COL221,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col221'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col221'),:OLD.COL221,:NEW.COL221,usrName);
  END IF;

IF NVL(:OLD.COL222,' ') != NVL(:NEW.COL222,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col222'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col222'),:OLD.COL222,:NEW.COL222,usrName);
  END IF;

IF NVL(:OLD.COL223,' ') != NVL(:NEW.COL223,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col223'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col223'),:OLD.COL223,:NEW.COL223,usrName);
  END IF;

IF NVL(:OLD.COL224,' ') != NVL(:NEW.COL224,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col224'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col224'),:OLD.COL224,:NEW.COL224,usrName);
  END IF;

IF NVL(:OLD.COL225,' ') != NVL(:NEW.COL225,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col225'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col225'),:OLD.COL225,:NEW.COL225,usrName);
  END IF;

IF NVL(:OLD.COL226,' ') != NVL(:NEW.COL226,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col226'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col226'),:OLD.COL226,:NEW.COL226,usrName);
  END IF;

IF NVL(:OLD.COL227,' ') != NVL(:NEW.COL227,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col227'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col227'),:OLD.COL227,:NEW.COL227,usrName);
  END IF;

IF NVL(:OLD.COL228,' ') != NVL(:NEW.COL228,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col228'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col228'),:OLD.COL228,:NEW.COL228,usrName);
  END IF;

IF NVL(:OLD.COL229,' ') != NVL(:NEW.COL229,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col229'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col229'),:OLD.COL229,:NEW.COL229,usrName);
  END IF;

IF NVL(:OLD.COL230,' ') != NVL(:NEW.COL230,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col230'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col230'),:OLD.COL230,:NEW.COL230,usrName);
  END IF;

IF NVL(:OLD.COL231,' ') != NVL(:NEW.COL231,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col231'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col231'),:OLD.COL231,:NEW.COL231,usrName);
  END IF;

IF NVL(:OLD.COL232,' ') != NVL(:NEW.COL232,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col232'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col232'),:OLD.COL232,:NEW.COL232,usrName);
  END IF;

IF NVL(:OLD.COL233,' ') != NVL(:NEW.COL233,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col233'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col233'),:OLD.COL233,:NEW.COL233,usrName);
  END IF;

IF NVL(:OLD.COL234,' ') != NVL(:NEW.COL234,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col234'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col234'),:OLD.COL234,:NEW.COL234,usrName);
  END IF;

IF NVL(:OLD.COL235,' ') != NVL(:NEW.COL235,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col235'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col235'),:OLD.COL235,:NEW.COL235,usrName);
  END IF;

IF NVL(:OLD.COL236,' ') != NVL(:NEW.COL236,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col236'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col236'),:OLD.COL236,:NEW.COL236,usrName);
  END IF;

IF NVL(:OLD.COL237,' ') != NVL(:NEW.COL237,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col237'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col237'),:OLD.COL237,:NEW.COL237,usrName);
  END IF;

IF NVL(:OLD.COL238,' ') != NVL(:NEW.COL238,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col238'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col238'),:OLD.COL238,:NEW.COL238,usrName);
  END IF;

IF NVL(:OLD.COL239,' ') != NVL(:NEW.COL239,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col239'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col239'),:OLD.COL239,:NEW.COL239,usrName);
  END IF;

IF NVL(:OLD.COL240,' ') != NVL(:NEW.COL240,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col240'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col240'),:OLD.COL240,:NEW.COL240,usrName);
  END IF;

IF NVL(:OLD.COL241,' ') != NVL(:NEW.COL241,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col241'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col241'),:OLD.COL241,:NEW.COL241,usrName);
  END IF;

IF NVL(:OLD.COL242,' ') != NVL(:NEW.COL242,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col242'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col242'),:OLD.COL242,:NEW.COL242,usrName);
  END IF;

IF NVL(:OLD.COL243,' ') != NVL(:NEW.COL243,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col243'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col243'),:OLD.COL243,:NEW.COL243,usrName);
  END IF;

IF NVL(:OLD.COL244,' ') != NVL(:NEW.COL244,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col244'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col244'),:OLD.COL244,:NEW.COL244,usrName);
  END IF;

IF NVL(:OLD.COL245,' ') != NVL(:NEW.COL245,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col245'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col245'),:OLD.COL245,:NEW.COL245,usrName);
  END IF;

IF NVL(:OLD.COL246,' ') != NVL(:NEW.COL246,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col246'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col246'),:OLD.COL246,:NEW.COL246,usrName);
  END IF;

IF NVL(:OLD.COL247,' ') != NVL(:NEW.COL247,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col247'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col247'),:OLD.COL247,:NEW.COL247,usrName);
  END IF;

IF NVL(:OLD.COL248,' ') != NVL(:NEW.COL248,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col248'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col248'),:OLD.COL248,:NEW.COL248,usrName);
  END IF;

IF NVL(:OLD.COL249,' ') != NVL(:NEW.COL249,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col249'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col249'),:OLD.COL249,:NEW.COL249,usrName);
  END IF;

IF NVL(:OLD.COL250,' ') != NVL(:NEW.COL250,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col250'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col250'),:OLD.COL250,:NEW.COL250,usrName);
  END IF;

IF NVL(:OLD.COL251,' ') != NVL(:NEW.COL251,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col251'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col251'),:OLD.COL251,:NEW.COL251,usrName);
  END IF;

IF NVL(:OLD.COL252,' ') != NVL(:NEW.COL252,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col252'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col252'),:OLD.COL252,:NEW.COL252,usrName);
  END IF;

IF NVL(:OLD.COL253,' ') != NVL(:NEW.COL253,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col253'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col253'),:OLD.COL253,:NEW.COL253,usrName);
  END IF;

IF NVL(:OLD.COL254,' ') != NVL(:NEW.COL254,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col254'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col254'),:OLD.COL254,:NEW.COL254,usrName);
  END IF;

IF NVL(:OLD.COL255,' ') != NVL(:NEW.COL255,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col255'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col255'),:OLD.COL255,:NEW.COL255,usrName);
  END IF;

IF NVL(:OLD.COL256,' ') != NVL(:NEW.COL256,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col256'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col256'),:OLD.COL256,:NEW.COL256,usrName);
  END IF;

IF NVL(:OLD.COL257,' ') != NVL(:NEW.COL257,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col257'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col257'),:OLD.COL257,:NEW.COL257,usrName);
  END IF;

IF NVL(:OLD.COL258,' ') != NVL(:NEW.COL258,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col258'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col258'),:OLD.COL258,:NEW.COL258,usrName);
  END IF;

IF NVL(:OLD.COL259,' ') != NVL(:NEW.COL259,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col259'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col259'),:OLD.COL259,:NEW.COL259,usrName);
  END IF;

IF NVL(:OLD.COL260,' ') != NVL(:NEW.COL260,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col260'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col260'),:OLD.COL260,:NEW.COL260,usrName);
  END IF;

IF NVL(:OLD.COL261,' ') != NVL(:NEW.COL261,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col261'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col261'),:OLD.COL261,:NEW.COL261,usrName);
  END IF;

IF NVL(:OLD.COL262,' ') != NVL(:NEW.COL262,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col262'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col262'),:OLD.COL262,:NEW.COL262,usrName);
  END IF;

IF NVL(:OLD.COL263,' ') != NVL(:NEW.COL263,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col263'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col263'),:OLD.COL263,:NEW.COL263,usrName);
  END IF;

IF NVL(:OLD.COL264,' ') != NVL(:NEW.COL264,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col264'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col264'),:OLD.COL264,:NEW.COL264,usrName);
  END IF;

IF NVL(:OLD.COL265,' ') != NVL(:NEW.COL265,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col265'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col265'),:OLD.COL265,:NEW.COL265,usrName);
  END IF;

IF NVL(:OLD.COL266,' ') != NVL(:NEW.COL266,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col266'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col266'),:OLD.COL266,:NEW.COL266,usrName);
  END IF;

IF NVL(:OLD.COL267,' ') != NVL(:NEW.COL267,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col267'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col267'),:OLD.COL267,:NEW.COL267,usrName);
  END IF;

IF NVL(:OLD.COL268,' ') != NVL(:NEW.COL268,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col268'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col268'),:OLD.COL268,:NEW.COL268,usrName);
  END IF;

IF NVL(:OLD.COL269,' ') != NVL(:NEW.COL269,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col269'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col269'),:OLD.COL269,:NEW.COL269,usrName);
  END IF;

IF NVL(:OLD.COL270,' ') != NVL(:NEW.COL270,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col270'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col270'),:OLD.COL270,:NEW.COL270,usrName);
  END IF;

IF NVL(:OLD.COL271,' ') != NVL(:NEW.COL271,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col271'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col271'),:OLD.COL271,:NEW.COL271,usrName);
  END IF;

IF NVL(:OLD.COL272,' ') != NVL(:NEW.COL272,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col272'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col272'),:OLD.COL272,:NEW.COL272,usrName);
  END IF;

IF NVL(:OLD.COL273,' ') != NVL(:NEW.COL273,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col273'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col273'),:OLD.COL273,:NEW.COL273,usrName);
  END IF;

IF NVL(:OLD.COL274,' ') != NVL(:NEW.COL274,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col274'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col274'),:OLD.COL274,:NEW.COL274,usrName);
  END IF;

IF NVL(:OLD.COL275,' ') != NVL(:NEW.COL275,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col275'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col275'),:OLD.COL275,:NEW.COL275,usrName);
  END IF;

IF NVL(:OLD.COL276,' ') != NVL(:NEW.COL276,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col276'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col276'),:OLD.COL276,:NEW.COL276,usrName);
  END IF;

IF NVL(:OLD.COL277,' ') != NVL(:NEW.COL277,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col277'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col277'),:OLD.COL277,:NEW.COL277,usrName);
  END IF;

IF NVL(:OLD.COL278,' ') != NVL(:NEW.COL278,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col278'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col278'),:OLD.COL278,:NEW.COL278,usrName);
  END IF;

IF NVL(:OLD.COL279,' ') != NVL(:NEW.COL279,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col279'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col279'),:OLD.COL279,:NEW.COL279,usrName);
  END IF;

IF NVL(:OLD.COL280,' ') != NVL(:NEW.COL280,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col280'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col280'),:OLD.COL280,:NEW.COL280,usrName);
  END IF;

IF NVL(:OLD.COL281,' ') != NVL(:NEW.COL281,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col281'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col281'),:OLD.COL281,:NEW.COL281,usrName);
  END IF;

IF NVL(:OLD.COL282,' ') != NVL(:NEW.COL282,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col282'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col282'),:OLD.COL282,:NEW.COL282,usrName);
  END IF;

IF NVL(:OLD.COL283,' ') != NVL(:NEW.COL283,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col283'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col283'),:OLD.COL283,:NEW.COL283,usrName);
  END IF;

IF NVL(:OLD.COL284,' ') != NVL(:NEW.COL284,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col284'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col284'),:OLD.COL284,:NEW.COL284,usrName);
  END IF;

IF NVL(:OLD.COL285,' ') != NVL(:NEW.COL285,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col285'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col285'),:OLD.COL285,:NEW.COL285,usrName);
  END IF;

IF NVL(:OLD.COL286,' ') != NVL(:NEW.COL286,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col286'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col286'),:OLD.COL286,:NEW.COL286,usrName);
  END IF;

IF NVL(:OLD.COL287,' ') != NVL(:NEW.COL287,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col287'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col287'),:OLD.COL287,:NEW.COL287,usrName);
  END IF;

IF NVL(:OLD.COL288,' ') != NVL(:NEW.COL288,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col288'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col288'),:OLD.COL288,:NEW.COL288,usrName);
  END IF;

IF NVL(:OLD.COL289,' ') != NVL(:NEW.COL289,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col289'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col289'),:OLD.COL289,:NEW.COL289,usrName);
  END IF;

IF NVL(:OLD.COL290,' ') != NVL(:NEW.COL290,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col290'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col290'),:OLD.COL290,:NEW.COL290,usrName);
  END IF;

IF NVL(:OLD.COL291,' ') != NVL(:NEW.COL291,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col291'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col291'),:OLD.COL291,:NEW.COL291,usrName);
  END IF;

IF NVL(:OLD.COL292,' ') != NVL(:NEW.COL292,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col292'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col292'),:OLD.COL292,:NEW.COL292,usrName);
  END IF;

IF NVL(:OLD.COL293,' ') != NVL(:NEW.COL293,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col293'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col293'),:OLD.COL293,:NEW.COL293,usrName);
  END IF;

IF NVL(:OLD.COL294,' ') != NVL(:NEW.COL294,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col294'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col294'),:OLD.COL294,:NEW.COL294,usrName);
  END IF;

IF NVL(:OLD.COL295,' ') != NVL(:NEW.COL295,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col295'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col295'),:OLD.COL295,:NEW.COL295,usrName);
  END IF;

IF NVL(:OLD.COL296,' ') != NVL(:NEW.COL296,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col296'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col296'),:OLD.COL296,:NEW.COL296,usrName);
  END IF;

IF NVL(:OLD.COL297,' ') != NVL(:NEW.COL297,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col297'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col297'),:OLD.COL297,:NEW.COL297,usrName);
  END IF;

IF NVL(:OLD.COL298,' ') != NVL(:NEW.COL298,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col298'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col298'),:OLD.COL298,:NEW.COL298,usrName);
  END IF;

IF NVL(:OLD.COL299,' ') != NVL(:NEW.COL299,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col299'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col299'),:OLD.COL299,:NEW.COL299,usrName);
  END IF;

IF NVL(:OLD.COL300,' ') != NVL(:NEW.COL300,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col300'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col300'),:OLD.COL300,:NEW.COL300,usrName);
  END IF;

IF NVL(:OLD.COL301,' ') != NVL(:NEW.COL301,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col301'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col301'),:OLD.COL301,:NEW.COL301,usrName);
  END IF;

IF NVL(:OLD.COL302,' ') != NVL(:NEW.COL302,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col302'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col302'),:OLD.COL302,:NEW.COL302,usrName);
  END IF;

IF NVL(:OLD.COL303,' ') != NVL(:NEW.COL303,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col303'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col303'),:OLD.COL303,:NEW.COL303,usrName);
  END IF;

IF NVL(:OLD.COL304,' ') != NVL(:NEW.COL304,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col304'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col304'),:OLD.COL304,:NEW.COL304,usrName);
  END IF;

IF NVL(:OLD.COL305,' ') != NVL(:NEW.COL305,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col305'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col305'),:OLD.COL305,:NEW.COL305,usrName);
  END IF;

IF NVL(:OLD.COL306,' ') != NVL(:NEW.COL306,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col306'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col306'),:OLD.COL306,:NEW.COL306,usrName);
  END IF;

IF NVL(:OLD.COL307,' ') != NVL(:NEW.COL307,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col307'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col307'),:OLD.COL307,:NEW.COL307,usrName);
  END IF;

IF NVL(:OLD.COL308,' ') != NVL(:NEW.COL308,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col308'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col308'),:OLD.COL308,:NEW.COL308,usrName);
  END IF;

IF NVL(:OLD.COL309,' ') != NVL(:NEW.COL309,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col309'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col309'),:OLD.COL309,:NEW.COL309,usrName);
  END IF;

IF NVL(:OLD.COL310,' ') != NVL(:NEW.COL310,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col310'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col310'),:OLD.COL310,:NEW.COL310,usrName);
  END IF;

IF NVL(:OLD.COL311,' ') != NVL(:NEW.COL311,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col311'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col311'),:OLD.COL311,:NEW.COL311,usrName);
  END IF;

IF NVL(:OLD.COL312,' ') != NVL(:NEW.COL312,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col312'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col312'),:OLD.COL312,:NEW.COL312,usrName);
  END IF;

IF NVL(:OLD.COL313,' ') != NVL(:NEW.COL313,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col313'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col313'),:OLD.COL313,:NEW.COL313,usrName);
  END IF;

IF NVL(:OLD.COL314,' ') != NVL(:NEW.COL314,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col314'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col314'),:OLD.COL314,:NEW.COL314,usrName);
  END IF;

IF NVL(:OLD.COL315,' ') != NVL(:NEW.COL315,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col315'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col315'),:OLD.COL315,:NEW.COL315,usrName);
  END IF;

IF NVL(:OLD.COL316,' ') != NVL(:NEW.COL316,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col316'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col316'),:OLD.COL316,:NEW.COL316,usrName);
  END IF;

IF NVL(:OLD.COL317,' ') != NVL(:NEW.COL317,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col317'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col317'),:OLD.COL317,:NEW.COL317,usrName);
  END IF;

IF NVL(:OLD.COL318,' ') != NVL(:NEW.COL318,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col318'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col318'),:OLD.COL318,:NEW.COL318,usrName);
  END IF;

IF NVL(:OLD.COL319,' ') != NVL(:NEW.COL319,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col319'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col319'),:OLD.COL319,:NEW.COL319,usrName);
  END IF;

IF NVL(:OLD.COL320,' ') != NVL(:NEW.COL320,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col320'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col320'),:OLD.COL320,:NEW.COL320,usrName);
  END IF;

IF NVL(:OLD.COL321,' ') != NVL(:NEW.COL321,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col321'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col321'),:OLD.COL321,:NEW.COL321,usrName);
  END IF;

IF NVL(:OLD.COL322,' ') != NVL(:NEW.COL322,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col322'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col322'),:OLD.COL322,:NEW.COL322,usrName);
  END IF;

IF NVL(:OLD.COL323,' ') != NVL(:NEW.COL323,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col323'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col323'),:OLD.COL323,:NEW.COL323,usrName);
  END IF;

IF NVL(:OLD.COL324,' ') != NVL(:NEW.COL324,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col324'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col324'),:OLD.COL324,:NEW.COL324,usrName);
  END IF;

IF NVL(:OLD.COL325,' ') != NVL(:NEW.COL325,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col325'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col325'),:OLD.COL325,:NEW.COL325,usrName);
  END IF;

IF NVL(:OLD.COL326,' ') != NVL(:NEW.COL326,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col326'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col326'),:OLD.COL326,:NEW.COL326,usrName);
  END IF;

IF NVL(:OLD.COL327,' ') != NVL(:NEW.COL327,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col327'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col327'),:OLD.COL327,:NEW.COL327,usrName);
  END IF;

IF NVL(:OLD.COL328,' ') != NVL(:NEW.COL328,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col328'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col328'),:OLD.COL328,:NEW.COL328,usrName);
  END IF;

IF NVL(:OLD.COL329,' ') != NVL(:NEW.COL329,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col329'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col329'),:OLD.COL329,:NEW.COL329,usrName);
  END IF;

IF NVL(:OLD.COL330,' ') != NVL(:NEW.COL330,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col330'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col330'),:OLD.COL330,:NEW.COL330,usrName);
  END IF;

IF NVL(:OLD.COL331,' ') != NVL(:NEW.COL331,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col331'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col331'),:OLD.COL331,:NEW.COL331,usrName);
  END IF;

IF NVL(:OLD.COL332,' ') != NVL(:NEW.COL332,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col332'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col332'),:OLD.COL332,:NEW.COL332,usrName);
  END IF;

IF NVL(:OLD.COL333,' ') != NVL(:NEW.COL333,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col333'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col333'),:OLD.COL333,:NEW.COL333,usrName);
  END IF;

IF NVL(:OLD.COL334,' ') != NVL(:NEW.COL334,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col334'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col334'),:OLD.COL334,:NEW.COL334,usrName);
  END IF;

IF NVL(:OLD.COL335,' ') != NVL(:NEW.COL335,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col335'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col335'),:OLD.COL335,:NEW.COL335,usrName);
  END IF;

IF NVL(:OLD.COL336,' ') != NVL(:NEW.COL336,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col336'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col336'),:OLD.COL336,:NEW.COL336,usrName);
  END IF;

IF NVL(:OLD.COL337,' ') != NVL(:NEW.COL337,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col337'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col337'),:OLD.COL337,:NEW.COL337,usrName);
  END IF;

IF NVL(:OLD.COL338,' ') != NVL(:NEW.COL338,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col338'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col338'),:OLD.COL338,:NEW.COL338,usrName);
  END IF;

IF NVL(:OLD.COL339,' ') != NVL(:NEW.COL339,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col339'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col339'),:OLD.COL339,:NEW.COL339,usrName);
  END IF;

IF NVL(:OLD.COL340,' ') != NVL(:NEW.COL340,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col340'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col340'),:OLD.COL340,:NEW.COL340,usrName);
  END IF;

IF NVL(:OLD.COL341,' ') != NVL(:NEW.COL341,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col341'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col341'),:OLD.COL341,:NEW.COL341,usrName);
  END IF;

IF NVL(:OLD.COL342,' ') != NVL(:NEW.COL342,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col342'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col342'),:OLD.COL342,:NEW.COL342,usrName);
  END IF;

IF NVL(:OLD.COL343,' ') != NVL(:NEW.COL343,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col343'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col343'),:OLD.COL343,:NEW.COL343,usrName);
  END IF;

IF NVL(:OLD.COL344,' ') != NVL(:NEW.COL344,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col344'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col344'),:OLD.COL344,:NEW.COL344,usrName);
  END IF;

IF NVL(:OLD.COL345,' ') != NVL(:NEW.COL345,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col345'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col345'),:OLD.COL345,:NEW.COL345,usrName);
  END IF;

IF NVL(:OLD.COL346,' ') != NVL(:NEW.COL346,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col346'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col346'),:OLD.COL346,:NEW.COL346,usrName);
  END IF;

IF NVL(:OLD.COL347,' ') != NVL(:NEW.COL347,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col347'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col347'),:OLD.COL347,:NEW.COL347,usrName);
  END IF;

IF NVL(:OLD.COL348,' ') != NVL(:NEW.COL348,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col348'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col348'),:OLD.COL348,:NEW.COL348,usrName);
  END IF;

IF NVL(:OLD.COL349,' ') != NVL(:NEW.COL349,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col349'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col349'),:OLD.COL349,:NEW.COL349,usrName);
  END IF;

IF NVL(:OLD.COL350,' ') != NVL(:NEW.COL350,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col350'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col350'),:OLD.COL350,:NEW.COL350,usrName);
  END IF;

IF NVL(:OLD.COL351,' ') != NVL(:NEW.COL351,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col351'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col351'),:OLD.COL351,:NEW.COL351,usrName);
  END IF;

IF NVL(:OLD.COL352,' ') != NVL(:NEW.COL352,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col352'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col352'),:OLD.COL352,:NEW.COL352,usrName);
  END IF;

IF NVL(:OLD.COL353,' ') != NVL(:NEW.COL353,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col353'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col353'),:OLD.COL353,:NEW.COL353,usrName);
  END IF;

IF NVL(:OLD.COL354,' ') != NVL(:NEW.COL354,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col354'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col354'),:OLD.COL354,:NEW.COL354,usrName);
  END IF;

IF NVL(:OLD.COL355,' ') != NVL(:NEW.COL355,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col355'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col355'),:OLD.COL355,:NEW.COL355,usrName);
  END IF;

IF NVL(:OLD.COL356,' ') != NVL(:NEW.COL356,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col356'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col356'),:OLD.COL356,:NEW.COL356,usrName);
  END IF;

IF NVL(:OLD.COL357,' ') != NVL(:NEW.COL357,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col357'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col357'),:OLD.COL357,:NEW.COL357,usrName);
  END IF;

IF NVL(:OLD.COL358,' ') != NVL(:NEW.COL358,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col358'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col358'),:OLD.COL358,:NEW.COL358,usrName);
  END IF;

IF NVL(:OLD.COL359,' ') != NVL(:NEW.COL359,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col359'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col359'),:OLD.COL359,:NEW.COL359,usrName);
  END IF;

IF NVL(:OLD.COL360,' ') != NVL(:NEW.COL360,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col360'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col360'),:OLD.COL360,:NEW.COL360,usrName);
  END IF;

IF NVL(:OLD.COL361,' ') != NVL(:NEW.COL361,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col361'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col361'),:OLD.COL361,:NEW.COL361,usrName);
  END IF;

IF NVL(:OLD.COL362,' ') != NVL(:NEW.COL362,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col362'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col362'),:OLD.COL362,:NEW.COL362,usrName);
  END IF;

IF NVL(:OLD.COL363,' ') != NVL(:NEW.COL363,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col363'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col363'),:OLD.COL363,:NEW.COL363,usrName);
  END IF;

IF NVL(:OLD.COL364,' ') != NVL(:NEW.COL364,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col364'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col364'),:OLD.COL364,:NEW.COL364,usrName);
  END IF;

IF NVL(:OLD.COL365,' ') != NVL(:NEW.COL365,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col365'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col365'),:OLD.COL365,:NEW.COL365,usrName);
  END IF;

IF NVL(:OLD.COL366,' ') != NVL(:NEW.COL366,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col366'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col366'),:OLD.COL366,:NEW.COL366,usrName);
  END IF;

IF NVL(:OLD.COL367,' ') != NVL(:NEW.COL367,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col367'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col367'),:OLD.COL367,:NEW.COL367,usrName);
  END IF;

IF NVL(:OLD.COL368,' ') != NVL(:NEW.COL368,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col368'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col368'),:OLD.COL368,:NEW.COL368,usrName);
  END IF;

IF NVL(:OLD.COL369,' ') != NVL(:NEW.COL369,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col369'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col369'),:OLD.COL369,:NEW.COL369,usrName);
  END IF;

IF NVL(:OLD.COL370,' ') != NVL(:NEW.COL370,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col370'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col370'),:OLD.COL370,:NEW.COL370,usrName);
  END IF;

IF NVL(:OLD.COL371,' ') != NVL(:NEW.COL371,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col371'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col371'),:OLD.COL371,:NEW.COL371,usrName);
  END IF;

IF NVL(:OLD.COL372,' ') != NVL(:NEW.COL372,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col372'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col372'),:OLD.COL372,:NEW.COL372,usrName);
  END IF;

IF NVL(:OLD.COL373,' ') != NVL(:NEW.COL373,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col373'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col373'),:OLD.COL373,:NEW.COL373,usrName);
  END IF;

IF NVL(:OLD.COL374,' ') != NVL(:NEW.COL374,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col374'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col374'),:OLD.COL374,:NEW.COL374,usrName);
  END IF;

IF NVL(:OLD.COL375,' ') != NVL(:NEW.COL375,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col375'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col375'),:OLD.COL375,:NEW.COL375,usrName);
  END IF;

IF NVL(:OLD.COL376,' ') != NVL(:NEW.COL376,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col376'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col376'),:OLD.COL376,:NEW.COL376,usrName);
  END IF;

IF NVL(:OLD.COL377,' ') != NVL(:NEW.COL377,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col377'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col377'),:OLD.COL377,:NEW.COL377,usrName);
  END IF;

IF NVL(:OLD.COL378,' ') != NVL(:NEW.COL378,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col378'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col378'),:OLD.COL378,:NEW.COL378,usrName);
  END IF;

IF NVL(:OLD.COL379,' ') != NVL(:NEW.COL379,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col379'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col379'),:OLD.COL379,:NEW.COL379,usrName);
  END IF;

IF NVL(:OLD.COL380,' ') != NVL(:NEW.COL380,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col380'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col380'),:OLD.COL380,:NEW.COL380,usrName);
  END IF;

IF NVL(:OLD.COL381,' ') != NVL(:NEW.COL381,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col381'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col381'),:OLD.COL381,:NEW.COL381,usrName);
  END IF;

IF NVL(:OLD.COL382,' ') != NVL(:NEW.COL382,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col382'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col382'),:OLD.COL382,:NEW.COL382,usrName);
  END IF;

IF NVL(:OLD.COL383,' ') != NVL(:NEW.COL383,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col383'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col383'),:OLD.COL383,:NEW.COL383,usrName);
  END IF;

IF NVL(:OLD.COL384,' ') != NVL(:NEW.COL384,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col384'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col384'),:OLD.COL384,:NEW.COL384,usrName);
  END IF;

IF NVL(:OLD.COL385,' ') != NVL(:NEW.COL385,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col385'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col385'),:OLD.COL385,:NEW.COL385,usrName);
  END IF;

IF NVL(:OLD.COL386,' ') != NVL(:NEW.COL386,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col386'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col386'),:OLD.COL386,:NEW.COL386,usrName);
  END IF;

IF NVL(:OLD.COL387,' ') != NVL(:NEW.COL387,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col387'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col387'),:OLD.COL387,:NEW.COL387,usrName);
  END IF;

IF NVL(:OLD.COL388,' ') != NVL(:NEW.COL388,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col388'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col388'),:OLD.COL388,:NEW.COL388,usrName);
  END IF;

IF NVL(:OLD.COL389,' ') != NVL(:NEW.COL389,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col389'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col389'),:OLD.COL389,:NEW.COL389,usrName);
  END IF;

IF NVL(:OLD.COL390,' ') != NVL(:NEW.COL390,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col390'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col390'),:OLD.COL390,:NEW.COL390,usrName);
  END IF;

IF NVL(:OLD.COL391,' ') != NVL(:NEW.COL391,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col391'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col391'),:OLD.COL391,:NEW.COL391,usrName);
  END IF;

IF NVL(:OLD.COL392,' ') != NVL(:NEW.COL392,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col392'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col392'),:OLD.COL392,:NEW.COL392,usrName);
  END IF;

IF NVL(:OLD.COL393,' ') != NVL(:NEW.COL393,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col393'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col393'),:OLD.COL393,:NEW.COL393,usrName);
  END IF;

IF NVL(:OLD.COL394,' ') != NVL(:NEW.COL394,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col394'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col394'),:OLD.COL394,:NEW.COL394,usrName);
  END IF;

IF NVL(:OLD.COL395,' ') != NVL(:NEW.COL395,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col395'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col395'),:OLD.COL395,:NEW.COL395,usrName);
  END IF;

IF NVL(:OLD.COL396,' ') != NVL(:NEW.COL396,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col396'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col396'),:OLD.COL396,:NEW.COL396,usrName);
  END IF;

IF NVL(:OLD.COL397,' ') != NVL(:NEW.COL397,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col397'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col397'),:OLD.COL397,:NEW.COL397,usrName);
  END IF;

IF NVL(:OLD.COL398,' ') != NVL(:NEW.COL398,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col398'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col398'),:OLD.COL398,:NEW.COL398,usrName);
  END IF;

IF NVL(:OLD.COL399,' ') != NVL(:NEW.COL399,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col399'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col399'),:OLD.COL399,:NEW.COL399,usrName);
  END IF;

IF NVL(:OLD.COL400,' ') != NVL(:NEW.COL400,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col400'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col400'),:OLD.COL400,:NEW.COL400,usrName);
  END IF;

IF NVL(:OLD.COL401,' ') != NVL(:NEW.COL401,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col401'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col401'),:OLD.COL401,:NEW.COL401,usrName);
  END IF;

IF NVL(:OLD.COL402,' ') != NVL(:NEW.COL402,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col402'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col402'),:OLD.COL402,:NEW.COL402,usrName);
  END IF;

IF NVL(:OLD.COL403,' ') != NVL(:NEW.COL403,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col403'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col403'),:OLD.COL403,:NEW.COL403,usrName);
  END IF;

IF NVL(:OLD.COL404,' ') != NVL(:NEW.COL404,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col404'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col404'),:OLD.COL404,:NEW.COL404,usrName);
  END IF;

IF NVL(:OLD.COL405,' ') != NVL(:NEW.COL405,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col405'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col405'),:OLD.COL405,:NEW.COL405,usrName);
  END IF;

IF NVL(:OLD.COL406,' ') != NVL(:NEW.COL406,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col406'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col406'),:OLD.COL406,:NEW.COL406,usrName);
  END IF;

IF NVL(:OLD.COL407,' ') != NVL(:NEW.COL407,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col407'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col407'),:OLD.COL407,:NEW.COL407,usrName);
  END IF;

IF NVL(:OLD.COL408,' ') != NVL(:NEW.COL408,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col408'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col408'),:OLD.COL408,:NEW.COL408,usrName);
  END IF;

IF NVL(:OLD.COL409,' ') != NVL(:NEW.COL409,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col409'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col409'),:OLD.COL409,:NEW.COL409,usrName);
  END IF;

IF NVL(:OLD.COL410,' ') != NVL(:NEW.COL410,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col410'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col410'),:OLD.COL410,:NEW.COL410,usrName);
  END IF;

IF NVL(:OLD.COL411,' ') != NVL(:NEW.COL411,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col411'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col411'),:OLD.COL411,:NEW.COL411,usrName);
  END IF;

IF NVL(:OLD.COL412,' ') != NVL(:NEW.COL412,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col412'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col412'),:OLD.COL412,:NEW.COL412,usrName);
  END IF;

IF NVL(:OLD.COL413,' ') != NVL(:NEW.COL413,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col413'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col413'),:OLD.COL413,:NEW.COL413,usrName);
  END IF;

IF NVL(:OLD.COL414,' ') != NVL(:NEW.COL414,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col414'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col414'),:OLD.COL414,:NEW.COL414,usrName);
  END IF;

IF NVL(:OLD.COL415,' ') != NVL(:NEW.COL415,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col415'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col415'),:OLD.COL415,:NEW.COL415,usrName);
  END IF;

IF NVL(:OLD.COL416,' ') != NVL(:NEW.COL416,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col416'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col416'),:OLD.COL416,:NEW.COL416,usrName);
  END IF;

IF NVL(:OLD.COL417,' ') != NVL(:NEW.COL417,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col417'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col417'),:OLD.COL417,:NEW.COL417,usrName);
  END IF;

IF NVL(:OLD.COL418,' ') != NVL(:NEW.COL418,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col418'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col418'),:OLD.COL418,:NEW.COL418,usrName);
  END IF;

IF NVL(:OLD.COL419,' ') != NVL(:NEW.COL419,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col419'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col419'),:OLD.COL419,:NEW.COL419,usrName);
  END IF;

IF NVL(:OLD.COL420,' ') != NVL(:NEW.COL420,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col420'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col420'),:OLD.COL420,:NEW.COL420,usrName);
  END IF;

IF NVL(:OLD.COL421,' ') != NVL(:NEW.COL421,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col421'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col421'),:OLD.COL421,:NEW.COL421,usrName);
  END IF;

IF NVL(:OLD.COL422,' ') != NVL(:NEW.COL422,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col422'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col422'),:OLD.COL422,:NEW.COL422,usrName);
  END IF;

IF NVL(:OLD.COL423,' ') != NVL(:NEW.COL423,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col423'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col423'),:OLD.COL423,:NEW.COL423,usrName);
  END IF;

IF NVL(:OLD.COL424,' ') != NVL(:NEW.COL424,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col424'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col424'),:OLD.COL424,:NEW.COL424,usrName);
  END IF;

IF NVL(:OLD.COL425,' ') != NVL(:NEW.COL425,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col425'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col425'),:OLD.COL425,:NEW.COL425,usrName);
  END IF;

IF NVL(:OLD.COL426,' ') != NVL(:NEW.COL426,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col426'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col426'),:OLD.COL426,:NEW.COL426,usrName);
  END IF;

IF NVL(:OLD.COL427,' ') != NVL(:NEW.COL427,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col427'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col427'),:OLD.COL427,:NEW.COL427,usrName);
  END IF;

IF NVL(:OLD.COL428,' ') != NVL(:NEW.COL428,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col428'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col428'),:OLD.COL428,:NEW.COL428,usrName);
  END IF;

IF NVL(:OLD.COL429,' ') != NVL(:NEW.COL429,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col429'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col429'),:OLD.COL429,:NEW.COL429,usrName);
  END IF;

IF NVL(:OLD.COL430,' ') != NVL(:NEW.COL430,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col430'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col430'),:OLD.COL430,:NEW.COL430,usrName);
  END IF;

IF NVL(:OLD.COL431,' ') != NVL(:NEW.COL431,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col431'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col431'),:OLD.COL431,:NEW.COL431,usrName);
  END IF;

IF NVL(:OLD.COL432,' ') != NVL(:NEW.COL432,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col432'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col432'),:OLD.COL432,:NEW.COL432,usrName);
  END IF;

IF NVL(:OLD.COL433,' ') != NVL(:NEW.COL433,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col433'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col433'),:OLD.COL433,:NEW.COL433,usrName);
  END IF;

IF NVL(:OLD.COL434,' ') != NVL(:NEW.COL434,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col434'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col434'),:OLD.COL434,:NEW.COL434,usrName);
  END IF;

IF NVL(:OLD.COL435,' ') != NVL(:NEW.COL435,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col435'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col435'),:OLD.COL435,:NEW.COL435,usrName);
  END IF;

IF NVL(:OLD.COL436,' ') != NVL(:NEW.COL436,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col436'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col436'),:OLD.COL436,:NEW.COL436,usrName);
  END IF;

IF NVL(:OLD.COL437,' ') != NVL(:NEW.COL437,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col437'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col437'),:OLD.COL437,:NEW.COL437,usrName);
  END IF;

IF NVL(:OLD.COL438,' ') != NVL(:NEW.COL438,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col438'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col438'),:OLD.COL438,:NEW.COL438,usrName);
  END IF;

IF NVL(:OLD.COL439,' ') != NVL(:NEW.COL439,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col439'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col439'),:OLD.COL439,:NEW.COL439,usrName);
  END IF;

IF NVL(:OLD.COL440,' ') != NVL(:NEW.COL440,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col440'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col440'),:OLD.COL440,:NEW.COL440,usrName);
  END IF;

IF NVL(:OLD.COL441,' ') != NVL(:NEW.COL441,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col441'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col441'),:OLD.COL441,:NEW.COL441,usrName);
  END IF;

IF NVL(:OLD.COL442,' ') != NVL(:NEW.COL442,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col442'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col442'),:OLD.COL442,:NEW.COL442,usrName);
  END IF;

IF NVL(:OLD.COL443,' ') != NVL(:NEW.COL443,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col443'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col443'),:OLD.COL443,:NEW.COL443,usrName);
  END IF;

IF NVL(:OLD.COL444,' ') != NVL(:NEW.COL444,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col444'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col444'),:OLD.COL444,:NEW.COL444,usrName);
  END IF;

IF NVL(:OLD.COL445,' ') != NVL(:NEW.COL445,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col445'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col445'),:OLD.COL445,:NEW.COL445,usrName);
  END IF;

IF NVL(:OLD.COL446,' ') != NVL(:NEW.COL446,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col446'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col446'),:OLD.COL446,:NEW.COL446,usrName);
  END IF;

IF NVL(:OLD.COL447,' ') != NVL(:NEW.COL447,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col447'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col447'),:OLD.COL447,:NEW.COL447,usrName);
  END IF;

IF NVL(:OLD.COL448,' ') != NVL(:NEW.COL448,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col448'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col448'),:OLD.COL448,:NEW.COL448,usrName);
  END IF;

IF NVL(:OLD.COL449,' ') != NVL(:NEW.COL449,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col449'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col449'),:OLD.COL449,:NEW.COL449,usrName);
  END IF;

IF NVL(:OLD.COL450,' ') != NVL(:NEW.COL450,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col450'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col450'),:OLD.COL450,:NEW.COL450,usrName);
  END IF;

IF NVL(:OLD.COL451,' ') != NVL(:NEW.COL451,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col451'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col451'),:OLD.COL451,:NEW.COL451,usrName);
  END IF;

IF NVL(:OLD.COL452,' ') != NVL(:NEW.COL452,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col452'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col452'),:OLD.COL452,:NEW.COL452,usrName);
  END IF;

IF NVL(:OLD.COL453,' ') != NVL(:NEW.COL453,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col453'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col453'),:OLD.COL453,:NEW.COL453,usrName);
  END IF;

IF NVL(:OLD.COL454,' ') != NVL(:NEW.COL454,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col454'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col454'),:OLD.COL454,:NEW.COL454,usrName);
  END IF;

IF NVL(:OLD.COL455,' ') != NVL(:NEW.COL455,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col455'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col455'),:OLD.COL455,:NEW.COL455,usrName);
  END IF;

IF NVL(:OLD.COL456,' ') != NVL(:NEW.COL456,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col456'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col456'),:OLD.COL456,:NEW.COL456,usrName);
  END IF;

IF NVL(:OLD.COL457,' ') != NVL(:NEW.COL457,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col457'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col457'),:OLD.COL457,:NEW.COL457,usrName);
  END IF;

IF NVL(:OLD.COL458,' ') != NVL(:NEW.COL458,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col458'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col458'),:OLD.COL458,:NEW.COL458,usrName);
  END IF;

IF NVL(:OLD.COL459,' ') != NVL(:NEW.COL459,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col459'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col459'),:OLD.COL459,:NEW.COL459,usrName);
  END IF;

IF NVL(:OLD.COL460,' ') != NVL(:NEW.COL460,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col460'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col460'),:OLD.COL460,:NEW.COL460,usrName);
  END IF;

IF NVL(:OLD.COL461,' ') != NVL(:NEW.COL461,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col461'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col461'),:OLD.COL461,:NEW.COL461,usrName);
  END IF;

IF NVL(:OLD.COL462,' ') != NVL(:NEW.COL462,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col462'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col462'),:OLD.COL462,:NEW.COL462,usrName);
  END IF;

IF NVL(:OLD.COL463,' ') != NVL(:NEW.COL463,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col463'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col463'),:OLD.COL463,:NEW.COL463,usrName);
  END IF;

IF NVL(:OLD.COL464,' ') != NVL(:NEW.COL464,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col464'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col464'),:OLD.COL464,:NEW.COL464,usrName);
  END IF;

IF NVL(:OLD.COL465,' ') != NVL(:NEW.COL465,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col465'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col465'),:OLD.COL465,:NEW.COL465,usrName);
  END IF;

IF NVL(:OLD.COL466,' ') != NVL(:NEW.COL466,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col466'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col466'),:OLD.COL466,:NEW.COL466,usrName);
  END IF;

IF NVL(:OLD.COL467,' ') != NVL(:NEW.COL467,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col467'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col467'),:OLD.COL467,:NEW.COL467,usrName);
  END IF;

IF NVL(:OLD.COL468,' ') != NVL(:NEW.COL468,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col468'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col468'),:OLD.COL468,:NEW.COL468,usrName);
  END IF;

IF NVL(:OLD.COL469,' ') != NVL(:NEW.COL469,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col469'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col469'),:OLD.COL469,:NEW.COL469,usrName);
  END IF;

IF NVL(:OLD.COL470,' ') != NVL(:NEW.COL470,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col470'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col470'),:OLD.COL470,:NEW.COL470,usrName);
  END IF;

IF NVL(:OLD.COL471,' ') != NVL(:NEW.COL471,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col471'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col471'),:OLD.COL471,:NEW.COL471,usrName);
  END IF;

IF NVL(:OLD.COL472,' ') != NVL(:NEW.COL472,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col472'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col472'),:OLD.COL472,:NEW.COL472,usrName);
  END IF;

IF NVL(:OLD.COL473,' ') != NVL(:NEW.COL473,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col473'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col473'),:OLD.COL473,:NEW.COL473,usrName);
  END IF;

IF NVL(:OLD.COL474,' ') != NVL(:NEW.COL474,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col474'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col474'),:OLD.COL474,:NEW.COL474,usrName);
  END IF;

IF NVL(:OLD.COL475,' ') != NVL(:NEW.COL475,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col475'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col475'),:OLD.COL475,:NEW.COL475,usrName);
  END IF;

IF NVL(:OLD.COL476,' ') != NVL(:NEW.COL476,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col476'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col476'),:OLD.COL476,:NEW.COL476,usrName);
  END IF;

IF NVL(:OLD.COL477,' ') != NVL(:NEW.COL477,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col477'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col477'),:OLD.COL477,:NEW.COL477,usrName);
  END IF;

IF NVL(:OLD.COL478,' ') != NVL(:NEW.COL478,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col478'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col478'),:OLD.COL478,:NEW.COL478,usrName);
  END IF;

IF NVL(:OLD.COL479,' ') != NVL(:NEW.COL479,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col479'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col479'),:OLD.COL479,:NEW.COL479,usrName);
  END IF;

IF NVL(:OLD.COL480,' ') != NVL(:NEW.COL480,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col480'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col480'),:OLD.COL480,:NEW.COL480,usrName);
  END IF;

IF NVL(:OLD.COL481,' ') != NVL(:NEW.COL481,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col481'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col481'),:OLD.COL481,:NEW.COL481,usrName);
  END IF;

IF NVL(:OLD.COL482,' ') != NVL(:NEW.COL482,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col482'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col482'),:OLD.COL482,:NEW.COL482,usrName);
  END IF;

IF NVL(:OLD.COL483,' ') != NVL(:NEW.COL483,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col483'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col483'),:OLD.COL483,:NEW.COL483,usrName);
  END IF;

IF NVL(:OLD.COL484,' ') != NVL(:NEW.COL484,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col484'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col484'),:OLD.COL484,:NEW.COL484,usrName);
  END IF;

IF NVL(:OLD.COL485,' ') != NVL(:NEW.COL485,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col485'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col485'),:OLD.COL485,:NEW.COL485,usrName);
  END IF;

IF NVL(:OLD.COL486,' ') != NVL(:NEW.COL486,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col486'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col486'),:OLD.COL486,:NEW.COL486,usrName);
  END IF;

IF NVL(:OLD.COL487,' ') != NVL(:NEW.COL487,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col487'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col487'),:OLD.COL487,:NEW.COL487,usrName);
  END IF;

IF NVL(:OLD.COL488,' ') != NVL(:NEW.COL488,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col488'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col488'),:OLD.COL488,:NEW.COL488,usrName);
  END IF;

IF NVL(:OLD.COL489,' ') != NVL(:NEW.COL489,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col489'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col489'),:OLD.COL489,:NEW.COL489,usrName);
  END IF;

IF NVL(:OLD.COL490,' ') != NVL(:NEW.COL490,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col490'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col490'),:OLD.COL490,:NEW.COL490,usrName);
  END IF;

IF NVL(:OLD.COL491,' ') != NVL(:NEW.COL491,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col491'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col491'),:OLD.COL491,:NEW.COL491,usrName);
  END IF;

IF NVL(:OLD.COL492,' ') != NVL(:NEW.COL492,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col492'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col492'),:OLD.COL492,:NEW.COL492,usrName);
  END IF;

IF NVL(:OLD.COL493,' ') != NVL(:NEW.COL493,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col493'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col493'),:OLD.COL493,:NEW.COL493,usrName);
  END IF;

IF NVL(:OLD.COL494,' ') != NVL(:NEW.COL494,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col494'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col494'),:OLD.COL494,:NEW.COL494,usrName);
  END IF;

IF NVL(:OLD.COL495,' ') != NVL(:NEW.COL495,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col495'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col495'),:OLD.COL495,:NEW.COL495,usrName);
  END IF;

IF NVL(:OLD.COL496,' ') != NVL(:NEW.COL496,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col496'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col496'),:OLD.COL496,:NEW.COL496,usrName);
  END IF;

IF NVL(:OLD.COL497,' ') != NVL(:NEW.COL497,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col497'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col497'),:OLD.COL497,:NEW.COL497,usrName);
  END IF;

IF NVL(:OLD.COL498,' ') != NVL(:NEW.COL498,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col498'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col498'),:OLD.COL498,:NEW.COL498,usrName);
  END IF;

IF NVL(:OLD.COL499,' ') != NVL(:NEW.COL499,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col499'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col499'),:OLD.COL499,:NEW.COL499,usrName);
  END IF;

IF NVL(:OLD.COL500,' ') != NVL(:NEW.COL500,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col500'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col500'),:OLD.COL500,:NEW.COL500,usrName);
  END IF;

IF NVL(:OLD.COL501,' ') != NVL(:NEW.COL501,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col501'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col501'),:OLD.COL501,:NEW.COL501,usrName);
  END IF;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


