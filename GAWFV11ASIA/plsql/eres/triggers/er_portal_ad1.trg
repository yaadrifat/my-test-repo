-- Creating trigger to delete all row the related in ER_PORTALPWD_HISTORY in case a portal is deleted from ER_PORTAL
CREATE OR REPLACE TRIGGER ER_PORTAL_AD1 AFTER DELETE ON ER_PORTAL REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
-- This will delete all the records for a portal whether they are for portal patients or the portal itself.
 DELETE FROM ER_PORTALPWD_HISTORY ph WHERE ph.FK_PORTAL = :OLD.PK_PORTAL AND ph.FK_ACCOUNT = :OLD.FK_ACCOUNT;
END;
/