CREATE  OR REPLACE TRIGGER ER_USER_BI0 BEFORE INSERT ON ER_USER        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_USER',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_JOBTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_JOBTYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SKIN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SKIN', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SPL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SPL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_THEME;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_THEME', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_GRP_DEFAULT', :NEW.FK_GRP_DEFAULT);
       Audit_Trail.column_insert (raid, 'FK_LOGINMODULE', :NEW.FK_LOGINMODULE);
       Audit_Trail.column_insert (raid, 'FK_PERADD', :NEW.FK_PERADD);
       Audit_Trail.column_insert (raid, 'FK_SITEID', :NEW.FK_SITEID);
       Audit_Trail.column_insert (raid, 'FK_TIMEZONE', :NEW.FK_TIMEZONE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_USER', :NEW.PK_USER);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'USER_CODE', :NEW.USER_CODE);
       Audit_Trail.column_insert (raid, 'USER_HIDDEN', :NEW.USER_HIDDEN);
       Audit_Trail.column_insert (raid, 'USR_ANS', :NEW.USR_ANS);
       Audit_Trail.column_insert (raid, 'USR_ATTEMPTCNT', :NEW.USR_ATTEMPTCNT);
       Audit_Trail.column_insert (raid, 'USR_CODE', :NEW.USR_CODE);
       Audit_Trail.column_insert (raid, 'USR_CURRNTSESSID', :NEW.USR_CURRNTSESSID);
       Audit_Trail.column_insert (raid, 'USR_ES', :NEW.USR_ES);
       Audit_Trail.column_insert (raid, 'USR_ESDAYS', :NEW.USR_ESDAYS);
       Audit_Trail.column_insert (raid, 'USR_ESREMIND', :NEW.USR_ESREMIND);
       Audit_Trail.column_insert (raid, 'USR_ESXPIRY', :NEW.USR_ESXPIRY);
       Audit_Trail.column_insert (raid, 'USR_FIRSTNAME', :NEW.USR_FIRSTNAME);
       Audit_Trail.column_insert (raid, 'USR_LASTNAME', :NEW.USR_LASTNAME);
       Audit_Trail.column_insert (raid, 'USR_LOGGEDINFLAG', :NEW.USR_LOGGEDINFLAG);
       Audit_Trail.column_insert (raid, 'USR_LOGINMODULEMAP', :NEW.USR_LOGINMODULEMAP);
       Audit_Trail.column_insert (raid, 'USR_LOGNAME', :NEW.USR_LOGNAME);
       Audit_Trail.column_insert (raid, 'USR_MIDNAME', :NEW.USR_MIDNAME);
       Audit_Trail.column_insert (raid, 'USR_PAHSEINV', :NEW.USR_PAHSEINV);
       Audit_Trail.column_insert (raid, 'USR_PWD', :NEW.USR_PWD);
       Audit_Trail.column_insert (raid, 'USR_PWDDAYS', :NEW.USR_PWDDAYS);
       Audit_Trail.column_insert (raid, 'USR_PWDREMIND', :NEW.USR_PWDREMIND);
       Audit_Trail.column_insert (raid, 'USR_PWDXPIRY', :NEW.USR_PWDXPIRY);
       Audit_Trail.column_insert (raid, 'USR_SECQUES', :NEW.USR_SECQUES);
       Audit_Trail.column_insert (raid, 'USR_SESSTIME', :NEW.USR_SESSTIME);
       Audit_Trail.column_insert (raid, 'USR_SITEFLAG', :NEW.USR_SITEFLAG);
       Audit_Trail.column_insert (raid, 'USR_STAT', :NEW.USR_STAT);
       Audit_Trail.column_insert (raid, 'USR_TYPE', :NEW.USR_TYPE);
       Audit_Trail.column_insert (raid, 'USR_WRKEXP', :NEW.USR_WRKEXP);
COMMIT;
END;
/
