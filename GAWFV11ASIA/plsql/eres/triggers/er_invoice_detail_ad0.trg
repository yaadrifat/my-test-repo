create or replace
TRIGGER ER_INVOICE_DETAIL_AD0 AFTER DELETE ON ER_INVOICE_DETAIL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;


    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
       Audit_Trail.record_transaction (raid, 'ER_INVOICE_DETAIL', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'PK_INVDETAIL', :OLD.PK_INVDETAIL);
       Audit_Trail.column_delete (raid, 'FK_INV', :OLD.FK_INV);
       Audit_Trail.column_delete (raid, 'FK_MILESTONE', :OLD.FK_MILESTONE);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'AMOUNT_DUE', :OLD.AMOUNT_DUE);
       Audit_Trail.column_delete (raid, 'AMOUNT_INVOICED', :OLD.AMOUNT_INVOICED);
       Audit_Trail.column_delete (raid, 'AMOUNT_HOLDBACK', :OLD.AMOUNT_HOLDBACK);
       Audit_Trail.column_delete (raid, 'PAYMENT_DUEDATE', :OLD.PAYMENT_DUEDATE);
       Audit_Trail.column_delete (raid, 'DETAIL_TYPE', :OLD.DETAIL_TYPE);
       Audit_Trail.column_delete (raid, 'DISPLAY_DETAIL', :OLD.DISPLAY_DETAIL);
       Audit_Trail.column_delete (raid, 'MILESTONES_ACHIEVED', :OLD.MILESTONES_ACHIEVED);
       Audit_Trail.column_delete (raid, 'ACHIEVED_ON', :OLD.ACHIEVED_ON);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'FK_MILEACHIEVED', :OLD.FK_MILEACHIEVED);
COMMIT;
END;