CREATE OR REPLACE TRIGGER "ER_REPFLDVALIDATE_BU_LM" 
BEFORE UPDATE
ON ER_REPFLDVALIDATE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Before update
begin :new.last_modified_date := sysdate;
end;
/


