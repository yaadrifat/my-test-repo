create or replace
TRIGGER ER_MILESTONE_AD0 AFTER DELETE ON ER_MILESTONE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;


    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_MILESTONE', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_BGTCAL', :OLD.FK_BGTCAL);
       Audit_Trail.column_delete (raid, 'FK_BGTSECTION', :OLD.FK_BGTSECTION);
       Audit_Trail.column_delete (raid, 'FK_BUDGET', :OLD.FK_BUDGET);
       Audit_Trail.column_delete (raid, 'FK_CAL', :OLD.FK_CAL);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_MILESTONE_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_MILESTONE_STAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_RULE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_RULE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_EVENTASSOC', :OLD.FK_EVENTASSOC);
       Audit_Trail.column_delete (raid, 'FK_LINEITEM', :OLD.FK_LINEITEM);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_VISIT', :OLD.FK_VISIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_CHECKED_ON', :OLD.LAST_CHECKED_ON);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'MILESTONE_ACHIEVEDCOUNT', :OLD.MILESTONE_ACHIEVEDCOUNT);
       Audit_Trail.column_delete (raid, 'MILESTONE_AMOUNT', :OLD.MILESTONE_AMOUNT);
       Audit_Trail.column_delete (raid, 'MILESTONE_COUNT', :OLD.MILESTONE_COUNT);
       Audit_Trail.column_delete (raid, 'MILESTONE_DATE_FROM', :OLD.MILESTONE_DATE_FROM);
       Audit_Trail.column_delete (raid, 'MILESTONE_DATE_TO', :OLD.MILESTONE_DATE_TO);
       Audit_Trail.column_delete (raid, 'MILESTONE_DELFLAG', :OLD.MILESTONE_DELFLAG);
       Audit_Trail.column_delete (raid, 'MILESTONE_DESCRIPTION', :OLD.MILESTONE_DESCRIPTION);
       Audit_Trail.column_delete (raid, 'MILESTONE_EVENTSTATUS', :OLD.MILESTONE_EVENTSTATUS);
       Audit_Trail.column_delete (raid, 'MILESTONE_ISACTIVE', :OLD.MILESTONE_ISACTIVE);
       Audit_Trail.column_delete (raid, 'MILESTONE_LIMIT', :OLD.MILESTONE_LIMIT);
       Audit_Trail.column_delete (raid, 'MILESTONE_PAYBYUNIT', :OLD.MILESTONE_PAYBYUNIT);
       Audit_Trail.column_delete (raid, 'MILESTONE_PAYDUEBY', :OLD.MILESTONE_PAYDUEBY);
       Audit_Trail.column_delete (raid, 'MILESTONE_PAYFOR', :OLD.MILESTONE_PAYFOR);
       Audit_Trail.column_delete (raid, 'MILESTONE_PAYTYPE', :OLD.MILESTONE_PAYTYPE);
       Audit_Trail.column_delete (raid, 'MILESTONE_STATUS', :OLD.MILESTONE_STATUS);
       Audit_Trail.column_delete (raid, 'MILESTONE_TYPE', :OLD.MILESTONE_TYPE);
       Audit_Trail.column_delete (raid, 'MILESTONE_USERSTO', :OLD.MILESTONE_USERSTO);
       Audit_Trail.column_delete (raid, 'MILESTONE_VISIT', :OLD.MILESTONE_VISIT);
       Audit_Trail.column_delete (raid, 'PK_MILESTONE', :OLD.PK_MILESTONE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'MILESTONE_HOLDBACK', :OLD.MILESTONE_HOLDBACK);
COMMIT;
END;