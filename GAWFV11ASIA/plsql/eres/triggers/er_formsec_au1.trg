CREATE OR REPLACE TRIGGER "ER_FORMSEC_AU1" 
AFTER UPDATE
OF FORMSEC_REPNO
ON ER_FORMSEC REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
	j NUMBER ;
	k NUMBER ;
	m NUMBER ;
	v_user NUMBER ;
	v_ipadd VARCHAR2(15) ;
	v_fldlib_id NUMBER ;
	v_newfld NUMBER;
	v_fld_tocopy NUMBER(10);
	v_new_repformfld NUMBER(10);
	v_fld_systemid VARCHAR2(50) ;
	v_fld_oldsystemid VARCHAR2(50) ;
	v_repformfld_xsl VARCHAR(4000);
	v_formfld_xsl   VARCHAR2(4000);
    v_formfld_xsl_init   VARCHAR2(4000);
    v_formfld_javascr VARCHAR2(4000) ;
    v_formfld_javascr_init VARCHAR2(4000) ;
	v_fk_formsec   NUMBER ;
	v_formfld_seq NUMBER ;
	v_formlib_id NUMBER ;
	v_formsec_fmt CHAR(1);
    v_formfld_xsl_1   VARCHAR2(4000);
    v_formfld_xsl_2   VARCHAR2(4000);
    v_formfld_xsl_3   VARCHAR2(4000);
    v_pos_1  NUMBER ;
    v_pos_2  NUMBER ;
    v_pos_3  NUMBER ;
    v_pos_endtd NUMBER;
    len NUMBER ;
	v_strlen NUMBER ;
	v_fld_type CHAR(1) ;
	v_orig_lkp_dispval VARCHAR2(2000);
	v_orig_lkp_formfld NUMBER;
    V_FLD_KEYWORD Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	v_count NUMBER;
	V_KEYWORDSETSTR VARCHAR2(32000);
	V_KEYWORDSET VARCHAR2(4000);
	V_POS NUMBER;
	v_mapset VARCHAR2(200);
	v_pipepos NUMBER;
	v_mapfld_sysid VARCHAR2(50);
	v_mapfld_pk_field NUMBER;
	V_ARRDISP_FLDID Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	V_ARRDISP_SYSID Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
    v_lkp_set NUMBER;
	v_lkp_rep_fld NUMBER;
	v_lkp_rep_lkpdispval VARCHAR2(200);
    v_new_mapfld_sysid VARCHAR2(50);
   	v_lkp_old_rep_lkpdispval VARCHAR2(2000);
	v_pk_repformfld NUMBER;
	v_fld_datatype VARCHAR2(3);
	v_new_repfldvalidate NUMBER;
	v_fldval_javascr VARCHAR2(4000);
	v_pk_fld NUMBER;
	v_field_id NUMBER;
	v_system_id VARCHAR2(50);
	v_fld_align VARCHAR2(20);

BEGIN


	IF ( :NEW.formsec_repno < :OLD.formsec_repno ) THEN

	 UPDATE ER_FLDLIB
	 	SET record_type = 'D'
		WHERE pk_field IN
				( SELECT fk_field FROM  ER_REPFORMFLD WHERE fk_formsec = :NEW.pk_formsec
					AND repformfld_set > :NEW.formsec_repno )  ;
        DELETE  ER_FLDRESP
		WHERE fk_field IN ( SELECT fk_field FROM  ER_REPFORMFLD WHERE fk_formsec = :NEW.pk_formsec
                  			AND repformfld_set > :NEW.formsec_repno  ) ;
		DELETE ER_REPFORMFLD
		WHERE fk_formsec = :NEW.pk_formsec
		AND repformfld_set > :NEW.formsec_repno  ;


	ELSE


		j :=  :NEW.formsec_repno - :OLD.formsec_repno ;
		v_user := :NEW.last_modified_by ;
		v_ipadd := :NEW.ip_add ;
          v_formsec_fmt := :NEW.formsec_fmt ;
	   	-- loop through all the fields in the ER_FORMFLD corresponding to this section
		FOR i  IN ( SELECT pk_formfld, fk_field
			   	    FROM ER_FORMFLD
					WHERE fk_formsec = :NEW.pk_formsec AND record_type <> 'D'
					ORDER BY formfld_seq ASC)
			LOOP
			k := 1 ;
			-- to select the corresponding fk_fldlib before copying to the er_fldlib
			v_fldlib_id :=  i.fk_field;

			WHILE k <= j  LOOP
			BEGIN
				--First copy the field to the ER_FLDLIB
           	     Pkg_Form.SP_REPEAT_FIELD(v_fldlib_id, v_user , v_ipadd, v_newfld);

				IF v_newfld > 0 THEN
		   		--We get the the FLD_SYSTEMID from the ER_FLDLIB table for using in the XSL
			  	 SELECT FLD_SYSTEMID , FLD_TYPE, FLD_DATATYPE,fld_align
				 INTO v_fld_systemid , v_fld_type,v_fld_datatype,v_fld_align
				 FROM ER_FLDLIB
				 WHERE PK_FIELD = v_newfld ;

				 SELECT
				 formfld_xsl , formfld_javascr , formfld_seq
				 INTO
				 v_formfld_xsl_init , v_formfld_javascr_init , v_formfld_seq
				 FROM ER_FORMFLD
				 WHERE pk_formfld = i.pk_formfld ;

				 -- get old system id
				 SELECT FLD_SYSTEMID
				 INTO v_fld_oldsystemid
				 FROM ER_FLDLIB
				 WHERE PK_FIELD = v_fldlib_id;

				v_formfld_xsl_1 := REPLACE(v_formfld_xsl_init,v_fld_oldsystemid,v_fld_systemid);


                  v_formfld_javascr := REPLACE(v_formfld_javascr_init,v_fld_oldsystemid,v_fld_systemid);
		   -- The new form field id generated from the seq_er_repformfld sequence.
               SELECT seq_er_repformfld.NEXTVAL
               INTO v_new_repformfld
               FROM dual ;

			   --we change the xsl and remove the <label> tags if the section format is tabular
                IF v_formsec_fmt = 'T'  THEN

			   		--modified by sonia abrol, 03/16/05 to use a function to cut the label

			 		 SELECT Pkg_Form.f_cut_fieldlabel(v_formfld_xsl_1,v_fld_type, v_fld_datatype,v_fld_align,v_newfld )
			 		 INTO v_formfld_xsl  FROM dual;


				/*
				   --modified by Sonika on Nov 21, 03 search only for <td width=''15%''  as align has been added in td
	                --modified by Sonika on April 19, 04 search only for <td
			       v_pos_1 := INSTR( v_formfld_xsl_1, '<td') ;
		     	   v_formfld_xsl_2 := SUBSTR(v_formfld_xsl_1 , 0 ,v_pos_1-1);
			       len := LENGTH(v_formfld_xsl_1);
			       v_pos_2 := INSTR(v_formfld_xsl_1 , '</label>');
				    --Modified by Sonika Talwar on April 22, 04 to remove help icon addition incase of Tabular section
	                --find </td> end tag of label
				    --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
	   		       v_pos_endtd := INSTR(v_formfld_xsl_1 , '</td>',v_pos_2);
	                --# of characters between </label> and </td>
				    v_strlen := LENGTH('</td>');
				    v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
			            v_formfld_xsl_3 := SUBSTR( v_formfld_xsl_1 , v_pos_2+v_strlen , len-1 ) ;
	                v_formfld_xsl := v_formfld_xsl_2 || v_formfld_xsl_3 ; */

			ELSE
			    v_formfld_xsl := v_formfld_xsl_1 ;
			END IF ;

			---------Enter all the respective data from existing ER_FORMFLD to ER_REPFORMFLD----------------
			INSERT INTO ER_REPFORMFLD
			( PK_REPFORMFLD , FK_FORMSEC , FK_FIELD , FK_FORMFLD,
			  REPFORMFLD_XSL,REPFORMFLD_JAVASCR, REPFORMFLD_SEQ , REPFORMFLD_SET  ,RECORD_TYPE,
			  CREATOR , CREATED_ON , IP_ADD)
			VALUES ( v_new_repformfld ,:NEW.pk_formsec , v_newfld , i.pk_formfld,
			v_formfld_xsl ,v_formfld_javascr ,v_formfld_seq , k + :OLD.formsec_repno ,
    	          'N' , v_user , SYSDATE , v_ipadd ) ;


			BEGIN
		     --get validation javascript of main field
			 SELECT REPLACE( FLDVALIDATE_JAVASCR , v_fld_oldsystemid ,v_fld_systemid)
			   INTO v_fldval_javascr
			   FROM ER_FLDVALIDATE
			   WHERE fk_fldlib = v_newfld ;

     		   -- The new repeated form field validation id generated from the seq_er_repfldvalidate sequence.
                 SELECT seq_er_repfldvalidate.NEXTVAL
                 INTO v_new_repfldvalidate
                 FROM dual ;

			 --insert repeated field validation javascript
			 INSERT INTO ER_REPFLDVALIDATE(
			  PK_REPFLDVALIDATE  , FK_REPFORMFLD, FK_FIELD ,
			  REPFLDVALIDATE_JAVASCR,
			  RECORD_TYPE, CREATOR, CREATED_ON, IP_ADD)
			 VALUES (v_new_repfldvalidate, v_new_repformfld , v_newfld,
			  v_fldval_javascr ,
			  'N' , v_user , SYSDATE , v_ipadd ) ;

	          EXCEPTION WHEN NO_DATA_FOUND THEN
			     P('no data');
		    END;


			END IF;


	      	END ;--end of insert begin


 			k := k+1;


		  END LOOP; -- WHILE the diff between old and new repno of the er_formsec


		END LOOP ; --for loop for the er_formfld


	-- for all the lookup type fields in the form's section
	   	FOR i  IN ( SELECT pk_formfld, fk_field, fld_datatype,fld_lkpdispval
			   	    FROM ER_FORMFLD frmfld , ER_FLDLIB
					WHERE frmfld.fk_formsec = :NEW.pk_formsec AND
					frmfld.record_type <> 'D'
					AND pk_field =  frmfld.fk_field AND fld_datatype = 'ML'
					ORDER BY formfld_seq ASC)
		LOOP --get all lookup fields in the section
			v_orig_lkp_dispval := i.fld_lkpdispval;
			v_orig_lkp_formfld := i.pk_formfld;
			--parse the v_orig_lkp_dispval to find out the fields this lookup is associated to
			 -- first loop for
		 --p('in trigger loop 1 v_orig_lkp_dispval ' || v_orig_lkp_dispval );
 		 --p('in trigger loop 1 v_orig_lkp_field  ' || v_orig_lkp_formfld );
			 v_count := 0;
			 V_KEYWORDSETSTR := v_orig_lkp_dispval || '~';
			 V_KEYWORDSET := v_orig_lkp_dispval || '~';
			 V_POS := 0;
			    LOOP
				    V_POS := INSTR (V_KEYWORDSETSTR, '~');
					V_KEYWORDSET := SUBSTR (V_KEYWORDSETSTR, 1, V_POS - 1);
					      EXIT WHEN V_KEYWORDSET IS NULL;
				 	V_COUNT := V_COUNT + 1;
					V_FLD_KEYWORD.EXTEND;
					V_FLD_KEYWORD(V_COUNT) := V_KEYWORDSET;
			 		--p('in trigger loop 2 V_FLD_KEYWORD ' || V_KEYWORDSET );
					V_KEYWORDSETSTR := SUBSTR (V_KEYWORDSETSTR, V_POS + 1);
				  END LOOP ;
				 v_count := 0;
				 FOR s IN 1..V_FLD_KEYWORD.COUNT -- put old sys id and pk_field in array
				  LOOP
				  	  v_mapset := trim(V_FLD_KEYWORD(s));
					  V_PIPEPOS := INSTR (v_mapset, '|');
					  v_mapfld_sysid := SUBSTR (v_mapset ,1,V_PIPEPOS - 1); --got system id
					  -- get field id for this system id
					  BEGIN
						  SELECT pk_field
						  INTO v_mapfld_pk_field
						  FROM ER_FLDLIB WHERE fld_systemid = trim(v_mapfld_sysid);
						  V_COUNT := V_COUNT + 1;
						  V_ARRDISP_SYSID.EXTEND;
						  V_ARRDISP_SYSID(v_count) :=  v_mapfld_sysid;
						  V_ARRDISP_FLDID.EXTEND;
						  V_ARRDISP_FLDID(v_count) :=  v_mapfld_pk_field;
					--	  p('in trigger loop 3 v_mapfld_sysid ' || v_mapfld_sysid );
						--  p('in trigger loop 3 v_mapfld_pk_field ' || v_mapfld_pk_field );
					  EXCEPTION WHEN NO_DATA_FOUND  THEN
					  	   v_mapfld_pk_field := 0;
				      END ;
				 END LOOP;
				-- p('in trigger :old.formsec_repno' || :old.formsec_repno);
			 ----get all lookup fields from er_repformfld that are rep of the orig
		 		 FOR a IN (SELECT pk_repformfld,fk_field,fld_lkpdispval,repformfld_set
			   	       	  FROM ER_REPFORMFLD rep, ER_FLDLIB
						  WHERE rep.fk_formfld = v_orig_lkp_formfld AND
						  		rep.repformfld_set > NVL(:OLD.formsec_repno,0) AND
								pk_field =  rep.fk_field)
 				  LOOP
					v_lkp_set := a.repformfld_set;
					v_lkp_rep_lkpdispval := a.fld_lkpdispval;
					v_lkp_old_rep_lkpdispval := a.fld_lkpdispval;
					v_lkp_rep_fld := a.fk_field;
					v_pk_repformfld := a.pk_repformfld;
					 --p('in loop 1 v_lkp_set' || v_lkp_set);
				  --p('in loop 1 v_lkp_rep_lkpdispval' || v_lkp_rep_lkpdispval);
					--change the v_lkp_rep_lkpdispval for sys id of new mapped fields
						 FOR k IN 1..V_ARRDISP_FLDID.COUNT
							LOOP
					  			v_mapfld_sysid    := trim(V_ARRDISP_SYSID(k));
								v_mapfld_pk_field := trim(V_ARRDISP_FLDID(k));

							     BEGIN
								 	  SELECT fld_systemid
									  INTO v_new_mapfld_sysid
									  FROM ER_FLDLIB, ER_REPFORMFLD rf, ER_FORMFLD ff
									  WHERE  ff.fk_field = v_mapfld_pk_field AND ff.fk_formsec = :NEW.pk_formsec  AND
									  ff.pk_formfld = rf.fk_formfld AND rf.repformfld_set = v_lkp_set  AND
									  rf.fk_field = pk_field;
								 EXCEPTION WHEN NO_DATA_FOUND  THEN
								   	   v_new_mapfld_sysid := '';
								 END ;

							    v_lkp_rep_lkpdispval := REPLACE(v_lkp_rep_lkpdispval,v_mapfld_sysid,v_new_mapfld_sysid);
					   	    END LOOP;
					  -- update the rep lkp field with new lookuyp disp val

					  UPDATE ER_FLDLIB
					  SET fld_lkpdispval = v_lkp_rep_lkpdispval
					  WHERE pk_field = v_lkp_rep_fld;
					-- update the lookup fld xsl with new fld-keyword displayval
				 	  UPDATE ER_REPFORMFLD
					  SET repformfld_xsl = REPLACE(repformfld_xsl, v_lkp_old_rep_lkpdispval, v_lkp_rep_lkpdispval)
					  WHERE fk_field = v_lkp_rep_fld AND pk_repformfld = v_pk_repformfld;
				  END LOOP;
			 	 V_FLD_KEYWORD.DELETE;
				 V_ARRDISP_FLDID.DELETE;
				 V_ARRDISP_SYSID.DELETE;
		END LOOP;
	-- end of change for lookup type field
	END IF ;



	-- To set the FORM_XSLREFRESH  to 1 after the change of the repeat number
    v_formlib_id := :NEW.fk_formlib;
    UPDATE ER_FORMLIB
    SET FORM_XSLREFRESH = 1
    WHERE pk_formlib = v_formlib_id  ;

END ;
/


