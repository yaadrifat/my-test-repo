create or replace
TRIGGER ER_MILESTONE_BI0 BEFORE INSERT ON ER_MILESTONE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_MILESTONE',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_BGTCAL', :NEW.FK_BGTCAL);
       Audit_Trail.column_insert (raid, 'FK_BGTSECTION', :NEW.FK_BGTSECTION);
       Audit_Trail.column_insert (raid, 'FK_BUDGET', :NEW.FK_BUDGET);
       Audit_Trail.column_insert (raid, 'FK_CAL', :NEW.FK_CAL);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_MILESTONE_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_MILESTONE_STAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_RULE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_RULE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_EVENTASSOC', :NEW.FK_EVENTASSOC);
       Audit_Trail.column_insert (raid, 'FK_LINEITEM', :NEW.FK_LINEITEM);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_VISIT', :NEW.FK_VISIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_CHECKED_ON', :NEW.LAST_CHECKED_ON);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'MILESTONE_ACHIEVEDCOUNT', :NEW.MILESTONE_ACHIEVEDCOUNT);
       Audit_Trail.column_insert (raid, 'MILESTONE_AMOUNT', :NEW.MILESTONE_AMOUNT);
       Audit_Trail.column_insert (raid, 'MILESTONE_COUNT', :NEW.MILESTONE_COUNT);
       Audit_Trail.column_insert (raid, 'MILESTONE_DATE_FROM', :NEW.MILESTONE_DATE_FROM);
       Audit_Trail.column_insert (raid, 'MILESTONE_DATE_TO', :NEW.MILESTONE_DATE_TO);
       Audit_Trail.column_insert (raid, 'MILESTONE_DELFLAG', :NEW.MILESTONE_DELFLAG);
       Audit_Trail.column_insert (raid, 'MILESTONE_DESCRIPTION', :NEW.MILESTONE_DESCRIPTION);
       Audit_Trail.column_insert (raid, 'MILESTONE_EVENTSTATUS', :NEW.MILESTONE_EVENTSTATUS);
       Audit_Trail.column_insert (raid, 'MILESTONE_ISACTIVE', :NEW.MILESTONE_ISACTIVE);
       Audit_Trail.column_insert (raid, 'MILESTONE_LIMIT', :NEW.MILESTONE_LIMIT);
       Audit_Trail.column_insert (raid, 'MILESTONE_PAYBYUNIT', :NEW.MILESTONE_PAYBYUNIT);
       Audit_Trail.column_insert (raid, 'MILESTONE_PAYDUEBY', :NEW.MILESTONE_PAYDUEBY);
       Audit_Trail.column_insert (raid, 'MILESTONE_PAYFOR', :NEW.MILESTONE_PAYFOR);
       Audit_Trail.column_insert (raid, 'MILESTONE_PAYTYPE', :NEW.MILESTONE_PAYTYPE);
       Audit_Trail.column_insert (raid, 'MILESTONE_STATUS', :NEW.MILESTONE_STATUS);
       Audit_Trail.column_insert (raid, 'MILESTONE_TYPE', :NEW.MILESTONE_TYPE);
       Audit_Trail.column_insert (raid, 'MILESTONE_USERSTO', :NEW.MILESTONE_USERSTO);
       Audit_Trail.column_insert (raid, 'MILESTONE_VISIT', :NEW.MILESTONE_VISIT);
       Audit_Trail.column_insert (raid, 'PK_MILESTONE', :NEW.PK_MILESTONE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'MILESTONE_HOLDBACK', :NEW.MILESTONE_HOLDBACK);
COMMIT;
END;