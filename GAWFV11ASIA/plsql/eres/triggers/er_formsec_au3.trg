CREATE OR REPLACE TRIGGER "ER_FORMSEC_AU3" 
AFTER UPDATE OF FORMSEC_SEQ,FORMSEC_NAME
ON ER_FORMSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_formlib_id Number ;

Begin

v_formlib_id := :new.fk_formlib ;

update er_formlib
set FORM_XSLREFRESH  = 1
where pk_formlib = v_formlib_id    ;

END ;
/


