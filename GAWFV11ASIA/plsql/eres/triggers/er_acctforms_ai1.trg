CREATE OR REPLACE TRIGGER "ER_ACCTFORMS_AI1" 
AFTER INSERT
ON ER_ACCTFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE



BEGIN

/* Whenever a form is answered it increases the datacount by one */
update
er_linkedforms
set lf_datacnt = lf_datacnt + 1
where fk_formlib = :new.fk_formlib ;



END;
/


