CREATE OR REPLACE PACKAGE BODY        "PKG_ADV" IS
  PROCEDURE SP_CALC_TOXICITY(p_toxicity_group NUMBER,p_labdate VARCHAR2, p_unit VARCHAR2,
		      p_labsite NUMBER, p_result VARCHAR2 ,p_lln NUMBER,p_uln NUMBER ,p_usegivenrange NUMBER, p_nci_ver VARCHAR2, p_patient NUMBER, o_grade OUT NUMBER , o_ulnused OUT NUMBER, o_llnused OUT NUMBER)
   AS
  /* Stored Procedure to calculate toxicity for lab based adverse events
     Author : Sonia Sahni
     Date : 9th Sept 03
     Notes for setting up the NCI LIB :
	    1. For exponent based calculations change data to - power(b,n). Ex. 10e9 should be changed to power(10,9)
	    2. For % based calculations, add % to first expression (lower limit expression) also
	    3. When operator1 is '<' / '<=', operator2 becomes '>='
	    4. When operator1 is '>=' / '>', operator2 becomes '<='
	    5. There will be a separate row for each lab test unit
  */
	v_Labrange_operator1 VARCHAR2(50);
    v_Labrange_expr1 VARCHAR2(50);
 	v_Labrange_operator2 VARCHAR2(50);
  	v_Labrange_expr2 VARCHAR2(50);
	v_lln NUMBER;
	v_uln NUMBER;
	v_val NUMBER;
	v_grade NUMBER;
	v_exprval1 NUMBER;
	v_exprval2 NUMBER;
	v_unit VARCHAR2(50);
	v_labdate DATE;

	v_pat_dob DATE;
	v_pat_gender CHAR(1);
	v_pat_age_days NUMBER;

	v_result NUMBER;
	v_nci_ver VARCHAR2(10);

  BEGIN

    v_unit := p_unit;

	v_result := TO_NUMBER(p_result);

	IF LENGTH(trim(p_labdate)) > 0 THEN
		v_labdate  := TO_DATE(p_labdate,PKG_DATEUTIL.F_GET_DATEFORMAT);
	ELSE
		v_labdate  := SYSDATE ;
	END IF;


	v_lln := p_lln;
	v_uln := p_uln;

	o_grade := -1 ;


	-- get patient data

	SELECT person_dob, DECODE(trim(c.codelst_subtyp),'female','F','male','M','M'), TRUNC(SYSDATE) - NVL(person_dob,TRUNC(SYSDATE))
	INTO v_pat_dob, v_pat_gender, v_pat_age_days
	FROM person P, ER_CODELST c
	WHERE pk_person = p_patient AND
	c.pk_codelst (+)= P.fk_codelst_gender;



	FOR i IN (SELECT nci_operator1, nci_expr1, NVL(nci_operator2,'0') nci_operator2, nci_expr2,grade,
		  labrange_lln, labrange_uln
		  FROM erv_nciadvlib WHERE
		  lkptype_version =  p_nci_ver AND
		  (toxicity_groupid =  p_toxicity_group AND  NVL(fk_site,0)  = NVL(p_labsite,0) AND
		   NVL(nci_unit,1) = NVL(v_unit,1) )
	      	AND (
					(v_labdate >= labrange_validfrom AND v_labdate <= NVL(labrange_validtill, TO_DATE('01/01/1900',PKG_DATEUTIL.F_GET_DATEFORMAT )) ) OR
					(v_labdate >= labrange_validfrom AND labrange_validtill IS NULL )
				)
		   AND DECODE( trim(NVL(labrange_gender,'')),'',v_pat_gender,'M','M','F','F','M') = v_pat_gender AND
		       v_pat_age_days >= NVL(LABRANGE_AGEFRM,0) AND   	 -- for age
   		       v_pat_age_days <= NVL(LABRANGE_AGETO,v_pat_age_days)

		  )

	LOOP


		v_Labrange_operator1 := i.nci_operator1;
		v_Labrange_expr1 := i.nci_expr1;
		v_Labrange_operator2 := i.nci_operator2;
		v_Labrange_expr2 := i.nci_expr2;

		IF p_usegivenrange = 0 THEN -- if range is not provided as SP parameter
		   v_lln := i.labrange_lln;
		   v_uln := i.labrange_uln;
		END IF;



		v_grade := i.grade;
		-- replace lowerlimit
		v_Labrange_expr1 := REPLACE(v_Labrange_expr1,'LLN',v_lln);
		v_Labrange_expr2 := REPLACE(v_Labrange_expr2,'LLN',v_lln);
		-- replace upperlimit
		v_Labrange_expr1 := REPLACE(v_Labrange_expr1,'ULN',v_uln);
		v_Labrange_expr2 := REPLACE(v_Labrange_expr2,'ULN',v_uln);
		-- replace % in expressions with *1/100*. This is for expressions that have calculations based on percentageof LN or 		--ULN
		v_Labrange_expr1 := REPLACE(v_Labrange_expr1,'%','*1/100*');
		v_Labrange_expr2 := REPLACE(v_Labrange_expr2,'%','*1/100*');
		--evaluate expressions
 		EXECUTE IMMEDIATE 'select ' || v_Labrange_expr1 || ',' || NVL(v_Labrange_expr2,-1) || '  from dual'  INTO v_exprval1,v_exprval2;




	    IF v_Labrange_operator2 = '0' THEN -- if there is no second operator
 		  IF  v_Labrange_operator1 = '>'  THEN
			IF v_result > v_exprval1 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
              ELSIF v_Labrange_operator1 = '>=' THEN
			IF v_result >= v_exprval1 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF v_Labrange_operator1 = '<' THEN
			IF v_result < v_exprval1 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF v_Labrange_operator1 = '<=' THEN
			IF v_result <= v_exprval1 THEN -- criteria met
				o_grade := v_grade ;
				P('grade is ' || v_grade);
				EXIT;
			END IF;
		  END IF; -- end if  fo operator comparison
	    ELSE -- if there is second operator
		 IF  v_Labrange_operator1 = '>' AND  v_Labrange_operator2 = '<' THEN
			IF v_result > v_exprval1 AND v_result < v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF	v_Labrange_operator1 = '>=' AND  v_Labrange_operator2 = '<' THEN
			IF v_result >= v_exprval1 AND v_result < v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF	v_Labrange_operator1 = '>' AND  v_Labrange_operator2 = '<=' THEN
			IF v_result > v_exprval1 AND v_result <= v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF	v_Labrange_operator1 = '>=' AND  v_Labrange_operator2 = '<=' THEN
			IF v_result >= v_exprval1 AND v_result <= v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		  ELSIF	v_Labrange_operator1 = '<' AND  v_Labrange_operator2 = '>' THEN
			IF v_result < v_exprval1 AND v_result > v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		 ELSIF	v_Labrange_operator1 = '<' AND  v_Labrange_operator2 = '>=' THEN
			IF v_result < v_exprval1 AND v_result >= v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		 ELSIF	v_Labrange_operator1 = '<=' AND  v_Labrange_operator2 = '>' THEN
			IF v_result <= v_exprval1 AND v_result > v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
		 ELSIF	v_Labrange_operator1 = '<=' AND  v_Labrange_operator2 = '>=' THEN
			IF v_result <= v_exprval1 AND v_result >= v_exprval2 THEN -- criteria met
				P('grade is ' || v_grade);
				o_grade := v_grade ;
				EXIT;
			END IF;
 	     END IF;
	END IF;
	END LOOP;

	o_ulnused := v_uln;
	o_llnused := v_lln;

  END;


   PROCEDURE SP_GET_NCILIB(p_nci_ver VARCHAR2 , o_lib OUT Gk_Cv_Types.GenericCursorType)

   /* Stored Procedure to get NCI Lib list
     Author : Sonia Sahni
     Date : 11th Sept 03

   */
	AS

	BEGIN


   	 	  OPEN o_lib FOR 'select  distinct ADV_EVENT ADV_EVENT, TOXICITY_DESC TOXICITY_DESC, TOXICITY_GROUPID TOXICITY_GROUPID from erv_nciadvlib where  LKPTYPE_VERSION = ' || p_nci_ver || ' and ISLAB = ''1'' ';


   END;

   PROCEDURE SP_GET_TOXICITY_GRADE_LIST(p_nci_ver VARCHAR2 , p_toxicity_group NUMBER , o_lib OUT Gk_Cv_Types.GenericCursorType)

   /* Stored Procedure to get NCI grade list for a toxicity group
     Author : Sonia Sahni
     Date : 11th Sept 03

   */
	AS

	BEGIN
   	 	  OPEN o_lib FOR 'select distinct adv_event , grade, description from erv_nciadvlib  where  toxicity_groupid = ' || p_toxicity_group  || ' and LKPTYPE_VERSION = ' || p_nci_ver || ' order by 2' ;


   END;

 FUNCTION   f_get_adv_subtypes(p_subtype Varchar2) RETURN split_tbl
  /* Function to get a cursor of adverse event code subtypes, if p_subtype = 'ALL', return all codes otherwise return code subtype passed as parameter
     Author : Sonia Abrol
     Date : 05/23/06

   */
	AS
	v_sql varchar2(500);
	o_codes split_tbl;

	BEGIN

		 if upper(p_subtype) =  'ALL' then

		  		SELECT Pkg_Util.f_split(pkg_util.f_join(cursor(select  codelst_subtyp  from sch_codelst  where codelst_type = 'adve_type'),',')) INTO o_codes FROM dual;

			else

		  		SELECT Pkg_Util.f_split(pkg_util.f_join(cursor(select  codelst_subtyp  from sch_codelst  where codelst_type = 'adve_type'  and trim(codelst_subtyp) = p_subtype),',')) INTO o_codes FROM dual;

 		end if;

		return o_codes ;
    END;


END Pkg_Adv;
/


CREATE SYNONYM ESCH.PKG_ADV FOR PKG_ADV;


CREATE SYNONYM EPAT.PKG_ADV FOR PKG_ADV;


GRANT EXECUTE, DEBUG ON PKG_ADV TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_ADV TO ESCH;

