CREATE OR REPLACE PACKAGE BODY        "PKG_VIE" 
AS
PROCEDURE SP_MESSAGES
AS

/**************************************************************************************************
   **
   ** Author: Rajeev Kalathil 08/18/2003
   ** This stored procedure loops through unprocessed messages and updates the corresponding tables.
   **
***************************************************************************************************/

v_pk_message NUMBER;
v_pk_msgseg NUMBER;
v_msg_type VARCHAR2(10);
v_seg_name VARCHAR2(10);
v_status NUMBER;
v_errtxt VARCHAR2(4000);
v_fk_account  NUMBER;
v_fk_site  NUMBER;
v_fk_timezone  NUMBER;
v_creator  NUMBER;
BEGIN

plog.debug(pctx,'start');

-- Check for unprocessed messages
  FOR  i IN (SELECT a.PK_MESSAGE, a.MSG_TYPE, a.EVENT, a.SENDING_APP FROM VIE_MESSAGE a , VIE_MSGPROCESS b
    WHERE  status = 0 AND nvl(dbms_lob.GETLENGTH(message),0)>1 AND a.MSG_TYPE = b.MSG_TYPE AND a.EVENT = b.EVENT  ORDER BY msg_date)
    LOOP

		FOR x IN (SELECT codelst_type,velos_value FROM VIE_MAPPING_DETAILS WHERE sending_app = i.sending_app AND codelst_type IN ('_ACCOUNT_','_SITE_','_TIMEZONE_','_CREATOR_'))
		LOOP

			CASE x.codelst_type
			WHEN '_ACCOUNT_' THEN v_fk_account := x.velos_value;
			WHEN '_SITE_' THEN v_fk_site := x.velos_value;
			WHEN '_TIMEZONE_' THEN v_fk_timezone  := x.velos_value;
			WHEN '_CREATOR_' THEN v_creator  := x.velos_value;
			END CASE;
		END LOOP;
        plog.debug(pctx,   'v_fk_account=' ||v_fk_account||',v_fk_site='||v_fk_site);

		CASE i.msg_type
		WHEN 'ADT' THEN sp_adt(i.pk_message,i.sending_app,i.event);
		WHEN 'ORU' THEN sp_lab(i.pk_message,i.sending_app,v_fk_account,v_fk_site);
		END CASE;

        plog.debug(pctx,   'i.sending_app=' ||i.sending_app||',i.pk_message='||i.pk_message);


--	  	v_pk_message := i.pk_message;
--		v_msg_type := i.msg_type;
--		  FOR  i in (select PK_MSGSEG,SEG_NAME FROM VIE_MSGSEG where fk_message = v_pk_message)
--		  	   LOOP
--   			   	   v_status := 0;
--   				   v_errtxt := '';
--			   	   v_pk_msgseg := i.pk_msgseg;
--			 	   v_seg_name := i.seg_name;
--			 	   if (v_msg_type = 'ADT' and v_seg_name = 'PID') then
-- 			 	   	  sp_pid(v_pk_msgseg, v_status, v_errtxt);
--					  if v_status = -1 then
--					  	 insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,v_pk_message,SYSDATE,v_status,v_errtxt);
--						 update vie_message set status = -1 where pk_message = v_pk_message;
--					  else
--					  	 update vie_message set status = 1 where pk_message = v_pk_message;
--					  end if;
--					 end if;
--			 	   if (v_msg_type = 'ORU' and v_seg_name = 'R01') then
-- 			 	   	  sp_lab(v_pk_msgseg, v_status, v_errtxt);
--					  if v_status = -1 then
--					  	 insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,v_pk_message,SYSDATE,v_status,v_errtxt);
--						 update vie_message set status = -1 where pk_message = v_pk_message;
--					  else
--					  	 update vie_message set status = 1 where pk_message = v_pk_message;
--					  end if;
--					 end if;
--			 END LOOP;
    END LOOP ;
END SP_MESSAGES;

----------------------------------------------------------------------------
PROCEDURE SP_ADT(
p_pk_message IN NUMBER,
p_sending_app IN VARCHAR2,
p_event IN VARCHAR2
)
AS
v_pk_msgseg NUMBER;
-- v_seg_name varchar2(10);
v_status NUMBER;
v_errtxt VARCHAR2(4000);
v_vie_column VARCHAR2(10);
v_vie_column_value VARCHAR2(1000);
v_temp_sql VARCHAR2(4000);
BEGIN
--	 FOR  i in (select PK_MSGSEG,SEG_NAME FROM VIE_MSGSEG where fk_message = p_pk_message order by pk_msgseg)
--	 LOOP
	 	 v_status := 0;
	 	 v_errtxt := '';
--	 	 v_pk_msgseg := i.pk_msgseg;
--	 	 v_seg_name := i.seg_name;

	 	 IF (p_event='A04' OR p_event='A08') THEN
		 	SELECT PK_MSGSEG INTO v_pk_msgseg FROM VIE_MSGSEG WHERE fk_message = p_pk_message AND seg_name = 'PID';
	 	 	sp_pid(v_pk_msgseg, v_status, v_errtxt,p_sending_app);
	 	 END IF;

		 IF p_event = 'A34' OR p_event = 'A44' OR p_event='A18' OR p_event = 'A01' OR p_event = 'A05' THEN
		 	v_status := -2;
	 	    v_errtxt := TO_CHAR(p_pk_message) || ': ' || p_event ;

			CASE p_event
			WHEN 'A34' THEN v_errtxt := v_errtxt || '(Patient ID Merge)';
            WHEN 'A18' THEN v_errtxt := v_errtxt || '(Patient Records Merge)';
			WHEN 'A44' THEN v_errtxt := v_errtxt || '(Patient Account Merge)';
            WHEN 'A01' THEN v_errtxt := v_errtxt || '(Admit/Visit Notification )';
            WHEN 'A05' THEN v_errtxt := v_errtxt || '(Pre-admit a Patient)';
			END CASE;

		 	SELECT PK_MSGSEG INTO v_pk_msgseg FROM VIE_MSGSEG WHERE fk_message = p_pk_message AND seg_name = 'PID';
		 	SELECT VIE_COLUMN INTO v_vie_column FROM VIE_MAPPING WHERE seg_name = 'PID' AND velos_column = 'PERSON_CODE^^^';
		 	v_temp_sql := 'select ' || v_vie_column || ' from vie_msgseg where pk_msgseg = ' || v_pk_msgseg;
	 		EXECUTE IMMEDIATE v_temp_sql INTO v_vie_column_value;

	 	    v_errtxt := v_errtxt || ' - To Patient ID: ' || v_vie_column_value;

			BEGIN
			 	SELECT PK_MSGSEG INTO v_pk_msgseg FROM VIE_MSGSEG WHERE fk_message = p_pk_message AND seg_name = 'MRG';
			 	SELECT VIE_COLUMN INTO v_vie_column FROM VIE_MAPPING WHERE seg_name = 'MRG' AND velos_column = 'PERSON_CODE^^^';
			 	v_temp_sql := 'select ' || v_vie_column || ' from vie_msgseg where pk_msgseg = ' || v_pk_msgseg;
		 		EXECUTE IMMEDIATE v_temp_sql INTO v_vie_column_value;
		 	    v_errtxt := v_errtxt || '	 From Patient ID: ' || v_vie_column_value;
				EXCEPTION WHEN OTHERS THEN
		 	    		  v_errtxt := v_errtxt || '	 From Patient ID: ' ;
			END;

		 END IF;
--			if v_status < 0 then
		   	   INSERT INTO VIE_MSGLOG (pk_msglog,fk_message,log_date,log_status,log_text) VALUES (seq_vie_msglog.NEXTVAL,p_pk_message,SYSDATE,v_status,v_errtxt);
		   	   UPDATE VIE_MESSAGE SET status = v_status WHERE pk_message = p_pk_message;
--			else
--			   update vie_message set status = 1 where pk_message = p_pk_message;
--			end if;

--	 END LOOP;
END SP_ADT;


----------------------------------------------------------------------------
PROCEDURE SP_PID(
p_pk_msgseg IN NUMBER,
p_status OUT NUMBER,
p_errtxt OUT VARCHAR2,
p_sending_app IN VARCHAR2
)
AS
v_match_sql VARCHAR2(500);
-- v_match_value varchar2(4000);
-- v_match_column varchar2(10);
-- v_match_column_sql varchar2(200);
v_pk_person NUMBER;
-- v_velos_coltype varchar2(10);
-- v_icount number;
v_errlog VARCHAR2(4000);
v_update_sql VARCHAR2(4000);
v_insert_col VARCHAR2(4000);
v_fk_account NUMBER;
v_fk_site NUMBER;
v_person_regdate DATE;
v_fk_codelst_pstat NUMBER;
v_fk_timezone NUMBER;
v_insertflag CHAR(1);
v_creator NUMBER;
v_per_code VARCHAR2(100);
BEGIN
-- Need to implement filtering based on MSG_CLIENT for sites where segments are mapped differently for different SENDING_APP

-- v_icount := 0;
-- FOR i in (select vie_column, velos_column,velos_coltype,format from vie_mapping where seg_name = 'PID' and match = 1)
-- LOOP
-- v_icount := v_icount + 1 ;
-- if (v_icount > 1) then
-- v_match_sql := v_match_sql || ' AND ';
-- end if;
-- v_match_column := i.vie_column;
-- v_velos_coltype := i.velos_coltype;
-- v_match_column_sql := 'select ' || v_match_column || ' from vie_msgseg where pk_msgseg = ' || p_pk_msgseg;
-- execute immediate v_match_column_sql into v_match_value;
-- if (v_match_value is null or length(v_match_value) = 0) then
-- p_status := -1;
-- p_errtxt := p_errtxt || 'FATAL (PID) - No data found to match the record - ' || v_match_column || ' - ' || i.velos_column || ' ***** ';
-- exit;
-- end if;
-- if (v_velos_coltype = 'DT' ) then
-- v_match_sql := v_match_sql || i.velos_column || ' = ' || 'to_date(''' || v_match_value || ''',''' || i.format || ''')';
-- end if;
-- if (v_velos_coltype = 'NM') then
-- if v_match_value is null or length(v_match_value) = 0 then
-- v_match_value := '0';
-- end if;
-- v_match_sql := v_match_sql || i.velos_column || ' = ' || v_match_value;
-- end if;
-- if (v_velos_coltype = 'ST') then
-- v_match_sql := v_match_sql || i.velos_column || ' = ' || '''' || v_match_value || '''';
-- end if;
-- END LOOP;
-- if p_status = -1 then
-- return;
-- end if;
-- Begin
-- execute immediate 'select pk_person from person@lnk2pat where ' || v_match_sql into v_pk_person;
-- exception when NO_DATA_FOUND then
-- v_pk_person := 0;
-- End;


	v_insertflag := '' ;
		FOR x IN (SELECT codelst_type,velos_value FROM VIE_MAPPING_DETAILS WHERE sending_app = p_sending_app AND codelst_type IN ('_ACCOUNT_','_SITE_','_TIMEZONE_','_CREATOR_'))
		LOOP
			CASE x.codelst_type
			WHEN '_ACCOUNT_' THEN v_fk_account := x.velos_value;
			WHEN '_SITE_' THEN v_fk_site := x.velos_value;
			WHEN '_TIMEZONE_' THEN v_fk_timezone  := x.velos_value;
			WHEN '_CREATOR_' THEN v_creator  := x.velos_value;
			END CASE;
		END LOOP;

   sp_getpatient(p_pk_msgseg, p_status, p_errtxt,v_pk_person,v_match_sql,v_fk_account,v_fk_site);

	IF (v_pk_person = 0) THEN
	   sp_create_sql(p_pk_msgseg,'PID',v_update_sql,'I',v_insert_col,p_errtxt,p_status,p_sending_app);
	   SELECT SEQ_ER_PER.NEXTVAL INTO v_pk_person FROM dual;


-- Get account
--   	     select internal_id into v_fk_account from vie_codelist where pk_codelist = 11;
-- Get site
--   	     select internal_id into v_fk_site from vie_codelist where pk_codelist = 10;
-- Get Patient Status
--   	     select internal_id into v_fk_codelst_pstat from vie_codelist where pk_codelist = 13;
-- Get timezone
--   	     select internal_id into v_fk_timezone from vie_codelist where pk_codelist = 12;
-- Get creator
--   	     select internal_id into v_creator from vie_codelist where pk_codelist = 20;


-- Get the default variables

--		SELECT pk_codelst INTO v_fk_codelst_pstat
--		FROM ER_CODELST a, VIE_MAPPING_DETAILS b
--		WHERE
--		sending_app = p_sending_app AND
--		a.codelst_type = 'patient_status' AND
--		b.codelst_type = 'patient_status' AND
--		a.codelst_subtyp = b.codelst_subtyp;



			v_update_sql := 'INSERT INTO PERSON@LNK2PAT (PK_PERSON,' || v_insert_col || ',FK_ACCOUNT, FK_SITE, PERSON_REGDATE, FK_TIMEZONE, CREATOR, PERSON_REGBY) values (' || v_pk_person || ',' ||v_update_sql || ',' || v_fk_account || ',' || v_fk_site || ',to_date(''' || SYSDATE || '''),' || v_fk_timezone || ',' || v_creator || ',' || v_creator ||')';
--			v_update_sql := 'UPDATE PERSON@LNK2PAT ' || v_update_sql || ' where pk_person = ' || v_pk_person;
			v_insertflag := 'Y';
		 ELSE
		 	sp_create_sql(p_pk_msgseg,'PID',v_update_sql,'U',v_insert_col,p_errtxt,p_status,p_sending_app);
			v_update_sql := 'UPDATE PERSON@LNK2PAT ' || v_update_sql || ',last_modified_by = ' || v_creator || ' WHERE ' || v_match_sql || ' and fk_site = ' || v_fk_site;
		 END IF;


		 BEGIN
		 EXECUTE IMMEDIATE v_update_sql;
		 UPDATE person@lnk2pat SET pat_facilityid = person_code WHERE pk_person = v_pk_person;
		 UPDATE ER_PATFACILITY SET pat_facilityid = (SELECT person_code FROM person@lnk2pat WHERE pk_person = v_pk_person) WHERE fk_per = v_pk_person AND fk_site = v_fk_site;
    	 EXCEPTION WHEN OTHERS THEN
		 	p_status := -1;
			p_errtxt := p_errtxt || ' Person table not updated - SQL -  ' || v_update_sql  || CHR(13) || SQLERRM;

--    	 exception when PROGRAM_ERROR THEN
		 END;

-- 		insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,1,SYSDATE,1,v_insertflag || ' | ' || p_status);

--		 BEGIN
--			 IF v_insertflag = 'Y' AND (p_status <> -1 OR p_status IS NULL) THEN
--			 	INSERT INTO ER_PER (pk_per,per_code,fk_account,fk_site,fk_timezone,CREATOR) (SELECT PK_PERSON,PERSON_CODE,FK_ACCOUNT,FK_SITE,FK_TIMEZONE,CREATOR FROM person@lnk2pat WHERE pk_person = v_pk_person);
--			 END IF;
--    	 EXCEPTION WHEN OTHERS THEN
--		 	p_status := -1;
--			p_errtxt := p_errtxt || ' ER_PER table not updated - SQL -  ' || 'insert into ER_PER (pk_per,per_code,fk_account,fk_site,fk_timezone,CREATOR) (select PK_PERSON,PERSON_CODE,FK_ACCOUNT,FK_SITE,FK_TIMEZONE,CREATOR from person@lnk2pat where pk_person = ' || v_pk_person || ')'   || CHR(13) || SQLERRM;

--			insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,1,SYSDATE,1,v_insertflag || ' | ' || p_status);
--			commit
--    	 exception when PROGRAM_ERROR THEN
--		 END;

		 IF p_status IS NULL THEN
		 	p_status := 1;
			SELECT per_code INTO v_per_code FROM ER_PER WHERE pk_per = v_pk_person;
			IF v_insertflag = 'Y' THEN
--				p_errtxt := p_errtxt || 'New Patient Added: Patient ID - ' || v_per_code || '***' || v_update_sql;

				p_errtxt := p_errtxt || 'New Patient Added: Patient ID - ' || v_per_code;
			ELSE
--				p_errtxt := p_errtxt || 'Patient Update: Patient ID - ' || v_per_code || '***' || v_update_sql;

				p_errtxt := p_errtxt || 'Patient Update: Patient ID - ' || v_per_code;
			END IF;
		 END IF;

--	 update vie_msgseg set status = 1 where pk_msgseg = p_pk_msgseg;
END SP_PID;

--------------------------------------------------------------------------------------------
PROCEDURE SP_LAB(
p_pk_message IN NUMBER,
p_sending_app IN VARCHAR2,
p_account NUMBER,
p_site NUMBER
)
AS
v_pk_msgseg NUMBER;
v_seg_name VARCHAR2(10);
v_status NUMBER;
v_errtxt VARCHAR2(4000);
v_pk_person NUMBER;
v_match_sql VARCHAR2(500);
v_vie_column VARCHAR2(10);
v_hl7group_name VARCHAR2(100);
v_hl7test_name VARCHAR2(500);
v_hl7testid VARCHAR2(50);
v_fk_labgroup VARCHAR2(100);
v_fk_site NUMBER;
v_fk_testid NUMBER;
v_fklabgroup NUMBER;
v_test_type CHAR(2);
v_test_result_nm VARCHAR2(4000);
v_test_result_tx CLOB;
v_test_unit VARCHAR2(50);
v_test_range VARCHAR2(100);
v_test_range_lln VARCHAR2(50);
v_test_range_uln VARCHAR2(50);
v_abnormal_flag VARCHAR2(50);
v_test_status VARCHAR2(100);
v_test_date_hl7 VARCHAR2(20);
-- capture time stamp
v_test_date DATE;
--v_test_date TIMESTAMP;
v_tx BOOLEAN := FALSE;

v_abnormal_flag_id NUMBER;
v_test_status_id NUMBER;
v_test_unit_id NUMBER;
v_labtestsname VARCHAR2(100);
v_fk_labgrp NUMBER;

-- delete these variables
v_pk_patlabs NUMBER;
BEGIN
     plog.debug(pctx,   'p_sending_app=' ||p_sending_app);
	 SELECT internal_id INTO v_fk_site FROM VIE_CODELIST WHERE hl7_value = p_sending_app;
      plog.debug(pctx,   'v_fk_site=' ||v_fk_site);
	 FOR  i IN (SELECT PK_MSGSEG,SEG_NAME FROM VIE_MSGSEG WHERE fk_message = p_pk_message ORDER BY pk_msgseg)
	 LOOP
	 	 v_status := 0;
	 	 v_errtxt := '';
	 	 v_pk_msgseg := i.pk_msgseg;
	 	 v_seg_name := i.seg_name;
         plog.debug(pctx,   'v_seg_name=' ||v_seg_name);
	 	 IF (v_seg_name = 'PID') THEN
          plog.debug(pctx,   'v_pk_msgseg=' ||v_pk_msgseg||'v_status='||v_status||'v_pk_person='||v_pk_person||'v_errtxt='||v_errtxt||'v_match_sql='||v_match_sql||'p_account='||p_account||'p_site='||p_site);
   		 	sp_getpatient(v_pk_msgseg, v_status, v_errtxt,v_pk_person,v_match_sql,p_account,p_site);

			IF v_status < 0 THEN
		   	   INSERT INTO VIE_MSGLOG (pk_msglog,fk_message,log_date,log_status,log_text) VALUES (seq_vie_msglog.NEXTVAL,p_pk_message,SYSDATE,v_status,v_errtxt);
		   	   UPDATE VIE_MESSAGE SET status = v_status WHERE pk_message = p_pk_message;
			   EXIT;
			END IF;
			IF v_pk_person = 0 THEN
		   	   INSERT INTO VIE_MSGLOG (pk_msglog,fk_message,log_date,log_status,log_text) VALUES (seq_vie_msglog.NEXTVAL,p_pk_message,SYSDATE,-2,v_errtxt);

            --insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,p_pk_message,SYSDATE,-2,' *** Patient not found (LABS) - ' || v_match_sql);
		   	   UPDATE VIE_MESSAGE SET status = -2 WHERE pk_message = p_pk_message;
			   EXIT;
			END IF;
	 	 END IF;

	 	 IF (v_seg_name = 'OBR') THEN
         plog.debug(pctx,   'inside OBR condition' );
		 	v_match_sql := '';
			v_fklabgroup := 0;
		 	SELECT VIE_COLUMN INTO v_vie_column FROM VIE_MAPPING WHERE seg_name = 'OBR';
		 	v_match_sql := 'select ' || v_vie_column || ' from vie_msgseg where pk_msgseg = ' || v_pk_msgseg;
	 		EXECUTE IMMEDIATE v_match_sql INTO v_hl7group_name;
             plog.debug(pctx,   'v_hl7group_name=' ||v_hl7group_name);
			v_hl7group_name := SUBSTR(v_hl7group_name,1,INSTR(v_hl7group_name,'^')-1);

			IF v_hl7group_name IS NOT NULL OR LENGTH(v_hl7group_name) > 0 THEN
			   BEGIN
			   		SELECT internal_id INTO v_fklabgroup FROM VIE_CODELIST WHERE hl7_description = v_hl7group_name;
			 		EXCEPTION WHEN NO_DATA_FOUND THEN
			   		v_fklabgroup := 1;
					-- If no group found then assign the test to unknown group
			   END;
			END IF;
		 END IF;

	 	 IF (v_seg_name = 'OBX') THEN
		 	FOR j IN (SELECT VIE_COLUMN,VELOS_COLUMN,COMPONENT,FORMAT FROM VIE_MAPPING WHERE seg_name = 'OBX' AND update_tab = 1 ORDER BY hl7_element)
			LOOP
				v_match_sql := '';
				v_match_sql := 'select ' || j.VIE_COLUMN || ' from vie_msgseg where pk_msgseg = ' || v_pk_msgseg;

                plog.debug(pctx,   'v_match_sql=' ||v_match_sql);

				CASE j.VELOS_COLUMN
				WHEN 'TEST_TYPE' THEN
					 v_test_type := '';
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_type ;
					 v_test_type := v_test_type ;
				WHEN 'LABTEST_NAME' THEN
					 v_hl7test_name := '';
					 EXECUTE IMMEDIATE v_match_sql INTO v_hl7test_name ;
					 v_hl7testid := SUBSTR(v_hl7test_name,1,INSTR(v_hl7test_name,'^')-1);
					 v_hl7test_name := SUBSTR(v_hl7test_name,INSTR(v_hl7test_name,'^')+1,LENGTH(v_hl7test_name)-INSTR(v_hl7test_name,'^',1,2)-1);

                     plog.debug(pctx,   'v_hl7test_name=' ||v_hl7test_name||'labtest_shortname'||v_hl7testid);
				WHEN 'TEST_RESULT' THEN
					 v_test_result_nm := '';
					 v_test_result_tx := '';
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_result_nm ;
					 IF j.COMPONENT = 9 THEN
					 	v_test_result_nm := SUBSTR(v_test_result_nm,INSTR(v_test_result_nm,'^')+1,LENGTH(v_test_result_nm) - INSTR(v_test_result_nm,'^',1,2));
					 END IF;
					 IF v_test_type = 'TX' THEN
					 	v_test_result_tx := v_test_result_tx || v_test_result_nm;
					 END IF;
				WHEN 'TEST_UNIT' THEN
					 v_test_unit := '';
					 v_test_unit_id := 0;
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_unit ;
					 IF v_test_unit IS NOT NULL OR LENGTH(v_test_unit) > 0 THEN
					 	BEGIN
                             --taking msx of pk in case of duplicates
					 		 SELECT max(pk_labunits) INTO v_test_unit_id FROM ER_LABUNITS WHERE labunits_unit = v_test_unit;
							 EXCEPTION WHEN NO_DATA_FOUND THEN
							 v_test_unit_id := 0;
						END;
					 END IF;
				WHEN 'TEST_RANGE' THEN
					 v_test_range := '';
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_range ;
					 IF INSTR(v_test_range,'^') > 0 THEN
					 	v_test_range_lln := SUBSTR(v_test_range,INSTR(v_test_range,'^')+1,LENGTH(v_test_range)-INSTR(v_test_range,'^',1,2)-1);
					 	v_test_range_uln := SUBSTR(v_test_range,INSTR(v_test_range,'^',1,2)+1);
						v_test_range := SUBSTR(v_test_range,1,INSTR(v_test_range,'^')-1);
					 END IF;
				WHEN 'ABNORMAL_FLAG' THEN
					 v_abnormal_flag := '';
					 v_abnormal_flag_id := 0;
					 EXECUTE IMMEDIATE v_match_sql INTO v_abnormal_flag ;
					 IF v_abnormal_flag IS NOT NULL OR LENGTH(v_abnormal_flag) > 0 THEN
					 BEGIN
					 	  SELECT  pk_codelst INTO v_abnormal_flag_id FROM ER_CODELST WHERE codelst_type = 'abflag' AND codelst_subtyp = v_abnormal_flag;
					 	  EXCEPTION WHEN NO_DATA_FOUND THEN
						  v_status := -2;
						  v_errtxt := v_errtxt || ' *** Code list entry for abnormal flag = ' || v_abnormal_flag || ' not found ***';
					 END;
					 END IF;
				WHEN 'TEST_STATUS' THEN
					 v_test_status := '';
					 v_test_status_id := 0;
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_status ;
					 IF v_test_status IS NOT NULL OR LENGTH(v_test_status) > 0 THEN
					 BEGIN
					 	  SELECT  pk_codelst INTO v_test_status_id FROM ER_CODELST WHERE codelst_type = 'tststat' AND codelst_subtyp = v_test_status;
					 	  EXCEPTION WHEN NO_DATA_FOUND THEN
						  v_status := -2;
						  v_errtxt := v_errtxt || ' *** Code list entry for test status = ' || v_test_status || ' not found ***';
					 END;
					 END IF;
				WHEN 'TEST_DATE' THEN
					 v_test_date_hl7 := '';
					 EXECUTE IMMEDIATE v_match_sql INTO v_test_date_hl7 ;
                     plog.debug(pctx,'j.format='||j.format||'v_test_date_hl7='||v_test_date_hl7);
					 SELECT TO_DATE(v_test_date_hl7,j.format) INTO v_test_date FROM dual;
                     plog.debug(pctx,'v_test_date==='||v_test_date);
				END CASE;
			END LOOP;

			BEGIN
		 		 SELECT fk_testid INTO v_fk_testid FROM ER_LABINTERFACE WHERE fk_site = v_fk_site AND hl7_testname = v_hl7test_name;
			 	 EXCEPTION WHEN NO_DATA_FOUND THEN
				 v_fk_testid := 0;
			END;

                --  04/21 revisit this code to check for a flag i.e if values exists dont insert or else insert

--                			IF v_fk_testid = 0  AND LENGTH(v_hl7test_name) > 0  THEN
--                			   SELECT seq_er_labtest.NEXTVAL INTO v_fk_testid FROM dual;
--                			   INSERT INTO ER_LABTEST (pk_labtest,labtest_name) VALUES (v_fk_testid,v_hl7test_name);
--                			   IF v_test_unit_id = 0 AND LENGTH(v_test_unit) > 0  THEN
--                			   	  INSERT INTO ER_LABUNITS (pk_labunits,fk_labtest,labunits_unit) VALUES (seq_er_labunits.NEXTVAL,v_fk_testid,v_test_unit);
--                			   END IF;
--                			   INSERT INTO ER_LABINTERFACE (PK_LABINTERFACE, FK_TESTID, HL7_TESTID,HL7_TESTNAME, FK_SITE) VALUES (seq_er_labinterface.NEXTVAL ,v_fk_testid ,v_hl7testid ,v_hl7test_name ,v_fk_site);
--                			END IF;

                -- code repeated, revisit
               	BEGIN
               		 SELECT fk_testid INTO v_fk_testid FROM ER_LABINTERFACE WHERE fk_site = v_fk_site AND hl7_testname = v_hl7test_name;
               	 	 EXCEPTION WHEN NO_DATA_FOUND THEN
               		 v_fk_testid := 0;
                END;

               -- changed 04/21/08 . Not using v_fklabgroup which is taken from VIE_CODELIST
               if(v_fk_testid>0) then
                 BEGIN
                     select fk_labgroup INTO v_fk_labgrp from er_labtestgrp where fk_testid = v_fk_testid;
                     EXCEPTION WHEN NO_DATA_FOUND THEN
                     INSERT INTO VIE_MSGLOG (pk_msglog,fk_message,log_date,log_status,log_text) VALUES (seq_vie_msglog.NEXTVAL,p_pk_message,SYSDATE,-2,' *** No lab group found for lab test - ' || v_fk_testid);
                     UPDATE VIE_MESSAGE SET status = -2 WHERE pk_message = p_pk_message;
               	     v_fk_labgrp := 0;

                 END;
               end if;

			-- changed 04/21/08 for checking with labtest_short name
            --and rejecting records if the labtest short name is not present in er_labtest.
            --(To avoid insertion of all new labtest names in er_labtest table)

            BEGIN
                select labtest_shortname into v_labtestsname from er_labtest where labtest_shortname = v_hl7testid;
                exception when NO_DATA_FOUND then
                v_labtestsname:='';
            END;
            plog.debug(pctx,'lab test short name ='||v_labtestsname);
          --IF LENGTH(v_hl7test_name) > 0 THEN
            IF LENGTH(v_labtestsname) > 0 and v_fk_labgrp>0 THEN
			BEGIN

               UPDATE VIE_MESSAGE SET status = 1 WHERE pk_message = p_pk_message;

			   SELECT seq_er_patlabs.NEXTVAL INTO v_pk_patlabs FROM dual;
               --plog.debug(pctx,'v_test_date is ***'||v_test_date);
			   v_match_sql := 'insert into er_patlabs (pk_patlabs,fk_per,fk_testid,test_date,test_result,test_type,test_unit,test_range,test_lln,test_uln,fk_site,fk_codelst_tststat,fk_codelst_abflag,fk_testgroup,created_on) values (' || v_pk_patlabs || ',' || v_pk_person || ',' || v_fk_testid || ',''' || v_test_date || ''',''' || v_test_result_nm || ''',''' || v_test_type || ''',''' || v_test_unit || ''',''' || v_test_range || ''',''' || v_test_range_lln || ''',''' || v_test_range_uln || ''',' || v_fk_site || ',''' || v_test_status_id || ''',''' || v_abnormal_flag_id || ''','''||v_fk_labgrp||''',sysdate)';
			   EXECUTE IMMEDIATE v_match_sql;
			   EXCEPTION WHEN OTHERS THEN
		   	   INSERT INTO VIE_MSGLOG (pk_msglog,fk_message,log_date,log_status,log_text) VALUES (seq_vie_msglog.NEXTVAL,p_pk_message,SYSDATE,-2,' *** Patient not found (LABS) - ' || v_match_sql);
		   	   UPDATE VIE_MESSAGE SET status = -2 WHERE pk_message = p_pk_message;
			END;
			END IF;

		 END IF;

--	 	 if (v_seg_name = 'NTE') then
--		 end if;

	 END LOOP;
END SP_LAB;
--------------------------------------------------------------------------------------------
PROCEDURE SP_CREATE_SQL(
p_pk_msgseg IN NUMBER,
p_seg_name IN VARCHAR2,
p_update_sql OUT VARCHAR2,
p_update_flag CHAR,
p_insert_col OUT VARCHAR2,
p_errtxt OUT VARCHAR2,
p_status OUT NUMBER,
p_sending_app IN VARCHAR2
)
AS
v_icount NUMBER := 0;
v_pk_mapping NUMBER;
v_match_column VARCHAR2(10);
v_match_column_sql VARCHAR2(200);
v_match_value VARCHAR2(4000);
v_velos_coltype VARCHAR2(10);
v_caret NUMBER;
v_caret1 NUMBER;
v_comp_val VARCHAR2(100);
v_comp_val1 VARCHAR2(100);
v_velos_value VARCHAR2(100);
v_vie_value VARCHAR2(100);
v_codelist VARCHAR2(50);
v_codelist_sql VARCHAR2(200);
v_insert_col VARCHAR2(4000);
v_insert_value VARCHAR2(4000);
v_tempstr VARCHAR2(4000);
BEGIN


-- Create a dynamic SQL based on which elements needs to be updated
	 FOR i IN (SELECT pk_mapping, vie_column, velos_column, velos_coltype, component,format,required,mapping_defined FROM VIE_MAPPING WHERE seg_name = p_seg_name AND UPDATE_TAB = 1 ORDER BY hl7_element)
          LOOP
                              --  insert into t (t1,t2) values ('BEGINLOOP v_insert_col',v_insert_col);
                               -- commit;
	 	 v_icount := v_icount + 1 ;
	 	 IF (v_icount = 1) THEN
		 	   p_update_sql := p_update_sql || ' SET ';
		 		v_insert_col := v_insert_col || ' ';
				v_insert_value := v_insert_value || ' ';
		 ELSE
                   if substr(trim(v_insert_col),length(v_insert_col)-1) <> ',' then
                        p_update_sql := p_update_sql || ' , ';
                        v_insert_col := v_insert_col || ' , ';
                        v_insert_value := v_insert_value || ' , ';
                  end if;
		 END IF;

	 	 v_match_column := i.vie_column;
		 v_velos_coltype := i.velos_coltype;

-- Get the value of the CUSTOM? column from VIE_MSGSEG
	 	v_match_column_sql := 'select ' || v_match_column || ' from vie_msgseg where pk_msgseg = ' || p_pk_msgseg;
	 	EXECUTE IMMEDIATE v_match_column_sql INTO v_match_value;

--	 insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,1,SYSDATE,1,v_CODELIST_sql);


		 IF (i.component = 1) THEN
          plog.debug(pctx,'inside if statement for component 1');
		 	v_velos_value := i.velos_column;
			v_vie_value := v_match_value;

			LOOP
			 	v_caret := INSTR(v_velos_value,'^',1);


				v_caret1 := INSTR(v_vie_value,'^',1);


                                IF (v_caret = 0) THEN
				   IF LENGTH(v_velos_value) > 0 THEN
                        --	if (v_caret1 = 0 ) then
                        --	v_comp_val := SUBSTR(v_velos_value,1,v_caret - 1);
                            -- else
					  	  v_comp_val := v_velos_value;
                        --  end if;
					  IF v_caret1 > 0 THEN
					  	 v_vie_value := REPLACE(v_vie_value,'^',' ');
					  END IF;
					  v_comp_val1 := v_vie_value;

					  IF i.mapping_defined = 1 THEN
					  BEGIN
						SELECT velos_value
						INTO v_tempstr
						FROM VIE_MAPPING_DETAILS
						WHERE sending_app = p_sending_app AND
						velos_column = v_comp_val AND
						hl7_value = v_comp_val1;
						EXCEPTION WHEN NO_DATA_FOUND THEN
						v_tempstr := NULL;
					END;
						IF v_tempstr IS NOT NULL THEN
						    v_comp_val1:= v_tempstr;
						END IF;
					  END IF;
                                          if length(trim(v_comp_val)) > 0 or v_comp_val is not null then
                                            p_update_sql := p_update_sql || v_comp_val || ' = ' || '''' || REPLACE(v_comp_val1,'''','`') || '''';
                                            v_insert_col := v_insert_col || v_comp_val ;
                                            v_insert_value := v_insert_value || '''' ||REPLACE(v_comp_val1,'''','`') || '''' ;
                                          end if;
				   END IF;
				   EXIT;
				END IF;
				IF (v_caret1 = 0) THEN
				   IF LENGTH(v_velos_value) > 0 THEN
				  	  v_comp_val := SUBSTR(v_velos_value,1,v_caret - 1);
					  v_comp_val1 := v_vie_value;

					  IF i.mapping_defined = 1 THEN
					  BEGIN
						SELECT velos_value
						INTO v_tempstr
						FROM VIE_MAPPING_DETAILS
						WHERE sending_app = p_sending_app AND
						velos_column = v_comp_val AND
						hl7_value = v_comp_val1;
						EXCEPTION WHEN NO_DATA_FOUND THEN
						v_tempstr := NULL;
					END;
						IF v_tempstr IS NOT NULL THEN
						    v_comp_val1:= v_tempstr;
						END IF;
					  END IF;
                                          if length(trim(v_comp_val)) > 0 or v_comp_val is not null then
                                            p_update_sql := p_update_sql || v_comp_val || ' = ' || '''' || REPLACE(v_comp_val1,'''','`') || '''';
                                            v_insert_col := v_insert_col || v_comp_val ;
                                            v_insert_value := v_insert_value || '''' || REPLACE(v_comp_val1,'''','`') || '''' ;
                                          end if;
				   END IF;
				   EXIT;
				END IF;
				v_comp_val := SUBSTR(v_velos_value,1,v_caret - 1);
				v_comp_val1 := SUBSTR(v_vie_value,1,v_caret1 - 1);
				v_velos_value := SUBSTR(v_velos_value,v_caret + 1);
                                v_vie_value := SUBSTR(v_vie_value,v_caret1 + 1);


                             --   insert into t (t1,t2) values ('v_comp_val',nvl(v_comp_val,'********'));
                              --  commit;

                                if length(trim(v_comp_val)) > 0 or v_comp_val is not null then
                                  v_match_value := v_match_value ;
                                  p_update_sql := p_update_sql || v_comp_val || ' = ' || '''' || REPLACE(v_comp_val1,'''','`') || ''', ';

                                  v_insert_col := v_insert_col || v_comp_val || ', ';

                                  v_insert_value := v_insert_value || '''' || REPLACE(v_comp_val1,'''','`') || ''', ';
                                end if;


			END LOOP;

		 ELSIF (i.component = 9) THEN
                plog.debug(pctx,'component value='||i.component||' , v_match_value is '||v_match_value);
                -- Check if the data needs conversion based on codelist
   		 	BEGIN

			IF v_match_value IS NULL THEN
                     plog.debug(pctx,'p_sending_app='||p_sending_app||' , i.velos_column is '||i.velos_column);
						SELECT pk_codelst INTO v_codelist
						FROM ER_CODELST a, VIE_MAPPING_DETAILS b
						WHERE
						sending_app = p_sending_app AND
						velos_column = i.velos_column AND
						hl7_value IS NULL AND
						a.codelst_type = b.codelst_type AND
						a.codelst_subtyp = b.codelst_subtyp;
			ELSE
						SELECT pk_codelst INTO v_codelist
						FROM ER_CODELST a, VIE_MAPPING_DETAILS b
						WHERE
						sending_app = p_sending_app AND
						velos_column = i.velos_column AND
						hl7_value = v_match_value AND
						a.codelst_type = b.codelst_type AND
						a.codelst_subtyp = b.codelst_subtyp;
			END IF;

--   			 v_codelist_sql := 'select internal_id from vie_codelist where fk_mapping = ' || i.pk_mapping || ' and hl7_value = ''' || v_match_value || '''';
--	 		 execute immediate v_codelist_sql into v_codelist;
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			 v_codelist := '''''';
			 IF i.required = 1 THEN
				 p_errtxt := p_errtxt || ' *** Code list entry not found : '  || i.velos_column || ' = ' || v_match_value ;
				 p_status := -2;
			 END IF;
			END;
--		 	p_update_sql := p_update_sql || i.velos_column || ' = TO_NUMBER(''' ||v_codelist || ''')';
		 	p_update_sql := p_update_sql || i.velos_column || ' = ' ||v_codelist;
			v_insert_col := v_insert_col || i.velos_column ;
--			v_insert_value := v_insert_value || 'TO_NUMBER(''' ||v_codelist || ''')' ;
			v_insert_value := v_insert_value || v_codelist  ;

		 ELSE

					  IF i.mapping_defined = 1 THEN
					  BEGIN
						SELECT velos_value
						INTO v_tempstr
						FROM VIE_MAPPING_DETAILS
						WHERE sending_app = p_sending_app AND
						velos_column = i.velos_column AND
						hl7_value = v_match_value;
						EXCEPTION WHEN NO_DATA_FOUND THEN
						v_tempstr := NULL;
					END;
						IF v_tempstr IS NOT NULL THEN
						    v_match_value:= v_tempstr;
						END IF;
					  END IF;


		 	IF (v_velos_coltype = 'DT' ) THEN
			   IF v_match_value IS NULL THEN
		 	   v_match_value := 'null';
			   ELSE
		 	   v_match_value := 'to_date(''' || v_match_value || ''',''' || i.format || ''')';
			   END IF;
		 	END IF;

			IF (v_velos_coltype = 'NM') THEN
--		 	   v_match_value := 'to_number(''' || v_match_value || ''')';
			   IF v_match_value IS NULL OR LENGTH(v_match_value) = 0 THEN
			   	  v_match_value := '0';
			   ELSE
		 	   	  v_match_value := v_match_value ;
			   END IF;
			END IF;

			IF (v_velos_coltype = 'ST') THEN
	 	 	   v_match_value :=  '''' || REPLACE(v_match_value,'''','`') || '''';
		 	END IF;

		 	p_update_sql := p_update_sql || i.velos_column || ' = ' ||v_match_value ;
			v_insert_col := v_insert_col || i.velos_column ;
			v_insert_value := v_insert_value || v_match_value ;

		 END IF;


-- Check if conversion of data is required
		 v_pk_mapping := i.pk_mapping;

--		 p_update_sql := velos_column || ' = ' || ;
 	 END LOOP;

	 IF (p_update_flag = 'I') THEN
	 	p_update_sql := v_insert_value;
		p_insert_col := v_insert_col;
	 END IF;


END SP_CREATE_SQL;


PROCEDURE SP_GETPATIENT(
p_pk_msgseg IN NUMBER,
p_status OUT NUMBER,
p_errtxt OUT VARCHAR2,
p_pk_person OUT NUMBER,
p_match_sql OUT VARCHAR2,
p_account NUMBER,
p_site NUMBER
)
AS
v_match_sql VARCHAR2(200);
v_match_value VARCHAR2(4000);
v_match_column VARCHAR2(10);
v_match_column_sql VARCHAR2(200);
--v_pk_person number;
v_velos_coltype VARCHAR2(10);
v_icount NUMBER;
v_temp varchar2(4000);
v_pos NUMBER;
--v_errlog varchar2(4000);
--v_update_sql varchar2(4000);
--v_insert_col varchar2(4000);
--v_fk_account number;
--v_fk_site number;
--v_person_regdate date;
--v_fk_codelst_pstat number;
--v_fk_timezone number;
--v_insertflag char(1);
--v_creator number;
BEGIN
-- Need to implement filtering based on MSG_CLIENT for sites where segments are mapped differently for different SENDING_APP
	 v_icount := 0;
-- Create a dynamic SQL based on the MATCH flag in the VIE_MAPPING table
	 FOR i IN (SELECT component,vie_column, velos_column,velos_coltype,format FROM VIE_MAPPING WHERE seg_name = 'PID' AND match = 1)
	 LOOP
	 	 v_icount := v_icount + 1 ;
	 	 IF (v_icount > 1) THEN
		 	p_match_sql := p_match_sql || ' AND ';
		 END IF;

	 	 v_match_column := i.vie_column;
		 v_velos_coltype := i.velos_coltype;

		 v_match_column_sql := 'select ' || v_match_column || ' from vie_msgseg where pk_msgseg = ' || p_pk_msgseg;
		 plog.debug(pctx,'v_match_column_sql='||v_match_column_sql);
         EXECUTE IMMEDIATE v_match_column_sql INTO v_match_value;

		 IF (v_match_value IS NULL OR LENGTH(v_match_value) = 0) THEN
		 	p_status := -1;
			p_errtxt := p_errtxt || 'FATAL (PID) - No data found to match the record - ' || v_match_column || ' - ' || i.velos_column || ' ***** ' ;
		 	EXIT;
		 END IF;

		 IF (v_velos_coltype = 'DT' ) THEN
		 	p_match_sql := p_match_sql || i.velos_column || ' = ' || 'to_date(''' || v_match_value || ''',''' || i.format || ''')';
		 END IF;
		IF (v_velos_coltype = 'NM') THEN
--		 	v_match_sql := v_match_sql || i.velos_column || ' = ' || 'to_number(''' || v_match_value || ''')';
			IF v_match_value IS NULL OR LENGTH(v_match_value) = 0 THEN
			   v_match_value := '0';
			END IF;
		 	p_match_sql := p_match_sql || i.velos_column || ' = ' || v_match_value;
		END IF;
        --plog.debug(pctx,'v_velos_coltype='||v_velos_coltype);
		IF (v_velos_coltype = 'ST') THEN
                  IF (i.component=1) THEN
                  --plog.debug(pctx,'component is 1');

                        i.velos_column := trim(trailing '^' from i.velos_column);
                        v_pos:=instr(v_match_value, '^');
                        -- 04/21/08 added the if condition for the case when there are no carrots along with per code
                        if (v_pos>0) then
                            v_match_value := substr(v_match_value,1,v_pos-1);
                        end if;
                       -- v_match_value := substr(v_match_value,1,v_pos-1);
                       --plog.debug(pctx,'v_match_value='||v_match_value);
	 	 	p_match_sql := p_match_sql || i.velos_column || ' = ' || '''' || v_match_value || '''';
                  ELSE
                        p_match_sql := p_match_sql || i.velos_column || ' = ' || '''' || v_match_value || '''';
                  END IF;
                  plog.debug(pctx,'p_match_sql='||p_match_sql);
                 END IF;
	 END LOOP;

	 IF p_status = -1 THEN
	 	RETURN;
	END IF;

-- Execute the SQL, if row found then UPDATE else INSERT
   		 BEGIN
--insert into vie_msglog (pk_msglog,fk_message,log_date,log_status,log_text) values (seq_vie_msglog.nextval,1,SYSDATE,1,'select pk_person from person@lnk2pat where ' || v_match_sql);
--commit;
            plog.debug(pctx,   'p_account=' ||p_account||',p_site='||p_site);
               v_temp := 'select pk_person from person@lnk2pat where ' || p_match_sql || ' and fk_account=' || p_account || ' and fk_site = ' || p_site;


                          EXECUTE IMMEDIATE 'select pk_person from person@lnk2pat where ' || p_match_sql || ' and fk_account=' || p_account || ' and fk_site = ' || p_site   INTO p_pk_person;
		 	  EXCEPTION WHEN NO_DATA_FOUND THEN
			  p_errtxt := p_errtxt || 'Patient not found. *** ' || 'select pk_person from person@lnk2pat where ' || p_match_sql || ' and fk_account=' || p_account;

		 	  p_pk_person := 0;
		 END;

END SP_GETPATIENT;


END Pkg_Vie;
/


CREATE SYNONYM ESCH.PKG_VIE FOR PKG_VIE;


CREATE SYNONYM EPAT.PKG_VIE FOR PKG_VIE;


GRANT EXECUTE, DEBUG ON PKG_VIE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_VIE TO ESCH;

