CREATE OR REPLACE PACKAGE        "DBMS_XMLSAVE" AUTHID CURRENT_USER AS

  SUBTYPE ctxType IS NUMBER;				     /* context type */

  DEFAULT_ROWTAG      CONSTANT VARCHAR2(3) := 'ROW';		   /* rowtag */
  DEFAULT_DATE_FORMAT CONSTANT VARCHAR2(21):= 'YYYY-MM-DD HH24:MI:SS';

  MATCH_CASE          CONSTANT NUMBER      := 0;	       /* match case */
  IGNORE_CASE         CONSTANT NUMBER      := 1;             /* ignore case */


  -------------------- constructor/destructor functions ---------------------
  FUNCTION newContext(targetTable IN VARCHAR2) RETURN ctxType;
  PROCEDURE closeContext(ctx IN ctxType);

  -------------------- parameters to the save (XMLtoDB) engine ----------------
  PROCEDURE setRowTag(ctx IN ctxType, tag IN VARCHAR2);
  PROCEDURE setIgnoreCase(ctx IN ctxType, flag IN NUMBER);

  PROCEDURE setDateFormat(ctx IN ctxType, mask IN VARCHAR2);

  PROCEDURE setBatchSize(ctx IN ctxType, batchSize IN NUMBER);
  PROCEDURE setCommitBatch(ctx IN ctxType, batchSize IN NUMBER);

  -- set the columns to update. Relevant for insert and update routines..
  PROCEDURE setUpdateColumn(ctx IN ctxType, colName IN VARCHAR2);
  PROCEDURE clearUpdateColumnList(ctx IN ctxType);

  -- set the key column name to be used for updates and deletes.
  PROCEDURE setKeyColumn(ctx IN ctxType, colName IN VARCHAR2);
  PROCEDURE clearKeyColumnList(ctx IN ctxType);

  ------------------- save ----------------------------------------------------
  -- insertXML
  FUNCTION  insertXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER;
  FUNCTION  insertXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER;
  -- updateXML
  FUNCTION  updateXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER;
  FUNCTION  updateXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER;
  -- deleteXML
  FUNCTION  deleteXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER;
  FUNCTION  deleteXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER;

  ------------------- misc ----------------------------------------------------
  PROCEDURE propagateOriginalException(ctx IN ctxType, flag IN BOOLEAN);
  PROCEDURE getExceptionContent(ctx IN ctxType, errNo OUT NUMBER, errMsg OUT VARCHAR2);

END;
/


GRANT EXECUTE, DEBUG ON DBMS_XMLSAVE TO EPAT;

GRANT EXECUTE, DEBUG ON DBMS_XMLSAVE TO ESCH;

