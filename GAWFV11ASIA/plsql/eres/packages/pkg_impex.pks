CREATE OR REPLACE PACKAGE        "PKG_IMPEX" 
AS
   PROCEDURE SP_EXP_STUDYSUM (
      P_EXPID          NUMBER,
      O_RESULT     OUT  Gk_Cv_Types.GenericCursorType,
          O_ARMRESULT     OUT  Gk_Cv_Types.GenericCursorType ,
        O_SETTINGS    OUT  Gk_Cv_Types.GenericCursorType
    );

   PROCEDURE SP_EXP_STUDYVER (
      P_EXPID          NUMBER,
      O_RESVER     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_APNDX     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_SEC     OUT  Gk_Cv_Types.GenericCursorType
    );


   PROCEDURE SP_EXP_PATSTUDYFORMS (
      P_EXPID          NUMBER,
      O_RESFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_MAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_LINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
   O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
   O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
   O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
   O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
   O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
   O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
   O_RES_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
   O_RES_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
     O_RES_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType
    );

    PROCEDURE SP_EXP_STUDYPROT (
      P_EXPID          NUMBER,
      O_RES_CAL     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_PROTSTAT     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_EVENTCRF     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFMAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFLINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
   O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
   O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
   O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
   O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
   O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
   O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
      O_CRF_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
   O_CRF_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
     O_CRF_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType,
   O_PROTOCOL_VISITS OUT Gk_Cv_Types.GenericCursorType
    );


    PROCEDURE SP_EXP_TEST (
      P_EXPID          NUMBER,
      O_RESULT     OUT  Gk_Cv_Types.GenericCursorType
    );

 PROCEDURE SP_GET_REQDETAILS(
      P_EXPID          NUMBER,
      O_DETAILS     OUT  CLOB
    );

 PROCEDURE SP_IMP_STUDY (P_IMPID NUMBER, O_SUCCESS OUT NUMBER);

   PROCEDURE SP_IMP_STUDYVER (P_IMPID NUMBER, O_SUCCESS OUT NUMBER);


  PROCEDURE SP_GET_IMPREQDETAILS(
      P_IMPID          NUMBER,
      O_DETAILS     OUT  CLOB,
   O_EXPID OUT NUMBER
   );

 FUNCTION getCodePk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2) RETURN NUMBER ;

 PROCEDURE SP_IMP_FORMS (P_IMPID NUMBER, O_SUCCESS OUT NUMBER);

 PROCEDURE SP_IMP_STUDYCAL (P_IMPID NUMBER, O_SUCCESS OUT NUMBER);

 PROCEDURE SP_ALTER_TRIGGER(P_IMPID NUMBER, P_ACTION VARCHAR2, O_SUCCESS OUT NUMBER);

 PROCEDURE SP_GETELEMENT_VALUES_REMOTE(P_IMPID NUMBER,P_PARAM VARCHAR2,O_EXPID OUT NUMBER, O_VALUES OUT VARCHAR2 );

 FUNCTION getSchCodePk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2) RETURN NUMBER;

  END Pkg_Impex;
/


CREATE SYNONYM ESCH.PKG_IMPEX FOR PKG_IMPEX;


CREATE SYNONYM EPAT.PKG_IMPEX FOR PKG_IMPEX;


GRANT EXECUTE, DEBUG ON PKG_IMPEX TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEX TO ESCH;

