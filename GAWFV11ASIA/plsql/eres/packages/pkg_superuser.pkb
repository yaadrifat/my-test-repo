CREATE OR REPLACE PACKAGE BODY        "PKG_SUPERUSER" 
IS

function F_Is_Superuser(p_user  NUmber, p_study Number) return number
  IS
  v_group Number;
  v_rights Varchar2(100) := '';
  v_flag Number;

   v_acc_study Number :=0;
   v_settings_count Number :=0;
   v_usr_account Number ;

   v_div_settings varchar2(4000);
   v_tarea_settings varchar2(4000);
   v_dis_settings varchar2(4000);
   v_study_disease_site varchar2(4000);
   v_div_pos number;
   v_tarea_pos number;
   v_dis_pos number;
   v_study_account number;
  Begin
      -- get user group and see if its super user    
      
      select fk_grp_default , nvl(grp_supusr_rights,''),nvl(grp_supusr_flag,0),er_user.fk_account
      into v_group,v_rights,v_flag,v_usr_account
      from ER_USER,ER_GRPS
      where pk_user = p_user and pk_grp = fk_grp_default ;
    
     select fk_account into v_study_account from er_study where pk_study = p_study;
     if v_study_account != v_usr_account then
		return 0;
     End if;

      if v_flag = 1 then

      begin
         select (select settings_value from  ER_SETTINGS
         WHERE settings_modname=4 AND settings_modnum=v_group and
         settings_keyword = 'STUDY_TAREA'),
         (select settings_value from  ER_SETTINGS
         WHERE settings_modname=4 AND settings_modnum=v_group and
         settings_keyword = 'STUDY_DIVISION') ,
         (select settings_value from  ER_SETTINGS
         WHERE settings_modname=4 AND settings_modnum=v_group and
         settings_keyword = 'STUDY_DISSITE')
         into v_tarea_settings,v_div_settings,v_dis_settings
         from dual; 
         
	 if v_tarea_settings is null and v_div_settings  is null and v_dis_settings is null then
	
		return 1;

	 end if;

         if v_tarea_settings is not null then
            v_tarea_settings := ','|| v_tarea_settings ||',';
         end if;
         
         if v_div_settings is not null then
         
            v_div_settings := ','|| v_div_settings ||',';
         end if;
         
       exception when no_data_found then
           return 1;
       end ;  

       
    
    begin
      
          select pk_study,instr(v_div_settings, ','||to_char(study_division)||','),
          instr(v_tarea_settings, ','||to_char(fk_codelst_tarea)||','),
           study_disease_site
          into v_acc_study,v_div_pos,v_tarea_pos,v_study_disease_site
                  from ER_STUDY
          where pk_study = p_study ; 
                  
         if v_div_pos > 0 or v_tarea_pos > 0 then
          
              return 1;
              
          else
                if v_dis_settings is not null and v_study_disease_site is not null then
         
                
                    select pkg_util.f_compare (     v_dis_settings,      v_study_disease_site ,',',',','','')
                    into v_dis_pos from dual;
                    
                    if v_dis_pos > 0 then 
                        return 1;
                        else
                        return 0;
                    end if;
                 
                else
                    return 0;
               
                end if;
              
              
              
          end if;
      
          
      
    exception when NO_DATA_FOUND then

              plog.fatal(pctx,'psuper' || sqlerrm);
              return 0;
    end;


    else
        return 0;
    end if;



  End; -- for function

 
     function F_get_Superuser_right(p_user Number) return varchar2
    is
    v_right varchar2(100);
  begin
	  	select nvl(grp_supusr_rights,'')
	  	into v_right
	  	from ER_USER,ER_GRPS
	  	where pk_user = p_user and pk_grp = fk_grp_default ;

		return v_right;
  end; -- for function
  
  function F_Is_BgtSuperuser(p_user  Number) return number
  IS
  v_group Number;
  v_rights Varchar2(100) := '';
  v_flag Number;
  v_usr_account Number ;

  Begin
  	-- get user group and see if its super user

  	select fk_grp_default , nvl(GRP_SUPBUD_RIGHTS,''),nvl(GRP_SUPBUD_FLAG,0),er_user.fk_account
  	into v_group,v_rights,v_flag,v_usr_account
  	from ER_USER,ER_GRPS
  	where pk_user = p_user and pk_grp = fk_grp_default ;

  	
	return v_flag;

	exception when NO_DATA_FOUND then

	  plog.fatal(pctx,'psuper' || sqlerrm);
	  return 0;
	
  End; -- for function


END Pkg_Superuser;
/


CREATE SYNONYM ESCH.PKG_SUPERUSER FOR PKG_SUPERUSER;


GRANT EXECUTE, DEBUG ON PKG_SUPERUSER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_SUPERUSER TO ESCH;

