CREATE OR REPLACE PACKAGE      PKG_DATEFORMAT AS
/******************************************************************************
   NAME:       PKG_DATEUTIL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/30/2009             1. Created this package.
******************************************************************************/

  DATE_FORMAT VARCHAR2(200) := 'mm/dd/yyyy';

  --default value for empty date. usually we start the null dates from 1/1/1900. set this value according to the date format field
  DEFAULT_FOR_NULL_DATE VARCHAR2(200) := '01/01/1900';

  TIME_FORMAT varchar2(20) := 'HH24:mi:ss';

  --default value for empty future date. usually this date is set for 01/01/3000. set this value according to the date format field
  DEFAULT_FOR_FUTURE_NULL_DATE VARCHAR2(200) := '01/01/3000';


END PKG_DATEFORMAT;
/


CREATE SYNONYM EPAT.PKG_DATEFORMAT FOR PKG_DATEFORMAT;


CREATE SYNONYM ESCH.PKG_DATEFORMAT FOR PKG_DATEFORMAT;


GRANT EXECUTE, DEBUG ON PKG_DATEFORMAT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_DATEFORMAT TO ESCH;

