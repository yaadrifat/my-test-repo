CREATE OR REPLACE PACKAGE BODY        "PKG_MILNOT" 
AS
/*
Date : 3 Jul 2002
Author: Sonia Sahni
Purpose
*/
 PROCEDURE SP_CREATE_MILNOT
   AS
 /* Date : 3 Jul 2002
    Author: Sonia Sahni
    Purpose - This will be a scheduled procedure that will create milestone achievement
    notifications on regular intervals
 */
  v_studyid NUMBER;
  v_patprot NUMBER;
  v_patient NUMBER;
  v_logcount NUMBER := 0;
  v_protocol NUMBER := 0;
  v_vm_fk_ms NUMBER := 0;
  v_vm_code CHAR(15);
  v_vm_visit NUMBER :=0;
  v_vm_users VARCHAR2(500);
  v_vm_calendar NUMBER :=0;
  v_vm_desc VARCHAR2(200);

  v_em_fk_ms NUMBER := 0;
  v_em_code CHAR(15);
  v_em_visit NUMBER :=0;
  v_em_users VARCHAR2(500);
  v_em_calendar NUMBER :=0;
  v_em_event NUMBER :=0;
  v_em_desc VARCHAR2(200);

--  v_studynumber VARCHAR2(20);
    v_studynumber VARCHAR2(100);--JM:

  V_VM_PKMS            TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_RULECODE        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_RULEVISIT       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_RULEUSERS       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_EVENT           TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_CAL             TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_RULEDESC        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_VM_RULEAMT        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();

  V_EM_PKMS            TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULECODE        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULEVISIT       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULEUSERS       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULEEVENT       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_CAL             TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULEDESC        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
  V_EM_RULEAMT        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();

  V_LASTDT VARCHAR2(200);
  V_STDATE DATE := SYSDATE;
  V_ENDDATE DATE := SYSDATE;
  V_MILESTONEFLAG NUMBER := 0;
  V_ACHIEVEMENT_DATE DATE ;

  V_VM_MSGTEMP VARCHAR2(4000);
  V_EM_MSGTEMP VARCHAR2(4000);
  V_MSG VARCHAR2(4000);
  v_fparam VARCHAR2(500);
  v_patcode VARCHAR2(20);
  v_active_code NUMBER :=0;

  v_protocol_calname VARCHAR2(50);
  v_eventname VARCHAR2(50);

  v_vm_msdesc VARCHAR2(70);
  v_em_msdesc VARCHAR2(120);


 BEGIN

-- p('in SP_CREATE_MILNOT');

   /*get last execution date of notification procedure */

/*   select CTRL_VALUE
   into V_LASTDT
   from er_ctrltab
   Where CTRL_KEY = 'VMEM_NOT';


   if V_LASTDT is null then
    V_STDATE := to_date('01-JAN-1900','dd-mon-yyyy');
    else
    V_STDATE := to_date(V_LASTDT ,'dd-mon-yyyy') ;
   end if; */

     V_STDATE := TO_DATE('01-JAN-1900','dd-mon-yyyy');

    --p('V_LASTDT ' || V_LASTDT || 'V_STDATE' || V_STDATE);

   /*get mail templates*/

--    V_VM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('VM');
  --  V_EM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('EM');

      V_VM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('c_vm');
      V_EM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('c_em');


   /* get studies for which milestones are defined */
    FOR i IN ( SELECT DISTINCT fk_study FROM ER_MILESTONE WHERE MILESTONE_DELFLAG <> 'Y' AND
                      trim(MILESTONE_TYPE) IN ('VM','EM') AND
			MILESTONE_USERSTO IS NOT NULL )

     LOOP -- loop 1
       v_studyid :=    i.fk_study;

	  /*get study activation date*/
       /*   select to_date(to_char (min(STUDYSTAT_DATE), 'dd-mon-yyyy'),'dd-mon-yyyy')
           into V_STDATE
           from ER_STUDYSTAT
          where FK_STUDY = v_studyid
            and FK_CODELST_STUDYSTAT = (select PK_CODELST
                                          from ER_CODELST
                                         where CODELST_TYPE = 'studystat'
                                           and CODELST_SUBTYP = 'active'); */


     /* STUDY_ACTUALDT will be used as study start date */
	    SELECT STUDY_ACTUALDT, STUDY_NUMBER
         INTO V_STDATE, v_studynumber
         FROM ER_STUDY
 	    WHERE PK_STUDY = v_studyid;

       /*get study number*/

       /*get all Visit Milestones for this study*/
	  SP_STUDY_MILESTONE_VMEM (v_studyid,'VM',V_VM_PKMS,V_VM_RULECODE ,V_VM_RULEVISIT ,V_VM_EVENT ,V_VM_RULEUSERS,V_VM_CAL,V_VM_RULEDESC, V_VM_RULEAMT );
       /*get all Event Milestones for this study*/
	  SP_STUDY_MILESTONE_VMEM (v_studyid,'EM',V_EM_PKMS,V_EM_RULECODE , V_EM_RULEVISIT ,V_EM_RULEEVENT ,V_EM_RULEUSERS,V_EM_CAL,V_EM_RULEDESC, V_EM_RULEAMT);

      /*p('STUDY:' || v_studyid || 'V_MCOUNT' || V_VM_PKMS.count);
      p('STUDY:' || v_studyid || 'E_MCOUNT' || V_EM_PKMS.count);*/

       /* for EM, VM get all enrollments for the study*/
       FOR j IN ( SELECT PK_PATPROT,FK_PROTOCOL,PATPROT_ENROLDT,FK_PER,PER_CODE
	             FROM ER_PATPROT,ER_PER WHERE
                fk_study = v_studyid AND
			 pk_per = fk_per )
	  LOOP --loop2
                v_patprot :=  j.PK_PATPROT;
			 v_patient := j.FK_PER;
			 v_protocol := j.FK_PROTOCOL;
			 v_patcode := j.PER_CODE;
            FOR s IN 1..V_VM_PKMS.COUNT --for all visit milestone in study, check for this enrollment
                LOOP --loop 3, all visit milestones
		     	  v_vm_fk_ms := TO_NUMBER(trim(V_VM_PKMS(s)));
  				  v_vm_code  := trim(V_VM_RULECODE(s));
                      v_vm_visit := TO_NUMBER(trim(V_VM_RULEVISIT(s)));
				  v_vm_users := trim(V_VM_RULEUSERS(s));
           		  v_vm_calendar := TO_NUMBER(trim(V_VM_CAL(s)));
				  v_logcount := 0;
                      v_vm_desc := trim(v_vm_ruledesc(s))  ;
       			  V_MILESTONEFLAG := 0;
				  V_ACHIEVEMENT_DATE := NULL;
     		    	 BEGIN
                    	   SELECT pkg_tz.f_getmaxtz(v_vm_users)
               	        INTO V_ENDDATE
     	                  FROM dual;

                   	  EXCEPTION WHEN NO_DATA_FOUND THEN
                           V_ENDDATE := SYSDATE ;
                      END ;

			    --p('------------------------');
		         --p('**ms ' || v_vm_fk_ms || '***type --' || v_vm_code || '***v_vm_users --' || v_vm_users);
			    --p('**v_logcount ' || v_logcount || '***v_vm_calendar --' || v_vm_calendar || '***v_vm_visit --' || v_vm_visit);
			    --p('------------------------');

				IF (v_protocol = v_vm_calendar) THEN -- if milestone calendar equals patprot calendar
   				  SELECT COUNT(*) --check whether there is a log entry for this milestone for this enrollment
     			  INTO v_logcount
				  FROM ER_MILENOTLOG
				  WHERE  FK_PAT =  v_patient AND
       		       FK_MILESTONE = v_vm_fk_ms AND
				  FK_PATPROT = v_patprot ;

                  --p( v_patprot || '*' || v_protocol || '*' || v_vm_visit || '*' ||  V_STDATE || '*' ||  V_ENDDATE);
    		                  --p('**v_logcount ' || v_logcount || '***v_vm_calendar --' || v_vm_calendar || '***v_vm_visit --' || v_vm_visit);
                            --p('v_vm_desc ' || v_vm_desc);

		    		  IF v_logcount = 0 THEN  --check whether MS is achieved

				      --get the name of the protocol calendar

           		  	 SELECT NAME
					 INTO v_protocol_calname
					 FROM EVENT_ASSOC WHERE  event_id = v_protocol AND UPPER(EVENT_TYPE) IN('p','P') ;

					 v_vm_msdesc := 'Visit ' || v_vm_visit || ' ' || v_protocol_calname;


          	           IF (trim(v_vm_code) = 'vm_4') THEN -- VM_4 All visit associated CRFs marked 'completed'
                          Pkg_Vm_Rule.SP_VIS_RULE_CRFS (v_patprot,   v_patient,   v_vm_visit,  'completed',
                                V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
                          END IF;
					 IF (trim(v_vm_code) = 'vm_5') THEN -- VM_5 All visit associated CRFs marked 'submitted'
					  Pkg_Vm_Rule.SP_VIS_RULE_CRFS (v_patprot,   v_patient,   v_vm_visit,  'submitted',
                                V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
                          END IF;
					 IF (trim(v_vm_code) = 'vm_2') THEN    -- VM_2 All events within the visit are marked as 'Done'
                               Pkg_Vm_Rule.SP_VM_PATPROT_EVENT_DONE_ALL ( v_patprot,v_protocol,v_vm_visit,
                                V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
                          END IF;
                          IF (trim(v_vm_code) = 'vm_3') THEN -- VM_3 At least one event within the visit is marked as 'Done'


                               Pkg_Vm_Rule.SP_VM_PATPROT_EVENT_DONE_ONE ( v_patprot,0,v_protocol,v_vm_visit,
                                V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );

	                           --p('**V_MILESTONEFLAG ' || V_MILESTONEFLAG) ;
		                     --p('**V_ACHIEVEMENT_DATE   ' || V_ACHIEVEMENT_DATE ) ;

                          END IF;
                       IF V_MILESTONEFLAG >= 1 THEN -- milestone is met

                       --p('**V_MILESTONEFLAG ' || V_MILESTONEFLAG) ;
                       --p('**V_ACHIEVEMENT_DATE   ' || V_ACHIEVEMENT_DATE ) ;

     				 -- Insert a record in milestone log if milestone is met
        			      INSERT INTO ER_MILENOTLOG ( PK_MILENOTLOG,FK_PAT,FK_MILESTONE,FK_PATPROT,CREATED_ON)
                          VALUES (SEQ_ER_MILENOTLOG.NEXTVAL,v_patient,v_vm_fk_ms,v_patprot,SYSDATE);

                       --p('################inserted record################### ') ;

				  -- set notification mail contents

                       v_fparam :=  v_studynumber || '~Visit~'|| v_vm_desc ||'~'|| v_vm_msdesc  ||'~'|| TO_CHAR(V_ACHIEVEMENT_DATE) || '~' || v_patcode;
               	   v_msg :=    PKG_COMMON.SCH_GETMAIL(V_VM_MSGTEMP ,v_fparam);

    				 -- call SP_TRANMAIL to Insert a record in sch_msgtran for notification for every user : clubbed notification

        				  SP_TRANMAIL(v_msg,v_vm_users,v_patprot);

                      END IF ; -- milestone is met
                    END IF; -- if - for milestone not met and not. not sent
                   END IF; -- if - for milestone calendar = patprot cal
                END LOOP ; --loop 3 all visit milestones

               -- p('event milestone----' || V_EM_PKMS.count);

                FOR s IN 1..V_EM_PKMS.COUNT --for all event milestones in study, check for this enrollment
                LOOP --loop 4, all event  milestones
    		       v_em_fk_ms := TO_NUMBER(trim(V_EM_PKMS(s)));
    			  v_em_code  := trim(V_EM_RULECODE(s));
                 v_em_visit := TO_NUMBER(trim(V_EM_RULEVISIT(s)));
			  v_em_users := trim(V_EM_RULEUSERS(s));
           	  v_em_calendar := TO_NUMBER(trim(V_EM_CAL(s)));
			  v_em_event := TO_NUMBER(trim(V_EM_RULEEVENT(s)));
                 v_em_desc := trim(v_em_ruledesc(s))  ;

	    		  v_logcount := 0;
      		  V_MILESTONEFLAG := 0;
			  V_ACHIEVEMENT_DATE := NULL;

		    	  BEGIN
                	   SELECT pkg_tz.f_getmaxtz(v_em_users)
  	                  INTO V_ENDDATE
                	   FROM dual;

                 EXCEPTION WHEN NO_DATA_FOUND THEN
                       V_ENDDATE := SYSDATE ;
                 END ;


			IF (v_protocol = v_em_calendar) THEN -- if milestone calendar equals patprot calendar
   				  SELECT COUNT(*) --check whether there is a log entry for this milestone for this enrollment
     			  INTO v_logcount
				  FROM ER_MILENOTLOG
				  WHERE  FK_PAT =  v_patient AND
       		       FK_MILESTONE = v_em_fk_ms AND
				  FK_PATPROT = v_patprot ;

     	    	    /*p('event milestone------' || v_patprot);
		           p('**ms ' || v_em_fk_ms || '***type --' || v_em_code || '***v_vm_users --' || v_em_users);
			      p('**v_logcount ' || v_logcount || '***v_em_calendar --' || v_em_calendar || '***v_em_visit --' || v_em_visit);
                     p('**v_em_event ' || v_em_event);
	    		      p('v_em_desc ' || v_em_desc);*/

  				  IF v_logcount = 0 THEN  --check whether MS is achieved

 				      --get the name of the protocol calendar

          		  	 SELECT NAME
					 INTO v_protocol_calname
					 FROM EVENT_ASSOC WHERE  event_id = v_em_calendar AND UPPER(EVENT_TYPE) IN('p','P') ;


          		  	 SELECT NAME
					 INTO v_eventname
					 FROM EVENT_ASSOC WHERE  event_id = v_em_event ;

					v_em_msdesc :=  'Event ' || v_eventname ||  ' in Visit ' || v_em_visit || ' ' || v_protocol_calname;


                        IF (trim(v_em_code) = 'em_2') THEN
                         Pkg_Em_Rule.SP_EVENT_DONE ( v_em_event,v_patprot,V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
                        END IF;
                        IF (trim(v_em_code) = 'em_3') THEN
                         Pkg_Em_Rule.SP_EVE_RULE_CRF ( v_em_event,'completed',v_patprot,V_STDATE, V_ENDDATE,  V_ACHIEVEMENT_DATE, V_MILESTONEFLAG );
                       END IF;
                        IF (trim(v_em_code) = 'em_4') THEN
                        Pkg_Em_Rule.SP_EVE_RULE_CRF ( v_em_event,'submitted',v_patprot,V_STDATE, V_ENDDATE,  V_ACHIEVEMENT_DATE, V_MILESTONEFLAG );
                       END IF;

        	             /*p('**V_MILESTONEFLAG ' || V_MILESTONEFLAG) ;
                       p('**V_ACHIEVEMENT_DATE   ' || V_ACHIEVEMENT_DATE ) ; */

                      IF V_MILESTONEFLAG >= 1 THEN -- milestone is met
     				 -- Insert a record in milestone log if milestone is met
  			      INSERT INTO ER_MILENOTLOG ( PK_MILENOTLOG,FK_PAT,FK_MILESTONE,FK_PATPROT,CREATED_ON)
                     VALUES (SEQ_ER_MILENOTLOG.NEXTVAL,v_patient,v_em_fk_ms,v_patprot,SYSDATE);

				 -- set notification mail contents

                     v_fparam :=  v_studynumber || '~Event~'|| v_em_desc ||'~'|| v_em_msdesc  ||'~'|| TO_CHAR(V_ACHIEVEMENT_DATE) || '~' || v_patcode;
               	 v_msg :=    PKG_COMMON.SCH_GETMAIL(V_EM_MSGTEMP ,v_fparam);

    				 -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user

				  SP_TRANMAIL(v_msg,v_em_users,v_patprot);

                     END IF ; -- milestone is met
                   END IF; -- if - for milestone not met and not. not sent

                  END IF; -- if - for milestone calendar = patprot cal
			   END LOOP; --end of loop 4, all event  milestones
       END LOOP; -- loop 2
     END LOOP; --loop1

	/* update er_ctrltab with sysdate for LAST EXECUTION DATE OF PROCEDURE*/
	/*UPDATE er_ctrltab
     SET CTRL_VALUE = to_char(sysdate)
     Where CTRL_KEY = 'VMEM_NOT';*/

	COMMIT;

   END SP_CREATE_MILNOT;

   PROCEDURE SP_STUDY_MILESTONE_VMEM (
   P_STUDY                    NUMBER,
   P_MSTYPE                   VARCHAR2,
   O_M_PKMS            OUT   TYPES.SMALL_STRING_ARRAY,
   O_M_RULECODE        OUT   TYPES.SMALL_STRING_ARRAY,
   O_M_RULEVISIT       OUT   TYPES.SMALL_STRING_ARRAY,
   O_M_EVENT       OUT       TYPES.SMALL_STRING_ARRAY,
   O_M_RULEUSERS       OUT   TYPES.SMALL_STRING_ARRAY ,
   O_M_RULECAL       OUT   TYPES.SMALL_STRING_ARRAY,
   O_M_RULEDESC       OUT   TYPES.SMALL_STRING_ARRAY,
   O_M_RULEAMT       OUT   TYPES.SMALL_STRING_ARRAY
  )
  AS
 /* Date : 3 Jul 2002
    Author: Sonia Sahni
    Purpose - GET all PM milestones for a study
 */
   V_M_PKMS            TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULECODE        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULEVISIT       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULEUSERS       TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_EVENT           TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULECAL         TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULEDESC        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   V_M_RULEAMT        TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
   O_COUNT NUMBER := 0;
  BEGIN
  FOR i IN (SELECT  TO_CHAR(m.PK_MILESTONE) PK_MILESTONE, TO_CHAR(m.MILESTONE_VISIT) MILESTONE_VISIT,
             m.MILESTONE_USERSTO, cd.codelst_subtyp, TO_CHAR(m.FK_EVENTASSOC) MILESTONE_EVENT,
             FK_CAL MILESTONE_CAL, cd.codelst_desc, m.milestone_amount
             FROM ER_MILESTONE m, ER_CODELST cd WHERE
             fk_study = p_study AND
             cd.pk_codelst = m.FK_CODELST_RULE AND
             trim(cd.codelst_type) = trim(p_mstype) AND
             m.MILESTONE_USERSTO  IS  NOT NULL AND
             m.MILESTONE_DELFLAG <> 'Y'
             )
  LOOP

         O_COUNT := O_COUNT + 1;
         V_M_PKMS.EXTEND;
         V_M_PKMS(O_COUNT) := i.PK_MILESTONE;
         V_M_RULECODE.EXTEND;
	    V_M_RULECODE(O_COUNT) := i.codelst_subtyp;
	    V_M_RULEVISIT.EXTEND;
         V_M_RULEVISIT(O_COUNT) := i.MILESTONE_VISIT;
	    V_M_RULEUSERS.EXTEND;
	    V_M_RULEUSERS(O_COUNT) := i.MILESTONE_USERSTO;
	    V_M_EVENT.EXTEND;
         V_M_EVENT(O_COUNT) := i.MILESTONE_EVENT;
	    V_M_RULECAL.EXTEND;
	    V_M_RULECAL  (O_COUNT) := i.MILESTONE_CAL;

        V_M_RULEDESC.EXTEND;
	    V_M_RULEDESC  (O_COUNT) := i.codelst_desc;
         V_M_RULEAMT.EXTEND;
	    V_M_RULEAMT  (O_COUNT) := i.milestone_amount;

   END LOOP;
   O_M_PKMS :=  V_M_PKMS;
   O_M_RULECODE  := V_M_RULECODE;
   O_M_RULEVISIT := V_M_RULEVISIT;
   O_M_RULEUSERS := V_M_RULEUSERS;
   O_M_EVENT := V_M_EVENT;
   O_M_RULECAL := V_M_RULECAL;
   O_M_RULEDESC := V_M_RULEDESC;
   O_M_RULEAMT := V_M_RULEAMT;
  END SP_STUDY_MILESTONE_VMEM;
------------------------------------------------------
 PROCEDURE SP_CREATE_PM_MILNOT
   AS
 /* Date : 5 Jul 2002
    Author: Sonia Sahni
    Purpose - This will be a scheduled procedure that will create 'PM Patient Milestone' milestone achievement
    notifications on regular intervals
 */
  v_studyid NUMBER;
  v_patprot NUMBER;
  v_patient NUMBER;
  v_logcount NUMBER := 0;
  v_protocol NUMBER := 0;
  v_pm_fk_ms NUMBER := 0;
  v_pm_code CHAR(15);
  v_pm_visit NUMBER :=0;
  v_pm_users VARCHAR2(500);
  v_pm_calendar NUMBER :=0;
  v_pm_count NUMBER :=0;
  V_STDATE DATE := TRUNC(SYSDATE);
  V_ENDDATE DATE := TRUNC(SYSDATE);
  V_MILESTONEFLAG NUMBER := 0;
  V_ACHIEVEMENT_DATE DATE ;
  V_LASTDT VARCHAR2(200);
  V_RULEDESC VARCHAR2(200);

--  v_studynumber VARCHAR2(20);
    v_studynumber VARCHAR2(100);--JM:
  V_PM_MSGTEMP VARCHAR2(4000);
  V_MSG VARCHAR2(4000);
  v_fparam VARCHAR2(500);

  --v_code_active Number :=0;

  v_rulecomp_desc VARCHAR2(200);

 BEGIN


     /*get last execution date of notification procedure */

/*   select CTRL_VALUE
   into V_LASTDT
   from er_ctrltab
   Where CTRL_KEY = 'PM_NOT'; */

  /* if V_LASTDT is null then
    V_STDATE := to_date('01-JAN-1900','dd-mon-yyyy');
    else
    V_STDATE := to_date(V_LASTDT ,'dd-mon-yy') ;
   end if; */

   V_STDATE := TO_DATE('01-JAN-1900','dd-mon-yyyy');

  -- p('V_STDATE ' || V_STDATE);
  -- p('V_LASTDT' || V_LASTDT);

   v_pm_visit := 1;

 --  V_PM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('PM');
    V_PM_MSGTEMP  :=  PKG_COMMON.SCH_GETMAILMSG('c_pm');

   -- get

/*    select PK_CODELST
    into v_code_active
    from ER_CODELST
    where CODELST_TYPE = 'studystat'
    and CODELST_SUBTYP = 'active';*/



   /* get studies for which milestones are defined */
    FOR i IN ( SELECT PK_MILESTONE,ER_MILESTONE.FK_STUDY,FK_CAL,FK_CODELST_RULE ,MILESTONE_VISIT ,
                MILESTONE_COUNT  , MILESTONE_USERSTO,CODELST_SUBTYP , CODELST_DESC, STUDY_NUMBER,
			 TO_DATE(TO_CHAR(STUDY_ACTUALDT,'dd-mon-yyyy'),'dd-mon-yyyy' ) STUDY_ACTUALDT
			FROM ER_MILESTONE , ER_CODELST, ER_STUDY
		  	WHERE MILESTONE_DELFLAG <> 'Y' AND
               trim(MILESTONE_TYPE) = 'PM' AND
		     MILESTONE_USERSTO IS NOT NULL AND
			pk_codelst = FK_CODELST_RULE AND
               trim(codelst_type) = 'PM' AND
               ER_MILESTONE.fk_study = pk_study)
     LOOP -- loop 1

        v_studyid :=    i.fk_study;
        v_pm_fk_ms :=  i.PK_MILESTONE;
  	    v_pm_calendar := i.FK_CAL;
        v_pm_code  := i.CODELST_SUBTYP ;
        v_pm_users := i.MILESTONE_USERSTO ;
	    v_pm_count := i.MILESTONE_COUNT;
        v_logcount := 0;
	    V_MILESTONEFLAG := 0;
	    V_ACHIEVEMENT_DATE := NULL;
        V_RULEDESC := i.CODELST_DESC;
        v_studynumber := i.STUDY_NUMBER;
	   V_STDATE := i.STUDY_ACTUALDT ;

--	   p('**ms' || v_pm_fk_ms || '***type --' || v_pm_code || '***v_pm_users --' || v_pm_users || '***v_pm_count --' || v_pm_count );


   	   SELECT COUNT(*) --check whether there is a log entry for this milestone for this enrollment
  	   INTO v_logcount
  	   FROM ER_MILENOTLOG
	   WHERE  fk_study = v_studyid AND
         FK_MILESTONE = v_pm_fk_ms ;

      --        p('**v_logcount' || v_logcount);

          --   p('V_STDATE ' || V_STDATE);


    	  -- calculate end date according to user's timezone - max timezone for a user list


    	  BEGIN
      	   SELECT pkg_tz.f_getmaxtz(v_pm_users)
  	        INTO V_ENDDATE
     	   FROM dual;

       	 EXCEPTION WHEN NO_DATA_FOUND THEN
              V_ENDDATE := SYSDATE ;
         END ;

       --   p('v_pm_users' || v_pm_users || 'V_ENDDATE**' || V_ENDDATE);

       IF v_logcount = 0 THEN  --check whether MS is achieved

        --   p('**fk_study' || v_studyid);
           IF (trim(v_pm_code) = 'pm_1') THEN --  Enrolled patients, active status
               Pkg_Pm_Rule.SP_CHK_PAT_RULE_ENR_ACT ( v_studyid,0,v_pm_count ,V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
            END IF;
            IF (trim(v_pm_code) = 'pm_2') THEN --  Enrolled patients, any status
                Pkg_Pm_Rule.SP_CHK_PAT_RULE_ENR_ALL ( v_studyid,0,v_pm_count ,V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
             END IF;
		   IF (trim(v_pm_code) = 'pm_4') THEN --  Patient's first visit (at least one event) is marked as 'Done'
                Pkg_Pm_Rule.SP_RULE_CHK_VIS_DONE_ONE ( v_studyid,0,v_pm_count ,v_pm_visit,V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
             END IF;
	   	   IF (trim(v_pm_code) = 'pm_5') THEN --  Patient's first visit (all events) is marked as 'Done'
                Pkg_Pm_Rule.SP_RULE_CHK_VIS_DONE_COMPLETE( v_studyid,0,v_pm_count ,V_STDATE, V_ENDDATE,  V_MILESTONEFLAG,V_ACHIEVEMENT_DATE );
             END IF;

		   /*
		   p('trim(v_pm_code)**' || trim(v_pm_code)) ;
     	   p('**V_MILESTONEFLAG ' || V_MILESTONEFLAG) ;
             p('**V_ACHIEVEMENT_DATE   ' || V_ACHIEVEMENT_DATE ) ;*/

              IF V_MILESTONEFLAG >= 1 THEN -- milestone is met
     				 -- Insert a record in milestone log if milestone is met
           	      INSERT INTO ER_MILENOTLOG ( PK_MILENOTLOG,FK_MILESTONE,FK_STUDY,CREATED_ON)
                     VALUES (SEQ_ER_MILENOTLOG.NEXTVAL,v_pm_fk_ms,v_studyid,SYSDATE);

			    v_rulecomp_desc := 'Patient Count ' || v_pm_count ;

                   v_fparam :=  v_studynumber || '~Patient Status~'|| V_RULEDESC ||'~'|| v_rulecomp_desc ||'~'|| TO_CHAR(V_ACHIEVEMENT_DATE) ;
               	   v_msg :=    PKG_COMMON.SCH_GETMAIL(V_PM_MSGTEMP ,v_fparam);

    				 -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user

				  SP_TRANMAIL(v_msg,v_pm_users,NULL);


                 END IF ; -- milestone is met
              END IF; -- if - for milestone not met and not. not sent
         END LOOP; --loop1

	/* update er_ctrltab with sysdate for LAST EXECUTION DATE OF PROCEDURE*/
	/*UPDATE er_ctrltab
     SET CTRL_VALUE = to_char(sysdate)
     Where CTRL_KEY = 'PM_NOT'; */

	COMMIT;

   END SP_CREATE_PM_MILNOT;
-----------------------------------------------------

 PROCEDURE SP_MILEMAIL (
   p_mailtxt VARCHAR2,
   p_users   VARCHAR2,
   p_patprot NUMBER
  )
 AS
 v_cnt NUMBER ;
 v_patprot NUMBER;
 V_POS		NUMBER :=0 ;
 v_params VARCHAR2(1000);
 V_USER_STR VARCHAR2(500);
 v_msgsendon DATE := SYSDATE;

 BEGIN

  IF p_patprot <> 0 THEN
      v_patprot:=p_patprot;
   END IF;

   V_USER_STR := p_users || ',' ;
   v_params := V_USER_STR ;

        LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;

		   	INSERT INTO sch_dispatchmsg (
				PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE , FK_SCHEVENT  ,
				MSG_TEXT ,FK_PAT,CREATOR,IP_ADD,FK_PATPROT             )
			      VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,v_msgsendon,0,'R',
				 NULL,p_mailtxt,v_params,NULL,NULL, v_patprot) ;

			   V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
           --			p('DONE');

		END LOOP ; /* end loop for multiple users*/

 END SP_MILEMAIL;

/*INSERTS A MAIL IN SCH_TRANMSG for clubbed mails*/

 PROCEDURE SP_TRANMAIL (
   p_mailtxt VARCHAR2,
   p_users   VARCHAR2,
   p_patprot NUMBER
  )
 AS
 v_cnt NUMBER ;
 v_patprot NUMBER;
 V_POS		NUMBER :=0 ;
 v_params VARCHAR2(1000);
 V_USER_STR VARCHAR2(500);
 v_msgsendon DATE := SYSDATE;

 BEGIN

  IF p_patprot <> 0 THEN
      v_patprot:=p_patprot;
   END IF;

   V_USER_STR := p_users || ',' ;
   v_params := V_USER_STR ;

        LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;

		    	INSERT INTO sch_msgtran (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_USER		       ,
				FK_PATPROT             )
              VALUES ( SEQ_MSGTRAN.NEXTVAL ,v_msgsendon,0,'dl_milnot',
				 NULL,p_mailtxt,v_params, v_patprot) ;

			   V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
           --			p('DONE');

		END LOOP ; /* end loop for multiple users*/

 END SP_TRANMAIL;
   /*end of package*/
  END Pkg_Milnot;
/


CREATE SYNONYM ESCH.PKG_MILNOT FOR PKG_MILNOT;


CREATE SYNONYM EPAT.PKG_MILNOT FOR PKG_MILNOT;


GRANT EXECUTE, DEBUG ON PKG_MILNOT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_MILNOT TO ESCH;

