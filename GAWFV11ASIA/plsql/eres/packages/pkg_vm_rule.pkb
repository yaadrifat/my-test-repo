CREATE OR REPLACE PACKAGE BODY        "PKG_VM_RULE"
AS
   PROCEDURE SP_VM_COUNT_EVENT_DONE_ONE (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sonia Sahni
  Date: 27th June 2002
   Purpose - Returns Count - Number of times VM_3 is achieved
              Returns an array of achievement dates
    VM_3 At least one event within the visit is marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
	 V_CUR   TYPES.cursorType;
	 V_PATPROT NUMBER;

   BEGIN

      O_COUNT := 0;
	 --date comparison change done by Sonika on Sep 13/26, 2002
    IF P_ORG_ID > 0 THEN
       OPEN V_CUR FOR SELECT PK_PATPROT, MIN (EVENT_EXEON) EVENT_EXEON
                  FROM ERV_PATSTUDY_EVEALL A
                 WHERE FK_STUDY = P_STUDY
                   AND VISIT = P_VISIT
                   AND FK_PROTOCOL = P_CAL
			    AND PER_SITE = P_ORG_ID
                   AND TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                   AND TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE
                   AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
                   AND PK_EVENTSTAT =
                          (SELECT MAX (PK_EVENTSTAT)
                             FROM ERV_PATSTUDY_EVEALL B
                            WHERE B.SCH_EVENTS1_PK = A.SCH_EVENTS1_PK
                              AND TO_DATE(TO_CHAR(B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                              AND TO_DATE(TO_CHAR(B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE)
                 GROUP BY PK_PATPROT ORDER BY 2 ;
    ELSE
       OPEN V_CUR FOR SELECT PK_PATPROT, MIN (EVENT_EXEON) EVENT_EXEON
                  FROM ERV_PATSTUDY_EVEALL A
                 WHERE FK_STUDY = P_STUDY
                   AND VISIT = P_VISIT
                   AND FK_PROTOCOL = P_CAL
                   AND TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                   AND TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE
                   AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
                   AND PK_EVENTSTAT =
                          (SELECT MAX (PK_EVENTSTAT)
                             FROM ERV_PATSTUDY_EVEALL B
                            WHERE B.SCH_EVENTS1_PK = A.SCH_EVENTS1_PK
                              AND TO_DATE(TO_CHAR(B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                              AND TO_DATE(TO_CHAR(B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE)
                 GROUP BY PK_PATPROT ORDER BY 2 ;
    END IF;
      LOOP
	  FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
       EXIT WHEN V_CUR%NOTFOUND;
         O_COUNT := O_COUNT + 1;
         V_DATEARRAY.EXTEND;
         V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
--         p('count:' || O_COUNT);
  --       p('date:' || V_ACHIEVEMENT_DATE);

      END LOOP;
    CLOSE V_CUR;
      O_ACHIEVEMENTDATES := V_DATEARRAY;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VM_COUNT_EVENT_DONE_ONE;
-------------------------------------------------------------------------------------------------------------------------

   PROCEDURE SP_VM_COUNT_EVENT_DONE_ALL (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sonia Sahni
  Date: 1st July June 2002
   Purpose - Returns Count - Number of times VM_2 is achieved
              Returns an array of achievement dates
    VM_2 All events within the visit are marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      I                    NUMBER;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

	 V_CUR   TYPES.cursorType;
	 V_PATPROT NUMBER;
   BEGIN

      O_COUNT := 0;
	-- p('p_org_ig ' || p_org_id);
	 --date comparison changed by Sonika on Sep 13, 2002
   IF P_ORG_ID > 0 THEN
    OPEN V_CUR FOR SELECT V.PK_PATPROT, MAX (EV.EVENTSTAT_DT) EVENT_EXEON
                   FROM ER_PATPROT V,
                       SCH_EVENTS1 S,
                       SCH_CODELST C,
                       SCH_EVENTSTAT EV,
				   ER_PER P
                 WHERE V.FK_STUDY =   P_STUDY
                   AND V.FK_PROTOCOL =   P_CAL
			    AND V.FK_PER = P.PK_PER
			    AND P.FK_SITE = P_ORG_ID
                   AND V.PK_PATPROT =  S.FK_PATPROT
                   AND S.EVENT_ID =   EV.FK_EVENT
                   AND C.PK_CODELST =   EV.EVENTSTAT
                   AND C.CODELST_TYPE = 'eventstatus'
                   AND trim ( C.CODELST_SUBTYP ) =  'ev_done'
                   AND S.VISIT =  P_VISIT
                   AND V.PK_PATPROT IN
                          (SELECT B.PATPROT
                             FROM (SELECT S.FK_PATPROT  PATPROT,  COUNT ( S.EVENT_ID  ) CNT
                                     FROM SCH_EVENTS1 S, ER_PATPROT E
                                    WHERE E.FK_STUDY = P_STUDY
                                      AND E.FK_PROTOCOL = P_CAL
                                      AND E.PK_PATPROT = S.FK_PATPROT
                                      AND S.VISIT =  P_VISIT
                                    GROUP BY S.FK_PATPROT) A,
                                  (SELECT V.PK_PATPROT   PATPROT,
                                          COUNT ( S.EVENT_ID )  CNT
                                     FROM ER_PATPROT V,
                                          SCH_EVENTS1 S,
                                          SCH_CODELST C,
                                          SCH_EVENTSTAT EV
                                    WHERE  S.EVENT_ID =    EV.FK_EVENT
                                      AND C.PK_CODELST =  EV.EVENTSTAT
                                      AND C.CODELST_TYPE =  'eventstatus'
                                      AND trim (C.CODELST_SUBTYP ) =  'ev_done'
                                      AND S.VISIT = P_VISIT
                                      AND EV.PK_EVENTSTAT =  (SELECT MAX (ES.PK_EVENTSTAT )
                                                FROM SCH_EVENTSTAT ES
                                               WHERE ES.FK_EVENT =  S.EVENT_ID)
							   AND V.FK_STUDY =  P_STUDY
                                      AND V.FK_PROTOCOL =  P_CAL
                                      AND V.PK_PATPROT =   S.FK_PATPROT
                                    GROUP BY V.PK_PATPROT) B
                            WHERE A.PATPROT =   B.PATPROT
                              AND A.CNT =  B.CNT)
                 GROUP BY V.PK_PATPROT  HAVING     MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) >=   P_STDATE
                       AND MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) <= P_ENDDATE ORDER BY 2 ;
	ELSE
    OPEN V_CUR FOR SELECT V.PK_PATPROT, MAX (EV.EVENTSTAT_DT) EVENT_EXEON
                   FROM ER_PATPROT V,
                       SCH_EVENTS1 S,
                       SCH_CODELST C,
                       SCH_EVENTSTAT EV
                 WHERE V.FK_STUDY =   P_STUDY
                   AND V.FK_PROTOCOL =   P_CAL
                   AND V.PK_PATPROT =  S.FK_PATPROT
                   AND S.EVENT_ID =   EV.FK_EVENT
                   AND C.PK_CODELST =   EV.EVENTSTAT
                   AND C.CODELST_TYPE = 'eventstatus'
                   AND trim ( C.CODELST_SUBTYP ) =  'ev_done'
                   AND S.VISIT =  P_VISIT
                   AND V.PK_PATPROT IN
                          (SELECT B.PATPROT
                             FROM (SELECT S.FK_PATPROT  PATPROT,  COUNT ( S.EVENT_ID  ) CNT
                                     FROM SCH_EVENTS1 S, ER_PATPROT E
                                    WHERE E.FK_STUDY = P_STUDY
                                      AND E.FK_PROTOCOL = P_CAL
                                      AND E.PK_PATPROT = S.FK_PATPROT
                                      AND S.VISIT =  P_VISIT
                                    GROUP BY S.FK_PATPROT) A,
                                  (SELECT V.PK_PATPROT   PATPROT,
                                          COUNT ( S.EVENT_ID )  CNT
                                     FROM ER_PATPROT V,
                                          SCH_EVENTS1 S,
                                          SCH_CODELST C,
                                          SCH_EVENTSTAT EV
                                    WHERE  S.EVENT_ID =    EV.FK_EVENT
                                      AND C.PK_CODELST =  EV.EVENTSTAT
                                      AND C.CODELST_TYPE =  'eventstatus'
                                      AND trim (C.CODELST_SUBTYP ) =  'ev_done'
                                      AND S.VISIT = P_VISIT
                                      AND EV.PK_EVENTSTAT =  (SELECT MAX (ES.PK_EVENTSTAT )
                                                FROM SCH_EVENTSTAT ES
                                               WHERE ES.FK_EVENT =  S.EVENT_ID)
							   AND V.FK_STUDY =  P_STUDY
                                      AND V.FK_PROTOCOL =  P_CAL
                                      AND V.PK_PATPROT =   S.FK_PATPROT
                                    GROUP BY V.PK_PATPROT) B
                            WHERE A.PATPROT =   B.PATPROT
                              AND A.CNT =  B.CNT)
                 GROUP BY V.PK_PATPROT  HAVING     MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) >=   P_STDATE
                       AND MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) <= P_ENDDATE ORDER BY 2;
	END IF;
      LOOP
        FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
        EXIT WHEN V_CUR%NOTFOUND;
         O_COUNT := O_COUNT + 1;
         V_DATEARRAY.EXTEND;
         V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
     END LOOP;
    CLOSE V_CUR;
      O_ACHIEVEMENTDATES := V_DATEARRAY;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VM_COUNT_EVENT_DONE_ALL;

----------------------------------------------------------------------------------------------------------------
   PROCEDURE SP_VM_PAT_CNT_EVENT_DONE_ONE (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      P_PAT                      NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sonia Sahni
  Date: 1st July 2002
   Purpose - Returns Count - Number of times VM_3 is achieved
              Returns an array of achievement dates for a patient
    VM_3 At least one event within the visit is marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      I                    NUMBER;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
   BEGIN

      O_COUNT := 0;
	 --date comparison changed by Sonika on Sep 13/26, 2002
      FOR I IN( SELECT PK_PATPROT, MIN (EVENT_EXEON) EVENT_EXEON
                  FROM ERV_PATSTUDY_EVEALL A
                 WHERE FK_STUDY = P_STUDY
                   AND VISIT = P_VISIT
                   AND FK_PER = P_PAT
                   AND FK_PROTOCOL = P_CAL
                   AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                   AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE
                   AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
                   AND PK_EVENTSTAT =
                          (SELECT MAX (PK_EVENTSTAT)
                             FROM ERV_PATSTUDY_EVEALL B
                            WHERE B.SCH_EVENTS1_PK = A.SCH_EVENTS1_PK
                              AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                              AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE)
                 GROUP BY PK_PATPROT ORDER BY 2)
      LOOP
         O_COUNT := O_COUNT + 1;
         V_ACHIEVEMENT_DATE := I.EVENT_EXEON;
         V_DATEARRAY.EXTEND;
         V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
        -- P ('count:' || O_COUNT);
--         P ('date:' || V_ACHIEVEMENT_DATE);
      END LOOP;

      O_ACHIEVEMENTDATES := V_DATEARRAY;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VM_PAT_CNT_EVENT_DONE_ONE;
---------------------------------------------------------------
   PROCEDURE SP_VM_PATPROT_EVENT_DONE_ONE (
      P_PATPROT                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATE    OUT   DATE
   )
   AS
/*Author: Sonia Sahni
  Date: 4th July 2002
   Purpose - Returns 1 - if VM_3 is achieved for the patprot
             Returns 0 - if VM_3 is not achieved for the patprot
              Returns achievement date/null for a patient
   VM_3 At least one event within the visit is marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
--      V_DYNSQL         VARCHAR2(4000);
	 V_CUR   TYPES.cursorType;
   BEGIN

      O_COUNT := 0;
	 --date comparison changed by Sonika on Sep 13/26, 2002
	 IF P_ORG_ID > 0 THEN
     	 OPEN v_cur FOR SELECT COUNT(PK_PATPROT), MIN (EVENT_EXEON) EVENT_EXEON
                  FROM ERV_PATSTUDY_EVEALL A
                     WHERE PK_PATPROT = P_PATPROT
                     AND VISIT = P_VISIT
			      AND PER_SITE = P_ORG_ID
                     AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                     AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE
                     AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
                     AND PK_EVENTSTAT =
                            (SELECT MAX (PK_EVENTSTAT)
                               FROM ERV_PATSTUDY_EVEALL B
                              WHERE B.SCH_EVENTS1_PK = A.SCH_EVENTS1_PK
                               AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                               AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE)
                  GROUP BY PK_PATPROT ORDER BY 2;
      ELSE
           OPEN v_cur FOR SELECT COUNT(PK_PATPROT), MIN (EVENT_EXEON) EVENT_EXEON
                  FROM ERV_PATSTUDY_EVEALL A
                     WHERE PK_PATPROT = P_PATPROT
                     AND VISIT = P_VISIT
                     AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                     AND TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE
                     AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
                     AND PK_EVENTSTAT =
                            (SELECT MAX (PK_EVENTSTAT)
                               FROM ERV_PATSTUDY_EVEALL B
                              WHERE B.SCH_EVENTS1_PK = A.SCH_EVENTS1_PK
                               AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= P_STDATE
                               AND TO_DATE(TO_CHAR( B.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= P_ENDDATE)
                  GROUP BY PK_PATPROT ORDER BY 2 ;
	END IF;

       LOOP
   	    FETCH V_CUR INTO V_MILESTONEFLAG, V_ACHIEVEMENT_DATE;
	   EXIT WHEN V_CUR%NOTFOUND;
          O_COUNT := V_MILESTONEFLAG;
          O_ACHIEVEMENTDATE  := V_ACHIEVEMENT_DATE;

      END LOOP;
    CLOSE V_CUR;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VM_PATPROT_EVENT_DONE_ONE;
----------------------------------------------------------------

   PROCEDURE SP_VM_PAT_CNT_EVENT_DONE_ALL (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      P_PAT                      NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sonia Sahni
  Date: 1st July June 2002
   Purpose - Returns Count - Number of times VM_2 is achieved
              Returns an array of achievement dates for the patient
    VM_2 All events within the visit are marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      I                    NUMBER;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
	 V_CUR   TYPES.cursorType;
      V_PATPROT NUMBER;
   BEGIN

     O_COUNT := 0;
	 --date comparison changed by Sonika on Sep 13, 2002
	IF p_org_id > 0 THEN
        OPEN V_CUR FOR SELECT V.PK_PATPROT,   MAX ( EV.EVENTSTAT_DT ) EVENT_EXEON
                  FROM ER_PATPROT V, SCH_EVENTS1 S,
                       SCH_CODELST C,SCH_EVENTSTAT EV, ER_PER P
                 WHERE V.FK_STUDY = P_STUDY
                   AND V.FK_PROTOCOL = P_CAL
                   AND V.FK_PER =     P_PAT
			    AND V.FK_PER = P.PK_PER
			    AND P.FK_SITE = P_ORG_ID
                   AND V.PK_PATPROT =   S.FK_PATPROT
                   AND S.EVENT_ID =  EV.FK_EVENT
                   AND C.PK_CODELST =   EV.EVENTSTAT
                   AND C.CODELST_TYPE =  'eventstatus'
                   AND trim ( C.CODELST_SUBTYP) =   'ev_done'
                   AND S.VISIT = P_VISIT
                   AND V.PK_PATPROT IN
                          (SELECT B.PATPROT
                             FROM (SELECT S.FK_PATPROT PATPROT, COUNT ( S.EVENT_ID ) CNT
                                     FROM SCH_EVENTS1 S, ER_PATPROT E
                                    WHERE E.FK_STUDY =   P_STUDY
                                      AND E.FK_PROTOCOL = P_CAL
                                      AND E.FK_PER =  P_PAT
                                      AND E.PK_PATPROT = S.FK_PATPROT
                                      AND S.VISIT =  P_VISIT
                                    GROUP BY S.FK_PATPROT) A,
                                  (SELECT V.PK_PATPROT PATPROT,
                                          COUNT ( S.EVENT_ID) CNT
                                     FROM ER_PATPROT V,  SCH_EVENTS1 S,
                                          SCH_CODELST C,  SCH_EVENTSTAT EV
                                    WHERE S.EVENT_ID =   EV.FK_EVENT
                                      AND C.PK_CODELST =   EV.EVENTSTAT
                                      AND C.CODELST_TYPE =   'eventstatus'
                                      AND trim ( C.CODELST_SUBTYP) = 'ev_done'
                                      AND S.VISIT = P_VISIT
                                      AND EV.PK_EVENTSTAT = (SELECT MAX ( ES.PK_EVENTSTAT)
                                                         FROM SCH_EVENTSTAT ES
                                                         WHERE ES.FK_EVENT =
                                                         S.EVENT_ID)
             	                        AND V.FK_STUDY = P_STUDY
                                      AND V.FK_PROTOCOL = P_CAL
                                      AND V.FK_PER =  P_PAT
                                      AND V.PK_PATPROT =   S.FK_PATPROT
                                       GROUP BY V.PK_PATPROT) B
                                    WHERE A.PATPROT =  B.PATPROT
                                     AND A.CNT =   B.CNT)
                                 GROUP BY V.PK_PATPROT
                               HAVING MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) >=  P_STDATE
                               AND MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) <= P_ENDDATE ORDER BY 2 ;
	ELSE
	   OPEN V_CUR FOR SELECT V.PK_PATPROT,   MAX ( EV.EVENTSTAT_DT ) EVENT_EXEON
                  FROM ER_PATPROT V, SCH_EVENTS1 S,
                       SCH_CODELST C,SCH_EVENTSTAT EV
                 WHERE V.FK_STUDY = P_STUDY
                   AND V.FK_PROTOCOL = P_CAL
                   AND V.FK_PER =     P_PAT
                   AND V.PK_PATPROT =   S.FK_PATPROT
                   AND S.EVENT_ID =  EV.FK_EVENT
                   AND C.PK_CODELST =   EV.EVENTSTAT
                   AND C.CODELST_TYPE =  'eventstatus'
                   AND trim ( C.CODELST_SUBTYP) =   'ev_done'
                   AND S.VISIT = P_VISIT
                   AND V.PK_PATPROT IN
                          (SELECT B.PATPROT
                             FROM (SELECT S.FK_PATPROT PATPROT, COUNT ( S.EVENT_ID ) CNT
                                     FROM SCH_EVENTS1 S, ER_PATPROT E
                                    WHERE E.FK_STUDY =   P_STUDY
                                      AND E.FK_PROTOCOL = P_CAL
                                      AND E.FK_PER =  P_PAT
                                      AND E.PK_PATPROT = S.FK_PATPROT
                                      AND S.VISIT =  P_VISIT
                                    GROUP BY S.FK_PATPROT) A,
                                  (SELECT V.PK_PATPROT PATPROT,
                                          COUNT ( S.EVENT_ID) CNT
                                     FROM ER_PATPROT V,  SCH_EVENTS1 S,
                                          SCH_CODELST C,  SCH_EVENTSTAT EV
                                    WHERE S.EVENT_ID =   EV.FK_EVENT
                                      AND C.PK_CODELST =   EV.EVENTSTAT
                                      AND C.CODELST_TYPE =   'eventstatus'
                                      AND trim ( C.CODELST_SUBTYP) = 'ev_done'
                                      AND S.VISIT = P_VISIT
                                      AND EV.PK_EVENTSTAT = (SELECT MAX ( ES.PK_EVENTSTAT)
                                                         FROM SCH_EVENTSTAT ES
                                                         WHERE ES.FK_EVENT =
                                                         S.EVENT_ID)
             	                        AND V.FK_STUDY = P_STUDY
                                      AND V.FK_PROTOCOL = P_CAL
                                      AND V.FK_PER =  P_PAT
                                      AND V.PK_PATPROT =   S.FK_PATPROT
                                       GROUP BY V.PK_PATPROT) B
                                    WHERE A.PATPROT =  B.PATPROT
                                     AND A.CNT =   B.CNT)
                                 GROUP BY V.PK_PATPROT
                               HAVING MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) >=  P_STDATE
                               AND MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) <= P_ENDDATE ORDER BY 2;
	END IF;
       LOOP
  	   FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
        EXIT WHEN V_CUR%NOTFOUND;
           O_COUNT := O_COUNT + 1;
           V_DATEARRAY.EXTEND;
          V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);

       END LOOP;
	CLOSE V_CUR;
       O_ACHIEVEMENTDATES := V_DATEARRAY;
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
        O_COUNT := 0;
   END SP_VM_PAT_CNT_EVENT_DONE_ALL;

----------------------------------------------------------------
   PROCEDURE SP_VM_PATPROT_EVENT_DONE_ALL (
      P_PATPROT                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATE    OUT   DATE       )
   AS
/*Author: Sonia Sahni
  Date: 5th July 2002
   Purpose - Returns 1 if VM_2 is achieved for the given enrollment
             Returns 0 if VM_2 is not achieved for the given enrollment
             Returns rule achievement date /NULL
    VM_2 All events within the visit are marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      I                    NUMBER;
     BEGIN

      O_COUNT := 0;
	 --date comparison changed by Sonika on Sep 13, 2002
	   SELECT COUNT(DISTINCT V.PK_PATPROT),   MAX ( EV.EVENTSTAT_DT ) EVENT_EXEON
	   INTO V_MILESTONEFLAG, V_ACHIEVEMENT_DATE
          FROM ER_PATPROT V, SCH_EVENTS1 S,SCH_CODELST C,SCH_EVENTSTAT EV
                 WHERE V.PK_PATPROT =   P_PATPROT
			    AND V.PK_PATPROT = S.fk_patprot
                   AND S.EVENT_ID =  EV.FK_EVENT
                   AND C.PK_CODELST =   EV.EVENTSTAT
                   AND C.CODELST_TYPE =  'eventstatus'
                   AND trim ( C.CODELST_SUBTYP) =   'ev_done'
                   AND S.VISIT = P_VISIT
                   AND V.PK_PATPROT IN
                          (SELECT B.PATPROT
                             FROM (SELECT S.FK_PATPROT PATPROT, COUNT ( S.EVENT_ID ) CNT
                                     FROM SCH_EVENTS1 S, ER_PATPROT E
                                    WHERE E.PK_PATPROT = P_PATPROT AND
							       E.PK_PATPROT = S.FK_PATPROT  AND
								  S.VISIT =  P_VISIT
                                    GROUP BY S.FK_PATPROT) A,
                                  (SELECT V.PK_PATPROT PATPROT,
                                          COUNT ( S.EVENT_ID) CNT
                                     FROM ER_PATPROT V,  SCH_EVENTS1 S,
                                          SCH_CODELST C,  SCH_EVENTSTAT EV
                                    WHERE V.PK_PATPROT = P_PATPROT
                                      AND V.PK_PATPROT =   S.FK_PATPROT
                                      AND S.EVENT_ID =   EV.FK_EVENT
                                      AND C.PK_CODELST =   EV.EVENTSTAT
                                      AND C.CODELST_TYPE =   'eventstatus'
                                      AND trim ( C.CODELST_SUBTYP) = 'ev_done'
                                      AND S.VISIT = P_VISIT
                                      AND EV.PK_EVENTSTAT = (SELECT MAX ( ES.PK_EVENTSTAT)
                                                         FROM SCH_EVENTSTAT ES
                                                         WHERE ES.FK_EVENT =
                                                         S.EVENT_ID)
                                      GROUP BY V.PK_PATPROT) B
                                    WHERE A.PATPROT =  B.PATPROT
                                   AND A.CNT =   B.CNT)
                                GROUP BY V.PK_PATPROT
                               HAVING     MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) >=  P_STDATE
                               AND MAX ( TO_DATE(TO_CHAR( EV.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) <= P_ENDDATE ORDER BY 2   ;
           O_COUNT := V_MILESTONEFLAG;
           O_ACHIEVEMENTDATE := V_ACHIEVEMENT_DATE ;
          P ('count:' || O_COUNT);
          P ('date:' || V_ACHIEVEMENT_DATE);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
        O_COUNT := 0;
   END SP_VM_PATPROT_EVENT_DONE_ALL;
   -------------------------------------------
   PROCEDURE SP_VIS_RULE_CRFS_COUNT (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sajal
  Date: 17th June 2002
  Purpose - Rule: (VM_4 and VM_5)All visit associated CRFs marked p_crfstat_subt
yp. To count
  the number of times this rule has been achieved in a given study and also the
dates when it was achieved
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := pkg_dateutil.f_get_null_date_str;
	 V_CUR   TYPES.cursorType;
      V_PATPROT NUMBER;
	 V_PER NUMBER;

   BEGIN

      O_ACHIEVEMENTDATES   := TYPES.SMALL_STRING_ARRAY ();
      O_COUNT := 0;
	 IF P_ORG_ID > 0 THEN
          OPEN V_CUR FOR SELECT DISTINCT PK_PATPROT, FK_PER
                  FROM ERV_PATSTUDY_EVE_CRF
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL
			     AND PER_SITE = P_ORG_ID ;
	 ELSE
          OPEN V_CUR FOR SELECT DISTINCT PK_PATPROT, FK_PER
                  FROM ERV_PATSTUDY_EVE_CRF
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL;
	 END IF;

      LOOP
        FETCH V_CUR INTO V_PATPROT, V_PER;
        EXIT WHEN V_CUR%NOTFOUND;
         SP_VIS_RULE_CRFS (
            V_PATPROT,
            V_PER,
            P_VISIT,
            P_CRFSTAT_SUBTYP,
            P_STDATE,
            P_ENDDATE,
            V_MILESTONEFLAG,
            V_ACHIEVEMENT_DATE
         );
         O_COUNT := O_COUNT + V_MILESTONEFLAG;
         IF (V_MILESTONEFLAG = 1) THEN
            O_ACHIEVEMENTDATES.EXTEND;
            O_ACHIEVEMENTDATES (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
         END IF;

    END LOOP;
    CLOSE V_CUR;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VIS_RULE_CRFS_COUNT;
------------------------------------------------------
   PROCEDURE SP_VIS_RULE_CRFS_PATIENT (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_PATIENT                  NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sajal
  Date: 17th June 2002
  Purpose - Rule: (VM_4 and VM_5)All visit associated CRFs marked p_crfstat_subt
yp. To count
  the number of times this rule has been achieved for a given patient in a given
 study and
  also the dates when it was achieved
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := pkg_dateutil.f_get_null_date_str;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

   BEGIN

      O_COUNT := 0;
      FOR I IN( SELECT DISTINCT PK_PATPROT
                  FROM ERV_PATSTUDY_EVE_CRF
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL
                   AND FK_PER = P_PATIENT)
      LOOP
         SP_VIS_RULE_CRFS (
            I.PK_PATPROT,
            P_PATIENT,
            P_VISIT,
            P_CRFSTAT_SUBTYP,
            P_STDATE,
            P_ENDDATE,
            V_MILESTONEFLAG,
            V_ACHIEVEMENT_DATE
         );
         O_COUNT := O_COUNT + V_MILESTONEFLAG;
         IF (V_MILESTONEFLAG = 1) THEN
            V_DATEARRAY.EXTEND;
            V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_VIS_RULE_CRFS_PATIENT;
--------------------------------------------------------
   PROCEDURE SP_VIS_RULE_CRFS (
      P_PATPROT                  NUMBER,
      P_PATIENT                  NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENT_DATE   OUT   DATE
   )
   AS
/*Author: Sajal
  Date: 17th June 2002
  Purpose - Rule: (VM_4 and VM_5)All visit associated CRFs marked p_crfstat_subt
yp. To check
  whether this rule has been achieved for a given patprot and the date if it has
 been achieved
 */
      V_CRF_STATUS_CNT     NUMBER := 0;
      V_TOTAL_CRF_CNT      NUMBER := 0;
      V_STATUSFLAG         NUMBER := 0;
      V_ACHIEVEMENT_DATE   DATE   := pkg_dateutil.f_get_null_date_str;
   BEGIN
      O_COUNT := 0;
      O_ACHIEVEMENT_DATE := pkg_dateutil.f_get_null_date_str;
	 --p('PK_PATPROT ' || P_PATPROT );
	 --p('FK_PER ' || P_PATIENT);
 	 --p('P_ENDDATE ' || P_ENDDATE);
	 --date comparison changed by Sonika on Sep 13, 2002
      SELECT COUNT (DISTINCT CRF_PK)
        INTO V_TOTAL_CRF_CNT
        FROM ERV_PATSTUDY_EVE_CRF
       WHERE PK_PATPROT = P_PATPROT
         AND VISIT = P_VISIT
	    AND TO_DATE(TO_CHAR(CRF_CREATION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
         AND TO_DATE(TO_CHAR(CRF_CREATION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE ;

      --   p('V_TOTAL_CRF_CNT ' || V_TOTAL_CRF_CNT);

      IF V_TOTAL_CRF_CNT > 0 THEN
         BEGIN
            V_STATUSFLAG := 0;
            V_ACHIEVEMENT_DATE := pkg_dateutil.f_get_null_date_str;
            FOR I IN( SELECT DISTINCT CRF_PK
                        FROM ERV_PATSTUDY_EVE_CRF
                       WHERE PK_PATPROT = P_PATPROT
                         AND VISIT = P_VISIT
                         AND TO_DATE(TO_CHAR(CRF_CREATION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                         AND TO_DATE(TO_CHAR(CRF_CREATION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE)
            LOOP
               O_COUNT := 0;
               BEGIN
                  SELECT 1, CRF_STAT_DATE
                    INTO V_STATUSFLAG, V_ACHIEVEMENT_DATE
                    FROM ERV_PATSTUDY_EVE_CRF
                   WHERE PK_PATPROT = P_PATPROT
                     AND VISIT = P_VISIT
                     AND CRF_PK = I.CRF_PK
                     AND CRF_STAT_PK = (SELECT MAX (CRF_STAT_PK)
                                          FROM ERV_PATSTUDY_EVE_CRF
                                         WHERE PK_PATPROT = P_PATPROT
                                           AND VISIT = P_VISIT
                                           AND CRF_PK = I.CRF_PK
								   AND TO_DATE(TO_CHAR(CRF_STAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                                           AND TO_DATE(TO_CHAR(CRF_STAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE)
                     AND trim (CRF_STAT_CODELST_SUBTYP) = P_CRFSTAT_SUBTYP;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     V_STATUSFLAG := 0;
               END;


               O_COUNT := O_COUNT + V_STATUSFLAG;
               IF V_STATUSFLAG = 1 THEN
                  BEGIN
                     IF V_ACHIEVEMENT_DATE > O_ACHIEVEMENT_DATE THEN
                        O_ACHIEVEMENT_DATE := V_ACHIEVEMENT_DATE;
                     --added by Sonika on Sep 23, 2002.
                     --Achievement date should be null if the milestone has not been achieved
                     ELSE
                        O_ACHIEVEMENT_DATE := NULL;
                     END IF;
                     V_STATUSFLAG := 0;
                  END;
			ELSE
                  --added by Sonika on Sep 23, 2002.
                  --Achievement date should be null if the milestone has not been achieved
                  O_ACHIEVEMENT_DATE := NULL;
               END IF;
            END LOOP;
         END;
      ELSE
         O_COUNT := 0;
    --added by Sonika on Sep 13, 2002.
    --Achievement date should be null if the milestone has not been achieved
         O_ACHIEVEMENT_DATE := NULL;
      END IF;
   END SP_VIS_RULE_CRFS;

--------------------------------------------------------------------------------------------------------
--FORECAST
 PROCEDURE SP_FORE_VM_CNT_EVENTDONE_ONE (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/* forecast
   Purpose - Returns Count - Number of times VM_3 is achieved
              Returns an array of achievement dates
    VM_3 At least one event within the visit is marked as 'Done'
*/
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := NULL;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
	 V_CUR   TYPES.cursorType;
	 V_PATPROT NUMBER;

   BEGIN

     O_COUNT := 0;
     OPEN V_CUR FOR SELECT PK_PATPROT, MAX (ACTUAL_SCHDATE) SCH_ON
                  FROM SCH_EVENTS1 s, SCH_CODELST c ,  ERV_ALLPATSTUDYSTAT v
                  WHERE v.FK_STUDY = P_STUDY
                   AND VISIT = P_VISIT
                   AND FK_PROTOCOL = P_CAL
                   AND TO_DATE(TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= P_STDATE
                   AND TO_DATE(TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  <= P_ENDDATE
                   AND c.codelst_subtyp  <> 'ev_done'
			    AND v.PK_PATPROT = s.fk_patprot AND c.pk_codelst = s.isconfirmed
			    AND c.codelst_type = 'eventstatus'
                 GROUP BY PK_PATPROT ORDER BY 2 DESC ;
      LOOP
	  FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
       EXIT WHEN V_CUR%NOTFOUND;
         O_COUNT := O_COUNT + 1;
         V_DATEARRAY.EXTEND;
         V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
         P('sonika in vm rule forecount' || O_COUNT);
         P('sonika in vm rule foredate ' || V_ACHIEVEMENT_DATE);

      END LOOP;
    CLOSE V_CUR;
      O_ACHIEVEMENTDATES := V_DATEARRAY;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_FORE_VM_CNT_EVENTDONE_ONE;
-------------------------------------------------------------------------------------------------------------------------


 PROCEDURE SP_FORE_VIS_RULE_CRFS_CNT (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/* forecast
  Purpose - Rule: (VM_4 and VM_5)All visit associated CRFs marked as completed/submitted
*/
      V_ACHIEVEMENT_DATE   DATE                     := pkg_dateutil.f_get_null_date_str;
	 V_CUR   TYPES.cursorType;
      V_PATPROT NUMBER;
	 V_PER NUMBER;

   BEGIN

      O_ACHIEVEMENTDATES   := TYPES.SMALL_STRING_ARRAY ();
      O_COUNT := 0;
      OPEN V_CUR FOR SELECT PK_PATPROT, MAX (EVENT_ACTUAL_SCHDATE) SCH_ON
                FROM SCH_CRF crf, SCH_CODELST c ,  ERV_PATSTUDY_EVE v
                  WHERE FK_STUDY = P_STUDY
                   AND VISIT = P_VISIT
                   AND FK_PROTOCOL = P_CAL
                   AND c.codelst_subtyp  <> P_CRFSTAT_SUBTYP
			    AND v.SCH_EVENTS1_PK = crf.fk_events1 AND c.pk_codelst = crf.crf_curstat
			    AND c.codelst_type = 'crfstatus'
                   AND crf.crf_delflag <> 'Y'
                 GROUP BY PK_PATPROT ORDER BY 2 DESC;
     LOOP
        FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
        EXIT WHEN V_CUR%NOTFOUND;
         O_COUNT := O_COUNT + 1;
         O_ACHIEVEMENTDATES.EXTEND;
         O_ACHIEVEMENTDATES (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
    END LOOP;
    CLOSE V_CUR;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_FORE_VIS_RULE_CRFS_CNT;
------------------------------------------------------

END Pkg_Vm_Rule;
/


CREATE SYNONYM ESCH.PKG_VM_RULE FOR PKG_VM_RULE;


CREATE SYNONYM EPAT.PKG_VM_RULE FOR PKG_VM_RULE;


GRANT EXECUTE, DEBUG ON PKG_VM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_VM_RULE TO ESCH;

