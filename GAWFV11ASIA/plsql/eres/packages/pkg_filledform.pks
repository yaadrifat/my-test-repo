CREATE OR REPLACE PACKAGE ERES."PKG_FILLEDFORM"
AS
  PROCEDURE SP_GENERATE_PRINTXSL(p_formid NUMBER) ;

  PROCEDURE SP_GETPRINTFORM_HTML(p_formid NUMBER, p_filledform_id NUMBER, p_linkfrom STRING, o_xml OUT CLOB,o_xsl OUT CLOB) ;

  PROCEDURE SP_GETFLDPRINTXSL(p_datatype VARCHAR2, p_fldsystemid VARCHAR2, p_fldlinesno NUMBER, o_fldxsl IN OUT VARCHAR2);
  PROCEDURE SP_GET_PRN_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT VARCHAR2);
  PROCEDURE SP_INSERT_FORM_QUERY(p_filledform_id IN NUMBER, p_form_id IN NUMBER, p_called_from NUMBER, p_query_type NUMBER,
  p_query_type_id IN ARRAY_STRING,
  p_fldsys_ids IN ARRAY_STRING,  p_fld_notes IN ARRAY_STRING,
  p_query_status IN ARRAY_STRING, p_query_comments IN ARRAY_STRING,
  p_mode IN ARRAY_STRING, p_pk_query_status IN ARRAY_STRING, p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER);

  PROCEDURE SP_INSERT_QUERY_RESPONSE(p_filledform_id IN NUMBER, p_called_from NUMBER, p_fld_id NUMBER,
  p_query_type NUMBER, p_query_type_id NUMBER,
  p_query_status_id NUMBER,  p_fld_comments VARCHAR2,
  p_entered_on VARCHAR2, p_entered_by NUMBER,
  p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER);

  PROCEDURE SP_CLOSED_SYS_GEN_QUERY(pk_form_query NUMBER,p_filledform_id IN NUMBER, p_called_from NUMBER, p_fld_id NUMBER,
   p_query_type NUMBER, p_query_type_id NUMBER,
   p_query_status_id NUMBER,  p_fld_comments VARCHAR2,
   p_entered_on VARCHAR2, p_entered_by NUMBER,
   p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER);


/* returns 1: if user has acccess to form's filled responses, 0: if user does not have access to form's filled responses
p_form : the form
p_user : the loggedin user
 p_filledform_creator : the creator of filled form record
by Sonia Abrol, 12/14/05, to use in AdHoc reports
*/

FUNCTION fn_getUserAccess (p_form NUMBER,p_user NUMBER,p_filledform_creator NUMBER) RETURN NUMBER;

FUNCTION fn_formfld_validate(p_form_id NUMBER, p_response_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2,p_operator VARCHAR2 ) RETURN NUMBER;

FUNCTION fn_formfld_validate_partial(p_form_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2) RETURN varchar2 ;

END Pkg_Filledform;
/


CREATE OR REPLACE SYNONYM ESCH.PKG_FILLEDFORM FOR PKG_FILLEDFORM;


CREATE OR REPLACE SYNONYM EPAT.PKG_FILLEDFORM FOR PKG_FILLEDFORM;


GRANT EXECUTE, DEBUG ON PKG_FILLEDFORM TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FILLEDFORM TO ESCH;

