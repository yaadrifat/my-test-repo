CREATE OR REPLACE PACKAGE      PKG_DATEUTIL IS

  FUNCTION f_get_dateformat  return Varchar2;

  FUNCTION f_get_null_date_str return Varchar2;

  FUNCTION f_get_first_dayofyear return Date;

 FUNCTION f_get_first_dayofyear_str return Varchar;

  FUNCTION f_get_timeformat  return Varchar2;

  FUNCTION f_get_datetimeformat  return Varchar2;

  FUNCTION f_get_future_null_date_str return Varchar2;

END PKG_DATEUTIL;
/


CREATE SYNONYM EPAT.PKG_DATEUTIL FOR PKG_DATEUTIL;


CREATE SYNONYM ESCH.PKG_DATEUTIL FOR PKG_DATEUTIL;


GRANT EXECUTE, DEBUG ON PKG_DATEUTIL TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_DATEUTIL TO ESCH WITH GRANT OPTION;

