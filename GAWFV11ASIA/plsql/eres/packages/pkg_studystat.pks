CREATE OR REPLACE PACKAGE ERES.Pkg_Studystat
AS
PROCEDURE Change_stat    (
 p_study IN NUMBER ,
 p_per IN NUMBER,
 p_dmlby NUMBER ,
 p_ipadd VARCHAR2,
 p_event_exeon DATE );
PROCEDURE SP_COPYSTUDYVERSION (
   p_studyver IN NUMBER,
   p_newversion_number IN VARCHAR,p_version_date IN VARCHAR,p_version_cat IN VARCHAR,p_version_type IN VARCHAR,
   p_user IN NUMBER,
   p_vercreated OUT NUMBER);
PROCEDURE SP_CREATE_DEFAULT_VERSION (
  p_study IN NUMBER,
  p_user IN NUMBER,
  p_ip IN VARCHAR2);
PROCEDURE Sp_Copystudy_Selective(
  p_origstudy IN NUMBER,
  p_statflag IN NUMBER,
  p_teamflag IN NUMBER,
  p_dictflag IN NUMBER,
  p_verarr IN ARRAY_STRING,
  p_calarr IN ARRAY_STRING,
  p_txarr IN ARRAY_STRING,
  p_formsarr IN ARRAY_STRING,
  p_newstudynum IN VARCHAR2,
  p_newstudydm IN NUMBER,
  p_newstudytitle IN VARCHAR2,
  p_user IN NUMBER,
  p_ip IN VARCHAR2,
  o_success OUT NUMBER);
PROCEDURE SP_GET_PATIENT_RIGHT ( p_user IN NUMBER, p_group IN NUMBER, p_patient IN NUMBER, o_right OUT NUMBER);
PROCEDURE SP_GET_STUDY_RIGHT ( p_user IN NUMBER, p_group IN NUMBER, p_study IN NUMBER, o_right OUT NUMBER) ;
FUNCTION F_GET_USERRIGHTS_FOR_STUDY ( p_user IN NUMBER, p_study IN NUMBER) return VARCHAR2 ;
PROCEDURE SP_SET_ACTUAL_DATE  (p_study IN NUMBER,p_status IN VARCHAR2);
   PROCEDURE SP_NEWSTUDYVERSION (
   p_study IN NUMBER,
   p_version_number IN VARCHAR,
   p_version_notes IN VARCHAR2,p_version_date IN VARCHAR,p_version_cat IN VARCHAR,p_version_type IN VARCHAR,
   p_user IN NUMBER,
   p_vercreated OUT NUMBER);
   PROCEDURE SP_UPDATE_LOCAL_SAMPLE_SIZE (
   p_studysites IN Array_String,
   p_localsamples IN Array_String,
   p_user IN NUMBER,
   p_ip_Add IN VARCHAR2,
   p_ret OUT NUMBER);
   	   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_STUDYSTAT', pLEVEL  => Plog.LFATAL);

	   FUNCTION f_getLatestStudyStatus(p_study NUMBER) RETURN NUMBER;
	   	   FUNCTION f_getLatestStudyStatusPK(p_study NUMBER) RETURN NUMBER;

		   /* added by Sonia Abrol, 12/04/06
		   grants the budget/object share access to the new team user*/
 PROCEDURE sp_grant_studyteam_access(p_study IN NUMBER, p_user IN NUMBER,p_creator 	IN NUMBER,p_ipAdd IN VARCHAR2);

		 		   /* added by sonia abrol,12/04/06
				    revokes the budget/object share access from the deleted/deactivated team user*/
 PROCEDURE sp_revoke_studyteam_access(p_study IN NUMBER, p_user IN NUMBER);

 FUNCTION f_get_patientright(p_user NUMBER, p_group NUMBER, p_patient NUMBER ) RETURN NUMBER;

 FUNCTION f_get_viewPHI(p_user NUMBER, p_group NUMBER) RETURN NUMBER;

END Pkg_Studystat;
/


CREATE SYNONYM ESCH.PKG_STUDYSTAT FOR PKG_STUDYSTAT;


CREATE SYNONYM EPAT.PKG_STUDYSTAT FOR PKG_STUDYSTAT;


GRANT EXECUTE, DEBUG ON PKG_STUDYSTAT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_STUDYSTAT TO ESCH;

