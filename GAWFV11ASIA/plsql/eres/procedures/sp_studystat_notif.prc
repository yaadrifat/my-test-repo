create or replace
PROCEDURE SP_STUDYSTAT_NOTIF
AS
  --  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'sudhir', pLEVEL  => Plog.LDEBUG);
    CURSOR StudyCursor IS 

          SELECT MAX(es1.pk_StudyStat), esn1.FK_USER AS FK_USERID ,
          (es1.fk_study) ,
          (es1.FK_SITE) ,
          (es1.FK_CODELST_STUDYSTAT)                    AS codelstID,
          (e1.study_title)                              AS study_title,
          (e1.study_number)                             AS study_number,
          (F_Get_Codelstdesc(es1.FK_CODELST_STUDYSTAT)) AS status,
          (e1.STUDY_CTRP_REPORTABLE) ,
          MAX(es1.CREATED_ON)
        FROM er_studyStat es1 ,
          ER_STUDYSTAT_NOTIF esn1,
          er_study e1
        WHERE es1.FK_SITE = esn1.FK_SITEID
        and  es1.pk_StudyStat = (select  MAX(pk_StudyStat) from er_studyStat where fk_study = es1.fk_study)
        AND e1.fk_account = esn1.Fk_account
          --and esn1.FK_CODELST_STUDYSTAT = es1.fk_codelst_studystat
        AND TRUNC(es1.CREATED_ON ) >= TRUNC(to_date(
          (SELECT CTRL_CUSTOM_COL1 FROM er_ctrltab WHERE CTRL_KEY ='stdstatup'
          ),'MM/DD/YYYY HH:MI:SS'))
        AND e1.pk_study = es1.fk_study
        AND e1.STUDY_CTRP_REPORTABLE =1
        GROUP BY esn1.FK_USER, (es1.fk_study), (es1.FK_SITE), (es1.FK_CODELST_STUDYSTAT), 
        (e1.study_title), (e1.study_number), (F_Get_Codelstdesc(es1.FK_CODELST_STUDYSTAT)), 
        (e1.STUDY_CTRP_REPORTABLE) ;

        /*    Select esn1.FK_USER as FK_USERID ,MAX(es1.fk_study)  ,MAX(es1.FK_SITE) ,MAX(es1.FK_CODELST_STUDYSTAT) as codelstID,
            Max(e1.study_title) as study_title,Max(e1.study_number) as study_number,
            Max(F_Get_Codelstdesc(es1.FK_CODELST_STUDYSTAT)) as status,
            Max(e1.STUDY_CTRP_REPORTABLE) ,Max(esn1.fk_user)  ,
            Max(es1.CREATED_ON) 
            from er_studyStat es1 , ER_STUDYSTAT_NOTIF esn1, er_study e1
            where esn1.fk_user = es1.FK_USER_DOCBY
            --and esn1.FK_CODELST_STUDYSTAT = es1.fk_codelst_studystat             
            and trunc(es1.CREATED_ON ) >= trunc(to_date((select CTRL_CUSTOM_COL1 from er_ctrltab where CTRL_KEY ='stdstatup'),'MM/DD/YYYY HH:MI:SS'))
            and e1.pk_study = es1.fk_study
          --  and es1.fk_study = 11578
           -- and e1.pk_study = 11578
            and e1.STUDY_CTRP_REPORTABLE =1
            group by esn1.FK_USER ;*/
        
         StudyCursorIterator StudyCursor%ROWTYPE;
        
        MSGTXT clob ;
        MSG_SUBJECT VARCHAR2(200);
        r rowid;
        u rowid;
        FK_USERID number;
        USERID number;
       -- stdyId number;
      --  FK_SITEID number;
       -- new_PK_MSG number;
      --  STUDYID number;
      --  fkstudyid number;
        fk_codelstID VARCHAR2(200);
      --  codelstID number;
        CTRP_REPORTABLE number;
        STD_NUM	VARCHAR2(100);
        STD_TIT 	VARCHAR2(1000);
        STATUS	VARCHAR2(200);
     --   flag varchar2(10);
       lastExecutedDate date;
        
BEGIN

        BEGIN
          select TO_Date(CTRL_CUSTOM_COL1,'MM/DD/YYYY HH:MI:SS') into lastExecutedDate 
          from er_ctrltab where CTRL_KEY ='stdstatup';
          EXCEPTION WHEN NO_DATA_FOUND THEN
          lastExecutedDate := (sysdate-1);
        END ;
    --    plog.DEBUG(pctx, 'lastExecutedDate ' || lastExecutedDate);
       
       BEGIN
        SELECT MSGTXT_LONG,MSGTXT_SUBJECT INTO MSGTXT ,MSG_SUBJECT
        FROM SCH_MSGTXT WHERE MSGTXT_TYPE = 'stdstatup' ;
         EXCEPTION WHEN NO_DATA_FOUND THEN
          MSGTXT := null;
          MSG_SUBJECT := null;
        END ;
     --  plog.DEBUG(pctx, 'MSGTXT ' || MSGTXT || ' MSG_SUBJECT '|| MSG_SUBJECT);
       
      /*a cursor to walk through all the studies associated to one account */ 
    OPEN StudyCursor;
      LOOP
          FETCH StudyCursor INTO StudyCursorIterator;
          /* if no more studies, exit the loop*/
          EXIT WHEN StudyCursor%NOTFOUND;
          
          STD_NUM	:=StudyCursorIterator.study_number;
          STD_TIT :=StudyCursorIterator.study_title;
          STATUS	:=StudyCursorIterator.status; 
          fk_codelstID := StudyCursorIterator.codelstID; 
          FK_USERID:=StudyCursorIterator.FK_USERID; 
          
         
          -- plog.DEBUG(pctx, ' StudyCursorIterator.FK_USERID ' ||  FK_USERID);
          -- plog.DEBUG(pctx, ' StudyCursorIterator.study_number ' ||  STD_NUM);
          -- plog.DEBUG(pctx, ' StudyCursorIterator.study_title ' ||  STD_TIT);
          -- plog.DEBUG(pctx, ' StudyCursorIterator.status ' || STATUS);
          -- plog.DEBUG(pctx, ' FK_USERID ' ||  FK_USERID ||' USERID '||USERID);
          -- plog.DEBUG(pctx, ' fk_codelstID '||fk_codelstID);
            -- dbms_output.put_line('Sudhir Shukla ' || StudyCursorIterator.FK_USER);
          BEGIN
          select FK_USER into USERID from er_studystat_notif where FK_USER = FK_USERID and FK_CODELST_STUDYSTAT = fk_codelstID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
          USERID:=0;
          END;
            --start
       --     IF(NVL( StudyCursorIterator.fk_study,0) != 0) THEN
               IF(NVL( USERID,0) != 0) THEN
                      insert into SCH_DISPATCHMSG
                      (PK_MSG , MSG_SENDON , MSG_STATUS , MSG_TYPE ,  MSG_TEXT , CREATED_ON , FK_PAT  ,
                      FK_SCHEVENT , FK_PATPROT ,  MSG_ISCLUBBED ,  MSG_CLUBTEXT ,  MSG_SUBJECT  ,  MSG_ATTEMPTS )
                      values ((SCH_DISPATCHMSG_SEQ1.nextval+1),sysdate,0,'U',MSGTXT || '  Study Number '||STD_NUM ||' Title '||STD_TIT ||'Status Change '|| STATUS,sysdate,
                      FK_USERID,'',0,0,MSGTXT || '  Study Number '||STD_NUM ||' Title '||STD_TIT ||' Status Change '|| STATUS,MSG_SUBJECT , 0 )RETURNING rowid
                      INTO r;
          --            plog.DEBUG(pctx, 'Record Inserted '||r );
                      commit;                     
               END IF;
        --   END IF;
            --end
  
       END LOOP;
   CLOSE StudyCursor;
   BEGIN
    update ER_CTRLTAB set CTRL_CUSTOM_COL1 = TO_CHAR(SYSDATE, 'MM/DD/YYYY HH:MI:SS') where CTRL_KEY='stdstatup'
    RETURNING rowid
    INTO u;
--           plog.DEBUG(pctx, 'Record Updated '||u );
    commit; 
  END;
END;
/
CREATE SYNONYM ESCH.SP_STUDYSTAT_NOTIF FOR SP_STUDYSTAT_NOTIF;


CREATE SYNONYM EPAT.SP_STUDYSTAT_NOTIF FOR SP_STUDYSTAT_NOTIF;


GRANT EXECUTE, DEBUG ON SP_STUDYSTAT_NOTIF TO EPAT;

GRANT EXECUTE, DEBUG ON SP_STUDYSTAT_NOTIF TO ESCH;
