CREATE OR REPLACE PROCEDURE        "TEMP_UPDATE_CLOB" IS
  v_form_js clob;
     BEGIN
     v_form_js :=  'function disableFlds(formobj)
{
	var fldtext ;
	var fldChangeState = false;

	fldtext = formobj.fld_6955_17284.options[formobj.fld_6955_17284.selectedIndex].text;

	if (fldtext == ''Aortic valve replacement -> go to section C'')
	{
		formobj.fld_6957_17286[0].disabled = false;
		formobj.fld_6957_17286[1].disabled = false;
		formobj.fld_6957_17286[2].disabled = false;
		formobj.fld_6957_17286[3].disabled = false;
		formobj.fld_6957_17286[4].disabled = false;
		formobj.fld_6957_17286[5].disabled = false;
		
		formobj.fld_6958_17287.disabled = false;
		formobj.fld_6959_17288.disabled = false;
		formobj.fld_6961_17290.disabled = false;
		formobj.fld_6962_17291.disabled = false;
		formobj.fld_6964_17293.disabled = false;
		formobj.fld_6965_17294.disabled = false;
		formobj.fld_6966_17295.disabled = false;
		formobj.fld_6967_17296.disabled = false;
		formobj.fld_6968_17297.disabled = false;
		formobj.fld_6969_17298.disabled = false;
		formobj.fld_6970_17299.disabled = false;
		formobj.fld_6972_17301.disabled = false;
		formobj.fld_6974_17303.disabled = false;

		formobj.fld_6977_17306.disabled = true;
		formobj.fld_6978_17307.disabled = true;
		formobj.fld_6979_17308.disabled = true;
		formobj.fld_6980_17309.disabled = true;
		formobj.fld_6981_17310.disabled = true;
		formobj.fld_6983_17312.disabled = true;
		formobj.fld_6984_17313.disabled = true;
		formobj.fld_6985_17314.disabled = true;
		formobj.fld_6987_17316.disabled = true;
		formobj.fld_6988_17317.disabled = true;
		formobj.fld_6990_17319.disabled = true;
		formobj.fld_6991_17320.disabled = true;
		formobj.fld_6992_17321.disabled = true;
		formobj.fld_6993_17322.disabled = true;
		formobj.fld_6994_17323.disabled = true;
		formobj.fld_6995_17324.disabled = true;
		formobj.fld_6996_17325.disabled = true;
		formobj.fld_6997_17326.disabled = true;
		formobj.fld_6998_17327.disabled = true;
		formobj.fld_6999_17328.disabled = true;
	}
	else if (fldtext == ''Aortic valve sparing -> go to section D'')
	{
		formobj.fld_6957_17286[0].disabled = true;
		formobj.fld_6957_17286[1].disabled = true;
		formobj.fld_6957_17286[2].disabled = true;
		formobj.fld_6957_17286[3].disabled = true;
		formobj.fld_6957_17286[4].disabled = true;
		formobj.fld_6957_17286[5].disabled = true;
												
		formobj.fld_6958_17287.disabled = true;
		formobj.fld_6959_17288.disabled = true;
		formobj.fld_6961_17290.disabled = true;
		formobj.fld_6962_17291.disabled = true;
		formobj.fld_6964_17293.disabled = true;
		formobj.fld_6965_17294.disabled = true;
		formobj.fld_6966_17295.disabled = true;
		formobj.fld_6967_17296.disabled = true;
		formobj.fld_6968_17297.disabled = true;
		formobj.fld_6969_17298.disabled = true;
		formobj.fld_6970_17299.disabled = true;
		formobj.fld_6972_17301.disabled = true;
		formobj.fld_6974_17303.disabled = true;

		formobj.fld_6977_17306.disabled = false;
		formobj.fld_6978_17307.disabled = false;
		formobj.fld_6979_17308.disabled = false;
		formobj.fld_6980_17309.disabled = false;
		formobj.fld_6981_17310.disabled = false;
		formobj.fld_6983_17312.disabled = false;
		formobj.fld_6984_17313.disabled = false;
		formobj.fld_6985_17314.disabled = false;
		formobj.fld_6987_17316.disabled = false;
		formobj.fld_6988_17317.disabled = false;
		formobj.fld_6990_17319.disabled = false;
		formobj.fld_6991_17320.disabled = false;
		formobj.fld_6992_17321.disabled = false;
		formobj.fld_6993_17322.disabled = false;
		formobj.fld_6994_17323.disabled = false;
		formobj.fld_6995_17324.disabled = false;
		formobj.fld_6996_17325.disabled = false;
		formobj.fld_6997_17326.disabled = false;
		formobj.fld_6998_17327.disabled = false;
		formobj.fld_6999_17328.disabled = false;

	}
	else
	{
		formobj.fld_6957_17286[0].disabled = false;
		formobj.fld_6957_17286[1].disabled = false;
		formobj.fld_6957_17286[2].disabled = false;
		formobj.fld_6957_17286[3].disabled = false;
		formobj.fld_6957_17286[4].disabled = false;
		formobj.fld_6957_17286[5].disabled = false;
		
		formobj.fld_6958_17287.disabled = false;
		formobj.fld_6959_17288.disabled = false;
		formobj.fld_6961_17290.disabled = false;
		formobj.fld_6962_17291.disabled = false;
		formobj.fld_6964_17293.disabled = false;
		formobj.fld_6965_17294.disabled = false;
		formobj.fld_6966_17295.disabled = false;
		formobj.fld_6967_17296.disabled = false;
		formobj.fld_6968_17297.disabled = false;
		formobj.fld_6969_17298.disabled = false;
		formobj.fld_6970_17299.disabled = false;
		formobj.fld_6972_17301.disabled = false;
		formobj.fld_6974_17303.disabled = false;

		formobj.fld_6977_17306.disabled = false;
		formobj.fld_6978_17307.disabled = false;
		formobj.fld_6979_17308.disabled = false;
		formobj.fld_6980_17309.disabled = false;
		formobj.fld_6981_17310.disabled = false;
		formobj.fld_6983_17312.disabled = false;
		formobj.fld_6984_17313.disabled = false;
		formobj.fld_6985_17314.disabled = false;
		formobj.fld_6987_17316.disabled = false;
		formobj.fld_6988_17317.disabled = false;
		formobj.fld_6990_17319.disabled = false;
		formobj.fld_6991_17320.disabled = false;
		formobj.fld_6992_17321.disabled = false;
		formobj.fld_6993_17322.disabled = false;
		formobj.fld_6994_17323.disabled = false;
		formobj.fld_6995_17324.disabled = false;
		formobj.fld_6996_17325.disabled = false;
		formobj.fld_6997_17326.disabled = false;
		formobj.fld_6998_17327.disabled = false;
		formobj.fld_6999_17328.disabled = false;
	}
}' ;
 update er_formlib set  form_custom_js = v_form_js where  pk_formlib = 251;
 commit;
     END;
/


CREATE SYNONYM ESCH.TEMP_UPDATE_CLOB FOR TEMP_UPDATE_CLOB;


CREATE SYNONYM EPAT.TEMP_UPDATE_CLOB FOR TEMP_UPDATE_CLOB;


