CREATE OR REPLACE PROCEDURE        "STUDYTEAM_ACCESS_BY_USER" (p_from_user IN NUMBER, p_to_user IN NUMBER, p_studyteam_role IN VARCHAR2)
IS
v_studyteam_rights VARCHAR2(100);
v_rolepk NUMBER;
v_usersite NUMBER;
v_studysitecnt NUMBER;
v_teamcount NUMBER;
BEGIN

SELECT ctrl_value INTO v_studyteam_rights FROM ER_CTRLTAB WHERE ctrl_key = p_studyteam_role;
SELECT pk_codelst INTO v_rolepk FROM ER_CODELST WHERE codelst_type = 'role' AND codelst_subtyp = p_studyteam_role;

SELECT fk_siteid INTO v_usersite FROM ER_USER WHERE pk_user = p_to_user;

FOR i IN (SELECT fk_study AS pk_study
   FROM ER_STUDYTEAM 
   WHERE fk_user = p_from_user AND study_team_usr_type = 'D' AND  
   fk_study NOT IN (SELECT DISTINCT fk_study FROM ER_STUDYSTAT WHERE fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'prmnt_cls')))
LOOP
 
 SELECT COUNT(*) INTO v_teamcount FROM ER_STUDYTEAM WHERE fk_user =  p_to_user AND fk_study = i.pk_study;

 IF v_teamcount = 0 THEN
  INSERT INTO  ER_STUDY_SITE_RIGHTS (pk_study_site_rights,fk_site,fk_study,fk_user,user_study_site_rights)
  SELECT seq_er_studysite_rights.NEXTVAL,fk_site,i.pk_study,p_to_user,DECODE(usersite_right,0,0,1) AS user_study_site_rights 
  FROM ER_USERSITE WHERE fk_user = p_to_user;
  
  INSERT INTO ER_STUDYTEAM (pk_studyteam,fk_codelst_tmrole,fk_user,fk_study,study_team_rights,study_team_usr_type) VALUES
  (seq_er_studyteam.NEXTVAL, v_rolepk, p_to_user, i.pk_study, v_studyteam_rights, 'D');
 END IF;

 SELECT COUNT(*) INTO v_studysitecnt FROM ER_STUDYSITES WHERE fk_study = i.pk_study AND fk_site = v_usersite;
 
 IF v_studysitecnt  = 0 THEN
    INSERT INTO ER_STUDYSITES (pk_studysites,fk_study,fk_site) VALUES (seq_er_studysites.NEXTVAL, i.pk_study,v_usersite);
 END IF;

 
END LOOP;
END;
/


