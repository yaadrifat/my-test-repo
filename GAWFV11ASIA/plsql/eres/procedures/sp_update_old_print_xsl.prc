CREATE OR REPLACE PROCEDURE        "SP_UPDATE_OLD_PRINT_XSL" 
IS
 v_form_id number;
    BEGIN
/*
Stored procedure to update the printing XSL for old data
*/
for i in (select pk_formlib
        from er_formlib
	   where nvl(record_type,'Z') <> 'D')
loop
    v_form_id := i.pk_formlib;
    pkg_filledform.SP_GENERATE_PRINTXSL(v_form_id);
end loop;
commit;
END;
/


CREATE SYNONYM ESCH.SP_UPDATE_OLD_PRINT_XSL FOR SP_UPDATE_OLD_PRINT_XSL;


CREATE SYNONYM EPAT.SP_UPDATE_OLD_PRINT_XSL FOR SP_UPDATE_OLD_PRINT_XSL;


GRANT EXECUTE, DEBUG ON SP_UPDATE_OLD_PRINT_XSL TO EPAT;

GRANT EXECUTE, DEBUG ON SP_UPDATE_OLD_PRINT_XSL TO ESCH;

