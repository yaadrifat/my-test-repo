CREATE OR REPLACE PROCEDURE "SP_ADDUSERSTOMULTIPLESTUDIES" (
    p_userIdarr IN ARRAY_STRING,
    p_studyIdarr IN ARRAY_STRING,
    p_rolearr IN ARRAY_STRING,
    p_user IN NUMBER,
    p_ip IN VARCHAR2,
    o_ret OUT NUMBER)

AS

	v_userstr long;
	v_studystr long;
	v_userFound INTEGER;
	v_studyFound INTEGER;
	v_userTemp VARCHAR2(50);
	v_studyTemp VARCHAR2(50);
	v_counter INTEGER;
	v_role_rights VARCHAR2(25);
	v_ret NUMBER;

	v_status_modpk NUMBER;
	v_fk_codelst_stat NUMBER;
	v_siteid NUMBER;

	v_siteCounter NUMBER;

	v_study_site_counter NUMBER;
	v_STUDYSITE_ENRCOUNT NUMBER;
     v_account NUMBER;


BEGIN


	 v_ret := -1;


FOR f IN 1..p_rolearr.COUNT

LOOP--1


	 v_userstr := p_userIdarr(f)||',';

 LOOP--2//JM loop for user
   v_userFound := INSTR(v_userstr,',');
   v_studystr := p_studyIdarr(f)||',';
   IF v_studystr = ',' THEN
   	  EXIT;
   END IF;
   IF v_userFound = 0 OR v_userstr IS NULL OR v_userstr =''    THEN
        EXIT;
   ELSE--3
   v_userTemp := '0';
   v_userTemp := SUBSTR(v_userstr,1,v_userFound-1) ;


    LOOP--4 Loop for study
      v_studyFound := INSTR(v_studystr,',');
		     IF v_studyFound = 0 OR v_studystr IS NULL OR v_studystr =''  THEN
		          EXIT;
		     ELSE


			  v_studyTemp := '0';
			  v_studyTemp := SUBSTR(v_studystr,1,v_studyFound-1) ;


			  v_counter :=0 ;
		      --find the count of existing records for a particular user,study and role combination

			  -- modified by sonia abrol, 1. do not check on user role 2. put a check for
			  SELECT COUNT(*) INTO v_counter FROM ER_STUDYTEAM WHERE
			  FK_STUDY = TO_NUMBER(v_studyTemp)
--JM: 01Dec2006: Modified as per Bugzilla issue #2734, duplicate study team user should not be entered hen the STUDY_TEAM_USR_TYPE = 'X'
--			  and FK_USER = TO_NUMBER(v_userTemp) and STUDY_TEAM_USR_TYPE = 'D' ;
			  AND FK_USER = TO_NUMBER(v_userTemp) AND STUDY_TEAM_USR_TYPE IN ('D', 'X') ;

			 --if count = 0 then insert a record in the er_studyteam and er_status_history table
		       IF v_counter = 0 THEN

			   --role rights
			   --we dont need to get role_rights, it is handled in trigger ER_STUDYTEAM_BI_ROLE

			  /* SELECT CTRL_VALUE INTO v_role_rights FROM ER_CTRLTAB WHERE CTRL_KEY =
			   (select codelst_subtyp from er_codelst where pk_codelst=p_rolearr(f));*/


			   SELECT seq_er_studyteam.NEXTVAL INTO v_status_modpk FROM dual;

		        INSERT INTO ER_STUDYTEAM (
		        PK_STUDYTEAM, FK_CODELST_TMROLE, FK_USER, FK_STUDY,CREATOR,
		        CREATED_ON, IP_ADD,STUDY_TEAM_USR_TYPE,
		        STUDY_SITEFLAG, STUDYTEAM_STATUS)
				VALUES (v_status_modpk ,
				TO_NUMBER(p_rolearr(f)),TO_NUMBER(v_userTemp), TO_NUMBER(v_studyTemp), p_user,SYSDATE,
		              p_ip, 'D','', 'Active' );


				SELECT pk_codelst INTO v_fk_codelst_stat FROM ER_CODELST WHERE codelst_type='teamstatus' AND codelst_subtyp='Active';

				INSERT INTO ER_STATUS_HISTORY(
				PK_STATUS, STATUS_MODPK, STATUS_MODTABLE, FK_CODELST_STAT, STATUS_DATE,
				STATUS_END_DATE, STATUS_NOTES, STATUS_CUSTOM1, STATUS_ENTERED_BY,
				CREATOR, RECORD_TYPE, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON,
				IP_ADD, STATUS_ISCURRENT, STATUSENDDATE)
				VALUES (SEQ_ER_STATUS_HISTORY.NEXTVAL,v_status_modpk,'er_studyteam', v_fk_codelst_stat, SYSDATE,
				NULL, NULL, NULL, NULL,
				p_user, 'N', NULL, NULL, SYSDATE,
				p_ip,  1, NULL);

				----
				--insert a record into the er_studysites table too


	SELECT fk_siteid , fk_account INTO v_siteid , v_account FROM ER_USER WHERE pk_user=TO_NUMBER(v_userTemp);



			   SELECT COUNT(*) INTO v_study_site_counter FROM ER_STUDYSITES
			   WHERE FK_STUDY = TO_NUMBER(v_studyTemp)
			   AND FK_SITE = v_siteid;

				IF v_study_site_counter = 0 THEN

					INSERT INTO ER_STUDYSITES (
					PK_STUDYSITES, FK_STUDY, FK_SITE,
					FK_CODELST_STUDYSITETYPE, STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC,
					STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON,
					IP_ADD,STUDYSITE_ENRCOUNT,IS_REVIEWBASED_ENR, ENR_NOTIFYTO, ENR_STAT_NOTIFYTO

					)

					VALUES(SEQ_ER_STUDYSITES.NEXTVAL,
					TO_NUMBER(v_studyTemp), v_siteid, NULL, NULL,NULL,
					1, p_user, SYSDATE, NULL, NULL,
					p_ip, 0,NULL, NULL, NULL
					);



				END IF;


		-- added by sonia Abrol, 12/05/06 to create study site rights data
	Pkg_User.SP_CREATE_STUDYSITE_DATA   (     TO_NUMBER(v_userTemp),	 v_account ,	 TO_NUMBER(v_studyTemp) ,	 p_user ,	 p_ip ,	 v_ret )	;


			    v_ret := 1;
--JM: 01Dec06 else part added;
				ELSE
				v_ret := 0;
				END IF ;



		       v_studystr := SUBSTR(v_studystr,v_studyFound+1);
		     END IF;
    END LOOP;--4
   -----
   v_userstr := SUBSTR(v_userstr,v_userFound+1);
   END IF;--3
 END LOOP;--2//JM


END LOOP;--1

COMMIT;
o_ret := v_ret;

END   ;
/


