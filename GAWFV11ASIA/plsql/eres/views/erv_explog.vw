/* Formatted on 2/9/2010 1:39:17 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_EXPLOG
(
   PK_LOG,
   FK_REQLOG,
   LOG_DATETIME,
   LOG_SEVERITY,
   LOG_MESSAGECODE,
   LOG_DESC,
   LOG_MODULE,
   SITE_CODE
)
AS
   SELECT   ID PK_LOG,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 1, '[VEL]')
               FK_REQLOG,
            LDATE LOG_DATETIME,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 3, '[VEL]')
               LOG_SEVERITY,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 4, '[VEL]')
               LOG_MESSAGECODE,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 5, '[VEL]')
               LOG_DESC,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 6, '[VEL]')
               LOG_MODULE,
            PKG_UTIL.f_getValueFromDelimitedString (LTEXTE, 7, '[VEL]')
               SITE_CODE
     FROM   TLOG
    WHERE   LSECTION = 'A2A';


CREATE SYNONYM ESCH.ERV_EXPLOG FOR ERV_EXPLOG;


CREATE SYNONYM EPAT.ERV_EXPLOG FOR ERV_EXPLOG;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_EXPLOG TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_EXPLOG TO ESCH;

