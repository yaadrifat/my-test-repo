/* Formatted on 2/9/2010 1:39:44 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_USRPROFILE
(
   PK_USER,
   USR_LASTNAME,
   USR_FIRSTNAME,
   USR_WRKEXP,
   USR_PAHSEINV,
   USR_ACCOUNT,
   USR_STAT,
   USR_TYPE,
   USER_HIDDEN,
   SPL,
   JOB_TYPE,
   SITE_NAME,
   USER_ADDRESS,
   USER_CITY,
   USER_STATE,
   USER_ZIP,
   USER_COUNTRY,
   USER_PHONE,
   USER_EMAIL,
   ORGANIZATION_ADDRESS,
   ORGANIZATION_CITY,
   ORGANIZATION_STATE,
   ORGANIZATION_ZIP,
   ORGANIZATION_COUNTRY,
   ORGANIZATION_PHONE,
   ORGANIZATION_EMAIL,
   USR_TYPE_DESC,
   DEFAULT_GRP,
   USRLOGIN,
   USR_FULLNAME
)
AS
   SELECT   er_user.pk_user,
            er_user.usr_lastname,
            er_user.usr_firstname,
            er_user.usr_wrkexp,
            er_user.usr_pahseinv,
            er_user.fk_account usr_account,
            er_user.usr_stat usr_stat,
            er_user.usr_type usr_type,
            er_user.user_hidden,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = er_user.fk_codelst_spl)
               spl,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = er_user.fk_codelst_jobtype)
               job_type,
            er_site.site_name,
            er_add.address user_address,
            er_add.add_city user_city,
            er_add.add_state user_state,
            er_add.add_zipcode user_zip,
            er_add.add_country user_country,
            er_add.add_phone user_phone,
            er_add.add_email user_email,
            er_add1.address organization_address,
            er_add1.add_city organization_city,
            er_add1.add_state organization_state,
            er_add1.add_zipcode organization_zip,
            er_add1.add_country organization_country,
            er_add1.add_phone organization_phone,
            er_add1.add_email organization_email,
            DECODE (er_user.usr_type || er_user.usr_stat,
                    'NA',
                    'Non System User',
                    'SD',
                    'Deactivated User',
                    'SB',
                    'Blocked User',
                    'SA',
                    'Active Account User',
                    'ND',
                    'Deactivated Non System User')
               usr_type_desc,
            (SELECT   grp_name
               FROM   er_grps
              WHERE   er_user.fk_grp_default = er_grps.pk_grp)
               AS default_grp,
            usr_logname AS usrlogin,
            usr_firstname || ' ' || usr_lastname AS usr_fullname
     FROM   er_user,
            er_site,
            er_add,
            er_add er_add1
    WHERE       er_user.fk_siteid = er_site.pk_site
            AND er_user.fk_peradd = er_add.pk_add
            AND er_site.fk_peradd = er_add1.pk_add(+);


CREATE SYNONYM ESCH.ERV_USRPROFILE FOR ERV_USRPROFILE;


CREATE SYNONYM EPAT.ERV_USRPROFILE FOR ERV_USRPROFILE;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_USRPROFILE TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_USRPROFILE TO ESCH;

