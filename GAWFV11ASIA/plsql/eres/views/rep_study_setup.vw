/* Formatted on 2/9/2010 1:40:07 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_STUDY_SETUP
(
   STSTP_STUDYNUM,
   STSTP_AE_DICT,
   STSTP_PATIDGEN_TYPE,
   STSTP_PATIDGEN_FRMT,
   STSTP_STUDYCENTENR,
   STSTP_STUDYENRPAGE,
   STSTP_CNT_ARMS,
   STSTP_CNT_CALNDRS,
   STSTP_CNT_FORMS,
   FK_ACCOUNT,
   RID,
   STSTP_RESPONSE_ID,
   PK_STUDY,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE
)
AS
   SELECT   STUDY_NUMBER STSTP_STUDYNUM,
            NVL (DECODE (STUDY_ADVLKP_VER,
                         NULL, 'Free Text Entry',
                         0, 'Free Text Entry',
                         (SELECT   lkpview_name
                            FROM   er_lkpview
                           WHERE   pk_lkpview = STUDY_ADVLKP_VER)),
                 'Free Text Entry')
               STSTP_AE_DICT,
            DECODE (
               NVL (
                  (SELECT   SETTINGS_VALUE
                     FROM   ER_SETTINGS
                    WHERE       SETTINGS_KEYWORD = 'PATSTUDY_ID_GEN'
                            AND SETTINGS_MODNUM = PK_STUDY
                            AND SETTINGS_MODNAME = 3),
                  'manual'
               ),
               'manual',
               'Allow Manual Entry',
               'auto',
               'System-Generated sequential'
            )
               STSTP_PATIDGEN_TYPE,
            NVL (
               (SELECT   SETTINGS_VALUE
                  FROM   ER_SETTINGS
                 WHERE       SETTINGS_KEYWORD = 'PATSTUDY_ID_FORMAT'
                         AND SETTINGS_MODNUM = PK_STUDY
                         AND SETTINGS_MODNAME = 3),
               '--#####'
            )
               STSTP_PATIDGEN_FRMT,
            DECODE (
               NVL (
                  (SELECT   SETTINGS_VALUE
                     FROM   ER_SETTINGS
                    WHERE       SETTINGS_KEYWORD = 'STUDY_CENTRIC_ENROLL'
                            AND SETTINGS_MODNUM = PK_STUDY
                            AND SETTINGS_MODNAME = 3),
                  'N'
               ),
               'Y',
               'Yes',
               'N',
               'No'
            )
               STSTP_STUDYCENTENR,
            NVL (
               (SELECT   DECODE (SETTINGS_VALUE,
                                 NULL, '-',
                                 f_get_enroll_forward_page (SETTINGS_VALUE))
                  FROM   ER_SETTINGS
                 WHERE       SETTINGS_KEYWORD = 'STUDY_ENROLL_FORWARD'
                         AND SETTINGS_MODNUM = PK_STUDY
                         AND SETTINGS_MODNAME = 3),
               '-'
            )
               STSTP_STUDYENRPAGE,
            (SELECT   COUNT ( * )
               FROM   ER_STUDYTXARM
              WHERE   FK_STUDY = ER_STUDY.PK_STUDY)
               STSTP_CNT_ARMS,
            (SELECT   COUNT ( * )
               FROM   ESCH.EVENT_ASSOC
              WHERE       EVENT_TYPE = 'P'
                      AND CHAIN_ID = PK_STUDY
                      AND NVL (event_calassocto, 'P') = 'P')
               STSTP_CNT_CALNDRS,
            (SELECT   COUNT ( * )
               FROM   ER_LINKEDFORMS
              WHERE   FK_STUDY = PK_STUDY AND NVL (record_type, 'N') <> 'D')
               STSTP_CNT_FORMS,
            FK_ACCOUNT,
            RID,
            PK_STUDY STSTP_RESPONSE_ID,
            pk_study,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDY.CREATOR)
               CREATOR,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDY.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE
     FROM   ER_STUDY;


