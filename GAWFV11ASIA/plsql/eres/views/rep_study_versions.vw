/* Formatted on 2/9/2010 1:40:10 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_STUDY_VERSIONS
(
   STVER_STUDY_NUM,
   STVER_NUMBER,
   STVER_DATE,
   STVER_CATEGORY,
   STVER_TYPE,
   STVER_SECTION_CNT,
   STVER_ATTACHMENT_CNT,
   STVER_STATUS,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_ACCOUNT,
   RID,
   STVER_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               STVER_STUDY_NUM,
            STUDYVER_NUMBER STVER_NUMBER,
            TRUNC (STUDYVER_DATE) STVER_DATE,
            f_get_codelstdesc (STUDYVER_CATEGORY) STVER_CATEGORY,
            f_get_codelstdesc (STUDYVER_TYPE) STVER_TYPE,
            (SELECT   COUNT ( * )
               FROM   ER_STUDYSEC
              WHERE   FK_STUDYVER = PK_STUDYVER)
               STVER_SECTION_CNT,
            (SELECT   COUNT ( * )
               FROM   ER_STUDYAPNDX
              WHERE   FK_STUDYVER = PK_STUDYVER)
               STVER_ATTACHMENT_CNT,
            (SELECT   f_get_codelstdesc (FK_CODELST_STAT)
               FROM   ER_STATUS_HISTORY
              WHERE       PK_STUDYVER = STATUS_MODPK
                      AND STATUS_MODTABLE = 'er_studyver'
                      AND STATUS_END_DATE IS NULL
                      AND ROWNUM < 2)
               STVER_STATUS,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYVER.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYVER.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            (SELECT   FK_ACCOUNT
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               FK_ACCOUNT,
            RID,
            PK_STUDYVER STVER_RESPONSE_ID,
            fk_study
     FROM   ER_STUDYVER;


