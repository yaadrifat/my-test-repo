/* Formatted on 2/9/2010 1:39:53 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_EVENT_RESOURCE
(
   EVREC_EVENT,
   EVREC_USER,
   EVREC_RESOURCE,
   EVREC_DURATION,
   EVREC_NOTES,
   FK_ACCOUNT,
   RID,
   EVREC_RESPONSE_ID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON
)
AS
   SELECT   d.NAME EVREC_EVENT,
            DECODE (EVENTUSR_TYPE,
                    'U', (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
                            FROM   ER_USER
                           WHERE   PK_USER = EVENTUSR),
                    '')
               EVREC_USER,
            DECODE (EVENTUSR_TYPE,
                    'R', (SELECT   CODELST_DESC
                            FROM   ER_CODELST
                           WHERE   PK_CODELST = EVENTUSR),
                    '')
               EVREC_RESOURCE,
            EVENTUSR_DURATION EVREC_DURATION,
            EVENTUSR_NOTES EVREC_NOTES,
            USER_ID FK_ACCOUNT,
            u.RID,
            PK_EVENTUSR EVREC_RESPONSE_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = u.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = u.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            u.LAST_MODIFIED_DATE,
            u.CREATED_ON
     FROM   SCH_EVENTUSR u, EVENT_DEF d
    WHERE       EVENT_ID = FK_EVENT
            AND EVENTUSR IS NOT NULL
            AND event_type = 'E'
            AND (EVENTUSR_TYPE = 'U' OR EVENTUSR_TYPE = 'R');


