/* Formatted on 2/9/2010 1:39:18 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_FORMFLDS
(
   PK_FORMLIB,
   FORM_CATEGORY,
   FK_ACCOUNT,
   FORM_NAME,
   FORM_DESC,
   PK_FORMSEC,
   FORMSEC_NAME,
   FORMSEC_SEQ,
   FORMSEC_FMT,
   FORMSEC_REPNO,
   PK_FIELD,
   FORMFLD_SEQ,
   FLD_CHARSNO,
   FLD_CATEGORY,
   FLD_NAME,
   FLD_DESC,
   FLD_UNIQUEID,
   FLD_SYSTEMID,
   FLD_KEYWORD,
   FLD_TYPE,
   FLD_DATATYPE,
   FLD_REPEATFLAG,
   FLD_SAMELINE,
   FLD_DEFRESP,
   FLD_COLCOUNT,
   FLD_LINESNO,
   FORMFLD_XSL,
   FORMFLD_JAVASCR,
   FORMFLD_BROWSERFLG,
   PK_FORMFLD,
   FLD_ISVISIBLE,
   FLD_OVERRIDE_MANDATORY,
   FLD_OVERRIDE_DATE,
   FLD_OVERRIDE_FORMAT,
   FLD_OVERRIDE_RANGE,
   FORMSEC_OFFLINE,
   FORMFLD_OFFLINE,
   FLD_HIDELABEL,
   FLD_ALIGN,
   FLD_SORTORDER
)
AS
     SELECT   frm.pk_formlib pk_formlib,
              frm.fk_catlib form_category,
              frm.fk_account fk_account,
              frm.form_name form_name,
              frm.form_desc form_desc,
              sec.pk_formsec pk_formsec,
              sec.formsec_name formsec_name,
              sec.formsec_seq formsec_seq,
              sec.formsec_fmt formsec_fmt,
              sec.formsec_repno formsec_repno,
              frmfld.fk_field pk_field,
              frmfld.formfld_seq formfld_seq,
              fld.fld_charsno fld_charsno,
              fld.fk_catlib fld_category,
              fld.fld_name fld_name,
              fld.fld_desc fld_desc,
              fld.fld_uniqueid fld_uniqueid,
              fld.fld_systemid fld_systemid,
              fld.fld_keyword fld_keyword,
              fld.fld_type fld_type,
              fld.fld_datatype fld_datatype,
              fld.fld_repeatflag fld_repeatflag,
              fld.fld_sameline fld_sameline,
              NVL (fld.fld_defresp, '') fld_defresp,
              fld.fld_colcount fld_colcount,
              fld.fld_linesno fld_linesno,
              frmfld.formfld_xsl formfld_xsl,
              frmfld.formfld_javascr formfld_javascr,
              frmfld.formfld_browserflg,
              frmfld.pk_formfld,
              fld.fld_isvisible fld_isvisible,
              fld.fld_override_mandatory,
              fld.fld_override_date,
              fld.fld_override_format,
              fld.fld_override_range,
              sec.formsec_offline formsec_offline,
              frmfld.formfld_offline formfld_offline,
              fld.fld_hidelabel,
              fld.fld_align,
              fld.fld_sortorder
       FROM   ER_FORMLIB frm,
              ER_FORMSEC sec,
              ER_FORMFLD frmfld,
              ER_FLDLIB fld
      WHERE       frm.record_type <> 'D'
              AND frm.pk_formlib = sec.fk_formlib
              AND sec.record_type <> 'D'
              AND frmfld.fk_formsec = sec.pk_formsec
              AND frmfld.record_type <> 'D'
              AND fld.record_type <> 'D'
              AND frmfld.fk_field = fld.pk_field
              AND NVL (fld.fld_repeatflag, 0) = 0
   ORDER BY   formsec_seq, formfld_seq;


CREATE SYNONYM ESCH.ERV_FORMFLDS FOR ERV_FORMFLDS;


CREATE SYNONYM EPAT.ERV_FORMFLDS FOR ERV_FORMFLDS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_FORMFLDS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_FORMFLDS TO ESCH;

