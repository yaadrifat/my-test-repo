/* Formatted on 2/9/2010 1:39:29 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_PATSTUDY_EVEALL
(
   PATSTUDYSTAT_ID,
   PATSTUDYSTAT_DESC,
   FK_PER,
   PER_CODE,
   FK_STUDY,
   STUDY_NUMBER,
   STUDY_VER_NO,
   FK_ACCOUNT,
   PATSTUDYSTAT_DATE,
   PATPROT_ENROLDT,
   PK_PATPROT,
   PK_PATSTUDYSTAT,
   PER_SITE,
   SCH_EVENTS1_PK,
   EVENT_PROTOCOL_ID,
   EVENT_STATUS_ID,
   EVENT_START_DATE,
   EVENT_ID_ASSOC,
   EVENT_EXEON,
   EVENT_EXEBY,
   EVENT_ACTUAL_SCHDATE,
   VISIT,
   EVENT_STATUS_DESC,
   EVENT_STATUS_CODELST_SUBTYP,
   FK_PROTOCOL,
   EVENTSTAT_END_DATE,
   PK_EVENTSTAT,
   FK_VISIT,
   CREATED_ON,
   LAST_MODIFIED_DATE
)
AS
   SELECT   v.STATUS_ID,
            v.STATUS_DESC,
            v.FK_PER,
            v.PER_CODE,
            v.FK_STUDY,
            v.STUDY_NUMBER,
            v.STUDY_VER_NO,
            v.FK_ACCOUNT,
            v.PATSTUDYSTAT_DATE,
            v.PATPROT_ENROLDT,
            v.PK_PATPROT,
            v.PK_PATSTUDYSTAT,
            v.PER_SITE,
            s.EVENT_ID,
            s.SESSION_ID,
            ev.EVENTSTAT,
            s.START_DATE_TIME,
            s.FK_ASSOC,
            ev.EVENTSTAT_DT,
            s.EVENT_EXEBY,
            s.ACTUAL_SCHDATE,
            s.VISIT,
            c.CODELST_DESC,
            c.CODELST_SUBTYP,
            v.fk_protocol,
            ev.EVENTSTAT_ENDDT,
            ev.PK_EVENTSTAT,
            s.fk_visit,
            ev.created_on,
            ev.last_modified_date
     FROM   ERV_ALLPATSTUDYSTAT v,
            SCH_EVENTS1 s,
            SCH_CODELST c,
            sch_eventstat ev
    WHERE       v.PK_PATPROT = s.fk_patprot
            AND s.EVENT_ID = ev.FK_EVENT
            AND c.pk_codelst = ev.EVENTSTAT
            AND c.codelst_type = 'eventstatus';


CREATE SYNONYM ESCH.ERV_PATSTUDY_EVEALL FOR ERV_PATSTUDY_EVEALL;


CREATE SYNONYM EPAT.ERV_PATSTUDY_EVEALL FOR ERV_PATSTUDY_EVEALL;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSTUDY_EVEALL TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSTUDY_EVEALL TO ESCH;

