/* Formatted on 2/9/2010 1:39:34 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDY
(
   STUDY_NUMBER,
   STUDY_TITLE,
   PRI_INVESTIGATOR,
   PRI_INV_NAME,
   STUDY_OTHERPRINV,
   PI_COMB,
   STUDY_PARTCNTR,
   STUDY_AUTHOR,
   TA,
   STUDY_PUBFLAG,
   FK_CODELST_TAREA,
   PK_STUDY,
   PK_USER,
   FK_ACCOUNT,
   CREATED_ON,
   STUDY_ACTUALDT,
   AUTHOR_SITE,
   STUDY_END_DATE,
   FK_AUTHOR
)
AS
   SELECT   ER_STUDY.study_number,
            ER_STUDY.study_title,
            ER_STUDY.study_prinv pri_investigator,
            (SELECT   ER_USER.usr_firstname || ' ' || ER_USER.usr_lastname
               FROM   ER_USER
              WHERE   ER_USER.pk_user = ER_STUDY.study_prinv)
               pri_inv_name,
            ER_STUDY.study_otherprinv,
            DECODE (
               ER_STUDY.study_prinv,
               NULL,
               DECODE (study_otherprinv, NULL, '', study_otherprinv),
               (SELECT   usr_firstname || ' ' || usr_lastname
                  FROM   ER_USER
                 WHERE   pk_user = study_prinv)
               || DECODE (study_otherprinv,
                          NULL, '',
                          '; ' || study_otherprinv)
            )
               AS pi_comb,
            ER_STUDY.study_partcntr,
            ER_USER.usr_firstname || ' ' || ER_USER.usr_lastname study_author,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = ER_STUDY.fk_codelst_tarea)
               ta,
            ER_STUDY.study_pubflag,
            ER_STUDY.fk_codelst_tarea,
            ER_STUDY.pk_study,
            ER_USER.pk_user,
            ER_STUDY.fk_account,
            ER_STUDY.created_on,
            ER_STUDY.study_actualdt,
            ER_SITE.site_name author_site,
            ER_STUDY.study_end_date,
            ER_STUDY.fk_author
     FROM   ER_STUDY, ER_USER, ER_SITE
    WHERE       ER_USER.pk_user = ER_STUDY.fk_author
            AND ER_USER.fk_siteid = ER_SITE.pk_site
            AND ER_STUDY.study_verparent IS NULL;


CREATE SYNONYM ESCH.ERV_STUDY FOR ERV_STUDY;


CREATE SYNONYM EPAT.ERV_STUDY FOR ERV_STUDY;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY TO ESCH;

