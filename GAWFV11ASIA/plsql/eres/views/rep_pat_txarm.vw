/* Formatted on 2/9/2010 1:40:03 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_PAT_TXARM
(
   PTARM_PAT_ID,
   PTARM_PAT_STUDY_ID,
   PTARM_TREATMT_NAME,
   PTARM_DRUG_INFO,
   PTARM_STATUS_DATE,
   PTARM_END_DATE,
   PTARM_NOTES,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   PTARM_RESPONSE,
   FK_STUDY,
   FK_PER,
   STUDY_NUMBER
)
AS
   SELECT   PER_CODE PTARM_PAT_ID,
            PATPROT_PATSTDID PTARM_PAT_STUDY_ID,
            (SELECT   TX_NAME
               FROM   ER_STUDYTXARM
              WHERE   PK_STUDYTXARM = FK_STUDYTXARM)
               PTARM_TREATMT_NAME,
            TX_DRUG_INFO PTARM_DRUG_INFO,
            TX_START_DATE PTARM_STATUS_DATE,
            TX_END_DATE PTARM_END_DATE,
            NOTES PTARM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_PATTXARM.CREATOR)
               CREATOR,
            ER_PATTXARM.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_PATTXARM.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_PATTXARM.LAST_MODIFIED_DATE,
            FK_ACCOUNT,
            ER_PATTXARM.RID,
            PK_PATTXARM PTARM_RESPONSE,
            fk_study,
            fk_per,
            (SELECT   study_number
               FROM   er_study
              WHERE   pk_study = fk_study)
               study_number
     FROM   ER_PATTXARM, ER_PATPROT, ER_PER
    WHERE   PK_PATPROT = FK_PATPROT AND pk_per = fk_per;


