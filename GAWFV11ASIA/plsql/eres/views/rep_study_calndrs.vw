/* Formatted on 2/9/2010 1:40:05 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_STUDY_CALNDRS
(
   STCAL_STUDYNUM,
   STCAL_NAME,
   STCAL_DESC,
   STCAL_STATUS,
   CREATED_ON,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   STCAL_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   (SELECT   STUDY_NUMBER
               FROM   ERES.ER_STUDY
              WHERE   PK_STUDY = CHAIN_ID)
               STCAL_STUDYNUM,
            "NAME" STCAL_NAME,
            DESCRIPTION STCAL_DESC,
            --JM: 08Feb2011, D-FIN9
	        --F_GET_CSTAT (where FK_CODELST_CALSTAT) STCAL_STATUS,
	        ( select CODELST_DESC  from sch_codelst where PK_CODELST=FK_CODELST_CALSTAT) STCAL_STATUS,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = EVENT_ASSOC.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = EVENT_ASSOC.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            (SELECT   FK_ACCOUNT
               FROM   ERES.ER_STUDY
              WHERE   PK_STUDY = CHAIN_ID)
               fk_ACCOUNT,
            RID,
            EVENT_ID STCAL_RESPONSE_ID,
            chain_id fk_study
     FROM   ESCH.EVENT_ASSOC
    WHERE   EVENT_TYPE = 'P';


