/* Formatted on 2/9/2010 1:39:58 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_PAT_DEMOGRAPHICS
(
   PTDEM_PAT_ID,
   PTDEM_SURVIVSTAT,
   PTDEM_DEATHDT,
   PTDEM_DEATHCAUSE,
   PTDEM_DEATHSPECS,
   PTDEM_FIRSTNAME,
   PTDEM_MIDNAME,
   PTDEM_LASTNAME,
   PTDEM_BIRTHDATE,
   PTDEM_GENDER,
   PTDEM_MARSTAT,
   PTDEM_BLOODGRP,
   PTDEM_SSN,
   PTDEM_EMAIL,
   PTDEM_PRI_ETHNY,
   PTDEM_PRI_RACE,
   PTDEM_ADD_ETHNY,
   PTDEM_ADD_RACE,
   PTDEM_ADD1,
   PTDEM_ADD2,
   PTDEM_CITY,
   PTDEM_STATE,
   PTDEM_COUNTY,
   PTDEM_ZIP,
   PTDEM_COUNTRY,
   PTDEM_HOMEPHONE,
   PTDEM_WORKPHONE,
   PTDEM_PRI_SITE,
   FK_SITE,
   PTDEM_TIMEZONE,
   PTDEM_EMPLOY,
   PTDEM_EDUCATION,
   PTDEM_NOTES,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   PTDEM_ACCOUNT,
   RID,
   PTDEM_RESPONSE_ID,
   FK_PER,
   PTDEM_PHYOTHER,
   PTDEM_SPLACCESS,
   PTDEM_REGBY,
   PTDEM_REGDATE
)
AS
   SELECT   PERSON_CODE PTDEM_PAT_ID,
            F_GET_CODELSTDESC (FK_CODELST_PSTAT) PTDEM_SURVIVSTAT,
            PERSON_DEATHDT PTDEM_DEATHDT,
            F_GET_CODELSTDESC (FK_CODELST_PAT_DTH_CAUSE) PTDEM_DEATHCAUSE,
            CAUSE_OF_DEATH_OTHER PTDEM_DEATHSPECS,
            PERSON_FNAME PTDEM_FIRSTNAME,
            PERSON_MNAME PTDEM_MIDNAME,
            PERSON_LNAME PTDEM_LASTNAME,
            PERSON_DOB PTDEM_BIRTHDATE,
            F_GET_CODELSTDESC (FK_CODELST_GENDER) PTDEM_GENDER,
            F_GET_CODELSTDESC (FK_CODELST_MARITAL) PTDEM_MARSTAT,
            F_GET_CODELSTDESC (FK_CODELST_BLOODGRP) PTDEM_BLOODGRP,
            PERSON_SSN PTDEM_SSN,
            PERSON_EMAIL PTDEM_EMAIL,
            F_GET_CODELSTDESC (FK_CODELST_ETHNICITY) PTDEM_PRI_ETHNY,
            F_GET_CODELSTDESC (FK_CODELST_RACE) PTDEM_PRI_RACE,
            PERSON_ADD_ETHNICITY PTDEM_ADD_ETHNY,
            PERSON_ADD_RACE PTDEM_ADD_RACE,
            PERSON_ADDRESS1 PTDEM_ADD1,
            PERSON_ADDRESS2 PTDEM_ADD2,
            PERSON_CITY PTDEM_CITY,
            PERSON_STATE PTDEM_STATE,
            PERSON_COUNTY PTDEM_COUNTY,
            PERSON_ZIP PTDEM_ZIP,
            PERSON_COUNTRY PTDEM_COUNTRY,
            PERSON_HPHONE PTDEM_HOMEPHONE,
            PERSON_BPHONE PTDEM_WORKPHONE,
            (SELECT   SITE_NAME
               FROM   ER_SITE
              WHERE   PK_SITE = FK_SITE)
               PTDEM_PRI_SITE,
            fk_site,
            (SELECT   TZ_NAME
               FROM   ESCH.SCH_TIMEZONES
              WHERE   PK_TZ = FK_TIMEZONE)
               PTDEM_TIMEZONE,
            F_GET_CODELSTDESC (FK_CODELST_EMPLOYMENT) PTDEM_EMPLOY,
            F_GET_CODELSTDESC (FK_CODELST_EDU) PTDEM_EDUCATION,
            PERSON_NOTES PTDEM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = PERSON.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            FK_ACCOUNT PTDEM_ACCOUNT,
            RID,
            PK_PERSON PTDEM_RESPONSE_ID,
            PK_PERSON FK_PER,
            PERSON_PHYOTHER PTDEM_PHYOTHER,
            pkg_util.f_getCodeValues (PERSON_SPLACCESS, ',', ';')
               AS PTDEM_SPLACCESS,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   pk_user = PERSON_REGBY)
               PTDEM_REGBY,
            PERSON_REGDATE PTDEM_REGDATE
     FROM   EPAT.PERSON;


