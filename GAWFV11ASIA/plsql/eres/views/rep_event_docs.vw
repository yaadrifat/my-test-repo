/* Formatted on 2/9/2010 1:39:52 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_EVENT_DOCS
(
   EVAPX_EVENT,
   EVAPX_URL_FILE,
   EVAPX_SHORT_DESC,
   FK_ACCOUNT,
   RID,
   EVAPX_RESPONSE_ID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON
)
AS
   SELECT   ev.NAME EVAPX_EVENT,
            d.DOC_NAME EVAPX_URL_FILE,
            d.DOC_DESC EVAPX_SHORT_DESC,
            ev.USER_ID FK_ACCOUNT,
            d.RID,
            d.PK_DOCS EVAPX_RESPONSE_ID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = d.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = d.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            d.LAST_MODIFIED_DATE,
            d.CREATED_ON
     FROM   SCH_EVENTDOC E, EVENT_DEF ev, SCH_DOCS D
    WHERE       E.PK_DOCS = D.PK_DOCS
            AND EVENT_ID = FK_EVENT
            AND event_type = 'E';


