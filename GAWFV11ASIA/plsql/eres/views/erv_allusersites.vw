/* Formatted on 2/9/2010 1:39:14 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_ALLUSERSITES
(
   PK_SITE,
   SITE_NAME,
   FK_USER,
   FK_STUDY,
   SITE_HIDDEN
)
AS
   SELECT   u.fk_site,
            site_name,
            u.fk_user,
            T.fk_study,
            site_hidden
     FROM   ER_USERSITE u, ER_STUDYTEAM T, ER_SITE
    WHERE       u.fk_site = pk_site
            AND u.fk_user = T.fk_user
            AND usersite_right >= 4
            AND NVL (study_team_usr_type, 'Y') = 'S'
   UNION
   SELECT   s.fk_site,
            site_name,
            T.fk_user,
            T.fk_study,
            site_hidden
     FROM   ER_STUDY_SITE_RIGHTS r,
            ER_STUDYSITES s,
            ER_STUDYTEAM T,
            ER_SITE
    WHERE       user_study_site_rights = 1
            AND pk_site = r.fk_site
            AND r.fk_site = s.fk_site
            AND r.fk_study = s.fk_study
            AND T.fk_user = r.fk_user
            AND T.fk_study = r.fk_study
            AND NVL (study_team_usr_type, 'Y') IN ('D', 'S')
   UNION
   SELECT   PK_SITE,
            SITE_NAME,
            FK_USER,
            NULL,
            site_hidden
     FROM   ER_SITE, ER_USERSITE
    WHERE   PK_SITE = FK_SITE AND usersite_right >= 4;


