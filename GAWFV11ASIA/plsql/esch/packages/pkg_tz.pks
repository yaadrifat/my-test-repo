CREATE OR REPLACE PACKAGE        "PKG_TZ" 
AS

FUNCTION f_DB2GMT return date ;

FUNCTION f_DB2GMT (p_userid number , p_usrtype varchar2 )
return date ;

FUNCTION f_getmaxtz(p_usrlst varchar )  return date ;

END PKG_tz;
/


CREATE SYNONYM ERES.PKG_TZ FOR PKG_TZ;


CREATE SYNONYM EPAT.PKG_TZ FOR PKG_TZ;


GRANT EXECUTE, DEBUG ON PKG_TZ TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_TZ TO ERES;

