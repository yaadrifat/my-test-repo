CREATE OR REPLACE PACKAGE BODY        "PKG_TZ" 
AS

FUNCTION f_DB2GMT
RETURN DATE IS
v_codelst NUMBER ;
v_dbGMT DATE ;
BEGIN
 BEGIN
 SELECT CODELST_DESC
  INTO v_codelst
  FROM sch_codelst
 WHERE UPPER(codelst_type) = 'DBTIMEZONE' ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   v_codelst := -480 ; -- Pacific Standard Time
 END ;
SELECT SYSDATE - (1/1440* V_codelst)
 INTO v_dbgmt
 FROM dual;

RETURN v_dbgmt;

END f_DB2GMT;


FUNCTION f_DB2GMT (p_userid NUMBER , p_usrtype VARCHAR2 )
RETURN DATE IS
v_tz NUMBER ;
v_dbGMT DATE ;
BEGIN
 BEGIN
 IF UPPER(p_usrType) = 'U' OR  UPPER(p_usrType) = 'R' OR UPPER(p_usrType) = 'UC' OR UPPER(p_usrType) = 'UN'
 OR UPPER(p_usrType) = 'UR' THEN

 SELECT st.TZ_VALUE
  INTO v_tz
  FROM sch_timezones st , er_user eu
 WHERE eu.pk_user = p_userid
   AND st.pk_tz = eu.fk_timezone   ;
 ELSIF UPPER(p_usrType) = 'P' OR UPPER(p_usrType) = 'PC' THEN
  SELECT st.TZ_VALUE
  INTO v_tz
  FROM sch_timezones st , person ep
 WHERE ep.pk_person = p_userid
   AND st.pk_tz = ep.fk_timezone   ;
 END IF ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   v_tz := -480 ; -- Pacific Standard Time
 END ;

SELECT f_DB2GMT() + (1/1440* V_tz)
 INTO v_dbgmt
 FROM dual;

 p(v_dbgmt);

RETURN v_dbgmt;

END f_DB2GMT ;


FUNCTION f_getmaxtz(p_usrlst VARCHAR )
 RETURN DATE IS
 v_retdate DATE ;
 v_sql VARCHAR2(1000) ;
 BEGIN
 IF p_usrlst IS NOT NULL THEN
  v_sql :=
   'select max(pkg_tz.f_db2gmt() + (1/1440 * tz_value) )
    FROM er_user, sch_timezones
    WHERE FK_TIMEZONE = pk_tz
    AND pk_user IN ( ' ||p_usrlst || ')' ;
    EXECUTE IMMEDIATE v_sql INTO v_retdate  ;
    IF v_retdate IS NULL THEN
      v_retdate := SYSDATE ;
    END IF ;
  END IF ;
  RETURN v_retdate;
END f_getmaxtz;


/*FUNCTION f_DB2GMT (p_userid number , p_usrtype varchar2, p_msgdate date )
return date is
v_tz number ;
v_dbGMT date ;
begin
 Begin
 If upper(p_usrType) = 'U' then

 select st.TZ_VALUE
  into v_tz
  from sch_timezones st , er_user eu
 where eu.pk_user = p_userid
   and st.pk_tz = eu.fk_timezone   ;
 elsif upper(p_usrType) = 'P' then
  select st.TZ_VALUE
  into v_tz
  from sch_timezones st , person ep
 where ep.pk_person = p_userid
   and st.pk_tz = ep.fk_timezone   ;
 End if ;
 Exception When no_Data_found then
   v_tz := -480 ; -- Pacific Standard Time
 End ;

select p_msgdate + (1/1440* V_tz)
 into v_dbgmt
 from dual;

 p(v_dbgmt);

return v_dbgmt;

end f_DB2GMT ; */

END PKG_tz ;
/


CREATE OR REPLACE SYNONYM ERES.PKG_TZ FOR PKG_TZ;


CREATE OR REPLACE SYNONYM EPAT.PKG_TZ FOR PKG_TZ;


GRANT EXECUTE, DEBUG ON PKG_TZ TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_TZ TO ERES;