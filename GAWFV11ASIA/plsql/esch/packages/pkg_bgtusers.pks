CREATE OR REPLACE PACKAGE        "PKG_BGTUSERS" 
AS

  PROCEDURE sp_addact_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_ACCT IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;


  PROCEDURE sp_delete_bgtusers (
     P_BUDGET IN NUMBER,
     USER_TYPE IN Varchar2 ) ;

  PROCEDURE sp_addsite_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_SITE IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;

  PROCEDURE sp_addteam_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_STUDY IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;

  PROCEDURE sp_update_rights (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     USER_TYPE IN Varchar2,
	P_lastmod_by in number ,
     P_lastmod_date in date ,
     P_IPADD in varchar2
     ) ;

   END PKG_BGTUSERS;
/


CREATE SYNONYM ERES.PKG_BGTUSERS FOR PKG_BGTUSERS;


CREATE SYNONYM EPAT.PKG_BGTUSERS FOR PKG_BGTUSERS;


GRANT EXECUTE, DEBUG ON PKG_BGTUSERS TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_BGTUSERS TO ERES;

