CREATE OR REPLACE PACKAGE  "ESCH"."PKG_BGT"
AS
  PROCEDURE sp_study_bgtsection (
     P_BUDGET IN NUMBER,
     P_DMLBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2) ;
   PROCEDURE sp_pat_bgtsection (
	P_PROTOCOL IN NUMBER,
	P_PKBGTCAL IN NUMBER,
	P_PROTTYPE IN VARCHAR2,
	P_DMLBY  IN   NUMBER,
	P_IPADD  IN   VARCHAR2,
	P_SOURCE IN VARCHAR2
	) ;
   PROCEDURE sp_pat_misc (
     P_PKBGTCAL IN NUMBER,
     P_DMLBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
 P_SEQ IN NUMBER
) ;
   PROCEDURE sp_update_bgtcal_children (
     P_PKBGTCAL IN NUMBER,
     P_DMBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    ) ;
   PROCEDURE sp_update_budgetTemplate (
     P_PKBGTCAL IN NUMBER,
     P_Modified_by  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    ) ;
   PROCEDURE sp_update_budget_children (
     P_PKBGT IN NUMBER,
     P_Modified_by  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    ) ;
   PROCEDURE sp_copy_budget (
     P_PKOLDBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
 P_NEWPKBGT IN NUMBER,
 P_TEMPFLAG IN CHAR,
 P_RETURN OUT NUMBER
    ) ;
   PROCEDURE sp_check_duplicate (
     P_ACCID IN NUMBER,
     P_NEWBGTNAME  IN   VARCHAR2,
     P_NEWBGTVER  IN   VARCHAR2,
     P_MODE  IN   VARCHAR2,
 P_BGTPK IN NUMBER,
 P_RETURN OUT NUMBER
    ) ;
   PROCEDURE sp_get_apndxused (
     P_ACCID IN NUMBER,
 P_RETURN OUT NUMBER
    ) ;
    PROCEDURE sp_poststudybgt (
     P_PKBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    ) ;
 PROCEDURE sp_new_budget(
 P_PKOLDBGT IN NUMBER,
 P_NEWBGTNAME  IN   VARCHAR2,
 P_NEWBGTVER  IN   VARCHAR2,
    P_CREATOR  IN   NUMBER,
 P_IPADD  IN   VARCHAR2,
 P_RETURN OUT NUMBER
 );
 PROCEDURE sp_create_copy_budget(
 p_old_budget IN NUMBER,
 p_new_budname IN VARCHAR2,
 p_new_budver IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER
 );
 PROCEDURE sp_insert_repeat_line_items
 (p_budgetcal_id IN NUMBER,
 p_lineitem_names IN Array_String,
 p_lineitem_descs IN Array_String,
 p_lineitem_cpts IN Array_String,
 p_lineitem_repeat IN Array_String,
 p_category IN ARRAY_STRING,
 p_tmid IN ARRAY_STRING,
 p_cdm IN ARRAY_STRING,
 p_creator IN NUMBER,
 p_ip_Add VARCHAR2,
 o_ret OUT NUMBER);
 PROCEDURE SP_INSERT_PERSONNEL_COST(
 p_budgetcal_id NUMBER,
 p_per_types IN ARRAY_STRING,
 p_rates IN ARRAY_STRING,
 p_inclusions IN ARRAY_STRING,
 p_notes IN ARRAY_STRING,
 p_category IN ARRAY_STRING,
 p_tmid IN ARRAY_STRING,
 p_cdm IN ARRAY_STRING,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 o_ret_number OUT NUMBER);
 PROCEDURE sp_update_personnel_cost(
 p_budgetcal_id IN NUMBER,
 p_parent_ids IN Array_String,
 p_personnel_types IN Array_String,
 p_rates IN Array_String,
 p_inclusions IN Array_String,
 p_notes IN Array_String,
 p_apply_types IN Array_String,
 p_category IN ARRAY_STRING,
 p_tmid IN ARRAY_STRING,
 p_cdm IN ARRAY_STRING,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 o_ret_number OUT NUMBER);
 PROCEDURE sp_delete_lineitem_per_cost(
 p_lineitem_id IN NUMBER,
 p_apply_type IN NUMBER,
 p_last_modified_by IN NUMBER,
 p_ip_Add IN VARCHAR2,
 o_ret OUT NUMBER);


     PROCEDURE sp_copy_base_sections (
     P_BUDGET IN NUMBER,
     P_PKBGTCAL IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
 P_SEQ    IN OUT NUMBER,p_commitflag In Number
    ) ;
 PROCEDURE sp_insert_default_sec_lineitms(
 p_section_id IN NUMBER,
 p_bgtcal_id IN NUMBER,
 p_creator IN NUMBER,
 p_ip_add VARCHAR2,
 o_ret OUT NUMBER);
 PROCEDURE sp_insert_def_lineitms_commit(
 p_commit IN CHAR,
 p_section_id IN NUMBER,
 p_bgtcal_id IN NUMBER,
 p_creator IN NUMBER,
 p_ip_add VARCHAR2,
 o_ret OUT NUMBER);
 PROCEDURE sp_update_repeat_line_items(
 p_budgetcal_id IN NUMBER ,
    p_parent_ids IN Array_String,
 p_lineitem_names IN Array_String,
 p_lineitem_descs IN Array_String,
 p_lineitem_cpts IN Array_String,
 p_lineitem_repeat IN Array_String,
 p_apply_types IN Array_String,
 p_category IN Array_String,
 p_tmid IN Array_String,
 p_cdm IN Array_String,
 p_creator IN NUMBER,
 p_ip_Add VARCHAR2,
 o_ret OUT NUMBER);
 PROCEDURE sp_apply_special_bgtsections (P_BUDGET IN NUMBER, P_PKBGTCAL IN NUMBER,
     P_MAPPING_TABLE IN tab_mapping,
      P_CREATOR  IN   NUMBER,
      P_IPADD  IN   VARCHAR2,
   P_SUCCESS   IN OUT NUMBER
 );
 PROCEDURE sp_clear_subTotals( p_lineitem_ids IN ARRAY_STRING);

 PROCEDURE sp_copy_budgetcontent (     P_PKOLDBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,     P_IPADD  IN   VARCHAR2,
 P_NEWPKBGT IN NUMBER, P_TEMPFLAG IN CHAR, P_RETURN OUT NUMBER
    ) ;

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_BGT', pLEVEL  => Plog.LFATAL);


/*Procedure to create and then copy the selected budget/template to this budget
p_commitflag : if passed as 1, issue commit else no commit
*/

PROCEDURE sp_copyCompleteBudget(
 p_old_budget IN NUMBER,
 p_new_budname IN VARCHAR2,
 p_new_budver IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_tempflag IN Varchar2,
 P_DEF_CALENDAR IN NUMBER
 );


 PROCEDURE sp_new_budget_common(
   P_PKOLDBGT IN NUMBER,
 P_NEWBGTNAME  IN   VARCHAR2,
 P_NEWBGTVER  IN   VARCHAR2,
    P_CREATOR  IN   NUMBER,
 P_IPADD  IN   VARCHAR2,
 P_RETURN OUT NUMBER ,
 P_DEF_CALENDAR IN NUMBER,
 P_BUDGET_TEMPLATE in number
 );

/*
Procedure to create default linked budget for a protoocl calendar
*/
PROCEDURE sp_createProtocolBudget(
 p_event_id IN Number,p_budget_template IN Number,
 p_new_budname IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_study in number
 );

/*
Procedure to add to default linked budget for a protoocl calendar
*/
PROCEDURE sp_addToProtocolBudget(
p_event_id IN Number,p_calledfrom IN varchar2,p_calendar IN number,
  p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_visit IN Number,
 p_evname IN Varchar2,
 p_cpt IN VARCHAR2,p_category IN Number,p_desc IN Varchar2,p_event_sequence IN number
   );

END Pkg_Bgt;
/


CREATE SYNONYM ERES.PKG_BGT FOR PKG_BGT;


CREATE SYNONYM EPAT.PKG_BGT FOR PKG_BGT;


GRANT EXECUTE, DEBUG ON PKG_BGT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_BGT TO ERES;

