CREATE OR REPLACE PROCEDURE        "GETSVCCOUNT" ( ObjectID char, ServiceID char, StDate date, EnDate date, count1  out number)
as
begin
  select count(*) into count1  from sch_events1 where object_id=ObjectID and service_id=ServiceID and(not(end_date_time<StDate or end_date_time>Endate));
end;
/


CREATE SYNONYM ERES.GETSVCCOUNT FOR GETSVCCOUNT;


CREATE SYNONYM EPAT.GETSVCCOUNT FOR GETSVCCOUNT;


GRANT EXECUTE, DEBUG ON GETSVCCOUNT TO EPAT;

GRANT EXECUTE, DEBUG ON GETSVCCOUNT TO ERES;

