CREATE OR REPLACE PROCEDURE        "SP_DEACTEVENTS" (
p_protocol IN NUMBER,
p_patientid IN NUMBER,
p_date IN DATE,
p_type CHAR :=''
)
/**
Procedure to deactivate the schedule of the patient in case of change to date or protocol
**/
AS
BEGIN

IF (p_type='S') THEN
DELETE FROM  SCH_DISPATCHMSG
WHERE fk_schevent IN ( SELECT event_id
        FROM SCH_EVENTS1
                     WHERE fk_study= (p_patientid)
                       AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND
					   status=0
                                       ) ;
UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE WHERE  fk_study= p_patientid  AND
SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND status=0 ;
ELSE
UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE WHERE  PATIENT_ID= LPAD(TO_CHAR(p_patientid),10,'0') AND
SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND bookedon=p_date;
DELETE FROM  SCH_DISPATCHMSG
WHERE fk_schevent IN ( SELECT event_id
        FROM SCH_EVENTS1
                     WHERE PATIENT_ID= LPAD(TO_CHAR(p_patientid),10,'0')
                       AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0')
                       AND bookedon=p_date
                   ) ;

END IF;

COMMIT;
END;
/


CREATE SYNONYM ERES.SP_DEACTEVENTS FOR SP_DEACTEVENTS;


CREATE SYNONYM EPAT.SP_DEACTEVENTS FOR SP_DEACTEVENTS;


GRANT EXECUTE, DEBUG ON SP_DEACTEVENTS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DEACTEVENTS TO ERES;

