create or replace
PROCEDURE        "SP_CPPROTEVENTS" (
   P_oldprot NUMBER ,
   p_newprot NUMBER ,
   p_userid  NUMBER,
   p_ip IN VARCHAR2
)
AS

/** Author: Charanjiv S Kalha 23rd Aug 2001
**  This procedure copies the events of one protocol to another.
** the input parameters are the id of the old and new protocols and hte user id of the
** user making the changes, ip
**
** Modification History
**
**Modified By		Date		Remarks
**Sonika Talwar		10March04	Added ip in the input parameter
**								The orig_id field incase of copy calendar event would
**								store the event id whose copy is being made
**Sonia Abrol		06/06/06	passed a new parameter to sp_cpefedtls to not to copy propagate_from
**JM				29Apr2008	modified to add event_category and event_sequence
**Sammie			30Apr2010	added new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
**Rohit				03May2010   added creator column
**Bikash      29jul2010   SW-FIN5b
** JM: 				09FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS  
*/

V_STATUS NUMBER ;
V_CURRVAL NUMBER ;
v_ret NUMBER;
v_visit_num NUMBER;

-- This cursor gets all the events of the old protocol, these are then inserted for the
-- new protocol. The details for these events are copied using the cp_evedtls proc.

CURSOR cur_eventprot
IS
	SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
	COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
	FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
	ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
	PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
	PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT, STATUS_CHANGEBY, fk_visit,fk_catlib,
	DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category, event_sequence,
	hide_flag, event_library_type ,event_line_category,
	SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, fk_codelst_calstat
	FROM EVENT_ASSOC
	WHERE CHAIN_ID = p_oldprot AND event_type = 'A' ;
BEGIN

	Sp_Copy_Protocol_Visits(p_oldprot, p_newprot, P_USERID, P_IP, V_RET);

	FOR studyprot_rec IN cur_eventprot LOOP
		SELECT EVENT_DEFINITION_SEQ.NEXTVAL
		INTO V_CURRVAL FROM dual ;

		--The orig_id field incase of copy calendar event would store the event id whose copy is being made
		INSERT INTO EVENT_ASSOC
		(
			EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES,
			COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
			FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
			ORG_ID, EVENT_MSG, EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
			PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
			STATUS_CHANGEBY,fk_catlib, DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,
			event_category, event_sequence,
			event_library_type ,event_line_category,CREATOR,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, fk_codelst_calstat
		)
		VALUES
		(
			v_currval , p_newprot, studyprot_rec.EVENT_TYPE, studyprot_rec.NAME,
			studyprot_rec.NOTES, studyprot_rec.COST, studyprot_rec.COST_DESCRIPTION,
			studyprot_rec.DURATION, studyprot_rec.USER_ID, studyprot_rec.LINKED_URI,
			studyprot_rec.FUZZY_PERIOD, studyprot_rec.MSG_TO, 
			studyprot_rec.DESCRIPTION, studyprot_rec.DISPLACEMENT,
			studyprot_rec.EVENT_ID,studyprot_rec.EVENT_MSG, studyprot_rec.EVENT_RES,
			studyprot_rec.EVENT_FLAG,
			studyprot_rec.PAT_DAYSBEFORE, studyprot_rec.PAT_DAYSAFTER,
			studyprot_rec.USR_DAYSBEFORE, studyprot_rec.USR_DAYSAFTER,
			studyprot_rec.PAT_MSGBEFORE, studyprot_rec.PAT_MSGAFTER,
			studyprot_rec.USR_MSGBEFORE, studyprot_rec.USR_MSGAFTER,
			SYSDATE , p_userid,studyprot_rec.FK_CATLIB,
			studyprot_rec.DURATION_UNIT, studyprot_rec.event_fuzzyafter, studyprot_rec.event_durationafter, studyprot_rec.event_cptcode,studyprot_rec.event_durationbefore,
			studyprot_rec.event_category, studyprot_rec.event_sequence, --YK: Modified for Bug#7270
			studyprot_rec.event_library_type ,studyprot_rec.event_line_category,p_userid,
			studyprot_rec.SERVICE_SITE_ID,studyprot_rec.FACILITY_ID,studyprot_rec.FK_CODELST_COVERTYPE,studyprot_rec.COVERAGE_NOTES,
      		studyprot_rec.fk_codelst_calstat
      
		);

		-- the SP_CPEVEDTLS procedure is call once for all the events of the procedure.
		-- the parameters are
		-- event_id  IN, event whoes details are to be copied
		-- v_currval IN, the id of the new event in which the details are to be copied.
		-- v_status - out parameter.
		-- creater column added in insert query with values p_userid for Bug 3335

		IF (studyprot_rec.fk_visit > 0) THEN

			--Get the visit no to differentiate between 2 events with same displacement
			SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT  WHERE pk_protocol_visit=studyprot_rec.fk_visit;
			Sp_Get_Prot_Visit(P_NEWPROT,studyprot_rec.DISPLACEMENT, P_USERid, P_IP, v_ret,v_visit_num);
			IF (v_ret > 0) THEN
				UPDATE EVENT_ASSOC
				SET FK_VISIT = v_ret
				WHERE EVENT_ID = v_currval;
			END IF;

		END IF;

		Sp_Cpevedtls (studyprot_rec.EVENT_ID, V_CURRVAL,'S', V_STATUS,P_USERId, P_IP,0);
	END LOOP ;
END ;
/


CREATE SYNONYM ERES.SP_CPPROTEVENTS FOR SP_CPPROTEVENTS;


CREATE SYNONYM EPAT.SP_CPPROTEVENTS FOR SP_CPPROTEVENTS;


GRANT EXECUTE, DEBUG ON SP_CPPROTEVENTS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_CPPROTEVENTS TO ERES;

