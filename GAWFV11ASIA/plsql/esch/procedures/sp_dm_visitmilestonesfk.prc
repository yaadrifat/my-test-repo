CREATE OR REPLACE PROCEDURE        "SP_DM_VISITMILESTONESFK" 

AS

v_chain_id NUMBER;
v_fk_visit NUMBER;
v_visit_nbr NUMBER;
v_cal_name VARCHAR2(50);
v_sql VARCHAR2(2000);
v_milestone_visit NUMBER;


CURSOR C1 IS
SELECT event_id
INTO v_chain_id
FROM event_assoc
WHERE event_type = 'P'
ORDER BY event_id;

CURSOR C2 IS
SELECT VISIT, fk_visit FROM
	(SELECT DISTINCT DENSE_RANK () OVER
(PARTITION BY d.mindisp ORDER BY (assoc.displacement - d.mindisp )) visit, assoc.fk_visit fk_visit
FROM EVENT_ASSOC  assoc, SCH_PROTOCOL_VISIT vis,
(SELECT MIN(displacement) mindisp FROM  EVENT_ASSOC
WHERE chain_id  = v_chain_id AND EVENT_TYPE='A' AND displacement > 0) d
WHERE CHAIN_ID  = v_chain_id AND EVENT_TYPE='A' AND assoc.DISPLACEMENT > 0 AND
assoc.fk_visit = vis.pk_protocol_visit ) a ;


BEGIN


OPEN C1;

LOOP

	FETCH C1 INTO v_chain_id;
	EXIT WHEN C1%NOTFOUND;

	OPEN C2;

	LOOP
		FETCH C2 INTO v_visit_nbr, v_fk_visit;
		EXIT WHEN C2%NOTFOUND;

		BEGIN
		UPDATE ER_MILESTONE SET FK_VISIT = v_fk_visit
		WHERE milestone_visit = v_visit_nbr AND fk_cal = v_chain_id AND milestone_type = 'VM';

		DBMS_OUTPUT.PUT_LINE('c,v,k,m=' || TO_CHAR(v_chain_id) || ',' || TO_CHAR(v_visit_nbr) || ',' || TO_CHAR(v_fk_visit) || ',' || TO_CHAR(v_milestone_visit));

		EXCEPTION
				  WHEN NO_DATA_FOUND THEN
				  	   DBMS_OUTPUT.PUT_LINE('NF');
				  WHEN TOO_MANY_ROWS THEN
				  	   DBMS_OUTPUT.PUT_LINE('TM');

		END;

	END LOOP;
	CLOSE  C2;

END LOOP;

END  SP_DM_VISITMILESTONESFK;
/


CREATE SYNONYM ERES.SP_DM_VISITMILESTONESFK FOR SP_DM_VISITMILESTONESFK;


CREATE SYNONYM EPAT.SP_DM_VISITMILESTONESFK FOR SP_DM_VISITMILESTONESFK;


GRANT EXECUTE, DEBUG ON SP_DM_VISITMILESTONESFK TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DM_VISITMILESTONESFK TO ERES;

