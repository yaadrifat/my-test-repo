set define off;
create or replace
PROCEDURE "SP_CPROTINSTUDY" (

   p_protocol IN NUMBER ,
   p_protname IN VARCHAR ,
   p_userid IN NUMBER ,
   p_ip IN VARCHAR2 ,
   p_newprot OUT NUMBER,
   p_accId IN NUMBER,
   p_calAssoc IN VARCHAR
)
AS
/************************************************************************************************
**  Author: Charanjiv S Kalha 14th Sept 2001
**
** This will copy an existing protocols in a study to the same study with a diferent name
** Paramters
** p_prodotcol - the protocl which has to be copied to the new study
** p_protname the name of the new protocol ,
** p_userid the name of the user who is doing the task
** p_ip the ip address
** p_newprot the OUT paramter wich will be "-1" if the name is found to be already existing
**  or the protocol id which will be generated
**
** Modification History
**
** Modified By		Date		Remarks
** Sonia Abrol		06/02/06
** JM				29Apr2008	modified to add event_category and event_sequence
** Sammie			30Apr2010	SW_FIN1
** Mukul						BUG 3335
** Bikash			29jul2010	SW_FIN5b
** JM:              09FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS
*************************************************************************************************
*/
   v_CHAIN_ID         NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   v_USER_ID          NUMBER;
   v_STATUS           NUMBER;
   v_NEWPROTOCOL      NUMBER;
   v_ret NUMBER;

   v_WIP_statusId number;
-- The new protocol id generated from the EVENT_DEFINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter

-- This protocol should give all the protocols attached to a study

BEGIN
		

	select pk_codelst into v_WIP_statusId from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='W';

	BEGIN
		SELECT COUNT(event_id)
		INTO v_status
		FROM EVENT_ASSOC
		WHERE  chain_id = (SELECT chain_id FROM EVENT_ASSOC WHERE event_id = p_protocol )
		AND event_type = 'P' AND EVENT_CALASSOCTO=p_calAssoc
		AND LOWER(trim(NAME)) = LOWER(trim(p_protname)) ;--KM--to fix the Bug 1756

		IF v_status > 0 THEN
			p_newprot := -1 ;
			RETURN ;
		END IF ;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			NULL ;
	END ;


	SELECT EVENT_DEFINITION_SEQ.NEXTVAL
	INTO p_newprot
	FROM DUAL;

	INSERT INTO EVENT_ASSOC
	(
		EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
		COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
		STATUS_CHANGEBY, CREATOR,IP_ADD,FK_CATLIB ,EVENT_CALASSOCTO,
		DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category,
		event_sequence,event_library_type, event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, fk_codelst_calstat
	)
	(
		SELECT p_newprot , CHAIN_ID, EVENT_TYPE, p_protname , NOTES, COST,
		COST_DESCRIPTION, DURATION, p_accId, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, SYSDATE STATUS_DT,
		p_userid, p_userid,P_ip,FK_CATLIB,EVENT_CALASSOCTO,
		DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore, event_category,
		event_sequence,event_library_type, event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, v_WIP_statusId 
		FROM EVENT_ASSOC
		WHERE EVENT_ID = p_protocol
		AND event_type = 'P'
	) ;

 ---
 --- call to procedure to copy all events of the protocol just copied, parameters are
 --- the old protocol id, and the new protocol (event id)
 --- Creator added in insert query to fix Bug 3335
 --- Last_modified column removed from Insert query to fix BUG 3335

   Sp_Cpprotevents(p_protocol , p_newprot, p_userid, p_ip ) ;
   COMMIT;
END;
/

CREATE OR REPLACE SYNONYM ERES.SP_CPROTINSTUDY FOR SP_CPROTINSTUDY;
CREATE OR REPLACE SYNONYM EPAT.SP_CPROTINSTUDY FOR SP_CPROTINSTUDY;