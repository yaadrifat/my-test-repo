create or replace
PROCEDURE        "SP_CPSTUDYPROT" (
   p_oldstudy IN NUMBER ,
   p_newstudy IN NUMBER ,
   p_userid IN NUMBER
)
AS
/************************************************************************************************
   **  Author: Charanjiv S Kalha 23rd Aug 2001
   **
   ** This will copy all the already existing protocols in a study to another study
   ** Paramters
   ** p_prodotcol - the protocl which has to be copied to the new study
   ** p_oldstudy - the old study id
   ** p_oldstudy - the new study id
   **
   ** Modification History
   **
   ** Modified By         Date     		Remarks

   ** JM: 				  09FEB2011,    #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS
*************************************************************************************************
*/
   v_CHAIN_ID         NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   v_USER_ID          NUMBER;
   v_STATUS           NUMBER;
   v_NEWPROTOCOL      NUMBER;
   V_EVENT_CALASSOCTO CHAR(1);

-- The new protocol id generated from the EVENT_DEFINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter

-- This protocol should give all the protocols attached to a study

   CURSOR cur_studyprot
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
             COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
             FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
             ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
             PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
             PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, SYSDATE STATUS_DT,
             STATUS_CHANGEBY,FK_CATLIB,EVENT_CALASSOCTO, fk_codelst_calstat
        FROM EVENT_ASSOC
       WHERE CHAIN_ID = p_oldstudy
         AND event_type = 'P'
       ORDER BY EVENT_TYPE DESC ;


BEGIN
 FOR studyprot_rec IN cur_studyprot
  LOOP

  SELECT EVENT_DEFINITION_SEQ.NEXTVAL
   INTO v_CURRVAL
   FROM DUAL;

  /*
  P_Chain_Id will remain the same for all the records inserted ie the id of the first
  protocol.
  */

  -- event id of the the protocol generated is the chain id of all the child events
  -- The staus of the new protocol will be 'W' work in progress, status date is sysdate and
  -- status changed by user id passed to the procedure.

   INSERT INTO EVENT_ASSOC
         (
 	   EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES,
	   COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
           FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
           ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
           PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
           PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
           STATUS_CHANGEBY,FK_CATLIB,EVENT_CALASSOCTO, fk_codelst_calstat
         )
      VALUES
         ( v_CURRVAL, p_newstudy, studyprot_rec.EVENT_TYPE, studyprot_rec.NAME,
           studyprot_rec.NOTES, studyprot_rec.COST, studyprot_rec.COST_DESCRIPTION,
           studyprot_rec.DURATION, studyprot_rec.USER_ID, studyprot_rec.LINKED_URI,
           studyprot_rec.FUZZY_PERIOD, studyprot_rec.MSG_TO, 
           studyprot_rec.DESCRIPTION, studyprot_rec.DISPLACEMENT, studyprot_rec.ORG_ID,
           studyprot_rec.EVENT_MSG, studyprot_rec.EVENT_RES, studyprot_rec.EVENT_FLAG,
           studyprot_rec.PAT_DAYSBEFORE, studyprot_rec.PAT_DAYSAFTER,
           studyprot_rec.USR_DAYSBEFORE, studyprot_rec.USR_DAYSAFTER,
           studyprot_rec.PAT_MSGBEFORE, studyprot_rec.PAT_MSGAFTER,
           studyprot_rec.USR_MSGBEFORE, studyprot_rec.USR_MSGAFTER,
           SYSDATE , p_userid,studyprot_rec.FK_CATLIB,studyprot_rec.EVENT_CALASSOCTO,studyprot_rec.fk_codelst_calstat
         );

 ---
 --- call to procedure to copy all events of the protocol just copied, parameters are
 --- the old protocol id, and the new protocol (event id)
 ---
   Sp_Cpprotevents(studyprot_rec.event_id , v_currval,p_userid,NULL ) ;

   END LOOP;

   COMMIT;
END;
/


CREATE SYNONYM ERES.SP_CPSTUDYPROT FOR SP_CPSTUDYPROT;


CREATE SYNONYM EPAT.SP_CPSTUDYPROT FOR SP_CPSTUDYPROT;


GRANT EXECUTE, DEBUG ON SP_CPSTUDYPROT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_CPSTUDYPROT TO ERES;

