CREATE OR REPLACE FUNCTION        "SCH_GETVISIT" (p_patprot NUMBER, p_eventid number)
return number
 is
 v_sql varchar2(3000) ;
 dt number ;
begin
 v_sql := 'Select m.visit
  from
  (select fk_patprot, dense_rank()
          over ( partition by fk_patprot
                 order by (ACTUAL_SCHDATE) asc nulls last ) visit,
          ACTUAL_SCHDATE,event_id
  FROM SCH_EVENTS1
 where fk_patprot = :x ) m
  where m.fk_patprot = :y and m.event_id = :z ' ;
 execute immediate v_sql into dt
   using p_patprot ,p_patprot , p_eventid  ;
 return dt ;
end ;
/


CREATE SYNONYM ERES.SCH_GETVISIT FOR SCH_GETVISIT;


CREATE SYNONYM EPAT.SCH_GETVISIT FOR SCH_GETVISIT;


GRANT EXECUTE, DEBUG ON SCH_GETVISIT TO EPAT;

GRANT EXECUTE, DEBUG ON SCH_GETVISIT TO ERES;

