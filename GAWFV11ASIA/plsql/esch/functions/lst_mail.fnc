CREATE OR REPLACE FUNCTION        "LST_MAIL" (event NUMBER ) RETURN VARCHAR IS
CURSOR uemail IS
SELECT a.ADD_EMAIL
  FROM er_add  a,
       er_user b ,
       sch_eventusr   c
 WHERE c.FK_EVENT = event
   AND c.EVENTUSR_TYPE='S'
   AND a.PK_ADD = b.FK_PERADD
   AND b.PK_USER = c.EVENTUSR ORDER BY a.ADD_EMAIL;
email_lst VARCHAR2(4000) ;
BEGIN
 FOR l_rec IN uemail LOOP
  email_lst :=  l_rec.add_email || ', '|| email_lst  ;
 END LOOP ;

email_lst  :=  SUBSTR(email_lst,1,LENGTH(email_lst) - 2) ;

DBMS_OUTPUT.PUT_LINE (email_lst) ;
RETURN email_lst ;
END ;
/


CREATE SYNONYM ERES.LST_MAIL FOR LST_MAIL;


CREATE SYNONYM EPAT.LST_MAIL FOR LST_MAIL;


GRANT EXECUTE, DEBUG ON LST_MAIL TO EPAT;

GRANT EXECUTE, DEBUG ON LST_MAIL TO ERES;

