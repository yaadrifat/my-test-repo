CREATE OR REPLACE FUNCTION        "ADDSTR" (s1 varchar2,s2 varchar2)
return varchar2 is
   s3 varchar2(20) ;
begin
   s3 := s1 || s2 ;
   return (s3) ;
end ;
-- END PL/SQL BLOCK (do not remove this line) ----------------------------------
/


CREATE SYNONYM ERES.ADDSTR FOR ADDSTR;


CREATE SYNONYM EPAT.ADDSTR FOR ADDSTR;


GRANT EXECUTE, DEBUG ON ADDSTR TO EPAT;

GRANT EXECUTE, DEBUG ON ADDSTR TO ERES;

