/* Formatted on 2/9/2010 1:41:14 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW EV_ADV_SITE_INFO
(
   PK_ADVEVE,
   FK_STUDY,
   FK_PER,
   PK_SITE,
   SITE_NAME,
   ADVINFO_TYPE,
   CODELST_SUBTYP,
   ADVINFO_VALUE,
   AE_STDATE
)
AS
   SELECT   ad.PK_ADVEVE,
            ad.FK_STUDY,
            ad.FK_PER,
            st.pk_site,
            st.site_name,
            ev.advinfo_type,
            ev.CODELST_SUBTYP,
            ev.ADVINFO_VALUE,
            ad.AE_STDATE
     FROM   sch_adverseve ad,
            EV_ADV_CHKINFO ev,
            er_site st,
            er_per per
    WHERE       ev.fk_adverse = ad.pk_adveve
            AND per.pk_per = ad.fk_per
            AND st.pk_site = per.fk_site;


CREATE SYNONYM ERES.EV_ADV_SITE_INFO FOR EV_ADV_SITE_INFO;


CREATE SYNONYM EPAT.EV_ADV_SITE_INFO FOR EV_ADV_SITE_INFO;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_SITE_INFO TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_SITE_INFO TO ERES;

