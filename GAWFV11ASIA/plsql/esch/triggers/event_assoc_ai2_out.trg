CREATE OR REPLACE TRIGGER ESCH."EVENT_ASSOC_AI2_OUT"
AFTER INSERT ON ESCH.EVENT_ASSOC FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.creator;
  IF(:NEW.EVENT_TYPE='P')
  THEN
  PKG_MSG_QUEUE.SP_POPULATE_CAL_STATUS_MSG(
      :NEW.event_id,
      fkaccount,
      'I',
      to_char(:NEW.EVENT_ID),
      :NEW.FK_CODELST_CALSTAT
   );
END IF;
  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AI1_OUT ' || SQLERRM);
END;
/