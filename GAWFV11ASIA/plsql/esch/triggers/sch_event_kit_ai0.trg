--When records are inserted in SCH_EVENT_KIT table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENT_KIT_AI0" 
AFTER INSERT ON ESCH.SCH_EVENT_KIT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:NEW.FK_EVENT);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
	
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
      --Inserting row in AUDIT_ROW_MODULE table.
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid, 'SCH_EVENT_KIT', :NEW.rid,v_chainid,'I',:NEW.Creator);    
      --Inserting rows in AUDIT_COLUMN_MODULE table.  
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','PK_EVENTKIT',NULL,:NEW.PK_EVENTKIT,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','FK_EVENT',NULL,:NEW.FK_EVENT,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','FK_storage',NULL,:NEW.FK_storage,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','PROPAGATE_FROM',NULL,:NEW.PROPAGATE_FROM,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','RID',NULL,:NEW.RID,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
	  
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENT_KIT_AI0 ');
END;
/
