CREATE OR REPLACE TRIGGER "SCH_EVENTCREATE" 
BEFORE INSERT
ON EVENT_DEF
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
status  NUMBER(1);
BEGIN
IF status=-1 THEN
raise_application_error(-20101,'data already exists');
END IF;
END;
/


