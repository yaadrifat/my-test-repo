CREATE OR REPLACE TRIGGER "EVENT_ASSOC_AD1" AFTER DELETE ON EVENT_ASSOC
REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
WHEN (
old.event_type='A'
      )
declare
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_ASSOC_AD1', pLEVEL  => Plog.LFATAL);
 begin

 --Delete the event from calendar's default budget
 begin
	 delete from sch_lineitem l
	 where fk_event = :old.event_id and l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_bgtcal =
	 			(select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.chain_id and fk_budget = (
	 			select  pk_budget from sch_budget where budget_calendar = :old.chain_id)
	 			)
	 		);
 exception when others then
	plog.fatal(pctx,'Exception:'||sqlerrm);
 end;
end;
/


