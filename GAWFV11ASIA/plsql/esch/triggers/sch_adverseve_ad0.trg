CREATE  OR REPLACE TRIGGER SCH_ADVERSEVE_AD0 AFTER DELETE ON SCH_ADVERSEVE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_ADVERSEVE', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'AE_ADDINFO', :OLD.AE_ADDINFO);
       Audit_Trail.column_delete (raid, 'AE_BDSYSTEM_AFF', :OLD.AE_BDSYSTEM_AFF);
       Audit_Trail.column_delete (raid, 'AE_DESC', :OLD.AE_DESC);
       Audit_Trail.column_delete (raid, 'AE_DISCVRYDATE', :OLD.AE_DISCVRYDATE);
       Audit_Trail.column_delete (raid, 'AE_ENDDATE', :OLD.AE_ENDDATE);
       Audit_Trail.column_delete (raid, 'AE_ENTERBY', :OLD.AE_ENTERBY);
       Audit_Trail.column_delete (raid, 'AE_GRADE', :OLD.AE_GRADE);
       Audit_Trail.column_delete (raid, 'AE_LKP_ADVID', :OLD.AE_LKP_ADVID);
       Audit_Trail.column_delete (raid, 'AE_LOGGEDDATE', :OLD.AE_LOGGEDDATE);
       Audit_Trail.column_delete (raid, 'AE_NAME', :OLD.AE_NAME);
       Audit_Trail.column_delete (raid, 'AE_NOTES', :OLD.AE_NOTES);
       Audit_Trail.column_delete (raid, 'AE_OUTDATE', :OLD.AE_OUTDATE);
       Audit_Trail.column_delete (raid, 'AE_OUTNOTES', :OLD.AE_OUTNOTES);
       Audit_Trail.column_delete (raid, 'AE_OUTTYPE', :OLD.AE_OUTTYPE);
       Audit_Trail.column_delete (raid, 'AE_RECOVERY_DESC', :OLD.AE_RECOVERY_DESC);
       Audit_Trail.column_delete (raid, 'AE_RELATIONSHIP', :OLD.AE_RELATIONSHIP);
       Audit_Trail.column_delete (raid, 'AE_REPORTEDBY', :OLD.AE_REPORTEDBY);
       Audit_Trail.column_delete (raid, 'AE_SEVERITY', :OLD.AE_SEVERITY);
       Audit_Trail.column_delete (raid, 'AE_STDATE', :OLD.AE_STDATE);
       Audit_Trail.column_delete (raid, 'AE_TREATMENT_COURSE', :OLD.AE_TREATMENT_COURSE);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CUSTOM_COL_INTERFACE', :OLD.CUSTOM_COL_INTERFACE);
       Audit_Trail.column_delete (raid, 'DICTIONARY', :OLD.DICTIONARY);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_OUTACTION;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_OUTACTION', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_CODLST_AETYPE', :OLD.FK_CODLST_AETYPE);
       Audit_Trail.column_delete (raid, 'FK_EVENTS1', :OLD.FK_EVENTS1);
       Audit_Trail.column_delete (raid, 'FK_LINK_ADVERSEVE', :OLD.FK_LINK_ADVERSEVE);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FORM_STATUS', :OLD.FORM_STATUS);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'MEDDRA', :OLD.MEDDRA);
       Audit_Trail.column_delete (raid, 'PK_ADVEVE', :OLD.PK_ADVEVE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/