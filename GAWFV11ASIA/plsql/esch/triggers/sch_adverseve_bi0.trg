CREATE  OR REPLACE TRIGGER SCH_ADVERSEVE_BI0 BEFORE INSERT ON SCH_ADVERSEVE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_ADVERSEVE', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'AE_ADDINFO', :NEW.AE_ADDINFO);
       Audit_Trail.column_insert (raid, 'AE_BDSYSTEM_AFF', :NEW.AE_BDSYSTEM_AFF);
       Audit_Trail.column_insert (raid, 'AE_DESC', :NEW.AE_DESC);
       Audit_Trail.column_insert (raid, 'AE_DISCVRYDATE', :NEW.AE_DISCVRYDATE);
       Audit_Trail.column_insert (raid, 'AE_ENDDATE', :NEW.AE_ENDDATE);
       Audit_Trail.column_insert (raid, 'AE_ENTERBY', :NEW.AE_ENTERBY);
       Audit_Trail.column_insert (raid, 'AE_GRADE', :NEW.AE_GRADE);
       Audit_Trail.column_insert (raid, 'AE_LKP_ADVID', :NEW.AE_LKP_ADVID);
       Audit_Trail.column_insert (raid, 'AE_LOGGEDDATE', :NEW.AE_LOGGEDDATE);
       Audit_Trail.column_insert (raid, 'AE_NAME', :NEW.AE_NAME);
       Audit_Trail.column_insert (raid, 'AE_NOTES', :NEW.AE_NOTES);
       Audit_Trail.column_insert (raid, 'AE_OUTDATE', :NEW.AE_OUTDATE);
       Audit_Trail.column_insert (raid, 'AE_OUTNOTES', :NEW.AE_OUTNOTES);
       Audit_Trail.column_insert (raid, 'AE_OUTTYPE', :NEW.AE_OUTTYPE);
       Audit_Trail.column_insert (raid, 'AE_RECOVERY_DESC', :NEW.AE_RECOVERY_DESC);
       Audit_Trail.column_insert (raid, 'AE_RELATIONSHIP', :NEW.AE_RELATIONSHIP);
       Audit_Trail.column_insert (raid, 'AE_REPORTEDBY', :NEW.AE_REPORTEDBY);
       Audit_Trail.column_insert (raid, 'AE_SEVERITY', :NEW.AE_SEVERITY);
       Audit_Trail.column_insert (raid, 'AE_STDATE', :NEW.AE_STDATE);
       Audit_Trail.column_insert (raid, 'AE_TREATMENT_COURSE', :NEW.AE_TREATMENT_COURSE);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CUSTOM_COL_INTERFACE', :NEW.CUSTOM_COL_INTERFACE);
       Audit_Trail.column_insert (raid, 'DICTIONARY', :NEW.DICTIONARY);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_OUTACTION;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_OUTACTION', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_CODLST_AETYPE', :NEW.FK_CODLST_AETYPE);
       Audit_Trail.column_insert (raid, 'FK_EVENTS1', :NEW.FK_EVENTS1);
       Audit_Trail.column_insert (raid, 'FK_LINK_ADVERSEVE', :NEW.FK_LINK_ADVERSEVE);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FORM_STATUS', :NEW.FORM_STATUS);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'MEDDRA', :NEW.MEDDRA);
       Audit_Trail.column_insert (raid, 'PK_ADVEVE', :NEW.PK_ADVEVE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/