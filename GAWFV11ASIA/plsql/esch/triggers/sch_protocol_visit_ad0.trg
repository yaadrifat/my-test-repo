CREATE  OR REPLACE TRIGGER SCH_PROTOCOL_VISIT_AD0 AFTER DELETE ON SCH_PROTOCOL_VISIT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_PROTOCOL_VISIT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'DESCRIPTION', :OLD.DESCRIPTION);
       Audit_Trail.column_delete (raid, 'DISPLACEMENT', :OLD.DISPLACEMENT);
       Audit_Trail.column_delete (raid, 'FK_PROTOCOL', :OLD.FK_PROTOCOL);
       Audit_Trail.column_delete (raid, 'HIDE_FLAG', :OLD.HIDE_FLAG);
       Audit_Trail.column_delete (raid, 'INSERT_AFTER', :OLD.INSERT_AFTER);
       Audit_Trail.column_delete (raid, 'INSERT_AFTER_INTERVAL', :OLD.INSERT_AFTER_INTERVAL);
       Audit_Trail.column_delete (raid, 'INSERT_AFTER_INTERVAL_UNIT', :OLD.INSERT_AFTER_INTERVAL_UNIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'NO_INTERVAL_FLAG', :OLD.NO_INTERVAL_FLAG);
       Audit_Trail.column_delete (raid, 'NUM_DAYS', :OLD.NUM_DAYS);
       Audit_Trail.column_delete (raid, 'NUM_MONTHS', :OLD.NUM_MONTHS);
       Audit_Trail.column_delete (raid, 'NUM_WEEKS', :OLD.NUM_WEEKS);
       Audit_Trail.column_delete (raid, 'OFFLINE_FLAG', :OLD.OFFLINE_FLAG);
       Audit_Trail.column_delete (raid, 'PK_PROTOCOL_VISIT', :OLD.PK_PROTOCOL_VISIT);
       Audit_Trail.column_delete (raid, 'PROTOCOL_TYPE', :OLD.PROTOCOL_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'VISIT_NAME', :OLD.VISIT_NAME);
       Audit_Trail.column_delete (raid, 'VISIT_NO', :OLD.VISIT_NO);
       Audit_Trail.column_delete (raid, 'VISIT_TYPE', :OLD.VISIT_TYPE);
       Audit_Trail.column_delete (raid, 'WIN_AFTER_NUMBER', :OLD.WIN_AFTER_NUMBER);
       Audit_Trail.column_delete (raid, 'WIN_AFTER_UNIT', :OLD.WIN_AFTER_UNIT);
       Audit_Trail.column_delete (raid, 'WIN_BEFORE_NUMBER', :OLD.WIN_BEFORE_NUMBER);
       Audit_Trail.column_delete (raid, 'WIN_BEFORE_UNIT', :OLD.WIN_BEFORE_UNIT);
COMMIT;
END;
/
