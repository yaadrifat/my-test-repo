CREATE OR REPLACE TRIGGER "SCH_EVENTS1_AU1"
	AFTER UPDATE ON sch_events1 REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
WHEN (
old.start_date_time is null
      )
BEGIN

    insert into sch_eventstat
   ( PK_EVENTSTAT, EVENTSTAT_DT, EVENTSTAT_NOTES, EVENTSTAT,
     FK_EVENT, CREATOR, LAST_MODIFIED_BY, IP_ADD  )
 values
   ( sch_eventstat_seq.nextval, :new.start_date_time, :old.notes, :old.isconfirmed,
     :old.event_id, :old.creator, :old.last_modified_by, :old.ip_add) ;

	END;
/


