CREATE OR REPLACE TRIGGER "SCH_LINEITEM_BU1"
BEFORE UPDATE
ON SCH_LINEITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   v_soccodeid number;
   v_bgtcal_excldsocflag number;
BEGIN

if nvl(:NEW.LINEITEM_DELFLAG,' ') != 'Y' then
    SELECT pk_codelst INTO v_soccodeid
    FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
    AND LOWER(codelst_subtyp)='stdcare_cost';

   -- get exclude soc flag

   select nvl(bgtcal_excldsocflag,0)
   into v_bgtcal_excldsocflag
   from sch_bgtcal,sch_bgtsection
   where pk_budgetsec = :new.fk_bgtsection and fk_bgtcal = pk_bgtcal;

    -- if SOC and exclude SOC is set

    if (:new.fk_codelst_cost_type = v_soccodeid and v_bgtcal_excldsocflag = 1) then
      :new.LINEITEM_OTHERCOST := '0.0';
    end if;
end if;

END;
/


