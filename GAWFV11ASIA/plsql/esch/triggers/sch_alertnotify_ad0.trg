CREATE  OR REPLACE TRIGGER SCH_ALERTNOTIFY_AD0 AFTER DELETE ON SCH_ALERTNOTIFY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_ALERTNOTIFY', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'ALNOT_FLAG', :OLD.ALNOT_FLAG);
       Audit_Trail.column_delete (raid, 'ALNOT_GLOBALFLAG', :OLD.ALNOT_GLOBALFLAG);
       Audit_Trail.column_delete (raid, 'ALNOT_MOBEVE', :OLD.ALNOT_MOBEVE);
       Audit_Trail.column_delete (raid, 'ALNOT_TYPE', :OLD.ALNOT_TYPE);
       Audit_Trail.column_delete (raid, 'ALNOT_USERS', :OLD.ALNOT_USERS);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_AN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_AN', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'FK_PROTOCOL', :OLD.FK_PROTOCOL);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'PK_ALNOT', :OLD.PK_ALNOT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/