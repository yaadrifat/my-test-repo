CREATE  OR REPLACE TRIGGER SCH_CRFSTAT_BI0 BEFORE INSERT ON SCH_CRFSTAT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_CRFSTAT', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CRFSTAT_ENTERBY', :NEW.CRFSTAT_ENTERBY);
       Audit_Trail.column_insert (raid, 'CRFSTAT_REVIEWBY', :NEW.CRFSTAT_REVIEWBY);
       Audit_Trail.column_insert (raid, 'CRFSTAT_REVIEWON', :NEW.CRFSTAT_REVIEWON);
       Audit_Trail.column_insert (raid, 'CRFSTAT_SENTBY', :NEW.CRFSTAT_SENTBY);
       Audit_Trail.column_insert (raid, 'CRFSTAT_SENTFLAG', :NEW.CRFSTAT_SENTFLAG);
       Audit_Trail.column_insert (raid, 'CRFSTAT_SENTON', :NEW.CRFSTAT_SENTON);
       Audit_Trail.column_insert (raid, 'CRFSTAT_SENTTO', :NEW.CRFSTAT_SENTTO);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_CRFSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CRFSTAT', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_CRF', :NEW.FK_CRF);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'PK_CRFSTAT', :NEW.PK_CRFSTAT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/