CREATE OR REPLACE TRIGGER "SCH_BUDGET_AU1" 
AFTER UPDATE OF BUDGET_RIGHTS,BUDGET_RIGHTSCOPE,FK_SITE,FK_STUDY
ON SCH_BUDGET
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN

  IF NVL(:OLD.budget_rightscope,' ') != NVL(:NEW.budget_rightscope,' ') THEN

   IF NVL(LENGTH(:NEW.budget_rightscope),0) > 0 THEN
     IF :NEW.budget_rightscope = 'A' THEN /* add access for all account users*/
        pkg_bgtusers.sp_addact_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_ACCOUNT,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
      END IF;
     IF :NEW.budget_rightscope = 'O' THEN /* add access for all users of budget's site*/
       pkg_bgtusers.sp_addsite_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_SITE,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
    END IF;
     IF :NEW.budget_rightscope = 'S' THEN  /* add access for all users of budget's study*/
        pkg_bgtusers.sp_addteam_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_STUDY,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
   END IF;

	 /***************Fix for bug# 2286*/
	 --if budget rights are removed
 ELSE
   pkg_bgtusers.sp_delete_bgtusers(:NEW.PK_BUDGET, 'G');
  /*************--anu*/
    END IF;

  ELSE -- bugtscope is not changed
    IF NVL(:OLD.budget_rights,' ') !=
       NVL(:NEW.budget_rights,' ') THEN
          pkg_bgtusers.sp_update_rights(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_DATE,:NEW.IP_ADD );
    END IF;
  END IF;
  IF :NEW.budget_rightscope = 'O' THEN /* access scope is budget's site*/
    --check if organisation is changed, then remove the users of old oganisation and add users of new organisation in sch_bgtusers
    IF NVL(:OLD.FK_SITE,0) != NVL(:NEW.FK_SITE,0) THEN
        pkg_bgtusers.sp_addsite_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_SITE,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
    END IF;
  END IF;
  IF :NEW.budget_rightscope = 'S' THEN /* access scope is budget's study*/
    --check if study is changed, then remove the users of old study and add users of new study in sch_bgtusers
    IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) THEN
      pkg_bgtusers.sp_addteam_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_STUDY,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
    END IF;
  END IF;
END;
/


