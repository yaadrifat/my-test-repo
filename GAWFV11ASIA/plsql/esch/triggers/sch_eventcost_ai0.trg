CREATE OR REPLACE TRIGGER "SCH_EVENTCOST_AI0"
AFTER INSERT
ON SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_name Varchar2(4000);
  v_protocol Number;
  v_creator Number;
  v_ipadd Varchar2(20);
  v_visit Number;
  v_cpt varchar2(50);
  v_desc  varchar2(4000);
v_line_category number;
  v_add_line boolean := false;
  v_pk_lineitem Number;
  v_calledfrom char(1);
  v_event_seq number;
  v_bgtTemplate number;
   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_EVENTCOST_AI0', pLEVEL  => Plog.LFATAL);
	v_res_costtype_pk number;
	v_apply_indirects number;
	v_soc_costpk number;

 BEGIN

  begin
	  SELECT pk_codelst INTO v_res_costtype_pk
	  FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
	  AND trim(LOWER(codelst_subtyp))='res_cost';
  exception when no_data_found then
	v_res_costtype_pk := 0;
 end;

  begin
	  SELECT pk_codelst INTO v_soc_costpk
	  FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
            AND trim(LOWER(codelst_subtyp))='stdcare_cost';
  exception when no_data_found then
	v_soc_costpk := 0;
 end;

 BEGIN

    select eve.name, eve.chain_id, eve.creator , eve.ip_Add , eve.fk_visit,
    eve.event_cptcode,eve.event_line_category,eve.description, eve.event_sequence, 
    (select cal.budget_template from event_assoc cal where cal.event_id = eve.chain_id)
    into v_name,v_protocol,v_creator,v_ipadd,v_visit,v_cpt,v_line_category,v_desc,v_event_seq,
    v_bgtTemplate
    from event_assoc eve
    where eve.event_id = :new.fk_event and eve.event_type = 'A' and nvl(eve.displacement,-99999999) <> 0;

  -- nvl for displacement because events added to 'no interval visit' has displacement as null
	v_add_line := true;

    v_calledfrom := 'S';

    exception when others then
        v_add_line := false;

 END;

if (nvl(v_line_category,0) <= 0 ) then
     SELECT  pk_codelst  INTO v_line_category FROM  SCH_CODELST
    WHERE trim (codelst_type) = 'category' AND trim (codelst_subtyp) = 'ctgry_patcare';
end if;

if (v_add_line = true  and v_bgtTemplate is not null) then 
 -- check if there is alineitem added for this event where cost is not set:
 -- CCF-FIN1 budget will be present if template is present

     begin
         select pk_lineitem
         into v_pk_lineitem
         from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
         where l.fk_event = :new.fk_event and (to_number(nvl(l.lineitem_sponsorunit,0))= 0) and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
         and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget and budget_calendar = v_protocol
         and rownum < 2;

     exception when no_data_found then
         v_pk_lineitem := 0;
     end ;


     if (v_pk_lineitem <= 0) then -- insert lineitem
            PKG_BGT.sp_addToProtocolBudget( :new.fk_event,v_calledfrom,v_protocol, v_creator , v_ipAdd , v_pk_lineitem , 0 ,v_visit,
        v_name,v_cpt,v_line_category,v_desc,v_event_seq );

    --Plog.FATAL(pctx,'v_pk_lineitem  insrted');

     end if;

     --update cost

     if (v_soc_costpk = nvl(:new.fk_cost_desc,0) ) then
		v_apply_indirects := 0;
	 else
	 	v_apply_indirects := 1;
	 end if;

	update sch_lineitem
	set LINEITEM_SPONSORUNIT    = :new.eventcost_value,
	last_modified_by = v_creator,fk_codelst_category = v_line_category,
	fk_codelst_cost_type = :new.fk_cost_desc ,lineitem_othercost = :new.eventcost_value,
	lineitem_applyindirects = v_apply_indirects
	where pk_lineitem = v_pk_lineitem;

    --Plog.FATAL(pctx,'v_pk_lineitem  updated');

 end if;

END;
/


