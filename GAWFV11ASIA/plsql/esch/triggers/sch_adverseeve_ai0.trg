CREATE OR REPLACE TRIGGER "SCH_ADVERSEEVE_AI0" 
AFTER INSERT
ON SCH_ADVERSEVE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   v_advsubtype VARCHAR2(20);

BEGIN

PKG_ALNOT.sp_adversevenotify (:NEW.FK_STUDY ,:NEW.FK_PER,
                          :NEW.FK_CODLST_AETYPE   ,
                          :NEW.AE_OUTTYPE ,
                          :NEW.CREATOR ,
                          :NEW.IP_ADD) ;


-- get adverse event type
BEGIN
   SELECT codelst_subtyp INTO  v_advsubtype FROM sch_codelst
   WHERE pk_codelst = :NEW.FK_CODLST_AETYPE  ;

   EXCEPTION WHEN NO_DATA_FOUND THEN
   			v_advsubtype := '';
END;

	IF trim(v_advsubtype) = 'al_sadve' THEN
			  --call study specific notification

			  PKG_ALNOT.sp_advnotify_for_studylevel (:NEW.FK_STUDY ,:NEW.FK_PER,
                          :NEW.CREATOR ,
                          :NEW.IP_ADD) ;


	END IF;

END;
/


