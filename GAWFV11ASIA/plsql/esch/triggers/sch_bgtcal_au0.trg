create or replace
TRIGGER "ESCH"."SCH_BGTCAL_AU0"
AFTER UPDATE
OF PK_BGTCAL,
FK_BUDGET,
BGTCAL_PROTID,
BGTCAL_PROTTYPE,
BGTCAL_DELFLAG,
BGTCAL_SPONSOROHEAD,
BGTCAL_CLINICOHEAD,
BGTCAL_SPONSORFLAG,
BGTCAL_CLINICFLAG,
BGTCAL_INDIRECTCOST,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON, IP_ADD,
BGTCAL_FRGBENEFIT,
BGTCAL_FRGFLAG,
BGTCAL_DISCOUNT,
BGTCAL_DISCOUNTFLAG,
BGTCAL_EXCLDSOCFLAG,
GRAND_RES_COST,
GRAND_RES_TOTALCOST,
GRAND_SOC_COST,
GRAND_SOC_TOTALCOST,
GRAND_TOTAL_COST,
GRAND_TOTAL_TOTALCOST,
GRAND_SALARY_COST,
GRAND_SALARY_TOTALCOST,
GRAND_FRINGE_COST,
GRAND_FRINGE_TOTALCOST,
GRAND_DISC_COST,
GRAND_DISC_TOTALCOST,
GRAND_IND_COST,
GRAND_IND_TOTALCOST,
GRAND_RES_SPONSOR,
GRAND_SOC_SPONSOR,
GRAND_SOC_VARIANCE,
GRAND_TOTAL_SPONSOR,
GRAND_TOTAL_VARIANCE,
GRAND_RES_VARIANCE,bgtcal_sp_overhead,
bgtcal_sp_overhead_flag
ON SCH_BGTCAL REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR(2000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'SCH_BGTCAL', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_bgtcal,0) !=
     NVL(:NEW.pk_bgtcal,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTCAL',
       :OLD.pk_bgtcal, :NEW.pk_bgtcal);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.bgtcal_protid,0) !=
     NVL(:NEW.bgtcal_protid,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_PROTID',
       :OLD.bgtcal_protid, :NEW.bgtcal_protid);
  END IF;
  IF NVL(:OLD.bgtcal_prottype,' ') !=
     NVL(:NEW.bgtcal_prottype,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_PROTTYPE',
       :OLD.bgtcal_prottype, :NEW.bgtcal_prottype);
  END IF;
  IF NVL(:OLD.bgtcal_delflag,' ') !=
     NVL(:NEW.bgtcal_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DELFLAG',
       :OLD.bgtcal_delflag, :NEW.bgtcal_delflag);
  END IF;
  IF NVL(:OLD.bgtcal_sponsorflag,' ') !=
     NVL(:NEW.bgtcal_sponsorflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SPONSORFLAG',
       :OLD.bgtcal_sponsorflag, :NEW.bgtcal_sponsorflag);
  END IF;
  IF NVL(:OLD.bgtcal_clinicflag,' ') !=
     NVL(:NEW.bgtcal_clinicflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_CLINICFLAG',
       :OLD.bgtcal_clinicflag, :NEW.bgtcal_clinicflag);
  END IF;
-----

  IF NVL(:OLD.bgtcal_sponsorohead,0) !=
     NVL(:NEW.bgtcal_sponsorohead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SPONSOROHEAD',
       :OLD.bgtcal_sponsorohead, :NEW.bgtcal_sponsorohead);
  END IF;
  IF NVL(:OLD.bgtcal_clinicohead,0) !=
     NVL(:NEW.bgtcal_clinicohead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_CLINICOHEAD',
       :OLD.bgtcal_clinicohead, :NEW.bgtcal_clinicohead);
  END IF;
  IF NVL(:OLD.bgtcal_indirectcost,0) !=
     NVL(:NEW.bgtcal_indirectcost,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_INDIRECTCOST',
       :OLD.bgtcal_indirectcost, :NEW.bgtcal_indirectcost);
  END IF;
  IF NVL(:OLD.bgtcal_frgbenefit,0) !=
     NVL(:NEW.bgtcal_frgbenefit,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_FRGBENEFIT',
       :OLD.bgtcal_frgbenefit, :NEW.bgtcal_frgbenefit);
  END IF;
  IF NVL(:OLD.bgtcal_frgflag,0) !=
     NVL(:NEW.bgtcal_frgflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_FRGFLAG',
       :OLD.bgtcal_frgflag, :NEW.bgtcal_frgflag);
  END IF;
  IF NVL(:OLD.bgtcal_discount,0) !=
     NVL(:NEW.bgtcal_discount,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DISCOUNT',
       :OLD.bgtcal_discount, :NEW.bgtcal_discount);
  END IF;
  IF NVL(:OLD.bgtcal_discountflag,0) !=
     NVL(:NEW.bgtcal_discountflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DISCOUNTFLAG',
       :OLD.bgtcal_discountflag, :NEW.bgtcal_discountflag);
  END IF;

-------

IF NVL(:OLD.bgtcal_excldsocflag,0) !=
     NVL(:NEW.bgtcal_excldsocflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_EXCLDSOCFLAG',
       :OLD.bgtcal_excldsocflag, :NEW.bgtcal_excldsocflag);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
--JM: added 02/15/07
  IF NVL(:OLD.grand_res_cost,' ') !=
     NVL(:NEW.grand_res_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_COST',
       :OLD.grand_res_cost, :NEW.grand_res_cost);
  END IF;
  IF NVL(:OLD.grand_res_totalcost,' ') !=
     NVL(:NEW.grand_res_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_TOTALCOST',
       :OLD.grand_res_totalcost, :NEW.grand_res_totalcost);
  END IF;
  IF NVL(:OLD.grand_soc_cost,' ') !=
     NVL(:NEW.grand_soc_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_COST',
       :OLD.grand_soc_cost, :NEW.grand_soc_cost);
  END IF;
  IF NVL(:OLD.grand_soc_totalcost,' ') !=
     NVL(:NEW.grand_soc_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_TOTALCOST',
       :OLD.grand_soc_totalcost, :NEW.grand_soc_totalcost);
  END IF;
  IF NVL(:OLD.grand_total_cost,' ') !=
     NVL(:NEW.grand_total_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_COST',
       :OLD.grand_total_cost, :NEW.grand_total_cost);
  END IF;
  IF NVL(:OLD.grand_total_totalcost,' ') !=
     NVL(:NEW.grand_total_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_TOTALCOST',
       :OLD.grand_total_totalcost, :NEW.grand_total_totalcost);
  END IF;
  IF NVL(:OLD.grand_salary_cost,' ') !=
     NVL(:NEW.grand_salary_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SALARY_COST',
       :OLD.grand_salary_cost, :NEW.grand_salary_cost);
  END IF;
  IF NVL(:OLD.grand_salary_totalcost,' ') !=
     NVL(:NEW.grand_salary_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SALARY_TOTALCOST',
       :OLD.grand_salary_totalcost, :NEW.grand_salary_totalcost);
  END IF;
  IF NVL(:OLD.grand_fringe_cost,' ') !=
     NVL(:NEW.grand_fringe_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_FRINGE_COST',
       :OLD.grand_fringe_cost, :NEW.grand_fringe_cost);
  END IF;
  IF NVL(:OLD.grand_fringe_totalcost,' ') !=
     NVL(:NEW.grand_fringe_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_FRINGE_TOTALCOST',
       :OLD.grand_fringe_totalcost, :NEW.grand_fringe_totalcost);
  END IF;
  IF NVL(:OLD.grand_disc_cost,' ') !=
     NVL(:NEW.grand_disc_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_DISC_COST',
       :OLD.grand_disc_cost, :NEW.grand_disc_cost);
  END IF;
  IF NVL(:OLD.grand_disc_totalcost,' ') !=
     NVL(:NEW.grand_disc_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_DISC_TOTALCOST',
       :OLD.grand_disc_totalcost, :NEW.grand_disc_totalcost);
  END IF;
  IF NVL(:OLD.grand_ind_cost,' ') !=
     NVL(:NEW.grand_ind_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_IND_COST',
       :OLD.grand_ind_cost, :NEW.grand_ind_cost);
  END IF;
  IF NVL(:OLD.grand_ind_totalcost,' ') !=
     NVL(:NEW.grand_ind_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_IND_TOTALCOST',
       :OLD.grand_ind_totalcost, :NEW.grand_ind_totalcost);
  END IF;
  IF NVL(:OLD.grand_res_sponsor,' ') !=
     NVL(:NEW.grand_res_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_SPONSOR',
       :OLD.grand_res_sponsor, :NEW.grand_res_sponsor);
  END IF;
  IF NVL(:OLD.grand_res_variance,' ') !=
     NVL(:NEW.grand_res_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_VARIANCE',
       :OLD.grand_res_variance, :NEW.grand_res_variance);
  END IF;
  IF NVL(:OLD.grand_soc_sponsor,' ') !=
     NVL(:NEW.grand_soc_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_SPONSOR',
       :OLD.grand_soc_sponsor, :NEW.grand_soc_sponsor);
  END IF;
  IF NVL(:OLD.grand_soc_variance,' ') !=
     NVL(:NEW.grand_soc_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_VARIANCE',
       :OLD.grand_soc_variance, :NEW.grand_soc_variance);
  END IF;
  IF NVL(:OLD.grand_total_sponsor,' ') !=
     NVL(:NEW.grand_total_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_SPONSOR',
       :OLD.grand_total_sponsor, :NEW.grand_total_sponsor);
  END IF;
  IF NVL(:OLD.grand_total_variance,' ') !=
     NVL(:NEW.grand_total_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_VARIANCE',
       :OLD.grand_total_variance, :NEW.grand_total_variance);
  END IF;

  IF NVL(:OLD.bgtcal_sp_overhead,0) !=
     NVL(:NEW.bgtcal_sp_overhead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SP_OVERHEAD',
       :OLD.bgtcal_sp_overhead, :NEW.bgtcal_sp_overhead);
  END IF;

  IF NVL(:OLD.bgtcal_sp_overhead_flag,0) !=
     NVL(:NEW.bgtcal_sp_overhead_flag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SP_OVERHEAD_FLAG',
       :OLD.bgtcal_sp_overhead_flag, :NEW.bgtcal_sp_overhead_flag);
  END IF;


END;