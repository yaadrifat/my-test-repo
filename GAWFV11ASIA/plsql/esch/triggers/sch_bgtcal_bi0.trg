CREATE  OR REPLACE TRIGGER SCH_BGTCAL_BI0 BEFORE INSERT ON SCH_BGTCAL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_BGTCAL', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'BGTCAL_CLINICFLAG', :NEW.BGTCAL_CLINICFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_CLINICOHEAD', :NEW.BGTCAL_CLINICOHEAD);
       Audit_Trail.column_insert (raid, 'BGTCAL_DELFLAG', :NEW.BGTCAL_DELFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_DISCOUNT', :NEW.BGTCAL_DISCOUNT);
       Audit_Trail.column_insert (raid, 'BGTCAL_DISCOUNTFLAG', :NEW.BGTCAL_DISCOUNTFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_EXCLDSOCFLAG', :NEW.BGTCAL_EXCLDSOCFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_FRGBENEFIT', :NEW.BGTCAL_FRGBENEFIT);
       Audit_Trail.column_insert (raid, 'BGTCAL_FRGFLAG', :NEW.BGTCAL_FRGFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_INDIRECTCOST', :NEW.BGTCAL_INDIRECTCOST);
       Audit_Trail.column_insert (raid, 'BGTCAL_PROTID', :NEW.BGTCAL_PROTID);
       Audit_Trail.column_insert (raid, 'BGTCAL_PROTTYPE', :NEW.BGTCAL_PROTTYPE);
       Audit_Trail.column_insert (raid, 'BGTCAL_SPONSORFLAG', :NEW.BGTCAL_SPONSORFLAG);
       Audit_Trail.column_insert (raid, 'BGTCAL_SPONSOROHEAD', :NEW.BGTCAL_SPONSOROHEAD);
       Audit_Trail.column_insert (raid, 'BGTCAL_SP_OVERHEAD', :NEW.BGTCAL_SP_OVERHEAD);
       Audit_Trail.column_insert (raid, 'BGTCAL_SP_OVERHEAD_FLAG', :NEW.BGTCAL_SP_OVERHEAD_FLAG);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_BUDGET', :NEW.FK_BUDGET);
       Audit_Trail.column_insert (raid, 'GRAND_DISC_COST', :NEW.GRAND_DISC_COST);
       Audit_Trail.column_insert (raid, 'GRAND_DISC_TOTALCOST', :NEW.GRAND_DISC_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_FRINGE_COST', :NEW.GRAND_FRINGE_COST);
       Audit_Trail.column_insert (raid, 'GRAND_FRINGE_TOTALCOST', :NEW.GRAND_FRINGE_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_IND_COST', :NEW.GRAND_IND_COST);
       Audit_Trail.column_insert (raid, 'GRAND_IND_TOTALCOST', :NEW.GRAND_IND_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_RES_COST', :NEW.GRAND_RES_COST);
       Audit_Trail.column_insert (raid, 'GRAND_RES_SPONSOR', :NEW.GRAND_RES_SPONSOR);
       Audit_Trail.column_insert (raid, 'GRAND_RES_TOTALCOST', :NEW.GRAND_RES_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_RES_VARIANCE', :NEW.GRAND_RES_VARIANCE);
       Audit_Trail.column_insert (raid, 'GRAND_SALARY_COST', :NEW.GRAND_SALARY_COST);
       Audit_Trail.column_insert (raid, 'GRAND_SALARY_TOTALCOST', :NEW.GRAND_SALARY_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_SOC_COST', :NEW.GRAND_SOC_COST);
       Audit_Trail.column_insert (raid, 'GRAND_SOC_SPONSOR', :NEW.GRAND_SOC_SPONSOR);
       Audit_Trail.column_insert (raid, 'GRAND_SOC_TOTALCOST', :NEW.GRAND_SOC_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_SOC_VARIANCE', :NEW.GRAND_SOC_VARIANCE);
       Audit_Trail.column_insert (raid, 'GRAND_TOTAL_COST', :NEW.GRAND_TOTAL_COST);
       Audit_Trail.column_insert (raid, 'GRAND_TOTAL_SPONSOR', :NEW.GRAND_TOTAL_SPONSOR);
       Audit_Trail.column_insert (raid, 'GRAND_TOTAL_TOTALCOST', :NEW.GRAND_TOTAL_TOTALCOST);
       Audit_Trail.column_insert (raid, 'GRAND_TOTAL_VARIANCE', :NEW.GRAND_TOTAL_VARIANCE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_BGTCAL', :NEW.PK_BGTCAL);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
