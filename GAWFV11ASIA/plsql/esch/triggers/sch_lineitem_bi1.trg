CREATE OR REPLACE TRIGGER "SCH_LINEITEM_BI1"
BEFORE INSERT
ON SCH_LINEITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   v_costcodeid  number;
   v_category  number;
   v_soccodeid number;
   v_bgtcal_excldsocflag number;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_LINEITEM_BI1', pLEVEL  => Plog.LFATAL);

BEGIN
   --preset values if null

    SELECT pk_codelst INTO v_soccodeid
    FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
    AND LOWER(codelst_subtyp)='stdcare_cost';

   -- get exclude soc flag

   select nvl(bgtcal_excldsocflag,0)
   into v_bgtcal_excldsocflag
   from sch_bgtcal,sch_bgtsection
   where pk_budgetsec = :new.fk_bgtsection and fk_bgtcal = pk_bgtcal;



if ( :NEW.FK_CODELST_CATEGORY is null or :NEW.FK_CODELST_CATEGORY = 0 ) then

    SELECT  pk_codelst  INTO v_category FROM  SCH_CODELST
    WHERE trim (codelst_type) = 'category' AND trim (codelst_subtyp) = 'ctgry_patcare';

    :NEW.FK_CODELST_CATEGORY := v_category;



end if;

if (:new.fk_codelst_cost_type is null or :new.fk_codelst_cost_type =  0) then

    SELECT pk_codelst INTO v_costcodeid
    FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
    AND LOWER(codelst_subtyp)='res_cost';

     :new.fk_codelst_cost_type :=  v_costcodeid;

 end if;


if :NEW.LINEITEM_SPONSORUNIT is null then
    :new.LINEITEM_SPONSORUNIT := '0.0';
end if;


if :NEW.LINEITEM_CLINICNOFUNIT is null then
    :new.LINEITEM_CLINICNOFUNIT := '1.0';
end if;

if :NEW.LINEITEM_OTHERCOST is null then
    :new.LINEITEM_OTHERCOST := nvl((to_char(to_number(:new.LINEITEM_SPONSORUNIT ) * to_number(:new.LINEITEM_CLINICNOFUNIT) ) ), '0.0');
end if;

    -- if SOC and exclude SOC is set

if (:new.fk_codelst_cost_type = v_soccodeid and v_bgtcal_excldsocflag = 1) then
    :new.LINEITEM_OTHERCOST := '0.0';
end if;

END;
/


