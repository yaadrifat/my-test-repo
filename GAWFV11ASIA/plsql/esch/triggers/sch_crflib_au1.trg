CREATE OR REPLACE TRIGGER "SCH_CRFLIB_AU1" 
AFTER UPDATE OF CRFLIB_NAME,CRFLIB_NUMBER
ON SCH_CRFLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN
--if the user changes crf data of a calendar event associated with a study
--then update these changes in sch_crf table also so that they are reflected in the schedule
update sch_crf
set crf_number = :new.crflib_number,
crf_name = :new.crflib_name
where crf_origlibid = :new.pk_crflib;
END;
/


