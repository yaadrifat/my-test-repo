CREATE  OR REPLACE TRIGGER EVENT_DEF_AD0 AFTER DELETE ON EVENT_DEF        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'EVENT_DEF', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CALENDAR_SHAREDWITH', :OLD.CALENDAR_SHAREDWITH);
       Audit_Trail.column_delete (raid, 'CHAIN_ID', :OLD.CHAIN_ID);
       Audit_Trail.column_delete (raid, 'COST', :OLD.COST);
       Audit_Trail.column_delete (raid, 'COST_DESCRIPTION', :OLD.COST_DESCRIPTION);
       Audit_Trail.column_delete (raid, 'COVERAGE_NOTES', :OLD.COVERAGE_NOTES);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'DESCRIPTION', :OLD.DESCRIPTION);
       Audit_Trail.column_delete (raid, 'DISPLACEMENT', :OLD.DISPLACEMENT);
       Audit_Trail.column_delete (raid, 'DURATION', :OLD.DURATION);
       Audit_Trail.column_delete (raid, 'DURATION_UNIT', :OLD.DURATION_UNIT);
       Audit_Trail.column_delete (raid, 'EVENT_CATEGORY', :OLD.EVENT_CATEGORY);
       Audit_Trail.column_delete (raid, 'EVENT_CPTCODE', :OLD.EVENT_CPTCODE);
       Audit_Trail.column_delete (raid, 'EVENT_DURATIONAFTER', :OLD.EVENT_DURATIONAFTER);
       Audit_Trail.column_delete (raid, 'EVENT_DURATIONBEFORE', :OLD.EVENT_DURATIONBEFORE);
       Audit_Trail.column_delete (raid, 'EVENT_FLAG', :OLD.EVENT_FLAG);
       Audit_Trail.column_delete (raid, 'EVENT_FUZZYAFTER', :OLD.EVENT_FUZZYAFTER);
       Audit_Trail.column_delete (raid, 'EVENT_ID', :OLD.EVENT_ID);
       Audit_Trail.column_delete (raid, 'EVENT_LIBRARY_TYPE', :OLD.EVENT_LIBRARY_TYPE);
       Audit_Trail.column_delete (raid, 'EVENT_LINE_CATEGORY', :OLD.EVENT_LINE_CATEGORY);
       Audit_Trail.column_delete (raid, 'EVENT_MSG', :OLD.EVENT_MSG);
       Audit_Trail.column_delete (raid, 'EVENT_RES', :OLD.EVENT_RES);
       Audit_Trail.column_delete (raid, 'EVENT_SEQUENCE', :OLD.EVENT_SEQUENCE);
       Audit_Trail.column_delete (raid, 'EVENT_TYPE', :OLD.EVENT_TYPE);
       Audit_Trail.column_delete (raid, 'FACILITY_ID', :OLD.FACILITY_ID);
       Audit_Trail.column_delete (raid, 'FK_CATLIB', :OLD.FK_CATLIB);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_CALSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CALSTAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_COVERTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_COVERTYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_VISIT', :OLD.FK_VISIT);
       Audit_Trail.column_delete (raid, 'FUZZY_PERIOD', :OLD.FUZZY_PERIOD);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'LINKED_URI', :OLD.LINKED_URI);
       Audit_Trail.column_delete (raid, 'MSG_TO', :OLD.MSG_TO);
       Audit_Trail.column_delete (raid, 'NAME', :OLD.NAME);
       Audit_Trail.column_delete (raid, 'NOTES', :OLD.NOTES);
       Audit_Trail.column_delete (raid, 'ORG_ID', :OLD.ORG_ID);
       Audit_Trail.column_delete (raid, 'PAT_DAYSAFTER', :OLD.PAT_DAYSAFTER);
       Audit_Trail.column_delete (raid, 'PAT_DAYSBEFORE', :OLD.PAT_DAYSBEFORE);
       Audit_Trail.column_delete (raid, 'PAT_MSGAFTER', :OLD.PAT_MSGAFTER);
       Audit_Trail.column_delete (raid, 'PAT_MSGBEFORE', :OLD.PAT_MSGBEFORE);
       Audit_Trail.column_delete (raid, 'REASON_FOR_COVERAGECHANGE', :OLD.REASON_FOR_COVERAGECHANGE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SERVICE_SITE_ID', :OLD.SERVICE_SITE_ID);
       Audit_Trail.column_delete (raid, 'STATUS', :OLD.STATUS);
       Audit_Trail.column_delete (raid, 'USER_ID', :OLD.USER_ID);
       Audit_Trail.column_delete (raid, 'USR_DAYSAFTER', :OLD.USR_DAYSAFTER);
       Audit_Trail.column_delete (raid, 'USR_DAYSBEFORE', :OLD.USR_DAYSBEFORE);
       Audit_Trail.column_delete (raid, 'USR_MSGAFTER', :OLD.USR_MSGAFTER);
       Audit_Trail.column_delete (raid, 'USR_MSGBEFORE', :OLD.USR_MSGBEFORE);
COMMIT;
END;
/