CREATE OR REPLACE TRIGGER "SCH_CRFLIB_AI1" 
AFTER INSERT
ON SCH_CRFLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
nvl(new.crflib_formflag,0) = 0
      )
BEGIN
--if the user adds new crf data of a calendar event associated with a study
--then insert new entries for sch_crf table also so that they are reflected in the schedule
 pkg_gensch.sp_transfer_crf_to_schedule ( :new.PK_CRFLIB, :new.FK_EVENTS, :new.CRFLIB_NUMBER,:new.CRFLIB_NAME,
 :new.CREATOR, :new.IP_ADD, 0 );
END;
/


