CREATE OR REPLACE TRIGGER "SCH_CRFSTAT_AI0"
AFTER INSERT
ON SCH_CRFSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
Declare
   V_STR      varchar2 (2001) ;
   V_SQLSTR   varchar2 (4000);
   V_PARAMS   varchar2 (2001) ;
   V_POS      number         := 0;
   V_CNT      number         := 0;
   V_CRFUSERTO varchar2(2000) ;
   v_patprot number ;
   v_event char(10) ;
   v_msg varchar2(4000) ;


   v_patcode varchar2(25);
   v_fparam varchar2(200);
   v_eventname varchar2(100);
   v_schdate varchar2(12);
   v_visit Number;
   v_msgtemplate varchar2(4000);
   v_crfnum varchar2(100);
   v_newstat varchar2(300);
   v_oldstat varchar2(300);
Begin

/*

1. if that status changes , check if the new status record is present in
sch_crfnotify columns (fk_csr and fk_codelst crf stat ) - get crfnot_userto - list of users, fk_crf

2. get pat prot - start from sch_crfstat :new.fk_crf
 link to sch_crf.pk_crf
   link to sch_crf.fk_events1=sch_events1.event1

3. get message -- select msgtxt_long from sch_msgtxt where msgtxt_type = 'crf_stat' ;

4. move to dispatchmsg - insert into sch_dispatchmsg (msgtxt , some date)
    the users to send are in the crfnot_userto in a comma separated list

*/

--get last crf status from sch_crf
v_oldstat := 'No Status';
Begin
       select codelst_desc
       into v_oldstat
       from sch_crf, sch_codelst
       where pk_crf = :new.fk_crf  and pk_codelst = crf_curstat;
   Exception When NO_DATA_FOUND then
  v_oldstat := 'No Status';
End ;


update sch_crf
set crf_curstat =  :new.FK_CODELST_CRFSTAT
Where pk_crf = :new.fk_crf ;

Begin

 v_msgtemplate  :=    PKG_COMMON.SCH_GETMAILMSG('crf_stat'); --get message template

  FOR  i in   (
          select CRFNOT_USERSTO, sch_crfnotify.fk_patprot, sch_crf.fk_events1, sch_events1.description,
          to_char(sch_events1.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE, sch_crf.CRF_NUMBER CRF_NUMBER, visit
          from sch_crfnotify , sch_crf , sch_events1
          where FK_CRF = :new.fk_crf
          and FK_CODELST_NOTSTAT = :new.FK_CODELST_CRFSTAT
          and sch_crfnotify.fk_crf = sch_crf.pk_crf
         and sch_crf.fk_events1 = sch_events1.event_id
	    UNION
          select CRFNOT_USERSTO, sch_crfnotify.fk_patprot, sch_crf.fk_events1, sch_events1.description,
          to_char(sch_events1.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE, sch_crf.CRF_NUMBER CRF_NUMBER, visit
          from sch_crfnotify , sch_crf , sch_events1
          where FK_CRF is null
          and FK_CODELST_NOTSTAT = :new.FK_CODELST_CRFSTAT
          and :new.fk_crf = sch_crf.pk_crf and
          sch_crf.fk_patprot = sch_crfnotify.fk_patprot
         and sch_crf.fk_events1 = sch_events1.event_id
    )
  LOOP

     v_crfuserto := i.CRFNOT_USERSTO;
     v_patprot := i.fk_patprot;
     v_event := i.fk_events1;
     v_eventname := i.description;
     v_schdate := i.ACTUAL_SCHDATE;
     v_crfnum := i.CRF_NUMBER;
     v_visit := i.visit;

  	Select  PKG_COMMON.SCH_GETPATCODE(v_patprot)
     into v_patcode
	from dual;        -- v_patcode is param 1

     select codelst_desc
     into v_newstat
     from sch_codelst
     where pk_codelst =  :new.FK_CODELST_CRFSTAT ;


      v_str    := v_CRFUSERTO || ',' ;
      v_params := v_CRFUSERTO || ',' ;

      LOOP
        V_CNT := V_CNT + 1;
        V_POS := instr (V_STR, ',');
        V_PARAMS := substr (V_STR, 1, V_POS - 1);
        exit when V_PARAMS Is null;

  	   v_fparam :=  v_patcode ||'~' || nvl(to_char(v_visit),'Not Known') ||'~'|| v_eventname ||'~'|| v_schdate ||'~'|| v_crfnum ||'~'|| v_oldstat ||'~'|| v_newstat;
        v_msg :=    PKG_COMMON.SCH_GETMAIL(v_msgtemplate,v_fparam);


      Insert into sch_dispatchmsg (
         PK_MSG                 ,
         MSG_SENDON             ,
         MSG_STATUS             ,
         MSG_TYPE               ,
         FK_SCHEVENT               ,
         MSG_TEXT               ,
         CREATOR                ,
         IP_ADD                 ,
         FK_PATPROT        ,
        FK_PAT )
      Values (
        SCH_DISPATCHMSG_SEQ1.nextval ,
        sysdate ,
        0,
       'U',
       v_event ,
       v_msg ,
      :new.creator,
      :new.ip_add,
      v_patprot,
      V_PARAMS) ;

      V_STR := substr (V_STR, V_POS + 1);
    END LOOP ;
  END LOOP;
    Exception When NO_DATA_FOUND then
  null;
End ;
End ;
/


