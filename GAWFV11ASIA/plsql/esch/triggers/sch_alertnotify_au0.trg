CREATE OR REPLACE TRIGGER SCH_ALERTNOTIFY_AU0
AFTER UPDATE
ON SCH_ALERTNOTIFY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_ALERTNOTIFY', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_alnot,0) !=
      NVL(:NEW.pk_alnot,0) THEN
      audit_trail.column_update
        (raid, 'PK_ALNOT',
        :OLD.pk_alnot, :NEW.pk_alnot);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.fk_codelst_an,0) !=
      NVL(:NEW.fk_codelst_an,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_AN',
        :OLD.fk_codelst_an, :NEW.fk_codelst_an);
   END IF;
   IF NVL(:OLD.alnot_type,' ') !=
      NVL(:NEW.alnot_type,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_TYPE',
        :OLD.alnot_type, :NEW.alnot_type);
   END IF;
   IF NVL(:OLD.alnot_users,' ') !=
      NVL(:NEW.alnot_users,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_USERS',
        :OLD.alnot_users, :NEW.alnot_users);
   END IF;
   IF NVL(:OLD.alnot_mobeve,' ') !=
      NVL(:NEW.alnot_mobeve,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_MOBEVE',
        :OLD.alnot_mobeve, :NEW.alnot_mobeve);
   END IF;
   IF NVL(:OLD.alnot_flag,0) !=
      NVL(:NEW.alnot_flag,0) THEN
      audit_trail.column_update
        (raid, 'ALNOT_FLAG',
        :OLD.alnot_flag, :NEW.alnot_flag);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL',
        :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.alnot_globalflag,' ') !=
      NVL(:NEW.alnot_globalflag,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_GLOBALFLAG',
        :OLD.alnot_globalflag, :NEW.alnot_globalflag);
   END IF;

END;
/


