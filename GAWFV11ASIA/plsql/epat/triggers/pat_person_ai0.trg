CREATE OR REPLACE TRIGGER "PAT_PERSON_AI0" 
AFTER INSERT
ON PERSON REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:  Vishal Abrol
   PURPOSE:   To insert rows in table er_per for each insertion in table
   person.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/3/2005             1. Created this trigger.
   1.1        09/23/2005         1. insert a row in patient facility for default patient facility

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:    ER_PERSON_AI0
      Sysdate:         5/3/2005
      Date and Time:   5/3/2005, 4:55:13 PM, and 5/3/2005 4:55:13 PM
      ******************************************************************************/
BEGIN
   tmpVar := 0;
   INSERT INTO ER_PER (PK_PER,PER_CODE, FK_ACCOUNT, FK_SITE, CREATOR, CREATED_ON, IP_ADD,
	FK_TIMEZONE) VALUES
   (:NEW.PK_PERSON,:NEW.PERSON_CODE,:NEW.FK_ACCOUNT,:NEW.FK_SITE,:NEW.CREATOR,:NEW.CREATED_ON
   ,:NEW.IP_ADD,:NEW.FK_TIMEZONE);

   -- insert a record in er_patfacility for the default patient site
   INSERT INTO ER_PATFACILITY (pk_patfacility,fk_per , fk_site , pat_facilityid ,  patfacility_regdate ,
  patfacility_provider ,  patfacility_otherprovider ,  patfacility_splaccess ,  patfacility_accessright ,
  patfacility_default ,IP_ADD,CREATOR)
  VALUES (  seq_er_patfacility.NEXTVAL,:NEW.PK_PERSON, :NEW.FK_SITE,:NEW.PAT_FACILITYID,trunc(SYSDATE), :NEW.person_regby,
  :NEW.PERSON_PHYOTHER, :NEW.PERSON_SPLACCESS,7,1,:NEW.IP_ADD ,:NEW.CREATOR);


   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


