bunyan=require('bunyan');

var log=bunyan.createLogger({name:"nodeLogger",streams:[{level:"info",type:"rotating-file",path:"../log/logfile_info.log",period:"1d",rotateExisting:!0,threshold:"10m",totalSize:"20m",count:5},{level:"error",type:"rotating-file",path:"../log/logfile_error.log",period:"1d",rotateExisting:!0,threshold:"10m",totalSize:"20m",count:5}]});

module.exports = log;