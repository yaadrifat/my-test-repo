/**
 * Created by kkumar on 9/8/2016.
 */

var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var url = config.espURL+'/webservices/studycalendarservice?wsdl';

/* Get Study Calendar Budget by looking up Study OID and Calender OID */
router.post('/', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(url,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
         * {
         "StudyIdentifier": {"OID": "fae6514e-705f-45ba-9e0d-036cbae5d6f6"},
         "CalendarIdentifier":{"OID":"8143ebd3-8038-4ef7-be95-389b20fdd63e"}
         }
         * */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getStudyCalendar(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

router.post('/getStudyCalendarList', function(req, res, next) {
var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
	
	//Soap client code to update patient demographic data
    soap.createClient(url,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
         * {
         "StudyIdentifier": {"OID": "fae6514e-705f-45ba-9e0d-036cbae5d6f6"},
         "CalendarIdentifier":{"OID":"8143ebd3-8038-4ef7-be95-389b20fdd63e"}
         }
         * */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        client.getStudyCalendarList(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

module.exports = router;
