/**
 * Created by kkumar on 8/29/2016.
 */

var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var url = config.espURL+'/webservices/patientdemographicsservice?wsdl';
var urlBizdev3 = config.espURL+'/webservices/patientdemographicsservice?wsdl';

/* Update Patient (PatientId/Demographics/Organizations). */
router.post('/updatePatient', function (req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(url, options, function (err, client) {
        if (err) throw err;

        var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*{
         "PatientIdentifier": {
         "patientId":"Test9876",
         "organizationId":{
         "siteName":"UTHSCSA"
         }
         },
         "PatientDemographics": {
         "address1": "2201 Walnut Ave",
         "address2": "Fremont, CA 94538",
         "city": "USA",
         "zipCode": "94539",
         "workPhone": "5104892832",
         "homePhone": "1234567890",
         "dateOfBirth": "sfd",
         "firstName": "Karthik",
         "middleName": "-MakeItWork4mWS-",
         "lastName": "Kumar",
         "eMail": "Something@Somewhere.grr",
         "race": { "code": "race_indala" },
         "gender": {
         "code": "female",
         "description": "Female",
         "type": "gender"
         },
         "organization": {
         "OID": "62d8c2bb-53bd-412b-b54f-a3ad83ebd08f",
         "PK": "53",
         "siteName": "UTHSCSA"
         },
         "reasonForChange": "Really??"
         }
         }*/

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.updatePatient(args1, function (err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });


});


/* Update Patient (PatientId/Demographics/Organizations). */
router.post('/PatientSearch/Name', function (req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(url, options, function (err, client) {
        if (err) throw err;

        var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
            {
                "PatientSearch":{
                    "patFirstName":"Karthik",
                    "patLastName":"Suresh"
                    }
            }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.searchPatient(args1, function (err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });


});




/*Get patient details from bizdev 3 eSP */
router.post('/bizdev3/getPatientDetails', function (req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(urlBizdev3, options, function (err, client) {
        if (err) throw err;

        var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*{
         "PatientIdentifier": {
         PK: 254
         }*/

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getPatientDetails(args1, function (err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });


}
)
;

module.exports = router;
