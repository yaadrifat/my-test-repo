/**
 * Created by kkumar on 9/6/2016.
 */

var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var url = config.espURL+'/webservices/budgetservice?wsdl';

/* Get Study Calendar Budget by looking up Study OID and Calender OID */
router.post('/', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(url,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };

        var wsSecurity = new WSSecurity('velosadmin', 'velos123',options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
        * {
         "StudyIdentifier": {"OID": "fae6514e-705f-45ba-9e0d-036cbae5d6f6"},
         "CalendarNameIdentifier":{"OID":"4dc25423-5206-42ae-aca1-93180b3fe6ba"}
         }
        * */

        //console.log("Search for this::" + req.body);
        var args1 = req.body;
        //console.log(req.body);
        client.getStudyCalBudget(args1, function(err, result) {
            console.log(result);
            console.log(err);
            //res.render('Study', result);
            res.send(result);
        });
    });



});

module.exports = router;
