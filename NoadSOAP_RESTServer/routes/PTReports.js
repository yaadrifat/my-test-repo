/**
 * This route was created to access a specific webservice on
 * PTReports to expose the esmaple data from multiple
 * customer sites for a POC demo for the 2017 velos annual meeting
 *
 * Created by kkumar on 4/5/2017.
 */

var express = require('express');
var bodyParser = require("body-parser");
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var urlPTReports = 'https://ptreports.veloseresearch.com:8444/ConvergePOCWS/ConvergeWSService?wsdl';


/*Get list of biospecimen samples based on selection criteria */
router.post('/getBioSpecimen', function (req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(urlPTReports, options, function (err, client) {
        if (err) throw err;

        var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};

        //var wsSecurity = new WSSecurity('user', 'pass', options);

        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        //client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
         {
         "Biospecimen":{
         "CONVERGE_SITE_ID":"?",
         "RECID":"?",
         "SPEC_ID":"?",
         "PARENT_SPEC_ID":"?",
         "SPEC_TYPE":"?",
         "SPEC_DESCRIPTION":"?",
         "SPEC_ANATOMIC_SITE":"?",
         "SPEC_PATHOLOGY_STAT":"?",
         "COLL_SPEC_QTY":"?",
         "COLL_QTY_UNITS":"?",
         "CURRENT_SPEC_QTY":"?",
         "CURRENT_SPEC_QTY_UNITS":"?",
         "STATUS_DESC":"?",
         "SPEC_ORGANIZATION":"?",
         "OWNER":"?",
         "DateRange":{
         "CREATED_ON_GENERAL":{
         "Start":"?",
         "End":"?"
         },
         "LAST_MODIFIED_DATE_GENERAL":{
         "Start":"?",
         "End":"?"
         }
         },
         "ResultRange":{
         "start":"20000",
         "noofrows":"20100"
         }
         }
         }
         */

        //console.log("Search for this::" + req.body);
        var args1 = req.body;
        //console.log(req.body);
        client.getBiospecimen(args1, function (err, result) {
            console.log(result);
            console.log(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

module.exports = router;


