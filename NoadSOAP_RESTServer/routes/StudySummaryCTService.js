/**
 * Created by kkumar on 8/23/2016.
 */
var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var restClient = require('node-rest-client').Client;
var unirest = require('unirest');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var CTStudyServiceURL = config.studySummaryWADL;

/* GET Study from Study OID listing. */
router.get('/getStudySummary', function(req, res, next) {
	//console.log('Hello');
	//var token = req.body.jwtToken;//req.query.jwttoken;
	//var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
	//console.log('username '+userDecoded.info.userId+' password '+userDecoded.info.password);
    var url = CTStudyServiceURL;
	//console.log(url);
	//res.status(200).json(url);
    var args = req.query.nctNumber;//req.body.nctNumber;
	url = url+args+"?displayxml=true";
	console.log(args);
	log.info(args);
	/*unirest.get(url)
	.header('Accept', 'application/json')
	.end(function (response) {
	   var xmlData = response.body;
	   xmlData = xmlData.replace(/\r\n/g, "");
		res.status(200).json(xmlData);
		});*/
	var client = new restClient();
    var req = client.get(url, function (data, response) {
    // parsed response body as js object 
	log.info(data);
	if(data.clinical_study)
			res.send(data);
	else{	data={};
			data["clinical_study"]="not_found";
			res.send(data);
			}
			console.log(data.clinical_study)
    // raw response 
    //console.log(response);
	});
 
	req.on('requestTimeout', function (req) {
	log.error(req);
    console.log('request has expired');
    req.abort();
	});
 
	req.on('responseTimeout', function (res) {
	log.error(res);
    console.log('response has expired');
	});
 
	//it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
	req.on('error', function (err) {
	log.error(err);
    console.log('request error'+err);
	});
});

module.exports = router;