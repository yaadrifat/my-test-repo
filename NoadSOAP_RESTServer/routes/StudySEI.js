/**
 * Created by kkumar on 8/23/2016.
 */
var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var StudyServiceURL = config.espURL+'/webservices/studyservice?wsdl';

/* GET Study Summary from Study OID listing. */
router.post('/getStudy', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(StudyServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
            StudyIdentifier: {studyNumber: 'MutipleVersionandCalendar'}
        };*/

        log.info("Search for this studysei::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getStudySummary(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});


/* create study using study Object (other filters possible). */
router.post('/createStudy', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(StudyServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
             StudySearch:
              {
                    studyNumber:Test_Study
              }
         };
         */
		log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.createStudy(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* Update study using study Object (other filters possible). */
router.post('/updateStudySummary', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(StudyServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
             StudySearch:
              {
                    studyNumber:Test_Study
              }
         };
         */
		log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.updateStudySummary(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});


module.exports = router;