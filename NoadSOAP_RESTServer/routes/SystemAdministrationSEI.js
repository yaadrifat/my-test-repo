/**
 * Created by kkumar on 8/29/2016.
 */
var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var url = config.espURL+'/webservices/systemadministrationservice?wsdl';

/* GET users listing. */
router.get('/getGenderList', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(url,options,function(err, client) {
        if(err) throw err;
        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        var args = {
            type:'gender'
        };

        log.info("Search for this::" + req.body);
        //var args1 = req.body;
        //console.log(req.body);
        client.getCodeList(args, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result.Codes);
        });
    });
});


/* Get Code list types */
router.get('/getCodeTypes', function (req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }

    //Soap client code to update patient demographic data
    soap.createClient(url, options, function (err, client) {
        if (err) throw err;

        var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
         {
         "PatientSearch":{
         "patFirstName":"Karthik",
         "patLastName":"Suresh"
         }
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = {};
        //console.log(req.body);
        client.getCodeTypes(args1, function (err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });


});


/* Get code list */
router.post('/getCodeList', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(url,options,function(err, client) {
        if(err) throw err;
        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
            type:'gender'
        };*/

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getCodeList(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});




module.exports = router;