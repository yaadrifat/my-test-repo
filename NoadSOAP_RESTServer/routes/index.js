var express = require('express');
var config = require('../config');
var log = require('../bunyan');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  log.info('Node REST Started...');
  res.render('index', { title: 'Node REST test' });
});

module.exports = router;
