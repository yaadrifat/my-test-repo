/**
 * Created by kkumar on 8/23/2016.
 */
var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var PatientScheduleServiceURL = config.espURL+'/webservices/patientscheduleservice?wsdl';

/* ADD new schedule event status. */
router.post('/addScheduleEventStatus', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
            var args =
        {
            "EventIdentifier":{"OID": "a2ea5b77-342b-4596-8889-5e20db594155"},
            "EventStatus":
            {
                "eventStatusCode":
                {
                    "code":"ev_done",
                    "description":"Event Done!"
                },
                "notes":"Added event Status code from WS",
                "statusValidFrom":"2016-10-12T00:23:54"
             }
         }

         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.addScheduleEventStatus(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* get patient schedule */
router.post('/getPatientSchedule', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args =
        {
             "ScheduleIdentifier":
             {
                "OID":"63edb0b8-a514-4e05-abfc-865fc9e247a5",
                "patientIdentifier":
                {
                    "patientId":"Test9876"
                },
                "studyIdentifier":
                {
                    "studyNumber":"Part_10"
                }
             }
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getPatientSchedule(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* get patient schedule */
router.post('/getPatientScheduleVisits', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args =
        {
             "ScheduleIdentifier":
             {
                "OID":"63edb0b8-a514-4e05-abfc-865fc9e247a5",
                "patientIdentifier":
                {
                    "patientId":"Test9876"
                },
                "studyIdentifier":
                {
                    "studyNumber":"Part_10"
                }
             }
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getPatientScheduleVisits(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});


/* get patient schedule list */
router.post('/getPatientScheduleList', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args =
         {
             "PatientIdentifier":
             {
                "patientId":"Test9876",
                "organizationId":
                 {
                    "siteName":"UTHSCSA"
                 }
             },

             "StudyIdentifier":
             {
                "OID":"f3f74aad-9e79-420b-9576-f4f670fa2db1"
             }
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getPatientScheduleList(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});


/* Update event status */
router.post('/updateMEventStatus', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args =
        {
             "ScheduleEventStatuses":
             {
                 "scheduleEventStatInfo":
                 {
                     "eventIdentifier":
                     {
                        "OID":"a2ea5b77-342b-4596-8889-5e20db594155"
                     },
                     "scheduleEventStatus":
                     {
                         "notes":"Updated event status from WS",
                         "statusValidFrom":"2016-09-05T00:00:00"
                     }
                 }
            }
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.updateMEventStatus(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* Create Patient Schedule */
router.post('/addMPatientSchedules', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(PatientScheduleServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
		var hostName = {remoteaddr:req.body.remoteaddr};
        client.setSecurity(wsSecurity);
        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.addMPatientSchedules(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        }, null, hostName);
    });
});



module.exports = router;