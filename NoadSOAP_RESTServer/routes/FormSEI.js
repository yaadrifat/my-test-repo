/**
 * Form SEI service
 * Created by kkumar on 11/03/2016.
 *
 */

var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var url = config.espURL+'/webservices/patientdemographicsservice?wsdl';
var urlBizdev3 = config.espURL+'/webservices/formservice?wsdl';


/*Get list of all patient forms from bizdev 3 eSP */
router.post('/bizdev3/getListOfPatientForms', function (req, res, next) {

        //Needed to get the correct XML namespaces included in the response
        var options = {
            ignoredNamespaces: {
                namespaces: ['targetNamespace', 'typedNamespace'],
                override: true
            }
        }

        //Soap client code to update patient demographic data
        soap.createClient(urlBizdev3, options, function (err, client) {
            if (err) throw err;

            var options = {hasTimeStamp: false, _hasTokenCreated: true, _passwordType: 'PasswordText'};

            var wsSecurity = new WSSecurity('velosadmin', 'velos123', options);
            //the 'options' object is optional and contains properties:
            //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
            //hasTimeStamp: true or false, default is true
            //hasTokenCreated: true or false, default is true
            client.setSecurity(wsSecurity);

            //Example Json paylod that will have to be submitted.
            /*{
             "PatientIdentifier": {
             PK: 254
             }*/

            //console.log("Search for this::" + req.body);
            var args1 = req.body;
            //console.log(req.body);
            client.getListOfPatientForms(args1, function (err, result) {
                console.log(result);
                console.log(err);
                //res.render('Study', result);
                res.send(result);
            });
        });
    });

module.exports = router;


