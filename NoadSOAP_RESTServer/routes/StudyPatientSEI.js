/**
 * Created by kkumar on 8/23/2016.
 */
var express = require('express');
var bodyParser = require("body-parser");
var config = require('../config');
var log = require('../bunyan');
var jwt = require('jsonwebtoken');
var router = express();

//Here we are configuring express to use body-parser as middle-ware.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var soap = require('soap');
var WSSecurity = require('../node_modules/soap/lib/security/WSSecurity');

var StudyPatientServiceURL = config.espURL+'/webservices/studypatientservice?wsdl';

/* GET patients from Study OID listing. */
router.post('/getStudyPatients', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
            var args = {
            "StudyIdentifier": {"OID": "8ee1d6a4-2503-4a8b-9529-529a740e1853"}
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getStudyPatients(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* get list of patient status list */
router.post('/getStudyPatientStatusHistory', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
            "StudyIdentifier": {"OID": "f3f74aad-9e79-420b-9576-f4f670fa2db1"},
            "PatientIdentifier":{"OID":"da0e98c0-ab99-4b5f-a036-97ce3e7e3ce1"}
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getStudyPatientStatusHistory(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});


/* get study patient status details */
router.post('/getStudyPatientStatus', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
            "PatientStudyStatusIdentifier":{"OID":"186f33ec-268d-4dfb-a9e2-06f3941c7ef2"}
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.getStudyPatientStatus(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        });
    });
});

/* enroll existing patient to study */
router.post('/enrollPatientToStudy', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
         "PatientStudyStatusIdentifier":{"OID":"186f33ec-268d-4dfb-a9e2-06f3941c7ef2"}
         }
         */
		var hostName = {remoteaddr:req.body.remoteaddr};
        log.info("Enrolling Patient to study:" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.enrollPatientToStudy(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            res.send(result);
        }, null, hostName);
    });
});

/* enroll existing patient to study */
router.post('/createAndEnrollPatientToStudy', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    };
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*var args = {
         "PatientStudyStatusIdentifier":{"OID":"186f33ec-268d-4dfb-a9e2-06f3941c7ef2"}
         }
         */
		var hostName = {remoteaddr:req.body.remoteaddr};
        log.info("Create and Enroll Patient to study:" + req.body);
        var args1 = req.body.args;
        //console.log(req.body);
        client.createAndEnrollPatient(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            res.send(result);
        }, null, hostName);
    });
});

/* Create New Status For Study Patient. */
router.post('/addStudyPatientStatus', function(req, res, next) {

    //Needed to get the correct XML namespaces included in the response
    var options = {
        ignoredNamespaces: {
            namespaces: ['targetNamespace', 'typedNamespace'],
            override: true
        }
    }
    soap.createClient(StudyPatientServiceURL,options,function(err, client) {
        if(err) throw err;

        var options = {hasTimeStamp:false, _hasTokenCreated:true, _passwordType:'PasswordText' };
		var token = req.body.jwtToken;//req.query.jwttoken;
		var userDecoded = jwt.verify(token,'LongAndHardToGuessValueWithSpecialCharacters@^($%*$%');
		
        var wsSecurity = new WSSecurity(userDecoded.info.userId, userDecoded.info.password,options);
        //the 'options' object is optional and contains properties:
        //passwordType: 'PasswordDigest' or 'PasswordText' default is PasswordText
        //hasTimeStamp: true or false, default is true
        //hasTokenCreated: true or false, default is true
        client.setSecurity(wsSecurity);

        //Example Json paylod that will have to be submitted.
        /*
            var args = {
            "StudyIdentifier": {"OID": "8ee1d6a4-2503-4a8b-9529-529a740e1853"}
         }
         */

        log.info("Search for this::" + req.body);
        var args1 = req.body.args;
		var options = {};
		//console.log("Search for this::" + req.body.remoteaddr);
		var hostName = {remoteaddr:req.body.remoteaddr};
        //console.log(req.body);
        client.addStudyPatientStatus(args1, function(err, result) {
            //log.info(result);
            log.error(err);
            //res.render('Study', result);
            res.send(result);
        }, null, hostName);
    });
});

module.exports = router;