module.exports = {
    SSL_Key: 'ia.key',
	SSL_Certificate: 'ia.crt',
    espURL                            :'http://localhost:8080',
	eresAppURL						  :'http://localhost:8080',
	studySummaryWADL                  :'https://clinicaltrials.gov/show/',
	NodeServerURL					  :'https://localhost',
	NodeServerIP					  :'localhost',
	NodePort						  :443
};