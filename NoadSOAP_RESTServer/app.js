var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');
var routes = require('./routes/index');
var users = require('./routes/users');
var study = require('./routes/StudyService');
var PatientDemographics = require('./routes/PatientDemographics');
var systemAdminSEI = require('./routes/SystemAdministrationSEI');
var StudyCalBudget = require('./routes/StudyCalendarBudget');
var StudyCalendar = require('./routes/StudyCalendar');
var StudyPatient = require("./routes/StudyPatientSEI");
var PatientSchedule = require("./routes/PatientScheduleSEI");
var FormSEI = require("./routes/FormSEI");
var FormResponseSEI = require("./routes/FormResponseSEI");
var studySEI = require('./routes/StudySEI');
var studySummaryCT = require('./routes/StudySummaryCTService');

//Endpoints for Velos Ascension 2017 demo
var PTReports = require("./routes/PTReports");

/* Adding HTTPS support - 11/22/16
    //var app = express();
 */
var fs = require('fs'),
    https = require('https'),
    app = express();
//var rfs = require('rotating-file-stream');
var logDirectory = path.join(__dirname, 'log');

fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// create a rotating write stream
/*var accessLogStream = rfs('logfile.log', {
  interval: '1d', // rotate daily
  path: logDirectory
});*/

// setup the logger
//app.use(morgan('combined', {stream: accessLogStream}));
// Add headers to allow for CORS
app.use(function (req, res, next) {

  // Website you wish to allow to connect  
  var allowedOrigins = [config.eresAppURL,
   config.espURL,config.NodeServerURL];
  var origin = req.headers.origin;
  //if(allowedOrigins.indexOf(origin) > -1){
  //  res.setHeader('Access-Control-Allow-Origin', origin);
  //}
  res.setHeader('Access-Control-Allow-Origin', "*");

  //res.setHeader('Access-Control-Allow-Origin', 'http://192.168.1.222:8081');

  //,


  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  if ('OPTIONS' === req.method) {
    res.send(200);
  } else {
    next();
  }
});

/*Link up routes and URL endpoints*/
app.use('/', routes);
app.use('/users', users);
app.use('/Study',study);
app.use('/PatientDemographics',PatientDemographics);
app.use('/systemAdminSEI',systemAdminSEI);
app.use('/StudyCalendarBudget',StudyCalBudget);
app.use('/StudyCalendar',StudyCalendar);
app.use('/StudyPatient',StudyPatient);
app.use('/PatientSchedule',PatientSchedule);
app.use('/Forms',FormSEI);
app.use('/FormResponse',FormResponseSEI);
app.use('/PTReports',PTReports);
app.use('/api/v1',studySEI);
app.use('/api/v1',studySummaryCT);
app.use('/proxy', function(req, res) {
    res.sendFile(__dirname + '/proxy.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

//Turn off ssl verification
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

process.env.PORT = 8088;

https.createServer({
  //Use self signed cert
    key: fs.readFileSync(config.SSL_Key),
    cert: fs.readFileSync(config.SSL_Certificate),
	requestCert: false,
    rejectUnauthorized: false
  //Use pfx
/*    pfx: fs.readFileSync('bizdev3.pfx'),
    passphrase: 'test',
  requestCert: false,
  rejectUnauthorized: false*/

}, app).listen(config.NodePort,config.NodeServerIP);

module.exports = app;
