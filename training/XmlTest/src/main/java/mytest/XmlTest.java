package mytest;

import java.io.StringReader;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.InputSource;

public class XmlTest {
    private static XPath xpath = XPathFactory.newInstance().newXPath();
	private static final String testXml = 
					"<Node1 xmlns:ns='http://velos.com'>" +
					"  <Node2>my data</Node2>" +
					"  <ns:Node3 attr3='my attr'></ns:Node3>" +
					"</Node1>";
			

	public static void main(String[] args) {
		System.out.println("Begin----------------------");
		XmlTest test = new XmlTest();
		String node2 = test.getNode2(test.getInputSource(testXml));
		String attr3 = test.getAttr3(test.getInputSource(testXml));
		System.out.println("node2="+node2+", attr3="+attr3);
		System.out.println("End------------------------");
	}
	
	private InputSource getInputSource(String xml) {
		InputSource source = new InputSource(new StringReader(xml));
		return source;
	}
	
	private String getNode2(InputSource source) {
		String node3 = null;
		try {
	        XPathExpression expr = xpath.compile("//*[name()='Node2']");
	        node3 = expr.evaluate(source);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return node3;
	}
	
	private String getAttr3(InputSource source) {
		String attr3 = null;
		try {
	        XPathExpression expr = xpath.compile("//*[local-name()='Node3']/@attr3");
	        attr3 = expr.evaluate(source);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return attr3;
	}

}
