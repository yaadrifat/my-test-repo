package hello;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class maps incoming request paths to appropriate methods.
 *
 */
@RestController
@RequestMapping("/codelst")
public class CodelstController {
	private CodelstDao codelstDao = null; // This gets wired by Spring
	
	// This is called by Spring
	public void setCodelstDao(CodelstDao codelstDao) {
		this.codelstDao = codelstDao;
	}

    @RequestMapping("/{codelstType}")
    public List<CodelstItem> fetchCodelstByType(
    		@PathVariable(value="codelstType") String codelstType
    		) {
    	System.out.println("Input codelstType="+codelstType);
    	List<CodelstItem> codelstArray = codelstDao.fetchCodelstByType(codelstType);
        return codelstArray;
    }
    
    @RequestMapping("/{codelstType}/{codelstSubtyp}")
    public CodelstItem fetchOne(
    		@PathVariable(value="codelstType") String codelstType,
    		@PathVariable(value="codelstSubtyp") String codelstSubtyp
    		) {
    	System.out.println("Input codelstType="+codelstType+" codelstSubtyp="+codelstSubtyp);
    	List<CodelstItem> codelstArray = codelstDao.fetchCodelstItem(codelstType, codelstSubtyp);
    	if (codelstArray != null && codelstArray.size() > 0) {
            return codelstArray.get(0);
    	}
    	return null;
    }

}
