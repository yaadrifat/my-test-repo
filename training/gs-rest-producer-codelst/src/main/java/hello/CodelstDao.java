package hello;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * This class gets called by the controller class and retrieves data from DB.
 * Then it maps DB columns to Java object attributes.
 *
 */
public class CodelstDao {
	@Autowired
	private DataSource dataSource; // This gets wired by Spring
	private JdbcTemplate jdbcTemplate; // This gets wired by Spring

	private static Logger logger = Logger.getLogger(CodelstDao.class);
	
	// This gets called by Spring
	public CodelstDao() {
		System.out.println("CodelstDao instantiated");
	}

	// This gets called by Spring
	public void setDataSource(DataSource dataSource) {
		System.out.println("CodelstDao setter called "+dataSource);
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	// This fetches a list of codelst by type from DB
	public List<CodelstItem> fetchCodelstByType(String codelstType) {
		List<CodelstItem> codelstArray = null;
		try {
			String sql = " select CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP from ER_CODELST where "
					+ " CODELST_TYPE = ? ";
			codelstArray = jdbcTemplate.query(sql,
					new Object[] { codelstType },
					new CodelstMapper()
			);
		} catch(Exception e) {
			logger.fatal(e.getMessage());
			e.printStackTrace();
		}
		return codelstArray;
	}
	
	// This fetches one codelst by type and subtyp from DB
	public List<CodelstItem> fetchCodelstItem(String codelstType, String codelstSubtyp) {
		List<CodelstItem> codelstArray = null;
		try {
			String sql = " select CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP from ER_CODELST where "
					+ " CODELST_TYPE = ? and CODELST_SUBTYP = ? ";
			codelstArray = jdbcTemplate.query(sql,
					new Object[] { codelstType, codelstSubtyp },
					new CodelstMapper()
			);
		} catch(Exception e) {
			logger.fatal(e.getMessage());
			e.printStackTrace();
		}
		return codelstArray;
	}
	
	// This maps DB columns to CodelstItem attributes
	private static final class CodelstMapper implements RowMapper<CodelstItem> {
		public CodelstItem mapRow(ResultSet rs, int rowNum) throws SQLException {
			CodelstItem codelstItem = new CodelstItem();
			codelstItem.setDescription(rs.getString("CODELST_DESC"));
			codelstItem.setType(rs.getString("CODELST_TYPE"));
			codelstItem.setSubtype(rs.getString("CODELST_SUBTYP"));
            return codelstItem;
		}
	}
	
}
