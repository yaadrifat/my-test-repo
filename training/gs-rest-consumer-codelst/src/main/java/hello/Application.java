package hello;

import org.springframework.web.client.RestTemplate;

/**
 * This is a RESTful client to test RESTful produce of codelst service.
 *
 */
public class Application {

	private static String longUrlFormat = "http://localhost:8080/codelst/%s/%s";
    public static void main(String args[]) {
    	Object[] testData = new Object[] {"tarea", "tarea_14"};
        RestTemplate restTemplate = new RestTemplate();
        
        // This calls the RESTful producer and converts the result to a Java object
        CodelstItem codelst = restTemplate.getForObject(
        		String.format(longUrlFormat, testData),
        		CodelstItem.class);
        System.out.println("Received: "+codelst.getDescription());
        
        // This calls the RESTful producer and gets the result as JSON string
        String codelstStr = restTemplate.getForObject(
        		String.format(longUrlFormat, testData),
        		String.class);
        System.out.println("Received: "+codelstStr);
    }

}
