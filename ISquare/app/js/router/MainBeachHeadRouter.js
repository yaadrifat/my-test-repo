define([
  'jquery',
  'underscore',
  'backbone',
  'views/beachHead/BeachHeadsView'
], function (jquery, _, Backbone, BeachHeadsView) {

  var MainBeachHeadRouter = Backbone.Router.extend({
    routes: {
      '*actions': 'defaultAction',
      'messages': 'showMessageAboutMongo', // All urls will trigger this route
      'about': 'showAbout'
    }
  });

  var initialize = function(bHPK){

    //var vent = _.extend({}, Backbone.Events);
    var router = new MainBeachHeadRouter();

	console.log("MainBeachHeadRouter / initialize");

	router.on('route:defaultAction', function (actions) {
        var beachHeadsView = new BeachHeadsView();
        beachHeadsView.render(bHPK);
	});

    router.on('route:showMessageAboutMongo', function () {
      console.log("display helpful message about setting up mongo");
    });

    router.on('route:showAbout', function () {
      console.log("display about");
    });

    if (!Backbone.History.started){
    	Backbone.history.start();
    } else {
    	Backbone.history.stop();
    	Backbone.history.start();
    }
   	Backbone.History.started = true;

  };
  return {
    initialize: initialize
  };
});
