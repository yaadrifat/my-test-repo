define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var TargetFieldModel = Backbone.Model.extend({
        el: "",
        elType: "",
        defaults:{

        },
        initialize: function(obj){
            //alert('Target Field initialized!'+JSON.stringify(obj));
        },
        getName: function(){
       	 var that = this;
            var fld = that.get("el");
            return fld["name"] || fld["name^"];
        },
        findField: function (){
            var that = this;
            var fld = that.get("el");
            var fldType = fld["type"];
            fldType = (!fldType)? "input" : fldType;
            var selector = '';
            
            for (var key in fld){
            	var value = fld[key];
            	if (!value || value.length <= 0) continue;
            	
            	switch(key){
	            	case 'id':
	            	case 'name':
	            	case 'name^':
	            		selector += "["+key+"='" + value + "']";
	            		break;
	            	default:
	            		break;
            	}
            }

            switch (fldType) {
                case 'input':
                case 'textarea':
                case 'select':
                    selector = fldType + selector;
                    break;
                default:
                    selector = "";
                    break;
            }
            return selector;
        },
        disableTargetField: function(){
            var that = this;
            var selector = this.findField();
            if (!selector || selector.length <= 0) {
                return;
            }
            if (document.readyState !== 'complete') {
                $(document).ready(function () {
                    $(selector).attr('disabled', true);
                });
            } else {
                $(selector).attr('disabled', true);
            }
        },
        lockTargetField: function(){
            var that = this;
            var selector = this.findField();
            if (!selector || selector.length <= 0) {
                return;
            }
        	console.log (selector);
            if (document.readyState !== 'complete') {
                $(document).ready(function () {
                	$(selector).focus(function(){
                		$(selector).attr('disabled', true);
                	});
                    $(selector).attr('disabled', true);
                });
            } else {            	
                $(selector).attr('disabled', true);
                
                $(selector).focus(function(){
            		$(selector).attr('disabled', true);
            	});
            }
        	$(selector).css( "background-color", 'lightgrey' );
        },
        unlockTargetField: function(){
            var that = this;
            var selector = this.findField();
            if (!selector || selector.length <= 0) {
                return;
            }
            if (document.readyState !== 'complete') {
                $(document).ready(function () {
                    $(selector).attr('disabled', false);
                });
            } else {
                $(selector).attr('disabled', false);
            }
        }
    });
    return TargetFieldModel;
});
