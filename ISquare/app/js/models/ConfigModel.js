define([
  'jquery',
  'underscore',
  'backbone'
], function(jquery, _, Backbone){
  var ConfigModel = Backbone.Model.extend({

    initialize: function(){
      console.log('ConfigModel instantiated!');
    },
    getValue: function(key){
    	return this.get(key);
    }
  });

  return ConfigModel;
});
