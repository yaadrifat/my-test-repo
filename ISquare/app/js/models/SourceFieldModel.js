define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var SourceFieldModel = Backbone.Model.extend({
        el: "",
        elType: "",
        defaults:{

        },
        initialize: function(obj){
            //alert('Target Field initialized!'+JSON.stringify(obj));
        },
        getName: function(){
        	 var that = this;
             var fld = that.get("el");
             return fld["name"] || fld["name^"];
        },
        findField: function (){
            var that = this;
            var fld = that.get("el");
            var fldType = fld["type"];
            fldType = (!fldType)? "input" : fldType;
            var selector = '';
            
            for (var key in fld){
            	var value = fld[key];
            	if (!value || value.length <= 0) continue;
            	
            	switch(key){
	            	case 'id':
	            	case 'name':
	            	case 'name^':
	            		selector += "["+key+"='" + value + "']";
	            		break;
	            	default:
	            		break;
            	}
            }

            switch (fldType) {
                case 'input':
                case 'textarea':
                case 'select':
                    selector = fldType + selector;
                    break;
                default:
                    selector = "";
                    break;
            }
            return selector;
        },
        checkField: function(){
            var that = this;
            var selector = this.findField();
            if (!selector || selector.length <= 0) {
                return;
            }
        	console.log(selector);
        	var expectedValue = (this.get("el"))["expected"];
        	var cOp = (this.get("el"))["cOp"]; 
        	console.log(JSON.stringify(that) + expectedValue +" "+cOp);
        	switch(cOp){
	        	case "=":
	        		 if (document.readyState !== 'complete') {
	                     $(document).ready(function () {
	                     	return (expectedValue == $(selector).val());
	                     });
	                 } else {            	
	                 	return (expectedValue == $(selector).val());
	                 }
	        		break;
	        	case "!=": 
	        		 if (document.readyState !== 'complete') {
	                     $(document).ready(function () {
	                     	return (expectedValue != $(selector).val());
	                     });
	                 } else {
	                 	return (expectedValue != $(selector).val());
	                 }
	        		break;
	        	default:
	        		return false;
	        		break;
        	}
           
        }
    });
    return SourceFieldModel;
});
