define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var SenderModel = Backbone.Model.extend({
        type: "",
        targets: [],
        defaults:{

        },
        initialize: function(obj){
            //alert('Sender initialized!'+JSON.stringify(obj));
        }
    });
    return SenderModel;
});
