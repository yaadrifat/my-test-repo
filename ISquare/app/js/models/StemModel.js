"use strict";
define([
    'global',
    'config',
    'jquery',
    'underscore',
    'backbone',
    'models/stems/ValidationCueModel',
    'models/stems/InformationCueModel',
    'util/email/EmailModel',
    'models/SourceFieldModel',
    'models/TargetFieldModel',
    'models/WSClient/StudyWSClientModel',
    'models/WSClient/UserWSClientModel',
    'models/WSClient/FormResponseWSClientModel',
    'models/WSClient/FormWSClientModel',
    'models/WSClient/PatientScheduleWSClientModel',
    'models/WSClient/StudyPatientWSClientModel',
    'models/WSClient/MonitorWSClientModel',
    'models/WSClient/TaylorWSClientModel',
    'models/WSClient/PatientDemographicsWSClientModel'
], function(global, cfg, $, _, Backbone,
		ValidationCueModel, InformationCueModel, EmailModel, SourceFieldModel, TargetFieldModel,
		StudyWSClientModel, UserWSClientModel, FormResponseWSClientModel, FormWSClientModel, PatientScheduleWSClientModel, StudyPatientWSClientModel, MonitorWSClientModel, TaylorWSClientModel, PatientDemographicsWSClientModel
) {
    var StemModel = Backbone.Model.extend({
        initialize: function(){
            //alert('Stem instantiated!');
        },
        processStem : function(){
            //console.log("stemModel: "+ JSON.stringify(this));

            var stemModelName = this.get("stem");
            //console.log("stemModelName: "+ stemModelName);

            switch(stemModelName){
                case 'InformationCueModel':
                    var stemConfig = this.get("stemConfig");
                    //console.log("stemConfig: "+ JSON.stringify(stemConfig));
                    var informationCueModel = new InformationCueModel(stemConfig);
                    informationCueModel.inform();
                    break;
                case 'ValidationCueModel':
                    var stemConfig = this.get("stemConfig");
                    //console.log("stemConfig: "+ JSON.stringify(stemConfig));
                    var validationCueModel = new ValidationCueModel(stemConfig);
                    validationCueModel.runValidations();
                    break;
                default:
                	var entities = {};
	                entities[stemModelName] = Backbone.Model.extend({
	                    /* Model definition based on metadata */
	                	url:  function(){
	                    	return cfg.isquareURL+'/stems/'+stemModelName;
	                    }
	                });

	                var stemConfig = this.get("stemConfig");
	                var stem = new entities[stemModelName](stemConfig);

	                stem.fetch({
	                	reset: true,
	                	success: function(stemJ){
	                		for (var key in stemJ.attributes){
	                			try {
		                			var modelFn = eval("("+stemJ.get(key)+")");
			                		if (modelFn){
				                		stem.set(key, modelFn);             			
			                		}	                				
	                			} catch (e){
	                				stem.set(key, stemJ.attributes[key]);
	                			}
	                		}
	     	                stem.get("do")({
	     	            	   stemModel: stem
	     	               });
	                	},
	                	error: function(err){
	                		console.log(err);
	                		console.log('Error loading stem');
	                	}
	                });
                    break;
            }
        }
    });

  return StemModel;
});
