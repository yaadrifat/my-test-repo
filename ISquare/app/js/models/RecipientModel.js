define([
    'jquery',
    'underscore',
    'backbone'
], function(jquery, _, Backbone) {
    var RecipientModel = Backbone.Model.extend({
        defaults:{

        },
        initialize: function(obj){
            //alert('Recipient initialized!'+JSON.stringify(obj));
        }
    });
    return RecipientModel;
});
