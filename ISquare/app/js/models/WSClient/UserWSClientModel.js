define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var UserWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['userServiceUrl'] = thisModel.appURL+'/webservices/userservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	//console.log(params.subParams);

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
        			//console.log(params.userSoapMessage);
       			
        			var position = getPosition(params.userSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			//console.log(position);
        			       			
        			params.userSoapMessage = params.userSoapMessage.replaceBetween(position, position+2, "");
        			//console.log(params.userSoapMessage);
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.userSoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			//console.log(data);
  		  			params.userSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		//console.log(e.responseText);
    	  		  		params.userSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var userServiceUrl = params.userServiceUrl, userSoapMessage = params.userSoapMessage;
        	
        	$.ajax({
	    		url: userServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: userSoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlUserData, status){
	    			//console.log(xmlUserData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlUserData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
    	/*Following web services under UserSEI are currently supported through ISquare*/
        searchUser: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var userSoapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://velos.com/services/">\
				  <soapenv:Header> \
	        		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
		        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
		        			<wsse:Username>%s</wsse:Username>\
		        			<wsse:Password>%s</wsse:Password>\
		        		</wsse:UsernameToken>\
		        	</wsse:Security> \
			        </soapenv:Header> \
					   <soapenv:Body>\
					      <ser:searchUser>\
					         <UserSearch>\
					            <firstName>%s</firstName>\
					            <group>\
					                  <OID></OID>\
					                  <PK></PK>\
					                  <groupName></groupName>\
					            </group>\
					            <jobType>\
					                  <code></code>\
					                  <description></description>\
					                  <hidden></hidden>\
					                  <type></type>\
					            </jobType>\
					            <lastName>%s</lastName>\
					            <loginName>%s</loginName>\
					            <organization>\
					                  <OID></OID>\
					                  <PK></PK>\
					                  <siteAltId></siteAltId>\
					                  <siteName></siteName>\
					            </organization>\
					            <pageNumber>1</pageNumber>\
					            <pageSize>1000</pageSize>\
					            <sortBy></sortBy>\
					            <sortOrder></sortOrder>\
					            <studyTeam>\
					                  <OID></OID>\
					                  <PK>%s</PK>\
					            </studyTeam>\
					            <userPK>%s</userPK>\
					         </UserSearch>\
					      </ser:searchUser>\
					   </soapenv:Body>\
					</soapenv:Envelope>';
			   
			   params['userSoapMessage'] = userSoapMessage;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        }
    });
    return UserWSClientModel;
});
