define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var StudyWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['studyServiceUrl'] = thisModel.appURL+'/webservices/studyservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);     	

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
       			
        			var position = getPosition(params.userSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			       			
        			params.userSoapMessage = params.userSoapMessage.replaceBetween(position, position+2, "");
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.studySoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			//console.log(data);
  		  			params.studySoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		//console.log(e.responseText);
    	  		  		params.studySoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var studyServiceUrl = params.studyServiceUrl, studySoapMessage = params.studySoapMessage;
        	
        	$.ajax({
	    		url: studyServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: studySoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlStudyData, status){
	    			//console.log(xmlStudyData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlStudyData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
    	/*Following web services under StudySEI are currently supported through ISquare*/
        getStudy: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var studySoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
        						<wsse:Username>%s</wsse:Username>\
        						<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
	        			<ser:getStudy xmlns:ser="http://velos.com/services/"> \
	        				<StudyIdentifier>\
	        					<PK>%s</PK>\
	        				</StudyIdentifier>\
	        			</ser:getStudy>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';
        	params['studySoapMessage'] = studySoapMessage;
        	params['subParams'] = [params.studyPK];

        	thisModel.call(callingModel, params, callback);
        },
        getStudySummary: function(callingModel, params, callback){
        	var thisModel = this;  //console.log(thisModel);

        	var studySoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
			        			<wsse:Username>%s</wsse:Username>\
			        			<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
	        			<ser:getStudySummary xmlns:ser="http://velos.com/services/"> \
	        				<StudyIdentifier>\
	        					<PK>%s</PK>\
	        				</StudyIdentifier>\
	        			</ser:getStudySummary>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';
        	params['studySoapMessage'] = studySoapMessage;
        	params['subParams'] = [params.studyPK];

        	thisModel.call(callingModel, params, callback);
        },
        getStudyTeamMembers: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var studySoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
			        			<wsse:Username>%s</wsse:Username>\
			        			<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
	        			<ser:getStudyTeamMembers xmlns:ser="http://velos.com/services/"> \
	        				<StudyIdentifier>\
	        					<PK>%s</PK>\
	        				</StudyIdentifier>\
	        			</ser:getStudyTeamMembers>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';
        	params['studySoapMessage'] = studySoapMessage;
        	params['subParams'] = [params.studyPK];

        	thisModel.call(callingModel, params, callback);
        }
    });
    return StudyWSClientModel;
});
