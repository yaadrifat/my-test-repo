define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var FormResponseWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['formResponseServiceUrl'] = thisModel.appURL+'/webservices/formresponseservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	//console.log(params.subParams);

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
        			//console.log(params.formResponseSoapMessage);
       			
        			var position = getPosition(params.formResponseSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			//console.log(position);
        			       			
        			params.formResponseSoapMessage = params.formResponseSoapMessage.replaceBetween(position, position+2, "");
        			//console.log(params.formResponseSoapMessage);
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.formResponseSoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			//console.log(data);
  		  			params.formResponseSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		//console.log(e.responseText);
    	  		  		params.formResponseSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var formResponseServiceUrl = params.formResponseServiceUrl, formResponseSoapMessage = params.formResponseSoapMessage;
        	
        	$.ajax({
	    		url: formResponseServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: formResponseSoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlFormResponseData, status){
	    			//console.log(xmlUserData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlFormResponseData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
    	getListofStudyFormResponses: function(callingModel, params, callback){
        	var thisModel = this;  //console.log(thisModel);

        	var formResponseSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
			        			<wsse:Username>%s</wsse:Username>\
			        			<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
        				<ser:getListOfStudyFormResponses xmlns:ser="http://velos.com/services/">\
			                <FormIdentifier>\
                    		  <PK>%s</PK>\
			                </FormIdentifier>\
            			    <StudyIdentifier>\
        		              <PK>%s</PK>\
        			        </StudyIdentifier>\
			                <PageNumber>1</PageNumber>\
            			    <PageSize>1000</PageSize>\
			             </ser:getListOfStudyFormResponses>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';

        	params['formResponseSoapMessage'] = formResponseSoapMessage;
        	params['subParams'] = [params.formPK, params.studyPK];

        	thisModel.call(callingModel, params, callback);
        },
        getStudyFormResponse: function(callingModel, params, callback){
        	var thisModel = this;

        	var formResponseSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
			        			<wsse:Username>%s</wsse:Username>\
			        			<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
        				<ser:getStudyFormResponse xmlns:ser="http://velos.com/services/">\
			                <FormResponseIdentifier>\
                    		  <PK>%s</PK>\
			                </FormResponseIdentifier>\
			            </ser:getStudyFormResponse>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';

        	params['formResponseSoapMessage'] = formResponseSoapMessage;
        	//alert(params.formResponsePK);
        	params['subParams'] = [params.formResponsePK];

        	thisModel.call(callingModel, params, callback);
        },
        createStudyFormResponse: function(callingModel, params, callback){
        	var thisModel = this;  //console.log(thisModel);

        	var formResponseSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
			        			<wsse:Username>%s</wsse:Username>\
			        			<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
        				<ser:createStudyFormResponse xmlns:ser="http://velos.com/services/">\
			                <StudyFormResponse>\
			                <formFieldResponses>\
			                   <field>\
			                      <fieldIdentifier>\
			                         <OID></OID>\
			                         <PK>%s</PK>\
			                      </fieldIdentifier>\
			                      <value>%s</value>\
			                   </field>\
			                </formFieldResponses>\
			                <formFillDt></formFillDt>\
			                <formId>\
			                   <OID></OID>\
			                   <PK>%s</PK>\
			                </formId>\
			                <formStatus>\
			                   <code></code>\
			                   <description></description>\
			                   <hidden></hidden>\
			                   <type></type>\
			                </formStatus>\
			                <formVersion></formVersion>\
			                <studyIdentifier>\
			                   <OID></OID>\
			                   <PK>%s</PK>\
			                   <studyNumber></studyNumber>\
			                   <version></version>\
			                </studyIdentifier>\
			                <systemID>\
			                   <OID></OID>\
			                   <PK>%s</PK>\
			                </systemID>\
			                </StudyFormResponse>\
			             </ser:createStudyFormResponse>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';

        	params['formResponseSoapMessage'] = formResponseSoapMessage;
        	//alert(params.studyPK);
        	params['subParams'] = [params.fieldPK, params.fieldValue, params.formPK, params.studyPK, params.formResponsePK];

        	thisModel.call(callingModel, params, callback);
        },
    	getListOfPatientFormResponses: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var formResponseSoapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://velos.com/services/">\
				  <soapenv:Header> \
	        		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
		        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
		        			<wsse:Username>%s</wsse:Username>\
		        			<wsse:Password>%s</wsse:Password>\
		        		</wsse:UsernameToken>\
		        	</wsse:Security> \
			        </soapenv:Header> \
		        		<soapenv:Body>\
		        	      <ser:getListOfPatientFormResponses xmlns:ser="http://velos.com/services/">\
		        	         <FormIdentifier>\
		        	            <OID></OID>\
		        	            <PK>%s</PK>\
		        	         </FormIdentifier>\
		        	         <PatientIdentifier>\
		        	            <OID></OID>\
		        	            <PK>%s</PK>\
		        	            <organizationId>\
		        	               <OID></OID>\
		        	               <PK></PK>\
		        	               <siteAltId></siteAltId>\
		        	               <siteName></siteName>\
		        	            </organizationId>\
		        	            <patientId></patientId>\
		        	         </PatientIdentifier>\
		        	         <pageNumber>1</pageNumber>\
		        	         <pageSize>1000</pageSize>\
		        	      </ser:getListOfPatientFormResponses>\
		        	   </soapenv:Body>\
					</soapenv:Envelope>';
			   
			   params['formResponseSoapMessage'] = formResponseSoapMessage;
			   params['subParams'] = [params.formPK, params.patientPK];

			   thisModel.call(callingModel, params, callback);
        }
    });
    return FormResponseWSClientModel;
});
