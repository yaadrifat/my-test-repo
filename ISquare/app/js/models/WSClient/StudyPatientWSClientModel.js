define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var StudyPatientWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['studyPatientServiceUrl'] = thisModel.appURL+'/webservices/studypatientservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);     	

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
       			
        			var position = getPosition(params.studyPatientSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			       			
        			params.studyPatientSoapMessage = params.studyPatientSoapMessage.replaceBetween(position, position+2, "");
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.studyPatientSoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			//console.log(data);
  		  			params.studyPatientSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		//console.log(e.responseText);
    	  		  		params.studyPatientSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var studyPatientServiceUrl = params.studyPatientServiceUrl, studyPatientSoapMessage = params.studyPatientSoapMessage;
        	
        	$.ajax({
	    		url: studyPatientServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: studyPatientSoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlStudyPatientData, status){
	    			console.log(xmlStudyPatientData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlStudyPatientData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
    	/*Following web services under StudyPatientSEI are currently supported through ISquare*/
    	getStudyPatients: function(callingModel, params, callback){
        	var thisModel = this; console.log(thisModel);

        	var studyPatientSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
        						<wsse:Username>%s</wsse:Username>\
        						<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
        		<ser:getStudyPatients xmlns:ser="http://velos.com/services/">\
                <StudyIdentifier> \
        	    <OID></OID>\
        		<PK>%s</PK>\
        		<studyNumber></studyNumber>\
        		<version></version>\
                </StudyIdentifier>\
        		</ser:getStudyPatients>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';
        	params['studyPatientSoapMessage'] = studyPatientSoapMessage;
        	params['subParams'] = [params.studyPK];

        	thisModel.call(callingModel, params, callback);
        },
    	getStudyPatientStatusHistory: function(callingModel, params, callback){
        	var thisModel = this; console.log(thisModel);

        	var studyPatientSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ns1="http://cxf.apache.org/bindings/xformat"> \
	        		<soapenv:Header> \
	        			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
			        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
        						<wsse:Username>%s</wsse:Username>\
        						<wsse:Password>%s</wsse:Password>\
			        		</wsse:UsernameToken>\
			        	</wsse:Security> \
	        		</soapenv:Header> \
	        		<soapenv:Body> \
        		<ser:getStudyPatientStatusHistory xmlns:ser="http://velos.com/services/"> \
                <StudyIdentifier> \
                   <OID></OID>\
                   <PK>%s</PK>\
                   <studyNumber></studyNumber>\
                   <version></version>\
                </StudyIdentifier>\
                <PatientIdentifier>\
                   <OID></OID>\
                   <PK>%s</PK>\
                   <organizationId>\
                      <OID></OID>\
                      <PK>%s</PK>\
                      <siteAltId></siteAltId>\
                      <siteName></siteName>\
                   </organizationId>\
                   <patientId></patientId>\
                </PatientIdentifier>\
             </ser:getStudyPatientStatusHistory>\
	        		</soapenv:Body>\
        		</soapenv:Envelope>';
        	params['studyPatientSoapMessage'] = studyPatientSoapMessage;
        	params['subParams'] = [params.studyPK, params.patientPK, params.organizationPK];

        	thisModel.call(callingModel, params, callback);
        }
    });
    return StudyPatientWSClientModel;
});
