define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var MonitorWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['monitorServiceUrl'] = thisModel.appURL+'/webservices/monitorservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	//console.log(params.subParams);

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
        			//console.log(params.monitorSoapMessage);
       			
        			var position = getPosition(params.monitorSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			//console.log(position);
        			       			
        			params.monitorSoapMessage = params.monitorSoapMessage.replaceBetween(position, position+2, "");
        			//console.log(params.monitorSoapMessage);
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.monitorSoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			//console.log(data);
  		  			params.monitorSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		//console.log(e.responseText);
    	  		  		params.monitorSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var monitorServiceUrl = params.monitorServiceUrl, monitorSoapMessage = params.monitorSoapMessage;
        	
        	$.ajax({
	    		url: monitorServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: monitorSoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlUserData, status){
	    			//console.log(xmlUserData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlUserData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
        getHeartBeat: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var monitorSoapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://velos.com/services/">\
				  <soapenv:Header> \
	        		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
		        		<wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="SecurityToken-2014-12-01T23:13:30Z">\
		        			<wsse:Username>%s</wsse:Username>\
		        			<wsse:Password>%s</wsse:Password>\
		        		</wsse:UsernameToken>\
		        	</wsse:Security> \
			        </soapenv:Header> \
        		<soapenv:Body>\
        	      <ser:getHeartBeat/>\
        	   </soapenv:Body>\
					</soapenv:Envelope>';
			   
			   params['monitorSoapMessage'] = monitorSoapMessage;
			   params['subParams'] = [];

			   thisModel.call(callingModel, params, callback);
        }
    });
    return MonitorWSClientModel;
});
