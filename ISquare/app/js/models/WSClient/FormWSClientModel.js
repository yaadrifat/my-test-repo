define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var FormWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){

        },
        call: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	var inParams = params['subParams']; 

        	$.ajax({
    	  		type: 'GET',
    	  		url: cfg.isquareURL+'/getServerConfig/serving.VelosER',
    	  		success: function(response){
    	  			//console.log(response);
    	  			try{
    	  				response = JSON.parse(response);
	   	  				thisModel['auth'] = response.WSCreds;
	   	  				thisModel['appURL'] = (response.AppURLs)[0];
   	  				
	   	  				var authParams = [ thisModel.auth.name, thisModel.auth.pass ];
	   	  				authParams.push.apply(authParams,inParams);
	   	  				params['subParams'] = authParams;

	   	  				params['formServiceUrl'] = thisModel.appURL+'/webservices/formservice?wsdl';
	    	        	thisModel.substituteString (callingModel, params, callback);
    	  			} catch(e){
    	  				console.log('Config error!');
    	  			}
	    		}
            });
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	//console.log(params.subParams);

        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx=0;indx<params.subParams.length;indx++){
        		if(params.subParams[indx] == ""){ 
        			//console.log(params.formSoapMessage);
       			
        			var position = getPosition(params.formSoapMessage, "%s", ((indx+1)-replaced));
        			replaced++;
        			//console.log(position);
        			       			
        			params.formSoapMessage = params.formSoapMessage.replaceBetween(position, position+2, "");
        			//console.log(params.formSoapMessage);
        		}
        	}

    		$.ajax({
  		  		type: 'GET',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.formSoapMessage, 
  					 "subParams": params.subParams
  		  		},
  		  		success: function (data) {
  		  			console.log(data);
  		  			params.formSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		console.log(e.responseText);
    	  		  		params.formSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});
        },
    	callWebService: function(callingModel, params, callback){
        	var thisModel = this;	//console.log(thisModel);
    		var formServiceUrl = params.formServiceUrl, formSoapMessage = params.formSoapMessage;
        	
        	$.ajax({
	    		url: formServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: formSoapMessage,
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlUserData, status){
	    			//console.log(xmlUserData);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlUserData, status);	
	    			}
	            	return;
	    		}
        	});
    	},
    	getStudyFormDesign: function(callingModel, params, callback){
        	var thisModel = this;  //console.log(thisModel);

        	var formSoapMessage =
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://velos.com/services/">\
        		   <soapenv:Header/>\
        		   <soapenv:Body>\
        	          <ser:getStudyFormDesign>\
                	     <FormIdentifier>\
                   		    <PK>%s</PK>\
                		 </FormIdentifier>\
                		 <StudyIdentifier>\
                   		    <PK>%s</PK>\
                   		 	<studyNumber></studyNumber>\
                   		 	<version></version>\
                		 </StudyIdentifier>\
                		 <FormName></FormName>\
                		 <IncludeFormatting></IncludeFormatting>\
        			  </ser:getStudyFormDesign>\
	        	   </soapenv:Body>\
        		</soapenv:Envelope>';
	        		
        	params['formSoapMessage'] = formSoapMessage;
        	alert(params.studyPK);
        	params['subParams'] = [params.formPK, params.studyPK];

        	thisModel.call(callingModel, params, callback);
        },	
    });
    return FormWSClientModel;
});
