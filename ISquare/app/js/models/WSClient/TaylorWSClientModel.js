define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var TaylorWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){
        	console.log("TaylorWSClinetModel initialize");
        },
        call: function(callingModel, params, callback){
        	console.log("TaylorWSClinetModel call");
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	console.log("TaylorWSClinetModel read config");
        	var thisModel = this; 
        	//var inParams = params['subParams']; 
        	var inParams = params.serviceParams;
        	//console.log(inParams);
        	//thisModel.callWebService (callingModel, params, callback);
        	thisModel.substituteString (callingModel, params, callback);
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	
        	var replaced = 0;
        	for(var indx = 0; indx < params.serviceParams.length; indx++){
        		console.log(indx + " / "+ params.serviceParams[indx]);
        		if(params.serviceParams[indx] == ""){ 
        			var position = getPosition(params.documentSoapParameters, "%s", ((indx+1)-replaced));
        			replaced++;
        			params.documentSoapParameters = params.documentSoapParameters.replaceBetween(position, position+2, "");
        		}else{
        			//:BB
        			var position = getPosition(params.documentSoapParameters, "%s", ((indx+1)-replaced));
        			replaced++;
        			params.documentSoapParameters = params.documentSoapParameters.replaceBetween(position, position+2, params.serviceParams[indx]);
        		}
        	}
        	//console.log('before ajax call ' + params.documentSoapParameters);
        	thisModel.callWebService(callingModel, params, callback);
        	
         	/*$.ajax({
  		  		type: 'POST',
  		  		url: cfg.isquareURL+'/substituteString',
  		  		dataType: "json",
  		  		data:{
  		  			 "oString": params.documentSoapParameters, 
  					 "subParams": params.serviceParams
  		  		},
  		  		success: function (data) {
  		  			console.log("substituteString success"+ data);
  		  			params.userSoapMessage = data;
    	        	thisModel.callWebService(callingModel, params, callback);
  		  		},
  		  		error: function (e) {
	  		  		if (e.status == 200){
    	  		  		console.log(e.responseText);
    	  		  		params.userSoapMessage = e.responseText; 		  			
	  		  			thisModel.callWebService(callingModel, params, callback);
	  				} else {
	  					console.log(e);
	  					console.log('Substitution error! '+ JSON.stringify(e));
	  				}
  		  		}
  		  	});*/
        	
        },
    	callWebService: function(callingModel, params, callback){
    		console.log("TaylorWSClientModel callWebService");
        	var thisModel = this;	//console.log(thisModel);
    		var userServiceUrl = params.serviceurl, documentSoapParameters = params.documentSoapParameters;
    		//console.log(" callWebService userServiceUrl" +userServiceUrl + " /userSoapMessage " + userSoapMessage);	
        	console.log(documentSoapParameters);
    		$.ajax({
        		type: 'POST',
  		  		url: cfg.isquareURL+'/callWebService',
  		  		dataType: 'json',
  		  		data: documentSoapParameters,
  		  		crossDomain: true,
  		  		contentType: "text; charset=\"utf-8\"",
  		  	    timeout: 100000,
  		  		complete: function(xmlUserData, status){
    				console.log("Taylor webservice complete" + xmlUserData + " / " + status);
    				var userData = JSON.parse(xmlUserData.responseText);
    				if (callback && callingModel){
	    				callback(callingModel, userData, status);	
	         
					 // uncomment this for loop for check the available values in response
    							  
	    	  		/*console.log("user valid -->  "+ userData.result.validUser);
	    	  		validUser = userData.result.validUser;
	    	  		
	    	  		if(validUser==true || validUser=="true"){
	    	  		  console.log("inside valid user");*/
	    	  		 
	    	  		}/*else{
	    	  		 console.log("else " + userData.result.status.errorMessage);
	    	  		 
	    	  		}*/
	    	  		return;
  		  		}
    		});
    		/*$.ajax({
	    		url: userServiceUrl,
	    		type: "POST",
	    		dataType: "xml",
	    		data: userSoapMessage,
	    		crossDomain: true,
	    		crossOrigin:true,
	    		headers: {"Access-Control-Allow-Origin":"http://localhost:8380","Access-Control-Allow-Methods":"POST"},
	    		contentType: "text/xml; charset=\"utf-8\"",
	    		complete: function(xmlUserData, status){
	    			console.log("TaylorWSClinetModel webservice complete" + xmlUserData + " / " + status);
	    			if (callback && callingModel){
	    				callback(callingModel, xmlUserData, status);	
	    			}
	            	return;
	    		}
        	});*/
        	console.log("end of webservice call");
    	},
    	/*Following Taylor's web services are currently supported through ISquare*/
        fetchNotes: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var userSoapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.business.taylor.velos.com/">\
        		<soapenv:Header/><soapenv:Body><ser:fetchNote>\
        		<note>\
        		<applnCode>%s</applnCode>\
        		<entityId>%s</entityId>\
        		<entityType>%s</entityType>\
        		</note>\
        		</ser:fetchNote>\</soapenv:Body>\</soapenv:Envelope>';
			   
			   params['userSoapMessage'] = userSoapMessage;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        },
        saveDocument: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var documentSoapParameters = 'params={"service": "saveDocument", "artifact": {"contentType":"%s","createdUser":"%s","file":"%s","fileName":"%s","formId":"%s","personId":"%s","studyId":"%s"}}';
			   
			   params['documentSoapParameters'] = documentSoapParameters;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        },
        deleteDocument: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var documentSoapParameters = 'params={"service": "deleteDocument","serviceurl":"%s","documentId":"%s"}';
			   
			   params['documentSoapParameters'] = documentSoapParameters;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        },
        retrieveDocument: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var documentSoapParameters = 'params={"service": "retrieveDocument", "serviceurl":"%s","FormId":"%s"}';
			   
			   params['documentSoapParameters'] = documentSoapParameters;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        }
    });
    return TaylorWSClientModel;
});