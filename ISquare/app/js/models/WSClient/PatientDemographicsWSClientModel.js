define([
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(cfg, $, _, Backbone) {
    var PatientDemographicsWSClientModel = Backbone.Model.extend({
    	product: 'VelosER',

    	defaults: function (){
  			var jsObj = {
	    	   	product: 'VelosER'
  			};
  			return jsObj;
    	},
        initialize: function(){
        	//console.log("PatientDemographicsWSClientModel initialize");
        },
        call: function(callingModel, params, callback){
        	//console.log("PatientDemographicsWSClientModel call");
        	var thisModel = this; //console.log(thisModel);
        	thisModel.readConfig(callingModel, params, callback);
        },
        readConfig: function(callingModel, params, callback){
        	//console.log("PatientDemographicsWSClientModel read config");
        	var thisModel = this; 
        	//var inParams = params['subParams']; 
        	var inParams = params.serviceParams;
        	thisModel.substituteString (callingModel, params, callback);
        },
        substituteString: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);
        	String.prototype.replaceBetween = function(start, end, what) {
        	    return this.substring(0, start) + what + this.substring(end);
        	};
        	
        	var getPosition = function (str, m, i) {
    		   return str.split(m, i).join(m).length;
    		}
        	var replaced = 0;
        	for(var indx = 0; indx < params.serviceParams.length; indx++){
        		//console.log(indx + " / "+ params.serviceParams[indx]);
        		if(params.serviceParams[indx] == ""){ 
        			var position = getPosition(params.documentSoapParameters, "%s", ((indx+1)-replaced));
        			replaced++;
        			params.documentSoapParameters = params.documentSoapParameters.replaceBetween(position, position+2, "");
        		}else{
        			//:BB
        			var position = getPosition(params.documentSoapParameters, "%s", ((indx+1)-replaced));
        			replaced++;
        			params.documentSoapParameters = params.documentSoapParameters.replaceBetween(position, position+2, params.serviceParams[indx]);
        		}
        	}
        	//console.log('before ajax call ' + params.documentSoapParameters);
        	thisModel.callWebService(callingModel, params, callback);	
        },
    	callWebService: function(callingModel, params, callback){
    		//console.log("PatientDemographicsWSClientModel callWebService");
        	var thisModel = this;	//console.log(thisModel);
    		var userServiceUrl = params.serviceurl, documentSoapParameters = params.documentSoapParameters;
    		//console.log(" callWebService userServiceUrl" +userServiceUrl + " /userSoapMessage " + userSoapMessage);	
        	//console.log(documentSoapParameters);
    		$.ajax({
        		type: 'POST',
  		  		url: cfg.isquareURL+'/callWebService',
  		  		dataType: 'json',
  		  		data: documentSoapParameters,
  		  		crossDomain: true,
  		  		contentType: "text; charset=\"utf-8\"",
  		  	    timeout: 100000,
  		  		complete: function(xmlUserData, status){
    				console.log("eResearch webservice complete" + xmlUserData + " / " + status);
    				var userData = JSON.parse(xmlUserData.responseText);
    				if (callback && callingModel){
	    				callback(callingModel, userData, status);	
	    	  		}
	    	  		return;
  		  		}
    		});
        	//console.log("end of webservice call");
    	},
    	/*Following Taylor's web services are currently supported through ISquare*/
        searchPatientAcrossOrganization: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var documentSoapParameters = 'params={"service": "searchPatient","serviceurl":"%s", "username":"%s", "password": "%s", "PatientSearch": {"patOrganization": {"siteName": "%s"}, "patientFacilityID": "%s", "distinctFacilityRequired": "true", "exactSearch": "true"}}';
			   
			   params['documentSoapParameters'] = documentSoapParameters;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        },
        /*Following Taylor's web services are currently supported through ISquare*/
        searchPatientAcrossApplication: function(callingModel, params, callback){
        	var thisModel = this; //console.log(thisModel);

        	var documentSoapParameters = 'params={"service": "searchPatient","serviceurl":"%s", "username":"%s", "password": "%s", "PatientSearch": {"patientFacilityID": "%s" , "distinctFacilityRequired": "true", "exactSearch": "true"}}';
			   
			   params['documentSoapParameters'] = documentSoapParameters;
			   params['subParams'] = [params.fname, params.lname, params.login, params.studyPK, params.userPK];

			   thisModel.call(callingModel, params, callback);
        }
    });
    return PatientDemographicsWSClientModel;
});