define([
    'jquery',
    'underscore',
    'backbone',
    'util/email/EmailModel'
], function(jquery, _, Backbone, EmailModel) {
    var InformationCueModel = Backbone.Model.extend({
        type: "",
        defaults:{

        },
        initialize: function(obj){
           // alert('Information Cue initialized!'+JSON.stringify(obj));
        },
        inform : function(){
            //alert('inside inform '+JSON.stringify(this));
            var informationType = this.get("type");

            if (informationType == "email"){
                //alert('email title '+ JSON.stringify(this.get("settings")));
                var emailModel = new EmailModel(this.get("settings"));
                emailModel.sendEmail(this.get("settings"));
            };
        }
    });
    return InformationCueModel;
});
