define([
    'jquery',
    'underscore',
    'backbone',
    'models/TargetFieldModel'
], function($, _, Backbone, TargetFieldModel) {
    var ValidationCueModel = Backbone.Model.extend({
        type: "",
        targets: [],
        defaults:{

        },
        initialize: function(obj){
            //alert('Validation Cue initialized!'+JSON.stringify(obj));
        },
        runValidations : function(sourceCondition){
        	sourceCondition = (sourceCondition != false)? true : false;
            //alert('inside processStems'+JSON.stringify(this));
            var validationType = this.get("type");

            if (validationType == "val-lock"){
                //alert('val-lock '+ this.get("targets"));
                var targets = this.get("targets");
                if (!targets || targets.length <= 0){ return; }

                for (var i = 0; i < targets.length; i++) {
                    var target = targets[i];
                    var targetFldModel = new TargetFieldModel(target);
                    if (sourceCondition)
                    	targetFldModel.lockTargetField();
                    else
                    	targetFldModel.unlockTargetField();
                };
            };
            if (validationType == "val-ronly"){
                //alert('val-lock '+ this.get("targets"));
                var targets = this.get("targets");
                if (!targets || targets.length <= 0){ return; }

                for (var i = 0; i < targets.length; i++) {
                    var target = targets[i]; //alert('target: ' + JSON.stringify(target));
                    var targetFldModel = new TargetFieldModel(target);
                    targetFldModel.lockTargetField();
                };
            };
            
            if (validationType == "embed"){
                var targets = this.get("targets");
                if (!targets || targets.length <= 0){ return; }
                
                for (var i = 0; i < targets.length; i++) {
                    var target = targets[i];
                    var targetFldModel = new TargetFieldModel(target);
                    if (sourceCondition)
                    	targetFldModel.embedLockTargetField("embedDataHere");
                    else
                    	targetFldModel.unlockTargetField();
                };
            };
        },
        doSomething : function(){
            alert('I did something!');
        }
    });
    return ValidationCueModel;
});
