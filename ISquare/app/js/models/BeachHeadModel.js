define([
    'config',
    'underscore',
    'backbone',
    'models/StemModel',
    'collections/StemsCollection'
], function(cfg, _, Backbone, StemModel, StemsCollection) {
    var BeachHeadModel = Backbone.Model.extend({
        id: "",
        pk: 0,

        initialize: function(){
            //alert('Beachhead instantiated!');
        },
        process : function(){
        	if (!this.id) return;
	        var entities = {};
	        var beachheadPK = this.id;
	        entities[beachheadPK] = Backbone.Model.extend({
	            /* Model definition based on metadata */
	        	url:  function(){
	            	return cfg.isquareURL+'/beachheads/'+beachheadPK;
	            }
	        });

	        var beachhead = new entities[beachheadPK]();
	        beachhead.fetch({
	        	reset: true,
	        	success: function(stemJ){
		        	//try{
		                console.log("stems: "+ JSON.stringify(beachhead.get("stems")));
		                var stemsCollection = new StemsCollection();
		                stemsCollection.add(beachhead.get("stems"));
		                if (undefined != stemsCollection){
		                    for (var indx = 0; indx < stemsCollection.models.length; indx++){
		                        var stemModel = new StemModel(JSON.parse(JSON.stringify(stemsCollection.models[indx])));
		                        stemModel.processStem();
		                    }
		                }
		            //} catch (e){
		            //    console.log('Not stemsCollection '+e);
		            //}
	        	},
	        	error: function(err){
	        		console.log(err);
	        		console.log('Error loading stem');
	        	}
	        });
           
        },
        doSomething : function(){
            alert('I did something!');
        }

    });

  return BeachHeadModel;
});
