define([
  'global',
  'config',
  'jquery',
  'underscore',
  'backbone',
  'collections/BeachHeadsCollection',
  'models/BeachHeadModel'
  //'text!templates/beachHead/beachHeadTemplate.html'
], function(global, cfg, jquery, _, Backbone, BeachHeadsCollection, BeachHeadModel
//, beachHeadTemplate
){
  var BeachHeadsView = Backbone.View.extend({
    el: '.beachHeadLocation',
    initialize: function () {
      //alert('BeachHeadsView');
    },
    render: function (bhPK) {
	  var LocationModel = Backbone.Model.extend({
	     /* Model definition based on metadata */
	     url: function(){
	       	return cfg.isquareURL+'/gestalt/'+global.app+'/beachheads.locations.ui';
	     }
	  });
	  var locationModel = new LocationModel();
	  locationModel.fetch({
        success: function(locationModel) {
        	console.log(locationModel.get(global.mainTarget));
        	
        	var configuredBhs = locationModel.get(global.mainTarget);
        	if (!configuredBhs) {
        		console.log(beachHeads, "BeachHeadCollection error!");
        		return false;
        	}
        	var beachHeadsCollection = new BeachHeadsCollection(configuredBhs);
            for (var indx = 0; indx < beachHeadsCollection.models.length; indx++){
              var bhModel = new BeachHeadModel(JSON.parse(JSON.stringify(beachHeadsCollection.models[indx])));
              if (""+bhPK+"" == bhModel.get('_id')){
              	bhModel.set("id", bhPK);
              	bhModel.process();
              	/*window.console.log = function(){
              	    console.error('The developer console is temp...');
              	    window.console.log = function() {
              	        return false;
              	    }
              	}*/
               break;
              }
            }
        },
        error: function(beachHeads) {
          console.log(beachHeads, "BeachHeadCollection error!");
        }
      }).complete(function() {
        //console.log(beachHeadsCollection);
      });
    }
  });

  return BeachHeadsView;

});
