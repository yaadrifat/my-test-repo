define([
  'jquery',
  'underscore',
  'backbone',
  'collections/StemsCollection',
  'models/StemModel'
], function(jquery, _, Backbone, StemsCollection, StemModel){
  var StemsView = Backbone.View.extend({
    el: '.beachHeadLocation',
    initialize: function () {

    },
    render: function (bhPK) {
      //$(this.el).html(beachHeadTemplate);
      var stemsCollection = new StemsCollection();
      stemsCollection.fetch({
        success: function(stemsCollection) {
          for (var indx = 0; indx < stemsCollection.models.length; indx++){
            var stemModel = new StemModel(JSON.parse(JSON.stringify(stemsCollection.models[indx])));
            if ("ObjectId('"+bhPK+"')" == stemModel.get('_id')){
              stemModel.processStems();
              break;
            }
          }
        },
        error: function(stemsCollection) {
          console.log(stemsCollection, "StemsCollection error!");
        }
      }).complete(function() {
        console.log(stemsCollection);
      });
    }
  });

  return StemsView;

});
