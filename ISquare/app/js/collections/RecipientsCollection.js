define([
  'jquery',
  'underscore',
  'backbone',
  'models/RecipientModel'
], function(jquery, _, Backbone, RecipientModel){
  var RecipientsCollection = Backbone.Collection.extend({
    Model: RecipientModel
  });

  var recipientsCollection = new RecipientsCollection();

  return RecipientsCollection;
});
