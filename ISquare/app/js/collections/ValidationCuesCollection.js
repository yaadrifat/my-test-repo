define([
  'jquery',
  'underscore',
  'backbone',
  '../models/stems/ValidationCueModel'
], function(jquery, _, Backbone, ValidationCueModel){
  var ValidationCuesCollection = Backbone.Collection.extend({
    Model: ValidationCueModel
  });

  var validationCuesCollection = new ValidationCuesCollection();

  return ValidationCuesCollection;
});
