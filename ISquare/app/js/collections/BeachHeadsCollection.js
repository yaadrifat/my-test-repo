define([
  'config',
  'jquery',
  'underscore',
  'backbone',
  'models/BeachHeadModel'
], function(cfg, jquery, _, Backbone, BeachHeadModel){
  var BeachHeadsCollection = Backbone.Collection.extend({
    Model: BeachHeadModel,

    initialize: function(){
      //console.log('BeachHeadsCollection instantiated!');
    }
  });

  var beachHeadsCollection = new BeachHeadsCollection();

  return BeachHeadsCollection;
});
