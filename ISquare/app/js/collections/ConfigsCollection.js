define([
  'config',
  'jquery',
  'underscore',
  'backbone',
  'models/ConfigModel'
], function(cfg, jquery, _, Backbone, ConfigModel){
  var ConfigsCollection = Backbone.Collection.extend({
	Model: ConfigModel,
	url: function(){
		var url = cfg.isquareURL;
		return url+'/config.json';
	},
	
    initialize: function(){
      console.log('ConfigsCollection instantiated!');
    },
    getValueByKey: function(key){
    	var configModel; 
		for (var indx = 0; indx < this.models.length; indx++){
			configModel = new ConfigModel(JSON.parse(JSON.stringify(this.models[indx])));
			var value = configModel.getValue(key)
			if (value){
				return value;
			}
		}
    }
  });

  var configsCollection = new ConfigsCollection();
  
  return ConfigsCollection;
});
