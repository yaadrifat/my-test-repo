define([
  'jquery',
  'underscore',
  'backbone',
  'models/StemModel'
], function(jquery, _, Backbone, StemModel){
  var StemsCollection = Backbone.Collection.extend({
    Model: StemModel,
    //url: '/gestalt',

    initialize: function(){
      console.log('StemsCollection instantiated!');
    }
  });

  var stemsCollection = new StemsCollection();

  stemsCollection.on("add", function(ship) {
    //alert("Ahoy " + ship.get("name") + "!");
  });

  return StemsCollection;
});
