requirejs.config({
    //By default load any module IDs from js/lib
    //except, if the module ID starts with "config",
    //load it from the js/config directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
	/*config: {
		text: {
			useXhr: function (url, protocol, hostname, port) {
				//Override function for determining if XHR should be used.
				//url: the URL being requested
				//protocol: protocol of page text.js is running on
				//hostname: hostname of page text.js is running on
				//port: port of page text.js is running on
				//Use protocol, hostname, and port to compare against the url
				//being requested.

				//alert(url +" "+ protocol +" "+  hostname +" "+  port);
				//Return true or false. true means "use xhr", false means
				//"fetch the .js version of this resource".
				return true;
			}
		}
	},*/
	map: {
		// '*' means all modules will get 'jquery-private'
		// for their 'jquery' dependency.
		'*': { 'jquery': 'jquery-private' },
		
		// 'jquery-private' wants the real jQuery module
		// though. If this line was not here, there would
		// be an unresolvable cyclic dependency.
		'jquery-private': { 'jquery': 'jquery' }
    },
	paths: {
	    // Major libraries
		jquery: 'libs/jquery/jquery-min',
	    'jquery-private': 'libs/jquery/jquery-private',
	    jqueryui: 'libs/jqueryui/jquery-ui.min',
	    jqueryvalidation: 'libs/jqueryvalidation/dist/jquery.validate.min',
	    underscore: 'libs/underscore/underscore-min',
		backbone: 'libs/backbone/backbone-min',
	    text: 'libs/require/text',
	  },
	waitSeconds: 15
});

require(['global', 'router/MainBeachHeadRouter'], function(global, MainBeachHeadRouter){
	console.log(global.bHeadId);
	if (global.bHeadId && (global.bHeadId).length > 0){
		MainBeachHeadRouter.initialize(global.bHeadId);
	}
});
