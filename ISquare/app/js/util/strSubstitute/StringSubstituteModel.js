define([
  'config',
  'jquery',
  'underscore',
  'backbone',
], function(cfg, jquery, _, Backbone){
  var StringSubstitutionModel = Backbone.Model.extend({
	that: {},
    sender: [],
    recipients: [],

    initialize: function(){
      that = this;
      console.log('StringSubstitutionModel instantiated!');
    },
    /*
     * substitutionObj = {"oString": text, "subParams": textParams}
     * */
    stringSubstitute: function(thisModel, substitutionObj, successCallback, errorCallback){
    	
    	if (!substitutionObj.subParams || (substitutionObj.subParams).length <= 0){
    		successCallback(thisModel, substitutionObj.oString);
    		return;
    	}
    	
    	String.prototype.replaceBetween = function(start, end, what) {
    	    return this.substring(0, start) + what + this.substring(end);
    	};
    	
    	var getPosition = function (str, m, i) {
		   return str.split(m, i).join(m).length;
		}
    	
    	var replaced = 0;
    	for(var indx=0;indx<substitutionObj.subParams.length;indx++){
    		if(substitutionObj.subParams[indx] == ""){ 
   			
    			var position = getPosition(substitutionObj.oString, "%s", ((indx+1)-replaced));
    			replaced++;
    			       			
    			substitutionObj.oString = substitutionObj.oString.replaceBetween(position, position+2, "");
    		}
    	}

    	var isquareURL = cfg.isquareURL; 
    	jquery.ajax({
	  		type: 'GET',
	  		url: isquareURL+'/substituteString',
	  		dataType: "json",
	  		data:{
	  			 "oString": substitutionObj.oString, 
				 "subParams": substitutionObj.subParams
	  		},
	  		success: function (data) {
	  			successCallback(thisModel, data);
	  		},
	  		error: function (e) {
	  			errorCallback(thisModel, e);
	  		}
	  	});
    }
  });

  return StringSubstitutionModel;
});
