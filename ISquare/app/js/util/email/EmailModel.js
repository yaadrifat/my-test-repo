define([
  'config',
  'jquery',
  'underscore',
  'backbone',
  'collections/RecipientsCollection',
  'models/RecipientModel'
], function(cfg, $, _, Backbone, RecipientsCollection, RecipientModel){
  var EmailModel = Backbone.Model.extend({
	that: {},
    sender: [],
    recipients: [],

    initialize: function(){
      that = this;
      console.log('EmailModel instantiated!');
      
      if (!document.getElementById('msgContainer')){
	      $("<div id='msgContainer' align='center' style:'width:auto; z-index:10000; position:relative; float:right; left:-50%;'></div>").prependTo("body");
      }
    },
    sendEmail: function(emailObject){
	  var isquareURL = cfg.isquareURL;
  	  if (!emailObject.success) {
  		emailObject.success = this.sendEmailSuccess;
  	  }
  	  if (!emailObject.error) {
  		emailObject.error = this.sendEmailError;
  	  }

	  var rEmails = emailObject["rEmails"];
	  if (!rEmails || rEmails.length <= 0) {
		  rEmails = emailObject["recipients"];
	  		  if (!rEmails || rEmails.length <= 0)
	  			  return;
	  		  rEmails = this.processRecipients(rEmails);
	  	  }
	      $.ajax({
	  		type: 'GET',
	  		url: isquareURL+'/sendEmail',
	  		dataType: "json",
	  		data:{
	  			"rEmails": rEmails,
	  			"subject": emailObject["subject"],
	  			"text": emailObject["text"]
	  		},
	  		success: function (data) {
	  			if (emailObject.success){
	  				(emailObject.success)(emailObject.successMessage);
	  			}
	  		},
	  		error: function (e) {
	  			if (emailObject.error){
	  				(emailObject.error)(e, emailObject.errorMessage);
	  			}
	  		}
        });
    },
    sendEmailSuccess: function (successMessage) {
    	successMessage = (!successMessage)? 
    			'Email sent successfully!' : successMessage;

    	var random = Math.floor((Math.random() * 10000) + 1);
    	$('#msgContainer').append("<div id='isqSpn"+random+"' align='center' style='width:auto; max-width:1000px; z-index:10000; position:relative;" 
    		+ " font-size:10pt; font-weight:bold; font-style:bold; color:#000000; border:1px solid #f0c36d; background-color:#f9edbe'>"+successMessage+"</div>");
    	$("#isqSpn"+random).slideDown('fast').delay(10000).slideUp(function() {
			$(this).remove(); 
		});
		//console.log(successMessage);
	},    	
    sendEmailError: function (e, errorMessage) {
    	errorMessage = (!errorMessage)? 
    			'Email error! '+ JSON.stringify(e) : errorMessage;

		$("<div/>", { text: errorMessage }).hide().prependTo("body").slideDown('fast').delay(10000).slideUp(function() {
			$(this).remove(); 
		});
		//console.log(errorMessage);
	},
    processRecipients: function(recipientsArrayStr){
      var recipientsCollection = new RecipientsCollection(recipientsArrayStr);
  	  if (!recipientsCollection || recipientsCollection.length <=0) return null;

        var rEmails = '';
        for (var indx = 0; indx < recipientsCollection.length; indx++){
          var recipientModel = new RecipientModel(JSON.parse(JSON.stringify(recipientsCollection.models[indx])));
          if (!recipientModel.get("Name")){ continue; }

          var rName = (recipientModel.get("Name"))? recipientModel.get("Name") : '';
          if (rEmails == '') {
            rEmails += rName + '<'+ recipientModel.get("email")+'>';
          } else {
            rEmails += ',' + rName + '<'+ recipientModel.get("email")+'>';
          }
        }
        return rEmails;
    },
    stringSubstitute: function(thisModel, substitutionObj, successCallback, errorCallback){
    	if (!substitutionObj.oString){
    		substitutionObj.oString = "Email from ISquare";
    		successCallback(thisModel, substitutionObj.oString);
    		return;
    	}
    	if (!substitutionObj.subParams || (substitutionObj.subParams).length <= 0){
    		successCallback(thisModel, substitutionObj.oString);
    		return;
    	}
    	var isquareURL = cfg.isquareURL; 
    	$.ajax({
	  		type: 'GET',
	  		url: isquareURL+'/substituteString',
	  		dataType: "json",
	  		data:{
	  			 "oString": substitutionObj.oString, 
				 "subParams": substitutionObj.subParams
	  		},
	  		success: function (data) {
	  			successCallback(thisModel, data);
	  		},
	  		error: function (e) {
	  			errorCallback(thisModel, e);
	  		}
	  	});
    },
    substituteSubjectSuccess: function (thisModel, data) {
    	thisModel.emailObject["subject"] = data;
		var text = thisModel.emailObject["text"];
		var textParams = (thisModel.get("emailParams"))["textParams"];
		
		thisModel.stringSubstitute(thisModel, {"oString": text, "subParams": textParams}, thisModel.substituteTextSuccess, thisModel.substituteTextError);
	},
	substituteSubjectError: function (thisModel, e) {
		if (e.status == 200){
			thisModel.emailObject["subject"] = e.responseText;
			var text = thisModel.emailObject["text"];
			var textParams = (thisModel.get("emailParams"))["textParams"];
			
			thisModel.stringSubstitute(thisModel, {"oString": text, "subParams": textParams}, thisModel.substituteTextSuccess, thisModel.substituteTextError);
		} else {
			console.log('Substitution error! '+ JSON.stringify(e));
		}
	},
    substituteTextSuccess: function (thisModel, data) {
    	thisModel.emailObject["text"] = data;
    	thisModel.sendEmail(thisModel.emailObject);
	},
	substituteTextError: function (thisModel, e) {
		if (e.status == 200){
			thisModel.emailObject["text"] = e.responseText;
			thisModel.sendEmail(thisModel.emailObject);
		} else {
			console.log('Substitution error! '+ JSON.stringify(e));
		}
	},
	substituteAndSendEmail: function(){
	  var isquareURL = cfg.isquareURL;

      var rEmails = this.processRecipients(this.get("recipients"));
      if (!rEmails || rEmails.length <= 0) return;

	  var subject = this.get("subject");
	  var text = this.get("text");
	  
	  this.emailObject = {
		"rEmails": rEmails,
		"subject": subject,
		"text": text,
		"success": this.sendEmailSuccess,
		"successMessage": this.get("successMessage"),
		"error": this.sendEmailError,
		"errorMessage": this.get("errorMessage")
	  };
	  
	  var subjectParams = (this.get("emailParams"))["subjectParams"];
	  this.stringSubstitute(this, {"oString": subject, "subParams": subjectParams}, this.substituteSubjectSuccess, this.substituteSubjectError);
	}
  });

  return EmailModel;
});
