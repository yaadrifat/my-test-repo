var fs = require('fs');
var path = require('path');
var restify = require('restify');
var nodemailer = require('nodemailer');
var rest = require('restler');
var soap = require('soap');
var XLSX = require('xlsx');
var cache = require('memory-cache');
var twilio = require('twilio');
var serverConfig = require('./serverConfig');
var gestalt = require('./gestalt/gestalt.json');
var util = require("util");
var bunyan = require('bunyan');
var crypto = require('crypto');
var AESCrypt = require('./server_util/AESCrypt');
var tokenauth = require('./server_util/tokenauth');
var cryptkey = AESCrypt.cryptkey;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

if (!serverConfig || !Object.keys(serverConfig).length || !serverConfig.serving){
	log.info('Server configuration error! ISquare Launch unsuccessful!');
	console.log('Server configuration error! ISquare Launch unsuccessful!');
	//return;
}

for (var key in serverConfig.serving){
	//var fileName = AESCrypt.encrypt(cryptkey, process.env.isqClient_iVECTOR, key);
	tokenauth.curateToken(key);
	tokenauth.watchAppFile(key);
	var prodObj = serverConfig.serving[key];
	prodObj.WSCreds.pass = AESCrypt.decrypt(cryptkey, process.env.isqClient_iVECTOR, prodObj.WSCreds.pass);
}

// localhost
var isquarePort = process.env.PORT || serverConfig.isquarePort;

var logDirPath = "./log";
if (!fs.existsSync(logDirPath)){
    fs.mkdirSync(logDirPath);
}

var log = bunyan.createLogger({
    name: 'ISquareLogger',
    streams: [
      {
    	  level: 'info',
          type: 'rotating-file',
          path: logDirPath+"/isquare_info.log",
          period: '1d',   // daily rotation
          rotateExisting: true,
          threshold: '10m',      // Rotate log files larger than 10 megabytes 
          totalSize: '20m',
          //period: '1d',   // daily rotation
          count: 5       // keep 10 back copies
      },
      {
    	  level: 'error',
          type: 'rotating-file',
          path: logDirPath+"/isquare_error.log",  // log ERROR and above to a file
          period: '1d',
          rotateExisting: true,  // Give ourselves a clean file when we start up, based on period 
          threshold: '10m',      // Rotate log files larger than 10 megabytes 
          totalSize: '20m',
         // period: '1d',   // daily rotation
          count: 5        // keep 10 back copies
      }/*,
      {
        level: 'trace',
        type: 'rotating-file',
        path: logDirPath+"/isquare.log",
        period: '1d',   // daily rotation
        count: 10        // keep 10 back copies
      }*/
    ],
	serializers: restify.bunyan.serializers
});

var sendHTML = function( filePath, contentType, response ){
    console.log('sendHTML: ' + filePath) ;
    fs.exists(filePath, function( exists ) {
        if (exists) {
            fs.readFile(filePath, function(error, content) {
                if (error) {
                	return send500_InternalServerError(null, response);
                } else {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                }
            });
        } else {
            response.writeHead(404);
            response.end();
        }
    });
}

var getFilePath = function(url) {
    var filePath = './app' + url;
    if (url == '/' ) filePath = './app/index.html';
    return filePath;
}

var getContentType = function(filePath) {
    var extname = path.extname(filePath);
    var contentType = 'text/html';

    switch (extname) {
        case '.js':
        case '.json':
        	contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
    }

    return contentType;
}

var onHtmlRequestHandler = function(request, response) {
    //console.log('onHtmlRequestHandler... request.url: ' + request.url) ;

    var filePath = getFilePath(request.url);
    var contentType = getContentType(filePath);

    //console.log('onHtmlRequestHandler... getting: ' + filePath) ;
    sendHTML(filePath, contentType, response);
}

var getPosition = function (str, m, i) {
   return str.split(m, i).join(m).length;
};

/**
 * Regular Expression Array IndexOf
 * This little addition to the Array prototype will iterate over Regular Expression array
 * and return the index of the first regular expression element which matches 
 * the provided String.
 * Note: This will not match on objects.
 * @param  {String}  matchMe can be any string 
 * @return {Numeric} -1 means not found
 */
if (typeof Array.prototype.regExArrayIndexOf === 'undefined') {
    Array.prototype.regExArrayIndexOf = function (matchMe) {
        for (var i in this) {
        	//this[i] is regEx
            if (matchMe.toString().match(this[i])) {
                return i;
            }
        }
        return -1;
    };
}

var whiteList = [];
for (var key in serverConfig.serving){
	var prodObj = serverConfig.serving[key];
	if (whiteList.length == 0){
		Array.prototype.push.apply(whiteList, prodObj.AppURLs);
		continue;
	}
	for (var iURL = 0; iURL < prodObj.AppURLs.length; iURL++){
		if (whiteList.indexOf(prodObj.AppURLs[iURL]) < 0) {
			whiteList.push(prodObj.AppURLs[iURL]);
		}
	}
}
//console.log(whiteList);

var authenticate = function(req){
	var header=req.headers['authorization']||'';    // get the header
	//console.log(header);

    var token=header.split(/\s+/).pop()||'',        // and the encoded auth token
    auth=new Buffer(token, 'base64').toString(),    // convert from base64
    parts=auth.split(/:/),                          // split on colon
    username=parts[0],
    password=parts[1];

	console.log ('user: '+ username + ' password:' + password);
	return (process.env.ID === username && 
			AESCrypt.decrypt(cryptkey, process.env.isq_iVECTOR, serverConfig.isquareCreds.pass) === AESCrypt.decrypt(cryptkey, process.env.app_iVECTOR, password))
			? true : false;
};

var regExAllowedNakedURLs = [  /^\/$/, /^\/login$/, '/gestalt', /^\/logs$/, /^\/logs\/[0-9]$/, '/webservices', '/substituteString', '/callWebService', '/sendMessage', '/responseMessage' ];
var validateRequest = function (req, res, next) {
	if (!req.headers.origin){
		if (!req.headers.referer){
			console.log('Naked URL! '+req.url);
			log.info('Naked URL! '+req.url);
			console.log(regExAllowedNakedURLs.regExArrayIndexOf(req.url));
			if (regExAllowedNakedURLs.regExArrayIndexOf(req.url) < 0){
				console.log('Not an authorized URL! '+ req.url);
				log.info('Not an authorized URL! %s', req.url);
				return iSquareUpMessage(req, res, next);		
			} else {
				return next();
			}
		}

		var indx = getPosition(req.headers.referer,'/',3);
		req.headers.origin = (indx > 0)? (req.headers.referer).substring(0,indx) : '';
	}
	console.log('Origin '+ req.headers.origin +' '+ req.headers.referer+ ' ' + whiteList.indexOf(req.headers.origin));
	log.info('Request Origin %s', req.headers.origin)
	log.info('Request Referrer %s', req.headers.referer);

	if (whiteList.indexOf(req.headers.origin) < 0) {
		console.log('Not an authorized URL!');
		log.info('Not an authorized URL! %s', reg.headers.origin + ' '+ req.url);
		return send500_InternalServerError(req, res);
	} else {
		res.header( 'Access-Control-Allow-Origin', req.headers.origin );
	    res.header( 'Access-Control-Allow-Method', 'GET' );
	    res.header( 'Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-File-Name, Content-Type, Cache-Control' );
	    
	    log.info('Incoming request %s', req.url);
		next();
	}
};
var iSquareUpMessage = function(req, res){
	res.writeHead(200, { 'Content-Type': 'text/css' });
    res.end("ISquare server is running successfully!\n\nversion:"+serverConfig.appVersion, 'utf-8');
};

var serverOptions = {
    name: 'ISquare',
    version: '0.0.1',
    log: log,
    formatters: {
        'application/json': function(req, res, body){
            if(req.params.callback){
                var callbackFunctionName = req.params.callback.replace(/[^A-Za-z0-9_\.]/g, '');
                return callbackFunctionName + "(" + JSON.stringify(body) + ");";
            } else {
                return JSON.stringify(body);
            }
        },
        'text/html': function(req, res, body){
            return body;
        }
    }
};

if (serverConfig.SSL_Key && serverConfig.SSL_Certificate){
	serverOptions.httpsServerOptions = {};
	if (serverConfig.SSL_Key != ''){
		serverOptions.key = fs.readFileSync(serverConfig.SSL_Key, 'utf8'); 
	}
	if (serverConfig.SSL_Certificate != ''){
		serverOptions.certificate = fs.readFileSync(serverConfig.SSL_Certificate, 'utf8'); 
	}
	
	serverOptions.requestCert = true;
	//serverOptions.rejectUnauthorized = true;
}

var server = restify.createServer(serverOptions);

//server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.authorizationParser()); 

server.listen(isquarePort, function () {
    console.log('%s listening at %s', server.name, server.url);
    log.info('%s listening at %s', server.name, server.url);
});

server.use(validateRequest);

server.get('/', iSquareUpMessage);

server.get('/login', function(req, res, next){
	log.info('Login request received!');
	if (!authenticate(req)){
		log.info('Authentication error! Login unsuccessful!');
		return send500_InternalServerError(req, res);		
	}

	var productSign = req.headers["product"] || '';    // get the product code
	if (!productSign || productSign.length < 1){
		log.info('Request Product error! Login unsuccessful!');
		return send500_InternalServerError(req, res);
	}

	if (!serverConfig || !Object.keys(serverConfig).length || !serverConfig.serving){
		log.info('Server configuration error! Login unsuccessful!');
		return send500_InternalServerError(req, res);
	}

	var productConfig = serverConfig.serving[productSign];
	if (!productConfig || !Object.keys(productConfig).length){
		log.info('Product configuration error! Login unsuccessful!');
		return send500_InternalServerError(req, res);
	}

    //Login successful
	console.log('Login successful!');
	log.info('Login successful!');

	//Generate and send token	
	var token = tokenauth.randomValueHex(12);
	productConfig["AppToken"] = token;
	var productGestalt = gestalt[productSign];
	var beachheadLocations = productGestalt.beachheads.locations;
	if (!beachheadLocations ||  !Object.keys(beachheadLocations).length){
		log.info('Beachhead configuration error! Login unsuccessful!');
		return send500_InternalServerError(req, res);
	}
	//console.log(beachheadLocations);

	var date = new Date();
	var respObj = {
		on: date.toISOString(),
		access_token: token
	}
	if (!tokenauth.curateToken(productSign, JSON.stringify(respObj))){}
	respObj.bhLocations = beachheadLocations;
	//console.log(respObj);
	res.writeHead(200, { 'Content-Type': 'text/css' });
	res.end(JSON.stringify(respObj), 'utf-8');
});

var send500_InternalServerError = function (req, res){
	res.writeHead(500);
	res.end();
};

server.put('/logIt/:message', function(req, res, next){
	if (req.params.message === undefined) {
		return next(new restify.InvalidArgumentError('Name must be supplied'))
	}
});

server.get("/getServerConfig/:configKey", function(req, res){
    var configKey = req.params["configKey"]; //console.log('configKey '+configKey);
    var configValue;

    if (!configKey || configKey.length <= 0){
    	res.writeHead(200, { 'Content-Type': 'text/css' });
        res.end('', 'utf-8');
    } else {
        try {
        	if (configKey.indexOf('.') <= 0){
        		configValue = serverConfig[configKey];
        		configValue = JSON.stringify(configValue);
        	} else {
        		var configKeys = configKey.split('.');
        		configValue = serverConfig[configKeys[0]];

	        	for (var indx = 1; indx < configKeys.length; indx++){
	        		key = configKeys[indx];
	        		configValue = configValue[key];
	        		
	        		if (!configValue) break;
	        	}
	        	configValue = JSON.stringify(configValue);
        		//console.log('configValue '+configValue);
        	}
        	res.writeHead(200, { 'Content-Type': 'text/css' });
            res.end(configValue, 'utf-8');
        } catch (e){
    	    console.log(e);
    	    return send500_InternalServerError(req, res);
    	} finally {
    		res.end();
    	}
    }
});

var regExExposedAPIs = [ "/logs", "/logs/:id" ];
server.get('/webservices', function(req, res){
	console.log(server.routes);
    
    var header = '\nAPI for ISquare \n\
	\n********************************************\
    \n\t\tRESTIFY\
    \n********************************************\n';
    var body = '';
	for (var key in server.routes) {
	  if (server.routes.hasOwnProperty(key)) {
		  var val = server.routes[key]; console.log(val);
		  if (regExExposedAPIs.indexOf(val.url) > -1){
			  body += '\n'
				  + val.method+'\t'+ val.url
				  +'\n';
		  }
	  }
	}
	//body += '</table>';

	res.end(header + body);
});

server.get(/^\/[A-Za-z0-9_]*\/[0-9a-fA-F]{12}\/require$/, function(req, res){
	//AppToken verification
	tokenauth.validateAuthToken(req, function(success){
		if (!success){
			console.log('Invalid Token error');
			log.info('Invalid Token error %s', req.url);
			return send500_InternalServerError(req, res);
		}
	    //console.log('reading /require/require.js');
	    var filePath = __dirname + '/app/js/libs/require/require.js';
	    var contentType = getContentType(filePath);
	    sendHTML(filePath, contentType, res);
	});
});

server.get(/^\/[A-Za-z0-9_]*\/[0-9a-fA-F]{12}\/main/, function(req, res){
	//AppToken verification
	tokenauth.validateAuthToken(req, function(success){
		if (!success){
			console.log('Invalid Token error');
			log.info('Invalid Token error %s', req.url);
			return send500_InternalServerError(req, res);
		}
		console.log ('requesting main');
	    var filePath = __dirname + '/app/js/main.js';
	    var contentType = getContentType(filePath);
	    sendHTML(filePath, contentType, res);
	});
});

var getRequestedFile = function (req, res){
	//type =  (!type || type.length <= 0)? 'src' : '';
    //console.log('reading '+req.url);
	var reqFile = req.url; console.log(reqFile);
	reqFile = reqFile.substring(getPosition(reqFile,'/', 3)); console.log(reqFile);

    var filePath = __dirname + '/app/js'+reqFile;
    var contentType = getContentType(filePath);

    try {
        sendHTML(filePath, contentType, res);
    } catch (e){
        console.log("Error loading file: "+e);
    }
};

var readFileFromGestalt = function(req, res){
	var reqFile = req.url;
	var filePath = __dirname + '/gestalt' + reqFile +'.json';
    var contentType = getContentType(filePath);

    try {
        sendHTML(filePath, contentType, res);
    } catch (e){
        console.log("Error loading file: "+e);
    }
};

server.get("/gestalt", readFileFromGestalt);
server.get(/\/beachheads\/[0-9a-fA-F]{24}/, readFileFromGestalt);
server.get(/\/stems\/[0-9a-fA-F]{24}/, readFileFromGestalt);

server.get("/gestalt/:whichProduct/:what", function(req, res){
	var whichProduct = req.params["whichProduct"];
	if (!whichProduct || whichProduct.length < 1){
		log.info('Invalid product request error');
		return send500_InternalServerError(req, res);
	}
    try {
    	var productGestalt = gestalt[whichProduct]; //console.log(productGestalt);
    	if (!productGestalt){
   			log.info('Product gestalt error! %s', whichProduct);
    		return send500_InternalServerError(req, res);
    	}

    	var what = req.params["what"];
    	switch(what){
	    	case 'beachheads.locations.ui':
	    		break;
	    	default:
  				log.info('Unsupported gestalt location error %s', what);
	    		return send500_InternalServerError(req, res);
	    		break;
    	}
    	var gestaltCfg;
    	try {
	    	if (what.indexOf('.') <= 0){
    			gestaltCfg = productGestalt[what];
    			gestaltCfg = JSON.stringify(gestaltCfg); //console.log(gestaltCfg);
	    	} else {
    			var whats = what.split('.');
    			gestaltCfg = productGestalt[whats[0]]; //console.log(gestaltCfg);

        		for (var indx = 1; indx < whats.length; indx++){
        			key = whats[indx];
	        		gestaltCfg = gestaltCfg[key]; //console.log(gestaltCfg);
        		
    	    		if (!gestaltCfg) break;
        		}
        		gestaltCfg = JSON.stringify(gestaltCfg); //console.log(gestaltCfg);
	    	}
	    } catch (e){
	    	log.info('gestalt JSON error %s', what);
	    	return send500_InternalServerError(req, res);
	    }
    	res.writeHead(200, { 'Content-Type': 'text/css' });
        res.end(gestaltCfg, 'utf-8');
    } catch (e){
        console.log("Error loading file: "+e);
        return send500_InternalServerError(req, res);
    }
});

server.get(/^\/[A-Za-z0-9_]*\/[0-9a-fA-F]{12}\/config.js$/, function(req, res) {
	//AppToken verification
	tokenauth.validateAuthToken(req, function(success){
		if (!success){
			console.log('Invalid Token error');
			log.info('Invalid Token error %s', req.url);
			return send500_InternalServerError(req, res);
		}
		var filePath = __dirname + '/config.js';
	    var contentType = getContentType(filePath);
	    try {
	        sendHTML(filePath, contentType, res);
	    } catch (e){
	        console.log("Error loading file: "+e);
	    }
	});
});

server.get(/\/libs\/.*\.js/, function(req, res){
    getRequestedFile(req, res);
});
server.get(/\/[A-Za-z0-9_]*\/[0-9a-fA-F]{12}\/router\/.*\.js/, function(req, res){
	//AppToken verification
	tokenauth.validateAuthToken(req, function(success){
		if (!success){
			console.log('Invalid Token error');
			log.info('Invalid Token error %s', req.url);
			return send500_InternalServerError(req, res);
		}
		getRequestedFile(req, res);
	});
});
server.get(/\/views\/.*\.js/, function(req, res){
    getRequestedFile(req, res);
});
server.get(/\/collections\/.*\.js/, function(req, res){
    getRequestedFile(req, res);
});
server.get(/\/models\/.*\.js/, function(req, res){
    getRequestedFile(req, res);
});
server.get(/\/util\/.*\.js/, function(req, res){
    getRequestedFile(req, res);
});

var emailConfig = serverConfig.email;
var sName = emailConfig.emailCreds.name,
sEmail = emailConfig.emailCreds.user;
var sender = (sName) ? sName + " <" + sEmail + ">" : "<" + sEmail + ">";

if ((!emailConfig.emailCreds)|| (!emailConfig.emailCreds.user || !emailConfig.emailCreds.pass)){
    console.log('Configuration error');
}

var transporter = nodemailer.createTransport({
    service: emailConfig.service,
    auth: {
        user: emailConfig.emailCreds.user,
        pass: AESCrypt.decrypt(cryptkey, process.env.isq_iVECTOR, emailConfig.emailCreds.pass)
    }
});

server.get(/\/sendEmail\.*/, function(req, res){
    if( 'OPTIONS' == req.method ) {
        res.send( 203, 'OK' );
    }

    console.log(emailConfig.service);
    var subject = req.params["subject"];
    text = req.params["text"];
	
	try {
	    // setup e-mail data with unicode symbols
	    var recipients = req.params["rEmails"];
	
	    if (!req.params["rEmails"]){
	        console.log('Configuration error');
	        return send500_InternalServerError(req, res);
	    }
	    
	    var mailOptions = {
	     from: sender,
	     to: recipients,
	     subject: subject,
	     text: text, // plaintext body
	     html: text // html body
	    };

	     // send mail with defined transport object
	    transporter.sendMail(mailOptions, function(error, info){
	        if(error){
	            console.log(error);
	            return send500_InternalServerError(req, res);
	        }else{
	            console.log('Message sent: ' + info.response);
	            res.writeHead(200, { 'Content-Type': 'text/css' });
	            res.end();
	        }
	    });
	} catch (e){
	    console.log(e);
	    return send500_InternalServerError(req, res);
	}
    
});

server.post(/\/substituteString\.*/, function(req, res){
	console.log('POST');
	console.log('params -->'+req.params);
	for(var key in req){
	//	console.log('key ' + key +'value ' + req[key]);
	}
	var parms = req['paramsvalue'];
	console.log("parms " + parms);
	for(var key in parms){
		console.log('key ' + key +'value ' + parms[key]);
	}
	var POST = {};
    if (req.method == 'POST') {
   console.log('inside post');
   req.on('data', function(data) {
	     console.log('inside data');
   			})
    }
    var oString = req.params["oString"]; console.log('oString '+oString);
    var subParams = req.params["subParams"]; console.log('subParams '+subParams);
    if (!subParams || subParams.length <= 0){
    	res.writeHead(200, { 'Content-Type': 'text/css' });
        res.end(oString, 'utf-8');
    } else {
    	var vArray = [oString];
    	vArray = vArray.concat(subParams);

        try {
        	var newString = util.format.apply(util,vArray);
    		console.log('newString '+newString);
            res.writeHead(200, { 'Content-Type': 'text/css' });
            res.end(newString, 'utf-8');
        } catch (e){
        	console.log(e);
    	    return send500_InternalServerError(req, res);
    	} finally {
    		res.end();
    	}
    }
});
server.post(/\/callWebService\.*/, function (req, res) {
	//var url = 'http://localhost:8380/Taylor/webservice/taylor?wsdl';
	var inparams = {};
    req.on('data', function(data) {
		//console.log('rawData'+data);
    	//console.log('inside data');
        data = data.toString();
        //console.log('toString ='+data);
        data = data.substring(7);
        //console.log('subString = '+data);
        /*data = data.split('&');
        console.log('split ='+data);*/
        inparams = JSON.parse(data);
        //console.log('inparams' + inparams);
        //console.log('params ' + inparams['params']);
       var args = inparams;
       //console.log("args.artifact" + args.artifact);
       var serviceMethod = args.service;
       //console.log("serviceMethod ="+serviceMethod);
       var url = args.serviceurl;
       //console.log("serviceurl ="+url);
       var username = args.username;
       var password = args.password;
       delete args["service"];
       delete args["serviceurl"];
       if (username && password) {
    	   delete args["username"];
           delete args["password"];
           var wssConfig = {
   	   			username: username,
   	   			password: password
   	    };
       }
       //args.remove("service");
       console.log("url::::"+url);
       try {
	       soap.createClient(url, function(err, client) {  
	          if (serviceMethod == 'deleteDocument'){
	        	  client.deleteDocument(args,function(err, result) {
	        		  if (err){
	  					console.log(err);
	        		  } else {
	        			 console.log('Result ---------------------->')
	                     console.log(JSON.stringify(result));
	                     res.send(JSON.stringify(result));
	        		  }
	              });  
	          }
	          if (serviceMethod == 'searchPatient'){
	        	  client.setSecurity(new soap.WSSecurity(wssConfig.username, wssConfig.password)); 
	        	  client.searchPatient(args,function(err, result) {
	        		  if (err){
	        			  console.log('Error ---------------------->')
	        			  console.log(JSON.stringify(err));
	        			  log.info(JSON.stringify(err));
	        			  return send500_InternalServerError(req, res);
	          		  } else {
	          			  console.log('Result ---------------------->')
	          			  console.log(JSON.stringify(result));
	          			  log.info(JSON.stringify(result));
	          			  res.send(JSON.stringify(result));
	          		  }
	              });  
	          } 
	          if (serviceMethod == 'retrieveDocument'){
	        	  client.retrieveDocument(args,function(err, result) {
	        		  if (err){
	  					console.log(err);
	        		  } else {
	        			  console.log('Result ---------------------->')
	                      console.log(JSON.stringify(result));
	                      res.send(JSON.stringify(result));
	        		  }
	              },{timeout: 100000});  
	          } 
	       });
       } catch (e){
   	    console.log(e);
   	    log.info('webservice fails: '+e);
   	    return send500_InternalServerError(req, res);
   	   }
    });   
});

server.post(/\/saveFileInForm\.*/, function (req, res) {
	//console.log(req);
	var type = req.files.filename.type;
	//console.log(type);
	var filename = req.files.filename.name;
	//console.log(filename);
	var createdUser = req.body.createdUser;
	var formId = req.body.formId;
	var personId = req.body.personId;
	var studyId = req.body.studyID;
	var taylorURL = req.body.taylorURL;
	fs.readFile(req.files.filename.path, 'base64', function read(err, data) {
	    if (err) {
	        throw err;
	    }    
	    var url = taylorURL;
		var args = {
				artifact: {
					contentType: type,
					createdUser: createdUser,
					file: data, 
					fileName: filename,
					formId: formId,
					personId: personId,
					studyId: studyId
				}
		};  
		console.log(args);
		soap.createClient(url, function(err, client) {
			//client.setSecurity(new soap.WSSecurity(wssConfig.username, wssConfig.password));
			console.log('saveDocument client created');
			client.saveDocument(args, function(err, result) {
				if (err){
					console.log(err);
				} else {
					res.send(result);
					console.log('saveDocument response received!');
					console.log(result);
				}
		    });
		});
	});
});

server.post(/\/saveFileInField\.*/, function (req, res) {
	//console.log(req);
	var type = req.files.filename.type;
	//console.log(type);
	var filename = req.files.filename.name;
	//console.log(filename);
	var createdUser = req.body.createdUser;
	var formId = req.body.formId;
	var personId = req.body.personId;
	var studyId = req.body.studyID;
	var fieldId = req.body.fieldId;
	var taylorURL = req.body.taylorURL;
	var finalFormId = formId+'-'+fieldId;
	fs.readFile(req.files.filename.path, 'base64', function read(err, data) {
	    if (err) {
	        throw err;
	    }    
	    var url = taylorURL;
		var args = {
				artifact: {
					contentType: type,
					createdUser: createdUser,
					file: data, 
					fileName: filename,
					formId: finalFormId,
					personId: personId,
					studyId: studyId
				}
		};  
		console.log(args);
		soap.createClient(url, function(err, client) {
			//client.setSecurity(new soap.WSSecurity(wssConfig.username, wssConfig.password));
			console.log('saveDocument field-level client created');
			client.saveDocument(args, function(err, result) {
				if (err){
					console.log(err);
				} else {
					res.send(result);
					console.log('saveDocument field-level response received!');
					console.log(result);
				}
		    });
		});
	});
});

server.post(/\/getExcelData\.*/, function (req, res) {
	var name = req.params.fileName;
	var tabname = req.params.tabName;
	//console.log('filename'+name);
	//console.log('tabname'+tabname);
	//var date = new Date();
	var readExcelFile = function(tabname) {
		var ref_cell = 'Y4';
		var worksheet = workbook.Sheets[tabname];
		var totalrules_cell = worksheet[ref_cell];
		var totalrules = totalrules_cell.v;
		//console.log(desired_value);
		var rules = [], sourceRef = [], targetRef = [], sourceRef_cell = [], targetRef_cell = [], sourceRef_value = [], targetRef_value = [];
		var increment = 0;
		for (var i = 0; i < totalrules; i++){
			sourceRef[i] = 'U'+(i+4);
			targetRef[i] = 'V'+(i+4);
			sourceRef_cell[i] = worksheet[sourceRef[i]];
			targetRef_cell[i] = worksheet[targetRef[i]];
			sourceRef_value[i] = sourceRef_cell[i].v;
			targetRef_value[i] = targetRef_cell[i].v;
			//console.log(''+[i]+':'+targetRef_value[i]);
			var sources = [];
			var targets = [];
			if (i > 0){
				if (sourceRef_value[i-1] > targetRef_value[i-1]){
					increment += sourceRef_value[i-1];
					//console.log('source greater');
				}
				if (targetRef_value[i-1] > sourceRef_value[i-1]){
					increment += targetRef_value[i-1];
					//console.log('target greater');
				}
	            if (targetRef_value[i-1] == sourceRef_value[i-1]){
					increment += targetRef_value[i-1];
					//console.log('target equals source');
				}			
			}
			//console.log(''+[i]+':'+incrementSource);
			for (var j = 0; j < sourceRef_value[i]; j++){
				var cellD = 'D'+(j+increment+4),cellE = 'E'+(j+increment+4), cellF = 'F'+(j+increment+4), cellG = 'G'+(j+increment+4), cellH = 'H'+(j+increment+4), cellI = 'I'+(j+increment+4), cellJ = 'J'+(j+increment+4), cellK = 'K'+(j+increment+4);
				var sourceFieldDetail = {FieldNameS:worksheet[cellD].v, CompOpt:worksheet[cellE].v, FieldValueS:worksheet[cellF].v, ValueType:worksheet[cellG].v,CondOpt: worksheet[cellH].v, FieldElementTypeS: worksheet[cellI].v, FieldElementKeyS: worksheet[cellJ].v, FieldElementValueS: worksheet[cellK].v};
				sources.push(sourceFieldDetail);
			}
			for (var k = 0; k < targetRef_value[i]; k++){
				var cellL = 'L'+(k+increment+4), cellM = 'M'+(k+increment+4), cellN = 'N'+(k+increment+4), cellO = 'O'+(k+increment+4), cellP = 'P'+(k+increment+4), cellQ = 'Q'+(k+increment+4), cellR = 'R'+(k+increment+4);
				var targetFieldDetail = {FieldNameT: worksheet[cellL].v, DefaultValidation: worksheet[cellM].v, DesiredValidation: worksheet[cellN].v, ConfigValue: worksheet[cellO].v, FieldElementTypeT: worksheet[cellP].v,  FieldElementKeyT: worksheet[cellQ].v, FieldElementValueT: worksheet[cellR].v};
				targets.push(targetFieldDetail);
			}
			var rule = {SourceFields: sources, TargetFields: targets};
			rules.push(rule);
		}
		console.log(rules);
		cache.put("'"+name+"-"+tabname+"'", JSON.stringify(rules));
		res.send(JSON.parse(cache.get("'"+name+"-"+tabname+"'")));
		console.log('Sent Excel Data');
	}
	if (name && tabname) {
		if (cache.get("'"+name+"'")){
			if (cache.get("'"+name+"-"+tabname+"'")){
				res.send(cache.get("'"+name+"-"+tabname+"'"));
				console.log('Sent Excel Data through cache');
			} else {
				var workbook = cache.get("'"+name+"'");
				readExcelFile(tabname);
			}
		} else {
			var workbook = XLSX.readFile(name);
			cache.put("'"+name+"'", workbook);
			readExcelFile(tabname);
		}
	} else {
		console.log('Filename and/or tabname do not exist');
		return send500_InternalServerError(req, res);	
	}
});

server.post(/\/messageToComaiba\.*/, function (req, res) {
	var inparams = {};
	req.on('data', function(data) {
		console.log('rawData'+data);
		data = data.toString();
		inparams = JSON.parse(data);
		var no = inparams.url;
		var url = serverConfig.API.Comaiba.APIURLs[no];
		//var username = inparams.username;
		//var password = inparams.password;
		var jsonData = inparams;
		delete inparams["url"];
		var restCredsConfig = {
				username: serverConfig.API.Comaiba.Creds.name,
				password: serverConfig.API.Comaiba.Creds.pass
		};
		rest.postJson(url, jsonData, restCredsConfig).on('complete', function(data, response) {
			if (response.statusCode == 200) {
				res.send(data);
				console.log('Comaiba response received!');
				console.log(data);
			} 
			else {
				console.log('Error: '+response.statusMessage);
			}
		});
	});
});
	    
server.get(/\/sendMessage\.*/, function(req, res){
	/*var auth = sinchAuth("615908ac-54ba-4e36-8b40-d2d2640787ea", "t6u8jw5FDka6hQzrpbZyIg==");
	sinchSms.sendMessage("+15106930295", "Hello world!");
	console.log(result);*/
	
	var accountSid = 'ACc4da343654fad97a83db594df5b90e84'; // Your Account SID from www.twilio.com/console
	var authToken = 'fb3f3a836fa0175a1ae103115a2877da';   // Your Auth Token from www.twilio.com/console
	var client = new twilio.RestClient(accountSid, authToken);

	client.messages.create({
	    body: 'Welcome to iSquare Messaging Alert. Text "Subscribe" to receive updates from us.',
	    to: '+15106930295',  // Text this number
	    from: '+15107579814' // From a valid Twilio number
	    //mediaUrl: "https://c1.staticflickr.com/3/2899/14341091933_1e92e62d12_b.jpg" 
	}, function(err, message) {
	    if(err) {
	    	console.log('There was an error sending a text message.');
	    	console.error(err.message);
	    } else {
	    	console.log('Success! The SID for this SMS message is: '+message.sid);
	    	console.log('Message sent on: '+message.dateCreated);	    	
	    }
	}); 
});

server.post(/\/responseMessage\.*/, function(req, res){
	var resp = new twilio.TwimlResponse();
	var getParameterByName = function (name) {
	    var match = RegExp('[?&]' + name + '=([^&]*)').exec(req.body);
	    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	};
	var responseText = getParameterByName("Body").toLowerCase();
	console.log(responseText);
	if(responseText === 'subscribe'){
		console.log('Send response text back to recipient');
		resp.message('Thank you for your subscription to iSquare Messaging Alert. Reply "STOP" to stop receiving updates.');
		res.writeHead(200, {
		    'Content-Type':'text/xml'
		});
		res.end(resp.toString());
	}
    return;
});

fs.watchFile(__dirname + '/log/isquare.log.9', function (curr, prev){
	console.log (prev);
	console.log (curr);
	if (curr.nlink > 0 ){
		console.log('Found the oldest file');
		sendEmail ();
	}
});

var getLogs = function(req, res) {
	var id = req.params["id"];
	var filePath = __dirname + '/log/isquare.log';
	filePath += (!id || id.length <= 0)? '' : '.' + id;
	try{
		var file = fs.readFileSync(filePath, 'utf-8');

		res.setHeader('Content-Length', file.length);
		res.write(file, 'utf-8');
		res.end();
	} catch (e){
		res.writeHead(404);
		res.write('File not found!', 'utf-8');
        res.end();
	}
};

server.get("/logs", function(req, res) {
	req.url += '/';
	getLogs(req, res);
});

server.get("/logs/:id", getLogs);