({
    baseUrl : "../../app/js",
    mainConfigFile: "../../app/js/main.js",
    name: "main",
    out: "./app/js/main.js",
    preserveLicenseComments: false,
    paths: {
	    config			: "empty:"
    }
})