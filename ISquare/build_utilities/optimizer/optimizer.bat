cd build
xcopy "../gestalt" "../dist/gestalt" /s/y/i
xcopy "../node_modules" "../dist/node_modules" /s/y/i
mkdir "../dist/stems"
xcopy "../app/js/libs" "../dist/app/js/libs" /s/y/i
xcopy "../app/js" "../dist/app/js" /y/i
xcopy ".." "../dist" /y/i
node r.js -o minify-uglify.js