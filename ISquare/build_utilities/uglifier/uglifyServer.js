var UglifyJS = require('uglify-js');
var fs = require('fs');
var uglifyConfig = require('./uglifyConfig');
var outputFolder = __dirname+"/output/";

if (!fs.existsSync(outputFolder)){
	fs.mkdirSync(outputFolder);
}

if (uglifyConfig.uglifyFiles){
	for (var i = 0; i < uglifyConfig.uglifyFiles.length; i++){
		//console.log(uglifyConfig.uglifyFiles[i]);
		
		var fileObj = uglifyConfig.uglifyFiles[i];
		
		/*if (fileObj.source == fileObj.target){
			console.log ('Source and target are same!');
			continue;
		}*/
		try{
			var result = UglifyJS.minify(fileObj.source, {
				mangle: true,
				compress: {
					sequences: true,
					dead_code: true,
					conditionals: true,
					booleans: true,
					unused: true,
					if_return: true,
					join_vars: true,
					drop_console: true
				}
			});
		
			fs.writeFileSync(outputFolder + fileObj.target, result.code);
			console.log('Minified and uglified file '+ fileObj.source + ' into ' + fileObj.target);
		} catch (e){
			console.log("File error: "+fileObj.source);
		}
	}
}
	
if (uglifyConfig.uglifyFolders){

	for (var i = 0; i < uglifyConfig.uglifyFolders.length; i++){
		console.log(uglifyConfig.uglifyFolders[i]);
		
		var folderObj = uglifyConfig.uglifyFolders[i];
		
		if (folderObj.source.path == fileObj.target.path){
			continue;
		}
		fs.readdir(folderObj.source.path, function (err, list) {
		   // Return the error if something went wrong
		   if (err){
			   console.log(err);
			   return;
		   }
		
		   list.filter(function(file) {
			   return file.substr(-3) === folderObj.source.filter; 
		   }).forEach(function(file) { 
			   // For every file in the list
			   // Full path of that file
			   var sourceFilePath = folderObj.source.path + "/" + file;
			  
			   var result = UglifyJS.minify(sourceFilePath, {
					mangle: true,
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true
					}
				});

			   var targetFolder = outputFolder + folderObj.target.path; 
			   if (!fs.existsSync(targetFolder)){
			       fs.mkdirSync(targetFolder);
			   }
			   	var targetFilePath = outputFolder + folderObj.target.path + "/" + file;
			   	console.log('Minified and uglified file '+ sourceFilePath + ' into ' + targetFilePath);
				fs.writeFileSync(targetFilePath, result.code);
		    });
		});
	}
}