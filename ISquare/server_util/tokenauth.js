var fs = require('fs');
var crypto = require('crypto');
var AESCrypt = require('./AESCrypt');
var serverConfig = require('../serverConfig');

var tmpDir = './tmp';
if (!fs.existsSync(tmpDir)){
    fs.mkdirSync(tmpDir);
}
var getPosition = function (str, m, i) {
   return str.split(m, i).join(m).length;
};
var tokenauth = {};

tokenauth.randomValueHex = function(len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
};

tokenauth.validateAuthToken = function(req, callback){
	if (!req || !req.url) return callback(false);

	var url = req.url; console.log(url);
	var slashPos = 1;
	var app = url.substring(1, getPosition(url,'/', ++slashPos));
	console.log(app);
	var reqToken = url.substring(getPosition(url,'/',slashPos)+1, getPosition(url,'/',++slashPos));
	console.log(reqToken);
	if (!reqToken || reqToken.length != 12) return callback(false);

	var fileName = AESCrypt.encrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, app);
	if (!fileName || fileName.length < 1) return callback(false);
	fileName += '.tkn'

	fs.readFile(tmpDir+"/"+fileName, 'utf8', function (err, data){
		if (!data || data.length < 1) return callback(true);
		var lines = data.split('\n');
		
		for (var lNo =0 ;lNo < lines.length; lNo++){
			console.log('line '+lines[lNo]);
			var line = lines[lNo];
			
			var fileToken = AESCrypt.decrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, line);
			console.log('Decrypted: ' + fileToken);
			
			var tokenObj = JSON.parse(fileToken);
			if (tokenObj.access_token == reqToken){
				return callback(true);
			}
		}
	});
	return false;
};

tokenauth.curateToken = function(app, token){
	//var productConfig = serverConfig.serving[app];
	//if (!productConfig || !Object.keys(productConfig).length) return false;

	var fileName = AESCrypt.encrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, app);
	if (!fileName || fileName.length < 1) return false;
	fileName += '.tkn';

	token = (token && token.length)?
		AESCrypt.encrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, token)+'\n'
		: '';

	fs.appendFile(tmpDir+"/"+fileName, token, function(err) {
    	if(err){
    		console.log(err);
    	}
	});
   	return true;
};

tokenauth.isExpired = function(token, timeout){
	var tokenObj = JSON.parse(token);
	var tokenDate = new Date(tokenObj.on);
	
	var currDate = new Date();
	var timeElapsed = Math.floor((currDate - tokenDate)/(1000*60)); //in minutes
	//console.log(timeElapsed);
	return (timeElapsed >= timeout)? true : false;
};

tokenauth.stripExpiredTokens = function(app){
	var productConfig = serverConfig.serving[app];
	if (!productConfig || !Object.keys(productConfig).length) return false;
	
	var timeout = (!productConfig.timeout)? 60 : productConfig.timeout;

	var fileName = AESCrypt.encrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, app);
	if (!fileName || fileName.length < 1) return false;
	fileName += '.tkn';

	fs.readFile(tmpDir+"/"+fileName, 'utf8', function (err, data){
		if (!data || data.length < 1) return true;
		var lines = data.split('\n');
		
		//console.log('line '+lines[0]);
		var line = lines[0];
		
		var fileToken = AESCrypt.decrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, line);
		//console.log('Decrypted: ' + fileToken);

		if (tokenauth.isExpired(fileToken, timeout)) {
			fs.readFile(tmpDir+"/"+fileName, 'utf8', function (readErr, newestdata){		
				if (readErr) console.log(readErr);
				
				newestdata = newestdata.substring(getPosition(newestdata,'\n',1)+1);
				fs.writeFile(tmpDir+"/"+fileName, newestdata, function(writeErr) {
				    if(writeErr) {
				        console.log(writeErr);
				    } else {
				        console.log("The file was saved!");
				    }
				});
			});
			return true;
		}
		
		return false;
	});
}

tokenauth.watchAppFile = function(app){
	var fileName = AESCrypt.encrypt(AESCrypt.cryptkey, process.env.isqClient_iVECTOR, app);
	if (!fileName || fileName.length < 1) return false;

	setInterval(function(){
		tokenauth.stripExpiredTokens(app);
	}, 15000);
	fs.watchFile(tmpDir+"/"+fileName,function(current, previous){
		//console.log(targetfile, 'is', event);
		tokenauth.stripExpiredTokens(app);
	});
};

module.exports = tokenauth;