var crypto = require('crypto');
var serverConfig = require('../serverConfig');
var nsys = 'hex';
var AESCrypt = {};
AESCrypt.cryptkey = crypto.createHash('sha256').update(process.env.PWD).digest();
AESCrypt.encrypt = function(cryptkey, iv, cleardata) {
	var encipher = crypto.createCipheriv('aes-256-cbc', cryptkey, iv);
	var encryptedBuffers = [encipher.update(cleardata, 'utf8', nsys)];
	var finalBuffer = encipher.final(nsys);
	encryptedBuffers.push(finalBuffer);
    
	var encryptdata = encryptedBuffers.join('');
	//console.log('Encrypted data: '+ encryptdata);
	return encryptdata;
};
AESCrypt.decrypt = function(cryptkey, iv, encryptdata) {
	if(serverConfig.noCrypt) return encryptdata;
	encryptdata = encryptdata.toString(nsys);

	var decipher = crypto.createDecipheriv('aes-256-cbc', cryptkey, iv);
	var dcryptedBuffers = [decipher.update(new Buffer(encryptdata, nsys))];
    dcryptedBuffers.push(decipher.final());

    var decoded = Buffer.concat(dcryptedBuffers).toString('utf8');

	/*var decoded = decipher.update(encryptdata, nsys, 'utf8');
	
	decoded += decipher.final('utf8');*/
	return decoded;
};

module.exports = AESCrypt;