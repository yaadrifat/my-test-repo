//version 3.0 for Web Service support and new field validation check
define([
    'global',
    'config',
	'jquery',
	'jqueryui',
	'jqueryvalidation',
    'underscore',
    'backbone'
], function(global,cfg, $,jq3,jq4, _, Backbone) {
	var stemConfig ='';
    var FieldValidation_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	//alert(" isquare running...");
        	//Add loading screen on ajax call
            //var date = new Date();
        	var loadingModalDiv = document.createElement('div');
            loadingModalDiv.id = 'loadingModalDiv';
            loadingModalDiv.className = 'isquareloader';
            $('body').append(loadingModalDiv);
            $('body').addClass("isquareloading");
            if(stemModel.get('TabName') != '') {	
        		var path = window.document.location.pathname.substring(11);// get jsp path of the url
				$.ajax({
            		type: 'POST',
      		  		url: cfg.isquareURL+'/getExcelData',
      		  		data: {tabName: stemModel.get('TabName')},
      		  		crossDomain: true,
      		  		complete: function(excelData, status){
        				//console.log("Read Excel Config complete" + excelData + " / " + status);
      		  			var excelData = JSON.parse(excelData.responseText);
        				console.log(excelData);
        				stemModel.runDefaultValidation(stemModel, excelData, status);
        				stemModel.readSourceField(stemModel, excelData, status);
        				$('body').removeClass("isquareloading");
      		  		}
        		});
        	}
            if(stemModel.get('FormID')[0] != '') {
        		var getParameterByName = function (name) {
            	    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            	    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            	};
            	var formId = getParameterByName("formId");
            	for (var i = 0; i < stemModel.get('FormID').length; i++) {
            		if (stemModel.get('FormID')[i] == formId){
            			//var elapsed = new Date() - date;
            			//console.log(elapsed);
            			$.ajax({
                    		type: 'POST',
              		  		url: cfg.isquareURL+'/getExcelData',
              		  		data: {tabName: stemModel.get('FormID')[i]},
              		  		crossDomain: true,
              		  		complete: function(excelData, status){
                				//console.log("Read Excel Config complete" + excelData + " / " + status);
              		  			var excelData = JSON.parse(excelData.responseText);
                				console.log(excelData);
                				stemModel.runDefaultValidation(stemModel, excelData, status);
                				stemModel.readSourceField(stemModel, excelData, status);
                				$('body').removeClass("isquareloading");      				
              		  		}
                		});
            		}
            	}
        	} 
        	if(stemModel.get('jspPages')[0] != '') {
            	var path = window.document.location.pathname.substring(11);// get jsp path of the url
            	for (var i = 0; i < stemModel.get('jspPages').length; i++) {
            		if (stemModel.get('jspPages')[i] == path){
            			$.ajax({
                    		type: 'POST',
              		  		url: cfg.isquareURL+'/getExcelData',
              		  		data: {tabName: stemModel.get('jspPages')[i]},
              		  		crossDomain: true,
              		  		complete: function(excelData, status){
                				//console.log("Read Excel Config complete" + excelData + " / " + status);
              		  			var excelData = JSON.parse(excelData.responseText);
                				console.log(excelData);
                				stemModel.runDefaultValidation(stemModel, excelData, status);
                				stemModel.readSourceField(stemModel, excelData, status);
                				$('body').removeClass("isquareloading");      				
              		  		}
                		});
            		}
            	}
        	}
        },
        runDefaultValidation: function(thisModel, excelData, status){
        	//console.log(excelData); 
        	if (excelData){
        		var configvalues = [], valueTypes = [], condOpts = [], compOpts = [], fieldElementTypeS = [], sourceConstructors = [], sourcevalueConstructors = [];
            	for (var j = 0; j < excelData.length; j++){//Loop for rules
            		//console.log("rule no."+j);
            		sourceConstructors[j] = []; sourcevalueConstructors[j] = [];configvalues[j] = []; valueTypes[j] = []; condOpts[j] = []; compOpts[j] = []; fieldElementTypeS[j] = []; 
            		for (var i = 0; i < excelData[j].SourceFields.length; i++){//Loop for source fields
            			if (excelData[j].SourceFields[i].FieldElementKeyS == 'advanced'){
							sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+excelData[j].SourceFields[i].FieldElementValueS+"'";
						} else {
							sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+"]'";
						}
            			configvalues[j][i] = excelData[j].SourceFields[i].FieldValueS;
    					valueTypes[j][i] = excelData[j].SourceFields[i].ValueType;
    					condOpts[j][i] = excelData[j].SourceFields[i].CondOpt;
    					compOpts[j][i] = excelData[j].SourceFields[i].CompOpt;
    					fieldElementTypeS[j][i] = excelData[j].SourceFields[i].FieldElementTypeS;
    					//sourcevalueConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+" ] option:selected'";		
            		}					
					for (var i = 0; i < excelData[j].TargetFields.length; i++){
	        			//console.log(excelData[j].TargetFields[i].DefaultValidation);
	        			//check default validation type
						var errormessage = '"span[id=error'+j+i+']"';
						var layover = '"div[id=alertMessage'+j+i+']"';
						var fieldElementTypeT = excelData[j].TargetFields[i].FieldElementTypeT;
						var configvalue = excelData[j].TargetFields[i].ConfigValue;
						var targetfieldtype = excelData[j].TargetFields[i].FieldElementTypeT;
						var fieldElementKeyT = excelData[j].TargetFields[i].FieldElementKeyT;
						var targetFieldName = excelData[j].TargetFields[i].FieldNameT;
	        			if (fieldElementKeyT == 'advanced'){
	        				var constructor = ""+excelData[j].TargetFields[i].FieldElementTypeT+excelData[j].TargetFields[i].FieldElementValueT+"'";
	        			} else {
	        				var constructor = ""+excelData[j].TargetFields[i].FieldElementTypeT+"["+excelData[j].TargetFields[i].FieldElementKeyT+"="+excelData[j].TargetFields[i].FieldElementValueT+"]'";
	        			}
	        			var DefaultValidation = excelData[j].TargetFields[i].DefaultValidation;
	        			if (DefaultValidation == 'hide element'){
	        				//Hide element based on configuration from Excel
	        				$(constructor).css({"display": "none"});
	        				//$(constructor).hide("fast");
							$(constructor).attr('required', false);
	        			}
	        			if (DefaultValidation == 'hide field'){
	        				//Hide field based on configuration from Excel
	        				$(constructor).closest("tr").css({"display": "none"});
	        				//$(constructor).closest("tr").hide("fast");
							$(constructor).attr('required', false);
								var fieldvalue = '';
								var logic = '';
								var count = 0;
								//console.log(condOpts.length);
								for (var v = 0; v < condOpts[j].length; v++){//Loop through sourcefields
									//console.log(compOpts[h][v]);
									//console.log(condOpts[h][v]);
									if (valueTypes[j][v] == 'text'){
										if (fieldElementTypeS[j][v] == 'select'){
											fieldvalue = $(sourceConstructors[j][v]).find("option:selected").text();
										} else {
											fieldvalue = $(sourceConstructors[j][v]).val();
										}
									}	
									if (valueTypes[j][v] == 'number' || valueTypes[j][v] == 'date'){
										fieldvalue = $(sourceConstructors[j][v]).val();
									}
									if (valueTypes[j][v] == 'checkbox' || valueTypes[j][v] == 'radio'){
										fieldvalue = $(sourceConstructors[j][v]).prop('checked');
									}
									if (compOpts[j][v] == 'equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue == ''";										
										} 
										else {
											logic = "fieldvalue == configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
									if (compOpts[j][v] == 'not equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue != ''";
										} 
										else {
											logic = "fieldvalue != configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
								}
								if (condOpts[j][0] == 'meets one'){
									if (count > 0){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}
								if (condOpts[j][0] == 'none'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}
								if (condOpts[j][0] == 'meets all'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}								
	        			}
						if (DefaultValidation == 'hide options'){
							//Hide field based on configuration from Excel
							$(constructor).closest("tr").css({"display": "none"});
							//$(constructor).closest("tr").hide("fast");
							$(constructor).attr('required', false);
								var fieldvalue = '';
								var logic = '';
								var count = 0;
								//console.log(condOpts.length);
								for (var v = 0; v < condOpts[j].length; v++){//Loop through sourcefields
									//console.log(compOpts[h][v]);
									//console.log(condOpts[h][v]);
									if (valueTypes[j][v] == 'text'){
										if (fieldElementTypeS[j][v] == 'select'){
											fieldvalue = $(sourceConstructors[j][v]).find("option:selected").text();
										} else {
											fieldvalue = $(sourceConstructors[j][v]).val();
										}
									}	
									if (valueTypes[j][v] == 'number' || valueTypes[j][v] == 'date'){
										fieldvalue = $(sourceConstructors[j][v]).val();
									}
									if (valueTypes[j][v] == 'checkbox' || valueTypes[j][v] == 'radio'){
										fieldvalue = $(sourceConstructors[j][v]).prop('checked');
									}
									if (compOpts[j][v] == 'equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue == ''";
										}
									    else {
											logic = "fieldvalue == configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
									if (compOpts[j][v] == 'not equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue != ''";
										} 
										else {
											logic = "fieldvalue != configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
								}
								if (condOpts[j][0] == 'meets one'){
									if (count > 0){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}
								if (condOpts[j][0] == 'none'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}
								if (condOpts[j][0] == 'meets all'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}								
	        			}
						if (DefaultValidation == 'show element required'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
						    $(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'show element'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
	        			}
						if (DefaultValidation == 'show field not required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
	        			}
						if (DefaultValidation == 'show field required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
	    					$(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'readonly'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (targetfieldtype == 'select') { 
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
								$(constructor+" option:selected").attr('disabled', false);
	        					$(constructor+" option:not(:selected)").attr('disabled', true);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
								$(constructor+" checked").attr('disabled', false);
	        					$(constructor+":not(:checked)").attr('disabled', true);
	        				} else {
	        					$(constructor).attr('readonly', true);
	        				}
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'editable'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (targetfieldtype == 'select') { 
	        					$(constructor).attr("style", "pointer-events: auto;");
	        					$(constructor).attr("tabindex", '');
	        					$(constructor+" option:not(:selected)").attr('disabled', false);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
		        				$(constructor).attr("style", "pointer-events: auto;");
		        				$(constructor).attr("tabindex", '');
		        				$(constructor+":not(:checked)").attr('disabled', false);
	        				} else {
	        					$(constructor).attr('readonly', false);
	        				}
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'disable'){
	        				//Disable fields based on configuration from Excel
	        				$(constructor).attr('disabled', true);
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'enable'){
	        				//Enable fields based on configuration from Excel
	        				$(constructor).attr('disabled', false);
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'show label'){
	        				if($(errormessage).length == 0) {
	        					var text = '<span id="error'+j+i+'" style="color:red">'+' '+configvalue+'</span>';
	        					var type = $(constructor)[0].type;
		        				if (type == 'radio' || type == 'checkbox') {
		        					$(constructor).closest("td").prepend(text);
			       				} else {
			       					$(constructor).after(text);
			       				}
	        				}	
	        			}
	        			if (DefaultValidation == 'remove label'){
	        				$(errormessage).remove();
	        			}
	        			if (DefaultValidation == 'none'){
	        				//doing nothing here!
	        			}
	        			if (DefaultValidation == 'autofill'){
	        				if(targetfieldtype == 'select'){
	        					$(constructor+" option:contains("+configvalue+")").attr('selected', 'selected');
	        				} else { 
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).attr({"checked": true});
								} else {
									$(constructor).val(configvalue);
								}
	        				}
							//("select[name=studyDivision] option:contains('Cardiology')").attr('selected', 'selected');
	        			}
	        			/*if (DefaultValidation == 'remove autofill'){
							$(constructor).val("");
	        			}*/
	        			if (DefaultValidation == 'show layover' || DefaultValidation == 'show layover top right'){
	        				if($(layover).length == 0) {
	        					var html = '<div id="alertMessage'+j+i+'" title="'+targetFieldName+'">\
		                			<p id="alertText'+j+i+'" style="display:none;">'+ configvalue +'</p>\
		        				</div>';
		        				$("#isquare").prepend(html);
	        				}
	        				$("#alertText"+j+i).css("display", "block");
	        				if (DefaultValidation == 'show layover'){
	                			jQuery("#alertMessage"+j+i).dialog();
	                		}
	                		if (DefaultValidation == 'show layover top right'){
	                			jQuery("#alertMessage"+j+i).dialog({position: {my:"top",at:"right top"}});
    						}
	        			}
	        			if (DefaultValidation == 'highlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": configvalue+' 2px solid'});
	        					//$(constructor).css({ "outline": configvalue+' 2px solid'});
							} else {
								$(constructor).css({"border": configvalue+' 2px solid'});
							}
	        			}
	        			if (DefaultValidation == 'unhighlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({ "border": ""});
	        					//$(constructor).css({ "outline": ""});
							} else {
								$(constructor).css({ "border": ""});
							}
	        			}
	        			if (DefaultValidation == 'change background color'){
							$(constructor).css("background-color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove background color'){
							$(constructor).css("background-color", "");
	        			}//("select[name=studyDivision]").css('color', 'red');
	        			if (DefaultValidation == 'change font color'){
							$(constructor).css("color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font color'){
							$(constructor).css("color", "");
	        			}
	        			if (DefaultValidation == 'change font weight'){
							$(constructor).css("font-weight", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font weight'){
							$(constructor).css("font-weight", "");
	        			}
	        			if (DefaultValidation == 'numeric check' || DefaultValidation == 'range check' || DefaultValidation == 'minimum length check' || DefaultValidation == 'maximum length check' || DefaultValidation == 'minimum check'|| DefaultValidation == 'maximum check'){
	        				var formID = $(constructor).closest('form')[0].id;
	        				$('#'+formID+' button[type="submit"]').click(function(){
								return $("#"+formID).valid();    
							});
							$("#"+formID).validate();
	        				if (DefaultValidation == 'numeric check'){
	        					$(constructor).rules("add", {
	        		                number: true,								
	        		                messages: {
	        		                    number: ' '+configvalue
	        		                }
	        		            });							
	        				}
	        				if (DefaultValidation == 'range check'){
	        					var range = configvalue.match(/[0-9]+/g).sort();
	        					$(constructor).rules("add", {
	        		                range: [range[0],range[1]],
	        		                messages: {
	        		                    range: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'minimum length check'){
	        					$(constructor).rules("add", {
	        						minlength: configvalue.replace(/[^0-9]/g,''),
	        		                messages: {
	        		                	minlength: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'maximum length check'){
	        					$(constructor).rules("add", {
	        						maxlength: configvalue.replace(/[^0-9]/g,''),
	        		                messages: {
	        		                	maxlength: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'minimum check'){
	        					$(constructor).rules("add", {
	        						min: parseInt(configvalue.replace(/[^0-9]/g,'')),
	        		                messages: {
	        		                	min: ' '+configvalue
	        		                }
	        		            });
	        					//console.log(configvalue.replace(/[^0-9]/g,''));
	        				}
	        				if (DefaultValidation == 'maximum check'){
	        					$(constructor).rules("add", {
	        						max: parseInt(configvalue.replace(/[^0-9]/g,'')),
	        		                messages: {
	        		                	max: ' '+configvalue
	        		                }
	        		            });
	        					//console.log(configvalue.replace(/[^0-9]/g,''));
	        				}
	        			}
	        		}    
				}
        	}
        },
        runDefaultValidationPerRule: function(thisModel, data, n, i, fieldvalues, status){
        	//console.log(Data);
        	if (data){
					for (var i = 0; i < data.TargetFields.length; i++){
	        			//console.log('length: '+data.TargetFields.length);
	        			//console.log(excelData[j].TargetFields[i].DefaultValidation);
	        			//check default validation type
	        			var errormessage = '"span[id=error'+n+i+']"';
	        			var layover = '"div[id=alertMessage'+n+i+']"';
	        			var fieldElementTypeT = data.TargetFields[i].FieldElementTypeT;
						var fieldElementKeyT = data.TargetFields[i].FieldElementKeyT;
	        			var configvalue = data.TargetFields[i].ConfigValue;
	        			var targetfieldtype = data.TargetFields[i].FieldElementTypeT;
	        			var targetFieldName = data.TargetFields[i].FieldNameT;
						if (fieldElementKeyT == 'advanced'){
							var constructor = ""+data.TargetFields[i].FieldElementTypeT+data.TargetFields[i].FieldElementValueT+"'";
						} else {
							var constructor = ""+data.TargetFields[i].FieldElementTypeT+"["+data.TargetFields[i].FieldElementKeyT+"="+data.TargetFields[i].FieldElementValueT+"]'";
						}
	        			var DefaultValidation = data.TargetFields[i].DefaultValidation;
	        			if (DefaultValidation == 'hide element'){
	        				//Hide element based on configuration from Excel
	        				$(constructor).css({"display": "none"});
	        				//$(constructor).hide("fast");
							$(constructor).attr('required', false);
							var type = $(constructor)[0].type;
							if (type == 'radio' || type == 'checkbox') {
								$(constructor).removeAttr('checked');
							} else {
								if (fieldElementTypeT == 'input' || fieldElementTypeT == 'select' || fieldElementTypeT == 'textarea'){
									$(constructor).val("");
								}
							}
	        			}
	        			if (DefaultValidation == 'hide field'){
	        				//Hide field based on configuration from Excel
	        				$(constructor).closest("tr").css({"display": "none"});
	        				//$(constructor).closest("tr").hide("fast");
							$(constructor).val("");
							//$(constructor).attr('required', false);
	        			}
						if (DefaultValidation == 'hide options'){
	        				//Hide field based on configuration from Excel
							$(constructor).closest("tr").css({"display": "none"});
							//$(constructor).closest("tr").hide("fast");
							$(constructor).removeAttr('checked');
							//$(constructor).attr('required', false);
	        			}
	        			if (DefaultValidation == 'show element'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
	        			}
	        			if (DefaultValidation == 'show element required'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
							$(constructor).attr('required', true);
	        			}
	        			if (DefaultValidation == 'show field required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
							$(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'show field not required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
	        			}
						if (DefaultValidation == 'readonly'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (fieldElementTypeT == 'select') { 
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
								$(constructor+" option:selected").attr('disabled', false);
	        					$(constructor+" option:not(:selected)").attr('disabled', true);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
								$(constructor+" checked").attr('disabled', false);
	        					$(constructor+":not(:checked)").attr('disabled', true);
	        				} else {
	        					$(constructor).attr('readonly', true);
	        				}
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'editable'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (fieldElementTypeT == 'select') { 
	        					$(constructor).attr("style", "pointer-events: auto;");
	        					$(constructor).attr("tabindex", '');
	        					$(constructor+" option:not(:selected)").attr('disabled', false);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
		        				$(constructor).attr("style", "pointer-events: auto;");
		        				$(constructor).attr("tabindex", '');
		        				$(constructor+":not(:checked)").attr('disabled', false);
	        				} else {
	        					$(constructor).attr('readonly', false);
	        				}
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'disable'){
	        				//Disable fields based on configuration from Excel
	        				$(constructor).attr('disabled', true);
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'enable'){
	        				//Enable fields based on configuration from Excel
	        				$(constructor).attr('disabled', false);
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'show label'){
	        				//Enable fields based on configuration from Excel
	        				if($(errormessage).length == 0) {
	        					i = 0; i++;
		        				if (configvalue.indexOf("response") > -1){
		        					for (var k = 0; k < fieldvalues.length; k++) {
		        						var key = new RegExp("response"+i, 'g');
		        						configvalue = configvalue.replace(key, fieldvalues[k]);		        					
		        						i++;
		        					}
	        					}
	        					var text = '<span id="error'+n+i+'" style="color:red">'+' '+configvalue+'</span>';
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).closest("td").prepend(text);
								} else {
									$(constructor).after(text);
								}
	        				}
	        			}
	        			if (DefaultValidation == 'remove label'){
	        				$(errormessage).remove();
	        			}
	        			if (DefaultValidation == 'none'){
	        				//doing nothing here!
	        			}
	        			if (DefaultValidation == 'autofill'){
	        				i = 0; i++;
	        				if (configvalue.indexOf("response") > -1){
	        					for (var k = 0; k < fieldvalues.length; k++) {
	        						var key = new RegExp("response"+i, 'g');
	        						configvalue = configvalue.replace(key, fieldvalues[k]);		        					
	        						i++;
	        					}
        					}
	        				if(targetfieldtype == 'select'){
	        					$(constructor+" option:contains("+configvalue+")").attr('selected', 'selected');
	        				} else { 
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).attr({"checked": true});
								} else {
									$(constructor).val(configvalue);
								}
	        				}
	        			}
	        			if (DefaultValidation == 'show layover' || DefaultValidation == 'show layover top right'){
	        				if($(layover).length == 0) {	    
	        					i = 0; i++;
		        				if (configvalue.indexOf("response") > -1){
		        					for (var k = 0; k < fieldvalues.length; k++) {
		        						var key = new RegExp("response"+i, 'g');
		        						configvalue = configvalue.replace(key, fieldvalues[k]);		        					
		        						i++;
		        					}
	        					}
	        					var html = '<div id="alertMessage'+n+i+'" title="'+targetFieldName+'">\
		                			<p id="alertText'+n+i+'" style="display:none;">'+ configvalue +'</p>\
		        				</div>';
	        					$("#isquare").prepend(html);
	        				}
	        				$("#alertText"+n+i).css("display", "block");
	                		if (DefaultValidation == 'show layover'){
	                			jQuery("#alertMessage"+n+i).dialog();
	                		}
	                		if (DefaultValidation == 'show layover top right'){
	                			jQuery("#alertMessage"+n+i).dialog({position: {my:"top",at:"right top"}});
    						}
	        			}
	        			if (DefaultValidation == 'highlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": configvalue+' 2px solid'});
	        					//$(constructor).css({ "outline": configvalue+' 2px solid'});
							} else {
								$(constructor).css({ "border": configvalue+' 2px solid'});
							}
	        			}
	        			if (DefaultValidation == 'unhighlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": ""});
	        					//$(constructor).css({ "outline": ""});
							} else {
								$(constructor).css({ "border": ""});
							}
	        			}
	        			if (DefaultValidation == 'change background color'){
							$(constructor).css("background-color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove background color'){
							$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'change font color'){
							$(constructor).css("color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font color'){
							$(constructor).css("color", "");
	        			}
	        			if (DefaultValidation == 'change font weight'){
							$(constructor).css("font-weight", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font weight'){
							$(constructor).css("font-weight", "");
	        			}
	        			if (DefaultValidation == 'numeric check' || DefaultValidation == 'range check' || DefaultValidation == 'minimum length check' || DefaultValidation == 'maximum length check' || DefaultValidation == 'minimum check'|| DefaultValidation == 'maximum check'){
	        				var formID = $(constructor).closest('form')[0].id;
							$('#'+formID+' button[type="submit"]').click(function(){
								return $("#"+formID).valid();    
							});
	        				$("#"+formID).validate();
	        				if (DefaultValidation == 'numeric check'){
	        					$(constructor).rules("add", {
	        		                number: true,
	        		                messages: {
	        		                    number: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'range check'){
	        					var range = configvalue.match(/[0-9]+/g).sort();
	        					$(constructor).rules("add", {
	        		                range: [range[0],range[1]], 
	        		                messages: {
	        		                    range: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'minimum length check'){
	        					$(constructor).rules("add", {
	        						minlength: configvalue.replace(/[^0-9]/g,''),
	        		                messages: {
	        		                	minlength: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'maximum length check'){
	        					$(constructor).rules("add", {
	        						maxlength: configvalue.replace(/[^0-9]/g,''),
	        		                messages: {
	        		                	maxlength: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'minimum check'){
	        					$(constructor).rules("add", {
	        						min: parseInt(configvalue.replace(/[^0-9]/g,'')),
	        		                messages: {
	        		                	min: ' '+configvalue
	        		                }
	        		            });
	        				}
	        				if (DefaultValidation == 'maximum check'){
	        					$(constructor).rules("add", {
	        						max: parseInt(configvalue.replace(/[^0-9]/g,'')),
	        		                messages: {
	        		                	max: ' '+configvalue
	        		                }
	        		            });
	        				}
	        			}
	        		}       
        	}
        },
        readSourceField: function(thisModel, excelData, status){
        	var runFieldValidation = function (n, i, fieldvalues){
				for (var m = 0; m < desiredValidation[n].length; m++){
   					var errormessage = '"span[id=error'+n+m+']"';
   					var layover = '"div[id=alertMessage'+n+m+']"';
   					if (desiredValidation[n][m] == 'hide element'){
        				//Hide element based on configuration from Excel
   						$(constructors[n][m]).css({"display": "none"});
   						//$(constructors[n][m]).hide("fast");
						$(constructors[n][m]).attr('required', false);
						var type = $(constructors[n][m])[0].type;
						if (type == 'radio' || type == 'checkbox') {
							$(constructors[n][m]).removeAttr('checked');
						} else {
							if (fieldElementTypeT[n][m] == 'input' || fieldElementTypeT[n][m] == 'select' || fieldElementTypeT[n][m] == 'textarea'){
								$(constructors[n][m]).val("");
							}
						}
	        		}
	        		if (desiredValidation[n][m] == 'hide field'){
	        			//Hide field based on configuration from Excel
	        			$(constructors[n][m]).closest("tr").css({"display": "none"});
	        			//$(constructors[n][m]).closest("tr").hide("fast");
						$(constructors[n][m]).val("");
						//$(constructors[n][m]).attr('required', false);
	        		}
					if (desiredValidation[n][m] == 'hide options'){
	        			//Hide field based on configuration from Excel
						$(constructors[n][m]).closest("tr").css({"display": "none"});
						//$(constructors[n][m]).closest("tr").hide("fast");
						//$(constructors[n][m]).attr('required', false);
						$(constructors[n][m]).removeAttr('checked');
	        		}
	        		if (desiredValidation[n][m] == 'show element'){
	        			//Hide element based on configuration from Excel
	    				$(constructors[n][m]).show("fast");
	        		}
	        		if (desiredValidation[n][m] == 'show element required'){
	        			//Hide element based on configuration from Excel
	    				$(constructors[n][m]).show("fast");
						$(constructors[n][m]).attr('required', true);
	        		}
	        		if (desiredValidation[n][m] == 'show field required'){
	        			//Hide field based on configuration from Excel
	    				$(constructors[n][m]).closest("tr").show("fast");
						$(constructors[n][m]).attr('required', true);
	       			}
					if (desiredValidation[n][m] == 'show field not required'){
	        			//Hide field based on configuration from Excel
	    				$(constructors[n][m]).closest("tr").show("fast");
	       			}
	       		
	       			if (desiredValidation[n][m] == 'readonly'){
	       				//Make field readonly based on configuration from Excel
	       				var type = $(constructors[n][m])[0].type;
	       				if (fieldElementTypeT[n][m] == 'select') { 
        					$(constructors[n][m]).attr("style", "pointer-events: none;");
        					$(constructors[n][m]).attr("tabindex", -1);	
							$(constructors[n][m]+" option:selected").attr('disabled', false);
        					$(constructors[n][m]+" option:not(:selected)").attr('disabled', true);
            			}
        				if (type == 'radio' || type == 'checkbox') { 
            				$(constructors[n][m]).attr("style", "pointer-events: none;");
            				$(constructors[n][m]).attr("tabindex", -1);	
							$(constructors[n][m]+" checked").attr('disabled', false);	
            				$(constructors[n][m]+":not(:checked)").attr('disabled', true);
	       				} else {
        					$(constructors[n][m]).attr('readonly', true);
        				}
        				$(constructors[n][m]).css("background-color", "lightgrey");
	        		}
	        		if (desiredValidation[n][m] == 'editable'){
	        			//Make field readonly based on configuration from Excel
	        			var type = $(constructors[n][m])[0].type;
	        			if (fieldElementTypeT[n][m] == 'select') { 
        					$(constructors[n][m]).attr("style", "pointer-events: auto;");
        					$(constructors[n][m]).attr("tabindex", '');	
        					$(constructors[n][m]+" option:not(:selected)").attr('disabled', false);
            			}
        				if (type == 'radio' || type == 'checkbox') { 
            				$(constructors[n][m]).attr("style", "pointer-events: auto;");
            				$(constructors[n][m]).attr("tabindex", '');	
            				$(constructors[n][m]+":not(:checked)").attr('disabled', false);
	       				} else {
        					$(constructors[n][m]).attr('readonly', false);
        				}
	        			$(constructors[n][m]).css("background-color", "");
	        		}
	        		if (desiredValidation[n][m] == 'disable'){
	        			//Disable fields based on configuration from Excel
	        			$(constructors[n][m]).attr('disabled', true);
	        			$(constructors[n][m]).css("background-color", "lightgrey");
	        		}
	        		if (desiredValidation[n][m] == 'enable'){
	       				//Enable fields based on configuration from Excel
	       				$(constructors[n][m]).attr('disabled', false);
	       				$(constructors[n][m]).css("background-color", "");
	       			}
	        		if (desiredValidation[n][m] == 'show label'){
	       				//Enable fields based on configuration from Excel
	        			if($(errormessage).length == 0) {
	        				i = 0; i++;
	        				if (configvalueTargets[n][m].indexOf("response") > -1){
	        					for (var k = 0; k < fieldvalues.length; k++) {
	        						var key = new RegExp("response"+i, 'g');
	        						configvalueTargets[n][m] = configvalueTargets[n][m].replace(key, fieldvalues[k]);
	        						i++;
	        					}
        					}
	        				var text = '<span id="error'+n+m+'" style="color:red">'+' '+configvalueTargets[n][m]+'</span>';
	        				var type = $(constructors[n][m])[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructors[n][m]).closest("td").prepend(text);
	        				} else {
	        					$(constructors[n][m]).after(text);
	        				}
	        			}
	       			}
	        		if (desiredValidation[n][m] == 'remove label'){
	       				//Enable fields based on configuration from Excel
	       				$(errormessage).remove();
	       			}
	        		if (desiredValidation[n][m] == 'none'){
        				//doing nothing here!
        			}
	        		if (desiredValidation[n][m] == 'autofill'){
						//$(constructors[n][m]).val(configvalueTargets[n][m]);
	        			i = 0; i++;
        				if (configvalueTargets[n][m].indexOf("response") > -1){
        					for (var k = 0; k < fieldvalues.length; k++) {
        						var key = new RegExp("response"+i, 'g');
        						configvalueTargets[n][m] = configvalueTargets[n][m].replace(key, fieldvalues[k]);
        						i++;
        					}
    					}
	        			if(targetfieldtype[n][m] == 'select'){
							$(constructors[n][m]+" option:contains("+configvalueTargets[n][m]+")").attr('selected', 'selected');
							//alert('hi');
	        			} else {
							var type = $(constructors[n][m])[0].type;
							if (type == 'radio' || type == 'checkbox') {
								$(constructors[n][m]).attr({"checked": true});
							} else {
								$(constructors[n][m]).val(configvalueTargets[n][m]);
							}
						}
	        		}
	        		/*if (desiredValidation[n][m] == 'remove autofill'){
						$(constructors[n][m]).val("");
	        		}*/
	        		if (desiredValidation[n][m] == 'show layover' || desiredValidation[n][m] == 'show layover top right'){
	        			if($(layover).length == 0) {
	        				i = 0; i++;
	        				if (configvalueTargets[n][m].indexOf("response") > -1){
	        					for (var k = 0; k < fieldvalues.length; k++) {
	        						var key = new RegExp("response"+i, 'g');
	        						configvalueTargets[n][m] = configvalueTargets[n][m].replace(key, fieldvalues[k]);
	        						i++;
	        					}
        					}
	        				//alert('configval: '+configvalueTargets[n][m]);
	        				var html = '<div id="alertMessage'+n+m+'" title="'+targetFieldName[n][m]+'">\
                				<p id="alertText'+n+m+'" style="display:none;">'+ configvalueTargets[n][m] +'</p>\
                			</div>';
	        				$("#isquare").prepend(html);
	        			}
	        			$("#alertText"+n+m).css("display", "block");
        				//jQuery("#alertMessage"+n+m).dialog();
        				if (desiredValidation[n][m] == 'show layover'){
        					jQuery("#alertMessage"+n+m).dialog();
                		}
                		if (desiredValidation[n][m] == 'show layover top right'){
                			jQuery("#alertMessage"+n+m).dialog({position: {my:"top",at:"right top"}});
                			//jQuery("#alertMessage"+n+m).dialog();
						}
        			}
	        		if (desiredValidation[n][m] == 'highlight field'){
	        			var type = $(constructors[n][m])[0].type;
	        			if (type == 'radio' || type == 'checkbox') {
	        				$(constructors[n][m]).closest("td").css({"border": configvalueTargets[n][m]+' 2px solid'});
	        				//$(constructors[n][m]).css({ "outline": configvalueTargets[n][m]+' 2px solid'});
						} else {
							$(constructors[n][m]).css({ "border": configvalueTargets[n][m]+' 2px solid'});
						}
        			}
        			if (desiredValidation[n][m] == 'unhighlight field'){
        				var type = $(constructors[n][m])[0].type;
        				if (type == 'radio' || type == 'checkbox') {
        					$(constructors[n][m]).closest("td").css({ "border": ""});
        					//$(constructors[n][m]).css({ "outline": ""});
						} else {
							$(constructors[n][m]).css({ "border": ""});
						}	
        			}
        			if (desiredValidation[n][m] == 'change background color'){
						$(constructors[n][m]).css("background-color", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove background color'){
						$(constructors[n][m]).css("background-color", "");
        			}
        			if (desiredValidation[n][m] == 'change font color'){
						$(constructors[n][m]).css("color", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove font color'){
						$(constructors[n][m]).css("color", "");
        			}
        			if (desiredValidation[n][m] == 'change font weight'){
						$(constructors[n][m]).css("font-weight", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove font weight'){
						$(constructors[n][m]).css("font-weight", "");
        			}
        			if (desiredValidation[n][m] == 'numeric check' || desiredValidation[n][m] == 'minimum length check' || desiredValidation[n][m] == 'maximum length check' || desiredValidation[n][m] == 'range check'|| desiredValidation[n][m] == 'minimum check' || desiredValidation[n][m] == 'maximum check'){
        				var formID = $(constructors[n][m]).closest('form')[0].id;
						$('#'+formID+' button[type="submit"]').click(function(){
							return $("#"+formID).valid();    
						});
        				//var targetID = excelData[n].TargetFields[m].FieldElementValueT;
        				$("#"+formID).validate();
        				if (desiredValidation[n][m] == 'numeric check'){
        					$(constructors[n][m]).rules("add", {
        		                number: true,
        		                //minlength: 2,
        		                messages: {
        		                    required: ' '+configvalueTargets[n][m]        		                    
        		                }
        		            });
        				}
        				if (desiredValidation[n][m] == 'range check'){
        					var range = configvalueTargets[n][m].match(/[0-9]+/g).sort();
        					$(constructors[n][m]).rules("add", {
        						range: [range[0],range[1]],
        		                //minlength: 2,
        		                messages: {
        		                    range: ' '+configvalueTargets[n][m]        		                    
        		                }
        		            });
        				}
        				if (desiredValidation[n][m] == 'minimum length check'){
        					$(constructors[n][m]).rules("add", {
        						minlength: configvalueTargets[n][m].replace(/[^0-9]/g,''),
        		                messages: {
        		                	minlength: ' '+configvalueTargets[n][m]
        		                }
        		            });
        				}
        				if (desiredValidation[n][m] == 'maximum length check'){
        					$(configvalueTargets[n][m]).rules("add", {
        						maxlength: configvalueTargets[n][m].replace(/[^0-9]/g,''),
        		                messages: {
        		                	maxlength: ' '+configvalueTargets[n][m]
        		                }
        		            });
        				}
        				if (desiredValidation[n][m] == 'minimum check'){
        					$(configvalueTargets[n][m]).rules("add", {
        						min: parseInt(configvalueTargets[n][m].replace(/[^0-9]/g,'')),
        		                messages: {
        		                	min: ' '+configvalueTargets[n][m]
        		                }
        		            });
        				}
        				if (desiredValidation[n][m] == 'maximum check'){
        					$(configvalueTargets[n][m]).rules("add", {
        						max: parseInt(configvalueTargets[n][m].replace(/[^0-9]/g,'')),
        		                messages: {
        		                	max: ' '+configvalueTargets[n][m]
        		                }
        		            });
        				}
        			}
				}
			};
			var callWebService = function (n,i){
				var webserviceResponse = response[n][i];
				var webserviceRequest = request[n][i];
				//var URL = fabricURL[n][i];
				var getParameterByName = function (name) {
				    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.href);
				    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
				};
				if (webserviceResponse != '-'){
					for (var v = 0; v < condOpts[n].length; v++){
						if (valueTypes[n][v] == 'text'){
							if (fieldElementTypeS[n][v] == 'select'){
								fieldvalue = $(sourceConstructors[n][v]).find("option:selected").text();
							} else {
								fieldvalue = $(sourceConstructors[n][v]).val();
							}	
						}
						if (valueTypes[n][v] == 'number' || valueTypes[n][v] == 'date'){
							fieldvalue = $(sourceConstructors[n][v]).val();
						}
						if (valueTypes[n][v] == 'checkbox' || valueTypes[n][v] == 'radio'){
							fieldvalue = $(sourceConstructors[n][v]).prop('checked');
						}
						if (valueTypes[n][v] == 'parameter'){
							fieldvalue = getParameterByName(fieldName[n][v]);
						}
						var key = new RegExp("substitute"+(v+1), 'g');
						webserviceRequest = webserviceRequest.replace(key, fieldvalue);
						//configvalues[n][v] = configvalues[n][v].replace("substitute"+(v+1), fieldvalue);
					}
					console.log(webserviceRequest);
					var fabricRequest = JSON.parse(webserviceRequest);
						fabricRequest["url"] = fabricURL[n][i];
						console.log(fabricRequest);
						$.ajax({
		            		type: 'POST',
		      		  		url: cfg.isquareURL+'/callFabric',
		      		  		dataType: 'json',
		      		  		data: JSON.stringify(fabricRequest),
		      		  		crossDomain: true,
		      		  		contentType: 'text; charset="utf-8"',
		      		  		//async: callsetting,
		      		  		success: function(data){
		      		  			console.log(data);
		        				runComparisonOptWS(n,i,data);
		    			  	}, 
		    			  	error: function(jqXHR, textStatus, errorThrown){
		    			  		//return dfd.reject();
		    			        console.log("Unsuccessful call to Fabric, textStatus: "+textStatus+' error: '+errorThrown);
		    			  	}
		        		});
				}
			};			
        	var runComparisonOptWS = function (n,i,userData){
        		fieldvalues = [];
				var logic = '';
				var count = 0;
				//console.log(condOpts.length);
				for (var v = 0; v < condOpts[n].length; v++){//Loop through sourcefields
				if (response[n][v] != '-'){
					var desiredResponse = response[n][v].split(".");
	        		var fieldvalue = '';
	        		for (var h = 0; h < desiredResponse.length; h++){
	        			if (fieldvalue == ''){
	        				fieldvalue = userData[desiredResponse[h]];
	        			} else {
	        				fieldvalue = fieldvalue[desiredResponse[h]];
	        			}
	        		}
	        		fieldvalues[v] = fieldvalue;
					//console.log(fieldvalue);
					if (compOpts[n][v] == 'equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue == ''";
							//console.log(logic);
						}
						else {
							logic = "fieldvalue == configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'not equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue != ''";
						}
						else {
							logic = "fieldvalue != configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue > configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue < configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue >= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue <= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					  }
					}
				}
				//console.log(count);
				if (condOpts[n][0] == 'meets one'){
					if (count > 0){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}
				if (condOpts[n][0] == 'none'){
					if (count == condOpts[n].length){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}
				if (condOpts[n][0] == 'meets all'){
					if (count == condOpts[n].length){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}	
				//missing date here
        		//console.log("fieldvalue"+fieldvalue);
				//console.log("configvalue"+configvalues[n]);
				//console.log("condOpt"+condOpts[n][i]);
				//console.log("compOpt"+compOpts[n][i]);				
        	};
        	var runComparisonOpt = function (n,i){
        		var fieldvalues = '';
				//Logic to check field value type
        		/*if (valueTypes[n][i] == 'text'){
					fieldvalue = $(sourceConstructors[n][i]).find("option:selected").text();
					//alert(sourceConstructors[n][i]);
				}
				if (valueTypes[n][i] == 'number'){
					fieldvalue = $(sourceConstructors[n][i]).val();
					//alert(sourceConstructors[n][i]);
				}*/
				var logic = '';
				var count = 0;
				//console.log(condOpts.length);
				for (var v = 0; v < condOpts[n].length; v++){//Loop through sourcefields
					//console.log(compOpts[h][v]);
					//console.log(condOpts[h][v]);
					if (valueTypes[n][v] == 'text'){
						if (fieldElementTypeS[n][v] == 'select'){
							fieldvalue = $(sourceConstructors[n][v]).find("option:selected").text();
						} else {
							fieldvalue = $(sourceConstructors[n][v]).val();
						}	
					}
					if (valueTypes[n][v] == 'number' || valueTypes[n][v] == 'date'){
						fieldvalue = $(sourceConstructors[n][v]).val();
					}
					if (valueTypes[n][v] == 'checkbox' || valueTypes[n][v] == 'radio'){
						fieldvalue = $(sourceConstructors[n][v]).prop('checked');
					}
					if (valueTypes[n][v] == 'parameter'){
						var getParameterByName = function (name) {
						    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.href);
						    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
						};
						fieldvalue = getParameterByName(fieldName[n][v]);
						var key = new RegExp("substitute"+(v+1), 'g');
						//console.log("configvaluelength"+configvalueTargets[n].length);
						for (var k = 0; k < configvalueTargets[n].length; k++){//Loop through targetfields	
							configvalueTargets[n][k] = configvalueTargets[n][k].replace(key, fieldvalue);
							//console.log("xxx"+configvalueTargets[n][k]);
						}
					}
					//console.log(fieldvalue);
					if (compOpts[n][v] == 'equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue == ''";
							//console.log(logic);
						}
						else {
							logic = "fieldvalue == configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'not equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue != ''";
						}
						else {
							logic = "fieldvalue != configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue > configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue < configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue >= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue <= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
				}
				//console.log(count);
				if (condOpts[n][0] == 'meets one'){
					if (count > 0){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}
				if (condOpts[n][0] == 'none'){
					if (count == condOpts[n].length){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}
				if (condOpts[n][0] == 'meets all'){
					if (count == condOpts[n].length){
						runFieldValidation(n, i, fieldvalues);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, i, fieldvalues, status);
					}
				}					
        	};
        	var checkandValidateTargetField = function (n,i){
        		var webserviceRequest = request[n][i];
        		$(document).ready(function(){
        			if (webserviceRequest != '-'){
        				callWebService(n,i);
        			} else {
        				runComparisonOpt(n,i);
        			}       			
        		});
        		if (valueTypes[n][i] == 'radio'){
					var sourceConstructorForRadio = ""+excelData[n].SourceFields[i].FieldElementTypeS+":"+valueTypes[n][i]+"'";
					$(sourceConstructorForRadio).on("change click", function(){
					if (webserviceRequest != '-'){
							callWebService(n,i);
	        			} else {
	        				runComparisonOpt(n,i);
	        			}
					});
				}
				if (valueTypes[n][i] == 'checkbox'){
					var sourceConstructorForCheckbox = ""+excelData[n].SourceFields[i].FieldElementTypeS+":"+valueTypes[n][i]+"'";
					$(sourceConstructorForCheckbox).on("change click", function(){
					if (webserviceRequest != '-'){
							callWebService(n,i);
	        			} else {
	        				runComparisonOpt(n,i);
	        			}
					});
				} else {
					$(sourceConstructors[n][i]).on("change keyup paste", function(){
						if (webserviceRequest != '-'){
							callWebService(n,i);
	        			} else {
	        				runComparisonOpt(n,i);
	        			}
					});
					//Enable below function if use in eResearch
					$("a[href=#]").on("focus", function(){
						runComparisonOpt(n,i);
					});
				}
        	};
        	var configvalues = [], valueTypes = [], condOpts = [], compOpts = [], fieldElementTypeS = [], sourceConstructors = [], sourcevalueConstructors = [], request = [], response = [], fabricURL = [], fieldName = [];
        	for (var j = 0; j < excelData.length; j++){//Loop for rules
        		//console.log("rule no."+j);
        		sourceConstructors[j] = []; sourcevalueConstructors[j] = [];configvalues[j] = []; valueTypes[j] = []; condOpts[j] = []; compOpts[j] = []; fieldElementTypeS[j] = [], request[j] = [], response[j] = [], fabricURL[j] = [], fieldName[j] = []; 
        		for (var i = 0; i < excelData[j].SourceFields.length; i++){//Loop for source fields
        			if (excelData[j].SourceFields[i].FieldElemechntKeyS == 'advanced'){
						sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+excelData[j].SourceFields[i].FieldElementValueS+"'";
					} else {
						sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+"]'";
					}
        			configvalues[j][i] = excelData[j].SourceFields[i].FieldValueS;
					valueTypes[j][i] = excelData[j].SourceFields[i].ValueType;
					condOpts[j][i] = excelData[j].SourceFields[i].CondOpt;
					compOpts[j][i] = excelData[j].SourceFields[i].CompOpt;
					fieldElementTypeS[j][i] = excelData[j].SourceFields[i].FieldElementTypeS;
					request[j][i] = excelData[j].SourceFields[i].Request;
					response[j][i] = excelData[j].SourceFields[i].Response;
					fabricURL[j][i] = excelData[j].SourceFields[i].FabricURL;
					fieldName[j][i] = excelData[j].SourceFields[i].FieldNameS;
					//sourcevalueConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+" ] option:selected'";		
        		}
        	}
        	var desiredValidation = [], constructors = [], configvalueTargets = [], targetfieldtype = [], fieldElementTypeT = [], fieldElementKeyT = [], targetFieldName = [];
        	//console.log(condOpts);
        	//console.log(compOpts);
        	for (var n = 0; n < excelData.length; n++){
        		desiredValidation[n] = []; constructors[n] = []; configvalueTargets[n] = [], targetfieldtype[n] = [], fieldElementTypeT[n] = [], fieldElementKeyT[n] = [], targetFieldName[n] = [];
				for (var k = 0; k < excelData[n].TargetFields.length; k++){//Loop for target field
					desiredValidation[n][k] = excelData[n].TargetFields[k].DesiredValidation;
					fieldElementTypeT[n][k] = excelData[n].TargetFields[k].FieldElementTypeT;
					fieldElementKeyT[n][k] = excelData[n].TargetFields[k].FieldElementKeyT;
					if (fieldElementKeyT[n][k] == 'advanced'){
						constructors[n][k] = ""+excelData[n].TargetFields[k].FieldElementTypeT+excelData[n].TargetFields[k].FieldElementValueT+"'";
					} else {
						constructors[n][k] = ""+excelData[n].TargetFields[k].FieldElementTypeT+"["+excelData[n].TargetFields[k].FieldElementKeyT+"="+excelData[n].TargetFields[k].FieldElementValueT+"]";
					}
					configvalueTargets[n][k] = excelData[n].TargetFields[k].ConfigValue;
					targetfieldtype[n][k] = excelData[n].TargetFields[k].FieldElementTypeT;
					targetFieldName[n][k] = excelData[n].TargetFields[k].FieldNameT;
				}	
				for (var i = 0; i < excelData[n].SourceFields.length; i++){
					//console.log(compOpts[n][i]);
					checkandValidateTargetField(n,i);
				}
			}
        }
    });
    return FieldValidation_Model;
});