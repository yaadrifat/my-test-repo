//version 2.4 for HF4
define([
    'global',
    'config',
    'jquery',
    'underscore',
    'backbone'
], function(global,cfg, $, _, Backbone) {
	var stemConfig ='';
    var FieldValidation_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	//alert(" isquare running...");
			//FieldValidation version 2 with mandatory field logic support for Skip Logic package May 1 2017
            var loadingIcon = function() {
			var loadingModalDiv = document.createElement('div');
            		loadingModalDiv.id = 'isqloadingModalDiv';
            		loadingModalDiv.className = 'isquareloader';
            		$('body').append(loadingModalDiv);
            		$('body').addClass("isquareloading");
		};
        	if(stemModel.get('FormID')[0] != '') {
        		var getParameterByName = function (name) {
            	    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            	    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            	};
            	var formId = getParameterByName("formId");
            	for (var i = 0; i < stemModel.get('FormID').length; i++) {
            		if (stemModel.get('FormID')[i] == formId){
				//loadingIcon();
            			$.ajax({
                    		type: 'POST',
              		  		url: cfg.isquareURL+'/getExcelData',
              		  		data: {fileName: stemModel.get('ExcelFile'), tabName: stemModel.get('FormID')[i]},
              		  		crossDomain: true,
              		  		complete: function(excelData, status){
                				//console.log("Read Excel Config complete" + excelData + " / " + status);
              		  			var excelData = JSON.parse(excelData.responseText);
                				console.log(excelData);
                				stemModel.runDefaultValidation(stemModel, excelData, status);
                				stemModel.readSourceField(stemModel, excelData, status);
						//$('body').removeClass("isquareloading"); 
              		  		}
                		});
            		}
            	}
        	} 
        	if(stemModel.get('TabName') != '') {
			//loadingIcon();
        		$.ajax({
            		type: 'POST',
      		  		url: cfg.isquareURL+'/getExcelData',
      		  		data: {fileName: stemModel.get('ExcelFile'), tabName: stemModel.get('TabName')},
      		  		crossDomain: true,
      		  		complete: function(excelData, status){
        				//console.log("Read Excel Config complete" + excelData + " / " + status);
      		  			var excelData = JSON.parse(excelData.responseText);
        				console.log(excelData);
        				stemModel.runDefaultValidation(stemModel, excelData, status);
        				stemModel.readSourceField(stemModel, excelData, status);
					//$('body').removeClass("isquareloading"); 
      		  		}
        		});
        	}
        },
        runDefaultValidation: function(thisModel, excelData, status){
        	//console.log(excelData); 
        	if (excelData){
        		var configvalues = [], valueTypes = [], condOpts = [], compOpts = [], fieldElementTypeS = [], sourceConstructors = [], sourcevalueConstructors = [];
            	for (var j = 0; j < excelData.length; j++){//Loop for rules
            		//console.log("rule no."+j);
            		sourceConstructors[j] = []; sourcevalueConstructors[j] = [];configvalues[j] = []; valueTypes[j] = []; condOpts[j] = []; compOpts[j] = []; fieldElementTypeS[j] = []; 
            		for (var i = 0; i < excelData[j].SourceFields.length; i++){//Loop for source fields
            			if (excelData[j].SourceFields[i].FieldElementKeyS == 'advanced'){
							sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+excelData[j].SourceFields[i].FieldElementValueS+"'";
						} else {
							sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+"]'";
						}
            			configvalues[j][i] = excelData[j].SourceFields[i].FieldValueS;
    					valueTypes[j][i] = excelData[j].SourceFields[i].ValueType;
    					condOpts[j][i] = excelData[j].SourceFields[i].CondOpt;
    					compOpts[j][i] = excelData[j].SourceFields[i].CompOpt;
    					fieldElementTypeS[j][i] = excelData[j].SourceFields[i].FieldElementTypeS;
    					//sourcevalueConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+" ] option:selected'";		
            		}					
					for (var i = 0; i < excelData[j].TargetFields.length; i++){
	        			//console.log(excelData[j].TargetFields[i].DefaultValidation);
	        			//check default validation type
						var errormessage = '"span[id=error'+j+i+']"';
						var layover = '"div[id=alertMessage'+j+i+']"';
						var configvalue = excelData[j].TargetFields[i].ConfigValue;
						var targetfieldtype = excelData[j].TargetFields[i].FieldElementTypeT;
						var fieldElementKeyT = excelData[j].TargetFields[i].FieldElementKeyT;; 
	        			if (fieldElementKeyT == 'advanced'){
	        				var constructor = ""+excelData[j].TargetFields[i].FieldElementTypeT+excelData[j].TargetFields[i].FieldElementValueT+"'";
	        			} else {
	        				var constructor = ""+excelData[j].TargetFields[i].FieldElementTypeT+"["+excelData[j].TargetFields[i].FieldElementKeyT+"="+excelData[j].TargetFields[i].FieldElementValueT+"]'";
	        			}
	        			var DefaultValidation = excelData[j].TargetFields[i].DefaultValidation;
	        			if (DefaultValidation == 'hide element'){
	        				//Hide element based on configuration from Excel
	        				$(constructor).css({"display": "none"});
							//$(constructor).hide("fast");
							$(constructor).attr('required', false);
	        			}
	        			if (DefaultValidation == 'hide field'){
	        				//Hide field based on configuration from Excel
							$(constructor).closest("tr").css({"display": "none"});
	    					//$(constructor).closest("tr").hide("fast");
							$(constructor).attr('required', false);
								var fieldvalue = '';
								var logic = '';
								var count = 0;
								//console.log(condOpts.length);
								for (var v = 0; v < condOpts[j].length; v++){//Loop through sourcefields
									//console.log(compOpts[h][v]);
									//console.log(condOpts[h][v]);
									if (valueTypes[j][v] == 'text'){
										if (fieldElementTypeS[j][v] == 'select'){
											fieldvalue = $(sourceConstructors[j][v]).find("option:selected").text();
										}
									}	
									if (valueTypes[j][v] == 'number' || valueTypes[j][v] == 'date'){
										fieldvalue = $(sourceConstructors[j][v]).val();
									}
									if (valueTypes[j][v] == 'checkbox' || valueTypes[j][v] == 'radio'){
										fieldvalue = $(sourceConstructors[j][v]).prop('checked');
									}
									if (compOpts[j][v] == 'equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue == ''";										
										} 
										else {
											logic = "fieldvalue == configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
									if (compOpts[j][v] == 'not equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue != ''";
										} 
										else {
											logic = "fieldvalue != configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
								}
								if (condOpts[j][0] == 'meets one'){
									if (count > 0){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}
								if (condOpts[j][0] == 'none'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}
								if (condOpts[j][0] == 'meets all'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).val("");
									}
								}								
	        			}
						if (DefaultValidation == 'hide options'){
							//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").css({"display": "none"});
							//$(constructor).closest("tr").hide("fast");
							$(constructor).attr('required', false);
								var fieldvalue = '';
								var logic = '';
								var count = 0;
								//console.log(condOpts.length);
								for (var v = 0; v < condOpts[j].length; v++){//Loop through sourcefields
									//console.log(compOpts[h][v]);
									//console.log(condOpts[h][v]);
									if (valueTypes[j][v] == 'text'){
										if (fieldElementTypeS[j][v] == 'select'){
											fieldvalue = $(sourceConstructors[j][v]).find("option:selected").text();
										} else {
											fieldvalue = $(sourceConstructors[j][v]).val();
										}
									}	
									if (valueTypes[j][v] == 'number' || valueTypes[j][v] == 'date'){
										fieldvalue = $(sourceConstructors[j][v]).val();
									}
									if (valueTypes[j][v] == 'checkbox' || valueTypes[j][v] == 'radio'){
										fieldvalue = $(sourceConstructors[j][v]).prop('checked');
									}
									if (compOpts[j][v] == 'equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue == ''";
										}
									    else {
											logic = "fieldvalue == configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
									if (compOpts[j][v] == 'not equal to'){
										var configvalue = configvalues[j][v];
										if (configvalue == 'blank'){
											logic = "fieldvalue != ''";
										} 
										else {
											logic = "fieldvalue != configvalue";
										}
										var result = eval(logic);
										//console.log(result);
										if (result == true){
											count++;
										}	
									}
								}
								if (condOpts[j][0] == 'meets one'){
									if (count > 0){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}
								if (condOpts[j][0] == 'none'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}
								if (condOpts[j][0] == 'meets all'){
									if (count == condOpts[j].length){
										//doing nothing
									} else {
										$(constructor).removeAttr('checked');
									}
								}								
	        			}
						if (DefaultValidation == 'show element required'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
						    $(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'show element'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
	        			}
	        			if (DefaultValidation == 'show field required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
							$(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'show field not required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
	        			}
	        			if (DefaultValidation == 'readonly'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (targetfieldtype == 'select') { 
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
	        					$(constructor+" option:not(:selected)").attr('disabled', true);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
	        					$(constructor+":not(:checked)").attr('disabled', true);
	        				} else {
	        					$(constructor).attr('readonly', true);
	        				}
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'editable'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (fieldElementTypeT == 'select') { 
	        					$(constructor).attr("style", "pointer-events: auto;");
	        					$(constructor).attr("tabindex", '');
	        					$(constructor+" option:not(:selected)").attr('disabled', false);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
		        				$(constructor).attr("style", "pointer-events: auto;");
		        				$(constructor).attr("tabindex", '');
		        				$(constructor+":not(:checked)").attr('disabled', false);
	        				} else {
	        					$(constructor).attr('readonly', false);
	        				}
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'disable'){
	        				//Disable fields based on configuration from Excel
	        				$(constructor).attr('disabled', true);
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'enable'){
	        				//Enable fields based on configuration from Excel
	        				$(constructor).attr('disabled', false);
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'show label'){
	        				if($(errormessage).length == 0) {
	        					var text = '<span id="error'+j+i+'" style="color:red">'+configvalue+'</span>';
	        					var type = $(constructor)[0].type;
		        				if (type == 'radio' || type == 'checkbox') {
		        					$(constructor).closest("td").prepend(text);
			       				} else {
			       					$(constructor).after(text);
			       				}
	        				}	
	        			}
	        			if (DefaultValidation == 'remove label'){
	        				$(errormessage).remove();
	        			}
	        			if (DefaultValidation == 'none'){
	        				//doing nothing here!
	        			}
	        			if (DefaultValidation == 'autofill'){
	        				if(targetfieldtype == 'select'){
	        					$(constructor+" option:contains("+configvalue+")").attr('selected', 'selected');
	        				} else { 
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).attr({"checked": true});
								} else {
									$(constructor).val(configvalue);
								}
	        				}
							//("select[name=studyDivision] option:contains('Cardiology')").attr('selected', 'selected');
	        			}
	        			/*if (DefaultValidation == 'remove autofill'){
							$(constructor).val("");
	        			}*/
	        			if (DefaultValidation == 'show layover'){
	        				if($(layover).length == 0) {
		        				var html = '<div id="alertMessage'+j+i+'" title="Alert">\
		                			<p id="alertText'+j+i+'" style="display:none;">'+ configvalue +'</p>\
		        				</div>';
		        				$("#isquare").prepend(html);
	        				}
	        				$("#alertText"+j+i).css("display", "block");
	        				jQuery("#alertMessage"+j+i).dialog();
	        			}
	        			if (DefaultValidation == 'highlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": configvalue+' 2px solid'});
	        					//$(constructor).css({ "outline": configvalue+' 2px solid'});
							} else {
								$(constructor).css({"border": configvalue+' 2px solid'});
							}
	        			}
	        			if (DefaultValidation == 'unhighlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({ "border": ""});
	        					//$(constructor).css({ "outline": ""});
							} else {
								$(constructor).css({ "border": ""});
							}
	        			}
	        			if (DefaultValidation == 'change background color'){
							$(constructor).css("background-color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove background color'){
							$(constructor).css("background-color", "");
	        			}//("select[name=studyDivision]").css('color', 'red');
	        			if (DefaultValidation == 'change font color'){
							$(constructor).css("color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font color'){
							$(constructor).css("color", "");
	        			}
	        			if (DefaultValidation == 'change font weight'){
							$(constructor).css("font-weight", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font weight'){
							$(constructor).css("font-weight", "");
	        			}
	        		}
        			//-->Hide
        			//$("input[name=txtSpeciality]").parent().parent().hide("fast");
        			//$("#enroll_links").find("tr td:nth-child(3)").hide("fast");
        			//$("#sectionHead2").hide("fast");        
				}
        	}
        },
        runDefaultValidationPerRule: function(thisModel, data, n, status){
        	//console.log(Data);
        	if (data){
					for (var i = 0; i < data.TargetFields.length; i++){
	        			//console.log('length: '+data.TargetFields.length);
	        			//console.log(excelData[j].TargetFields[i].DefaultValidation);
	        			//check default validation type
	        			var errormessage = '"span[id=error'+n+i+']"';
	        			var layover = '"div[id=alertMessage'+n+i+']"';
	        			var fieldElementTypeT = data.TargetFields[i].FieldElementTypeT;
						var fieldElementKeyT = data.TargetFields[i].FieldElementKeyT;
	        			var configvalue = data.TargetFields[i].ConfigValue;
	        			var targetfieldtype = data.TargetFields[i].FieldElementTypeT;
						if (fieldElementKeyT == 'advanced'){
							var constructor = ""+data.TargetFields[i].FieldElementTypeT+data.TargetFields[i].FieldElementValueT+"'";
						} else {
							var constructor = ""+data.TargetFields[i].FieldElementTypeT+"["+data.TargetFields[i].FieldElementKeyT+"="+data.TargetFields[i].FieldElementValueT+"]'";
						}
	        			var DefaultValidation = data.TargetFields[i].DefaultValidation;
	        			if (DefaultValidation == 'hide element'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).css({"display": "none"});
							//$(constructor).hide("fast");
							$(constructor).attr('required', false);
							var type = $(constructor)[0].type;
							if (type == 'radio' || type == 'checkbox') {
								$(constructor).removeAttr('checked');
							} else {
								if (fieldElementTypeT == 'input' || fieldElementTypeT == 'select' || fieldElementTypeT == 'textarea'){
									$(constructor).val("");
								}
							}
	        			}
	        			if (DefaultValidation == 'hide field'){
	        				//Hide field based on configuration from Excel
							$(constructor).closest("tr").css({"display": "none"});
	    					//$(constructor).closest("tr").hide("fast");
							$(constructor).val("");
							//$(constructor).attr('required', false);
	        			}
						if (DefaultValidation == 'hide options'){
	        				//Hide field based on configuration from Excel
							$(constructor).closest("tr").css({"display": "none"});
	    					//$(constructor).closest("tr").hide("fast");
							$(constructor).removeAttr('checked');
							//$(constructor).attr('required', false);
	        			}
	        			if (DefaultValidation == 'show element'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
	        			}
	        			if (DefaultValidation == 'show element required'){
	        				//Hide element based on configuration from Excel
	    					$(constructor).show("fast");
							$(constructor).attr('required', true);
	        			}
	        			if (DefaultValidation == 'show field required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
							$(constructor).attr('required', true);
	        			}
						if (DefaultValidation == 'show field not required'){
	        				//Hide field based on configuration from Excel
	    					$(constructor).closest("tr").show("fast");
	        			}
	        			if (DefaultValidation == 'readonly'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (fieldElementTypeT == 'select') { 
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
	        					$(constructor+" option:not(:selected)").attr('disabled', true);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).attr("style", "pointer-events: none;");
	        					$(constructor).attr("tabindex", -1);
	        					$(constructor+":not(:checked)").attr('disabled', true);
	        				} else {
	        					$(constructor).attr('readonly', true);
	        				}
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'editable'){
	        				//Make field readonly based on configuration from Excel
	        				var type = $(constructor)[0].type;
	        				if (fieldElementTypeT == 'select') { 
	        					$(constructor).attr("style", "pointer-events: auto;");
	        					$(constructor).attr("tabindex", '');
	        					$(constructor+" option:not(:selected)").attr('disabled', false);
	        				}
	        				if (type == 'radio' || type == 'checkbox') {
		        				$(constructor).attr("style", "pointer-events: auto;");
		        				$(constructor).attr("tabindex", '');
		        				$(constructor+":not(:checked)").attr('disabled', false);
	        				} else {
	        					$(constructor).attr('readonly', false);
	        				}
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'disable'){
	        				//Disable fields based on configuration from Excel
	        				$(constructor).attr('disabled', true);
	        				$(constructor).css("background-color", "lightgrey");
	        			}
	        			if (DefaultValidation == 'enable'){
	        				//Enable fields based on configuration from Excel
	        				$(constructor).attr('disabled', false);
	        				$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'show label'){
	        				//Enable fields based on configuration from Excel
	        				if($(errormessage).length == 0) {
	        					var text = '<span id="error'+n+i+'" style="color:red">'+configvalue+'</span>';
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).closest("td").prepend(text);
								} else {
									$(constructor).after(text);
								}
	        				}
	        			}
	        			if (DefaultValidation == 'remove label'){
	        				$(errormessage).remove();
	        			}
	        			if (DefaultValidation == 'none'){
	        				//doing nothing here!
	        			}
	        			if (DefaultValidation == 'autofill'){
	        				if(targetfieldtype == 'select'){
	        					$(constructor+" option:contains("+configvalue+")").attr('selected', 'selected');
	        				} else { 
	        					var type = $(constructor)[0].type;
								if (type == 'radio' || type == 'checkbox') {
									$(constructor).attr({"checked": true});
								} else {
									$(constructor).val(configvalue);
								}
	        				}
	        			}
	        			/*if (DefaultValidation == 'remove autofill'){
							$(constructor).val("");
	        			}*/
	        			if (DefaultValidation == 'show layover'){
	        				if($(layover).length == 0) {
	        					var html = '<div id="alertMessage'+j+i+'" title="Alert">\
		                			<p id="alertText'+j+i+'" style="display:none;">'+ configvalue +'</p>\
		        				</div>';
		        				$("#isquare").prepend(html);
	        				}
	        				$("#alertText"+j+i).css("display", "block");
	                		jQuery("#alertMessage"+j+i).dialog();
	        			}
	        			if (DefaultValidation == 'highlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": configvalue+' 2px solid'});
	        					//$(constructor).css({ "outline": configvalue+' 2px solid'});
							} else {
								$(constructor).css({ "border": configvalue+' 2px solid'});
							}
	        			}
	        			if (DefaultValidation == 'unhighlight field'){
	        				var type = $(constructor)[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructor).closest("td").css({"border": ""});
	        					//$(constructor).css({ "outline": ""});
							} else {
								$(constructor).css({ "border": ""});
							}
	        			}
	        			if (DefaultValidation == 'change background color'){
							$(constructor).css("background-color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove background color'){
							$(constructor).css("background-color", "");
	        			}
	        			if (DefaultValidation == 'change font color'){
							$(constructor).css("color", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font color'){
							$(constructor).css("color", "");
	        			}
	        			if (DefaultValidation == 'change font weight'){
							$(constructor).css("font-weight", configvalue);
	        			}
	        			if (DefaultValidation == 'remove font weight'){
							$(constructor).css("font-weight", "");
	        			}
	        		}       
        	}
        },
        readSourceField: function(thisModel, excelData, status){
        	var runFieldValidation = function (n){
				for (var m = 0; m < desiredValidation[n].length; m++){
   					var errormessage = '"span[id=error'+n+m+']"';
   					var layover = '"div[id=alertMessage'+n+m+']"';
   					if (desiredValidation[n][m] == 'hide element'){
        				//Hide element based on configuration from Excel
	    				$(constructors[n][m]).css({"display": "none"});
						//$(constructors[n][m]).hide("fast");
						$(constructors[n][m]).attr('required', false);
						var type = $(constructors[n][m])[0].type;
						if (type == 'radio' || type == 'checkbox') {
							$(constructors[n][m]).removeAttr('checked');
						} else {
							if (fieldElementTypeT[n][m] == 'input' || fieldElementTypeT[n][m] == 'select' || fieldElementTypeT[n][m] == 'textarea'){
								$(constructors[n][m]).val("");
							}
						}
	        		}
	        		if (desiredValidation[n][m] == 'hide field'){
	        			//Hide field based on configuration from Excel
	    				$(constructors[n][m]).closest("tr").css({"display": "none"});
						//$(constructors[n][m]).closest("tr").hide("fast");
						$(constructors[n][m]).val("");
						//$(constructors[n][m]).attr('required', false);
	        		}
					if (desiredValidation[n][m] == 'hide options'){
	        			//Hide field based on configuration from Excel
						$(constructors[n][m]).closest("tr").css({"display": "none"});
						//$(constructors[n][m]).closest("tr").hide("fast");
						//$(constructors[n][m]).attr('required', false);
						$(constructors[n][m]).removeAttr('checked');
	        		}
	        		if (desiredValidation[n][m] == 'show element'){
	        			//Hide element based on configuration from Excel
	    				$(constructors[n][m]).show("fast");
	        		}
	        		if (desiredValidation[n][m] == 'show element required'){
	        			//Hide element based on configuration from Excel
	    				$(constructors[n][m]).show("fast");
						$(constructors[n][m]).attr('required', true);
	        		}
	        		if (desiredValidation[n][m] == 'show field required'){
	        			//Hide field based on configuration from Excel
	    				$(constructors[n][m]).closest("tr").show("fast");
						$(constructors[n][m]).attr('required', true);
	       			}
					if (desiredValidation[n][m] == 'show field not required'){
	        			//Hide field based on configuration from Excel
	    				$(constructors[n][m]).closest("tr").show("fast");
	       			}
	       			if (desiredValidation[n][m] == 'readonly'){
	       				//Make field readonly based on configuration from Excel
	       				var type = $(constructors[n][m])[0].type;
	       				if (fieldElementTypeT[n][m] == 'select') { 
        					$(constructors[n][m]).attr("style", "pointer-events: none;");
        					$(constructors[n][m]).attr("tabindex", -1);	
        					$(constructors[n][m]+" option:not(:selected)").attr('disabled', true);
            			}
        				if (type == 'radio' || type == 'checkbox') { 
            				$(constructors[n][m]).attr("style", "pointer-events: none;");
            				$(constructors[n][m]).attr("tabindex", -1);	
            				$(constructors[n][m]+":not(:checked)").attr('disabled', true);
	       				} else {
        					$(constructors[n][m]).attr('readonly', true);
        				}
        				$(constructors[n][m]).css("background-color", "lightgrey");
	        		}
	        		if (desiredValidation[n][m] == 'editable'){
	        			//Make field readonly based on configuration from Excel
	        			var type = $(constructors[n][m])[0].type;
	        			if (fieldElementTypeT[n][m] == 'select') { 
        					$(constructors[n][m]).attr("style", "pointer-events: auto;");
        					$(constructors[n][m]).attr("tabindex", '');	
        					$(constructors[n][m]+" option:not(:selected)").attr('disabled', false);
            			}
        				if (type == 'radio' || type == 'checkbox') { 
            				$(constructors[n][m]).attr("style", "pointer-events: auto;");
            				$(constructors[n][m]).attr("tabindex", '');	
            				$(constructors[n][m]+":not(:checked)").attr('disabled', false);
	       				} else {
        					$(constructors[n][m]).attr('readonly', false);
        				}
	        			$(constructors[n][m]).css("background-color", "");
	        		}
	        		if (desiredValidation[n][m] == 'disable'){
	        			//Disable fields based on configuration from Excel
	        			$(constructors[n][m]).attr('disabled', true);
	        			$(constructors[n][m]).css("background-color", "lightgrey");
	        		}
	        		if (desiredValidation[n][m] == 'enable'){
	       				//Enable fields based on configuration from Excel
	       				$(constructors[n][m]).attr('disabled', false);
	       				$(constructors[n][m]).css("background-color", "");
	       			}
	        		if (desiredValidation[n][m] == 'show label'){
	       				//Enable fields based on configuration from Excel
	        			if($(errormessage).length == 0) {
	        				var text = '<span id="error'+n+m+'" style="color:red">'+configvalueTargets[n][m]+'</span>';
	        				var type = $(constructors[n][m])[0].type;
	        				if (type == 'radio' || type == 'checkbox') {
	        					$(constructors[n][m]).closest("td").prepend(text);
	        				} else {
	        					$(constructors[n][m]).after(text);
	        				}
	        			}
	       			}
	        		if (desiredValidation[n][m] == 'remove label'){
	       				//Enable fields based on configuration from Excel
	       				$(errormessage).remove();
	       			}
	        		if (desiredValidation[n][m] == 'none'){
        				//doing nothing here!
        			}
	        		if (desiredValidation[n][m] == 'autofill'){
						//$(constructors[n][m]).val(configvalueTargets[n][m]);
	        			if(targetfieldtype[n][m] == 'select'){
							$(constructors[n][m]+" option:contains("+configvalueTargets[n][m]+")").attr('selected', 'selected');
							//alert('hi');
	        			} else {
							var type = $(constructors[n][m])[0].type;
							if (type == 'radio' || type == 'checkbox') {
								$(constructors[n][m]).attr({"checked": true});
							} else {
								$(constructors[n][m]).val(configvalueTargets[n][m]);
							}
						}
	        		}
	        		/*if (desiredValidation[n][m] == 'remove autofill'){
						$(constructors[n][m]).val("");
	        		}*/
	        		if (desiredValidation[n][m] == 'show layover'){
	        			if($(layover).length == 0) {
	        				var html = '<div id="alertMessage'+n+m+'" title="Alert">\
                				<p id="alertText'+n+m+'" style="display:none;">'+ configvalueTargets[n][m] +'</p>\
                			</div>';
	        				$("#isquare").prepend(html);
	        			}
	        			$("#alertText"+n+m).css("display", "block");
        				jQuery("#alertMessage"+n+m).dialog();
        			}
	        		if (desiredValidation[n][m] == 'highlight field'){
	        			var type = $(constructors[n][m])[0].type;
	        			if (type == 'radio' || type == 'checkbox') {
	        				$(constructors[n][m]).closest("td").css({"border": configvalueTargets[n][m]+' 2px solid'});
	        				//$(constructors[n][m]).css({ "outline": configvalueTargets[n][m]+' 2px solid'});
						} else {
							$(constructors[n][m]).css({ "border": configvalueTargets[n][m]+' 2px solid'});
						}
        			}
        			if (desiredValidation[n][m] == 'unhighlight field'){
        				var type = $(constructors[n][m])[0].type;
        				if (type == 'radio' || type == 'checkbox') {
        					$(constructors[n][m]).closest("td").css({ "border": ""});
        					//$(constructors[n][m]).css({ "outline": ""});
						} else {
							$(constructors[n][m]).css({ "border": ""});
						}	
        			}
        			if (desiredValidation[n][m] == 'change background color'){
						$(constructors[n][m]).css("background-color", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove background color'){
						$(constructors[n][m]).css("background-color", "");
        			}
        			if (desiredValidation[n][m] == 'change font color'){
						$(constructors[n][m]).css("color", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove font color'){
						$(constructors[n][m]).css("color", "");
        			}
        			if (desiredValidation[n][m] == 'change font weight'){
						$(constructors[n][m]).css("font-weight", configvalueTargets[n][m]);
        			}
        			if (desiredValidation[n][m] == 'remove font weight'){
						$(constructors[n][m]).css("font-weight", "");
        			}
				}
			};
        	var runComparisonOpt = function (n,i){
        		var fieldvalue = '';
				//Logic to check field value type
        		/*if (valueTypes[n][i] == 'text'){
					fieldvalue = $(sourceConstructors[n][i]).find("option:selected").text();
					//alert(sourceConstructors[n][i]);
				}
				if (valueTypes[n][i] == 'number'){
					fieldvalue = $(sourceConstructors[n][i]).val();
					//alert(sourceConstructors[n][i]);
				}*/
				var logic = '';
				var count = 0;
				//console.log(condOpts.length);
				for (var v = 0; v < condOpts[n].length; v++){//Loop through sourcefields
					//console.log(compOpts[h][v]);
					//console.log(condOpts[h][v]);
					if (valueTypes[n][v] == 'text'){
						if (fieldElementTypeS[n][v] == 'select'){
							fieldvalue = $(sourceConstructors[n][v]).find("option:selected").text();
						} else {
							fieldvalue = $(sourceConstructors[n][v]).val();
						}	
					}
					if (valueTypes[n][v] == 'number' || valueTypes[n][v] == 'date'){
						fieldvalue = $(sourceConstructors[n][v]).val();
					}
					if (valueTypes[n][v] == 'checkbox' || valueTypes[n][v] == 'radio'){
						fieldvalue = $(sourceConstructors[n][v]).prop('checked');
					}
					//console.log(fieldvalue);
					if (compOpts[n][v] == 'equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue == ''";
							//console.log(logic);
						}
						else {
							logic = "fieldvalue == configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'not equal to'){
						var configvalue = configvalues[n][v];
						if (configvalue == 'blank'){
							logic = "fieldvalue != ''";
						}
						else {
							logic = "fieldvalue != configvalue";
						}
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue > configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue < configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'greater than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue >= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
					if (compOpts[n][v] == 'less than or equal to'){
						var configvalue = configvalues[n][v];
						logic = "fieldvalue <= configvalue";
						var result = eval(logic);
						//console.log(result);
						if (result == true){
							count++;
						}	
					}
				}
				//console.log(count);
				if (condOpts[n][0] == 'meets one'){
					if (count > 0){
						runFieldValidation(n);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, status);
					}
				}
				if (condOpts[n][0] == 'none'){
					if (count == condOpts[n].length){
						runFieldValidation(n);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, status);
					}
				}
				if (condOpts[n][0] == 'meets all'){
					if (count == condOpts[n].length){
						runFieldValidation(n);
					} else {
						var data = excelData[n];
						thisModel.runDefaultValidationPerRule(thisModel, data, n, status);
					}
				}	
				//missing date here
        		//console.log("fieldvalue"+fieldvalue);
				//console.log("configvalue"+configvalues[n]);
				//console.log("condOpt"+condOpts[n][i]);
				//console.log("compOpt"+compOpts[n][i]);				
        	};
        	var checkandValidateTargetField = function (n,i){
        		$(document).ready(function(){
        			runComparisonOpt(n,i);
        		});
        		if (valueTypes[n][i] == 'radio'){
					var sourceConstructorForRadio = ""+excelData[n].SourceFields[i].FieldElementTypeS+":"+valueTypes[n][i]+"'";
					$(sourceConstructorForRadio).on("change click", function(){
					runComparisonOpt(n,i);
					});
				}
				if (valueTypes[n][i] == 'checkbox'){
					var sourceConstructorForCheckbox = ""+excelData[n].SourceFields[i].FieldElementTypeS+":"+valueTypes[n][i]+"'";
					$(sourceConstructorForCheckbox).on("change click", function(){
					runComparisonOpt(n,i);
					});
				} else {
					$(sourceConstructors[n][i]).on("change keyup paste", function(){
						runComparisonOpt(n,i);
					});
					//Enable below function if use in eResearch
					$("a[href=#]").on("focus", function(){
						runComparisonOpt(n,i);
					});
				}
        	};
        	var configvalues = [], valueTypes = [], condOpts = [], compOpts = [], fieldElementTypeS = [], sourceConstructors = [], sourcevalueConstructors = [];
        	for (var j = 0; j < excelData.length; j++){//Loop for rules
        		//console.log("rule no."+j);
        		sourceConstructors[j] = []; sourcevalueConstructors[j] = [];configvalues[j] = []; valueTypes[j] = []; condOpts[j] = []; compOpts[j] = []; fieldElementTypeS[j] = []; 
        		for (var i = 0; i < excelData[j].SourceFields.length; i++){//Loop for source fields
        			if (excelData[j].SourceFields[i].FieldElementKeyS == 'advanced'){
						sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+excelData[j].SourceFields[i].FieldElementValueS+"'";
					} else {
						sourceConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+"]'";
					}
        			configvalues[j][i] = excelData[j].SourceFields[i].FieldValueS;
					valueTypes[j][i] = excelData[j].SourceFields[i].ValueType;
					condOpts[j][i] = excelData[j].SourceFields[i].CondOpt;
					compOpts[j][i] = excelData[j].SourceFields[i].CompOpt;
					fieldElementTypeS[j][i] = excelData[j].SourceFields[i].FieldElementTypeS;
					//sourcevalueConstructors[j][i] = ""+excelData[j].SourceFields[i].FieldElementTypeS+"["+excelData[j].SourceFields[i].FieldElementKeyS+"="+excelData[j].SourceFields[i].FieldElementValueS+" ] option:selected'";		
        		}
        	}
        	var desiredValidation = [], constructors = [], configvalueTargets = [], targetfieldtype = [], fieldElementTypeT = [], fieldElementKeyT = [];
        	//console.log(condOpts);
        	//console.log(compOpts);
        	for (var n = 0; n < excelData.length; n++){
        		desiredValidation[n] = []; constructors[n] = []; configvalueTargets[n] = [], targetfieldtype[n] = [], fieldElementTypeT[n] = [], fieldElementKeyT[n] = [];
				for (var k = 0; k < excelData[n].TargetFields.length; k++){//Loop for target field
					desiredValidation[n][k] = excelData[n].TargetFields[k].DesiredValidation;
					fieldElementTypeT[n][k] = excelData[n].TargetFields[k].FieldElementTypeT;
					fieldElementKeyT[n][k] = excelData[n].TargetFields[k].FieldElementKeyT;
					if (fieldElementKeyT[n][k] == 'advanced'){
						constructors[n][k] = ""+excelData[n].TargetFields[k].FieldElementTypeT+excelData[n].TargetFields[k].FieldElementValueT+"'";
					} else {
						constructors[n][k] = ""+excelData[n].TargetFields[k].FieldElementTypeT+"["+excelData[n].TargetFields[k].FieldElementKeyT+"="+excelData[n].TargetFields[k].FieldElementValueT+"]";
					}
					configvalueTargets[n][k] = excelData[n].TargetFields[k].ConfigValue;
					targetfieldtype[n][k] = excelData[n].TargetFields[k].FieldElementTypeT;
				}	
				for (var i = 0; i < excelData[n].SourceFields.length; i++){
					//console.log(compOpts[n][i]);
					checkandValidateTargetField(n,i);
				}
			}
        }
    });
    return FieldValidation_Model;
});
