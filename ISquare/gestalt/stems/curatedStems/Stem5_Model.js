 /* Determines default group of user */
define([ 
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/UserWSClientModel'
], function(global, $, _, Backbone, UserWSClientModel) {
	var stemConfig ='';
	var Stem5_Model = Backbone.Model.extend({
		name: "Stem5_Model",
        initialize: function(obj){
        	//alert(global.params.loggedInUser + global.mainTarget);
        	
        },
        do: function(args){
        	var stemModel = args.stemModel, studyPK = global.params.studyPK; 
        	stemConfig = stemModel.toJSON();
        	var userWSClientModel = new UserWSClientModel();
        	userWSClientModel.searchUser(stemModel, 
        		{
        			studyPK: studyPK,
        			fname:'',
        			lname:'',
        			userPK: global.loggedInUser,
        			login:''
        		}, stemModel.next);
        }, 
        next: function(thisModel, xmlUserData, status){
			//console.log('Logging '+xmlUserData.responseXML);
		    var valSuccess = false;

			 $(xmlUserData.responseXML)
			    .find('UserSearchResults')
			    .find('user')
			    .each(function()
    		 {
			    	var memberPK = $(this).find('PK').text();
				    var defGroupName = $(this).find('userDefaultGroup').find('groupName').text();
				    var defGroupPK = $(this).find('userDefaultGroup').find('PK').text();
				    //alert(global.loggedInUser  + ' ' +memberPK);
				    /*if (global.loggedInUser == memberPK){*/

					   if (!document.getElementById('isqLogUsrDefGroup')){
	        			   var JSONObj = {
	        					   groupName : defGroupName, 
	        					   groupPK : defGroupPK
	        			   };
		        		   //alert('Field created!');
		        		   //alert(JSONObj.groupName);
		        		   var $newInput = $( "<input type='hidden' id='isqLogUsrDefGroup' value='"+JSON.stringify(JSONObj)+"'/>");
		        		   $( "body" ).append( $newInput );
	        		   } 
					   else {
	        			   $('#isqLogUsrDefGroup').val(defGroupName);
	        		   }		
		            //}
    		 });
        }
	});
    return Stem5_Model;
});
