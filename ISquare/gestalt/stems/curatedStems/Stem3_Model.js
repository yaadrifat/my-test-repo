define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/StudyWSClientModel'
], function(global, $, _, Backbone, StudyWSClientModel) {
	var stemConfig ='';
	var Stem3_Model = Backbone.Model.extend({
        initialize: function(obj){
        	//alert(global.loggedInUser + global.mainTarget);
        },
        do: function(args){
        	var stemModel = args.stemModel, studyPK = global.params.studyPK; 
        	stemConfig = stemModel.toJSON();

        	var studyWSClientModel = new StudyWSClientModel();
        	studyWSClientModel.getStudyTeamMembers(stemModel, {studyPK: studyPK}, stemModel.get('next'));
        },
        next: function(thisModel, xmlStudyData, status){
			console.log('Logging '+xmlStudyData.responseXML);
		    var valSuccess = false;

			 $(xmlStudyData.responseXML)
			    .find('StudyTeamMembers')
			    .find('studyTeamMember')
			    .each(function()
    		 {
			    var memberPK = $(this).find('userId').find("PK").text();
			    var role = $(this).find('teamRole').find('code').text();

			    //alert(global.loggedInUser + ' ' +memberPK);
			    if (global.loggedInUser == memberPK){
				   /*var fname = $(this).find('userId').find("firstName").text();
				   var lname = $(this).find('userId').find("lastName").text();

				   console.log(fname+' '+lname);*/
				   console.log(global.mainTarget+ ' ' + role);

	               switch(global.mainTarget){
		        		case 'patSchedule' : 
		        	    default:
		        		   if (!document.getElementById('isqLogUsrRole')){
			        		   var $newInput = $( "<input type='hidden' id='isqLogUsrRole' value='"+role+"'/>" );
			        		   $( "body" ).append( $newInput );        			   
		        		   } else {
		        			   $('#isqLogUsrRole').val(role);
		        		   }
			               return false;
			               break;
	               	}
			    }
    		});
        }
	});
    return Stem3_Model;
});
