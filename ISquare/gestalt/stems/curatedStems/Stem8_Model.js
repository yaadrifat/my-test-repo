define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/StudyWSClientModel',
    '../WSClient/FormResponseWSClientModel',
    '../../util/email/EmailModel'
], function(global, $, _, Backbone, StudyWSClientModel, FormResponseWSClientModel, EmailModel) {
    var Stem8_Model = Backbone.Model.extend({
    	that: {},
    	studyPK: 0,
    	formPK: 0,
    	formResponsePK: 0,
    	studyNumber: "",
    	studyTitle: "",
    	pi: "",
    	uiMessage: '',
        initialize: function(){
        	//that = stemModel;
            //alert('Stem8_Model initialized!');
        },
        do: function(args){
        	var stemModel = args.stemModel, studyPK = global.params.studyPK; 
        	stemModel.studyPK = studyPK;
        	var studyWSClientModel = new StudyWSClientModel();
        	studyWSClientModel.getStudySummary(stemModel, {studyPK: studyPK}, stemModel.process);
        },
        process: function(thisModel, xmlStudyData, status){
        	thisModel.findStudySpecifics(thisModel, xmlStudyData, status);
        	thisModel.processMSD(thisModel, xmlStudyData, status);
        },
        findStudySpecifics: function(thisModel, xmlStudyData, status){
		    var studyNumber = $(xmlStudyData.responseXML).find('StudySummary').find('parentIdentifier').find('studyNumber').text();
		    thisModel.studyNumber = studyNumber;
		    
		    var studyTitle = $(xmlStudyData.responseXML).find('StudySummary').find('studyTitle').text();
		    thisModel.studyTitle = studyTitle;
		    
		    var pi = $(xmlStudyData.responseXML).find('StudySummary').find('principalInvestigator');
        	var fname = pi.find("firstName").text(), lname = pi.find("lastName").text();
		    thisModel.pi = fname + " " + lname;
        },
        processMSD: function(thisModel, xmlStudyData, status){
        	var spnsrdProt;
			
			$(xmlStudyData.responseXML)
			    .find('StudySummary').find('moreStudyDetails').each(function()
			{
			   var key = $(this).find('key').text();
			   if (key == 'spnsrdProt'){
				   spnsrdProt = $(this).find('value').text();		   
				   if (spnsrdProt != 'MDACCInvIni' && spnsrdProt != 'indstrStdy' ){
					   return false;
				   }
			   }  
			});
			if (spnsrdProt != 'MDACCInvIni' && spnsrdProt != 'indstrStdy'){
			    return false;
			}	
			var formPK = global.params.formPK;
			var formPKConfig = thisModel.get('settings').PI_Act_Req.piActFormPK;
			if (formPK == formPKConfig){
				var formResponseWSClientModel = new FormResponseWSClientModel();
		        formResponseWSClientModel.getStudyFormResponse(thisModel, {
		        	formResponsePK: global.params.formResponsePK
		        }, thisModel.checkPiActSignOff); 
			}
	    },
	    checkPiActSignOff: function(thisModel, xmlFormResponseData, status){
	    	$(xmlFormResponseData.responseXML)
	    	.find('StudyFormResponse').each(function(){
	    		var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
	   			if (formStatusCodeSubType == 'complete'){
	   				var formResponseWSClientModel = new FormResponseWSClientModel();
	   			 	formResponseWSClientModel.getListofStudyFormResponses(thisModel, 
	   			 	{
		   			 	studyPK: global.params.studyPK,
		   			 	formPK: (thisModel.get('settings')).ProtActivation.protActFormPK
	   			 	}, thisModel.checkProtActSignOff);
	   			}
			});
	    },
	    checkProtActSignOff: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		//console.log(xmlFormResponseData.responseXML);
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}      		
		    	if (sendEmail){
		    		thisModel.sendEmailToOPR(thisModel);
		    	}
        	}
	    },    	
	    sendEmailToOPR: function(thisModel){
	    	var emailSettings = (thisModel.get("settings"))["ProtActivation"]; 
	    	if (!emailSettings || !emailSettings.recipients) return;
	    	var subject = emailSettings["subject"]; //alert('OPR'+subject);
	    	var text = emailSettings["text"];
	    	var emailParams = {
	    		subjectParams: [thisModel.studyNumber],	
	    		textParams : [thisModel.pi, thisModel.studyNumber, thisModel.studyTitle,''+thisModel.studyPK]
	    	};
	    	
	    	var emailObj = {
				recipients:emailSettings.recipients, 
				subject: subject, 
				text: text,
				emailParams: emailParams,
				successMessage: "Email sent successfully to OPR",
				errorMessage: "Email Error - OPR"
			};console.log('Sending email to OPR');
			thisModel.sendEmailWithSubstitution(emailObj);
	    },
	    sendEmailWithSubstitution: function(emailObj){
	    	if (!emailObj) return;
	    	
	    	var emailModel = new EmailModel(emailObj);
			emailModel.substituteAndSendEmail();
	    },	   
    });
    return Stem8_Model;
});
