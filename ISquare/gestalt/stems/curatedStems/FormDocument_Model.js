define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/TaylorWSClientModel'
], function(global, $, _, Backbone, TaylorWSClientModel) {
	var stemConfig ='';
    var FormDocument_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	//alert(" isquare running...");
        	var subParams = [stemModel.get('taylorUrl'), global.params.formResponsePK];
    		var params = {"serviceParams":subParams};
    		var taylorWSClientModel = new TaylorWSClientModel();
 		    taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.checkDocumentExistence); 		    	    
        	var html = '<div class="custom-file-upload">\
        		<form name="multiform" id="multiform" enctype="multipart/form-data">\
	        		<div style="width:450px;height:30px;margin-left:13px;margin-top:-10px;">\
	        			<div style="float:left;width:82px;height:25px;overflow:hidden">\
							<input id="upload" type="file" name="filename" value="Attach documents"/>\
	        			</div>\
	        			<div id="fileDetails" style="font-size:120%; color:black; float:left; margin-left:7px; width:195px;height:30px;">\
							<span>no file selected</span>\
	        			</div>\
	        			<div style="float:right;">\
							<a href"#" onclick=pop(popDiv); id="link" class="disabled">View Files</a>\
	        			</div>\
        				<div style="float:right;margin-right:10px">\
							<input type="submit" id="smtbtn" value="Upload" disabled/>\
        				</div>\
		        		<input type="hidden" id="createdUser" class="docid" name="createdUser" value=""/>\
		        		<input type="hidden" id="formId" class="docid" name="formId" value=""/>\
		        		<input type="hidden" id="personId" class="docid"  name="personId" value=""/>\
		        		<input type="hidden" id="studyID" class="docid" name="studyID" value=""/>\
		        		<input type="hidden" id="taylorURL" class="docid" name="taylorURL" value=""/>\
	        		</div>\
            	</form>\
        </div>\
        		<div id="viewerMain" title="Basic dialog">\
                <embed id="viewer" width="800" height="600" type="" style="margin-left:110px; margin-right: auto;"></embed>\
        		</div>\
        		<div id="uploadProgressBar" title="Upload Progress">\
        			<div id="progressbox">\
        				<div id="progressbar"></div>\
        				<div id="statustxt">0%</div>\
        		    </div>\
        		    <div id="output" style="display:none; color:black; margin-left:8px;"><b>Upload Status:</b></div>\
        		</div>\
        		<div id="deleteConfirm" title="Delete Confirmation">\
      		  		<p id="deleteMsg" style="display:none;">Are you sure you want to delete?</p>\
        		</div>\
        		<div id="deleteStatus" title="Delete Confirmation">\
        		  <p id="deleteSuccess" style="display:none;">Document was deleted successfully.</p>\
        		  <p id="deleteFail" style="display:none;">An error occured while deleting the file.</p>\
        		</div>\
        		<div id="fileList" title="Form Documents">\
        		<div class="container-main" id="popDiv">\
                <div class="container-outer">\
                    <div class="container-inner">\
                        <table id="dsTable" cellspacing="0">\
                                <tr class="top">\
                                    <td class="first-inner-1">\
            		                    <div class="dsTable-row-ht">\
                                        <p style="font-size:140%; padding-left: 15px"><b>List of all attached documents</b></p>\
            				            </div>\
                                    </td>\
            		<td class="sec">\
            		</td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo" style="font-size:120%; padding-left: 15px"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstm second-inner" id="show-1" value="View"/>\
            		</td>\
                                    <td class="third-inner">\
            							<input type="button" id="innerDel" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo1" style="font-size:120%; padding-left: 15px"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstm second-inner" id="show-2" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-2" value="x" style="margin-right: 15px;" class="btnDelete"  onchange="show()"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo2" style="font-size:120%; padding-left: 15px"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstm second-inner" id="show-3" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-3" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo3" style="font-size:120%; padding-left: 15px"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstm second-inner" id="show-4" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-4" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo4" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-5" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-5" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo5" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-6" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-6" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo6" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-7" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-7" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo7" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-8" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-8" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo8" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-9" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-9" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo9" style="font-size:120%; padding-left: 15px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstm second-inner" id="show-10" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-10" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
                        </table>\
                    </div>\
                </div>\
            </div>\
        		</div>';
        	var htmlf1 = '<div class="custom-file-upload">\
        		<form name="multiform" id="multiformf1" enctype="multipart/form-data">\
	        		<div style="width:450px;height:30px;margin-top:0px;padding-bottom:0px;">\
	    				<div style="float:left;width:82px;height:25px;overflow:hidden">\
							<input id="uploadf1" type="file" name="filename" value="Attach documents"/>\
	    				</div>\
	    				<div id="fileDetailsf1" style="font-size:120%; color:black; float:left; margin-left:7px; width:195px;height:30px;">\
							<span>no file selected</span>\
	    				</div>\
	    				<div style="float:right;">\
							<a href"#" onclick=pop(popDivf1); id="linkf1" class="disabled">View files</a>\
	    				</div>\
						<div style="float:right;margin-right:10px">\
							<input type="submit" id="smtbtnf1" value="Upload" disabled/>\
						</div>\
		        		<input type="hidden" id="createdUserf1" class="docid" name="createdUser" value=""/>\
		        		<input type="hidden" id="formIdf1" class="docid" name="formId" value=""/>\
		        		<input type="hidden" id="personIdf1" class="docid"  name="personId" value=""/>\
		        		<input type="hidden" id="studyIDf1" class="docid" name="studyID" value=""/>\
		        		<input type="hidden" id="fieldIdf1" class="docid" name="fieldId" value=""/>\
		        		<input type="hidden" id="taylorURLf1" class="docid" name="taylorURL" value=""/>\
	        		</div>\
        		</form>\
            </div>\
        		<div id="viewerMainf1" title="Basic dialog">\
                	<iframe id="viewerf1" frameborder="0" scrolling="auto" width="800" height="600" style="margin-left:110px; margin-right: auto;"></iframe>\
        		</div>\
        		<div id="uploadProgressBarf1" title="Upload Progress">\
    			<div id="progressboxf1">\
    				<div id="progressbarf1"></div>\
    				<div id="statustxtf1">0%</div>\
    		    </div>\
    		    <div id="outputf1" style="display:none; color:black; margin-left:8px;"><b>Upload Status:</b></div>\
    		<div id="deleteConfirmf1" title="Delete Confirmation">\
  		  		<p id="deleteMsgf1" style="display:none;">Are you sure you want to delete?</p>\
    		</div>\
    		<div id="deleteStatusf1" title="Delete Confirmation">\
    		  <p id="deleteSuccessf1" style="display:none;">Document was deleted successfully.</p>\
    		  <p id="deleteFailf1" style="display:none;">An error occured while deleting the file.</p>\
    		</div>\
    		<div id="fileListf1" title="Field Documents">\
        		<div class="container-main" id="popDivf1">\
                <div class="container-outer">\
                    <div class="container-inner">\
                        <table id="dsTablef1" cellspacing="0">\
                                <tr class="top">\
                                    <td class="first-inner-1">\
            		                    <div class="dsTable-row-ht">\
                                        <p style="font-size:140%; padding-left: 10px;"><b>List of all attached documents</b></p>\
            				            </div>\
                                    </td>\
            		<td class="sec">\
            		</td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demof1" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf1 second-inner" id="show-1f1" value="View"/>\
            		</td>\
                                    <td class="third-inner">\
            							<input type="button" id="innerDelf1" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo1f1" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf1 second-inner" id="show-2f1" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-2f1" value="x" style="margin-right: 7px;" class="btnDelete"  onchange="showr()"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo2f1" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf1 second-inner" id="show-3f1" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-3f1" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo3f1" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf1 second-inner" id="show-4f1" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-4f1" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo4f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-5f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-5f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo5f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-6f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-6f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo6f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-7f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-7f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo7f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-8f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-8f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo8f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-9f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-9f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo9f1" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf1 second-inner" id="show-10f1" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-10f1" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
                        </table>\
                    </div>\
                </div>\
            </div>';
        	var htmlf2 = '<div class="custom-file-upload">\
        		<form name="multiform" id="multiformf2" enctype="multipart/form-data">\
	        		<div style="width:450px;height:30px;margin-top:-10px;padding-bottom:5px;">\
	    				<div style="float:left;width:102px;height:50px;overflow:hidden">\
							<input id="uploadf2" type="file" name="filename" value="Attach documents"/>\
	    				</div>\
	    				<div id="fileDetailsf2" style="font-size:120%; color:black; float:left; margin-left:7px; width:150px;height:30px;overflow:hidden;">\
							<p>no file selected</p>\
	    				</div>\
	    				<div style="float:right;">\
							<a href"#" onclick=pop(popDivf2); id="linkf2" class="disabled">View files</a>\
	    				</div>\
						<div style="float:right;margin-right:10px">\
							<input type="submit" id="smtbtnf2" value="Upload" disabled/>\
						</div>\
		        		<input type="hidden" id="createdUserf2" class="docid" name="createdUser" value=""/>\
		        		<input type="hidden" id="formIdf2" class="docid" name="formId" value=""/>\
		        		<input type="hidden" id="personIdf2" class="docid"  name="personId" value=""/>\
		        		<input type="hidden" id="studyIDf2" class="docid" name="studyID" value=""/>\
		        		<input type="hidden" id="fieldIdf2" class="docid" name="fieldId" value=""/>\
		        		<input type="hidden" id="taylorURLf2" class="docid" name="taylorURL" value=""/>\
	        		</div>\
        		</form>\
            </div>\
        		<div id="viewerMainf2" title="Basic dialog">\
                	<iframe id="viewerf2" frameborder="0" scrolling="auto" width="800" height="600" style="margin-left:110px; margin-right: auto;"></iframe>\
        		</div>\
        		<div id="uploadProgressBarf2" title="Upload Progress">\
    			<div id="progressboxf2">\
    				<div id="progressbarf2"></div>\
    				<div id="statustxtf2">0%</div>\
    		    </div>\
    		    <div id="outputf2" style="display:none; color:black; margin-left:8px;"><b>Upload Status:</b></div>\
    		<div id="deleteConfirmf2" title="Delete Confirmation">\
  		  		<p id="deleteMsgf2" style="display:none;">Are you sure you want to delete?</p>\
    		</div>\
    		<div id="deleteStatusf2" title="Delete Confirmation">\
    		  <p id="deleteSuccessf2" style="display:none;">Document was deleted successfully.</p>\
    		  <p id="deleteFailf2" style="display:none;">An error occured while deleting the file.</p>\
    		</div>\
    		<div id="fileListf2" title="Field Document">\
        		<div class="container-main" id="popDivf2">\
                <div class="container-outer">\
                    <div class="container-inner">\
                        <table id="dsTablef2" cellspacing="0">\
                                <tr class="top">\
                                    <td class="first-inner-1">\
            		                    <div class="dsTable-row-ht">\
                                        <p style="font-size:140%; padding-left: 10px;"><b>List of all attached documents</b></p>\
            				            </div>\
                                    </td>\
            		<td class="sec">\
            		</td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demof2" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf2 second-inner" id="show-1f1" value="View"/>\
            		</td>\
                                    <td class="third-inner">\
            							<input type="button" id="innerDelf2" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo1f2" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf2 second-inner" id="show-2f2" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-2f2" value="x" style="margin-right: 7px;" class="btnDelete"  onchange="showr()"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo2f2" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf2 second-inner" id="show-3f2" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-3f2" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
                                <tr class="top">\
                                    <td class="first-inner">\
                                        <p id="demo3f2" style="font-size:120%; padding-left: 10px;"></p>\
                                    </td>\
            		<td>\
            		<input type="button" class="btn-cstmf2 second-inner" id="show-4f2" value="View"/>\
                </td>\
                                    <td>\
                                        <input type="button" id="innerDel-4f2" style="margin-right: 7px;" class="btnDelete" value="x"/>\
                                    </td>\
                                </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo4f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-5f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-5f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo5f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-6f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-6f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo6f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-7f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-7f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo7f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-8f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-8f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo8f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-9f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-9f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
        		<tr class="top">\
                <td class="first-inner">\
                    <p id="demo9f2" style="font-size:120%; padding-left: 10px"></p>\
                </td>\
<td>\
<input type="button" class="btn-cstmf2 second-inner" id="show-10f2" value="View"/>\
</td>\
                <td>\
                    <input type="button" id="innerDel-10f2" style="margin-right: 15px;" class="btnDelete" value="x"/>\
                </td>\
            </tr>\
                        </table>\
                    </div>\
                </div>\
            </div>';
        	
        	   var javascript = '<script lang="javascript">function pop(popDiv) {\
        		   popDiv.style.display = "block";\
        	}\
        		   document.getElementById("viewerMain").style.display = "none";</script>';
        	   
        	   var javascriptf1 = '<script lang="javascript">function pop(popDivf1) {\
        		   popDivf1.style.display = "block";\
        	}\
        		   document.getElementById("viewerMainf1").style.display = "none";</script>';
        	   var javascriptf2 = '<script lang="javascript">function pop(popDivf2) {\
        		   popDivf2.style.display = "block";\
        	}\
           	   document.getElementById("viewerMainf2").style.display = "none";</script>';
               var style = '<style>body {\
           	    width: 100%;\
       }\
           	   .custom-file-upload {\
           	    width: 450px;\
           	}\
           	input[type="file"]{\
           	    height: 0px;\
           	}\
           	input[type="file"]:before {\
           	    content: "Choose File";\
           	    width: 80px;\
           	    height: 20px;\
           	    display: block;\
           	    text-align: center;\
           	    padding-bottom: 5px;\
           	    position: relative;\
           	    border-radius: 5px;\
           	    line-height: 25px;\
           	    padding-left: 0px;\
           	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
           	    font-size: 12px;\
            	background-color: #72a3c2;\
           	    color: rgb(255, 255, 255);\
           	    border: none;\
           	    float: left;\
           	    margin-left: 2px;\
           	}\
            	   #smtbtn {\
            	   background-color: rgb(211, 211, 211);\
            	   color: rgb(255, 255, 255);\
                   border-radius: 5px;\
            	   border: none;\
            	   padding-top: 5px;\
            	   padding-bottom: 5px;\
            	   padding-left: 10px;\
            	   padding-right: 10px;\
            	   margin-left:10px;\
            	   }\
            	   #link {\
           text-decoration: none;\
           padding-left: 10px;\
           padding-right: 10px;\
           padding-top: 5px;\
           padding-bottom: 5px;\
           background-color: rgb(211, 211, 211);\
           color: rgb(255, 255, 255);\
           border-radius: 5px;\
           font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
           font-size: 12px;\
           float: right;\
           margin-top: 0;\
       }\
            	   a.disabled {\
            	   pointer-events: none;\
            	   cursor: default;\
            	}\
       .container-main {\
           display: none;\
           height: 100%;\
           width: 100%;\
           margin-top: 40px\
       }\
       #popUp {\
           position: absolute;\
           color: #000000;\
           background-color: #ffffff;\
           /* To align popup window at the center of screen*/\
           top: 50%;\
           left: 50%;\
           margin-top: -100px;\
           margin-left: -150px;\
           z-index: 9002;\
       }\
       .container-outer {\
           width: 600px;\
           height: 300px;\
           background-color: rgb(52, 58, 78);\
           border-radius: 10px;\
           position: relative;\
           margin: auto;\
       }\
       .container-inner {\
           width: 580px;\
           height: 280px;\
           background-color: rgb(239, 233, 229);\
           position: relative;\
           margin-left: auto;\
           margin-right: auto;\
           top: 10px;\
           border-radius: 10px;\
           font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
           overflow: auto;\
       }\
       /*table tr:nth-child(odd) {*/\
           /*background-color: rgb(102, 102, 102);*/\
       /*}*/\
       #close {\
           background-color: rgb(102, 102, 102);\
           color: rgb(255, 255, 255);\
           padding: 10px;\
           border-radius: 10px;\
           float: right;\
           text-decoration: none;\
       }\
            	   .top {\
            	    width: 500px;\
            	    height: 55px;\
            	    overflow: hidden;\
            	    padding: 0;\
            	    margin: 0;\
            	}\
            	   .first-inner-1 {\
            	    width: 380px;\
            	    height: 50px;\
            	    padding: 0;\
            	}\
            	   .second-inner {\
            	    width: 60px;\
            	    display: none;\
            	}\
            	   .third-inner {\
            	    width: 500px;\
            	}\
            	   \
           input[type = "button"] {\
           text-decoration: none;\
           background: none;\
           background-color: rgb(181, 9, 56);\
           color: rgb(255, 255, 255);\
           border: none;\
           border-radius: 5px;\
           padding: 8px;\
           font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
           font-size: 14px;\
           float: right;\
       }\
       input[type="file"]:focus {\
           border-style: none;\
       }\
            	   #progressbox {\
            	   border: 1px solid #0099CC;\
            	   padding: 1px; \
            	   position:relative;\
            	   width:400px;\
            	   border-radius: 3px;\
            	   margin: 10px;\
            	   display:none;\
            	   text-align:left;\
            	   }\
            	   #progressbar {\
            	   height:20px;\
            	   border-radius: 3px;\
            	   background-color: rgb(22, 106, 143);\
            	   width:1%;\
            	   }\
            	   #statustxt {\
            	   top:3px;\
            	   left:50%;\
            	   position:absolute;\
            	   display:inline-block;\
            	   color: #000000;\
            	   }\
       #innerDel {\
           display: none;\
           float: right;\
       }\
       #innerDel-2 {\
           display: none;\
       }\
       #innerDel-3 {\
           display: none;\
     	}\
      	#innerDel-4 {\
           display: none;\
    	}\
            	   #innerDel-5 {\
                   display: none;\
            	}\
            	   #innerDel-6 {\
                   display: none;\
            	}\
            	   #innerDel-7 {\
                   display: none;\
            	}\
            	   #innerDel-8 {\
                   display: none;\
            	}\
            	   #innerDel-9 {\
                   display: none;\
            	}\
            	   #innerDel-10 {\
                   display: none;\
            	}\
            	   .dsTable {\
            	    width: 550px;\
            	}\
            	.btn-cstm {\
            	    border: none;\
            	    /*background-color: rgb(59, 158, 198);*/\
            	    background-color: rgb(51, 72, 88);\
            	    color: white;\
            	    height: 35px;\
            	    width: 50px;\
            	    border-radius: 8px;\
            	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
            	    font-size: 15px;\
            	}\
            	.dsTable-row-ht {\
            	    width: 400px;\
            	}</style>';
               
               var stylef1 = '<style>body {\
              	    width: 100%;\
          }\
              	   .custom-file-upload {\
              	    width: 450px;\
              	}\
              	input[type="file"]{\
              	    height: 0px;\
              	}\
            	   input[type="file"]:before {\
              	    content: "Choose File";\
              	    width: 80px;\
              	    height: 20px;\
              	    display: block;\
              	    text-align: center;\
              	    padding-bottom: 5px;\
              	    position: relative;\
              	    border-radius: 5px;\
              	    line-height: 25px;\
              	    padding-left: 0px;\
              	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
              	    font-size: 12px;\
               	    background-color: #72a3c2;\
              	    color: rgb(255, 255, 255);\
              	    border: none;\
              	    float: left;\
              	    margin-left: 2px;\
              	}\
               	   #smtbtnf1 {\
            	   background-color: rgb(211, 211, 211);\
            	   color: rgb(255, 255, 255);\
                   border-radius: 5px;\
            	   border: none;\
            	   padding-top: 5px;\
            	   padding-bottom: 5px;\
            	   padding-left: 10px;\
            	   padding-right: 10px;\
            	   margin-left:10px;\
               	   }\
               	   #linkf1 {\
            	   text-decoration: none;\
                   padding-left: 10px;\
                   padding-right: 10px;\
                   padding-top: 5px;\
                   padding-bottom: 5px;\
                   background-color: rgb(211, 211, 211);\
                   color: rgb(255, 255, 255);\
                   border-radius: 5px;\
                   font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
                   font-size: 12px;\
                   float: right;\
                   margin-top: 0;\
          }\
            	   a.disabled {\
            	   pointer-events: none;\
            	   cursor: default;\
            	}\
          .container-main {\
              display: none;\
              height: 100%;\
              width: 100%;\
              margin-top: 40px\
          }\
          #popUpf1 {\
              position: absolute;\
              color: #000000;\
              background-color: #ffffff;\
              /* To align popup window at the center of screen*/\
              top: 50%;\
              left: 50%;\
              margin-top: -100px;\
              margin-left: -150px;\
              z-index: 9002;\
          }\
          .container-outer {\
              width: 605px;\
              height: 300px;\
              background-color: rgb(52, 58, 78);\
              border-radius: 10px;\
              position: relative;\
              margin: auto;\
          }\
          .container-inner {\
              width: 585px;\
              height: 280px;\
              background-color: rgb(239, 233, 229);\
              position: relative;\
              margin-left: auto;\
              margin-right: auto;\
              top: 10px;\
              border-radius: 10px;\
              font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
              overflow: auto;\
          }\
          /*table tr:nth-child(odd) {*/\
              /*background-color: rgb(102, 102, 102);*/\
          /*}*/\
          #closef1 {\
              background-color: rgb(102, 102, 102);\
              color: rgb(255, 255, 255);\
              padding: 10px;\
              border-radius: 10px;\
              float: right;\
              text-decoration: none;\
          }\
               	   .top {\
               	    width: 500px;\
               	    height: 55px;\
               	    overflow: hidden;\
               	    padding: 0;\
               	    margin: 0;\
               	}\
               	   .first-inner-1 {\
               	    width: 380px;\
               	    height: 50px;\
               	    padding: 0;\
               	}\
               	   .second-inner {\
               	    width: 60px;\
               	    display: none;\
               	}\
               	   .third-inner {\
               	    width: 500px;\
               	}\
               	   \
              input[type = "button"] {\
              text-decoration: none;\
              background: none;\
              background-color: rgb(181, 9, 56);\
              color: rgb(255, 255, 255);\
              border: none;\
              border-radius: 5px;\
              padding: 8px;\
              font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
              font-size: 14px;\
              float: right;\
          }\
          input[type="file"]:focus {\
              border-style: none;\
          }\
            	   #progressboxf1 {\
            	   border: 1px solid #0099CC;\
            	   padding: 1px; \
            	   position:relative;\
            	   width:400px;\
            	   border-radius: 3px;\
            	   margin: 10px;\
            	   display:none;\
            	   text-align:left;\
            	   }\
            	   #progressbarf1 {\
            	   height:20px;\
            	   border-radius: 3px;\
            	   background-color: rgb(22, 106, 143);\
            	   width:1%;\
            	   }\
            	   #statustxtf1 {\
            	   top:3px;\
            	   left:50%;\
            	   position:absolute;\
            	   display:inline-block;\
            	   color: #000000;\
            	   }\
          #innerDelf1 {\
              display: none;\
              float: right;\
          }\
          #innerDel-2f1 {\
              display: none;\
          }\
          #innerDel-3f1 {\
              display: none;\
        	}\
         	#innerDel-4f1 {\
              display: none;\
       	}\
            	   #innerDel-5f1 {\
                   display: none;\
            	}\
            	   #innerDel-6f1 {\
                   display: none;\
            	}\
            	   #innerDel-7f1 {\
                   display: none;\
            	}\
            	   #innerDel-8f1 {\
                   display: none;\
            	}\
            	   #innerDel-9f1 {\
                   display: none;\
            	}\
            	   #innerDel-10f1 {\
                   display: none;\
            	}\
               	   .dsTable {\
               	    width: 550px;\
               	}\
               	.btn-cstmf1 {\
               	    border: none;\
               	    /*background-color: rgb(59, 158, 198);*/\
               	    background-color: rgb(51, 72, 88);\
               	    color: white;\
               	    height: 35px;\
               	    width: 60px;\
               	    border-radius: 8px;\
               	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
               	    font-size: 15px;\
               	}\
               	.dsTable-row-ht {\
               	    width: 400px;\
               	}</style>';
               var stylef2 = '<style>body {\
             	    width: 100%;\
         }\
             	   .custom-file-upload {\
             	    width: 450px;\
             	}\
             	input[type="file"]{\
             	    height: 0px;\
             	}\
           	  	input[type="file"]:before {\
             	    content: "Choose File";\
             	    width: 100px;\
             	    height: 30px;\
             	    display: block;\
             	    text-align: center;\
             	    padding-bottom: 5px;\
             	    position: relative;\
             	    border-radius: 5px;\
             	    line-height: 35px;\
             	    padding-left: 0px;\
             	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
             	    font-size: 12px;\
              	    background-color: #72a3c2;\
             	    color: rgb(255, 255, 255);\
             	    border: none;\
             	    float: left;\
             	    margin-left: 2px;\
             	}\
              	   #smtbtnf1 {\
              	   background-color: rgb(211, 211, 211);\
              	   color: rgb(255, 255, 255);\
                  border-radius: 5px;\
              	   border: none;\
              	   padding-top: 10px;\
              	   padding-bottom: 10px;\
              	   padding-left: 15px;\
              	   padding-right: 15px;\
              	   margin-left:10px;\
              	   }\
              	   #linkf1 {\
             text-decoration: none;\
             padding-left: 15px;\
             padding-right: 15px;\
             padding-top: 10px;\
             padding-bottom: 10px;\
             background-color: rgb(211, 211, 211);\
             color: rgb(255, 255, 255);\
             border-radius: 5px;\
             font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
             font-size: 12px;\
             float: right;\
             margin-top: 0;\
         }\
           	   a.disabled {\
           	   pointer-events: none;\
           	   cursor: default;\
           	}\
         .container-main {\
             display: none;\
             height: 100%;\
             width: 100%;\
             margin-top: 40px\
         }\
         #popUpf1 {\
             position: absolute;\
             color: #000000;\
             background-color: #ffffff;\
             /* To align popup window at the center of screen*/\
             top: 50%;\
             left: 50%;\
             margin-top: -100px;\
             margin-left: -150px;\
             z-index: 9002;\
         }\
         .container-outer {\
             width: 605px;\
             height: 300px;\
             background-color: rgb(52, 58, 78);\
             border-radius: 10px;\
             position: relative;\
             margin: auto;\
         }\
         .container-inner {\
             width: 585px;\
             height: 280px;\
             background-color: rgb(239, 233, 229);\
             position: relative;\
             margin-left: auto;\
             margin-right: auto;\
             top: 10px;\
             border-radius: 10px;\
             font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
             overflow: auto;\
         }\
         /*table tr:nth-child(odd) {*/\
             /*background-color: rgb(102, 102, 102);*/\
         /*}*/\
         #closef1 {\
             background-color: rgb(102, 102, 102);\
             color: rgb(255, 255, 255);\
             padding: 10px;\
             border-radius: 10px;\
             float: right;\
             text-decoration: none;\
         }\
              	   .top {\
              	    width: 500px;\
              	    height: 55px;\
              	    overflow: hidden;\
              	    padding: 0;\
              	    margin: 0;\
              	}\
              	   .first-inner-1 {\
              	    width: 380px;\
              	    height: 50px;\
              	    padding: 0;\
              	}\
              	   .second-inner {\
              	    width: 60px;\
              	    display: none;\
              	}\
              	   .third-inner {\
              	    width: 500px;\
              	}\
              	   \
             input[type = "button"] {\
             text-decoration: none;\
             background: none;\
             background-color: rgb(181, 9, 56);\
             color: rgb(255, 255, 255);\
             border: none;\
             border-radius: 5px;\
             padding: 8px;\
             font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
             font-size: 14px;\
             float: right;\
         }\
         input[type="file"]:focus {\
             border-style: none;\
         }\
           	   #progressboxf1 {\
           	   border: 1px solid #0099CC;\
           	   padding: 1px; \
           	   position:relative;\
           	   width:400px;\
           	   border-radius: 3px;\
           	   margin: 10px;\
           	   display:none;\
           	   text-align:left;\
           	   }\
           	   #progressbarf1 {\
           	   height:20px;\
           	   border-radius: 3px;\
           	   background-color: rgb(22, 106, 143);\
           	   width:1%;\
           	   }\
           	   #statustxtf1 {\
           	   top:3px;\
           	   left:50%;\
           	   position:absolute;\
           	   display:inline-block;\
           	   color: #000000;\
           	   }\
         #innerDelf1 {\
             display: none;\
             float: right;\
         }\
         #innerDel-2f1 {\
             display: none;\
         }\
         #innerDel-3f1 {\
             display: none;\
       	}\
        	#innerDel-4f1 {\
             display: none;\
      	}\
           	   #innerDel-5f1 {\
                  display: none;\
           	}\
           	   #innerDel-6f1 {\
                  display: none;\
           	}\
           	   #innerDel-7f1 {\
                  display: none;\
           	}\
           	   #innerDel-8f1 {\
                  display: none;\
           	}\
           	   #innerDel-9f1 {\
                  display: none;\
           	}\
           	   #innerDel-10f1 {\
                  display: none;\
           	}\
              	   .dsTable {\
              	    width: 550px;\
              	}\
              	.btn-cstmf1 {\
              	    border: none;\
              	    /*background-color: rgb(59, 158, 198);*/\
              	    background-color: rgb(51, 72, 88);\
              	    color: white;\
              	    height: 35px;\
              	    width: 60px;\
              	    border-radius: 8px;\
              	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
              	    font-size: 15px;\
              	}\
              	.dsTable-row-ht {\
              	    width: 400px;\
              	}</style>';
            //Display document upload button for form level on client UI   
    		var button = html + style + javascript;
    		//Assign eResearch's context to hidden fields
    		$("#"+stemModel.get('htmlFormId')).prepend(button);
    		//$('#fillform').prepend(button);
    		$('#createdUser').val(global.params.loggedInUser);
 		    $('#formId').val(global.params.formResponsePK);
 		    $('#personId').val(global.params.patientPK);
 		    $('#studyID').val(global.params.studyPK);
 		    $('#taylorURL').val(stemModel.get('taylorUrl'));
    		
 		    //Logic to show document attachment in field level
 		    if (stemModel.get('htmlSpanIdOfFieldvalue1')) {
 		    	var fieldvalue1spanId = stemModel.get('htmlSpanIdOfFieldvalue1');
 		    	for (var i = 0; i < fieldvalue1spanId.length; i++) {
 		    		if (document.getElementById(fieldvalue1spanId[i])) {
 		    			var buttonMarginRight = 621 - $("#"+fieldvalue1spanId[i]).width();
 		    			//alert($("#"+fieldvalue1spanId[i]).width());
 		    			//Display document upload button for field level on client UI  
 		    			var buttonf1 = '<div style="float:right; overflow:hidden; margin-right:'+buttonMarginRight+'px;">'+ htmlf1 + stylef1 + javascriptf1+'</div>';//Append at same line
 		    			$("#"+fieldvalue1spanId[i]).append(buttonf1);
 		 		    	$('#createdUserf1').val(global.params.loggedInUser);
 		    		    $('#formIdf1').val(global.params.formResponsePK);
 		    		    $('#personIdf1').val(global.params.patientPK);
 		    		    $('#studyIDf1').val(global.params.studyPK);
 		     		    $('#fieldIdf1').val(fieldvalue1spanId[i]);
 		     		    $('#taylorURLf1').val(stemModel.get('taylorUrl'));
 		     		    //Check Document existence in field level after page loading
 		     		    var fieldId = document.getElementById("fieldIdf1").value; 		    
 		     		    if (fieldId == fieldvalue1spanId[i]){
 		     		    	var formId = global.params.formResponsePK;
 		        			var formIdfieldId = formId+'-'+fieldId;
 		        			var subParams = [stemModel.get('taylorUrl'),formIdfieldId];
 		            		var params = {"serviceParams":subParams};
 		            		var taylorWSClientModel = new TaylorWSClientModel();
 		         		    taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.checkDocumentExistencef1);
 		     		    }
 		     		    //Function to Fetch Document in field level
 		        		document.getElementById("linkf1").addEventListener("click", function(){
 		        			$("#dsTablef1 tr.top:odd").css("background-color", "rgb(114, 163, 194)");
 		        			$("#dsTablef1 tr.top:even").css("background-color", "rgb(239, 233, 229)");
 		        			jQuery("#fileListf1").dialog({
 		         		          autoOpen: true,
 		         		          modal: true, width:669, height:402,
 		         		          close: function() {
 		         		          jQuery("#fileListf1").dialog("destroy");
 		         		          }
 		         		    });
 		        			var formId = global.params.formResponsePK;
 		        			var fieldId = document.getElementById("fieldIdf1").value;
 		        			var formIdfieldId = formId+'-'+fieldId;
 		        			var subParams = [stemModel.get('taylorUrl'),formIdfieldId];
 		            		var params = {"serviceParams":subParams};
 		            		var taylorWSClientModel = new TaylorWSClientModel();
 		         		    taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.fetchFilef1);
 		        		});
 		        		//Function to enable/disable upload button for field level
 		        		var uploadFieldLevel = document.getElementById("uploadf1");
 		        		var handleFileSelectf1 = function(evt) {
 		        		    var files = evt.target.files;
 		        		    var file = files[0];
 		        		    $("#uploadf1").blur();//de-focus button after clicking
 		        		    if (files && file) {
 		        		    	$("#smtbtnf1").css("background-color", "rgb(114, 163, 194)");
 		        		    	$("#smtbtnf1").prop('disabled', false);
 		        		    	var fileSize = (Math.round(file.size * 100 / 1024) / 100).toString();
 		        		    	var fileSize = parseInt(fileSize) + 'KB';
 		        		    	var text = file.name+' (' + fileSize + ')';
 		        		    	document.getElementById("fileDetailsf1").innerHTML = text;
 		        		    } else {
 		        		    	$("#smtbtnf1").css("background-color", "rgb(211, 211, 211)");
 		        		    	$('#smtbtnf1').attr('disabled',true);
 		        		    }
 		        		};    
 		        		uploadFieldLevel.addEventListener('change', handleFileSelectf1, false);
 		    		}
 		    	}
    		}
 		   // Function to save document to Server for form level
 		   $("#multiform").submit(function(e)
	 				  {
				          document.getElementById('progressbox').style.display = 'block';
				          document.getElementById('output').style.display = 'block';
				          document.getElementById("fileDetails").innerHTML = '';
				          this.blur(); //To de-focus the on-click button after clicking	    			
	 				      var formObj = $(this);
	 				      var formURL = stemModel.get('isquareUrl')+'saveFileInForm';
	 				      var formData = new FormData(this);
	 				      //Function to show upload progress bar in dialog box
	 				      jQuery("#uploadProgressBar").dialog({
	 				    	  autoOpen: true,
	 				    	  modal: true, width:453, height:125,
	 				    	  close: function() {	 	 		  
	 				    		  jQuery("#uploadProgressBar").dialog("destroy");
	 				    		  document.getElementById("output").innerHTML = '<b>'+'Upload Status: '+'</b>'; 
	 				    	  }
	 	 		          });
	 				      jQuery("#uploadProgressBar").dialog("open");
	 				      $.ajax({
	 				    	xhr: function(){
	 				          var xhr = new XMLHttpRequest();
	 				          //Function to display upload progress bar
	 				          xhr.upload.addEventListener("progress", function(evt){
	 				        	 if (evt.lengthComputable) {
	 				        		  var percentComplete = Math.round(evt.loaded * 100 / evt.total);
	 				        		  //console.log(percentComplete);
	 				        		  $('#progressbar').css({
	 			 	                    width: percentComplete + '%' //update progressbar percent complete
	 				        		  });
	 				        		  document.getElementById("statustxt").innerHTML = percentComplete + '%'; //update status text	 	
	 				        		  if(percentComplete > 50) {
	 				        			  $('#statustxt').css({
	 				        				  color: '#fff'
	 				        			  }); 
	 				        		  }
	 			 				 }
	 				          }, false);
	 				          //Function to show abort message
	 				          xhr.addEventListener("abort", function(evt){
	 				        	$("#output").html($("#output").html() + " The file upload has been canceled by the user.");
	 				        	//document.getElementById("output").innerHTML = "The file upload has been canceled by the user.";
	 				    	  }, false);	 	
	 				      return xhr;
	 				      },
	 				      url: formURL,
	 				      type: 'POST',
	 				      data:  formData,
	 				      mimeType:"multipart/form-data",
	 				      contentType: false,
	 				      cache: false,
	 				      processData:false,
	 				      success: function(data, textStatus, jqXHR) {
	 				    	 //document.getElementById("output").innerHTML = "The file was uploaded successfully! Click 'See files attached' to view files";
	 				    	 $("#output").html($("#output").html() + " The file was uploaded successfully. Click 'View files' to see documents.");
	 				    	 $("#smtbtn").css("background-color", "rgb(211, 211, 211)");
	 	    		    	 $('#smtbtn').attr('disabled',true);
	 				    	 //console.log('textStatus: '+textStatus);
	 				    	 var subParams = [stemModel.get('taylorUrl'),global.params.formResponsePK];
		            		 var params = {"serviceParams":subParams};
		            		 var taylorWSClientModel = new TaylorWSClientModel();
		         		     taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.checkDocumentExistence);
	 				      },
	 				      error: function(jqXHR, textStatus, errorThrown) {
	 				    	  $("#output").html($("#output").html() + " An error occured while uploading the file.");
	 				    	  $('#progressbar').css("background-color", "#f93807"); 
	 				    	  console.log('textStatus: '+textStatus);
	 				      }          
	 				      });
	 				      e.preventDefault(); //Prevent Default action. 
	 				      //e.unbind();
		   });
 		// Function to save document to Server for field level
 		   $("#multiformf1").submit(function(e)
	 				  {
				          document.getElementById('progressboxf1').style.display = 'block';
				          document.getElementById('outputf1').style.display = 'block';
				          document.getElementById("fileDetailsf1").innerHTML = '';
				          this.blur(); //To de-focus the on-click button after clicking	    			
	 				      var formObj = $(this);
	 				      var formURL = stemModel.get('isquareUrl')+'saveFileInField';
	 				      var formData = new FormData(this);
	 				      //Function to show upload progress bar in dialog box
	 				      jQuery("#uploadProgressBarf1").dialog({
	 				    	  autoOpen: true,
	 				    	  modal: true, width:453, height:125,
	 				    	  close: function() {	 	 		  
	 				    		  jQuery("#uploadProgressBarf1").dialog("destroy");
	 				    		  document.getElementById("outputf1").innerHTML = '<b>'+'Upload Status: '+'</b>'; 
	 				    	  }
	 	 		          });
	 				      jQuery( "#uploadProgressBarf1" ).dialog("open");
	 				      $.ajax({
	 				    	xhr: function(){
	 				          var xhr = new XMLHttpRequest();
	 				          //Function to display upload progress bar
	 				          xhr.upload.addEventListener("progress", function(evt){
	 				        	 if (evt.lengthComputable) {
	 				        		  var percentComplete = Math.round(evt.loaded * 100 / evt.total);
	 				        		  //console.log(percentComplete);
	 				        		  $('#progressbarf1').css({
	 			 	                    width: percentComplete + '%' //update progressbar percent complete
	 				        		  });
	 				        		  document.getElementById("statustxtf1").innerHTML = percentComplete + '%'; //update status text	 	
	 				        		  if(percentComplete > 50) {
	 				        			  $('#statustxtf1').css({
	 				        				  color: '#fff'
	 				        			  }); 
	 				        		  }
	 			 				 }
	 				          }, false);	 				          
	 				          //Function to show abort message
	 				          xhr.addEventListener("abort", function(evt){
	 				        	$("#outputf1").html($("#outputf1").html() + " The file upload has been canceled by the user.");
	 				        	//document.getElementById("output").innerHTML = "The file upload has been canceled by the user.";
	 				    	  }, false);	 	
	 				      return xhr;
	 				      },
	 				      url: formURL,
	 				      type: 'POST',
	 				      data:  formData,
	 				      mimeType:"multipart/form-data",
	 				      contentType: false,
	 				      cache: false,
	 				      processData:false,
	 				      success: function(data, textStatus, jqXHR) {
	 				    	 //document.getElementById("output").innerHTML = "The file was uploaded successfully! Click 'See files attached' to view files";
	 				    	 $("#outputf1").html($("#outputf1").html() + " The file was uploaded successfully. Click 'View files' to see documents.");
	 				    	 $("#smtbtnf1").css("background-color", "rgb(211, 211, 211)");
	 	    		    	 $('#smtbtnf1').attr('disabled',true);
	 				    	 //console.log('textStatus: '+textStatus);
	 				    	 var formId = global.params.formResponsePK;
	 				    	 var fieldId = document.getElementById("fieldIdf1").value;
	 	        			 var formIdfieldId = formId+'-'+fieldId;
	   	        			 var subParams = [stemModel.get('taylorUrl'),formIdfieldId];
		            		 var params = {"serviceParams":subParams};
		            		 var taylorWSClientModel = new TaylorWSClientModel();
		         		     taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.checkDocumentExistencef1);
	 				      },
	 				      error: function(jqXHR, textStatus, errorThrown) {
	 				    	 $("#outputf1").html($("#outputf1").html() + " An error occured while uploading the file.");
	 				    	  $('#progressbarf1').css("background-color", "#f93807");
	 				    	  console.log('textStatus: '+textStatus);
	 				      }          
	 				      });
	 				      e.preventDefault(); //Prevent Default action. 
		   });		    
 		   //Function to Delete Document for form level
    		$("#dsTable").on("click", ".btnDelete", function(){	
    			this.blur();
    			var docID = $(this).closest("tr").find(".docid").val();
    			var deleteButtonID = $(this).attr("id");
    			var textID = $(this).closest("tr").find("p").attr("id");
    			var viewButtonID = $(this).closest("tr").find(".btn-cstm").attr("id");
    			document.getElementById("deleteMsg").style.display = 'block';
    			//alert('Delete ID:'+deleteButtonID);
    			//alert('textID:'+textID);
    			//alert('viewButtonID:'+viewButtonID);
    			jQuery("#deleteConfirm").dialog({
    			      resizable: false,
    			      height:140,
    			      modal: true,
    			      buttons: {
    			        "OK": function() {
    			        	jQuery(this).dialog("close");
    			        	document.getElementById(viewButtonID).style.display = 'none';
    			        	document.getElementById(deleteButtonID).style.display = 'none';
    			        	document.getElementById(textID).innerHTML = '';
    			        },
    			        Cancel: function() {
    			        	jQuery(this).dialog("close");
    			        }
    			      }
    			});
    			//assign 'OK' button an id
    			$('.ui-button-text:contains(OK)').attr("id","confirmDelete"); 
    			$("#confirmDelete").on("click", function() {
    				//alert("before delete" + docID);
    				var subParams = [stemModel.get('taylorUrl'),docID];
            		var params = {"serviceParams":subParams};
            		var taylorWSClientModel = new TaylorWSClientModel();
         		    taylorWSClientModel.deleteDocument(stemModel, params, stemModel.deleteFile);
    			});
        	});
    		//Function to Delete Document for field level
    		$("#dsTablef1").on("click", ".btnDelete", function(){	
    			this.blur();
    			var docID = $(this).closest("tr").find(".docid").val();
    			var deleteButtonID = $(this).attr("id");
    			var textID = $(this).closest("tr").find("p").attr("id");
    			var viewButtonID = $(this).closest("tr").find(".btn-cstmf1").attr("id");
    			document.getElementById("deleteMsgf1").style.display = 'block';
    			//alert('Delete ID:'+deleteButtonID);
    			//alert('textID:'+textID);
    			//alert('viewButtonID:'+viewButtonID);
    			jQuery("#deleteConfirmf1").dialog({
    			      resizable: false,
    			      height:140,
    			      modal: true,
    			      buttons: {
    			        "OK": function() {
    			        	jQuery(this).dialog("close");
    			        	document.getElementById(viewButtonID).style.display = 'none';
    			        	document.getElementById(deleteButtonID).style.display = 'none';
    			        	document.getElementById(textID).innerHTML = '';
    			        },
    			        Cancel: function() {
    			        	jQuery(this).dialog("close");
    			        }
    			      }
    			});
    			//assign 'OK' button an id
    			$('.ui-button-text:contains(OK)').attr("id","confirmDeletef1"); 
    			$("#confirmDeletef1").on("click", function() {
    				//deleteDocument;
    				//alert("before delete" + docID);
    				var subParams = [stemModel.get('taylorUrl'),docID];
            		var params = {"serviceParams":subParams};
            		var taylorWSClientModel = new TaylorWSClientModel();
         		    taylorWSClientModel.deleteDocument(stemModel, params, stemModel.deleteFilef1);
    			});
        	});
    		//Function to View Document in form level
    		$(".btn-cstm").on("click", function() {
    			this.blur(); //To de-focus the on-click button after clicking
    			var fileContent = $(this).closest("tr").find(".docContent").val();
    			var fileName = $(this).closest("tr").find(".docName").val();
    			var contentType = $(this).closest("tr").find(".docContentType").val();
    			var sourceFile = fileContent;
    			document.getElementById('viewerMain').style.display = 'block';
 		        $("#viewer").attr("src",sourceFile);
 		        $("#viewer").attr("type",contentType);
 		        $('#viewerMain').prop('title', fileName);
 		        jQuery("#viewerMain").dialog({
 		          autoOpen: true,
 		          modal: true, width:1050, height:710,
 		          close: function() {
 		        	 $("#viewer").attr("src","");
 		        	 $("#viewer").attr("type","");
 		        	 jQuery("#viewerMain").dialog("destroy");
 		          
 		          }
 		       });
        	});
    		//Function to View Document in field level
    		$(".btn-cstmf1").on("click", function() {
    			this.blur(); //To de-focus the on-click button after clicking
    			var fileContent = $(this).closest("tr").find(".docContent").val();
    			var fileName = $(this).closest("tr").find(".docName").val();
    			var contentType = $(this).closest("tr").find(".docContentType").val();
    			var sourceFile = 'data:'+contentType+';base64,'+fileContent;
    			document.getElementById('viewerMainf1').style.display = 'block';
    			$("#viewerf1").attr("src",sourceFile);
    			$('#viewerMainf1').prop('title', fileName);
 		        jQuery("#viewerMainf1").dialog({
 		          autoOpen: true,
 		          modal: true, width:1050, height:710,
 		          close: function() {
 		        	 $("#viewerf1").attr("src","");
 		        	 jQuery("#viewerMainf1").dialog("destroy");
 		          }
 		       });
        	});
    		//Function to Fetch Document in form level
    		$("#link").on("click", function(){
    			$("#dsTable tr.top:odd").css("background-color", "rgb(114, 163, 194)");
    			$("#dsTable tr.top:even").css("background-color", "rgb(239, 233, 229)");
    			jQuery("#fileList").dialog({
   		          autoOpen: true,
   		          modal: true, width:669, height:402,
   		          close: function() {
   		          jQuery("#fileList").dialog("destroy");
   		          }
   		       });
    			var subParams = [stemModel.get('taylorUrl'),global.params.formResponsePK];
        		var params = {"serviceParams":subParams};
        		var taylorWSClientModel = new TaylorWSClientModel();
     		    taylorWSClientModel.retrieveDocument(stemModel, params, stemModel.fetchFile);
    		});
    		
    		//Function to enable/disable upload button for form level
    		var uploadFormLevel = document.getElementById("upload");
    		var handleFileSelect = function(evt) {
    		    var files = evt.target.files;
    		    var file = files[0];
    		    $("#upload").blur();//de-focus button after clicking
    		    if (files && file) {
    		    	$("#smtbtn").css("background-color", "rgb(114, 163, 194)");
    		    	$("#smtbtn").prop('disabled', false);
    		    	var fileSize = (Math.round(file.size * 100 / 1024) / 100).toString();
    		    	var fileSize = parseInt(fileSize) + 'KB';
    		    	var text = file.name+' (' + fileSize + ')';
    		    	document.getElementById("fileDetails").innerHTML = text;
    		    	$("#fileDetails").css("overflow", "hidden");
    		    	//$('#smtBtn').attr('disabled',false);
    		    } else {
    		    	$("#smtbtn").css("background-color", "rgb(211, 211, 211)");
    		    	$('#smtbtn').attr('disabled',true);
    		    }
    		};    
    		$("#upload").on('change', handleFileSelect, false);    		    		
        },     
        deleteFile: function(thisModel, xmlDocumentData, status){
        	//console.log('deleteAttachedDocuments '+xmlDocumentData);
        	var result = xmlDocumentData.result;
        	if (result == "Success"){
        		document.getElementById("deleteSuccess").style.display = 'block';
        		jQuery("#deleteStatus").dialog({
   		          autoOpen: true,
   		          modal: true, width:260, height:90,
   		          close: function() {
   		        	jQuery(this).dialog("destroy");
   		          }
   		       });
        		var subParams = [thisModel.get('taylorUrl'),global.params.formResponsePK];
        		var params = {"serviceParams":subParams};
        		var taylorWSClientModel = new TaylorWSClientModel();
     		    taylorWSClientModel.retrieveDocument(thisModel, params, thisModel.checkDocumentExistenceAfterDeletion);	
        	} else {
        		document.getElementById("deleteFail").style.display = 'block';
        		jQuery("#deleteStatus").dialog({
   		          autoOpen: true,
   		          modal: true, width:260, height:90,
   		          close: function() {
   		        	jQuery(this).dialog("destroy");
   		          }
   		       });
        	}
        },
        deleteFilef1: function(thisModel, xmlDocumentData, status){
        	//console.log('deleteAttachedDocuments '+xmlDocumentData);
        	var result = xmlDocumentData.result;
        	if (result == "Success"){
        		document.getElementById("deleteSuccessf1").style.display = 'block';
        		jQuery("#deleteStatusf1").dialog({
   		          autoOpen: true,
   		          modal: true, width:260, height:90,
   		          close: function() {
   		        	jQuery(this).dialog("destroy");
   		          }
   		       });
        		var formId = global.params.formResponsePK;
        		var fieldId = document.getElementById("fieldIdf1").value;
    			var formIdfieldId = formId+'-'+fieldId;
    			var subParams = [thisModel.get('taylorUrl'),formIdfieldId];
        		var params = {"serviceParams":subParams};
        		var taylorWSClientModel = new TaylorWSClientModel();
     		    taylorWSClientModel.retrieveDocument(thisModel, params, thisModel.checkDocumentExistenceAfterDeletionf1);	
        	} else {
        		document.getElementById("deleteFailf1").style.display = 'block';
        		jQuery("#deleteStatusf1").dialog({
   		          autoOpen: true,
   		          modal: true, width:260, height:90,
   		          close: function() {
   		        	jQuery(this).dialog("destroy");
   		          }
   		       });
        	}
        },
        checkDocumentExistence: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	console.log(xmlDocumentData);
        	if (xmlDocumentData){
        		$("#link").css("background-color", "#72a3c2");
        		$("#link").removeClass("disabled");      	
        	} else {
        		$("#link").css("background-color", "rgb(211, 211, 211)");
        	}
        },
        checkDocumentExistencef1: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	console.log(xmlDocumentData);
        	if (xmlDocumentData){
        		$("#linkf1").css("background-color", "#72a3c2");
        		$("#linkf1").removeClass("disabled");
        	} else {
        		$("#linkf1").css("background-color", "rgb(211, 211, 211)");
        	}
        },	
        checkDocumentExistenceAfterDeletion: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	console.log(xmlDocumentData);
        	if (!xmlDocumentData){
        		$("#link").css("background-color", "rgb(211, 211, 211)");
        		$("#link").addClass("disabled");
        	} else {
        		//Set list of documents to be blank for re-indexing order of documents after deletion
        		document.getElementById("show-1").style.display = 'none';
	        	document.getElementById("innerDel").style.display = 'none';
	        	document.getElementById("demo").innerHTML = '';
	        	document.getElementById("show-2").style.display = 'none';
	        	document.getElementById("innerDel-2").style.display = 'none';
	        	document.getElementById("demo1").innerHTML = '';
	        	document.getElementById("show-3").style.display = 'none';
	        	document.getElementById("innerDel-3").style.display = 'none';
	        	document.getElementById("demo2").innerHTML = '';
	        	document.getElementById("show-4").style.display = 'none';
	        	document.getElementById("innerDel-4").style.display = 'none';
	        	document.getElementById("demo3").innerHTML = '';
	        	document.getElementById("show-5").style.display = 'none';
	        	document.getElementById("innerDel-5").style.display = 'none';
	        	document.getElementById("demo4").innerHTML = '';
	        	document.getElementById("show-6").style.display = 'none';
	        	document.getElementById("innerDel-6").style.display = 'none';
	        	document.getElementById("demo5").innerHTML = '';
	        	document.getElementById("show-7").style.display = 'none';
	        	document.getElementById("innerDel-7").style.display = 'none';
	        	document.getElementById("demo6").innerHTML = '';
	        	document.getElementById("show-8").style.display = 'none';
	        	document.getElementById("innerDel-8").style.display = 'none';
	        	document.getElementById("demo7").innerHTML = '';
	        	document.getElementById("show-9").style.display = 'none';
	        	document.getElementById("innerDel-9").style.display = 'none';
	        	document.getElementById("demo8").innerHTML = '';
	        	document.getElementById("show-10").style.display = 'none';
	        	document.getElementById("innerDel-10").style.display = 'none';
	        	document.getElementById("demo9").innerHTML = '';
        		thisModel.fetchFile(thisModel, xmlDocumentData, status);
        	}
        },
        checkDocumentExistenceAfterDeletionf1: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	console.log(xmlDocumentData);
        	if (!xmlDocumentData){
        		$("#linkf1").css("background-color", "rgb(211, 211, 211)");
        		$("#linkf1").addClass("disabled");
        	} else {
        		//Set list of documents to be blank for re-indexing order of documents after deletion
        		document.getElementById("show-1f1").style.display = 'none';
	        	document.getElementById("innerDelf1").style.display = 'none';
	        	document.getElementById("demof1").innerHTML = '';
	        	document.getElementById("show-2f1").style.display = 'none';
	        	document.getElementById("innerDel-2f1").style.display = 'none';
	        	document.getElementById("demo1f1").innerHTML = '';
	        	document.getElementById("show-3f1").style.display = 'none';
	        	document.getElementById("innerDel-3f1").style.display = 'none';
	        	document.getElementById("demo2f1").innerHTML = '';
	        	document.getElementById("show-4f1").style.display = 'none';
	        	document.getElementById("innerDel-4f1").style.display = 'none';
	        	document.getElementById("demo3f1").innerHTML = '';
	        	document.getElementById("show-5f1").style.display = 'none';
	        	document.getElementById("innerDel-5f1").style.display = 'none';
	        	document.getElementById("demo4f1").innerHTML = '';
	        	document.getElementById("show-6f1").style.display = 'none';
	        	document.getElementById("innerDel-6f1").style.display = 'none';
	        	document.getElementById("demo5f1").innerHTML = '';
	        	document.getElementById("show-7f1").style.display = 'none';
	        	document.getElementById("innerDel-7f1").style.display = 'none';
	        	document.getElementById("demo6f1").innerHTML = '';
	        	document.getElementById("show-8f1").style.display = 'none';
	        	document.getElementById("innerDel-8f1").style.display = 'none';
	        	document.getElementById("demo7f1").innerHTML = '';
	        	document.getElementById("show-9f1").style.display = 'none';
	        	document.getElementById("innerDel-9f1").style.display = 'none';
	        	document.getElementById("demo8f1").innerHTML = '';
	        	document.getElementById("show-10f1").style.display = 'none';
	        	document.getElementById("innerDel-10f1").style.display = 'none';
	        	document.getElementById("demo9f1").innerHTML = '';
        		thisModel.fetchFilef1(thisModel, xmlDocumentData, status);
        	}
        },
        fetchFile: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	var docID = [];
        	var fileContent = [];
        	var fileName = [];
        	var contentType = [];
        	//console.log(xmlDocumentData);
        	if (xmlDocumentData){
        		var resultCount = xmlDocumentData.result.length;
        		console.log(resultCount);
        		if (resultCount){
        			for (var i = 0; i < resultCount; i++){
            			//alert('printing i'+i);
        				var docID = xmlDocumentData.result[i].documentUuid;
            			var fileContent = xmlDocumentData.result[i].file;
            			var fileName = xmlDocumentData.result[i].fileName;
            			var contentType = xmlDocumentData.result[i].contentType;
            			if (i == 0){
            				var txt1 = '';
                			txt1 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt1 += ": " + fileName + "<br>";
                			}
                			txt1 += "<input type='hidden' id='document' class='docid' value=''/>";
                			txt1 += "<input type='hidden' id='docName' class='docName' value=''/>";
                			txt1 += "<input type='hidden' id='content' class='docContent' value=''/>";
                			txt1 += "<input type='hidden' id='contentType' class='docContentType' value=''/>";
                			document.getElementById("demo").innerHTML = txt1;
             		        document.getElementById("innerDel").style.display = "block";
             		        document.getElementById("show-1").style.display = "block";
             		        $('#document').val(docID);
             		        $('#docName').val(fileName);
             		        $('#content').val(fileContent);
             		        $('#contentType').val(contentType);   
            			}
            			if (i == 1){
            				var txt2 = '';
                			txt2 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt2 += ": " + fileName + "<br>";
                			}
                			txt2 += "<input type='hidden' id='document1' class='docid' value=''/>";
                			txt2 += "<input type='hidden' id='docName1' class='docName' value=''/>";
                			txt2 += "<input type='hidden' id='content1' class='docContent' value=''/>";
                			txt2 += "<input type='hidden' id='contentType1' class='docContentType' value=''/>";
                			document.getElementById("demo1").innerHTML = txt2;
             		        document.getElementById("innerDel-2").style.display = "block";
             		        document.getElementById("show-2").style.display = "block";
             		        $('#document1').val(docID);
             		        $('#docName1').val(fileName);
             		        $('#content1').val(fileContent);
             		        $('#contentType1').val(contentType); 
            			}
            			if (i == 2){
            				var txt3 = '';
                			txt3 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt3 += ": " + fileName + "<br>";
                			}
                			txt3 += "<input type='hidden' id='document2' class='docid' value=''/>";
                			txt3 += "<input type='hidden' id='docName2' class='docName' value=''/>";
                			txt3 += "<input type='hidden' id='content2' class='docContent' value=''/>";
                			txt3 += "<input type='hidden' id='contentType2' class='docContentType' value=''/>";
                			document.getElementById("demo2").innerHTML = txt3;
             		        document.getElementById("innerDel-3").style.display = "block";
             		        document.getElementById("show-3").style.display = "block";
             		        $('#document2').val(docID);
             		        $('#docName2').val(fileName);
             		        $('#content2').val(fileContent);
             		        $('#contentType2').val(contentType); 
            			}
            			if (i == 3){
            				var txt4 = '';
                			txt4 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt4 += ": " + fileName + "<br>";
                			}
                			txt4 += "<input type='hidden' id='document3' class='docid' value=''/>";
                			txt4 += "<input type='hidden' id='docName3' class='docName' value=''/>";
                			txt4 += "<input type='hidden' id='content3' class='docContent' value=''/>";
                			txt4 += "<input type='hidden' id='contentType3' class='docContentType' value=''/>";
                			document.getElementById("demo3").innerHTML = txt4;
             		        document.getElementById("innerDel-4").style.display = "block";
             		        document.getElementById("show-4").style.display = "block";
             		        $('#document3').val(docID);
             		        $('#docName3').val(fileName);
             		        $('#content3').val(fileContent);
             		        $('#contentType3').val(contentType); 
            			}
            			if (i == 4){
            				var txt5 = '';
                			txt5 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt5 += ": " + fileName + "<br>";
                			}
                			txt5 += "<input type='hidden' id='document4' class='docid' value=''/>";
                			txt5 += "<input type='hidden' id='docName4' class='docName' value=''/>";
                			txt5 += "<input type='hidden' id='content4' class='docContent' value=''/>";
                			txt5 += "<input type='hidden' id='contentType4' class='docContentType' value=''/>";
                			document.getElementById("demo4").innerHTML = txt5;
             		        document.getElementById("innerDel-5").style.display = "block";
             		        document.getElementById("show-5").style.display = "block";
             		        $('#document4').val(docID);
             		        $('#docName4').val(fileName);
             		        $('#content4').val(fileContent);
             		        $('#contentType4').val(contentType); 
            			} 
            			if (i == 5){
            				var txt6 = '';
                			txt6 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt6 += ": " + fileName + "<br>";
                			}
                			txt6 += "<input type='hidden' id='document5' class='docid' value=''/>";
                			txt6 += "<input type='hidden' id='docName5' class='docName' value=''/>";
                			txt6 += "<input type='hidden' id='content5' class='docContent' value=''/>";
                			txt6 += "<input type='hidden' id='contentType5' class='docContentType' value=''/>";
                			document.getElementById("demo5").innerHTML = txt6;
             		        document.getElementById("innerDel-6").style.display = "block";
             		        document.getElementById("show-6").style.display = "block";
             		        $('#document5').val(docID);
             		        $('#docName5').val(fileName);
             		        $('#content5').val(fileContent);
             		        $('#contentType5').val(contentType); 
            			}
            			if (i == 6){
            				var txt7 = '';
                			txt7 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt7 += ": " + fileName + "<br>";
                			}
                			txt7 += "<input type='hidden' id='document6' class='docid' value=''/>";
                			txt7 += "<input type='hidden' id='docName6' class='docName' value=''/>";
                			txt7 += "<input type='hidden' id='content6' class='docContent' value=''/>";
                			txt7 += "<input type='hidden' id='contentType6' class='docContentType' value=''/>";
                			document.getElementById("demo6").innerHTML = txt7;
             		        document.getElementById("innerDel-7").style.display = "block";
             		        document.getElementById("show-7").style.display = "block";
             		        $('#document6').val(docID);
             		        $('#docName6').val(fileName);
             		        $('#content6').val(fileContent);
             		        $('#contentType6').val(contentType); 
            			} 
            			if (i == 7){
            				var txt8 = '';
                			txt8 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt8 += ": " + fileName + "<br>";
                			}
                			txt8 += "<input type='hidden' id='document7' class='docid' value=''/>";
                			txt8 += "<input type='hidden' id='docName7' class='docName' value=''/>";
                			txt8 += "<input type='hidden' id='content7' class='docContent' value=''/>";
                			txt8 += "<input type='hidden' id='contentType7' class='docContentType' value=''/>";
                			document.getElementById("demo7").innerHTML = txt8;
             		        document.getElementById("innerDel-8").style.display = "block";
             		        document.getElementById("show-8").style.display = "block";
             		        $('#document7').val(docID);
             		        $('#docName7').val(fileName);
             		        $('#content7').val(fileContent);
             		        $('#contentType7').val(contentType); 
            			}
            			if (i == 8){
            				var txt9 = '';
                			txt9 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt9 += ": " + fileName + "<br>";
                			}
                			txt9 += "<input type='hidden' id='document8' class='docid' value=''/>";
                			txt9 += "<input type='hidden' id='docName8' class='docName' value=''/>";
                			txt9 += "<input type='hidden' id='content8' class='docContent' value=''/>";
                			txt9 += "<input type='hidden' id='contentType8' class='docContentType' value=''/>";
                			document.getElementById("demo8").innerHTML = txt9;
             		        document.getElementById("innerDel-9").style.display = "block";
             		        document.getElementById("show-9").style.display = "block";
             		        $('#document8').val(docID);
             		        $('#docName8').val(fileName);
             		        $('#content8').val(fileContent);
             		        $('#contentType8').val(contentType); 
            			}
            			if (i == 9){
            				var txt10 = '';
                			txt10 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt10 += ": " + fileName + "<br>";
                			}
                			txt10 += "<input type='hidden' id='document8' class='docid' value=''/>";
                			txt10 += "<input type='hidden' id='docName8' class='docName' value=''/>";
                			txt10 += "<input type='hidden' id='content8' class='docContent' value=''/>";
                			txt10 += "<input type='hidden' id='contentType8' class='docContentType' value=''/>";
                			document.getElementById("demo9").innerHTML = txt10;
             		        document.getElementById("innerDel-10").style.display = "block";
             		        document.getElementById("show-10").style.display = "block";
             		        $('#document9').val(docID);
             		        $('#docName9').val(fileName);
             		        $('#content9').val(fileContent);
             		        $('#contentType9').val(contentType); 
            			} 
            		}
        		} else {
        			var docID = xmlDocumentData.result.documentUuid;
        			var contentType = xmlDocumentData.result.contentType;
        			var fileName = xmlDocumentData.result.fileName;
        			var fileContent = xmlDocumentData.result.file;
        			var txt1 = '';
        			txt1 += "<strong>" + (1) + "</strong>";
        			if (fileName){
        				txt1 += ": " + fileName + "<br>";
        			}
        			txt1 += "<input type='hidden' id='document' class='docid' value=''/>";
        			txt1 += "<input type='hidden' id='docName' class='docName' value=''/>";
        			txt1 += "<input type='hidden' id='content' class='docContent' value=''/>";
        			txt1 += "<input type='hidden' id='contentType' class='docContentType' value=''/>";
        			document.getElementById("demo").innerHTML = txt1;
     		        document.getElementById("innerDel").style.display = "block";
     		        document.getElementById("show-1").style.display = "block";
     		        $('#document').val(docID);
     		        $('#docName').val(fileName);
     		        $('#content').val(fileContent);
     		        $('#contentType').val(contentType); 
        		}
        	}
        },
        fetchFilef1: function(thisModel, xmlDocumentData, status){
        	//console.log('fetchAttachedDocuments '+xmlDocumentData);
        	var docID = [];
        	var fileContent = [];
        	var fileName = [];
        	var contentType = [];
        	console.log(xmlDocumentData);
        	if (xmlDocumentData){
        		var resultCount = xmlDocumentData.result.length;
        		console.log(resultCount);
        		if (resultCount){
        			for (var i = 0; i < resultCount; i++){
            			var docID = xmlDocumentData.result[i].documentUuid;
            			var fileContent = xmlDocumentData.result[i].file;
            			var fileName = xmlDocumentData.result[i].fileName;
            			var contentType = xmlDocumentData.result[i].contentType;
            			if (i == 0){
            				var txt1 = '';
                			txt1 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt1 += ": " + fileName + "<br>";
                			}
                			txt1 += "<input type='hidden' id='documentf1' class='docid' value=''/>";
                			txt1 += "<input type='hidden' id='docNamef1' class='docName' value=''/>";
                			txt1 += "<input type='hidden' id='contentf1' class='docContent' value=''/>";
                			txt1 += "<input type='hidden' id='contentTypef1' class='docContentType' value=''/>";
                			document.getElementById("demof1").innerHTML = txt1;
             		        document.getElementById("innerDelf1").style.display = "block";
             		        document.getElementById("show-1f1").style.display = "block";
             		        $('#documentf1').val(docID);
             		        $('#docNamef1').val(fileName);
             		        $('#contentf1').val(fileContent);
             		        $('#contentTypef1').val(contentType);   
            			}
            			if (i == 1){
            				var txt2 = '';
                			txt2 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt2 += ": " + fileName + "<br>";
                			}
                			txt2 += "<input type='hidden' id='document1f1' class='docid' value=''/>";
                			txt2 += "<input type='hidden' id='docName1f1' class='docName' value=''/>";
                			txt2 += "<input type='hidden' id='content1f1' class='docContent' value=''/>";
                			txt2 += "<input type='hidden' id='contentType1f1' class='docContentType' value=''/>";
                			document.getElementById("demo1f1").innerHTML = txt2;
             		        document.getElementById("innerDel-2f1").style.display = "block";
             		        document.getElementById("show-2f1").style.display = "block";
             		        $('#document1f1').val(docID);
             		        $('#docName1f1').val(fileName);
             		        $('#content1f1').val(fileContent);
             		        $('#contentType1f1').val(contentType); 
            			}
            			if (i == 2){
            				var txt3 = '';
                			txt3 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt3 += ": " + fileName + "<br>";
                			}
                			txt3 += "<input type='hidden' id='document2f1' class='docid' value=''/>";
                			txt3 += "<input type='hidden' id='docName2f1' class='docName' value=''/>";
                			txt3 += "<input type='hidden' id='content2f1' class='docContent' value=''/>";
                			txt3 += "<input type='hidden' id='contentType2f1' class='docContentType' value=''/>";
                			document.getElementById("demo2f1").innerHTML = txt3;
             		        document.getElementById("innerDel-3f1").style.display = "block";
             		        document.getElementById("show-3f1").style.display = "block";
             		        $('#document2f1').val(docID);
             		        $('#docName2f1').val(fileName);
             		        $('#content2f1').val(fileContent);
             		        $('#contentType2f1').val(contentType); 
            			}
            			if (i == 3){
            				var txt4 = '';
                			txt4 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt4 += ": " + fileName + "<br>";
                			}
                			txt4 += "<input type='hidden' id='document3f1' class='docid' value=''/>";
                			txt4 += "<input type='hidden' id='docName3f1' class='docName' value=''/>";
                			txt4 += "<input type='hidden' id='content3f1' class='docContent' value=''/>";
                			txt4 += "<input type='hidden' id='contentType3f1' class='docContentType' value=''/>";
                			document.getElementById("demo3f1").innerHTML = txt4;
             		        document.getElementById("innerDel-4f1").style.display = "block";
             		        document.getElementById("show-4f1").style.display = "block";
             		        $('#document3f1').val(docID);
             		        $('#docName3f1').val(fileName);
             		        $('#content3f1').val(fileContent);
             		        $('#contentType3f1').val(contentType); 
            			}
            			if (i == 4){
            				var txt5 = '';
                			txt5 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt5 += ": " + fileName + "<br>";
                			}
                			txt5 += "<input type='hidden' id='document4f1' class='docid' value=''/>";
                			txt5 += "<input type='hidden' id='docName4f1' class='docName' value=''/>";
                			txt5 += "<input type='hidden' id='content4f1' class='docContent' value=''/>";
                			txt5 += "<input type='hidden' id='contentType4f1' class='docContentType' value=''/>";
                			document.getElementById("demo4f1").innerHTML = txt5;
             		        document.getElementById("innerDel-5f1").style.display = "block";
             		        document.getElementById("show-5f1").style.display = "block";
             		        $('#document4f1').val(docID);
             		        $('#docName4f1').val(fileName);
             		        $('#content4f1').val(fileContent);
             		        $('#contentType4f1').val(contentType); 
            			} 
            			if (i == 5){
            				var txt6 = '';
                			txt6 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt6 += ": " + fileName + "<br>";
                			}
                			txt6 += "<input type='hidden' id='document5f1' class='docid' value=''/>";
                			txt6 += "<input type='hidden' id='docName5f1' class='docName' value=''/>";
                			txt6 += "<input type='hidden' id='content5f1' class='docContent' value=''/>";
                			txt6 += "<input type='hidden' id='contentType5f1' class='docContentType' value=''/>";
                			document.getElementById("demo5f1").innerHTML = txt6;
             		        document.getElementById("innerDel-6f1").style.display = "block";
             		        document.getElementById("show-6f1").style.display = "block";
             		        $('#document5f1').val(docID);
             		        $('#docName5f1').val(fileName);
             		        $('#content5f1').val(fileContent);
             		        $('#contentType5f1').val(contentType); 
            			}
            			if (i == 6){
            				var txt7 = '';
                			txt7 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt7 += ": " + fileName + "<br>";
                			}
                			txt7 += "<input type='hidden' id='document6f1' class='docid' value=''/>";
                			txt7 += "<input type='hidden' id='docName6f1' class='docName' value=''/>";
                			txt7 += "<input type='hidden' id='content6f1' class='docContent' value=''/>";
                			txt7 += "<input type='hidden' id='contentType6f1' class='docContentType' value=''/>";
                			document.getElementById("demo6f1").innerHTML = txt7;
             		        document.getElementById("innerDel-7f1").style.display = "block";
             		        document.getElementById("show-7f1").style.display = "block";
             		        $('#document6f1').val(docID);
             		        $('#docName6f1').val(fileName);
             		        $('#content6f1').val(fileContent);
             		        $('#contentType6f1').val(contentType); 
            			} 
            			if (i == 7){
            				var txt8 = '';
                			txt8 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt8 += ": " + fileName + "<br>";
                			}
                			txt8 += "<input type='hidden' id='document7f1' class='docid' value=''/>";
                			txt8 += "<input type='hidden' id='docName7f1' class='docName' value=''/>";
                			txt8 += "<input type='hidden' id='content7f1' class='docContent' value=''/>";
                			txt8 += "<input type='hidden' id='contentType7f1' class='docContentType' value=''/>";
                			document.getElementById("demo7f1").innerHTML = txt8;
             		        document.getElementById("innerDel-8f1").style.display = "block";
             		        document.getElementById("show-8f1").style.display = "block";
             		        $('#document7f1').val(docID);
             		        $('#docName7f1').val(fileName);
             		        $('#content7f1').val(fileContent);
             		        $('#contentType7f1').val(contentType); 
            			}
            			if (i == 8){
            				var txt9 = '';
                			txt9 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt9 += ": " + fileName + "<br>";
                			}
                			txt9 += "<input type='hidden' id='document8f1' class='docid' value=''/>";
                			txt9 += "<input type='hidden' id='docName8f1' class='docName' value=''/>";
                			txt9 += "<input type='hidden' id='content8f1' class='docContent' value=''/>";
                			txt9 += "<input type='hidden' id='contentType8f1' class='docContentType' value=''/>";
                			document.getElementById("demo8f1").innerHTML = txt9;
             		        document.getElementById("innerDel-9f1").style.display = "block";
             		        document.getElementById("show-9f1").style.display = "block";
             		        $('#document8f1').val(docID);
             		        $('#docName8f1').val(fileName);
             		        $('#content8f1').val(fileContent);
             		        $('#contentType8f1').val(contentType); 
            			}
            			if (i == 9){
            				var txt10 = '';
                			txt10 += "<strong>" + (i+1) + "</strong>";
                			if (fileName){
                				txt10 += ": " + fileName + "<br>";
                			}
                			txt10 += "<input type='hidden' id='document8f1' class='docid' value=''/>";
                			txt10 += "<input type='hidden' id='docName8f1' class='docName' value=''/>";
                			txt10 += "<input type='hidden' id='content8f1' class='docContent' value=''/>";
                			txt10 += "<input type='hidden' id='contentType8f1' class='docContentType' value=''/>";
                			document.getElementById("demo9f1").innerHTML = txt10;
             		        document.getElementById("innerDel-10f1").style.display = "block";
             		        document.getElementById("show-10f1").style.display = "block";
             		        $('#document9f1').val(docID);
             		        $('#docName9f1').val(fileName);
             		        $('#content9f1').val(fileContent);
             		        $('#contentType9f1').val(contentType);
            			}
        			}	
        		} else {
        			var docID = xmlDocumentData.result.documentUuid;
        			var contentType = xmlDocumentData.result.contentType;
        			var fileContent = xmlDocumentData.result.file;
        			var fileName = xmlDocumentData.result.fileName;
        			var txt1 = '';
        			txt1 += "<strong>" + (1) + "</strong>";
        			if (fileName){
        				txt1 += ": " + fileName + "<br>";
        			}
        			txt1 += "<input type='hidden' id='documentf1' class='docid' value=''/>";
        			txt1 += "<input type='hidden' id='docNamef1' class='docName' value=''/>";
        			txt1 += "<input type='hidden' id='contentf1' class='docContent' value=''/>";
        			txt1 += "<input type='hidden' id='contentTypef1' class='docContentType' value=''/>";
        			document.getElementById("demof1").innerHTML = txt1;
     		        document.getElementById("innerDelf1").style.display = "block";
     		        document.getElementById("show-1f1").style.display = "block";
     		        $('#documentf1').val(docID);
     		        $('#docNamef1').val(fileName);
     		        $('#contentf1').val(fileContent);
     		        $('#contentTypef1').val(contentType); 
        		}
        	}
        }
    });
    return FormDocument_Model;
});