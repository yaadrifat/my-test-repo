define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    'ValidationCueModel',
    '../SourceFieldModel',
    '../TargetFieldModel'
], function(global, $, _, Backbone, ValidationCueModel, SourceFieldModel, TargetFieldModel) {
	var stemConfig ='';
	var Stem1_Model = Backbone.Model.extend({
        initialize: function(obj){
        	//alert(global.loggedInUser + global.mainTarget);
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	stemConfig = stemModel.toJSON();
        	var defGroupJSObj;

            switch(global.mainTarget){
	    		case 'editMultiEvent' : 
	    		case 'markDone' :
	    			defGroupJSObj = $('#isqLogUsrDefGroup', opener.document).val();
	    			break;
	    		case 'editVisit':
	    			defGroupJSObj = $('#isqLogUsrDefGroup').val();
				    break;
				default:
					return;
            }
            defGroupJSObj = JSON.parse(defGroupJSObj);
            //console.log(Object.keys(defGroupJSObj).length);
            if (!defGroupJSObj || Object.keys(defGroupJSObj).length <= 0){
		    	return;
		    }
		    //console.log(global.mainTarget+ ' ' + defGroupJSObj);
            //alert(defGroupJSObj.groupName);
		    //alert(global.params.loggedInUser + ' ' +memberPK);
		
		    if (defGroupJSObj.groupName != 'Admin'){
		    	var validationCueModel = new ValidationCueModel(stemConfig);
	            var source = validationCueModel.get('source');
	            var sourceFldModel = new SourceFieldModel(source);
	            var sourceSelector = sourceFldModel.findField();
	   		    //console.log('sourceSelector '+ sourceSelector);
	            var sourceName = sourceFldModel.getName();
			    var conditions = sourceFldModel.get("el")["conditions"];
	            var condition = conditions[0];
	            //console.log('condition '+JSON.stringify(condition));

	            var targets = validationCueModel.get("targets");
	            if (!targets || targets.length <= 0){ return; }
	
	            switch(global.mainTarget){
	            	
	        		case 'editMultiEvent' : 
		               for (var i = 0; i < targets.length; i++) {
		                    var target = targets[i];
		                    //console.log(target);
		                    var targetFldModel = new TargetFieldModel(target);
		                    //console.log(targetFldModel);
		                    var targetSelector = targetFldModel.findField();
	                		//console.log('targetSelector '+ targetSelector);
		                    var targetName = targetFldModel.getName();
		                    var skipValues = targetFldModel.get("el")["skipValues"];

	                    	$(sourceSelector).bind("change", function (event) {
	                    		var index = $(sourceSelector).index(this);

	                    		var val = (!$(this).val())? 'A' : val;
	                    		if ($(this).val() != 'U'){
	                    			var ddSelectedValue = $(this).parent().siblings().find(targetSelector).find("option:selected").html();
	                    			//alert(ddSelectedValue + ' ' + skipValues.indexOf(ddSelectedValue));
		                    		if (skipValues && skipValues.indexOf(ddSelectedValue) > -1){
		                    			return;
		                    		}

	                    			$($(targetSelector).get(index)).attr("disabled", true);
		                    		
	                    			$($(targetSelector).get(index)).focus(function(){
	                    				$($(targetSelector).get(index)).attr("disabled", true);
		                 			});
	                    			$($(targetSelector).get(index)).css( "background-color", 'lightgrey' );
	                    		}
	                 		});
		                 	$(sourceSelector).change();	
		               };
		               break;
	        		case 'markDone' : 
		               for (var i = 0; i < targets.length; i++) {
		                    var target = targets[i];
		                    //console.log(target);
		                    var targetFldModel = new TargetFieldModel(target);
		                    //console.log(targetFldModel);
		                    var targetSelector = targetFldModel.findField();
	                		//console.log('targetSelector '+ targetSelector);
		                    var targetName = targetFldModel.getName();
		                    var skipValues = targetFldModel.get("el")["skipValues"];

	                    	$(sourceSelector).bind("change", function (event) {
	                    		var index = $(sourceSelector).index(this);

	                    		var val = (!$(this).val())? 'A' : val;
	                    		if ($(this).val() != 'U'){
	                    			var ddSelectedValue = $(this).parent("form").find(targetSelector).find("option:selected").html();
	                    			//alert(ddSelectedValue + ' ' + skipValues.indexOf(ddSelectedValue));
		                    		if (skipValues && skipValues.indexOf(ddSelectedValue) > -1){
		                    			return;
		                    		}

	                    			$($(targetSelector).get(index)).attr("disabled", true);
		                    		
	                    			$($(targetSelector).get(index)).focus(function(){
	                    				$($(targetSelector).get(index)).attr("disabled", true);
		                 			});
	                    			$($(targetSelector).get(index)).css( "background-color", 'lightgrey' );
	                    		}
	                 		});
		                 	$(sourceSelector).change();	
		               };
		               break;
	        		case 'editVisit':
	        			for (var i = 0; i < targets.length; i++) {
		                    var target = targets[i];
		                    //console.log(target);
		                    var targetFldModel = new TargetFieldModel(target);
		                    //console.log(targetFldModel);
		                    var targetName = targetFldModel.getName();
		                    //console.log('targetName '+ targetName);
		                    var skipValues = targetFldModel.get("el")["skipValues"];  
		                    
	                    	$(sourceSelector).bind("change", function (event) {
	                    		var index = (this.name).substring(sourceName.length, (this.name).length);
	                    		//alert('check point!');
	                    		var sourceSelector = '[name="'+sourceName+index+'"]';
	                    		if ($(sourceSelector).val() != 'U'){
		                    		var targetSelector = '[name="'+targetName+index+'"]';
		                    		var ddSelectedValue = $(targetSelector).find("option:selected").html();
		                    		//alert(ddSelectedValue + ' ' + skipValues.indexOf(ddSelectedValue));

		                    		if (skipValues && skipValues.indexOf(ddSelectedValue) > -1){
		                    			return;
		                    		}
		                    		$(targetSelector).attr("disabled", true);
		                    		
		                    		$(targetSelector).focus(function(){
		                 				$(targetSelector).attr("disabled", true);
		                 			});
		    	        			$(targetSelector).css( "background-color", 'lightgrey' );
	                    		}
	                 		});
	                 		$(sourceSelector).change();
		               };
		               return false;
		               break;
	        	    default:
				    	break;
               	}
		    }
        }
	});
    return Stem1_Model;
});