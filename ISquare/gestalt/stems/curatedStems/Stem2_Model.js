define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/StudyWSClientModel',
    '../WSClient/FormResponseWSClientModel',
    '../WSClient/UserWSClientModel',
    '../../util/email/EmailModel'
], function(global, $, _, Backbone, StudyWSClientModel, FormResponseWSClientModel, UserWSClientModel, EmailModel) {
    var Stem2_Model = Backbone.Model.extend({
    	that: {},
    	studyPK: 0,
    	studyNumber: "",
    	studyTitle: "",
    	pi: "",
    	uiMessage: '',
        initialize: function(){
        	//that = stemModel;
            //alert('Stem2_Model initialized!');
        },
        do: function(args){
        	var stemModel = args.stemModel, studyPK = global.params.studyPK; 
        	stemModel.studyPK = studyPK;
        	var studyWSClientModel = new StudyWSClientModel();
        	studyWSClientModel.getStudyTeamMembers(stemModel, {studyPK: studyPK}, stemModel.findStudyTeamMembers);
        },
        findStudyTeamMembers: function(thisModel, xmlStudyData, status){
	    	//console.log(xmlStudyData.responseXML);
	    	$(xmlStudyData.responseXML)
		    	.find('StudyTeamMembers').find('studyTeamMember').each(function()
		    { 
		    	var teamRole = $(this).find('teamRole').find('code').text();
		    	var userID = $(this).find('userId').find('PK').text();
		    	var teamRoles = teamRole.split(" ");
		    	var userIDs = userID.split(" ");
		    	//alert('2: userID'+userID);
		    	//alert('2: teamRole'+teamRole);
		    	if (thisModel.teamRole){
		    		var teamRoleInString = thisModel.teamRole;
		    		var teamRoleInArray = teamRoleInString.split(" ");
		    		var updateTeamRoleInArray = teamRoleInArray.concat(teamRoles);//array
		    		var teamRole = updateTeamRoleInArray.toString();
		    		//alert('4.teamRole'+teamRole);
		    		thisModel.teamRole = teamRole;
		    	} else {
		    			thisModel.teamRole = teamRole;
		    			//alert('3.teamRole'+thisModel.teamRole);
		    	}
		    	if (thisModel.userID){
			    		var userIDInString = thisModel.userID;//string
			    		var userIDInArray = userIDInString.split(" ");
			    		var updateUserIDInArray = userIDInArray.concat(userIDs);//array
			    		var userID = updateUserIDInArray.toString();
			    		//alert('4.userID'+userID);
			    		thisModel.userID = userID;
			    } else {
			    	thisModel.userID = userID;
			    	//alert('3.userID'+thisModel.userID);
			    }
		    }
			);    
	    	var studyWSClientModel = new StudyWSClientModel();
			studyWSClientModel.getStudySummary(thisModel, {
				studyPK: global.params.studyPK,
			}, thisModel.process);		     
	    },
        process: function(thisModel, xmlStudyData, status){
			thisModel.findStudySpecifics(thisModel, xmlStudyData, status);
			thisModel.processMSD(thisModel, xmlStudyData, status);
        },
        findStudySpecifics: function(thisModel, xmlStudyData, status){
		    var studyNumber = $(xmlStudyData.responseXML).find('StudySummary').find('parentIdentifier').find('studyNumber').text();
		    thisModel.studyNumber = studyNumber;
		    
		    var studyTitle = $(xmlStudyData.responseXML).find('StudySummary').find('studyTitle').text();
		    thisModel.studyTitle = studyTitle;
		    
		    var pi = $(xmlStudyData.responseXML).find('StudySummary').find('principalInvestigator');
        	var fname = pi.find("firstName").text(), lname = pi.find("lastName").text();
		    thisModel.pi = fname + " " + lname;
        },
        processMSD: function(thisModel, xmlStudyData, status){
			var spnsrdProt, prot_ready, nom_reviewers, dept_chairs, biostat;
			
			 $(xmlStudyData.responseXML)
			    .find('StudySummary').find('moreStudyDetails').each(function()
			 {
			    var key = $(this).find('key').text();
			    if (key == 'spnsrdProt'){
			    	spnsrdProt = $(this).find('value').text();
					   
				    if (spnsrdProt != 'MDACCInvIni' && spnsrdProt != 'indstrStdy' ){
				    	return false;
				    }
			    }
			    if (key == 'prot_ready'){
				    prot_ready = $(this).find('value').text();
				    if (prot_ready != 'Y'){
				    	return false;
				    }
			    }
			    if (key == 'nom_rev_id'){
				    nom_reviewers = $(this).find('value').text();
			    }
			    
			    if (key == 'dept_chairs_ids'){
			    	dept_chairs = $(this).find('value').text();
			    }
			 });

			 if ((spnsrdProt != 'MDACCInvIni' && spnsrdProt != 'indstrStdy') || prot_ready != 'Y'){
		    	return false;
		     }	
			 
			 if (spnsrdProt == 'MDACCInvIni' && prot_ready == 'Y'){
				 if (thisModel.userID && thisModel.teamRole){
					 var formResponseWSClientModel = new FormResponseWSClientModel();
					 formResponseWSClientModel.getListofStudyFormResponses(thisModel, {
						 studyPK: global.params.studyPK,
						 formPK: (thisModel.get('settings')).BioStat.bioStaFormPK
					 }, thisModel.checkBioStatSignOffAndRole);
				 } 
			 }
			 
			 thisModel.processIND(thisModel, xmlStudyData, status);
			 
			 if (thisModel.userID && thisModel.teamRole){
				 var formResponseWSClientModel = new FormResponseWSClientModel();
				 formResponseWSClientModel.getListofStudyFormResponses(thisModel, {
					 studyPK: global.params.studyPK,
					 formPK: (thisModel.get('settings')).Collaborators.collabFormPK
				 }, thisModel.checkCollabSignOffAndRole);
			 }	
             if (nom_reviewers && nom_reviewers.length > 0){
            	 thisModel.nom_reviewers = nom_reviewers;
            	 //alert((thisModel.get('settings')).NomReview.nomRevFormPK);
            	 var formResponseWSClientModel = new FormResponseWSClientModel();
            	 formResponseWSClientModel.getListofStudyFormResponses(thisModel, {
            		 studyPK: global.params.studyPK,
            		 formPK: (thisModel.get('settings')).NomReview.nomRevFormPK
            	 }, thisModel.checkNomRevSignOff);
		     }
			 if (dept_chairs && dept_chairs.length > 0){
				 thisModel.dept_chairs = dept_chairs;
				 //alert((thisModel.get('settings')).DepChair.depChaFormPK);
				 var formResponseWSClientModel = new FormResponseWSClientModel();
				 formResponseWSClientModel.getListofStudyFormResponses(thisModel, {
					 studyPK: global.params.studyPK,
					 formPK: (thisModel.get('settings')).DepChair.depChaFormPK
				 }, thisModel.checkDepCharSignOff);   
		     }
	    },
        checkNomRevSignOff: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		//console.log(xmlFormResponseData.responseXML);
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		//alert(formResponseCount);
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}
		    	if (sendEmail){
	            	var nom_reviewers = thisModel.nom_reviewers;
	            	var logins = nom_reviewers.split(",");		     	
	            	for (var userIndx = 0; userIndx < logins.length; userIndx++){
	 				   var login = logins[userIndx];
	 				   var params = {studyPK:'', fname:'', lname:'', userPK:'', login:login};
	 				   var userWSClientModel = new UserWSClientModel();
	 				   userWSClientModel.searchUser(thisModel, params, thisModel.sendEmailToNomReviewer);
	 			   }
		    	}
        	}
	    },
	    checkDepCharSignOff: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		//console.log(xmlFormResponseData.responseXML);
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}        		
		    	if (sendEmail){
	            	var dept_chairs = thisModel.dept_chairs;
	            	var logins = dept_chairs.split(",");		     	
	            	for (var userIndx = 0; userIndx < logins.length; userIndx++){
	 				   var login = logins[userIndx];
	 				   var params = {studyPK:'', fname:'', lname:'', userPK:'', login:login};
	 				   var userWSClientModel = new UserWSClientModel();
	 				   userWSClientModel.searchUser(thisModel, params, thisModel.sendEmailToDepChair);
	 			   }
		    	}
        	}
	    },
	    checkCollabSignOffAndRole: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		//console.log(xmlFormResponseData.responseXML);
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		//alert('formResponseCount'+formResponseCount);
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}
		    	if (sendEmail){
		    		var teamRole = thisModel.teamRole;
			    	var teamRoles = teamRole.split(",");
			    	var userID = thisModel.userID;
			    	var userIDs = userID.split(",");
			    	for (var userIndx = 0; userIndx < teamRoles.length; userIndx++){
			    		if (teamRoles[userIndx] == 'Collaborators'){
			    			var userPK = userIDs[userIndx];
			    			var params = {studyPK:'', fname:'', lname:'', userPK: userPK, login:''};
			    			var userWSClientModel = new UserWSClientModel();
			    			userWSClientModel.searchUser(thisModel, params, thisModel.sendEmailToCollaborators);
			    		}
			    	}
		    	}
        	}
	    },
	    checkBioStatSignOffAndRole: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}        
		    	if (sendEmail){     	
	            	var teamRole = thisModel.teamRole;
			    	var teamRoles = teamRole.split(",");
			    	var userID = thisModel.userID;
			    	var userIDs = userID.split(",");
			    	for (var userIndx = 0; userIndx < teamRoles.length; userIndx++){
			    		if (teamRoles[userIndx] == 'role_126'){
			    			var userPK = userIDs[userIndx];
			    			var params = {studyPK:'', fname:'', lname:'', userPK: userPK, login:''};
			    			var userWSClientModel = new UserWSClientModel();
			    			userWSClientModel.searchUser(thisModel, params, thisModel.sendEmailToBioStatistician);
			    		}
	 			   }
		    	}
        	}
	    },
	    checkIndSignOff: function(thisModel, xmlFormResponseData, status){
        	var sendEmail = false;
        	if (xmlFormResponseData){
        		/* We expect a maximum of 1 form response to be returned by the web service call as the form is designed to be single entry/ edit only form.*/
        		var formResponseCount = $(xmlFormResponseData.responseXML).find('studyFormResponses').length;
        		if (formResponseCount < 1){
        			sendEmail = true;
        		} else {
			    	$(xmlFormResponseData.responseXML)
			    	.find('studyFormResponses').each(function()
			   		{
			            var formStatusCodeSubType = $(this).find('formStatus').find('code').text();
			            if (formStatusCodeSubType != 'complete'){
			            	sendEmail = true;
			            }
			   		});
        		}
		    	if (sendEmail){
		    		thisModel.sendEmailToINDPreReviewer(thisModel);
		    	}
        	}
	    },
	    sendEmailToNomReviewer: function(thisModel, xmlUserData, status){
	    	var emailSettings = (thisModel.get("settings"))["NomReview"]; 
	    	if (!emailSettings) return;

	    	var subject = emailSettings["subject"];
	    	var text = emailSettings["text"];
	    	
	    	if (xmlUserData){
		    	$(xmlUserData.responseXML)
		    	.find('UserSearchResults').find('user').each(function()
		   		{
					var userEmail = $(this).find('email').text();
					var userName = $(this).find('firstName').text() + ' ' + $(this).find('lastName').text();

					if (userEmail && userName){
						var emailObj = {
							recipients:[{Name:userName, email:userEmail}], 
							subject: subject, 
							text: text,
							successMessage: "Email sent successfully to Nominated Reviewer",
							errorMessage: "Email Error - Nominated Reviewer"
						};
						thisModel.sendEmailNoSubstitution(emailObj);
					}
		   		});	    		
	    	}
	    	return false;
	    },
	    sendEmailToDepChair: function(thisModel, xmlUserData, status){
	    	var emailSettings = (thisModel.get("settings"))["DepChair"]; 
	    	if (!emailSettings) return;

	    	emailSettings["emailParams"]={
	    		//subjectParams: ["1"],
	    		textParams : [thisModel.pi, thisModel.studyNumber, thisModel.studyTitle,''+thisModel.studyPK]
	    	};
	    	
	    	var subject = emailSettings["subject"];
	    	var text = emailSettings["text"];
	    	
	    	if (xmlUserData){
		    	$(xmlUserData.responseXML)
		    	.find('UserSearchResults').find('user').each(function()
		   		{
					var userEmail = $(this).find('email').text();
					var userName = $(this).find('firstName').text() + ' ' + $(this).find('lastName').text();

					if (userEmail && userName){
						var emailObj = {
							recipients:[{Name:userName, email:userEmail}], 
							subject: subject, 
							text: text,
							emailParams: emailSettings.emailParams,
							successMessage: "Email sent successfully to Department Chair",
							errorMessage: "Email Error - Department Chair"
						};
						thisModel.sendEmailWithSubstitution(emailObj);
					}
		   		});	    		
	    	}
	    	return false;
	    },
	    sendEmailToCollaborators: function(thisModel, xmlUserData, status){
	    	var emailSettings = (thisModel.get("settings"))["Collaborators"]; 
	    	if (!emailSettings) return;
	    	//alert('6: ');
	    	emailSettings["emailParams"]={
	    		//subjectParams: ["1"],
	    		textParams : [thisModel.studyNumber, thisModel.studyTitle,''+thisModel.studyPK]
	    	};
	    	
	    	var subject = emailSettings["subject"];
	    	var text = emailSettings["text"];
	    	
	    	if (xmlUserData){
		    	$(xmlUserData.responseXML)
		    	.find('UserSearchResults').find('user').each(function()
		   		{
					var userEmail = $(this).find('email').text();
					var userName = $(this).find('firstName').text() + ' ' + $(this).find('lastName').text();
	
					if (userEmail && userName){
						var emailObj = {
							recipients:[{Name:userName, email:userEmail}], 
							subject: subject, 
							text: text,
							emailParams: emailSettings.emailParams,
							successMessage: "Email sent successfully to Collaborators",
							errorMessage: "Email Error - Collaborators"
						};
						thisModel.sendEmailWithSubstitution(emailObj);
					}
		   		});	    		
	    	}
	    	return false;
	    },
	    sendEmailToBioStatistician: function(thisModel, xmlUserData, status){
	    	var emailSettings = (thisModel.get("settings"))["BioStat"]; 
	    	if (!emailSettings) return;

	    	emailSettings["emailParams"]={
	    		//subjectParams: ["1"],
	    		textParams : [thisModel.studyNumber, thisModel.studyTitle,''+thisModel.studyPK]
	    	};
	    	
	    	var subject = emailSettings["subject"];
	    	var text = emailSettings["text"];
	    	
	    	if (xmlUserData){
		    	$(xmlUserData.responseXML)
		    	.find('UserSearchResults').find('user').each(function()
		   		{
					var userEmail = $(this).find('email').text();
					var userName = $(this).find('firstName').text() + ' ' + $(this).find('lastName').text();

					if (userEmail && userName){
						var emailObj = {
							recipients:[{Name:userName, email:userEmail}], 
							subject: subject, 
							text: text,
							emailParams: emailSettings.emailParams,
							successMessage: "Email sent successfully to Biostatistician",
							errorMessage: "Email Error - Biostatistician"
						};
						thisModel.sendEmailWithSubstitution(emailObj);
					}
		   		});	    		
	    	}
	    	return false;
	    },
	    sendEmailToINDPreReviewer: function(thisModel){
	    	var emailSettings = (thisModel.get("settings"))["IND_PreReview"]; 
	    	if (!emailSettings || !emailSettings.recipients) return;
	    	var subject = emailSettings["subject"];  //alert('INDPreReviewer'+subject);
	    	var text = emailSettings["text"];
	    	var emailParams ={
	    		//subjectParams: ["1"],
	    		textParams : [thisModel.studyNumber, thisModel.studyTitle,''+thisModel.studyPK]
	    	};

	    	var emailObj = {
				recipients:emailSettings.recipients, 
				subject: subject, 
				text: text,
				emailParams: emailParams,
				successMessage: "Email sent successfully to IND Pre-reviewer",
				errorMessage: "Email Error - IND Pre-reviewer"
			};console.log('Sending email to INDPreReviewer');
			thisModel.sendEmailWithSubstitution(emailObj);
	    },
        processIND: function(thisModel, xmlStudyData, status){
			 $(xmlStudyData.responseXML)
			    .find('StudySummary').find('studyIndIdes').find('studyIndIde').each(function()
			 {
			    var holderType = $(this).find('holderType').find('description').text();
			    var indIdeType = $(this).find('indIdeType').text();
			    if (holderType == 'MDACC' && indIdeType == 'IND'){
			    	var formResponseWSClientModel = new FormResponseWSClientModel();
			        formResponseWSClientModel.getListofStudyFormResponses(thisModel, {
			            studyPK: global.params.studyPK,
			            formPK: (thisModel.get('settings')).IND_PreReview.indRevFormPK
			        }, thisModel.checkIndSignOff);
			    }
			 });
	    	 return false;
	    },
	    sendEmailWithSubstitution: function(emailObj){
	    	if (!emailObj) return;
	    	
	    	var emailModel = new EmailModel(emailObj);
			emailModel.substituteAndSendEmail();
	    },
	    sendEmailNoSubstitution: function(emailObj){
	    	if (!emailObj) return;
	    	
	    	var emailModel = new EmailModel();
	    	emailObj["rEmails"] = emailModel.processRecipients(emailObj["recipients"]);
			emailModel.sendEmail(emailObj);
	    }
    });
    return Stem2_Model;
});
