define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/TaylorWSClientModel'
], function(global, $, _, Backbone, TaylorWSClientModel) {
	var stemConfig ='';
    var AutoFormFilled_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	//alert(" isquare running...");    	    
     		    var html = '<div style="float:right;margin-right:113px;">\
    							<a href"#" id="link">Map Fields</a>\
    	        			</div>\
     		    			<div id="fabric" title="Field Mapping Table">\
     		    	            <div id="autofilldiv"></div>\
 		    	    			<embed id="gridviewer" frameborder="0" scrolling="no" width="1000" height="600" style="margin-left:30px; margin-right: auto;">\
     		    			</div>';
                var html2 = '<div><a href"#" id="autopop">Autofill</a><br>\</div>';
            	   var javascript = '<script lang="javascript">function pop(popDiv) {\
            		   popDiv.style.display = "block";\
            	}'; 
            	   var style2 = '<style>#autopop {\
                	   background-color: grey;\
                	   color: rgb(255, 255, 255);\
                       border-radius: 3px;\
                	   border: none;\
                	   padding-left: 9px;\
                       padding-right: 8px;\
                       padding-top: 5px;\
                       padding-bottom: 5px;\
                	   display: none;\
                	   margin-left: 53px;\
                	   width: 48px;\
                	   }</style>';
                   var style = '<style>body {\
               	    width: 100%;\
           }\
               	   .custom-file-upload {\
               	    width: 450px;\
               	}\
                   .fileContainer {\
                	overflow: hidden;\
                	position: relative;\
                	width: 80px;\
               	    height: 20px;\
               	    display: block;\
               	    text-align: center;\
               	    padding-bottom: 5px;\
               	    position: relative;\
               	    border-radius: 5px;\
               	    line-height: 25px;\
               	    padding-left: 0px;\
               	    font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
               	    font-size: 12px;\
                	background-color: #72a3c2;\
               	    border: none;\
               	    float: left;\
               	    margin-left: 2px;\
               	}\
                	   .fileContainer [type=file] {\
                	    cursor: inherit;\
                	    display: block;\
                	    font-size: 999px;\
                	    filter: alpha(opacity=0);\
                	    min-height: 100%;\
                	    min-width: 100%;\
                	    opacity: 0;\
                	    position: absolute;\
                	    right: 0;\
                	    text-align: right;\
                	    top: 0;\
                	}\
                	   #link {\
               text-decoration: none;\
               padding-left: 9px;\
               padding-right: 8px;\
               padding-top: 5px;\
               padding-bottom: 5px;\
               background-color: #72a3c2;\
               color: rgb(255, 255, 255);\
               border-radius: 3px;\
               font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
               font-size: 11.5px;\
               float: right;\
               margin-top: 0;\
           }\
                	   a.disabled {\
                	   pointer-events: none;\
                	   cursor: default;\
                	}\
           .container-main {\
               display: none;\
               height: 100%;\
               width: 100%;\
               margin-top: 40px\
           }\
           #popUp {\
               position: absolute;\
               color: #000000;\
               background-color: #ffffff;\
               /* To align popup window at the center of screen*/\
               top: 50%;\
               left: 50%;\
               margin-top: -100px;\
               margin-left: -150px;\
               z-index: 9002;\
           }\
           .container-outer {\
               width: 600px;\
               height: 300px;\
               background-color: rgb(52, 58, 78);\
               border-radius: 10px;\
               position: relative;\
               margin: auto;\
           }\
           .container-inner {\
               width: 580px;\
               height: 280px;\
               background-color: rgb(239, 233, 229);\
               position: relative;\
               margin-left: auto;\
               margin-right: auto;\
               top: 10px;\
               border-radius: 10px;\
               font-family: "lucida sans", "lucida grande", lucida , arial, sans-serif;\
               overflow: auto;\
           }\
           /*table tr:nth-child(odd) {*/\
               /*background-color: rgb(102, 102, 102);*/\
           /*}*/\
           </style>';
                   
                  
                //Display document upload button for form level on client UI  
                $('.dynSection').text("");
                $('#er_def_date_01').attr("size", "20");
                $('#er_def_formstat').css("width", "147px");//170 in chrome
                $('#er_def_formstat').css("margin-left", "67px");
                $('#fdaStudy').next().next().find("tbody").find("tr").find("td").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form Version Number: 1");
                //$('override_count').prepend("<br><p>Form Version Number: 1</p>");
                
        		var button = html + style + javascript;
        		//Assign eResearch's context to hidden fields
        		//$("#"+stemModel.get('htmlFormId')).prepend(button);
                $('.dynSection').append(button);
                $("#autopop").blur();
        		//Function to View Document in form level
        		$("#link").on("click", function() {
        			var button2 = html2 + style2;
        			$('#autofilldiv').append(button2);
        			document.getElementById('autopop').style.display = 'block';
        			//$("#autopop").css("display", "block");
        			var sourceFile = 'http://12.0.122.155:9000/evaluation';//http://207.141.67.99:9000/
        			$("#gridviewer").attr("src",sourceFile);
     		        jQuery("#fabric").dialog({
     		          autoOpen: true,
     		          modal: true, width:1050, height:650,
     		          close: function() {
     		        	 $("#gridviewer").attr("src","");
     		        	 //$("#viewer").attr("type","");
     		        	 jQuery("#fabric").dialog("destroy");
     		          
     		          }
     		       });
     		       var weight = 100,
     		       height = 5.4,
     		       pulse = 7.6,
     		       systolic = 112;
     		       $("#autopop").on("click", function() {
     		    	   this.blur();
     		    	   $('#fldvelos_25273_30371').val(weight);
     		    	   $('#fldvelos_25274_30372').val(height);
     		    	   $('#fldvelos_25276_30374').val(pulse);
     		    	   $('#fldvelos_25275_30373').val(systolic);
     		       });
            	});	
        		
        }     
    });
    return AutoFormFilled_Model;
});