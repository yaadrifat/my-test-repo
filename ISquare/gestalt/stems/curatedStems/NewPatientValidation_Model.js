define([
    'global',
    'jquery',
    'underscore',
    'backbone',
    '../WSClient/PatientDemographicsWSClientModel'
], function(global, $, _, Backbone, PatientDemographicsWSClientModel) {
	var stemConfig ='';
    var NewPatientValidation_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
        	var stemModel = args.stemModel;
        	//alert(" isquare running...");
        	
        	//Hide the Specialty field on the patientdetailsquick.jsp and patientFacility.jsp page
        	$(".basetbl.outline.midAlign.bgcolorFix").find("tr:first").hide("fast");
        	$("input[name=txtSpeciality]").parent().parent().hide("fast");
        	
			//Make patFacilityID field read only and grey out the field
			$("input[name=patFacilityID]").attr('readonly', true);
			$("input[name=patFacilityID]").css("background-color", "lightgrey");  
			
        	//Hide the �Add New Patient� link under �Manage Patients� >> Study Patients page on the studypatients.jsp.
        	$("#enroll_links").find("tr td:nth-child(3)").hide("fast");
        	
        	//HTML for displaying alert message
        	var html = '<div id="alertMessage" title="Alert">\
                			<p id="alertText" style="display:none;">'+ stemModel.get('alertMsgContent') +'</p>\
        				</div>';
        	$("#isquare").prepend(html);
        	var callWebService = function (){
        		if (stemModel.get('mrnUniqueAcrossApp') == 'yes'){
        			if (stemModel.patFacilityID){
        				var subParams = [stemModel.get('espUrl')+'/webservices/patientdemographicsservice?wsdl',stemModel.get('username'),stemModel.get('password'), stemModel.patFacilityID];
        				var params = {"serviceParams":subParams};
            			var patientDemographicsWSClientModel = new PatientDemographicsWSClientModel();
            			patientDemographicsWSClientModel.searchPatientAcrossApplication(stemModel, params, stemModel.checkExistingPatient);
        			}
        		}
        		if (stemModel.get('mrnUniqueAcrossApp') == 'no'){
        			if (stemModel.patOrganization && stemModel.patFacilityID){
        				var subParams = [stemModel.get('espUrl')+'/webservices/patientdemographicsservice?wsdl',stemModel.get('username'),stemModel.get('password'), stemModel.patOrganization, stemModel.patFacilityID];
        				var params = {"serviceParams":subParams};
            			var patientDemographicsWSClientModel = new PatientDemographicsWSClientModel();
            			patientDemographicsWSClientModel.searchPatientAcrossOrganization(stemModel, params, stemModel.checkExistingPatient);
        			}
        		}	
        	};	     	
        	//Read field values when the page loads
        	var patOrganization = $("select[name=patorganization] option:selected").text();
        	var patFacilityID = $("input[name=patFacilityID]").val();
        	stemModel.patOrganization = patOrganization;
        	stemModel.patFacilityID	= patFacilityID;
        	callWebService();
        	var delay = function(){
        		var timer = 0;
        		return function(callback, ms){
        		clearTimeout (timer);
        		timer = setTimeout(callback, ms);
        		};
        	};
        	//check updated value from patFacilityID field
        	$("input[name=patFacilityID]").on("keyup keypress paste", function(){
        		delay({
        			stemModel.patFacilityID = $(this).val()
            		callWebService();
        		},1000);		
        	});
        	//check updated value from siteID field
        	$("select[name=patorganization]").on("change", function(){
        		stemModel.patOrganization = $("select[name=patorganization] option:selected").text();
        		if (stemModel.get('mrnUniqueAcrossApp') == 'no'){
        			callWebService(); //callWebService to check if it's unique across organization
        		}
        	});
        },
        checkExistingPatient: function(thisModel, xmlDocumentData, status){
        	//console.log(xmlDocumentData);
        	if (xmlDocumentData){
        		var patientCount = xmlDocumentData.PatientSearchResponse.totalCount;
            	if (patientCount == 1) {
            		var patientID = xmlDocumentData.PatientSearchResponse.patDataBean[0].patientIdentifier.patientId;
            		if (patientID == thisModel.patFacilityID){
            			//Enable Interruption
            			$("#alertText").css("display", "block");
                		jQuery("#alertMessage").dialog();
                    	$("#submit_btn").prop('disabled', true);
                    	$("#submit_btn").css("background-color", "rgb(211, 211, 211)");  
            		}
            	}
            	if (patientCount != 1 || patientID != thisModel.patFacilityID){
            		//Disable interruption
            		jQuery("#alertMessage").dialog("close");
            		$("#submit_btn").prop('disabled', false);
            		$("#submit_btn").css("background-color", "#72a3c2");
            	}
        	}
        }
    });
    return NewPatientValidation_Model;
});