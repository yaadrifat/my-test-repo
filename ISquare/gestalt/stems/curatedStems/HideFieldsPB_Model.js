define([
    'global',
    'jquery',
    'underscore',
    'backbone'
], function(global, $, _, Backbone) {
    var stemConfig ='';
    var HideFieldsPB_Model = Backbone.Model.extend({
        initialize: function(obj){
        },
        do: function(args){
            var stemModel = args.stemModel;
            //alert(" isquare running...");
            var hideByDefault = function (){
                //Hide specific sections
                $("#sectionHead2").hide("fast");
                $("#sectionHead3").hide("fast");
                $("#sectionHead4").hide("fast");
                //Hide specific fields
                $("#studyPhase").parent().parent().hide();
                $("#STUDY_INNTTYP").parent().parent().hide();
            }
            var logicToShowAccordionsAndFields = function (){
                if (stemModel.recordType && stemModel.studyResType){
                    if ((stemModel.recordType == stemModel.get('recordType')[0] || stemModel.recordType == stemModel.get('recordType')[1]) && stemModel.studyResType == stemModel.get('studyResType')){
                        $("#sectionHead2").show("fast");
                        $("#sectionHead4").show("fast");
                        if (stemModel.studyType == stemModel.get('studyType')){
                            $("#STUDY_INNTTYP").parent().parent().show();
                            if (stemModel.studyIntType == stemModel.get('studyIntType')[0] || stemModel.studyIntType == stemModel.get('studyIntType')[1]){
                                $("#studyPhase").parent().parent().show();
                                $("#sectionHead3").show("fast");
                            } else {
                                $("#sectionHead3").hide("fast");
                                $("#studyPhase").parent().parent().hide();
                            }
                        } else {
                            $("#sectionHead3").hide("fast");
                            $("#studyPhase").parent().parent().hide();
                            $("#STUDY_INNTTYP").parent().parent().hide();
                        }
                    } else {
                        hideByDefault();
                    }
                   }
            };            
            hideByDefault();
            var recordType = $("select[name=alternateId32607] option:selected").text();
            var studyResType = $("select[name=studyResType] option:selected").text();
            var studyType = $("select[name=studyType] option:selected").text();
            var studyIntType = $("select[name=alternateId32616] option:selected").text();
            stemModel.recordType = recordType;
            stemModel.studyResType = studyResType;
            stemModel.studyType = studyType;
            stemModel.studyIntType = studyIntType;
            //check updated value from recordType field
            $("select[name=alternateId32607]").on("change", function(){
                stemModel.recordType = $("select[name=alternateId32607] option:selected").text();
                //alert(stemModel.recordType);
                logicToShowAccordionsAndFields();
            });
            //check updated value from studyResType field
            $("select[name=studyResType]").on("change", function(){
                stemModel.studyResType = $("select[name=studyResType] option:selected").text();
                //alert(stemModel.studyResType);
                logicToShowAccordionsAndFields();
            });
            //check updated value from studyType field
            $("select[name=studyType]").on("change", function(){
                stemModel.studyType = $("select[name=studyType] option:selected").text();
                //alert(stemModel.studyType);
                logicToShowAccordionsAndFields();
            });
            //check updated value from studyIntType field
            $("select[name=alternateId32616]").on("change", function(){
                stemModel.studyIntType = $("select[name=alternateId32616] option:selected").text();
                //alert(stemModel.studyIntType);
                logicToShowAccordionsAndFields();
            });
        }
    });
    return HideFieldsPB_Model;
});