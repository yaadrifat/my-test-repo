module.exports = {
	isquareURL: "",
	isquarePort: 8000,
	isquareCreds: {
		name: "admin",
		pass: ""
	},
	SSL_Key: '',
	SSL_Certificate: '',
	appVersion:"POC_build#12",
	email:{
		service: "Gmail",
		emailCreds:{
			name: "ISquare Mailing System",
			user: "notifications@velos.com",
			pass: ""
		}
	},
	serving: {
		VelosER: {
			AppURLs: [""],
			WSCreds: {
				name: "",
				pass: ""
			},
			timeout: 60
		}
	},
	API: {
		Comaiba:{
			APIURLs: [""],
			Creds:{
				name: "",
				pass: ""
			}
		}
	},
	webservice: {
		Creds:{
			name: "",
			pass: ""
		}
	},
	Excel:{
		fileName: ""
	}
};
