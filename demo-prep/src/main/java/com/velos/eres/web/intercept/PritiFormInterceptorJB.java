package com.velos.eres.web.intercept;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.FormCompareUtilityDao;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.esch.service.util.StringUtil;

public class PritiFormInterceptorJB extends InterceptorJB {
	@Override
	public void handle(Object... args) {
		System.out.println("hello world2!");
		if (args == null || args.length < 1) { return; }
		HttpServletRequest request = (HttpServletRequest)args[0];
		HttpSession tSession = request.getSession(true);
		
		SessionMaint sessionmaint = new SessionMaint();
		if (!sessionmaint.isValidSession(tSession)){
			return;
		}

		String userId = (String)tSession.getAttribute("userId");
		System.out.println("userId="+tSession.getAttribute("userId"));

		String formId = "";
		String formPullDown = request.getParameter("formPullDown");
		if (StringUtil.isEmpty(formPullDown)){
			return;
		} else {
		 	StringTokenizer strTokenizer = new StringTokenizer(formPullDown,"*");
 	     	formId = strTokenizer.nextToken();
 	     	//entryChar = strTokenizer.nextToken();
			//numEntries = EJBUtil.stringToNum(strTokenizer.nextToken());
		}
		System.out.println("formPullDown="+formPullDown);
		
		if (StringUtil.isEmpty(formId)){
			tSession.setAttribute("infoHashMap", null);
			return;
		} else {
			String advanceToThisForm = (String)request.getParameter("advanceToThisForm");
			if (StringUtil.stringToNum(advanceToThisForm) > 0){
				HashMap<String, Object> infoObject = new HashMap<String, Object>();
				infoObject.put("advanceToForm", advanceToThisForm);
				tSession.setAttribute("infoHashMap", infoObject);
			}
		}
	}

}