velosSliderJS ={
	sliderItems: [],
	createSlider: {}
};

velosSliderJS.createSlider = function(fieldKeyword, fieldDisplayValues, fieldDataValues){
	var sliderOutputFld = (document.getElementsByName(fieldKeyword)[0]);
	var sliderOutputFldTR = sliderOutputFld.parentNode.parentNode.parentNode;
	
	var sliderTD = document.createElement('TD');
	sliderTD.id = 'sliderTD';
	sliderTD.name = 'sliderTD';
	sliderTD.innerHTML = ' ';
	sliderOutputFldTR.appendChild(sliderTD);
	
	var sliderDiv = document.createElement('div');
	sliderDiv.id = 'slider';
	sliderDiv.name = 'slider';
	sliderDiv.innerHTML = ' ';
	sliderTD.appendChild(sliderDiv);

	var legendDiv = document.createElement('div');
	legendDiv.id = 'legend';
	legendDiv.name = 'legend';
	legendDiv.innerHTML = ' ';
	sliderTD.appendChild(legendDiv);
	
	//var fieldDisplayValues = ['Sampada','Ruchira','Julia','Mugdha','Aman','Isaac'];
	
	//var fieldDataValues = ['0','1','2','3','4','5'];

	var s = $j("#slider");
	s.slider({
	  min:fieldDataValues[0],
	  max:fieldDataValues[fieldDataValues.length-1],
	  value: $j('#'+sliderOutputFld.id).val(),
	  change: function(event, ui) {
		  $j('#'+sliderOutputFld.id).val(ui.value);
	  }
	});
	
	var oneBig = 100 / (fieldDisplayValues.length - 1);

	$j("#legend").append("<table cellpadding='0'><tr id='legendTR'></tr></table>");
	$j.each(fieldDisplayValues, function(key,value){
	  var labelHTML = '';

	  var w = oneBig;
	  labelHTML = "<label style='display:inline-block; text-align:center;'>"+value+"</label>";

	  if(key === 0){
		  //w = oneBig/2;
		  labelHTML = "<label style='display:inline-block; text-align:left;'>"+value+"</label>";  
	  } else if (key === fieldDisplayValues.length-1){
		  w = oneBig/2;
		  labelHTML = "<label style='display:inline-block; text-align:right;'>"+value+"</label>";
	  }
			
	  $j("#legendTR").append("<TD padding='0px 0px 0px 0px' width='"+w+"%'>"+labelHTML+"</TD>");

	});
};

$j(document).ready(function(){

});