package com.velos.eres.service.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;


import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CommonDAO;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


public class GetStudyStatusData extends ActionSupport{
	/**
	 * 
	 */

		private static final long serialVersionUID = 1L;
		private HttpServletRequest request = null;
		private ArrayList<HashMap<String,Object>> jsonData=null;
	    JSONObject   jsObj=new JSONObject();
	    
	    private ArrayList statStartDates;

	    private ArrayList statStartDatesPRC;
	    
	    private ArrayList statStartDatesCTRC;

	    
	    public ArrayList getStatStartDates() {
	        return this.statStartDates;
	    }

	    public void setStatStartDates(ArrayList statStartDates) {
	        this.statStartDates = statStartDates;
	    }

	    public ArrayList getStatStartDatesPRC() {
	    	return this.statStartDatesPRC;
	    }
	    
	    public void setStatStartDatesPRC(ArrayList statStartDatesPRC) {
	    	this.statStartDatesPRC = statStartDatesPRC;
	    }
	    
	    public ArrayList getStatStartDatesCTRC() {
	    	return this.statStartDatesCTRC;
	    }
	    
	    public void setStatStartDatesCTRC(ArrayList statStartDatesCTRC) {
	    	this.statStartDatesCTRC = statStartDatesCTRC;
	    }

	    public void setStatStartDates(String statStartDate) {
	        statStartDates.add(statStartDate);
	    }
	    
	    public String getStatStartDates(int idx) {
	    	if (statStartDates != null && idx < statStartDates.size())
	    	{
	    		return (String) statStartDates.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }

	    public void setStatStartDatesPRC(String statStartDate) {
	    	statStartDatesPRC.add(statStartDate);
	    }
	    
	    public String getStatStartDatesPRC(int idx) {
	    	if (statStartDatesPRC != null && idx < statStartDatesPRC.size())
	    	{
	    		return (String) statStartDatesPRC.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }

	    
	    public void setStatStartDatesCTRC(String statStartDate) {
	    	statStartDatesCTRC.add(statStartDate);
	    }
	    
	    public String getStatStartDatesCTRC(int idx) {
	    	if (statStartDatesCTRC != null && idx < statStartDatesCTRC.size())
	    	{
	    		return (String) statStartDatesCTRC.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }


  ///  public JSONObject jsObj = null;
	
	/**
	 * @return the jsonData
	 */
	public ArrayList<HashMap<String, Object>> getJsonData() {
		return jsonData;
	}

	/**
	 * @param jsonData the jsonData to set
	 */
	public void setJsonData(ArrayList<HashMap<String, Object>> jsonData) {
		this.jsonData = jsonData;
	}
	
	public GetStudyStatusData() {
		request = ServletActionContext.getRequest();
        statStartDates = new ArrayList();
        statStartDatesPRC = new ArrayList();
        statStartDatesCTRC = new ArrayList();
	}
	
	public String getStudyStatusData(){

		String studyID=request.getParameter("studyID");
		int StudyID1 = Integer.parseInt(studyID);
		ArrayList<HashMap<String, Object>> argMap=new  ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> argHMap = new HashMap<String, Object>();
		String userId = null;
		try {
			userId = (String)request.getSession(false).getAttribute("userId");
			}
		finally {
			userId = StringUtil.trueValue(userId);
			}
		int userID = Integer.parseInt(userId);
		getStudy(StudyID1);
		ArrayList<String>  studystatusdatePRC=new ArrayList<String>();			  
		ArrayList<String>  studystatusdateCTRC=new ArrayList<String>();			  
		studystatusdatePRC=getStatStartDatesPRC();
		studystatusdateCTRC=getStatStartDatesCTRC();
				 
		if(null != studystatusdatePRC.get(0)){
		String ctrc_kac_PRCdate =studystatusdatePRC.get(0).substring(0,10);
		String prcDate;
		prcDate = DateUtil.format2DateFormat(ctrc_kac_PRCdate);
		ctrc_kac_PRCdate = prcDate;
		argHMap.put("ctrc_kac_PRCdate", ctrc_kac_PRCdate);
		}else{
		argHMap.put("ctrc_kac_PRCdate", "d1null");
		}
		
		if(null != studystatusdateCTRC.get(0)){
			String ctrc_kac_oandldate = studystatusdateCTRC.get(0).substring(0,10);
			String oandlDate;
			oandlDate = DateUtil.format2DateFormat(ctrc_kac_oandldate);
			ctrc_kac_oandldate = oandlDate;
			argHMap.put("ctrc_kac_oandldate", ctrc_kac_oandldate);   
		}else{
		argHMap.put("ctrc_kac_oandldate", "d2null");
		}
		argMap.add(argHMap);
		setJsonData(argMap);
		
	return SUCCESS;
	}
	
	   public void getStudy(int study) {

	        PreparedStatement pstmt = null;
	        PreparedStatement pstmtPRC = null;
	        PreparedStatement pstmtCTRC = null;
	        Connection conn = null;
	        try {
	        	CommonDAO cd = new CommonDAO();
	            conn = cd.getConnection();
	            Rlog.debug("studystatus", "got connection" + conn);
	            String mysql = "SELECT PK_STUDYSTAT,"
	                    + "FK_STUDY," + "STUDYSTAT_DATE"
	                    + " FROM ER_STUDYSTAT WHERE FK_STUDY =?";

	            String mysqlCustom = " Select max(STUDYSTAT_DATE) as StudyStat_Date "
	         	   + " from ER_CODELST ERC," + " ER_STUDY ERS, " + " ER_STUDYSTAT ERST "
	               + " WHERE ERS.PK_STUDY=ERST.FK_STUDY "
	               + " AND ERST.FK_CODELST_STUDYSTAT=ERC.PK_CODELST "
	               + " AND ers.pk_study = ?" ;
	            
	            String mysqlPRC = mysqlCustom + " and  erc.codelst_subtyp = 'app_PRC' ";
	            String mysqlCTRC = mysqlCustom + " and  erc.codelst_subtyp = 'CTRC_Operations' ";
	            Rlog.debug("studystatus", mysql);
	            pstmt = conn.prepareStatement(mysql);
	            pstmt.setInt(1, study);
	            ResultSet rs = pstmt.executeQuery();
	            Rlog.debug("studystatus", "got resultset" + rs);

	            while (rs.next()) {
	                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
	            }
	//START
	// This code is only intended for fetching the custom Study Statuses.
	            pstmtPRC = conn.prepareStatement(mysqlPRC);
	            pstmtPRC.setInt(1, study);
	            ResultSet rsPRC = pstmtPRC.executeQuery();
	            while (rsPRC.next()) {
	           		setStatStartDatesPRC(rsPRC.getString("STUDYSTAT_DATE"));
	            }

	            pstmtCTRC = conn.prepareStatement(mysqlCTRC);
	            pstmtCTRC.setInt(1, study);
	            ResultSet rsCTRC = pstmtCTRC.executeQuery();
	            while (rsCTRC.next()) {
	            	setStatStartDatesCTRC(rsCTRC.getString("STUDYSTAT_DATE"));
	            }
	            Rlog.debug("studystatus", "StudyStatusDao.rows ");
	//END
	        } catch (SQLException ex) {
	            Rlog.fatal("studystatus",
	                    "studystatusDao.getMsgcntr EXCEPTION IN FETCHING FROM er_studystat"
	                            + ex);
	        } finally {
	            try {
	                if ((pstmt != null)||(pstmtPRC != null)||(pstmtCTRC != null)){
	                    pstmt.close();
	                    pstmtPRC.close();
	                    pstmtCTRC.close();
	                    }
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null){
	                    conn.close();
	                    }
	            } catch (Exception e) {
	            }
	        }
	    }
	
}
