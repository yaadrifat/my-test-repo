
function studystatusData_loadstudystatusData()
{	
		var studyID = document.getElementsByName("studyId")[0].value;
			jQuery.ajax({
				url:'/velos/jsp/getStudyStatusData',
				type: 'GET',
				dataType:'json',
				global: true,
				async: false,
				data: {"studyID": studyID
					},
					
					
				success: function(resp) 
				{		
				if(resp.jsonData[0].ctrc_kac_PRCdate == "d1null" && resp.jsonData[0].ctrc_kac_oandldate == "d2null")
				{
				var ctrc_kac_PRCdate = document.getElementById(getColumnSysId('ctrc_kac_PRCdate'));
				ctrc_kac_PRCdate.value = "";
				
				var ctrc_kac_oandldate = document.getElementById(getColumnSysId('ctrc_kac_oandldate'));
			 	ctrc_kac_oandldate.value = "";
				}
				else if(resp.jsonData[0].ctrc_kac_PRCdate == "d1null" || resp.jsonData[0].ctrc_kac_oandldate == "d2null")
				{
						if(resp.jsonData[0].ctrc_kac_PRCdate == "d1null" ) {
							var ctrc_kac_PRCdate = document.getElementById(getColumnSysId('ctrc_kac_PRCdate'));
							ctrc_kac_PRCdate.value = "";
							var ctrc_kac_oandldate = document.getElementById(getColumnSysId('ctrc_kac_oandldate'));
						 	ctrc_kac_oandldate.value = resp.jsonData[0].ctrc_kac_oandldate;
						}
						else if(resp.jsonData[0].ctrc_kac_oandldate == "d2null" ) {
						var ctrc_kac_PRCdate = document.getElementById(getColumnSysId('ctrc_kac_PRCdate'));
						ctrc_kac_PRCdate.value = resp.jsonData[0].ctrc_kac_PRCdate;
							var ctrc_kac_oandldate = document.getElementById(getColumnSysId('ctrc_kac_oandldate'));
						 	ctrc_kac_oandldate.value = "";
						}
					}
				else if(resp.jsonData[0].ctrc_kac_PRCdate != "d1null" && resp.jsonData[0].ctrc_kac_oandldate  != "d2null")
				{
					var ctrc_kac_PRCdate = document.getElementById(getColumnSysId('ctrc_kac_PRCdate'));
					ctrc_kac_PRCdate.value = resp.jsonData[0].ctrc_kac_PRCdate;
					var ctrc_kac_oandldate = document.getElementById(getColumnSysId('ctrc_kac_oandldate'));
				 	ctrc_kac_oandldate.value = resp.jsonData[0].ctrc_kac_oandldate;
					
				}
							
			},
			error:function(resp){
					//alert("CTRC Status and O&L Status Is Not Configured On This Study.");
			}
					});
	}
		
		
//////******************************

function getstudystatusData_Init() {
	studystatusData_loadstudystatusData();
}
window.onload = function() {
    setTimeout(function(){ getstudystatusData_Init();}, 1000);
}