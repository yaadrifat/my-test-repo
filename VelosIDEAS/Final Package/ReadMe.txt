*********************************************************************************
This Patch is for Velos IDEAS as per the requirement document.
*********************************************************************************

This patch consists of following files:
1. DB Patch:
	1. CODELST_INSERT.sql 
2. JS Files: 
	1. getStudyStatusData.js - 		server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos\getStudyStatusData.js

3. XML File:
	2. struts-eresearch.xml - 			server\eresearch\deploy\velos.ear\velos.war\WEB-INF\classes\struts-eresearch.xml
	
4. JAVA Class File:
	1. getStudyStatusData.class - 		server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\service\web\getStudyStatusData.class (web folder is not present at App server, we need to mannually create the folder and put the class file in it)


Steps to deploy the Patch:

- Stop the eResearch application.
- Execute the DB Patches on eres schema.
- Replace the JS file, XML file and Class File on the application server with the files in this 'server' folder.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.

NOTE: Prior to this, all the UI changes(making changes in the mentioned forms like hiding fields) needs to be done as per the requirement Document.
*********************************************************************************************************************************************************************

