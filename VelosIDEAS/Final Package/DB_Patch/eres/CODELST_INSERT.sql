set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat_type' and CODELST_SUBTYP = 'studystat_ty_16';
  if (v_record_exists = 0) then
INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat_type', 'studystat_ty_16', 'CTRC Operations and Logistics Committee', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat_type'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL,'default_data', NULL);
	  commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='studystat' and CODELST_SUBTYP = 'CTRC_Operations';
  if (v_record_exists = 0) then
INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval)+1 , NULL, 'studystat', 'CTRC_Operations', 'CTRC O&L � Approved', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studystat'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, 'browser', 'studystat_ty_16','default_data', NULL);
	  commit;
  end if;
end;
/
commit;
