SET define OFF;

-- INSERTING into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20011;
  IF (v_record_exists = 0) THEN
    INSERT
    INTO ER_REPORT
      (
        PK_REPORT,
        REP_NAME,
        REP_DESC,
        REP_SQL,
        FK_ACCOUNT,
        FK_CODELST_TYPE,
        IP_ADD,
        REP_HIDE,
        REP_COLUMNS,
        REP_FILTERBY,
        GENERATE_XML,
        REP_TYPE,
        REP_SQL_CLOB,
        REP_FILTERKEYWORD,
        REP_FILTERAPPLICABLE
      ) Values
      (20011,
        'Coverage Analysis Summary',
        'Coverage Analysis Summary',
        '',
        0,
        NULL,
        NULL,
        'N',
        NULL,
        'Study',
        1,
        'rep_pcr',
        '',
        'protId',
        'protId');
    COMMIT;
END IF;
    UPDATE er_report SET rep_sql = ' SELECT BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,PI,DEPT,prot_ver,qual_trial,SITE_NAME,
total_cost_all_pat,lower(LINEITEM_NAME) as l_LINEITEM_NAME,DECODE(COST_CUSTOMCOL_COV,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
SUBCOST_ITEM_FLAG,NVL(FK_EVENT, 0) as FK_EVENT,0 AS INTERSECTION_ID, '''' AS Cal_Name, 0 AS EVENT_ID, 0 AS ORG_ID, 0 AS CHAIN_ID, '''' AS EVENT_TYPE, '''' AS NAME, 0 AS COST,'''' AS  DESCRIPTION, ''''  AS flag, 0  AS FK_VISIT,  0 as  VISIT_NO, 0 as DISPLACEMENT, '''' AS EVENT_CATEGORY, '''' as covetype_desc, 0 as FK_CODELST_COVERTYPE, '''' AS COVERAGE_NOTES, '''' AS EVENT_CPTCODE FROM ERV_BUDGET_COVTYPE WHERE fk_study = :protId AND BGTCal_PROTID is not null
AND NVL(BGTCAL_PROTTYPE,''L'')<>''L'' AND FK_EVENT >0 
UNION ALL
select BUDGET_NAME,(Select name from event_assoc where event_id= a.bgtcal_protid) as PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,'''' STUDY_NUMBER,
'''' STUDY_TITLE,'''' PI,'''' DEPT,'''' prot_ver,'''' qual_trial,'''' SITE_NAME,0 total_cost_all_pat,null as l_LINEITEM_NAME,''Other'' AS standard_of_care,
0 SUBCOST_ITEM_FLAG,0  as FK_EVENT ,0 AS INTERSECTION_ID, '''' AS Cal_Name, 0 AS EVENT_ID, 0 AS ORG_ID, 0 AS CHAIN_ID, '''' AS EVENT_TYPE,'''' AS NAME,
  0 AS COST,'''' AS  DESCRIPTION, ''''  AS flag, 0  AS FK_VISIT,  0 as  VISIT_NO, 0 as DISPLACEMENT, '''' AS EVENT_CATEGORY, '''' as covetype_desc,
 0 as FK_CODELST_COVERTYPE, '''' AS COVERAGE_NOTES, '''' AS EVENT_CPTCODE
from sch_budget,(select pk_bgtcal,BGTCAL_PROTID  from sch_budget, sch_bgtcal where pk_budget=fk_budget 
and fk_study= :protId and NVL(budget_delflag,''N'')=''N'' and BGTCAL_PROTTYPE=''S'' and bgtcal_protid is not null) a
where  NVL(BUDGET_DELFLAG,''N'')=''N'' and fk_study= :protId
UNION All
SELECT '''' as BUDGET_NAME,'''' as PROT_CALENDAR,'''' as BUDGET_VERSION,'''' as BUDGET_DESC,'''' as STUDY_NUMBER,'''' as STUDY_TITLE,'''' as PI,'''' as DEPT,
'''' prot_ver,'''' qual_trial,'''' as SITE_NAME,0 as total_cost_all_pat,'''' as  l_LINEITEM_NAME,''Other'' AS standard_of_care,0 as SUBCOST_ITEM_FLAG,
0 as FK_EVENT,EVENT_ID AS INTERSECTION_ID, (Select name from event_assoc where event_id=a.chain_id) Cal_Name,  EVENT_ID,  ORG_ID,  CHAIN_ID,  EVENT_TYPE,
  trim(NAME) AS NAME,  COST,  DESCRIPTION,  flag,  FK_VISIT,   VISIT_NO,  DISPLACEMENT,  EVENT_CATEGORY, (SELECT codelst_subtyp from sch_codelst where pk_codelst=FK_CODELST_COVERTYPE) as covetype_desc,FK_CODELST_COVERTYPE AS FK_CODELST_COVERTYPE,  COVERAGE_NOTES, EVENT_CPTCODE
FROM erv_covanal a
where chain_id in (select distinct bgtcal_protid from sch_budget,sch_bgtcal where pk_budget=fk_budget 
and  NVL(BUDGET_DELFLAG,''N'')=''N'' 
and fk_study= :protId 
and bgtcal_protid is not null)
ORDER BY BUDGET_NAME nulls last,PROT_CALENDAR, l_LINEITEM_NAME nulls last ,chain_id,flag DESC,
  NAME,
  FK_VISIT nulls FIRST '
    WHERE pk_report = 20011;
    COMMIT;

   UPDATE er_report SET rep_sql_clob = ' SELECT BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,PI,DEPT,prot_ver,qual_trial,SITE_NAME,
total_cost_all_pat,lower(LINEITEM_NAME) as l_LINEITEM_NAME,DECODE(COST_CUSTOMCOL_COV,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
SUBCOST_ITEM_FLAG,NVL(FK_EVENT, 0) as FK_EVENT,0 AS INTERSECTION_ID, '''' AS Cal_Name, 0 AS EVENT_ID, 0 AS ORG_ID, 0 AS CHAIN_ID, '''' AS EVENT_TYPE, '''' AS NAME, 0 AS COST,'''' AS  DESCRIPTION, ''''  AS flag, 0  AS FK_VISIT,  0 as  VISIT_NO, 0 as DISPLACEMENT, '''' AS EVENT_CATEGORY, '''' as covetype_desc, 0 as FK_CODELST_COVERTYPE, '''' AS COVERAGE_NOTES, '''' AS EVENT_CPTCODE FROM ERV_BUDGET_COVTYPE WHERE fk_study = :protId AND BGTCal_PROTID is not null
AND NVL(BGTCAL_PROTTYPE,''L'')<>''L'' AND FK_EVENT >0 
UNION ALL
select BUDGET_NAME,(Select name from event_assoc where event_id= a.bgtcal_protid) as PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,'''' STUDY_NUMBER,
'''' STUDY_TITLE,'''' PI,'''' DEPT,'''' prot_ver,'''' qual_trial,'''' SITE_NAME,0 total_cost_all_pat,null as l_LINEITEM_NAME,''Other'' AS standard_of_care,
0 SUBCOST_ITEM_FLAG,0  as FK_EVENT ,0 AS INTERSECTION_ID, '''' AS Cal_Name, 0 AS EVENT_ID, 0 AS ORG_ID, 0 AS CHAIN_ID, '''' AS EVENT_TYPE,'''' AS NAME,
  0 AS COST,'''' AS  DESCRIPTION, ''''  AS flag, 0  AS FK_VISIT,  0 as  VISIT_NO, 0 as DISPLACEMENT, '''' AS EVENT_CATEGORY, '''' as covetype_desc,
 0 as FK_CODELST_COVERTYPE, '''' AS COVERAGE_NOTES, '''' AS EVENT_CPTCODE
from sch_budget,(select pk_bgtcal,BGTCAL_PROTID  from sch_budget, sch_bgtcal where pk_budget=fk_budget 
and fk_study= :protId and NVL(budget_delflag,''N'')=''N'' and BGTCAL_PROTTYPE=''S'' and bgtcal_protid is not null) a
where  NVL(BUDGET_DELFLAG,''N'')=''N'' and fk_study= :protId
UNION All
SELECT '''' as BUDGET_NAME,'''' as PROT_CALENDAR,'''' as BUDGET_VERSION,'''' as BUDGET_DESC,'''' as STUDY_NUMBER,'''' as STUDY_TITLE,'''' as PI,'''' as DEPT,
'''' prot_ver,'''' qual_trial,'''' as SITE_NAME,0 as total_cost_all_pat,'''' as  l_LINEITEM_NAME,''Other'' AS standard_of_care,0 as SUBCOST_ITEM_FLAG,
0 as FK_EVENT,EVENT_ID AS INTERSECTION_ID, (Select name from event_assoc where event_id=a.chain_id) Cal_Name,  EVENT_ID,  ORG_ID,  CHAIN_ID,  EVENT_TYPE,
  trim(NAME) AS NAME,  COST,  DESCRIPTION,  flag,  FK_VISIT,   VISIT_NO,  DISPLACEMENT,  EVENT_CATEGORY, (SELECT codelst_subtyp from sch_codelst where pk_codelst=FK_CODELST_COVERTYPE) as covetype_desc,FK_CODELST_COVERTYPE AS FK_CODELST_COVERTYPE,  COVERAGE_NOTES, EVENT_CPTCODE
FROM erv_covanal a
where chain_id in (select distinct bgtcal_protid from sch_budget,sch_bgtcal where pk_budget=fk_budget 
and  NVL(BUDGET_DELFLAG,''N'')=''N'' 
and fk_study= :protId 
and bgtcal_protid is not null)
ORDER BY BUDGET_NAME nulls last,PROT_CALENDAR, l_LINEITEM_NAME nulls last ,chain_id,flag DESC,
  NAME,
  FK_VISIT nulls FIRST '
    WHERE pk_report = 20011;
    COMMIT; 
 
END;
/