SET define OFF;

DECLARE
filterID number := 0;
begin

	select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="protDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013&form=reports&maxselect=1&seperator=,&defaultvalue=&keyword=selprotId|STUDY_NUMBER~paramprotId|LKP_PK|[VELHIDE]'','''')">Select Specific Study</Font></DIV></td><td><DIV id="protdataDIV"> <input TYPE="text" NAME="selprotId" readonly value=""><input TYPE="hidden" NAME="paramprotId" value=""></DIV></td>',
		'SELECT pk_study FROM ER_STUDY WHERE EXISTS (SELECT * FROM ER_STUDYTEAM WHERE fk_user = :sessUserId AND fk_study = pk_study AND study_team_usr_type <> ''X'' ) OR Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1','Protocol','protId','protDIV,protdataDIV');
		
		COMMIT;
	
END;
/
