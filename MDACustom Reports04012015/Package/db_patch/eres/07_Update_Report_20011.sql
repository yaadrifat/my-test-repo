SET define OFF;
-- INSERTING into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
  SQL_CLOB CLOB;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20011;
  IF (v_record_exists <> 0) THEN
	
	SQL_CLOB := ' SELECT pk_budget,BUDGET_NAME,PROT_CALENDAR,BGTCAL_PROTID,BUDGET_VERSION,STUDY_NUMBER,STUDY_TITLE,PI,DEPT,prot_ver,qual_trial,SITE_NAME,
TOTAL_COST_PER_PAT,lower(LINEITEM_NAME) as l_LINEITEM_NAME,DECODE(COST_CUSTOMCOL_COV,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
SUBCOST_ITEM_FLAG,NVL(FK_EVENT, 0) as FK_EVENT,0 INTERSECTION_ID, '''' Cal_Name, 0  EVENT_ID, 0 CHAIN_ID, '''' EVENT_TYPE, '''' NAME,
'''' flag, 0 FK_VISIT,0 VISIT_NO,'''' covetype_desc, 0 FK_CODELST_COVERTYPE, '''' COVERAGE_NOTES, '''' EVENT_CPTCODE, '''' EVENT_ADDCODE FROM ERV_BUDGET_COVTYPE WHERE fk_study = :protId AND BGTCal_PROTID is not null
AND NVL(BGTCAL_PROTTYPE,''L'')<>''L'' AND FK_EVENT >0  and pk_budget in (select d.fk_budget from sch_bgtusers d  Where d.fk_user = :sessUserId)
UNION ALL
SELECT pk_budget,BUDGET_NAME,(Select name from event_assoc where event_id= b.bgtcal_protid) as PROT_CALENDAR,b.BGTCAL_PROTID,BUDGET_VERSION,'''' STUDY_NUMBER,
'''' STUDY_TITLE,'''' PI,'''' DEPT,'''' prot_ver,'''' qual_trial,'''' SITE_NAME,0 TOTAL_COST_PER_PAT,null as l_LINEITEM_NAME,''Other'' AS standard_of_care,
0 SUBCOST_ITEM_FLAG,0 FK_EVENT ,0 INTERSECTION_ID, '''' Cal_Name, 0 EVENT_ID, 0 CHAIN_ID, '''' EVENT_TYPE,'''' NAME,'''' flag, 0 FK_VISIT,0 VISIT_NO,'''' covetype_desc, 0 FK_CODELST_COVERTYPE, 
'''' COVERAGE_NOTES, '''' EVENT_CPTCODE, '''' EVENT_ADDCODE FROM (SELECT pk_budget,BUDGET_NAME,BUDGET_VERSION FROM sch_budget, sch_bgtcal WHERE pk_budget =fk_budget 
AND fk_study  = :protId AND NVL(budget_delflag,''N'')=''N'' AND BGTCAL_PROTTYPE =''S'' AND bgtcal_protid IS NOT NULL ) a,
(SELECT pk_bgtcal, BGTCAL_PROTID FROM sch_budget,sch_bgtcal WHERE pk_budget =fk_budget AND fk_study = :protId AND NVL(budget_delflag,''N'')=''N'' AND BGTCAL_PROTTYPE =''S''
  AND bgtcal_protid IS NOT NULL ) b WHERE a.pk_budget IN (SELECT d.fk_budget FROM sch_bgtusers d WHERE d.fk_user = :sessUserId)
UNION All
SELECT 0 as pk_budget,'''' BUDGET_NAME, '''' PROT_CALENDAR,0 BGTCAL_PROTID,'''' BUDGET_VERSION,'''' STUDY_NUMBER,'''' STUDY_TITLE,'''' PI,'''' DEPT,'''' prot_ver,'''' qual_trial,'''' SITE_NAME,0 TOTAL_COST_PER_PAT,
  '''' l_LINEITEM_NAME,''Other'' standard_of_care,0 SUBCOST_ITEM_FLAG,0 FK_EVENT,EVENT_ID AS INTERSECTION_ID,(SELECT name FROM event_assoc WHERE event_id=a.chain_id) Cal_Name,
  EVENT_ID,CHAIN_ID,EVENT_TYPE,trim(NAME) AS NAME,flag,FK_VISIT,VISIT_NO,(SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst=FK_CODELST_COVERTYPE) AS covetype_desc,
  NVL(FK_CODELST_COVERTYPE,0) AS FK_CODELST_COVERTYPE, COVERAGE_NOTES, EVENT_CPTCODE,(CASE WHEN FK_VISIT IS NULL THEN (pkg_util.f_join(cursor(select  codelst_desc || '' - '' || md_modelementdata from  er_moredetails, er_codelst  
  where pk_codelst = md_modelementpk  and fk_modpk = a.event_id and md_modname = ''evtaddlcode''  and CODELST_TYPE = ''evtaddlcode'' and trim(md_modelementdata) is not null  order by codelst_seq), '', ''))  
  ELSE  ('''')  END) AS EVENT_ADDCODE FROM erv_covanal a WHERE chain_id IN  (SELECT DISTINCT bgtcal_protid FROM sch_budget,sch_bgtcal WHERE pk_budget =fk_budget AND NVL(BUDGET_DELFLAG,''N'')=''N''
  AND fk_study  = :protId AND bgtcal_protid IS NOT NULL  AND pk_budget IN (SELECT d.fk_budget FROM sch_bgtusers d WHERE d.fk_user = :sessUserId ))
UNION ALL
select 0 as pk_budget,'''' BUDGET_NAME,'''' PROT_CALENDAR,0 BGTCAL_PROTID,'''' BUDGET_VERSION,'''' STUDY_NUMBER,'''' STUDY_TITLE,'''' PI,'''' DEPT,'''' prot_ver,'''' qual_trial,'''' SITE_NAME,0 TOTAL_COST_PER_PAT,
  '''' l_LINEITEM_NAME,''Other'' standard_of_care,0 SUBCOST_ITEM_FLAG,0 FK_EVENT,INTERSECTION_ID,(SELECT name FROM event_assoc WHERE event_id=b.chain_id) Cal_Name,EVENT_ID,
  CHAIN_ID, EVENT_TYPE,NAME,''E'' AS flag,fk_visit,0 VISIT_NO,'''' covetype_desc,-1 FK_CODELST_COVERTYPE,null COVERAGE_NOTES,EVENT_CPTCODE,esch.lst_additionalcodes_financial(event_id,''evtaddlcode'',-1) AS EVENT_ADDCODE
  from (SELECT  (SELECT EVENT_ID  FROM event_assoc WHERE CHAIN_ID = a.CHAIN_ID AND COST = a.COST AND FK_VISIT= pk_protocol_visit AND EVENT_TYPE = ''A'') AS INTERSECTION_ID, 
  a.EVENT_ID as EVENT_ID, CHAIN_ID,EVENT_TYPE,trim(NAME) as name,pk_protocol_visit as fk_visit,(SELECT EVENT_CPTCODE FROM event_assoc  WHERE CHAIN_ID = a.CHAIN_ID
  AND COST = a.COST AND FK_VISIT  IS NULL AND EVENT_TYPE = ''A'') EVENT_CPTCODE FROM event_assoc a, sch_protocol_visit WHERE fk_protocol = a.CHAIN_ID
and a.CHAIN_ID IN (SELECT DISTINCT bgtcal_protid FROM sch_budget, sch_bgtcal WHERE pk_budget =fk_budget AND NVL(BUDGET_DELFLAG,''N'')=''N'' AND fk_study = :protId
  AND bgtcal_protid IS NOT NULL  AND pk_budget IN (SELECT d.fk_budget FROM sch_bgtusers d WHERE d.fk_user = :sessUserId)) AND a.event_type = ''A''
and a.FK_VISIT  IS NULL) b where b.INTERSECTION_ID is null ORDER BY BUDGET_NAME nulls last,pk_budget,PROT_CALENDAR, l_LINEITEM_NAME nulls last ,chain_id,flag DESC,VISIT_NO,NAME,FK_VISIT nulls FIRST ';

update er_report set REP_SQL_CLOB = SQL_CLOB
where pk_report = 20011;
COMMIT;
  END IF;
END;
/