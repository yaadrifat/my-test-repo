DECLARE
filterMID number := 0;
filterFK number := 0;
v_exists NUMBER := 0;
v_seq NUMBER := 0;
begin
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'protId';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'study';
  select max(REPFILTERMAP_SEQ)+1 into v_seq  from er_repfiltermap where repfiltermap_repcat = 'study';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'study',v_seq);
			
		COMMIT;
	end if;
END;
/