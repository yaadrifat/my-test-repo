SET define OFF;
-- INSERTING into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
  SQL_CLOB CLOB;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 20010;
  IF (v_record_exists <> 0) THEN
	
	SQL_CLOB := 'select distinct
es.study_number as study_number,
es.study_title as Study_title,
visit_name,
se.event_id,
(select sum(eventcost_value) from sch_eventcost where fk_cost_desc = (select pk_codelst from sch_codelst where codelst_type = ''cost_desc'' and codelst_subtyp = ''PRS PRICE'') and fk_event = se.fk_assoc) as cost,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select md_modelementdata from er_moredetails where md_modelementpk = (select pk_codelst from er_codelst where codelst_type = ''evtaddlcode'' and codelst_subtyp = ''addl_code'') and LPAD(fk_modpk,10,''0'') = se.fk_assoc) as AdditionalCode,
(SELECT studyid_id FROM er_studyid WHERE fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = ''studyidtype'' AND codelst_subtyp = ''protver'')
  AND fk_study = es.pk_study
  )as ProtVer,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatName,
(select pat_facilityid from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatFacilityId,
NVL((select col1 from er_formslinear where pk_formslinear = (select max(pk_formslinear) from er_formslinear where fk_form = 390 and id = es.pk_study)),''N/A'')as Sponsor,
NVL((select substr(col2,0,INSTR(col2, ''['')-1) from er_formslinear where pk_formslinear = (select max(pk_formslinear) from er_formslinear where fk_form = 390 and id = es.pk_study)),''N/A'')as Funding_Source,
NVL((select col3 from er_formslinear where pk_formslinear = (select max(pk_formslinear) from er_formslinear where fk_form = 390 and id = es.pk_study)),''N/A'')as Account
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
LPAD(ea.event_id,10,''0'') = se.session_id and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select max(eventstat_dt) from sch_eventstat where fk_event = se.event_id) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select max(eventstat_dt) from sch_eventstat where fk_event = se.event_id) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''R0'') and
pk_study IN (:studyId) and
se.fk_patprot IN(select pk_patprot from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1)';

update er_report set REP_SQL_CLOB = SQL_CLOB, REP_SQL = SQL_CLOB
where pk_report = 20010;
COMMIT;
  END IF;
END;
/