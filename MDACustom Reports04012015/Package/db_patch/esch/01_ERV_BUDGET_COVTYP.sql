CREATE OR REPLACE FORCE VIEW "ESCH"."ERV_BUDGET_COVTYPE" ("PK_BUDGET", "BGTCAL_PROTID", "PK_BGTCAL", "BUDGET_CALENDAR", "BGTCAL_PROTTYPE", "PROT_CALENDAR", "BUDGET_NAME", "BUDGET_VERSION", "BUDGET_DESC", "STUDY_NUMBER", "FK_STUDY", "STUDY_TITLE", "PI", "DEPT", "PROT_VER", "QUAL_TRIAL", "SITE_NAME", "BGTSECTION_NAME", "BGTSECTION_PATNO", "LINEITEM_NAME", "TOTAL_COST", "TOTAL_COST_AFTER", "TOTAL_PAT_COST_AFTER", "TOTAL_LINE_FRINGE", "PER_PATIENT_LINE_FRINGE", "CATEGORY", "CATEGORY_SEQ", "CATEGORY_SUBTYP", "UNIT_COST", "NUMBER_OF_UNIT", "STANDARD_OF_CARE", "COST_PER_PATIENT", "COST_DISCOUNT_ON_LINE_ITEM", "PER_PAT_LINE_ITEM_DISCOUNT", "TOTAL_COST_DISCOUNT", "CPT_CODE", "TMID", "CDM", "LINE_ITEM_INDIRECTS_FLAG", "INDIRECTS", "BUDGET_INDIRECT_FLAG", "FRINGE_BENEFIT", "FRINGE_FLAG", "BUDGET_DISCOUNT", "BUDGET_DISCOUNT_FLAG", "PERPAT_INDIRECT", "TOTAL_COST_INDIRECT", "BUDGET_CURRENCY", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "BUDGETSECTION_SEQUENCE", "BGTCAL_INDIRECTCOST", "LINEITEM_DESC", "LINEITEM_SPONSORAMOUNT", "LINEITEM_APPLYPATIENTCOUNT", "LINEITEM_VARIANCE", "BGTSECTION_TYPE", "LINEITEM_OTHERCOST", "FK_CODELST_COST_TYPE", "COST_TYPE_DESC", "LINEITEM_SEQ", "LINITEM_SPONSOR_PERPAT", "LINEITEM_DIRECT_PERPAT", "TOTAL_COST_PER_PAT", "TOTAL_COST_ALL_PAT", "COST_CUSTOMCOL_COV", "COST_CUSTOMCOL", "PK_BUDGETSEC", "BUDGETSEC_FKVISIT", "PK_LINEITEM", "LINEITEM_INPERSEC", "LINEITEM_FKEVENT", "BGTCAL_EXCLDSOCFLAG", "BUDGET_COMBFLAG", "COVERAGE_TYPE", "SUBCOST_ITEM_FLAG", "FK_EVENT") AS 
  SELECT DISTINCT pk_budget,
    bgtcal_protid,
    pk_bgtcal,
    BUDGET_CALENDAR,
    bgtcal_prottype,
    prot_calendar,
    budget_name,
    budget_version,
    budget_desc,
    study_number,
    fk_study,
    study_title,
    PI,
    Dept,
    prot_ver,
    qual_trial,
    site_name,
    bgtsection_name,
    bgtsection_patno,
    lineitem_name,
    total_cost,
    ROUND(NVL (total_cost_after, 0), 2) total_cost_after,
    ROUND(NVL (total_pat_cost_after, 0), 2) total_pat_cost_after,
    total_line_fringe,
    per_patient_line_fringe,
    CATEGORY,
    category_seq,
    category_subtyp,
    ROUND(unit_cost, 2) AS unit_cost,
    number_of_unit,
    standard_of_care,
    ROUND(cost_per_patient, 2) AS cost_per_patient,
    cost_discount_on_line_item,
    per_pat_line_item_discount,
    total_cost_discount,
    cpt_code,
    tmid,
    cdm,
    line_item_indirects_flag,
    indirects,
    budget_indirect_flag,
    fringe_benefit,
    fringe_flag,
    budget_discount,
    budget_discount_flag,
    ROUND(NVL ( DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after * line_item_indirects_flag * indirects , 0 ), 0 )/100 , 2)  AS perpat_indirect,
    ROUND(NVL ( DECODE ( budget_indirect_flag, 'Y', total_cost_after     * line_item_indirects_flag * indirects , 0 ) , 0 )/100 , 2) AS total_cost_indirect,
    budget_currency,
    last_modified_by,
    last_modified_date,
    bgtsection_sequence,
    NVL (bgtcal_indirectcost, 0) AS bgtcal_indirectcost,
    lineitem_desc,
    ROUND((lineitem_sponsoramount ), 2) lineitem_sponsoramount,
    LINEITEM_APPLYPATIENTCOUNT,
    (ROUND((NVL(lineitem_sponsoramount, 0) ),2) - ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat) * bgtsection_patno, 0 ) , 2)) AS lineitem_variance,
    bgtsection_type,
    LINEITEM_OTHERCOST,
    FK_CODELST_COST_TYPE,
    COST_TYPE_DESC,
    lineitem_seq,
    linitem_sponsor_perpat,
    ROUND(DECODE (bgtsection_type, 'P',NVL (lineitem_direct_perpat, 0),'O',0 ) ,2) lineitem_direct_perpat,
    ROUND(DECODE (bgtsection_type, 'P',NVL ( DECODE ( budget_indirect_flag, 'Y' , total_pat_cost_after * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat, 0 ) ,'O',0), 2)              AS total_cost_per_pat,
    ROUND(NVL ( (DECODE ( budget_indirect_flag, 'Y', total_pat_cost_after                              * line_item_indirects_flag * indirects/100 , 0 ) + lineitem_direct_perpat) * bgtsection_patno, 0 ) , 2) AS total_cost_all_pat,
    DECODE(NVL(FK_EVENT,0),0,'other',DECODE(coverage_col1,null,'other',coverage_col1)) COST_CUSTOMCOL_COV,
    (SELECT codelst_custom_col1
    FROM sch_codelst
    WHERE pk_codelst = FK_CODELST_COST_TYPE
    ) COST_CUSTOMCOL,
    pk_budgetsec,
    budgetsec_fkvisit,
    pk_lineitem,
    lineitem_inpersec,
    lineitem_fkevent,
    bgtcal_excldsocflag,
    BUDGET_COMBFLAG,
    coverage_type,
    SUBCOST_ITEM_FLAG,
    FK_EVENT
  FROM
    (SELECT pk_budget,
      bgtcal_protid,
      pk_bgtcal,
      BUDGET_CALENDAR,
      bgtcal_prottype,
      DECODE(bgtcal_prottype, 'L',
      ( SELECT NAME FROM event_def WHERE event_id = bgtcal_protid
      ) , 'S',
      ( SELECT NAME FROM event_assoc WHERE event_id = bgtcal_protid
      ) , NULL, 'No Calendar') AS prot_calendar,
      budget_name,
      budget_version,
      budget_desc,
      study_number,
      fk_study,
      study_title,
      PI,
      Dept,
      prot_ver,
      qual_trial,
      site_name,
      bgtsection_name,
      bgtsection_patno,
      lineitem_name,
      NVL (total_cost, 0)                                                                                                                                                                                                                                                                                                                                            AS total_cost,
      ROUND ( (total_line_fringe       + (total_cost)) - DECODE ( bgtcal_discountflag, 0, 0, 2, 0, 1, (total_line_fringe + (total_cost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE ( bgtcal_discountflag, 0, 0, 1, 0, 2, (total_line_fringe + (total_cost) ) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 )                                     AS total_cost_after,
      ROUND ( (per_patient_line_fringe + (lineitem_othercost)) - DECODE ( bgtcal_discountflag, 0, 0, 2, 0, 1, (per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE ( bgtcal_discountflag, 0, 0, 1, 0, 2, (per_patient_line_fringe + (lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS total_pat_cost_after,
      total_line_fringe,
      per_patient_line_fringe,
      CATEGORY,
      category_seq,
      category_subtyp,
      lineitem_sponsorunit                                                                                                                                                                                                                  AS unit_cost,
      lineitem_clinicnofunit                                                                                                                                                                                                                AS number_of_unit,
      DECODE (lineitem_stdcarecost, 0, 0, lineitem_stdcarecost)                                                                                                                                                                             AS standard_of_care,
      lineitem_othercost                                                                                                                                                                                                                    AS cost_per_patient,
      lineitem_incostdisc                                                                                                                                                                                                                   AS cost_discount_on_line_item,
      DECODE ( bgtcal_discountflag, 0, 0, (lineitem_incostdisc * ( lineitem_othercost * DECODE ( category_subtyp, 'ctgry_sfaculty', NVL2 (bgtcal_frgbenefit, bgtcal_frgbenefit / 100 , 0),'ctgry_sclass',NVL2 (bgtcal_frgbenefit, bgtcal_frgbenefit / 100 , 0), 0 ) + lineitem_othercost) * bgtcal_discount / 100 ) ) AS per_pat_line_item_discount,
      DECODE ( bgtcal_discountflag, 0, 0, lineitem_incostdisc  * (total_cost * DECODE ( category_subtyp, 'ctgry_sfaculty', NVL2 (bgtcal_frgbenefit, bgtcal_frgbenefit / 100, 0),'ctgry_sclass',NVL2 (bgtcal_frgbenefit, bgtcal_frgbenefit / 100, 0), 0 ) + total_cost) * bgtcal_discount / 100 )                     AS total_cost_discount,
      lineitem_cptcode                                                                                                                                                                                                                      AS cpt_code,
      lineitem_tmid                                                                                                                                                                                                                         AS tmid,
      lineitem_cdm                                                                                                                                                                                                                          AS cdm,
      lineitem_applyindirects                                                                                                                                                                                                               AS line_item_indirects_flag,
      NVL (bgtcal_sponsorohead, 0)                                                                                                                                                                                                          AS indirects,
      bgtcal_sponsorflag                                                                                                                                                                                                                    AS budget_indirect_flag,
      NVL (bgtcal_frgbenefit, 0)                                                                                                                                                                                                            AS fringe_benefit,
      bgtcal_frgflag                                                                                                                                                                                                                        AS fringe_flag,
      NVL (bgtcal_discount, 0)                                                                                                                                                                                                              AS budget_discount,
      bgtcal_discountflag                                                                                                                                                                                                                   AS budget_discount_flag,
      budget_currency,
      last_modified_by,
      last_modified_date,
      bgtsection_sequence,
      bgtcal_indirectcost,
      lineitem_desc,
      lineitem_sponsoramount,
      LINEITEM_APPLYPATIENTCOUNT,
      bgtsection_type,
      LINEITEM_OTHERCOST,
      FK_CODELST_COST_TYPE,
      COST_TYPE_DESC,
      lineitem_seq,
      linitem_sponsor_perpat,
      ROUND ( (NVL (per_patient_line_fringe, 0) + (lineitem_othercost)) - DECODE( bgtcal_discountflag, 0,0,2,0,1, (per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ) + DECODE( bgtcal_discountflag, 0,0,1,0,2,(per_patient_line_fringe + ( lineitem_othercost)) * bgtcal_discount * lineitem_incostdisc / 100 ), 2 ) AS lineitem_direct_perpat,
      pk_budgetsec,
      budgetsec_fkvisit,
      pk_lineitem,
      lineitem_inpersec,
      lineitem_fkevent,
      bgtcal_excldsocflag,
      BUDGET_COMBFLAG,
      NVL(coverage_type,'Other') coverage_type,
      coverage_col1,
      SUBCOST_ITEM_FLAG,
      FK_EVENT,
      EVENT_TYPE
    FROM
      (SELECT pk_budget,
        bgtcal_prottype,
        bgtcal_protid,
        pk_bgtcal,
        BUDGET_CALENDAR,
        budget_name,
        budget_version,
        budget_desc,
        bgtsection_name,
        bgtsection_type,
         bgtsection_patno,
        lineitem_name,
        DECODE ( bgtsection_type, 'P', NVL (bgtsection_patno, 1) * lineitem_othercost * DECODE (lineitem_clinicnofunit, 0, 0, 1), 'O', lineitem_sponsorunit * lineitem_clinicnofunit ) AS total_cost,
        DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_sfaculty', DECODE ( bgtsection_type, 'P', NVL ( bgtsection_patno, 1) * lineitem_othercost , 'O', lineitem_sponsorunit * lineitem_clinicnofunit ) * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100,'ctgry_sclass',DECODE ( bgtsection_type, 'P', NVL ( bgtsection_patno, 1) * lineitem_othercost , 'O', lineitem_sponsorunit * lineitem_clinicnofunit ) * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100, 0 ) AS total_line_fringe,
        DECODE ( TRIM(
        (SELECT codelst_subtyp
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) ), 'ctgry_sfaculty', lineitem_othercost * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100,'ctgry_sclass',lineitem_othercost * bgtcal_frgbenefit * NVL (bgtcal_frgflag, 0)/100 , 0 ) AS per_patient_line_fringe,
        fk_codelst_category,
         lineitem_sponsorunit,
        lineitem_clinicnofunit,
         lineitem_stdcarecost,
         lineitem_rescost,
         lineitem_incostdisc,
        lineitem_cptcode,
         CATEGORY,
         category_seq,
        category_subtyp,
        lineitem_tmid,
        lineitem_cdm,
         lineitem_applyindirects,
         bgtcal_sponsorohead,
         bgtcal_sponsorflag,
         bgtcal_frgbenefit,
         bgtcal_frgflag,
         bgtcal_discount,
         bgtcal_discountflag,
         study_number,
         fk_study,
         study_title,
         (SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user = PI
        ) AS PI,
         (SELECT codelst_desc
        FROM er_codelst
        WHERE pk_codelst = Dept
        ) AS Dept,
        prot_ver,
        qual_trial,
        site_name,
         budget_currency,
         last_modified_by,
         last_modified_date,
        bgtsection_sequence,
         bgtcal_indirectcost,
        lineitem_desc,
         lineitem_sponsoramount,
        LINEITEM_APPLYPATIENTCOUNT,
        LINEITEM_OTHERCOST AS LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
         COST_TYPE_DESC,
        lineitem_seq,
        linitem_sponsor_perpat,
        pk_budgetsec,
       budgetsec_fkvisit,
        pk_lineitem,
         lineitem_inpersec,
         lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
         event_type,
         coverage_type,
         coverage_col1,
         SUBCOST_ITEM_FLAG,
        FK_EVENT
      FROM (SELECT pk_budget,
        bgtcal_prottype,
        bgtcal_protid,
        pk_bgtcal,
        BUDGET_CALENDAR,
        budget_name,
        budget_version,
        budget_desc,
        bgtsection_name,
        bgtsection_type,
        NVL (bgtsection_patno, 1) bgtsection_patno,
        lineitem_name,
        fk_codelst_category,
        lineitem_sponsorunit AS lineitem_sponsorunit,
        lineitem_clinicnofunit,
        lineitem_stdcarecost AS lineitem_stdcarecost,
        lineitem_rescost     AS lineitem_rescost,
        lineitem_incostdisc  AS lineitem_incostdisc,
        lineitem_cptcode,
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) AS CATEGORY,
        ( SELECT codelst_seq FROM sch_codelst WHERE pk_codelst = fk_codelst_category
        ) AS category_seq,
        (SELECT TRIM (codelst_subtyp)
        FROM sch_codelst
        WHERE pk_codelst = fk_codelst_category
        ) AS category_subtyp,
        lineitem_tmid,
        lineitem_cdm,
        NVL (lineitem_applyindirects, 0) lineitem_applyindirects,
        NVL (bgtcal_sponsorohead, 0) bgtcal_sponsorohead,
        NVL (bgtcal_sponsorflag, 0) bgtcal_sponsorflag,
        NVL (bgtcal_frgbenefit, 0) bgtcal_frgbenefit,
        NVL (bgtcal_frgflag, 0) bgtcal_frgflag,
        NVL (bgtcal_discount, 0) bgtcal_discount,
        NVL (bgtcal_discountflag, 0) bgtcal_discountflag,
        ( SELECT study_number FROM er_study WHERE pk_study = fk_study
        ) AS study_number,
        ( SELECT pk_study FROM er_study WHERE pk_study = fk_study
        ) AS fk_study,
        ( SELECT study_title FROM er_study WHERE pk_study = fk_study
        ) AS study_title,
        ( SELECT study_prinv FROM er_study WHERE pk_study = fk_study
        ) AS PI,
        ( SELECT fk_codelst_tarea FROM er_study WHERE pk_study = fk_study
        ) AS Dept,
		( SELECT STUDYID_ID FROM er_studyid WHERE er_studyid.fk_study = sch_budget.fk_study and FK_CODELST_IDTYPE =(select pk_codelst from ER_CODELST where CODELST_TYPE='studyidtype' and CODELST_SUBTYP = 'protver')) AS prot_ver,
		( SELECT STUDYID_ID FROM er_studyid WHERE er_studyid.fk_study = sch_budget.fk_study and FK_CODELST_IDTYPE =(select pk_codelst from ER_CODELST where CODELST_TYPE='studyidtype' and CODELST_SUBTYP = 'qualtrial')) AS qual_trial,
        ( SELECT site_name FROM er_site WHERE pk_site = fk_site
        ) AS site_name,
        (SELECT NVL (codelst_subtyp, codelst_desc)
        FROM sch_codelst
        WHERE pk_codelst = budget_currency
        ) AS budget_currency,
        (SELECT usr_firstname
          || ' '
          || usr_lastname
        FROM er_user
        WHERE pk_user = NVL ( sch_budget.last_modified_by, sch_budget.creator )
        ) AS last_modified_by,
        TO_CHAR ( NVL (sch_budget.last_modified_date, sch_budget.created_on ), PKG_DATEUTIL.F_GET_DATEFORMAT
        || ' hh:mi:ss' ) AS last_modified_date,
        bgtsection_sequence,
        bgtcal_indirectcost AS bgtcal_indirectcost,
        lineitem_desc,
        DECODE(NVL(LINEITEM_APPLYPATIENTCOUNT,'0'),'0' , lineitem_sponsoramount , ( lineitem_sponsoramount * NVL( bgtsection_patno,'1'))) AS lineitem_sponsoramount,
        LINEITEM_APPLYPATIENTCOUNT,
        (Case
        When(DECODE( bgtcal_prottype, 'L',
        (SELECT CODELST_CUSTOM_COL1
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_DEF
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'S',
        (SELECT CODELST_CUSTOM_COL1
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'other' )='soc' and bgtcal_excldsocflag=1) then
        0
        else
        (lineitem_sponsorunit * lineitem_clinicnofunit)
        end ) LINEITEM_OTHERCOST,
        FK_CODELST_COST_TYPE,
        (SELECT codelst_desc
        FROM sch_codelst
        WHERE pk_codelst = FK_CODELST_COST_TYPE
        ) COST_TYPE_DESC,
        lineitem_seq,
        NULL AS linitem_sponsor_perpat,
        pk_budgetsec,
        fk_visit budgetsec_fkvisit,
        pk_lineitem,
        NVL (lineitem_inpersec, 0) lineitem_inpersec,
        sch_lineitem.fk_event lineitem_fkevent,
        bgtcal_excldsocflag,
        BUDGET_COMBFLAG,
        (SELECT EVENT_TYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event) AS event_type,
        DECODE( bgtcal_prottype, 'L',
        (SELECT CODELST_SUBTYP
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_DEF
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'S',
        (SELECT CODELST_SUBTYP
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , NULL) AS coverage_type,
        DECODE( bgtcal_prottype, 'L',
        (SELECT CODELST_CUSTOM_COL1
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_DEF
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'S',
        (SELECT CODELST_CUSTOM_COL1
        FROM sch_codelst
        WHERE pk_codelst =
          (SELECT FK_CODELST_COVERTYPE
          FROM EVENT_ASSOC
          WHERE event_id= sch_lineitem.fk_event
          )
        ) , 'other' ) AS coverage_col1,
        NVL(SUBCOST_ITEM_FLAG,0)       AS SUBCOST_ITEM_FLAG,
        FK_EVENT
      FROM sch_bgtcal,
        sch_budget,
        sch_bgtsection,
        sch_lineitem
      WHERE pk_budget                   = fk_budget
      AND pk_bgtcal                     = fk_bgtcal
      AND NVL (bgtsection_delflag, 'N') = 'N'
      AND pk_budgetsec                  = fk_bgtsection(+)
      AND NVL (lineitem_delflag, 'N')   = 'N')
      )
      WHERE NVL(EVENT_TYPE,'A')         <> 'U'
    );
/
 
CREATE OR REPLACE SYNONYM ERES.ERV_BUDGET_COVTYPE FOR ERV_BUDGET_COVTYPE;


CREATE OR REPLACE SYNONYM EPAT.ERV_BUDGET_COVTYPE FOR ERV_BUDGET_COVTYPE;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_BUDGET_COVTYPE TO ERES;