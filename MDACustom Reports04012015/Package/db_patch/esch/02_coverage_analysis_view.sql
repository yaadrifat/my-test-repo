CREATE OR REPLACE FORCE VIEW "ESCH"."ERV_COVANAL" ("INTERSECTION_ID", "EVENT_ID", "ORG_ID", "CHAIN_ID", "EVENT_TYPE", "NAME", "COST", "DESCRIPTION", "FLAG", "FK_VISIT", "VISIT_NO", "DISPLACEMENT", "EVENT_CATEGORY", "FK_CODELST_COVERTYPE", "COVERAGE_NOTES", "EVENT_CPTCODE") AS 
  SELECT 0            AS INTERSECTION_ID,
  0                 AS EVENT_ID,
  0                 AS ORG_ID,
  fk_protocol       AS CHAIN_ID,
  ''                AS EVENT_TYPE,
  visit_name        AS NAME,
  0                 AS COST,
  DESCRIPTION       AS DESCRIPTION,
  'V'               AS flag,
  pk_protocol_visit AS FK_VISIT,
  VISIT_NO,
  DISPLACEMENT,
  '' AS EVENT_CATEGORY,
  0  AS FK_CODELST_COVERTYPE,
  '' AS COVERAGE_NOTES,
  '' AS EVENT_CPTCODE
FROM sch_protocol_visit
UNION
SELECT EVENT_ID AS INTERSECTION_ID,
  (SELECT EVENT_ID
  FROM event_assoc
  WHERE CHAIN_ID = a.CHAIN_ID
  AND COST       = a.COST
  AND FK_VISIT  IS NULL
  AND EVENT_TYPE = 'A'
  ),
  ORG_ID,
  CHAIN_ID,
  EVENT_TYPE,
  trim(NAME) AS NAME,
  COST,
  DESCRIPTION,
  'E' AS flag,
  FK_VISIT,
  0 AS VISIT_NO,
  0 AS DISPLACEMENT,
  EVENT_CATEGORY,
  FK_CODELST_COVERTYPE,
  COVERAGE_NOTES,
  (SELECT EVENT_CPTCODE
  FROM event_assoc
  WHERE CHAIN_ID = a.CHAIN_ID
  AND COST       = a.COST
  AND FK_VISIT  IS NULL
  AND EVENT_TYPE = 'A'
  ) EVENT_CPTCODE
FROM event_assoc a
WHERE a.event_type = 'A';
/

CREATE OR REPLACE SYNONYM ERES.ERV_COVANAL FOR ERV_COVANAL;

CREATE OR REPLACE SYNONYM EPAT.ERV_COVANAL FOR ERV_COVANAL;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_COVANAL TO ERES;