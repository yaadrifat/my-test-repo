*********************************************************************************
This Patch is for MDA Custom Reports and the changes are MDA Specific only.
*********************************************************************************
1. "Research Type Ticket" reports are implemented  as per the specifications mentioned in the "PRS Physician Charge Research Ticket Report v0.3 - Approved.docx" and "Technical Charge Research Ticket Report v0.3 - Approved.docx".
2. Coverage Analysis Report  is implemented as per the specifications mentioned in "LindF Coverage Analysis Report vR1".

This patch consists of following files:

1. JSP Files: 
	1.server\eresearch\deploy\velos.ear\velos.war\jsp\repGetPdf.jsp
	2.server\eresearch\deploy\velos.ear\velos.war\jsp\reportDisplay.jsp 
	3.server\eresearch\deploy\velos.ear\velos.war\jsp\budget.jsp
2. JAR File:
	1. server\eresearch\lib\itextpdf-5.5.2.jar
	1. server\eresearch\lib\xmlworker-5.4.3.jar
3. Java Class File:
	1. server\eresearch\deploy\velos.ear\velos-main.jar\com\velos\eres\service\util\TableHeader.class
              2. server\eresearch\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util\LC.class
4. IMAGES File
	1. server\eresearch\deploy\velos.ear\velos.war\jsp\images\pdf.GIF

5.ereshome:  
	1. labelBundle.properties
6. DB Script files:
    eres:
	1) 01_ER_REPORT_20009.sql
	2) 02_ER_REPORT_20010.sql
	3) 03_CODELST_INSERT.sql 
	4) 04_er_repfilter.sql
	5) 05_er_repfiltermap.sql
	6) 06_Create_Report_20011.sql
	7) 07_Update_Report_20011.sql
	8) 08_Update_Report_20009.sql
	9) 09_Update_Report_20010.sql
	10) 10_grant_tables.sql
  esch:
 	1) 01_ERV_BUDGET_COVTYP.sql
	2) 02_coverage_analysis_view.sql	

    xsl:
				
        	1) 20009.xsl
	2) 20010.xsl
	3) 20011.xsl
	4) GENREPXSL.ctl
	5) loadxsl.bat   


Note: PLease follow the Instructions mentioned in the "Configuration Doc for MDA Reports.doc" 


Steps to deploy the Patch:
- Execute the DB scripts in the 'db_patch' folder on the database in the following order.
 1. eres  2. esch 3. xsl

- Replace the JSP files, IMAGES files & add Jar files on the application server with the files in this 'server' folder.
- Clear the tmp and work folders under JBoss as usual and changes would be deployed on eResearch application.

Assumptions For Research Type Ticket:
---------------------------------------------
1. In "ER_REPORT_20009.sql" and "ER_REPORT_20010.sql", the Pk_report is hardcoded as "20009" and "20010" respectively. They can further be configured as per the environment.
2. Please note that if there is any change in point one above, the same pk_report should be reflected on 20009.xsl and 20010.xsl also.
3. To show the Form Fields in the report, user need to use the FK_FORM from the ER_FORMSLINEAR table from its respective Study. The Data in the Form fields such as Sponsor, Funding Source and Account will be coming from the ER_FORMSLINEAR table and these data elements are configured as col1, col2 and col3. These col1, col2 and col3 are the fields from the ER_FORMSLINEAR table and its data is mapped to the fields Sponsor, Funding Source and Account in the Report. One needs to make sure that col1 is mapped to Sponsor, col2 to Funding Source and col3 to Account.
4. Please configure CODELST_TYPE = "coverage_type" and COVERAGE_SUBTYP = "RO" at your database in the table SCH_CODELST in order to view the data in the report. Because these reports will be showing data for the events having COVERAGE TYPE = "RO".



Assumptions for Coverage Analysis Report:
------------------------------------------------
1. In "ER_REPORT_20011.sql", the Pk_report is hardcoded as "20011" respectively. It can further be configured as per the environment.
2. Please note that if there is any change in point one above, the same pk_report should be reflected 20011.xsl also.
3. In order to view the Reports budget Coverage Analysis , We need to configure "research","soc" and "other" in CODELST_CUSTOM_COL1 for CODELST_TYPE = "coverage_type"   in sch_codelst table in ESCH schema as per your convenience.
4. This report is shown from by selecting "Study" type report.
5. Below coverage type report is partialy implimented.
6. PDF functionalty is working but not with both side pagination.

*******************************************************************************************************************************************************

