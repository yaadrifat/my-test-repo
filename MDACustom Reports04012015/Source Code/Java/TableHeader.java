package com.velos.eres.service.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;;


public class TableHeader extends PdfPageEventHelper {
	String repLogName;
	String repDate;
	
	public void setRepLogName(String repLogName) {
		this.repLogName = repLogName;
	}

	public void setRepDate(String repDate) {
		this.repDate = repDate;
	}
    /** The header text. */
    String header;
    /** The template with the total number of pages. */
    PdfTemplate total;
    
    /**
     * Allows us to change the content of the header.
     * @param header The new header String
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Creates the PdfTemplate that will hold the total number of pages.
     */
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30, 16);
    }
    
    /**
     * Adds a header to every page
     */
    public void onEndPage(PdfWriter writer, Document document) {
        PdfPTable table = new PdfPTable(5);
        try {
            table.setWidths(new int[]{20,20,20,37,3});
            table.setTotalWidth(840);
            //table.setFooterRows(3);
            table.setLockedWidth(true);
            table.getDefaultCell().setFixedHeight(20);
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setColspan(2);
            table.addCell(repDate.trim());
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.getDefaultCell().setColspan(3);
            table.addCell("UT MDACC-Confidential");
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setColspan(2);
            table.addCell(repLogName.trim());
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(String.format("Page %d of", writer.getPageNumber()));
            PdfPCell cell = new PdfPCell(Image.getInstance(total));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            table.writeSelectedRows(-1, -1, 1, 35, writer.getDirectContentUnder());
        }
        catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    /**
     * Fills out the total number of pages before the document is closed.
     */
    public void onCloseDocument(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                2, 2, 0);
    }
}
