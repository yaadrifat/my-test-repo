set define off;

delete from ER_CODELST where CODELST_TYPE = 'other_rec_cat' and CODELST_SUBTYP ='phy_assess'; 

delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='ac'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='bc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='cc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='dc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='cbu_assessment'; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,2,'02_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#611');

commit;