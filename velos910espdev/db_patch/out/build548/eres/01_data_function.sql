alter table er_codelst modify LAST_MODIFIED_DATE default NULL;

CREATE OR REPLACE FUNCTION ERES."F_GET_MCFIELDVAL" (fldValue VARCHAR2)
RETURN VARCHAR2
IS
  v_new_val VARCHAR2(4000);
BEGIN
     
     if nvl(length(fldValue),0) = 0 then
         return fldValue;
     end if;
     
     begin
         select 
         pkg_util.f_getvaluefromdelimitedstring('[VELSEP1]'||fldValue,1,'[VELSEP1]')
         into v_new_val
         from dual;
    exception when others then
        v_new_val  := fldValue;
    end ;         
         
  RETURN v_new_val ;
END ;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,91,1,'01_data_function.sql',sysdate,'8.10.0 Build#548');

commit;