set define off;

create or replace
TRIGGER ERES.ER_CB_NOTES_BI0 BEFORE INSERT ON CB_NOTES
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

DECLARE
  ERID NUMBER(10);
  USR VARCHAR(200); 
  RAID NUMBER(10);
  INSERT_DATA varchar2(4000);
  
BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;  
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CB_NOTES', ERID, 'I',USR);  
  INSERT_DATA := :NEW.PK_NOTES || '|' ||
  TO_CHAR(:NEW.ENTITY_ID) || '|' ||
  TO_CHAR(:NEW.ENTITY_TYPE) || '|' ||
  TO_CHAR(:NEW.FK_NOTES_TYPE) || '|' ||
  TO_CHAR(:NEW.FK_NOTES_CATEGORY) || '|' ||
  TO_CHAR(:NEW.NOTE_ASSESSMENT) || '|' ||
  TO_CHAR(:NEW.AVAILABLE_DATE) || '|' ||
  TO_CHAR(:NEW.EXPLAINATION) || '|' ||
  TO_CHAR(:NEW.NOTES) || '|' ||
  TO_CHAR(:NEW.FK_REASON) || '|' ||
  TO_CHAR(:NEW.KEYWORD) || '|' ||
  TO_CHAR(:NEW.SEND_TO) || '|' ||
  TO_CHAR(:NEW.VISIBILITY) || '|' ||
  TO_CHAR(:NEW.CREATOR) || '|' ||
  TO_CHAR(:NEW.CREATED_ON) || '|' ||
  TO_CHAR(:NEW.IP_ADD) || '|' ||
  TO_CHAR(:NEW.LAST_MODIFIED_BY) || '|' ||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE) || '|' ||
  TO_CHAR(:NEW.RID) || '|' ||
  to_char(:NEW.DELETEDFLAG) || '|' ||
  TO_CHAR(:NEW.REVIEWED_BY) || '|' ||
  TO_CHAR(:NEW.CONSULTED_BY) || '|' ||
  TO_CHAR(:NEW.APPROVED_BY) || '|' ||
  TO_CHAR(:NEW.AMENDED) || '|' ||
  TO_CHAR(:NEW.FK_CBU_STATUS) || '|' ||
  TO_CHAR(:NEW.FLAG_FOR_LATER) || '|' ||
  TO_CHAR(:NEW.NOTE_SEQ) || '|' ||
  TO_CHAR(:NEW.COMMENTS) || '|' ||
  TO_CHAR(:NEW.SUBJECT) || '|' ||
  TO_CHAR(:NEW.REQUEST_DATE) || '|' ||
  TO_CHAR(:NEW.REQUEST_TYPE) || '|' ||
  TO_CHAR(:NEW.EXPLAINATION_TU);                                  
       INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
                 
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,152,16,'16_ER_CB_NOTES_BI0.sql',sysdate,'9.0.0 Build#609');

commit;
