set define off;

CREATE OR REPLACE PROCEDURE ERES."SP_OBJECTSHAREWITH" (p_id NUMBER,p_obj_number VARCHAR2 ,p_pk_sharewith_ids VARCHAR2,
			    p_sharewith_type CHAR, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER, p_Commit_flag VARCHAR2 DEFAULT 'Y')
AS
 /****************************************************************************************************
   ** Procedure
   ** Author: Anu Khanna 29/April/04
   ** Input parameter: id - PK - form id,calendar id,report id
   ** Input parameter: comma separated String of the pks of the entity (group/study/organization) to share the form with
   ** Input parameter: the entity  type (group/study/organization)
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   **
   ** Output parameter: 0 for successful completion, -1 for error
   ** Changed by Vishal on 08/30/04
   ** put a check for object_number to avoid deletion of cross module data.
    **
	** Modified by Sonia Abrol, 06/02/2005, insert the parent record's PK as FK_OBJECTSHARE in child records (for type=U). This way, user records can be related with their parent
   **/
   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'sp_objectsharewith', pLEVEL  => PLOG.LFATAL);
   v_pk_sharewith_id NUMBER;
   V_STR      VARCHAR2 (2001) DEFAULT p_pk_sharewith_ids || ',';
   V_PARAMS   VARCHAR2 (2001) DEFAULT p_pk_sharewith_ids || ',';
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   v_date_field  NUMBER         := 0;
   v_date_formfld NUMBER         := 0;
   v_parent NUMBER;
   V_P_OR_U CHAR := 'U';
BEGIN
   DELETE FROM ER_OBJECTSHARE
           WHERE  FK_OBJECT = p_id AND object_number=p_obj_number;
     LOOP
       V_CNT := V_CNT + 1;
       V_POS := INSTR (V_STR, ',');
       V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
        EXIT WHEN V_PARAMS IS NULL;
	  v_pk_sharewith_id := TO_NUMBER(V_PARAMS);
     BEGIN
	       --insert a record for the organization/study/group

		   SELECT SEQ_ER_OBJECTSHARE.NEXTVAL INTO v_parent FROM dual;

		   INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		  VALUES (v_parent,p_obj_number,p_id,v_pk_sharewith_id,p_sharewith_type,p_user,'N',SYSDATE,p_ipadd);

		 EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
		-- Plog.DEBUG('ERROR'||SQLERRM);
		  Plog.FATAL(pCTX,'ERROR'||SQLERRM);
         o_ret:=-1;
 	   RETURN;
    END ;--end of update begin
	  IF p_sharewith_type = 'S' THEN
	  	 -- insert records for the entire study team
		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,FK_OBJECTSHARE)
		 SELECT  SEQ_ER_OBJECTSHARE.NEXTVAL,p_obj_number,p_id,fk_user,'U',p_user,'N',SYSDATE,p_ipadd,v_parent
		 FROM ER_STUDYTEAM
		 WHERE FK_STUDY = v_pk_sharewith_id
		 AND NVL(STUDY_TEAM_USR_TYPE,'D')='D' ;
	   ELSIF p_sharewith_type = 'O' THEN
      	  -- insert records for the organization users
		  -- Since 'All Users in an Organization' should apply to user's primary organization only, insert
		  -- only those users whose primary organization is the one which has been selected
		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,FK_OBJECTSHARE)
		 SELECT  SEQ_ER_OBJECTSHARE.NEXTVAL,p_obj_number,p_id,pk_user,'U',p_user,'N',SYSDATE,p_ipadd,v_parent
		 FROM ER_USER
		 WHERE FK_SITEID = v_pk_sharewith_id ;
        ELSIF p_sharewith_type = 'G' THEN
       	  -- insert records for the group users
   		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,FK_OBJECTSHARE)
		 SELECT  SEQ_ER_OBJECTSHARE.NEXTVAL,p_obj_number,p_id,fk_user,'U',p_user,'N',SYSDATE,p_ipadd,v_parent
		 FROM ER_USRGRP
		 WHERE FK_GRP = v_pk_sharewith_id ;
          ELSIF p_sharewith_type = 'A' THEN
       	  -- insert records for all account users
   		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,FK_OBJECTSHARE)
		 SELECT  SEQ_ER_OBJECTSHARE.NEXTVAL,p_obj_number,p_id,pk_user,'U',p_user,'N',SYSDATE,p_ipadd,v_parent
		 FROM ER_USER
		 WHERE FK_ACCOUNT = v_pk_sharewith_id ;
          ELSIF p_sharewith_type = 'P' THEN
       	  -- insert records for Private
         IF p_obj_number > 3 and p_obj_number < 7 THEN
            V_P_OR_U := 'P';
         END IF;
   		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,FK_OBJECTSHARE)
		 VALUES(SEQ_ER_OBJECTSHARE.NEXTVAL,p_obj_number,p_id,p_user,V_P_OR_U,p_user,'N',SYSDATE,p_ipadd,v_parent);
	 END IF;
	V_STR := SUBSTR (V_STR, V_POS + 1);
  END LOOP; --v_cnt loop

  -- SV, 9/29/04, modified to be able to call this procedure from other procs. If we are calling from other procedures, then we cannot commit here.
  -- The new parameter has a default value, so it doesn't need to be passed in, thus maintaining backward compatibility with calls from client.
  -- When calling from other procs, we will have to pass in extra parameter 'N' to p_commit_flag to skip commit.
   -- default is to commit
	IF (p_commit_flag = 'Y') THEN
	  COMMIT;
	END IF;

END; --end of sp_objectshare
/




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,6,'06_SP_OBJECTSHAREWITH.sql',sysdate,'9.0.0 Build#635');

commit;
