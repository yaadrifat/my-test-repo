LOAD DATA
INFILE *
INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
 FK_REPORT,
 FK_ACCOUNT,
 REPXSL_NAME,
 xsl_file filler char,
"REPXSL_FO" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE'
)
BEGINDATA
165,165,0,CBU Assessment,165.xsl
162,162,0,Cord Blood Bank Inventory by Age of CBU,162.xsl
163,163,0,NCBI Cord Blood Bank Inventory by Age of CBU,163.xsl