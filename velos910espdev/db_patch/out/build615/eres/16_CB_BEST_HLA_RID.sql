set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_BEST_HLA WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_BEST_HLA  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,16,'16_CB_BEST_HLA_RID.sql',sysdate,'9.0.0 Build#615');

commit;