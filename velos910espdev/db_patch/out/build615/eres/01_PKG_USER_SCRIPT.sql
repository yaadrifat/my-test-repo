create or replace
PACKAGE        "PKG_USER"
AS
  PROCEDURE SP_SITETREE
     (p_account IN NUMBER,
	 p_user IN NUMBER,
	 p_site_cursor OUT Types.cursorType);
  FUNCTION F_GETCHILDSITES
     (p_parentsite NUMBER,
	 p_site_table SITEROWS_TABLE,
	 p_account NUMBER,
	 p_user NUMBER,
	 p_indent NUMBER,
	 p_main_parent NUMBER)
    RETURN  SITEROWS_TABLE;
   PROCEDURE SP_UPDATESITERIGHTS(p_pkuser_sites ARRAY_STRING,p_rights ARRAY_STRING,p_user VARCHAR2,p_ipadd VARCHAR2,o_ret OUT NUMBER);
   PROCEDURE SP_UPDATESITERIGHTS(p_pkuser_sites ARRAY_STRING,p_rights ARRAY_STRING,p_user VARCHAR2,p_userId VARCHAR2,p_ipadd VARCHAR2,o_ret OUT NUMBER);
   PROCEDURE SP_CREATE_USERSITE_DATA
   (
      p_user IN NUMBER,
	 p_account IN NUMBER,
	 p_def_group IN NUMBER,
	 p_old_site IN NUMBER,
	 p_def_site IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 p_mode IN VARCHAR2,
	 o_success OUT NUMBER
   );
   PROCEDURE SP_CREATE_FORMSHAREWITH_DATA
   (
      p_user IN NUMBER,
	 p_account NUMBER,
	 p_site IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 o_success OUT NUMBER
   );
   PROCEDURE SP_CREATE_USERFORMSHARE_DATA
   (
      p_user IN NUMBER,
 	 p_oldsite IN NUMBER,
	 p_newsite IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 o_success OUT NUMBER
   );
   PROCEDURE SP_CREATE_STUDYSITE_DATA
   (
     p_user IN NUMBER,
	 p_account IN NUMBER,
	 p_study IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 o_success OUT NUMBER
   );
   PROCEDURE SP_STUDYSITETREE
     (p_account IN NUMBER,
	 p_user IN NUMBER,
	 p_study IN NUMBER,
	 p_site_cursor OUT Types.cursorType);
  FUNCTION F_GETCHILDSTUDYSITES
     (p_parentsite NUMBER,
	 p_site_table SITEROWS_TABLE,
	 p_account NUMBER,
	 p_user NUMBER,
	 p_study NUMBER,
	 p_indent NUMBER,
	 p_main_parent NUMBER)
    RETURN  SITEROWS_TABLE;

	PROCEDURE SP_UPDATESTUDYSITERIGHTS
	(p_pk_study_sites IN ARRAY_STRING,
	p_site_ids IN ARRAY_STRING,
	p_rights IN ARRAY_STRING,
	p_study_id IN NUMBER,
	p_user IN VARCHAR2,
	p_ipadd IN VARCHAR2,
	o_ret OUT NUMBER,p_creator IN Varchar2);



	PROCEDURE SP_DELETE_SITE_AND_USERS(p_study_id NUMBER , p_acc_id NUMBER, p_site_id NUMBER, p_user NUMBER);
	FUNCTION f_chk_right_for_studysite(p_study NUMBER,p_user NUMBER,p_site NUMBER) RETURN NUMBER;
	FUNCTION f_chk_right_for_patprotsite(p_patprot NUMBER,p_user NUMBER) RETURN NUMBER;
	FUNCTION f_chk_studyright_using_pat(p_pat NUMBER,p_study NUMBER , p_user NUMBER) RETURN NUMBER;

		PROCEDURE SP_UPDATE_START_END_DATES(p_study_id NUMBER, userId NUMBER ) ;

	FUNCTION f_right_forpatsites(p_pat NUMBER,p_user NUMBER) RETURN NUMBER;

	/* added by sonia abrol, 12/21/06, to get user's right for anAdHoc query*/
	FUNCTION f_getUserAdHocRight(p_user  NUMBER , p_pk_dynrep NUMBER) RETURN NUMBER;

	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_USER', pLEVEL  => Plog.LFATAL);

	/* added by sonia abrol, 06/09/08, to get user's right for anAdHoc query*/
	PROCEDURE SP_deactivate_user
	(p_user IN NUMBER,
	p_creator IN VARCHAR2,
	p_ipadd IN VARCHAR2,
	o_ret OUT NUMBER);

  /* Added by Bikash to 07/04/2011 ,to notify user about reset credential information
   It takes the message from msgtxt and saves it in sch_dispatchmsg which is fetched by the velosmailservice*/
	PROCEDURE SP_NOTIFY_USER_INFO(p_word VARCHAR2,p_url VARCHAR2,p_eSign VARCHAR2, userId NUMBER,userLogin VARCHAR2,infoFlag VARCHAR2,o_ret OUT NUMBER);
  
  /* Added by Yogesh on 01/30/2012 , for Garuda new functionality of attaching multiple groups to one organization*/
	PROCEDURE SP_ATTACH_USER_SITE_GROUPS(
    p_userid IN NUMBER ,
    p_fkSiteIds ARRAY_STRING,
    p_fkUserGrpIds ARRAY_STRING,
    p_pkDel ARRAY_STRING,
    p_ipadd   IN VARCHAR2,
    p_creator IN VARCHAR2,
    o_ret OUT NUMBER
);
END Pkg_User;
/

CREATE OR REPLACE SYNONYM ESCH.PKG_USER FOR PKG_USER;


CREATE OR REPLACE SYNONYM EPAT.PKG_USER FOR PKG_USER;


GRANT EXECUTE, DEBUG ON PKG_USER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_USER TO ESCH;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,1,'01_PKG_USER_SCRIPT.sql',sysdate,'9.0.0 Build#615');

commit;
