CREATE OR REPLACE PROCEDURE        "SP_DELETEASSOCEVENTS" (
p_protocol_id IN NUMBER, p_duration IN NUMBER DEFAULT 0
)
AS
v_arr_visits ARRAY_STRING := ARRAY_STRING();
v_arr_displacements ARRAY_STRING := ARRAY_STRING();
i number;
visCount number default 0;
BEGIN
--SV, 8/19/04, he default value above makes sure, current usage of this proc. isn't affected.
--modified by Sonia Abrol, 12/06/06 , delete the extra visits too!


IF p_duration = 0 THEN
   -- SV, 8/19/04, original Statement.
   DELETE FROM  EVENT_ASSOC WHERE chain_id = p_protocol_id AND event_type='A';

   -- delete visits
   DELETE FROM SCH_PROTOCOL_VISIT WHERE fk_protocol =  p_protocol_id;

ELSE
	i := 1;
	FOR vis  IN (SELECT PK_PROTOCOL_VISIT, pkg_reschedule.f_get_displacement(p_protocol_id, PK_PROTOCOL_VISIT) as disp
    	FROM SCH_PROTOCOL_VISIT WHERE fk_protocol =  p_protocol_id
    	AND pkg_reschedule.f_get_displacement(p_protocol_id, PK_PROTOCOL_VISIT) > p_duration)
  	LOOP
    	v_arr_visits.EXTEND;
    	v_arr_displacements.EXTEND;

		v_arr_visits(i) := vis.PK_PROTOCOL_VISIT;
    	v_arr_displacements(i) := vis.disp;
    	i := i+1;
		-- When duration is passed in, delete all events that have displacement outside of the duration.
   		DELETE FROM  EVENT_ASSOC WHERE chain_id = p_protocol_id AND event_type='A' AND
   		fk_visit = vis.PK_PROTOCOL_VISIT;
	END LOOP;
	
	visCount := i-1;
  
	FOR i IN 1..visCount
	LOOP
    	-- delete visits
   		DELETE FROM SCH_PROTOCOL_VISIT WHERE fk_protocol =  p_protocol_id
   		AND PK_PROTOCOL_VISIT = v_arr_visits(i);
	END LOOP;
END IF;

COMMIT;
END;
/


CREATE OR REPLACE SYNONYM ERES.SP_DELETEASSOCEVENTS FOR SP_DELETEASSOCEVENTS;


CREATE OR REPLACE SYNONYM EPAT.SP_DELETEASSOCEVENTS FOR SP_DELETEASSOCEVENTS;


GRANT EXECUTE, DEBUG ON SP_DELETEASSOCEVENTS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DELETEASSOCEVENTS TO ERES;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,2,'02_sp_deleteassocevents.sql',sysdate,'9.0.0 Build#615');
