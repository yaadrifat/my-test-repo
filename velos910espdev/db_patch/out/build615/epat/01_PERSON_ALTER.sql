set define off;
alter table PERSON modify  PERSON_ALTID  varchar2(50);
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,1,'01_PERSON_ALTER.sql',sysdate,'9.0.0 Build#615');

commit;