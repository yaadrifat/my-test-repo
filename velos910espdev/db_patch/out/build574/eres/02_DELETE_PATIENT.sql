set define off;

create or replace
PROCEDURE      "DELETE_PATIENT" (p_pat IN NUMBER)
IS

 v_patient number;
 v_stat varchar(2);
 BEGIN
   /******************** ESCH USER*********************/
--Added by Gopu to fix the bugzilla issue #2656
       UPDATE sch_msgtran SET msg_status=2 WHERE fk_patprot IN(SELECT pk_patprot FROM ER_PATPROT WHERE fk_per=p_pat) AND msg_status=0;



     --KM--#3318
     DELETE FROM SCH_EVRES_TRACK WHERE FK_EVENTSTAT IN (SELECT PK_EVENTSTAT FROM SCH_EVENTSTAT WHERE FK_EVENT IN (SELECT EVENT_ID FROM SCH_EVENTS1 WHERE FK_PATPROT IN (SELECT PK_PATPROT FROM ER_PATPROT WHERE FK_PER=P_PAT)));

     DELETE FROM SCH_EVENTSTAT WHERE FK_EVENT IN (SELECT EVENT_ID FROM SCH_EVENTS1 WHERE FK_PATPROT IN      (SELECT PK_PATPROT FROM ER_PATPROT WHERE  FK_PER=P_PAT));

     DELETE FROM SCH_ADVERSEVE WHERE FK_PER = p_pat ;

       DELETE FROM sch_alertnotify WHERE FK_PATPROT IN
       (SELECT pk_patprot FROM ER_PATPROT WHERE FK_PER = p_pat);

        DELETE FROM sch_crfnotify
        WHERE fk_crf IN
         (SELECT pk_crf FROM sch_crf
           WHERE FK_PATPROT IN
              (SELECT pk_patprot FROM
               ER_PATPROT WHERE FK_PER = p_pat)
           );

         DELETE FROM sch_crfstat
         WHERE fk_crf IN
          (SELECT pk_crf FROM sch_crf
            WHERE FK_PATPROT IN
               (SELECT pk_patprot FROM
                ER_PATPROT WHERE FK_PER = p_pat)
               );

        DELETE FROM SCH_CRF
        WHERE FK_PATPROT IN
          (SELECT pk_patprot FROM
           ER_PATPROT WHERE FK_PER = p_pat);
           DELETE FROM sch_events1
          WHERE FK_PATPROT IN
              (SELECT pk_patprot FROM
               ER_PATPROT WHERE FK_PER = p_pat);
   /******************** END OF ESCH USER*********************/
   /******************** ERES USER*********************/

  DELETE FROM ER_MILENOTLOG
  WHERE  FK_PATPROT IN
        (SELECT pk_patprot FROM
         ER_PATPROT WHERE FK_PER = p_pat);


  DELETE FROM ER_PATSTUDYSTAT
  WHERE fk_per = p_pat;

  FOR i IN (  SELECT fk_formlib, COUNT(*) ct FROM ER_PATFORMS WHERE FK_PER=P_PAT
	  	 	GROUP BY fk_formlib
)
LOOP
			 UPDATE ER_LINKEDFORMS
      	    SET lf_datacnt = (NVL(lf_datacnt,0) - i.ct) WHERE fk_formlib = i.fk_formlib;
END LOOP;


DELETE FROM ER_PATFORMS WHERE fk_per=p_pat;

DELETE FROM ER_FORMSLINEAR WHERE ID=p_pat AND form_type = 'P';

  DELETE FROM ER_PATPROT
    WHERE fk_per = p_pat;

 DELETE FROM ER_SAVEDREP
 WHERE fk_pat = p_pat;

 DELETE FROM ER_PATLABS WHERE fk_per = p_pat;

 --DELETE FROM ER_PER
 --WHERE pk_per = p_pat;

 /******************** END OF ERES USER*********************/
  /******************** PAT USER*********************/
 DELETE FROM PAT_PERAPNDX
     WHERE fk_per = p_pat;

--Added by Manimaran to delete the orphan records in the pat_perid table
 DELETE FROM PAT_PERID WHERE fk_per=p_pat;
 DELETE FROM person
 WHERE pk_person = p_pat;
  /******************** END OFPAT USER*********************/
/**JM: 042006 added **/
 DELETE FROM ER_PATFACILITY WHERE FK_PER = p_pat;
--commit;

--
--Sonia Abrol, 07/23/07 - remove all portal logins for the patient

delete from er_portal_logins
where pl_id = P_PAT and pl_id_type = 'P' ;


--JM: 06Apr2011: #6000
for s in (select FK_PORTAL from ER_PORTAL_POPLEVEL where PP_OBJECT_TYPE ='P' 
and  PP_OBJECT_ID = p_pat )

loop
 select count(PP_OBJECT_ID) into v_patient from ER_PORTAL_POPLEVEL where PP_OBJECT_TYPE ='P' 
  and FK_PORTAL = s.fk_portal ;

 select PORTAL_STATUS into v_stat from er_portal where pk_portal=s.fk_portal;

 if (v_patient =1 and (v_stat='A' or v_stat='D')) then
  sp_delete_portal(s.fk_portal); ----------only when A/D status of the portal considered
 end if;

end loop;

 delete from ER_SPECIMEN_STATUS where FK_SPECIMEN = (select pk_specimen from er_specimen where fk_per=p_pat);
 delete from er_specimen where fk_per=p_pat;

 DELETE FROM ER_PER 
 WHERE pk_per = p_pat; 

 
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,117,2,'02_DELETE_PATIENT.sql',sysdate,'8.10.0 Build#574');

commit;