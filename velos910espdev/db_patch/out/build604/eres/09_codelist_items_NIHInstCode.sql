set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AA','AA','N',1);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AA inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AE';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AE','AE','N',2);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AE inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AE already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AF';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AF','AF','N',3);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AF inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AF already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AG';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AG','AG','N',4);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AG inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AG already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AI','AI','N',5);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AI inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AM','AM','N',6);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AO';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AO','AO','N',7);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AO inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AO already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AR','AR','N',8);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'AT';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','AT','AT','N',9);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AT inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:AT already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'BC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','BC','BC','N',10);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:BC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:BC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'BX';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','BX','BX','N',11);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:BX inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:BX already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CA','CA','N',12);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CA inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CB','CB','N',13);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CB inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CD','CD','N',14);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CE';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CE','CE','N',15);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CE inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CE already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CH','CH','N',16);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CI','CI','N',17);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CI inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CK';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CK','CK','N',18);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CK inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CK already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CL';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CL','CL','N',19);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CL inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CL already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CM','CM','N',20);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CN';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CN','CN','N',21);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CN inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CN already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CO';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CO','CO','N',22);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CO inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CO already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CP','CP','N',23);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CR','CR','N',24);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CT';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CT','CT','N',25);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CT inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CT already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CU';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CU','CU','N',26);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CU inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CU already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'CX';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','CX','CX','N',27);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CX inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:CX already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DA','DA','N',28);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DA inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DC','DC','N',29);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DD','DD','N',30);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DE';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DE','DE','N',31);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DE inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DE already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DK';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DK','DK','N',32);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DK inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DK already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'DP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','DP','DP','N',33);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:DP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'EB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','EB','EB','N',34);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EB inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'EH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','EH','EH','N',35);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'EM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','EM','EM','N',36);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'EP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','EP','EP','N',37);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'ES';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','ES','ES','N',38);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:ES inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:ES already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'EY';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','EY','EY','N',39);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EY inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:EY already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'FD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','FD','FD','N',40);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:FD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:FD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'GD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','GD','GD','N',41);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'GH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','GH','GH','N',42);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'GM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','GM','GM','N',43);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'GW';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','GW','GW','N',44);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GW inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:GW already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HB','HB','N',45);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HB inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HC','HC','N',46);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HD','HD','N',47);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HG';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HG','HG','N',48);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HG inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HG already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HI','HI','N',49);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HI inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HK';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HK','HK','N',50);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HK inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HK already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HL';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HL','HL','N',51);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HL inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HL already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HM','HM','N',52);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HO';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HO','HO','N',53);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HO inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HO already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HP','HP','N',54);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HR','HR','N',55);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HS','HS','N',56);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HS inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HV';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HV','HV','N',57);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HV inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HV already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HX';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HX','HX','N',58);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HX inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HX already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'HY';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','HY','HY','N',59);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HY inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:HY already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'IP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','IP','IP','N',60);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:IP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:IP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'JT';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','JT','JT','N',61);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:JT inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:JT already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'LM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','LM','LM','N',62);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:LM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:LM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'MD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','MD','MD','N',63);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'MH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','MH','MH','N',64);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'MN';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','MN','MN','N',65);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MN inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:MN already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'NB';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','NB','NB','N',66);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NB inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NB already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'NH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','NH','NH','N',67);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'NR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','NR','NR','N',68);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'NS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','NS','NS','N',69);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NS inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'NU';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','NU','NU','N',70);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NU inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:NU already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OA','OA','N',71);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OA inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OC','OC','N',72);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OD','OD','N',73);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OF';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OF','OF','N',74);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OF inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OF already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OH','OH','N',75);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OL';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OL','OL','N',76);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OL inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OL already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'OR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','OR','OR','N',77);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:OR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'PC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','PC','PC','N',78);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'PH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','PH','PH','N',79);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'PR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','PR','PR','N',80);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'PS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','PS','PS','N',81);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PS inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:PS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RD','RD','N',82);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RD inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RX';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RX','RX','N',83);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RX inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RX already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SC','SC','N',84);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SF';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SF','SF','N',85);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SF inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SF already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SH','SH','N',86);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SM','SM','N',87);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SP','SP','N',88);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'SU';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','SU','SU','N',89);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SU inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:SU already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'TI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','TI','TI','N',90);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TI inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'TP';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','TP','TP','N',91);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TP inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TP already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'TS';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','TS','TS','N',92);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TS inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TS already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'WH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','WH','WH','N',93);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WH inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RC','RC','N',94);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RC already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RG';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RG','RG','N',95);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RG inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RG already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RM';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RM','RM','N',96);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RM inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RM already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'RR';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','RR','RR','N',97);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RR inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:RR already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'TW';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','TW','TW','N',98);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TW inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:TW already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'WT';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','WT','WT','N',99);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WT inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WT already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'VA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','VA','VA','N',100);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:VA inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:VA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHInstCode' and codelst_subtyp = 'WC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHInstCode','WC','WC','N',101);
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WC inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHInstCode Subtype:WC already exists');
	end if;

	COMMIT;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,9,'09_codelist_items_NIHInstCode.sql',sysdate,'9.0.0 Build#604');

commit;