
delete from ER_OBJECT_SETTINGS WHERE OBJECT_TYPE ='TM' AND OBJECT_SUBTYPE='cbu_menu' AND OBJECT_NAME='top_menu';
delete from ER_OBJECT_SETTINGS WHERE OBJECT_SUBTYPE ='new_cbu' AND OBJECT_NAME='cbm_menu' AND OBJECT_TYPE='M';
delete from ER_OBJECT_SETTINGS WHERE OBJECT_SUBTYPE ='cord_entry' AND OBJECT_NAME='cbm_menu' AND OBJECT_TYPE='M';
delete from ER_OBJECT_SETTINGS WHERE OBJECT_SUBTYPE ='review_unit_report' AND OBJECT_NAME='cbm_menu' AND OBJECT_TYPE='M';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,14,'14_Menu_delete.sql',sysdate,'9.0.0 Build#604');

commit;