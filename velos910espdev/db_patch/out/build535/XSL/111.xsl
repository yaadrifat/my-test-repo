<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByCategory" match="ROW" use="BGTSECTION_NAME" />
<xsl:key name="RecordsByCategoryLineitem" match="ROW" use="concat(BGTSECTION_NAME, ' ', LINEITEM_NAME)" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
Download the report in: 
<A href='{$wd}' >
Word Format
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
Excel Format
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
Printer Friendly Format
</A> 
</td>
</tr>
</table>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<hr class="thickLine" />


<TABLE WIDTH="100%" >
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Name: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_NAME" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Version: </TD><TD class="reportData" align="left"><b><xsl:value-of select="//BUDGET_VERSION" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Description: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_DESC" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
Study Number: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="LEFT" width="22%">
Study Title: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_TITLE" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
Organization: </TD><TD class="reportData"><b><xsl:value-of select="//SITE_NAME" /></b>
</TD>
</TR>
</TABLE>
<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Section</TH>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">Type</TH>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">Category</TH>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">Description</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Unit Cost</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Number of Units</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Total Cost</TH>
</TR>
<xsl:for-each select="ROW[count(. | key('RecordsByCategory', BGTSECTION_NAME)[1])=1]">

<TR>
<TD class="reportGrouping" WIDTH="10%" colspan="9">
<xsl:value-of select="BGTSECTION_NAME" />
</TD>
</TR>

<xsl:variable name="str" select="key('RecordsByCategory', BGTSECTION_NAME)" />
<xsl:for-each select="key('RecordsByCategory', BGTSECTION_NAME)">




<xsl:variable name="class">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
<xsl:otherwise>reportOddRow</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>

<TD WIDTH="10%">&#xa0;</TD>
<TD class="reportData" ><xsl:value-of select="LINEITEM_NAME" />&#xa0;</TD>
<TD class="reportData" ><xsl:value-of select="CATEGORY" />&#xa0;</TD>
<TD class="reportData" ><xsl:value-of select="LINEITEM_DESC" />&#xa0;</TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(UNIT_COST,'##,###,###,###,###,##0.00')" />&#xa0;</TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(NUMBER_OF_UNIT,'##,###,###,###,###,##0.00')" />&#xa0;</TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(TOTAL_COST,'##,###,###,###,###,##0.00')" />&#xa0;</TD>


</TR>

</xsl:for-each>

<tr>
<td colspan="5">&#xa0;</td>
<td align="left" class="reportGrouping">Sub Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', BGTSECTION_NAME)/TOTAL_COST),'##,###,###,###,###,##0.00')"/></td>
</tr>

</xsl:for-each>

<xsl:variable name="TOTAL"><xsl:value-of select="sum(//TOTAL_COST)"/></xsl:variable>

<tr>
<td colspan="5">&#xa0;</td>
<td align="left" class="reportGrouping">Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number($TOTAL,'##,###,###,###,###,##0.00')"/></td>
</tr>

<xsl:variable name="INDIRECTS"><xsl:value-of select="$TOTAL * //BGTCAL_INDIRECTCOST div 100"/></xsl:variable>


<tr>
<td colspan="3">&#xa0;</td>
<td colspan="3" align="right" class="reportGrouping">Indirects of <xsl:value-of select="format-number(//BGTCAL_INDIRECTCOST,'##0.00')"/>% applied to Total</td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number($INDIRECTS,'##,###,###,###,###,##0.00')"/></td>
</tr>

<tr>
<td colspan="5">&#xa0;</td>
<td align="left" class="reportGrouping"><font color = "red">Grand Total</font></td>
<td align="right" class="reportGrouping"><font color = "red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number($TOTAL + $INDIRECTS,'##,###,###,###,###,##0.00')"/></font></td>
</tr>

</TABLE>

<hr class="thickLine" />
<TABLE WIDTH="100%" >
<TR>
<TD>Last Modified by: <xsl:value-of select="//LAST_MODIFIED_BY"/></TD>
<TD>Date Last Modified: <xsl:value-of select="//LAST_MODIFIED_DATE"/></TD>
</TR>
<br/>
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
Report By:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
Date:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>