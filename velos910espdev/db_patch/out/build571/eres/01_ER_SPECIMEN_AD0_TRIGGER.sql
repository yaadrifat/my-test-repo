create or replace TRIGGER ER_SPECIMEN_AD0 AFTER DELETE ON ER_SPECIMEN
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(2000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
audit_trail.record_transaction
    (raid, 'ER_SPECIMEN', :OLD.rid, 'D');
deleted_data :=
:OLD.PK_SPECIMEN || '|' ||
:OLD.SPEC_ID|| '|' ||
:OLD.SPEC_ALTERNATE_ID || '|' ||
:OLD.SPEC_DESCRIPTION || '|' ||
:OLD.SPEC_ORIGINAL_QUANTITY || '|' ||
:OLD.SPEC_QUANTITY_UNITS || '|' ||
:OLD.SPEC_TYPE || '|' ||
:OLD.FK_STUDY || '|' ||
TO_CHAR(:OLD.SPEC_COLLECTION_DATE) || '|' ||
:OLD.FK_PER || '|' ||
:OLD.SPEC_OWNER_FK_USER || '|' ||
:OLD.FK_STORAGE || '|' ||
:OLD.FK_SITE || '|' ||
:OLD.FK_VISIT || '|' ||
:OLD.FK_SCH_EVENTS1 || '|' ||
:OLD.FK_SPECIMEN || '|' ||
:OLD.SPEC_STORAGE_TEMP || '|' ||
:OLD.SPEC_STORAGE_TEMP_UNIT || '|' ||
:OLD.RID || '|' ||
:OLD.CREATOR || '|' ||
:OLD.LAST_MODIFIED_BY || '|' ||
TO_CHAR(:OLD.LAST_MODIFIED_DATE) || '|' ||
TO_CHAR(:OLD.CREATED_ON) || '|' ||
:OLD.IP_ADD || '|' ||
:OLD.FK_ACCOUNT|| '|' ||
:OLD.SPEC_PROCESSING_TYPE || '|' ||
:OLD.FK_STORAGE_KIT_COMPONENT --KM-013008
||'|'||:OLD.FK_USER_PATH||'|'||:OLD.FK_USER_SURG||'|'||:OLD.FK_USER_COLLTECH||'|'||:OLD.FK_USER_PROCTECH
||'|'||TO_CHAR(:OLD.SPEC_REMOVAL_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) ||'|'|| TO_CHAR(:OLD.SPEC_FREEZE_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT)||'|'||
:OLD.spec_anatomic_site||'|'||
:OLD.spec_tissue_side||'|'||
:OLD.spec_pathology_stat||'|'||
:OLD.fk_storage_kit||'|'||
:OLD.spec_expected_quantity||'|'||
:OLD.spec_base_orig_quantity||'|'||
:OLD.spec_expectedq_units||'|'||
:OLD.spec_base_origq_units||'|'||
:OLD.spec_disposition||'|'|| --AK(Bug#5828-16thMar11)
:OLD.spec_envt_cons;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,114,1,'01_ER_SPECIMEN_AD0_TRIGGER.sql',sysdate,'8.10.0 Build#571');

commit;