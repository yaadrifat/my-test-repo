set define off;
begin
	UPDATE TRACK_PATCHES SET DB_PATCH_NAME ='04_Update_ER_CODELST.sql'
	WHERE DB_VER_MJR=160 AND DB_VER_MNR=4;
  commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,27,'27_TRACK_PATCHES_UPDATE.sql',sysdate,'9.0.0 Build#618');

commit;
