--STARTS INSERTING RECORD INTO ER_LABGROUP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABGROUP
    where GROUP_NAME = 'IDMOthers'
    AND GROUP_TYPE = 'IOG';
  if (v_record_exists = 0) then
     INSERT INTO ER_LABGROUP(PK_LABGROUP,GROUP_NAME,GROUP_TYPE) VALUES(SEQ_ER_LABGROUP.nextval,'IDMOthers','IOG');
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,5,'05_ER_LABGROUP_INSERT.sql',sysdate,'9.0.0 Build#618');

commit;

