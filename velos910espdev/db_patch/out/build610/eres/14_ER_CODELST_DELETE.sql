set define off;


delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='na';
delete from ER_CODELST where CODELST_TYPE ='note_assess' and codelst_subtyp='ncd';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='tu';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='ineligible';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='incmp_eligible';

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,153,14,'14_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#610');

commit;
