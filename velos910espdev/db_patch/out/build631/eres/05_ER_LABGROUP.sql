set define off;

Insert into ER_LABGROUP(PK_LABGROUP,GROUP_NAME,GROUP_TYPE) values(SEQ_ER_LABTESTGRP.nextval,'OTHERLABGROUP','OLBT');

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,174,5,'05_ER_LABGROUP.sql',sysdate,'9.0.0 Build#631');

commit;