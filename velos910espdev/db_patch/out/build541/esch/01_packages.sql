CREATE OR REPLACE PACKAGE BODY "ESCH"."PKG_PROTOCOL_VISIT" AS

-- SV, 10/23, created this package to include all protcol visit relatedPROCEDUREs and functions.
-- i. when a protocol visit is deleted, delete all events attached to it.
-- ii. when a protocol visit's displacement is changed, change all attached events's as well.
-- iii. when all events for a visit are deleted, delete the visit also, if it was a generated one. (type = 'S')

PROCEDURE sp_cascade_delete_visit_events(p_protocol_id IN NUMBER,   p_protocol_visit IN NUMBER,   p_source IN VARCHAR2) AS
BEGIN
	DECLARE v_dummy NUMBER;

	BEGIN
		IF(p_source = 'P') THEN

			DELETE FROM EVENT_DEF
			WHERE chain_id = p_protocol_id
			AND fk_visit = p_protocol_visit;
		ELSIF(p_source = 'S') THEN

			DELETE FROM EVENT_ASSOC
			WHERE chain_id = p_protocol_id
			AND fk_visit = p_protocol_visit;
		END IF;

	END;

	COMMIT;
END sp_cascade_delete_visit_events;

PROCEDURE sp_update_visit_event(p_protocol_id IN NUMBER,   p_protocol_visit IN NUMBER,   p_source IN VARCHAR2) AS
BEGIN
	DECLARE v_dummy NUMBER;

	BEGIN
		IF(p_source = 'P') THEN

			UPDATE EVENT_DEF
			SET displacement =
			(SELECT displacement
		 	FROM SCH_PROTOCOL_VISIT
		 	WHERE fk_protocol = p_protocol_id
		 	AND pk_protocol_visit = p_protocol_visit)
			WHERE chain_id = p_protocol_id
		 	AND fk_visit = p_protocol_visit;

		ELSIF(p_source = 'S') THEN

			UPDATE EVENT_ASSOC
			SET displacement =
			(SELECT displacement
			FROM SCH_PROTOCOL_VISIT
			WHERE fk_protocol = p_protocol_id
			AND pk_protocol_visit = p_protocol_visit)
			WHERE chain_id = p_protocol_id
			AND fk_visit = p_protocol_visit;

		END IF;

	END;

	COMMIT;
END sp_update_visit_event;

PROCEDURE sp_delete_generated_visit(p_protocol_id IN NUMBER,   p_source IN VARCHAR2) AS
BEGIN
	DECLARE v_dummy NUMBER;
	BEGIN

		-- delete all the system generated visits that are no longer used in this calendar. visit type 'S' - system generated; p_source = 'S' is study calendar.

		IF(p_source = 'P') THEN

		    DELETE FROM SCH_PROTOCOL_VISIT
		    WHERE fk_protocol = p_protocol_id
			AND visit_type = 'S'
			AND pk_protocol_visit NOT IN
			(SELECT fk_visit
			FROM EVENT_DEF
			WHERE chain_id = p_protocol_id
			AND displacement <> 0);
		ELSIF(p_source = 'S') THEN

			DELETE FROM SCH_PROTOCOL_VISIT
			WHERE fk_protocol = p_protocol_id
			AND visit_type = 'S'
			AND pk_protocol_visit NOT IN
			(SELECT fk_visit
			FROM EVENT_ASSOC
			WHERE chain_id = p_protocol_id
			AND displacement <> 0);
	    END IF;

	END;

	COMMIT;

END sp_delete_generated_visit;

FUNCTION validate_visit_name(p_protocol_id NUMBER,   p_visit_id NUMBER,   p_visit_name VARCHAR2) RETURN VARCHAR2 IS
	/*
	** The function checks if the visit name is already existing or not. It returns a -1 if a duplicate
	** is found within the calendar. -1 is passed in the visit_id parameter if the name is to be checked during an Insert.
	** To check during an UPDATE the visit_id is passed in. This is used to check the name
	** in all other events other than this particular visit_ID. Visit name can be duplicated across calendars.
	**
	** Return Value
	** -1 on finding a duplicate
	**  0 on not finding a duplicate
	*/ v_visit_count NUMBER;
BEGIN
	-- If visit_id is -1 then its a check during a INSERT

	IF(p_visit_id = -1) THEN
		SELECT COUNT(pk_protocol_visit)
		INTO v_visit_count
		FROM SCH_PROTOCOL_VISIT
		WHERE UPPER(visit_name) = UPPER(p_visit_name)
		AND fk_protocol = p_protocol_id;
		-- within the same calendar

		IF(v_visit_count > 0) THEN
			RETURN(-1);
		END IF;

		--
		-- ELSE if the visit_id is not equal to -1, then its a check during an UPDATE.
		-- The visit_id contains the id of the event whose name has to be validated.
		--
	ELSE
		SELECT COUNT(pk_protocol_visit)
		INTO v_visit_count
		FROM SCH_PROTOCOL_VISIT
		WHERE UPPER(visit_name) = UPPER(p_visit_name)
		AND pk_protocol_visit != p_visit_id
		AND fk_protocol = p_protocol_id;
		-- within the same calendar

		IF(v_visit_count > 0) THEN
			RETURN(-1);
		END IF;

	END IF;

	RETURN(0);
END validate_visit_name;

FUNCTION validate_visit_displacement(p_protocol_id NUMBER,   p_visit_id NUMBER,   p_displacement NUMBER) RETURN VARCHAR2 IS
	/*
	** The function checks if the visit's displacement is unique. With the current design, visit is tied to event
	** one to one and thus we cannot have two or more visits with the same displacement(interval. This function checks
	** returns a -1 if a duplicate is found within the calendar. -1 is passed in the visit_id parameter if the name is to be checked during an Insert.
	** To check during an UPDATE the visit_id is passed in. This is used to check the displacement
	** in all other events other than this particular visit_ID.
	**
	** Return Value
	** -1 on finding a duplicate
	**  0 on not finding a duplicate
	*/ v_visit_count NUMBER;
BEGIN
	-- If visit_id is -1 then its a check during a INSERT

	IF(p_visit_id = -1) THEN
		SELECT COUNT(pk_protocol_visit)
		INTO v_visit_count
		FROM SCH_PROTOCOL_VISIT
		WHERE displacement = p_displacement
		AND fk_protocol = p_protocol_id;
		-- within the same calendar

		IF(v_visit_count > 0) THEN
			RETURN(-1);
		END IF;

		--
		-- ELSE if the visit_id is not equal to -1, then its a check during an UPDATE.
		-- The visit_id contains the id of the event whose name has to be validated.
		--
	ELSE
		SELECT COUNT(pk_protocol_visit)
		INTO v_visit_count
		FROM SCH_PROTOCOL_VISIT
		WHERE displacement = p_displacement
		AND pk_protocol_visit != p_visit_id
		AND fk_protocol = p_protocol_id;
		-- within the same calendar

		IF(v_visit_count > 0) THEN
			RETURN(-1);
		END IF;

	END IF;

	RETURN(0);
END validate_visit_displacement;

FUNCTION validate_visit(p_protocol_id NUMBER,   p_visit_id NUMBER,   p_visit_name VARCHAR2 DEFAULT '',   p_displacement NUMBER DEFAULT 0) RETURN VARCHAR2 IS
BEGIN
	DECLARE ret NUMBER := 0;

	BEGIN

		IF(p_visit_name = '' AND p_displacement = 0) THEN
			RETURN -3;
		END IF;

		ret := validate_visit_name(p_protocol_id,   p_visit_id,   p_visit_name);

		IF(ret = 0) THEN
			--ret := validate_visit_displacement(p_protocol_id, p_visit_id, p_displacement);

			IF(ret = 0) THEN
				RETURN 0;
			ELSE
				RETURN -2;
				-- displacement in error
			END IF;

		ELSE
			RETURN -1;
			--name in error
		END IF;

		RETURN 0;
		-- no duplicates
	END;
END validate_visit;

PROCEDURE sp_generate_ripple(p_protocol_visit IN NUMBER,   p_called_from CHAR) AS
	v_dummy VARCHAR2(20);
	v_displacement NUMBER;
	v_interval NUMBER;

	pctx plog.log_ctx := plog.init(psection => 'PKG_PROTOCOL_VISIT',   plevel => plog.ldebug);

BEGIN

	BEGIN
		SELECT displacement
		INTO v_displacement
		FROM SCH_PROTOCOL_VISIT
		WHERE pk_protocol_visit = p_protocol_visit;

		plog.DEBUG(pctx,   ' main v_displacement FROM ' || v_displacement || p_called_from);

		FOR i IN (SELECT pk_protocol_visit, insert_after_interval, insert_after_interval_unit, displacement
			FROM SCH_PROTOCOL_VISIT
			WHERE insert_after = p_protocol_visit)
		LOOP
			plog.DEBUG(pctx,   ' main for loop p_protocol_visit  ' || p_protocol_visit);
			BEGIN

				IF(i.insert_after_interval_unit = 'M') THEN
					v_interval := i.insert_after_interval *30;
				ELSIF(i.insert_after_interval_unit = 'W') THEN
					v_interval := i.insert_after_interval *7;
				ELSE
					v_interval := i.insert_after_interval;
				END IF;

				UPDATE SCH_PROTOCOL_VISIT
				SET displacement =(v_displacement + v_interval)
				WHERE pk_protocol_visit = i.pk_protocol_visit;

				IF(p_called_from = 'S') THEN

					UPDATE EVENT_ASSOC -- assuming its is in library for now, later we will have to put a check if we need to update event_def or event_assoc
					SET displacement =(v_displacement + v_interval)
					WHERE fk_visit = i.pk_protocol_visit;
				ELSE

					UPDATE EVENT_DEF -- assuming its is in library for now, later we will have to put a check if we need to update event_def or event_assoc
					SET displacement =(v_displacement + v_interval)
					WHERE fk_visit = i.pk_protocol_visit;
				END IF;

				EXCEPTION
					WHEN OTHERS THEN
						plog.DEBUG(pctx,   ' exception when updating ' || i.pk_protocol_visit || SQLERRM);
			END;

			plog.DEBUG(pctx,   ' calling sp_genripple for  ' || i.pk_protocol_visit);
			sp_generate_ripple(i.pk_protocol_visit,   p_called_from);

			/* UPDATE event_def -- assuming its is in library for now, later we will have to put a check if we need to update event_def or event_assoc
			SET displacement = v_displacement+i.insert_after_interval
			WHERE fk_visit = i.pk_protocol_visit;*/

			/*	PLOG.DEBUG(pCTX,' Running this ID as parent ' || i.pk_protocol_visit);
			FOR k IN (SELECT pk_protocol_visit,insert_after_interval,displacement FROM SCH_PROTOCOL_VISIT WHERE insert_after = i.pk_protocol_visit)
			LOOP
			PLOG.DEBUG(pCTX,' child for loop i.pk_protocol_visit ' || i.pk_protocol_visit);

			BEGIN
			UPDATE SCH_PROTOCOL_VISIT SET displacement=(i.displacement+k.insert_after_interval) WHERE pk_protocol_visit=k.pk_protocol_visit;
			EXCEPTION WHEN OTHERS THEN

									 PLOG.DEBUG(pCTX,' exception when updating ' || k.pk_protocol_visit  || SQLERRM );
			END;
			PLOG.DEBUG(pCTX,' calling sp_genripple for  ' || k.pk_protocol_visit);
						sp_genripple(k.pk_protocol_visit);
			PLOG.DEBUG(pCTX,' updated k.pk_protocol_visit' || k.pk_protocol_visit);

			END LOOP;
			*/

		END LOOP;

		COMMIT;

		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				v_dummy := '0';
	END;
END sp_generate_ripple;

FUNCTION group_events(p_protocol_id NUMBER,   p_displacment NUMBER) RETURN VARCHAR2 IS v_event_str VARCHAR2(4000);
BEGIN
	FOR i IN (SELECT DISTINCT name FROM EVENT_ASSOC WHERE chain_id = p_protocol_id 
		AND displacement = p_displacment)
	LOOP
		plog.DEBUG(pctx,   v_event_str || ' Len=' || LENGTH(v_event_str));
		-- IF (LENGTH(NVL(v_event_str,''))=0) THEN

		IF(NVL(v_event_str,   '0') = '0') THEN
			v_event_str := i.name;
		ELSE
			v_event_str := v_event_str || ',' || i.name;
		END IF;

	END LOOP;

	RETURN v_event_str;
END group_events;

/* Added by Sonia Abrol, 10/19/06, updat ethe duration of the calendar when the maximum visit displacement is more than event duration. In this
case event duration unit will be set to days */

PROCEDURE sp_update_protocol_duration(p_protocol_visit IN NUMBER,   p_calledfrom IN VARCHAR2,   o_new_duration OUT NUMBER) AS
BEGIN
	DECLARE

	v_max_disp NUMBER;
	v_event_duration NUMBER;
	v_protocol NUMBER;

	BEGIN

		-- get protocol id
		SELECT fk_protocol
		INTO v_protocol
		FROM SCH_PROTOCOL_VISIT
		WHERE pk_protocol_visit = p_protocol_visit;

		-- get the maximum displacement for the protocol

		SELECT MAX(displacement)
		INTO v_max_disp
		FROM SCH_PROTOCOL_VISIT
		WHERE fk_protocol = v_protocol;

		IF(p_calledfrom = 'P') THEN

			--get event duration

			SELECT duration
			INTO v_event_duration
			FROM EVENT_DEF
			WHERE event_id = v_protocol
			AND event_type = 'P';

			o_new_duration := v_event_duration;

			IF v_event_duration <= v_max_disp THEN
				-- if maximum displacement is more than protocol duration, update protocol duration with maximum displacement and duration unit with Days
				--KM- 7Nov08 - issue3580
				UPDATE EVENT_DEF
				SET duration =(v_max_disp),
				duration_unit = 'D'
				WHERE event_id = v_protocol
				AND event_type = 'P';

				o_new_duration :=(v_max_disp);

			END IF;

		ELSIF(p_calledfrom = 'S') THEN
			--get event duration

			SELECT duration
			INTO v_event_duration
			FROM EVENT_ASSOC
			WHERE event_id = v_protocol
			AND event_type = 'P';

			o_new_duration := v_event_duration;

			IF v_event_duration <= v_max_disp THEN
				-- if maximum displacement is more than protocol duration, update protocol duration with maximum displacement and duration unit with Days

				UPDATE EVENT_ASSOC
				SET duration =(v_max_disp),
				duration_unit = 'D'
				WHERE event_id = v_protocol
				AND event_type = 'P';

				o_new_duration :=(v_max_disp) ;

			END IF;

		END IF;

	END;

	COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
			plog.fatal(pctx,   'Exception in sp_update_protocol_duration' || SQLERRM);
END sp_update_protocol_duration;



-- Added by Manimaran for the Enh#C8.3
PROCEDURE sp_visit_copy(p_protocol_id IN NUMBER,   p_frm_visitid IN NUMBER,   p_to_visitid IN NUMBER, p_called_from IN VARCHAR2,   usr IN VARCHAR2,   ipadd IN VARCHAR2,   o_ret OUT NUMBER) IS
	v_val NUMBER;
	old_event_id NUMBER;
	v_docseq NUMBER;
	v_disp NUMBER;

BEGIN
	IF(p_called_from = 'P') THEN

		FOR m IN
			(SELECT event_id,chain_id, event_type, name, notes,  cost, cost_description, duration,  user_id, linked_uri,  fuzzy_period,
			msg_to, status, description, displacement, org_id, event_msg, event_res, event_flag, pat_daysbefore, pat_daysafter,
			usr_daysbefore, usr_daysafter, pat_msgbefore, pat_msgafter, usr_msgbefore, usr_msgafter, calendar_sharedwith, duration_unit,
			event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore, fk_catlib, event_category, event_sequence,
			SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE
			FROM EVENT_DEF WHERE chain_id = p_protocol_id AND fk_visit = p_frm_visitid)
		LOOP
			SELECT event_definition_seq.NEXTVAL  INTO v_val FROM dual;
			SELECT displacement INTO v_disp  FROM SCH_PROTOCOL_VISIT  WHERE pk_protocol_visit = p_to_visitid;

			INSERT INTO EVENT_DEF(event_id,   chain_id,   event_type,   name,   notes,   cost,   cost_description,   duration,   user_id,   linked_uri,   fuzzy_period,
			msg_to,   status,   description,   displacement,   org_id,   event_msg,   event_res,   event_flag,   created_on,   pat_daysbefore,   pat_daysafter,
			usr_daysbefore,   usr_daysafter,   pat_msgbefore,   pat_msgafter,   usr_msgbefore,   usr_msgafter,   creator,   ip_add,   calendar_sharedwith,   duration_unit,
			fk_visit,   event_fuzzyafter,   event_durationafter,   event_cptcode,   event_durationbefore,   fk_catlib,   event_category,   event_sequence,
			SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE)
			VALUES
			(v_val,   m.chain_id,   m.event_type,   m.name,   m.notes,   m.cost,   m.cost_description,   m.duration,   m.user_id,   m.linked_uri,   m.fuzzy_period,
			m.msg_to,   m.status,   m.description,   v_disp,   m.org_id,   m.event_msg,   m.event_res,   m.event_flag,   SYSDATE,   m.pat_daysbefore,   m.pat_daysafter,
			m.usr_daysbefore,   m.usr_daysafter,   m.pat_msgbefore,   m.pat_msgafter,   m.usr_msgbefore,   m.usr_msgafter,   usr,   ipadd,   m.calendar_sharedwith,   m.duration_unit,
			p_to_visitid,   m.event_fuzzyafter,   m.event_durationafter,   m.event_cptcode,   m.event_durationbefore,   m.fk_catlib,   m.event_category,   m.event_sequence,
			m.SERVICE_SITE_ID, m.FACILITY_ID, m.FK_CODELST_COVERTYPE);


			old_event_id := m.event_id;

			--modified by sonia abrol 07/11/08 to use teh genericPROCEDURE. removed the redundant code
			SP_CPEVEDTLS ( old_event_id,  v_val,'C',   o_ret    , usr,   ipadd,1);
		END LOOP;
	END IF;


	IF(p_called_from = 'S') THEN

		--JM: 23May2008 removed offline_flag and added EVENT_TYPE = 'A' issue #3509
		--BK: 11AUG2010  Defect #5172
		FOR m IN (SELECT EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,USER_ID,LINKED_URI,FUZZY_PERIOD,MSG_TO,STATUS,DESCRIPTION,
		DISPLACEMENT,ORG_ID,EVENT_MSG,EVENT_RES,EVENT_FLAG,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,PAT_MSGBEFORE,
		PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,STATUS_DT,STATUS_CHANGEBY,IP_ADD,ORIG_STUDY,
		ORIG_CAL,ORIG_EVENT,DURATION_UNIT,FK_VISIT,EVENT_CPTCODE,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_CALASSOCTO,
		EVENT_CALSCHDATE,EVENT_CATEGORY,EVENT_SEQUENCE, HIDE_FLAG,
		SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE, EVENT_LINE_CATEGORY
		FROM EVENT_ASSOC WHERE chain_id = p_protocol_id AND fk_visit = p_frm_visitid AND EVENT_TYPE = 'A')
		LOOP

			SELECT event_definition_seq.NEXTVAL INTO v_val FROM dual;
			SELECT displacement INTO v_disp FROM SCH_PROTOCOL_VISIT   WHERE pk_protocol_visit = p_to_visitid;

			INSERT INTO EVENT_ASSOC(  EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,USER_ID,LINKED_URI,FUZZY_PERIOD,MSG_TO,STATUS,DESCRIPTION,
			DISPLACEMENT,ORG_ID,EVENT_MSG,EVENT_RES,EVENT_FLAG,CREATED_ON,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,PAT_MSGBEFORE,
			PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,STATUS_DT,STATUS_CHANGEBY,CREATOR,IP_ADD,ORIG_STUDY,
			ORIG_CAL,ORIG_EVENT,DURATION_UNIT,FK_VISIT,EVENT_CPTCODE,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_CALASSOCTO,
			EVENT_CALSCHDATE,EVENT_CATEGORY,EVENT_SEQUENCE,HIDE_FLAG,
			SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,EVENT_LINE_CATEGORY)
			VALUES
			(v_val, m.CHAIN_ID,m.EVENT_TYPE,m.NAME,m.NOTES,m.COST,m.COST_DESCRIPTION,m.DURATION,m.USER_ID,m.LINKED_URI,m.FUZZY_PERIOD,m.MSG_TO,m.STATUS,m.DESCRIPTION,
			v_disp,m.ORG_ID,m.EVENT_MSG,m.EVENT_RES,m.EVENT_FLAG,SYSDATE,m.PAT_DAYSBEFORE,m.PAT_DAYSAFTER,m.USR_DAYSBEFORE,m.USR_DAYSAFTER,m.PAT_MSGBEFORE,
			m.PAT_MSGAFTER,m.USR_MSGBEFORE,m.USR_MSGAFTER,m.STATUS_DT,m.STATUS_CHANGEBY,usr,ipadd,m.ORIG_STUDY,
			m.ORIG_CAL,m.ORIG_EVENT,m.DURATION_UNIT,p_to_visitid,m.EVENT_CPTCODE,m.EVENT_FUZZYAFTER,m.EVENT_DURATIONAFTER,m.EVENT_DURATIONBEFORE,m.FK_CATLIB,m.EVENT_CALASSOCTO,
			m.EVENT_CALSCHDATE,m.EVENT_CATEGORY,m.EVENT_SEQUENCE,m.HIDE_FLAG,
			m.SERVICE_SITE_ID, m.FACILITY_ID, m.FK_CODELST_COVERTYPE, m.EVENT_LINE_CATEGORY);


			old_event_id := m.event_id;
			--modified by sonia abrol 07/11/08 to use teh genericPROCEDURE. removed the redundant code
			SP_CPEVEDTLS ( old_event_id,  v_val,'S',   o_ret    , usr,   ipadd,1);

		END LOOP;
	END IF;


	o_ret := 0;
END sp_visit_copy;
---
END pkg_protocol_visit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,84,1,'01_packages.sql',sysdate,'8.9.0 Build#541');

commit;