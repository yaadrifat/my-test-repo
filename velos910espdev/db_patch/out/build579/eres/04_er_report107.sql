Update ER_REPORT set REP_SQL = 'SELECT
PROT_ID,PROT_NAME,PROT_DESC,
PROT_DURATION,decode(displacement,null,''No Interval'',nvl(sch_date_grp, ''No Interval'')) as sch_date_grp,
decode(displacement,null,''No Interval Defined'',nvl(sch_date_grp_disp, ''No Interval Defined'')) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
decode(displacement,null,''Not Defined'',(TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT,event_sequence' where pk_report = 107;

COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'SELECT
PROT_ID,PROT_NAME,PROT_DESC,
PROT_DURATION,decode(displacement,null,''No Interval'',nvl(sch_date_grp, ''No Interval'')) as sch_date_grp,
decode(displacement,null,''No Interval Defined'',nvl(sch_date_grp_disp, ''No Interval Defined'')) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
decode(displacement,null,''Not Defined'',(TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT,event_sequence' where pk_report = 107;

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,122,4,'04_er_report107.sql',sysdate,'8.10.0 Build#579');

commit;