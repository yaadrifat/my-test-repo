


-- INSERTING CODE LIST VALUES -
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'LAB_SUM';
  if (v_column_exists = 0) then
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'sub_entity_type','LAB_SUM','LAB_SUMMERY','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);

  end if;
end;
/
--STARTS UPDATING RECORD INTO ER_CODELST TABLE -
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'na';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Defer' where codelst_type ='note_assess'AND codelst_subtyp = 'na';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,177,8,'08_ER_CODELST_INSERT.sql',sysdate,'9.0.0 Build#634');

commit;