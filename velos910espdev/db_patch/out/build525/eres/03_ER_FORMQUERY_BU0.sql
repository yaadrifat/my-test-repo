CREATE OR REPLACE TRIGGER "ERES"."ER_FORMQUERY_BU0" BEFORE UPDATE ON ER_FORMQUERY

FOR EACH ROW 
  WHEN (new.last_modified_by is not null) 
begin
 :new.last_modified_date := sysdate;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,3,'03_ER_FORMQUERY_BU0.sql',sysdate,'8.8.0 Build#525');

commit;