SET DEFINE OFF;

DELETE FROM ER_REPXSL WHERE FK_REPORT = 110;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,4,'04_ER_REPXSL_Delete_110.sql',sysdate,'8.8.0 Build#525');

commit;