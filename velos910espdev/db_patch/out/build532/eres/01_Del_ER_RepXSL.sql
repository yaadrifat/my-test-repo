DELETE FROM ER_REPXSL WHERE FK_REPORT in (106, 110, 159);

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,75,1,'01_Del_ER_RepXSL.sql',sysdate,'8.9.0 Build#532');

commit;