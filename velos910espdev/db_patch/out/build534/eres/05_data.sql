 declare

v_count number;
 
 begin
	
	select count(*) into v_count from er_ctrltab where ctrl_key = 'app_rights' and CTRL_VALUE = 'MIRB_PREV';

	if v_count= 0 then 
		-- insert group rights

		update er_ctrltab set CTRL_SEQ = CTRL_SEQ + 1 
		Where ctrl_key = 'app_rights' and CTRL_SEQ > 43;
		

		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ,ctrl_custom_col1 )
		VALUES ( seq_er_ctrltab.nextval, 'MIRB_PREV', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pending Review', 'app_rights', 44,'EV');
		
		 
		UPDATE ER_GRPS
		SET grp_rights = SUBSTR(grp_rights,1,43) || '0' || SUBSTR(grp_rights,44);
		

		update er_ctrltab set CTRL_SEQ = CTRL_SEQ + 1 
		Where ctrl_key = 'acc_type_rights' and CTRL_SEQ > 43;
		

		--------insert account type modules

		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,CTRL_SEQ ) VALUES ( 
		seq_er_ctrltab.nextval, 'G', 'MIRB_PREV', 'acc_type_rights',44);
		


		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,  CTRL_SEQ) VALUES ( 
		seq_er_ctrltab.nextval, 'MIRB_PREV', 'MODEIRB', 'hid_rights', 15);
		
		 
		COMMIT;

	end if ;		
end ;
/	

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,5,'05_data.sql',sysdate,'8.9.0 Build#534');

commit;
