set define off

declare

v_count number;
 
 begin
	
	select count(*) into v_count from er_ctrltab where ctrl_key = 'app_rights' and CTRL_VALUE = 'MIRB_OUT';

	if v_count= 0 then 
		-- insert group rights

		update er_ctrltab set CTRL_SEQ = CTRL_SEQ + 1 
		Where ctrl_key = 'app_rights' and CTRL_SEQ > 45;
		

		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ,ctrl_custom_col1 )
		VALUES ( seq_er_ctrltab.nextval, 'MIRB_OUT', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Final Outcome', 'app_rights', 46,'EV');
		
		 
		UPDATE ER_GRPS
		SET grp_rights = SUBSTR(grp_rights,1,45) || '0' || SUBSTR(grp_rights,46);
		

		update er_ctrltab set CTRL_SEQ = CTRL_SEQ + 1 
		Where ctrl_key = 'acc_type_rights' and CTRL_SEQ > 45;
		

		--------insert account type modules

		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,CTRL_SEQ ) VALUES ( 
		seq_er_ctrltab.nextval, 'G', 'MIRB_OUT', 'acc_type_rights',46);
		


		INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,  CTRL_SEQ) VALUES ( 
		seq_er_ctrltab.nextval, 'MIRB_OUT', 'MODEIRB', 'hid_rights', 15);
		
		 
		COMMIT;

	end if ;		
end ;
/	

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,6,'06_data.sql',sysdate,'8.9.0 Build#534');

commit;
