CREATE OR REPLACE PROCEDURE Sp_Get_Prot_Visit(
   P_PROTOCOL_ID	IN	NUMBER,
   P_DISPLACEMENT IN NUMBER,
   p_user  IN NUMBER,
   p_ip IN VARCHAR2,
   p_out_visit_id OUT NUMBER,
   p_visit_num NUMBER :=0
   )
  IS

v_VISIT_NAME VARCHAR2(50);
v_VISIT_NO NUMBER;
v_DESCRIPTION VARCHAR2(200);
v_DISPLACEMENT NUMBER;
v_NUM_MONTHS NUMBER;
v_NUM_WEEKS NUMBER;
v_NUM_DAYS NUMBER;
v_INSERT_AFTER NUMBER;
v_visit_id NUMBER ;
v_visit_type CHAR(1) := 'S'; -- SV, 10/20/04, system generated
C1 TYPES.cursorType;



/******************************************************************************
   NAME:       sp_get_prot_visit
   PURPOSE:    To calculate the desired information.


   PARAMETERS:
   INPUT:
   OUTPUT:
   RETURNED VALUE:
   CALLED BY:
   CALLS:
   EXAMPLE USE:     sp_copy_prot_visits;
   ASSUMPTIONS:
   LIMITATIONS:
   ALGORITHM:
   NOTES:

      Object Name:     sp_copy_prot_visit
      Sysdate:         9/29/2004
      Date/Time:       9/29/2004 11:43:47 PM
      Date:            9/29/2004
      Time:            11:43:47 PM
      Username:         Sam Varadarajan
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/29/2004    Sam V         1. Created this procedure.

******************************************************************************/





BEGIN

  IF (p_visit_num>0) THEN
     OPEN  c1 FOR SELECT pk_protocol_visit
     FROM SCH_PROTOCOL_VISIT
     WHERE fk_protocol = p_protocol_id 
     AND visit_no=p_visit_num; 
  ELSE
     OPEN  c1 FOR SELECT pk_protocol_visit
     FROM SCH_PROTOCOL_VISIT
     WHERE fk_protocol = p_protocol_id;
  END IF;

	FETCH c1
	INTO v_visit_id;
	-- SV, 09/30, copy the visit, only if old visit is found.
	IF (c1%FOUND) THEN
	   p_out_visit_id := v_visit_id;
	   RETURN;
	END IF;

END Sp_Get_Prot_Visit;
/

CREATE OR REPLACE SYNONYM ERES.SP_GET_PROT_VISIT FOR SP_GET_PROT_VISIT;


CREATE OR REPLACE SYNONYM EPAT.SP_GET_PROT_VISIT FOR SP_GET_PROT_VISIT;


GRANT EXECUTE, DEBUG ON SP_GET_PROT_VISIT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_GET_PROT_VISIT TO ERES;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,3,'03_583b_Bug6536_sp_get_prot_visit.sql',sysdate,'9.0.0 Build#594');

commit;