SET DEFINE OFF;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbu_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'cbu_menu', 'top_menu', 9, 
      0, 'CBU', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbu_menu for top_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'open_order' and OBJECT_NAME = 'cbm_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'open_order', 'cbm_menu', 2, 
     0, 'Search Open Orders', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting open_order for cbm_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'new_cbu' and OBJECT_NAME = 'cbm_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'new_cbu', 'cbm_menu', 3, 
      0, 'View CBU Records', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting new_cbu for cbm_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cord_entry' and OBJECT_NAME = 'cbm_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'cord_entry', 'cbm_menu', 4, 
    0, 'Cord Entry', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cord_entry for cbm_menu already exists');
  end if;
END;
/

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,6,'06_Er_Object_Setting_insert.sql',sysdate,'9.0.0 Build#594');

commit;