/* This readMe is specific to Velos eResearch version 9.0 build #594 */
=====================================================================================
Garuda Menu items
Few new Garuda Menu items are added. By default these menu items are invisible
and can be switched on based on requirements.(Wherever Garuda is deployed.)

Following is the list of Menu-items introduced for Garuda. They can be made visible 
either by updating the ER_Object_Settings table record (column to be updated- OBJECT_VISIBLE), 
or by using eTools.

OBJECT_SUBTYPE			OBJECT_NAME			OBJECT_DISPLAYTEXT
review_unit_report		cbm_menu			Unit Report
pending_ordr			pf_menu_ext			Pending Orders
opn_task				pf_menu_ext			Open Task
prdt_fullfill			pf_menu_ext			Product Fullfillment
user_prf				pf_menu_ext			User Profile
cbb_proc				cbb					CBB Procedures
cbb_add					cbb		 			Manage CBB
cbb						manage_org			CBB
pf_menu_ext				pf_menu				PF Other Links
manage_org				pf_menu				Manage Organizations
pf_menu					top_menu			PRODUCT FULLFILLMENT
cord_entry				cbm_menu			Cord Entry
new_cbu					cbm_menu			View CBU Records
open_order				cbm_menu			Search Open Orders
cbu_menu				top_menu			CBU

=====================================================================================
For enhancement INF-18183

Enclosed is an Excel document (INF18183impactJsps.xls) containing a list of JSP files 
touched for ehancement INF-18183. This document is a working document and will contain 
a progressive list of JSP files developers worked on

=====================================================================================
For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. Application server restart is needed. The change is not immediate.

=====================================================================================
For Localization Build #594
  #Bug 7005 fix:
1   protocoleventsave.jsp

  #Bug 7018 fix:
2   labelBundle.properties (Label Change)

  #New Localization 
3	additempatient.jsp
4	addmileurl.jsp
5	addperurl.jsp
6	adveventbrowser.jsp
7	allPatient.jsp
8	allSchedules.jsp
9	appendix_file.jsp
10	appendix_file_delete.jsp
11	appendix_file_edit.jsp
12	appendix_file_multi.jsp
13	appendix_file_update.jsp
14	appendix_url.jsp
15	appendix_url_edit.jsp
16	appendix_url_insert.jsp
17	appendix_url_update.jsp
18	appendixbrowser.jsp
19	bgtcaldelete.jsp
20	bgtprotocollist.jsp
21	budget.jsp
22	budgetapndx.jsp
23	budgetbrowserpg.jsp
24	budgetglobalusers.jsp
25	budgetrights.jsp
26	budgetsections.jsp
27	budgeturlsave.jsp
28	budgetuserrights.jsp
29	studyDoesNotExist.jsp
30	budrepretrieve.jsp
31	combinedBudget.jsp
32	copybudget.jsp
33	copystudyprotocol.jsp
34	copystudyversion.jsp
35	createCombinedBgt.jsp
36	createMultiMilestones.jsp
37	deleteAchMS.jsp
38	deletefromteam.jsp
39	deleteStatusHistory.jsp
40	deleteStudySiteApndx.jsp
41	editbgtprotocols.jsp
42	editmilestonerule.jsp
43	editstatus.jsp
44	editStudyTeamStatus.jsp
45	enrollpatient.jsp
46	enrollpatientsearch.jsp
47	formfilledpatbrowser.jsp
48	formfilledstdpatbrowser.jsp
49	formfilledstudybrowser.jsp
50	geninvoice.jsp
51	getTeamSupUser.jsp
52	insertStudySiteApndxURL.jsp
53	invoice_step1.jsp
54	invoicebrowser.jsp
55	labSelection.jsp
56	linkBudgetPayBrowser.jsp
57	linkInvPayBrowser.jsp
58	linkMilestonePayBrowser.jsp
59	LSampleSizePopUp.jsp
60	makeStudyPublic.jsp
61	mileapndx.jsp
62	mileapndxdelete.jsp
63	mileapndxfile.jsp
64	milefileupdate.jsp
65	milepaymentbrowser.jsp
66	milepaymentdelete.jsp
67	milepayments.jsp
68	milestone.jsp
69	milestonenotifications.jsp
70	mileurlsave.jsp
71	modifyTeam.jsp
72	morePerDetails.jsp
73	multipleusersearchdetails.jsp
74	multipleusersearchdetailswithsave.jsp
75	multiselectcodepopup.jsp
76	perapndxdelete.jsp
77	perapndxfile.jsp
78	perurlsave.jsp
79	reportsinstudy.jsp
80	sectionBrowserNew.jsp
81	sectionSequence.jsp
82	selectdiseasesite.jsp
83	showinvoicehistory.jsp
84	showStudyTeamHistory.jsp
85	showVersionHistory.jsp
86	studyrights.jsp
87	studysection.jsp
88	studysectiondelete.jsp
89	studySiteApndxFile.jsp
90	studySiteApndxURL.jsp
91	studySiteRights.jsp
92	studystatus.jsp
93	studytrtarmsubmit.jsp
94	studytxarmdelete.jsp
95	updateadditempatient.jsp
96	updatebudgetrights.jsp
97	updatebudgetuserrights.jsp
98	updatecopybudget.jsp
99	updateformhideseq.jsp
100	updateinvoice.jsp
101	updateLSampleSize.jsp
102	updateNewStudy.jsp
103	updatePatientFacility.jsp
104	updatepatstatus.jsp
105	updatequickpatientdetails.jsp
106	updatesection.jsp
107	updateSectionSequence.jsp
108	updatestatus.jsp
109	updateStdSiteRights.jsp


