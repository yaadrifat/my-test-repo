LOAD DATA
INFILE *
INTO TABLE ER_LABEL_TEMPLATES 
APPEND
FIELDS TERMINATED BY ','
(PK_LT "SEQ_ER_LABEL_TEMPLATES.nextval",TEMPLATE_NAME,TEMPLATE_TYPE,TEMPLATE_SUBTYPE,
 TEMP_FILE filler char,
"TEMPLATE_FORMAT" LOBFILE (TEMP_FILE) TERMINATED BY EOF NULLIF TEMP_FILE = 'NONE'
)
BEGINDATA 
,PDF417 Barcode,specimen,default,tmpl_specimen_pdf417.txt
,DataMatrix Barcode,specimen,default,tmpl_specimen_datamatrix.txt
,PDF417 Barcode,storage,default,tmpl_storage_pdf417.txt
,DataMatrix Barcode,storage,default,tmpl_storage_datamatrix.txt
