set define off;

UPDATE  track_patches
SET APP_VERSION = replace(APP_VERSION,'9.1','9.1.0') 
WHERE APP_VERSION like '9.1 %';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,1,'01_er_versionUpdate.sql',sysdate,'9.1.0 Build#645');

commit;