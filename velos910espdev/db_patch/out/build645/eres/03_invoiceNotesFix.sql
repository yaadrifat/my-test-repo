set define off;

update er_invoice set INV_NOTES = 'Your prompt payment is appreciated.' 
where INV_NOTES ='Your prompt payment is appreciated';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,3,'03_invoiceNotesFix.sql',sysdate,'9.1.0 Build#645');

commit;