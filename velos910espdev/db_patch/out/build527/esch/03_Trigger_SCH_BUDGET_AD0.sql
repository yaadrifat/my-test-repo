create or replace TRIGGER ESCH.SCH_BUDGET_AD0
AFTER DELETE
ON ESCH.SCH_BUDGET REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BUDGET', :OLD.rid, 'D');

  deleted_data := substr(
  TO_CHAR(:OLD.pk_budget) || '|' ||
  :OLD.budget_name || '|' ||
  :OLD.budget_version || '|' ||
  substr(:OLD.budget_desc,1,2500) || '|' ||
  TO_CHAR(:OLD.budget_creator) || '|' ||
  TO_CHAR(:OLD.budget_template) || '|' ||
  :OLD.budget_status || '|' ||
  TO_CHAR(:OLD.budget_currency) || '|' ||
  :OLD.budget_siteflag || '|' ||
  :OLD.budget_calflag || '|' ||
  :OLD.budget_delflag || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  TO_CHAR(:OLD.fk_site) || '|' ||
  :OLD.budget_rightscope || '|' ||
  :OLD.budget_rights || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' || 
  :old.FK_CODELST_STATUS,1,4000)|| '|' ||
  :old.BUDGET_COMBFLAG;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END; 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,3,'03_Trigger SCH_BUDGET_AD0.sql',sysdate,'8.9.0 Build#527');

commit;