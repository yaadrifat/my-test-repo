update er_object_settings
set object_displaytext =  'Pre-review'
where object_name = 'irb_subm_tab' and object_type = 'T' and fk_account = 0
and object_subtype = 'irb_assigned_tab';

update er_object_settings
set object_displaytext =  'Review Assignment'
where object_name = 'irb_subm_tab' and object_type = 'T' and fk_account = 0
and object_subtype = 'irb_compl_tab';

update er_object_settings
set object_displaytext =  'Post Review Processing'
where object_name = 'irb_subm_tab' and object_type = 'T' and fk_account = 0
and object_subtype = 'irb_post_tab';

commit;

SET DEFINE OFF;

Insert into ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (seq_ER_CODELST.nextval, NULL, 'prov_type', 'irb', 'IRB', 
    'N', 1);

Insert into ER_CODELST
   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
    CODELST_HIDE, CODELST_SEQ)
 Values
   (seq_ER_CODELST.nextval, NULL, 'prov_type', 'hipaa', 'HIPAA', 
    'N', 2);

COMMIT;

update er_submission_proviso set rid = seq_rid.nextval where rid is null;

commit;

update er_submission_proviso
set fk_codelst_provisotype = (select pk_codelst from er_codelst where codelst_type='prov_type' and codelst_subtyp = 'irb' );

commit;

update er_browserconf
set BROWSERCONF_SEQ = BROWSERCONF_SEQ+1 where FK_BROWSER = (select pk_browser
from er_browser
where browser_module = 'irbReview') and BROWSERCONF_SEQ>=8;


Insert into ER_BROWSERCONF
   (PK_BROWSERCONF, FK_BROWSER, BROWSERCONF_COLNAME, BROWSERCONF_SEQ, BROWSERCONF_SETTINGS, 
    BROWSERCONF_EXPLABEL)
select  seq_ER_BROWSERCONF.nextval, (select pk_browser
from er_browser
where browser_module = 'irbReview'), 'SUBMISSION_MEETING_DATE', 8, '{"key":"SUBMISSION_MEETING_DATE", "label":"Meeting Date", "sortable":false, "resizeable":true,"hideable":true}', 
    'Meeting Date' from dual;
    
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,74,9,'09_data.sql',sysdate,'8.9.0 Build#531');

commit;