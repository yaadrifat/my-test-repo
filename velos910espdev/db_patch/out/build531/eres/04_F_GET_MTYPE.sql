CREATE OR REPLACE FUNCTION        "F_GET_MTYPE" (V_VALUE CHAR)
RETURN VARCHAR2 AS V_RETVAL VARCHAR2(50);
BEGIN
  CASE V_VALUE
    WHEN 'PM' THEN RETURN 'Patient Milestone';
    WHEN 'VM' THEN RETURN 'Visit Milestone';
    WHEN 'EM' THEN RETURN 'Event Milestone';
    WHEN 'SM' THEN RETURN 'Study Milestone';
    WHEN 'AM' THEN RETURN 'Additional Milestone';
    ELSE RETURN NULL;
  END CASE;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,74,4,'04_F_GET_MTYPE.sql',sysdate,'8.9.0 Build#531');

commit;