
set define off;



Insert into ER_MAILCONFIG(
PK_MAILCONFIG,
MAILCONFIG_MAILCODE,
MAILCONFIG_SUBJECT,
MAILCONFIG_CONTENT,
MAILCONFIG_CONTENTTYPE,
MAILCONFIG_FROM,
MAILCONFIG_BCC,
MAILCONFIG_CC,
DELETEDFLAG,
FK_CODELSTORGID,
RID,
MAILCONFIG_HASATTACHEMENT,
CREATED_ON,
CREATOR,
LAST_MODIFIED_DATE,
LAST_MODIFIED_BY,
IP_ADD) 
values (SEQ_ER_MAILCONFIG.nextval,'UnitReport',
'request on your CBU # :CBUID for NMDP',
'Dear Colleague,
A Unit Report has been request on your CBU # [CBUID] for NMDP
Please Use This Link <a href="[URL]">CDR Unit Report Request For Link</a>  to attach the unit report and complete additional CBU Information as you are able to aid the Transplant Center in their section.',
'text',
'velos_garuda@del.aithent.com',
'velos_garuda@del.aithent.com',
0,0,0,1,0,sysdate,null,sysdate,null,null
);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,139,3,'03_ER_MAILCONFIG_INSERT.sql',sysdate,'9.0.0 Build#596');

commit;