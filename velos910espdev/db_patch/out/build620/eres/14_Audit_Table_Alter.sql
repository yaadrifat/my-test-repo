set define off;
DECLARE
  v_column_exists number := 0;  
BEGIN
	Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'AUDIT_COLUMN' AND column_name = 'REMARKS'; 
	if (v_column_exists = 0) then
		execute immediate 'alter table "ERES"."AUDIT_COLUMN" add(REMARKS VARCHAR2(4000))';
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,163,14,'14_Audit_Table_Alter.sql',sysdate,'9.0.0 Build#620');

commit;