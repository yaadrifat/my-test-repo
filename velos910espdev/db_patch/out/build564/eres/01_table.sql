SET DEFINE OFF; 

DECLARE 
  col_count NUMBER; 
BEGIN 
  select count(*) into col_count
   from all_tab_columns where owner = 'ERES' and 
  table_name = 'ER_STORAGE' and 
  column_name = 'FK_STORAGE_KIT';
  if (col_count > 0) then 
    execute immediate 'ALTER TABLE ERES.ER_STORAGE '|| 
    ' DROP (FK_STORAGE_KIT)'; 
  end if; 
  execute immediate 'ALTER TABLE ERES.ER_STORAGE '|| 
    ' ADD (FK_STORAGE_KIT NUMBER)'; 
  execute immediate 'COMMENT ON COLUMN '|| 
    ' ERES.ER_STORAGE.FK_STORAGE_KIT IS '|| 
    ' ''For main kit copy only. Stores the pk of main kit.''';
END; 
/ 

DECLARE 
  col_count NUMBER; 
BEGIN 
  select count(*) into col_count
   from all_tab_columns where owner = 'ERES' and 
  table_name = 'ER_SPECIMEN' and 
  column_name = 'SPEC_EXPETCTED_QUANTITY';
  if (col_count > 0) then 
    execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' DROP (SPEC_EXPETCTED_QUANTITY)'; 
  end if; 
  execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' ADD (SPEC_EXPETCTED_QUANTITY NUMBER)'; 
  execute immediate 'COMMENT ON COLUMN '|| 
    ' ERES.ER_SPECIMEN.SPEC_EXPETCTED_QUANTITY IS '|| 
    ' ''Stores the specimen expected quantity.''';
END; 
/ 

DECLARE 
  col_count NUMBER; 
BEGIN 
  select count(*) into col_count
   from all_tab_columns where owner = 'ERES' and 
  table_name = 'ER_SPECIMEN' and 
  column_name = 'SPEC_DISPOSITION';
  if (col_count > 0) then 
    execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' DROP (SPEC_DISPOSITION)'; 
  end if; 
  execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' ADD (SPEC_DISPOSITION NUMBER)'; 
  execute immediate 'COMMENT ON COLUMN '|| 
    ' ERES.ER_SPECIMEN.SPEC_DISPOSITION IS '|| 
    ' ''Stores the specimen disposition.''';
END; 
/ 

DECLARE 
  col_count NUMBER; 
BEGIN 
  select count(*) into col_count
   from all_tab_columns where owner = 'ERES' and 
  table_name = 'ER_SPECIMEN' and 
  column_name = 'SPEC_ENVT_CONS';
  if (col_count > 0) then 
    execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' DROP (SPEC_ENVT_CONS)'; 
  end if; 
  execute immediate 'ALTER TABLE ERES.ER_SPECIMEN '|| 
    ' ADD (SPEC_ENVT_CONS Varchar2(255 CHAR))'; 
  execute immediate 'COMMENT ON COLUMN '|| 
    ' ERES.ER_SPECIMEN.SPEC_ENVT_CONS IS '|| 
    ' ''Stores the specimen environmental constraints''';
END; 
/ 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,107,1,'01_table.sql',sysdate,'8.10.0 Build#564');

commit;
