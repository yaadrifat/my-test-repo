--To Check NO of Jobs in database 
--SELECT * FROM dba_scheduler_jobs WHERE job_name = 'AUDIT_ROW_MODULE_UPDATE';

--To drop Jobs in database
--BEGIN
--DBMS_SCHEDULER.drop_job (job_name => 'AUDIT_ROW_MODULE_UPDATE');
--END;

--To create Jobs in database
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'AUDIT_ROW_MODULE_UPDATE',
   start_date         =>  SYSTIMESTAMP,
   repeat_interval    =>  'FREQ=MINUTELY; INTERVAL=1;',
   job_type           =>  'STORED_PROCEDURE',
   job_action         =>  'ESCH.PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ROW_MODULE_UPDATE',
   enabled            =>  TRUE,
   comments           =>  'To Update Blank Module IDs in Budget Audit row table');
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,167,9,'09_JOB_AUDIT_ROW_MODULE_UPDATE.sql',sysdate,'9.0.0 Build#624');

commit;
