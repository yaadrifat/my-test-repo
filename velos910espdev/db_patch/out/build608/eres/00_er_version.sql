set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '9.0.0 Build#608' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,151,0,'00_er_version.sql',sysdate,'9.0.0 Build#608');

commit;

