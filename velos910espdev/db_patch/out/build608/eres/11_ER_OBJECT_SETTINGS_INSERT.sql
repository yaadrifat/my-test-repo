set define off;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'maint_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'maint_menu', 'top_menu', 4, 
      0, 'Maintenance', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting maint_menu for top_menu already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'centr_manag' and OBJECT_NAME = 'maint_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'centr_manag', 'maint_menu', 1, 
        0, 'Center Management', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting Center Management for maint_menu already exists');
  end if;
END;
/



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbb_profile' and OBJECT_NAME = 'centr_manag';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'cbb_profile', 'centr_manag', 1, 
      0, 'CBB Profile', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbb_profile for centr_manag already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'proc_proced' and OBJECT_NAME = 'centr_manag';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'proc_proced', 'centr_manag', 2, 
      0, 'Processing Procedures', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting proc_proced for centr_manag already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'my_pro' and OBJECT_NAME = 'maint_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'my_pro', 'maint_menu', 2, 
    0, 'My Profile', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting my_pro for maint_menu already exists');
  end if;
END;
/


commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,151,11,'11_ER_OBJECT_SETTINGS_INSERT.sql',sysdate,'9.0.0 Build#608');

commit;
