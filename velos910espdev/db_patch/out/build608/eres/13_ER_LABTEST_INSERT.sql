set define off;

Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values (SEQ_ER_LABTEST.nextval,'Other','OTH',null,null,null);

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,151,13,'13_ER_LABTEST_INSERT.sql',sysdate,'9.0.0 Build#608');

commit;
