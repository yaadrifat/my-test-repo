-- INSERTING DATA INTO CB_QUESTION TABLE FOR IDM FORMS --

--QUESTION 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cms_cert_lab_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Were all screening tests performed in a CMS approved laboratory?', '<u>Current Evaluation and Action</u><br/>If Yes - no action.<br/>If Some or None - the CBU should be considered Incomplete if the collection date is on or after May 25, 2005.<br/>If Some or None - no action if the collection date is before May 25, 2005.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.<br/>', '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cms_cert_lab_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'fda_lic_mfg_inst_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Were all screening tests performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instruction?', '<u>Current Evaluation and Action</u><br/>If Yes - no action.<br/>If Some or None - the CBU should be considered Incomplete if the collection  date is on or after May 25, 2005.<br/>If Some or None - no action if the collection date is before May 25, 2005.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.<br/>', '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'fda_lic_mfg_inst_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HBsAg','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>
If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.</br>NMDP and 10-CBA requires valid test for HBsAg.<br/>', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hbsag_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'hbsag_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hbsag_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hbsag_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HBc','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Ineligible if collection date on or after May 25, 2005.<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Incomplete if collection date on or after May 25, 2005.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hbc_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hbc_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 4.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hbc_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 4.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hbc_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HCV','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>
If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA requires valid test for anti-HCV.<br/>', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hcv_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hcv_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 5.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hcv_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 5.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hcv_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HIV 1/2 + O','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - then Question 7 is required, Defer if Question 7 is also Not Done.<br/>If test completed, Question 7 will be inactivated.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA requires valid test for anti-HIV 1/2.  Anti-HIV 1/2 + O testing is required when related questions are not included as part of maternal donor screnning for eligiblity determination.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_o_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hiv_1_2_o_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 6.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_o_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 6.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_o_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HIV 1/2','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - then Question 6 is required, Defer if Question 6 is also Not Done.<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA requires valid test for anti-HIV 1/2.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hiv_1_2_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 7.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 7.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_1_2_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HTLV I/II','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Incomplete if the collection date is on or after May 25, 2005.<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_htlv1_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_htlv1_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 8.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_htlv1_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 8.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_htlv1_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-CMV Total','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - No action<br/>If Non-Reactive/Negative - No action<br/>br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Anti-CMV Total is not part of eligibility testing.<br/>Anti-CMV Total (total IgG + IgM) was required by the NMDP by August 27, 2007.  If Anti-CMV Total was not performed, record IgG or IgM results in Additional/Miscellaneious IDM Tests  section.<br/>', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cmv_total_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'cmv_total_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 9.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cmv_total_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 9.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cmv_total_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'NAT HIV-1 / HCV / HBV (MPX)','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Questions 11 and 13 are required and Question 14 is optional.  Defer if Questions 10, 11 and 12 are all Not Done.<br/>If tests performed, Question 11, 13 and 14 are inactivated.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/> Combination MPX test added October 1, 2011 as an option for reporting NAT results. If test performed on historical inventory, results may be reported in this field.<br/>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_1_hcv_hbv_mpx_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_hiv_1_hcv_hbv_mpx_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 10.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_1_hcv_hbv_mpx_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 10.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_1_hcv_hbv_mpx_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HIV-1 NAT','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - and Question 10 is also Not Done, then Question 12 is required.<br/> If tests performed, Questions 10 and 12 are inactivated.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_hiv_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 11.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_rslt';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HIV p24 Ag','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - Defer if collection date on or after February 28, 2004.  No action if collection date before February 28, 2004.<br/>If Not Done - Defer<br/>Required if MPX Question 10 and Question 11 are both Not Done, otherwise inactive.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.</u>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hiv_antigen_rslt',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'hiv_antigen_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 12.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hiv_antigen_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 12.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hiv_antigen_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HCV NAT','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>
If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.</br>NMDP and 10-CBA requires valid test for HBsAg.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hcv_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_hcv_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 13.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hcv_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 13.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hcv_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HBV NAT','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>
If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.</br>NMDP and 10-CBA requires valid test for HBsAg.<br/>', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hbv_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_hbv_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 14.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hbv_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 14.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hbv_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Syphilis','<u>Current Evaluation and Action</u><br/>If Reactive/Positive -  Evaluate against the confirmatory test results. Question 18 is required.<br/>If Non-Reactive/Negative - No action<br/>If Not Done -Incomplete if the collection date is on or after May 25, 2005.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Accommodates incomplete eligibility status when testing not performed using FDA-cleared screening tests or FDA-cleared diagnostic serologic tests; implemented on October 1, 2011.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'syphilis_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'syphilis_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 15.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'syphilis_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 15.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'syphilis_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'WNV NAT','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done -Incomplete if the collection date is on or after May 25, 2005.<br/>
<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.<br/>WNV NAT testing was required by the NMDP by October 19, 2004.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_west_nile_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_west_nile_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_west_nile_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 16.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_west_nile_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Chagas','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/> If Non-Reactive/Negative - No action<br/>If Not Done -Incomplete if the collection date is on or after January 1, 2008.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.<br/>Chagas (T.cruzi) testing was not required by January 1, 2008.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'chagas_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'chagas_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'chagas_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 17.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_fda';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer’s instructions?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'chagas_react_fda',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Syphilis Confirmatory / Supplemental Test','<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Indeterminate - Defer<br/>If Not Done - Defer<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>As of October 1, 2011, specific FDA-approved confirmatory test (FTA-ABS) must be used (see Question 19)  or unit is deferred.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'syphilis_ct_sup_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', NULL, '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'syphilis_ct_sup_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_cms';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was this screening test performed in a CMS approved laboratory?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'syphilis_ct_sup_react_cms',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'treponemal_ct_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was the syphilis confirmatory test performed using a FDA-cleared confirmatory test (FTA-ABS)?', '<u>Current Evaluation and Action</u><br/>If Yes - Ineligible (Question 15 is Reactive/Positive, Question 18 is Non-Reactive/Negative)<br/>If No - Defer<br/>Question is activated based on response to Question 18.  This question active if Non-Reactive/Negative response to syphilis confirmatory Question 18.<br/><br/><u>Historical Evaluation</u><br/>Specific FDA-approved confirmatory test (FTA-ABS) must be used in order to status as ineligible.<br/>', '1', '0', '1', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'treponemal_ct_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_1_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_1_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_1_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_1_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_2_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_2_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_2_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_2_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_3_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_3_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_3_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_3_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_4_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_4_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_4_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_4_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_5_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_5_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 24.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_5_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_5_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_6_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, null, null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'other_idm_6_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 25.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_6_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', null, '0', '0', '0', '0', '0', NULL, '1', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'other_idm_6_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,6,'06_cb_question.sql',sysdate,'9.0.0 Build#636');

commit;