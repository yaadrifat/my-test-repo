--Update ER_CTRLTAB 

Set define off;
DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRLTAB'
    AND column_name = 'CTRL_DESC';
  if (v_column_value_update =1) then
   UPDATE ER_CTRLTAB SET CTRL_DESC ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PF Order Assignment'
   WHERE CTRL_DESC='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PF Pending Screen' AND CTRL_VALUE='CB_PFPENDINGW' AND CTRL_KEY='app_rights';
   commit;
  end if;
end;
/




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,15,'15_ASSESS_RIGHT_UPDATE.sql',sysdate,'9.0.0 Build#636');

commit;
