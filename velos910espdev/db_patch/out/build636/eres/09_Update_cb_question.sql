--STRAT UPDATE CB_QUESTIONS TABLE--
--QUESTION 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_1_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0', ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_1_react_date';
commit;
  end if;
end;
/

--QUESTION 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_2_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0', ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_2_react_date';
commit;
  end if;
end;
/

--QUESTION 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_3_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0', ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_3_react_date';
commit;
  end if;
end;
/

--QUESTION 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_4_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0',ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_4_react_date';
commit;
  end if;
end;
/

--QUESTION 24.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_5_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0', ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_5_react_date';
commit;
  end if;
end;
/

--QUESTION 25.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_6_react_date';
  if (v_record_exists = 1) then
	update cb_questions set ADD_COMMENT_FLAG='0', ASSESMENT_FLAG='0' where QUES_CODE = 'other_idm_6_react_date';
commit;
  end if;
end;
/
--END--
--START UPDATE CB_FORM_QUESTION TABLE--
--QUESTION 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_1_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_1_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_1_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/

--QUESTION 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_2_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_2_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_2_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/
--QUESTION 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_3_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_3_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_3_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/
--QUESTION 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_4_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_4_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_4_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/
--QUESTION 24.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_5_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_5_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_5_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/
--QUESTION 25.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
  where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_6_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET FK_MASTER_QUES= (SELECT pk_questions FROM cb_questions WHERE 			QUES_CODE='other_idm_6_react_ind') where fk_question=(select pk_questions from cb_questions where ques_code='other_idm_6_react_date') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  
  
commit;
  end if;
end;
/
--END--
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='immune_def' AND CODELST_SUBTYP = 'cis';
  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Combined Immunodeficiency  Syndrome'' where  CODELST_TYPE = ''immune_def'' AND CODELST_SUBTYP = ''cis''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='immune_def' AND CODELST_SUBTYP = 'cvid';
  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Common Variable Immunodeficiency'' where  CODELST_TYPE = ''immune_def'' AND CODELST_SUBTYP = ''cvid''';
  end if;
end;
/
--END--
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,9,'09_Update_cb_question.sql',sysdate,'9.0.0 Build#636');

commit;