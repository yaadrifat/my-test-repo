update ER_REPORT set REP_HIDE='Y' where PK_REPORT=140;

commit;

Update ER_CTRLTAB set CTRL_DESC = CTRL_DESC || ' / Combined Budget'
where CTRL_VALUE= 'STUDYCAL' and CTRL_key='study_rights';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,85,1,'01_data.sql',sysdate,'8.9.0 Build#542');

commit;