set define off;
set scan off;

Insert into ER_MAILCONFIG(
PK_MAILCONFIG,
MAILCONFIG_MAILCODE,
MAILCONFIG_SUBJECT,
MAILCONFIG_CONTENT,
MAILCONFIG_CONTENTTYPE,
MAILCONFIG_FROM,
MAILCONFIG_BCC,
MAILCONFIG_CC,
DELETEDFLAG,
FK_CODELSTORGID,
RID,
MAILCONFIG_HASATTACHEMENT,
CREATED_ON,
CREATOR,
LAST_MODIFIED_DATE,
LAST_MODIFIED_BY,
IP_ADD) 
values (SEQ_ER_MAILCONFIG.nextval,'TranCenter',
'Declaration Of Urgent medical Need',
CHR(13) || 
'To [USER]' || CHR(13) || 
CHR(13) || 
'From [CM]:' || CHR(13) || 
CHR(13) || 
'Dear Colleague,' || CHR(13) || 
'Urgent Medical Need is required for CBU # [CBUID] for NMDP' || CHR(13) || 
'Please Use This Link <a href="[URL]">CDR Clinical Record Link</a>  to sign off an Urgent Medical Need request.' || CHR(13) || 
CHR(13) || 
'Kind Regards,' || CHR(13) || 
'[CM]' || 
CHR(13),
'text/html',
'velos_garuda@del.aithent.com',
'velos_garuda@del.aithent.com',
0,0,0,1,0,sysdate,null,sysdate,null,null
);

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,3,'03_ER_MAILCONFIG_INSERT.sql',sysdate,'9.0.0 Build#602');

commit;