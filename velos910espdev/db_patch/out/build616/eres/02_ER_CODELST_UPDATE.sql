set define off;

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'phy_find_ass';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Physical Findings Assessment' where codelst_type = 'ineligible'AND codelst_subtyp = 'phy_find_ass';
	commit;
  end if;
end;
/
--END--

set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'vaibility'
    AND codelst_subtyp = '7aadt';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='7AAD' where codelst_type = 'vaibility' AND codelst_subtyp = '7aadt';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='idm_sub_ques' AND CODELST_SUBTYP = 'subques1';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Were all screening tests performed in a CMS approved laboratory?'' where  CODELST_TYPE = ''idm_sub_ques'' AND CODELST_SUBTYP = ''subques1''';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='idm_sub_ques' AND CODELST_SUBTYP = 'subques2';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Were all screening tests performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''''s instruction?'' where  CODELST_TYPE = ''idm_sub_ques'' AND CODELST_SUBTYP = ''subques2''';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='test_outcome' AND CODELST_SUBTYP = 'posi';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Reactive/Positive'' where  CODELST_TYPE = ''test_outcome'' AND CODELST_SUBTYP = ''posi''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='test_outcome' AND CODELST_SUBTYP = 'negi';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Non-Reactive/Negative'' where  CODELST_TYPE = ''test_outcome'' AND CODELST_SUBTYP = ''negi''';
  end if;
end;
/
--END--

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,2,'02_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#616');

commit;