Set Define Off;

Update ER_MAILCONFIG mail set mail.MAILCONFIG_SUBJECT='Request on your 
CBU  [CBUID] for NMDP' where mail.mailconfig_mailcode='UnitReport';


Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,4,'04_ER_MAILCONFIG_UPDATE.sql',sysdate,'9.0.0 Build#600');

commit;