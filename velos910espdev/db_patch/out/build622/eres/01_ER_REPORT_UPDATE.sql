SET DEFINE OFF;
UPDATE ER_REPORT
SET REP_SQL_CLOB =
  'SELECT TO_CHAR(PATFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS PATFORMS_FILLDATE,trim(TO_CHAR(PATFORMS_FILLDATE,''MONTH'')) || '' '' || TO_CHAR(PATFORMS_FILLDATE,''yyyy'') AS PATFORMS_FILLDATE_MONTH, form_name, description, study_number, study_title, per_code, patprot_patstdid, TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS patprot_enroldt FROM (
SELECT PATFORMS_FILLDATE,
(SELECT form_name FROM ER_FORMLIB WHERE pK_FORMLIB = a.FK_FORMLIB) form_name,
F_Getformbrowsedata(fk_formlib, pk_patforms) AS Description,
a.FK_PER,
FK_PATPROT, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ER_PATFORMS a, ER_PATPROT, ER_STUDY, ER_PER
WHERE pk_patprot = fk_patprot AND
fk_study in (:studyId) AND
a.fk_per in (:patientId)  AND
PATFORMS_FILLDATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
pk_study = fk_study AND
a.fk_per = pk_per AND
NVL(a.record_type,''N'') <> ''D''
UNION
SELECT test_date,''Lab'', (SELECT labtest_name FROM ER_LABTEST WHERE pk_labtest = fk_testid) || ''  :  '' ||  test_result || '' '' || test_unit ,
a.fk_per, fk_patprot,study_number, study_title, per_code, NULL AS patprot_patstdid, NULL AS patprot_enroldt
FROM ER_PATLABS a, ER_STUDY, ER_PER
WHERE a.fk_per in (:patientId) AND
a.fk_study in (:studyId)
AND test_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_study = a.fk_study AND
a.fk_per = pk_per
UNION
SELECT ae_stdate, ''Adverse Event'', ''Adverse Event : '' || AE_NAME || DECODE(fk_codlst_aetype,(SELECT pk_codelst FROM ESCH.sch_codelst WHERE codelst_type = ''adve_type'' AND codelst_subtyp=''al_sadve''),''X@*@X'','''') || ''X@@@XGrade : '' || AE_GRADE || ''X@@@XStop DATE : '' ||  TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) || ''X@@@XRelationship TO THE study : '' || (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst=ae_relationship) || ''X@@@XOutcome TYPE : '' || DECODE(SUBSTR(AE_OUTTYPE,1,1),''1'',''[ Death '' || to_char(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT) || '' ] '') || DECODE(SUBSTR(AE_OUTTYPE,2,1),''1'',''[ Life-threatening ] '') || DECODE(SUBSTR(AE_OUTTYPE,3,1),''1'',''[ Hospitalization ] '')  || DECODE(SUBSTR(AE_OUTTYPE,4,1),''1'',''[ Disability ] '') || DECODE(SUBSTR(AE_OUTTYPE,5,1),''1'',''[ Congenital Anomaly ] '') || DECODE(SUBSTR(AE_OUTTYPE,6,1),''1'',''[ Required Intervention ] '') || DECODE(SUBSTR(AE_OUTTYPE,7,1),''1'',''[ Recovered ]''),
pk_per, pk_patprot, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ESCH.sch_adverseve a, ER_PER b,ER_STUDY c, ER_PATPROT d
WHERE a.fk_study = c.pk_study AND
a.fk_per = b.pk_per AND
d.patprot_stat = 1 AND
d.PATPROT_ENROLDT IS NOT NULL AND
a.fk_study = d.fk_study AND
a.fk_per = d.fk_per AND
a.fk_per in (:patientId) AND
a.fk_study in (:studyId)  AND
ae_stdate BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY 1)'
WHERE PK_REPORT = 93;
COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,1,'01_ER_REPORT_UPDATE.sql',sysdate,'9.0.0 Build#622');

commit;
