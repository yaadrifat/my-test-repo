CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_PREPOST_INFO" ("CORD_REGISTRY_ID", "TEST_NAME", "TIMING_OF_TEST", "TEST_RESULT", "TEST_METHOD", "TEST_COMMENTS", "SAMPLE_TYPE", "TEST_DATE", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CBB_NAME", "LAST_MODIFIED_DATE", "CREATED_ON", "CREATOR", "FK_ACCOUNT", "TEST_NOTES", "LAST_MODIFIED_BY")
AS
  SELECT cord.CORD_REGISTRY_ID,
    (SELECT LABTEST_NAME
    FROM ER_LABTEST
    WHERE ER_LABTEST.PK_LABTEST =e.fk_testid
    ),
    f_codelst_desc(e.fk_timing_of_test),
    e.TEST_RESULT,
    f_codelst_desc(e.FK_TEST_METHOD),
    e.CUSTOM002,
    f_codelst_desc(e.fk_test_specimen),
    e.TEST_DATE,
    cord.CORD_LOCAL_CBU_ID,
    cord.REGISTRY_MATERNAL_ID ,
    cord.MATERNAL_LOCAL_ID,
    er.site_name AS CBB_NAME,
    e.LAST_MODIFIED_DATE,
    e.CREATED_ON,
    u.usr_firstname
    ||' '
    || u.usr_lastname,
    er.fk_account AS FK_ACCOUNT,
    e.NOTES,
    u.usr_firstname
    ||' '
    ||u.usr_lastname
  FROM ER_PATLABS e,
    CB_CORD cord,
    ER_LABTEST el,
    ER_SITE er,
    ER_USER u
  WHERE e.fk_specimen    = cord.fk_specimen_id
  AND e.fk_testid        = el.PK_LABTEST
  AND cord.fk_cbb_id     = er.pk_site
  AND e.creator          = u.PK_USER
  AND e.LAST_MODIFIED_BY = u.PK_USER;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,34,'34_REP_CBU_PREPOST_INFO.sql',sysdate,'9.0.0 Build#622');

commit;

  