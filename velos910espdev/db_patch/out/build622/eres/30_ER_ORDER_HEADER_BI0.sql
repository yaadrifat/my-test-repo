create or replace TRIGGER "ERES".ER_ORDER_HEADER_BI0 BEFORE INSERT ON ER_ORDER_HEADER
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
      NEW_ORDER_ENTITYTYPE                    VARCHAR2(200);
      NEW_FK_ORDER_TYPE                    VARCHAR2(200);
      NEW_FK_ORDER_STATUS                    VARCHAR2(200);
      NEW_FK_SITE_ID                    VARCHAR2(200);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
IF NVL(:OLD.ORDER_ENTITYTYPE,0) !=     NVL(:NEW.ORDER_ENTITYTYPE,0) THEN
  BEGIN
   		SELECT F_Codelst_Desc(:new.ORDER_ENTITYTYPE)		INTO NEW_ORDER_ENTITYTYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_ENTITYTYPE := '';
   END; 
   END IF;  
   IF NVL(:OLD.FK_ORDER_TYPE,0) !=     NVL(:NEW.FK_ORDER_TYPE,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_TYPE)		INTO NEW_FK_ORDER_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_TYPE := '';
   END;  
   END IF;  
   IF NVL(:OLD.FK_ORDER_STATUS,0) !=     NVL(:NEW.FK_ORDER_STATUS,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_STATUS)		INTO NEW_FK_ORDER_STATUS  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_STATUS := '';
   END;
  END IF;
   IF NVL(:OLD.FK_SITE_ID,0) !=     NVL(:NEW.FK_SITE_ID,0) THEN
   BEGIN
   		SELECT F_GET_ER_SITE_NAME(:new.FK_SITE_ID)		INTO NEW_FK_SITE_ID  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SITE_ID := '';
   END; 
   END IF;
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_ORDER_HEADER',erid, 'I',usr);

  INSERT_DATA:=:NEW.PK_ORDER_HEADER || '|' ||
to_char(:NEW.ORDER_GUID) || '|' ||
to_char(:NEW.ORDER_NUM) || '|' ||
to_char(:NEW.ORDER_ENTITYID) || '|' ||
NEW_ORDER_ENTITYTYPE || '|' ||
to_char(:NEW.ORDER_OPEN_DATE) || '|' ||
to_char(:NEW.ORDER_CLOSE_DATE) || '|' ||
NEW_FK_ORDER_TYPE || '|' ||
NEW_FK_ORDER_STATUS || '|' ||
to_char(:NEW.ORDER_STATUS_DATE) || '|' ||
to_char(:NEW.ORDER_REMARKS) || '|' ||
to_char(:NEW.ORDER_REQUESTED_BY) || '|' ||
to_char(:NEW.ORDER_APPROVED_BY) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
NEW_FK_SITE_ID;
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,30,'30_ER_ORDER_HEADER_BI0.sql',sysdate,'9.0.0 Build#622');

commit;