set define off;

DECLARE

countFlage1 NUMBER(5);
BEGIN

SELECT COUNT(1) INTO countFlage1 FROM USER_TABLES WHERE TABLE_NAME='ER_OBJECT_SETTINGS';
if (countFlage1 > 0) then
   UPDATE eres.ER_OBJECT_SETTINGS SET OBJECT_DISPLAYTEXT='Manage Visits'  where  object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Schedule and Customize';
  commit;
end if;

END;
/

INSERT INTO eres.track_patches
VALUES(eres.seq_track_patches.nextval,134,2,'02_ER_OBJECT_SETTINGS.sql',sysdate,'9.0.0 Build#591');

commit;