--Update custom cols of new skins.

Update eres.er_codelst set CODELST_CUSTOM_COL = 'redmond' where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_redmond';
Update eres.er_codelst set CODELST_CUSTOM_COL = 'bethematch' where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_bethematch';

COMMIT;

INSERT INTO eres.track_patches
VALUES(eres.seq_track_patches.nextval,134,1,'01_skin_codelst.sql',sysdate,'9.0.0 Build#591');

commit;