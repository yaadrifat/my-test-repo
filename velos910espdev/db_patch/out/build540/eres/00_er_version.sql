set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '8.9.0 Build#540' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,83,0,'00_er_version.sql',sysdate,'8.9.0 Build#540');

commit;
