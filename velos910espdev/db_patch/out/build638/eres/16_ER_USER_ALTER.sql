set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_USER'
    AND COLUMN_NAME = 'USR_CURRNTSESSID'; 
	
	if (v_column_exists < 1) then
		execute immediate 'ALTER TABLE ER_USER ADD USR_CURRNTSESSID VARCHAR(50)';
		dbms_output.put_line('Column USR_CURRNTSESSID is added to ER_USER');
	else
		dbms_output.put_line('Column USR_CURRNTSESSID already exists in ER_USER');
	end if;
END;
/


COMMENT ON COLUMN ER_USER.USR_CURRNTSESSID IS 'This column stores the Current Logged in User Session ID';



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,16,'16_ER_USER_ALTER.sql',sysdate,'9.0.1 Build#638');

commit;