set define off;

declare
v_table_exists number default 0;
begin
	Select count(*) into v_table_exists
    from USER_TABLES
    where UPPER(TABLE_NAME) = 'AUDIT_COLUMN_MODULE'; 
	
	if (v_table_exists > 0) then
		DELETE FROM "ESCH"."AUDIT_COLUMN_MODULE";
		COMMIT;
		
		execute immediate 'DROP TABLE "ESCH"."AUDIT_COLUMN_MODULE" ';
	end if;	
	
	Select count(*) into v_table_exists
    from USER_TABLES
    where UPPER(TABLE_NAME) = 'AUDIT_ROW_MODULE'; 
	
	if (v_table_exists > 0) then
		DELETE FROM "ESCH"."AUDIT_ROW_MODULE";
		COMMIT;
		
		execute immediate 'DROP TABLE "ESCH"."AUDIT_ROW_MODULE" ';
	end if;	
	
end;
/


CREATE TABLE "ESCH"."AUDIT_ROW_MODULE" 
(	"PK_ROW_ID" 		NUMBER(10,0) NOT NULL ENABLE, 
	"TABLE_NAME" 		VARCHAR2(30 BYTE), 
	"RID" 				NUMBER(10,0), 
	"MODULE_ID" 		NUMBER(10,0), 
	"ACTION" 			VARCHAR2(1 BYTE), 
	"TIMESTAMP" 		DATE, 
	"USER_ID" 			NUMBER(10,0), 
	"CHANGE_LOCATION" 	VARCHAR2(100 BYTE), 
	PRIMARY KEY ("PK_ROW_ID")
) ;

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."PK_ROW_ID" IS 'Primary key of the table , this is generated from the Database sequence';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."TABLE_NAME" IS 'This column stores name of the table for which the auditing is being done.';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."RID" IS 'This column stores Record ID (RID) from the table.This will identify the row on which the audit is being done.';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."MODULE_ID" IS 'This column stores Primary key of the module (for example pk_budget/fk_budget for budget module).';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."ACTION" IS 'This column stores the action performed (I - Insert, U-Update, D-Delete)';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."TIMESTAMP" IS 'This column stores the Date and Time of the action';

	COMMENT ON COLUMN "ESCH"."AUDIT_ROW_MODULE"."USER_ID" IS 'This column stores the userid who made the transaction.';

	COMMENT ON TABLE "ESCH"."AUDIT_ROW_MODULE"  IS 'This table stores the ModuleID, Table(s) in the module and transaction actions on which the audit operation is being done.';

GRANT ALL ON "ESCH"."AUDIT_ROW_MODULE" TO ERES;
 
CREATE TABLE "ESCH"."AUDIT_COLUMN_MODULE"
(
	"PK_COLUMN_ID"			NUMBER(10,0) NOT NULL,
	"FK_ROW_ID"		 		NUMBER(10,0) NOT NULL ,
	"COLUMN_NAME"			VARCHAR2(30 BYTE) ,
	"OLD_VALUE"				VARCHAR2(4000 BYTE),
	"NEW_VALUE"				VARCHAR2(4000 BYTE),
	"FK_CODELST_REASON" 	NUMBER(10,0),
	"REMARKS"				VARCHAR2(4000 BYTE), 
  PRIMARY KEY ("PK_COLUMN_ID")
);

   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."PK_COLUMN_ID" IS 'Primary key of the table, this is generated from the Database sequence';
 
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."FK_ROW_ID" IS 'Foreign key of the table AUDIT_ROW_MODULE.';
 
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."COLUMN_NAME" IS 'The column name of the the audited column.';
 
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."OLD_VALUE" IS 'Old data value of the affected column.';
 
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."NEW_VALUE" IS 'New data value of the affected column.';
   
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."FK_CODELST_REASON" IS 'This column stores the REASON ID from code list table.';
      
   COMMENT ON COLUMN "ESCH"."AUDIT_COLUMN_MODULE"."REMARKS" IS 'This column stores the REMARKS (Additional details/comments) related to changes' ;
   
   COMMENT ON TABLE "ESCH"."AUDIT_COLUMN_MODULE"  IS 'This table stores the column details ie the old and new values of the updated/inserted/deleted  columns';
 


--Primary key and Foreigm Key reference between "AUDIT_ROW_MODULE" and "AUDIT_COLUMN_MODULE"
ALTER TABLE "ESCH"."AUDIT_COLUMN_MODULE"
ADD FOREIGN KEY ("FK_ROW_ID") 
REFERENCES "ESCH"."AUDIT_ROW_MODULE" ("PK_ROW_ID");

GRANT ALL ON "ESCH"."AUDIT_COLUMN_MODULE" TO ERES;

declare
v_sequence_exists number default 0;
begin
	Select count(*) into v_sequence_exists
    from USER_SEQUENCES
    where UPPER(SEQUENCE_NAME) = 'SEQ_AUDIT_ROW_MODULE'; 
	
	if (v_sequence_exists = 0) then
		--Sequence SEQ_AUDIT_ROW_MODULE for PK_ROW_ID column of AUDIT_ROW_MODULE Table 
		execute immediate 'CREATE SEQUENCE "ESCH"."SEQ_AUDIT_ROW_MODULE"
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1
            NOCACHE  ORDER  NOCYCLE';
	end if;
	
	Select count(*) into v_sequence_exists
    from USER_SEQUENCES
    where UPPER(SEQUENCE_NAME) = 'SEQ_AUDIT_COLUMN_MODULE'; 
	
	if (v_sequence_exists = 0) then
		--Sequence SEQ_AUDIT_COLUMN_MODULE for PK_COLUMN_ID column of AUDIT_COLUMN_MODULE Table 			
		execute immediate 'CREATE SEQUENCE "ESCH"."SEQ_AUDIT_COLUMN_MODULE"
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1
            NOCACHE  ORDER  NOCYCLE';
	end if;	
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,6,'06_AUDIT_TABLE_CREATION_SCRIPTS.sql',sysdate,'9.0.1 Build#638');

commit;