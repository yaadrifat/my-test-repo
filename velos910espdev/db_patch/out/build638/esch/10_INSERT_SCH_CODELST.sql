set define off;

DECLARE
  v_row_exists number;  
BEGIN
	Select count(*) into v_row_exists
    from ER_CODELST
    where CODELST_TYPE = 'audit_reason'
    AND CODELST_SUBTYP = 'req_change'; 
	
	if (v_row_exists < 1) then
		INSERT INTO SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) 
		VALUES (SCH_CODELST_SEQ1.NEXTVAL,'audit_reason','req_change','Requested Change','N',1,Sysdate);
		
		COMMIT;
		dbms_output.put_line('Code-list item "Requested Change" is inserted');
	else
		dbms_output.put_line('Code-list item "Requested Change" already exists in ER_CODELST');
	end if;
	
	Select count(*) into v_row_exists
    from ER_CODELST
    where CODELST_TYPE = 'audit_reason'
    AND CODELST_SUBTYP = 'change_std'; 
	
	if (v_row_exists < 1) then
		INSERT INTO SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) 
		VALUES (SCH_CODELST_SEQ1.NEXTVAL,'audit_reason','change_std','Change to Study Contract','N',2,Sysdate);
		
		COMMIT;
		dbms_output.put_line('Code-list item "Change to Study Contract" is inserted');
	else
		dbms_output.put_line('Code-list item "Change to Study Contract" already exists in ER_CODELST');
	end if;
	
	Select count(*) into v_row_exists
    from ER_CODELST
    where CODELST_TYPE = 'audit_reason'
    AND CODELST_SUBTYP = 'inst_change'; 
	
	if (v_row_exists < 1) then
		INSERT INTO SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) 
		VALUES (SCH_CODELST_SEQ1.NEXTVAL,'audit_reason','inst_change','Institutional Change to Procedure','N',3,Sysdate);
		
		COMMIT;
		dbms_output.put_line('Code-list item "Institutional Change to Procedure" is inserted');
	else
		dbms_output.put_line('Code-list item "Institutional Change to Procedure" already exists in ER_CODELST');
	end if;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,10,'10_INSERT_SCH_CODELST.sql',sysdate,'9.0.1 Build#638');

commit;
