--When SCH_BGTCAL table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_BGTCAL_AU2" 
AFTER UPDATE ON ESCH.SCH_BGTCAL 
REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW 
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	--This Block run when there is Delete operation perform on SCH_BGTCAL
	  IF :NEW.BGTCAL_DELFLAG = 'Y' THEN
	  --Inserting row in AUDIT_ROW_MODULE table.Action - 'D' is used for Logical Deletion of the record.
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid, 'SCH_BGTCAL', :OLD.RID,:OLD.FK_BUDGET,'D',:NEW.LAST_MODIFIED_BY);
      
	  --Inserting rows in AUDIT_COLUMN_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'PK_BGTCAL',:OLD.PK_BGTCAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'FK_BUDGET',:OLD.FK_BUDGET,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTID', :OLD.BGTCAL_PROTID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTTYPE', :OLD.BGTCAL_PROTTYPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DELFLAG', :OLD.BGTCAL_DELFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSOROHEAD', :OLD.BGTCAL_SPONSOROHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICOHEAD', :OLD.BGTCAL_CLINICOHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSORFLAG', :OLD.BGTCAL_SPONSORFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICFLAG', :OLD.BGTCAL_CLINICFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_INDIRECTCOST', :OLD.BGTCAL_INDIRECTCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'RID', :OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATOR', :OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'IP_ADD', :OLD.IP_ADD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGBENEFIT', :OLD.BGTCAL_FRGBENEFIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGFLAG', :OLD.BGTCAL_FRGFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNT', :OLD.BGTCAL_DISCOUNT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNTFLAG', :OLD.BGTCAL_DISCOUNTFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_EXCLDSOCFLAG', :OLD.BGTCAL_EXCLDSOCFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_COST', :OLD.GRAND_RES_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_TOTALCOST', :OLD.GRAND_RES_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_COST', :OLD.GRAND_SOC_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_TOTALCOST', :OLD.GRAND_SOC_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_COST', :OLD.GRAND_TOTAL_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_TOTALCOST', :OLD.GRAND_TOTAL_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_COST', :OLD.GRAND_SALARY_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_TOTALCOST', :OLD.GRAND_SALARY_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_COST', :OLD.GRAND_FRINGE_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_TOTALCOST', :OLD.GRAND_FRINGE_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_COST', :OLD.GRAND_DISC_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_TOTALCOST', :OLD.GRAND_DISC_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_COST', :OLD.GRAND_IND_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_TOTALCOST', :OLD.GRAND_IND_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_SPONSOR', :OLD.GRAND_RES_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_VARIANCE', :OLD.GRAND_RES_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_SPONSOR', :OLD.GRAND_SOC_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_VARIANCE', :OLD.GRAND_SOC_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_SPONSOR', :OLD.GRAND_TOTAL_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_VARIANCE', :OLD.GRAND_TOTAL_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD', :OLD.BGTCAL_SP_OVERHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD_FLAG', :OLD.BGTCAL_SP_OVERHEAD_FLAG,NULL,NULL,NULL);
    ELSE
	--This Block execute when there is Update operation perform on SCH_BGTCAL
		--Inserting row in AUDIT_ROW_MODULE table.
		PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid, 'SCH_BGTCAL', :OLD.RID,:OLD.FK_BUDGET,'U',:NEW.LAST_MODIFIED_BY);
	  
		--Inserting rows in AUDIT_COLUMN_MODULE table.
	  IF NVL(:OLD.PK_BGTCAL,0) != NVL(:NEW.PK_BGTCAL,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'PK_BGTCAL',:OLD.PK_BGTCAL, :NEW.PK_BGTCAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_BUDGET,0) != NVL(:NEW.FK_BUDGET,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'FK_BUDGET',:OLD.FK_BUDGET, :NEW.FK_BUDGET,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_PROTID,0) != NVL(:NEW.BGTCAL_PROTID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTID', :OLD.BGTCAL_PROTID, :NEW.BGTCAL_PROTID,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_PROTTYPE,' ') != NVL(:NEW.BGTCAL_PROTTYPE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTTYPE', :OLD.BGTCAL_PROTTYPE, :NEW.BGTCAL_PROTTYPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DELFLAG,' ') != NVL(:NEW.BGTCAL_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DELFLAG', :OLD.BGTCAL_DELFLAG, :NEW.BGTCAL_DELFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SPONSOROHEAD,0) != NVL(:NEW.BGTCAL_SPONSOROHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSOROHEAD', :OLD.BGTCAL_SPONSOROHEAD, :NEW.BGTCAL_SPONSOROHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_CLINICOHEAD,0) != NVL(:NEW.BGTCAL_CLINICOHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICOHEAD', :OLD.BGTCAL_CLINICOHEAD, :NEW.BGTCAL_CLINICOHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SPONSORFLAG,' ') != NVL(:NEW.BGTCAL_SPONSORFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSORFLAG', :OLD.BGTCAL_SPONSORFLAG, :NEW.BGTCAL_SPONSORFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_CLINICFLAG,' ') != NVL(:NEW.BGTCAL_CLINICFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICFLAG', :OLD.BGTCAL_CLINICFLAG, :NEW.BGTCAL_CLINICFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_INDIRECTCOST,0) != NVL(:NEW.BGTCAL_INDIRECTCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_INDIRECTCOST', :OLD.BGTCAL_INDIRECTCOST, :NEW.BGTCAL_INDIRECTCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_FRGBENEFIT,0) != NVL(:NEW.BGTCAL_FRGBENEFIT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGBENEFIT', :OLD.BGTCAL_FRGBENEFIT, :NEW.BGTCAL_FRGBENEFIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_FRGFLAG,0) != NVL(:NEW.BGTCAL_FRGFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGFLAG', :OLD.BGTCAL_FRGFLAG, :NEW.BGTCAL_FRGFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DISCOUNT,0) != NVL(:NEW.BGTCAL_DISCOUNT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNT', :OLD.BGTCAL_DISCOUNT, :NEW.BGTCAL_DISCOUNT,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DISCOUNTFLAG,0) != NVL(:NEW.BGTCAL_DISCOUNTFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNTFLAG', :OLD.BGTCAL_DISCOUNTFLAG, :NEW.BGTCAL_DISCOUNTFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_EXCLDSOCFLAG,0) != NVL(:NEW.BGTCAL_EXCLDSOCFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_EXCLDSOCFLAG', :OLD.BGTCAL_EXCLDSOCFLAG, :NEW.BGTCAL_EXCLDSOCFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_COST,' ') != NVL(:NEW.GRAND_RES_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_COST', :OLD.GRAND_RES_COST, :NEW.GRAND_RES_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_TOTALCOST,' ') != NVL(:NEW.GRAND_RES_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_TOTALCOST', :OLD.GRAND_RES_TOTALCOST, :NEW.GRAND_RES_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_COST,' ') != NVL(:NEW.GRAND_SOC_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_COST', :OLD.GRAND_SOC_COST, :NEW.GRAND_SOC_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_TOTALCOST,' ') != NVL(:NEW.GRAND_SOC_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_TOTALCOST', :OLD.GRAND_SOC_TOTALCOST, :NEW.GRAND_SOC_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_COST,' ') != NVL(:NEW.GRAND_TOTAL_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_COST', :OLD.GRAND_TOTAL_COST, :NEW.GRAND_TOTAL_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_TOTALCOST,' ') != NVL(:NEW.GRAND_TOTAL_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_TOTALCOST', :OLD.GRAND_TOTAL_TOTALCOST, :NEW.GRAND_TOTAL_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SALARY_COST,' ') != NVL(:NEW.GRAND_SALARY_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_COST', :OLD.GRAND_SALARY_COST, :NEW.GRAND_SALARY_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SALARY_TOTALCOST,' ') != NVL(:NEW.GRAND_SALARY_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_TOTALCOST', :OLD.GRAND_SALARY_TOTALCOST, :NEW.GRAND_SALARY_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_FRINGE_COST,' ') != NVL(:NEW.GRAND_FRINGE_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_COST', :OLD.GRAND_FRINGE_COST, :NEW.GRAND_FRINGE_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_FRINGE_TOTALCOST,' ') != NVL(:NEW.GRAND_FRINGE_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_TOTALCOST', :OLD.GRAND_FRINGE_TOTALCOST, :NEW.GRAND_FRINGE_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_DISC_COST,' ') != NVL(:NEW.GRAND_DISC_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_COST', :OLD.GRAND_DISC_COST, :NEW.GRAND_DISC_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_DISC_TOTALCOST,' ') != NVL(:NEW.GRAND_DISC_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_TOTALCOST', :OLD.GRAND_DISC_TOTALCOST, :NEW.GRAND_DISC_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_IND_COST,' ') != NVL(:NEW.GRAND_IND_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_COST', :OLD.GRAND_IND_COST, :NEW.GRAND_IND_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_IND_TOTALCOST,' ') != NVL(:NEW.GRAND_IND_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_TOTALCOST', :OLD.GRAND_IND_TOTALCOST, :NEW.GRAND_IND_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_SPONSOR,' ') != NVL(:NEW.GRAND_RES_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_SPONSOR', :OLD.GRAND_RES_SPONSOR, :NEW.GRAND_RES_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_VARIANCE,' ') != NVL(:NEW.GRAND_RES_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_VARIANCE', :OLD.GRAND_RES_VARIANCE, :NEW.GRAND_RES_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_SPONSOR,' ') != NVL(:NEW.GRAND_SOC_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_SPONSOR', :OLD.GRAND_SOC_SPONSOR, :NEW.GRAND_SOC_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_VARIANCE,' ') != NVL(:NEW.GRAND_SOC_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_VARIANCE', :OLD.GRAND_SOC_VARIANCE, :NEW.GRAND_SOC_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_SPONSOR,' ') != NVL(:NEW.GRAND_TOTAL_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_SPONSOR', :OLD.GRAND_TOTAL_SPONSOR, :NEW.GRAND_TOTAL_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_VARIANCE,' ') != NVL(:NEW.GRAND_TOTAL_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_VARIANCE', :OLD.GRAND_TOTAL_VARIANCE, :NEW.GRAND_TOTAL_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SP_OVERHEAD,0) != NVL(:NEW.BGTCAL_SP_OVERHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD', :OLD.BGTCAL_SP_OVERHEAD, :NEW.BGTCAL_SP_OVERHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SP_OVERHEAD_FLAG,0) != NVL(:NEW.BGTCAL_SP_OVERHEAD_FLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD_FLAG', :OLD.BGTCAL_SP_OVERHEAD_FLAG, :NEW.BGTCAL_SP_OVERHEAD_FLAG,NULL,NULL);
      END IF;
    END IF;
	
    EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTCAL_AU2 ');

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,22,'22_SCH_BGTCAL_AU2.sql',sysdate,'9.0.1 Build#638');

commit;