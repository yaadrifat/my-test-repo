--When SCH_EVENTDOC table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTDOC_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENTDOC 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:OLD.FK_EVENT);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTDOC
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTDOC',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTDOC,0) != NVL(:NEW.PK_EVENTDOC,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PK_EVENTDOC',:OLD.PK_EVENTDOC,:NEW.PK_EVENTDOC,NULL,NULL);
    END IF;
    IF NVL(:OLD.PK_DOCS,0) != NVL(:NEW.PK_DOCS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PK_DOCS',:OLD.PK_DOCS,:NEW.PK_DOCS,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,0) != NVL(:NEW.PROPAGATE_FROM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTDOC_AU1 ');
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,50,'50_SCH_EVENTDOC_AU1.sql',sysdate,'9.0.1 Build#638');

commit;
