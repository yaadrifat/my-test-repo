CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_BI0" BEFORE INSERT ON EVENT_ASSOC
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 IF :NEW.FK_VISIT IS NOT NULL AND :NEW.FK_VISIT > 0 THEN
    :NEW.COVERAGE_NOTES := NULL;
 END IF;
 BEGIN
	v_new_creator := :NEW.creator;
	SELECT TO_CHAR(pk_user) ||', ' || trim(usr_lastname) ||', ' ||trim(usr_firstname)
	INTO usr FROM er_user WHERE pk_user = v_new_creator ;
	EXCEPTION WHEN NO_DATA_FOUND THEN
	USR := 'New User' ;
	END ;
	SELECT TRUNC(seq_rid.NEXTVAL)
	INTO erid FROM dual;
	:NEW.rid := erid ;
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
-- Added by Ganapathy on 06/23/05 for Audit inserting
--modified: JM
  insert_data:=:NEW.DURATION_UNIT||'|'|| :NEW.FK_VISIT||'|'|| :NEW.EVENT_ID||'|'||
	:NEW.CHAIN_ID||'|'||:NEW.EVENT_TYPE||'|'||:NEW.NAME||'|'||:NEW.NOTES||'|'||
	:NEW.COST||'|'||:NEW.COST_DESCRIPTION||'|'|| :NEW.DURATION||'|'|| :NEW.USER_ID||'|'||
	:NEW.LINKED_URI||'|'||:NEW.FUZZY_PERIOD||'|'||:NEW.MSG_TO||'|'||
	:NEW.DESCRIPTION||'|'|| :NEW.DISPLACEMENT||'|'|| :NEW.ORG_ID||'|'||:NEW.EVENT_MSG||'|'||
	:NEW.EVENT_RES||'|'|| :NEW.EVENT_FLAG||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.PAT_DAYSBEFORE||'|'|| :NEW.PAT_DAYSAFTER||'|'|| :NEW.USR_DAYSBEFORE||'|'||
	:NEW.USR_DAYSAFTER||'|'||:NEW.PAT_MSGBEFORE||'|'||:NEW.PAT_MSGAFTER||'|'||
	:NEW.USR_MSGBEFORE||'|'||:NEW.USR_MSGAFTER||'|'|| TO_CHAR(:NEW.STATUS_DT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.STATUS_CHANGEBY||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
	:NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.IP_ADD||'|'||
	:NEW.ORIG_STUDY ||'|'||:NEW.ORIG_CAL ||'|'||:NEW.ORIG_EVENT ||'|'||
	:NEW.EVENT_CPTCODE||'|'||:NEW.EVENT_FUZZYAFTER||'|'||:NEW.EVENT_DURATIONAFTER||'|'||
	:NEW.EVENT_DURATIONBEFORE||'|'||:NEW.FK_CATLIB||'|'||:NEW.EVENT_CALASSOCTO||'|'||
	:NEW.EVENT_CALSCHDATE ||'|'|| :NEW.EVENT_CATEGORY ||'|'|| :NEW.EVENT_LIBRARY_TYPE ||'|'|| :NEW.EVENT_LINE_CATEGORY ||'|'||
	:NEW.SERVICE_SITE_ID ||'|'|| :NEW.FACILITY_ID ||'|'|| :NEW.FK_CODELST_COVERTYPE||'|'|| :NEW.COVERAGE_NOTES||'|'||
	:NEW.FK_CODELST_CALSTAT; 

	audit_trail.record_transaction(raid, 'EVENT_ASSOC', erid, 'I', USR );
	INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,184,2,'02_event_assoc_bi0.sql',sysdate,'9.0.1 Build#638');

commit;
