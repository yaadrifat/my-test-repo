--STARTS RENAMING COLUMN last_modified_on TO LAST_MODIFIED_DATE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_FUNDING_SCHEDULE' AND COLUMN_NAME = 'LAST_MODIFIED_ON';
IF (v_column_exists>  0) THEN
    EXECUTE IMMEDIATE 'alter table CB_FUNDING_SCHEDULE rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE';
END IF;
END;
/
--END



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,166,4,'04_MISC_ALTER.sql',sysdate,'9.0.0 Build#623');

commit;