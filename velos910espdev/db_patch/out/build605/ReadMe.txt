/* This readMe is specific to Velos eResearch version 9.0 build #605 */
=========================================================================================================================
eResearch 

# While re-organizing the existing skins, following files 
1. ie_1024.css

have to be removed from following locations:
1] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\bethematch
2] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\default
3] \server\eresearch\deploy\velos.ear\velos.war\jsp\styles\redmond

=========================================================================================================================
For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. These properties files and their custom properties files can now be dynamically loaded to eResearch.
Once the changes in these files are saved in ERES_HOME, the admin group user can go to Manage Users and 
click "Refresh tabs/menus" and reload the page. The changes should appear.

=========================================================================================================================
Following Files have been Modified for localization:

1	addFieldToFormSubmit.jsp
2	additemstudy.jsp
3	budget.jsp
4	budgetapndx.jsp
5	budgetsections.jsp
6	calDoesNotExist.jsp
7	editBudgetCalAttributes.jsp
8	editPersonnelCost.jsp
9	editRepeatLineItems.jsp
10	formfldbrowser.jsp
11	getlookup.jsp
12	horizontalRuleType.jsp
13	importStudyForms.jsp
14	labelBundle.properties
15	LC.java
16	LC.jsp
17	MC.java
18	MC.jsp
19	messageBundle.properties
20	patientbudget.jsp
21	patientbudgetView.jsp
22	prepareSpecimen.jsp
23	reportcentral.jsp
24	specimenbrowser.jsp
25	studybudget.jsp
26	studypatients.jsp
27	studyprotocols.jsp
========================================================================================================================
Garuda :
1. hibernate.cfg.xml database configurations are no longer needed. Please leave the file as it is.

========================================================================================================================
2. After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar", where the application is deployed.
	
	1.Remove "hibernate-validator-3.1.0.GA.jar" and
	2.Keep "hibernate-validator.jar".

========================================================================================================================
3. Steps to integrate Workflow module:
	Changes to file gems_configuration.properties:
	a1) Copy the gems_configuration.properties file in folder server/eresearch/conf.
	a2) In file gems_configuration.properties, replace the [IP_ADDRESS] with the server's IP address. 
	Please do not change the specified port number.
	--------------------------------------------------------------------------------------------------------------------
	Changes to file eres-ds.xml:
	b1) Add following XML snippet at the end in server/eresearch/deploy/eres-ds.xml (Just before the tag </datasources>)
	<xa-datasource> 
		<jndi-name>gems</jndi-name> 
		<track-connection-by-tx>true</track-connection-by-tx> 
		<isSameRM-override-value>false</isSameRM-override-value> 
		<xa-datasource-class>oracle.jdbc.xa.client.OracleXADataSource</xa-datasource-class> 
		<xa-datasource-property name="URL">jdbc:oracle:thin:@[IP_ADDRESS]:[PORT]:[SID]</xa-datasource-property> 
		<xa-datasource-property name="User">eres</xa-datasource-property> 
		<xa-datasource-property name="Password">[PASSWORD]</xa-datasource-property> 
		<valid-connection-check-class-name>org.jboss.resource.adapter.jdbc.vendor.OracleValidconnectionChecker</valid-connection-check-class-name>
		<min-pool-size>5</min-pool-size>
		<max-pool-size>50</max-pool-size>
		<idle-timeout-minutes>5</idle-timeout-minutes>
		<blocking-timeout-millis>5000</blocking-timeout-millis>
		<query-timeout>300</query-timeout>
		<exception-sorter-class-name>org.jboss.resource.adapter.jdbc.vendor.OracleXAExceptionFormatter
			</exception-sorter-class-name> 
		<no-tx-separate-pools/> 
	</xa-datasource> 
	b2) Replace the place-holders as below:
		[IP_ADDRESS]					Database server's IP address
		[PORT]						Port specified for database connection (Usually is 1521)
		[SID]						Database SID
		[PASSWORD]					eres schema Password
	--------------------------------------------------------------------------------------------------------------------
	c1) To Test the workflow functionality,use the below script to insert records in er_order table.
	Note: Change the <PK_CORD> with existing CB_CORD.PK_CORD value.	  
	
		DECLARE
		  v_orderId number := 0;
		BEGIN
		
		  Insert into ER_ORDER (PK_ORDER,ORDER_NUMBER,ORDER_ENTITYID,ORDER_ENTITYTYP,ORDER_DATE,FK_ORDER_STATUS,FK_ORDER_TYPE)
		values (SEQ_ER_ORDER.nextval,'ORD107',<PK_CORD>,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU'),sysdate,(select pk_codelst from er_codelst where codelst_type='order_status' and codelst_subtyp='NEW'),(select pk_codelst from er_codelst where codelst_type='order_type' and codelst_subtyp='CT'));
		
		Select SEQ_ER_ORDER.currval into v_orderId from dual;
		
		
		insert into ER_ORDER_TEMP(ORDER_ID,ORDER_TYPE) values(v_orderId,'CTORDER');
		
		insert into cb_shipment(pk_shipment,fk_order_id) values(seq_cb_shipment.nextval,v_orderId);
		commit;
		
		end;
		/
========================================================================================================================
4. Following two new menus are released. These menus have be manually made visible. 
	1.References
	2.Contact Info
			
========================================================================================================================