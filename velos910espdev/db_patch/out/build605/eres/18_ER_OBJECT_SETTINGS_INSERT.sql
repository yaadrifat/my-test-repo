SET DEFINE OFF;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'con_info_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'con_info_menu', 'top_menu', 8, 
      0, 'Contact Info', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cinfo_menu for top_menu already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'staff_dir' and OBJECT_NAME = 'cinfo_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'staff_dir', 'cinfo_menu', 2, 
     0, 'Staff Directory', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting staff_dir for cinfo_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'chat_help' and OBJECT_NAME = 'cinfo_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'chat_help', 'cinfo_menu', 3, 
      0, 'Chat/Help', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting chat_help for cinfo_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'net_dir' and OBJECT_NAME = 'cinfo_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'net_dir', 'cinfo_menu', 4, 
    0, 'Network Directory', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting net_dir for cinfo_menu already exists');
  end if;
END;
/

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,18,'18_ER_OBJECT_SETTINGS_INSERT.sql',sysdate,'9.0.0 Build#605');

commit;