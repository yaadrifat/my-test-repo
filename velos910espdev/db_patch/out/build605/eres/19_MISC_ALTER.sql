
set define off;
    --STARTS ADDING COLUMN LOC_CBU_ID_PRE TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'LOC_CBU_ID_PRE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(LOC_CBU_ID_PRE VARCHAR2(15 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ERES.CBB.LOC_CBU_ID_PRE IS 'Prefix for local cbu id';

    --STARTS ADDING COLUMN LOC_CBU_ID_STR_NUM TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'LOC_CBU_ID_STR_NUM';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(LOC_CBU_ID_STR_NUM NUMBER(10))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ERES.CBB.LOC_CBU_ID_STR_NUM IS 'Local cbu id start number';
 
     --STARTS ADDING COLUMN LOC_MAT_ID_PRE TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'LOC_MAT_ID_PRE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(LOC_MAT_ID_PRE VARCHAR2(15 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ERES.CBB.LOC_MAT_ID_PRE IS 'Local Maternal id prefix';



    --STARTS ADDING COLUMN LOC_MAT_ID_STR_NUM TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'LOC_MAT_ID_STR_NUM';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(LOC_MAT_ID_STR_NUM NUMBER(10))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB.LOC_MAT_ID_STR_NUM IS 'Local maternal start number';



    --STARTS ADDING COLUMN FK_LOC_CBU_ASSGN_ID TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'FK_LOC_CBU_ASSGN_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(FK_LOC_CBU_ASSGN_ID NUMBER(10))';
  end if;
end;
/
--END--

 COMMENT ON COLUMN ERES.CBB.FK_LOC_CBU_ASSGN_ID IS 'Refrenenced from ER_CODELST';

 --STARTS ADDING COLUMN FK_LOC_MAT_ASSGN_ID TO CBB TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB'
    AND column_name = 'FK_LOC_MAT_ASSGN_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB ADD(FK_LOC_MAT_ASSGN_ID NUMBER(10))';
  end if;
end;
/
--END--

 COMMENT ON COLUMN ERES.CBB.FK_LOC_MAT_ASSGN_ID IS 'Refrenenced from ER_CODELST';


--STARTS ADDING COLUMN OTHER_BAG_TYPE TO CBB_PROCESSING_PROCEDURES_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTHER_BAG_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTHER_BAG_TYPE VARCHAR2(30 CHAR))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTHER_BAG_TYPE IS 'Stores other bag type description';


--STARTS ADDING COLUMN DELETEDFLAG TO CB_UPLOAD_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_UPLOAD_INFO'
    AND column_name = 'DELETEDFLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_UPLOAD_INFO ADD(DELETEDFLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_UPLOAD_INFO.DELETEDFLAG IS 'Stores whether column is deleted';


--STARTS ADDING COLUMN ADDITI_TYPING_FLAG TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'ADDITI_TYPING_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(ADDITI_TYPING_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.ADDITI_TYPING_FLAG IS 'Stores whether additional typing has done or not';


--STARTS ADDING COLUMN PACKAGE_SLIP_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'PACKAGE_SLIP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(PACKAGE_SLIP_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_SHIPMENT.PACKAGE_SLIP_FLAG IS 'Stores whether package slip generated or not';

--STARTS ADDING COLUMN REQ_CLIN_INFO_FLAG TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'REQ_CLIN_INFO_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER ADD(REQ_CLIN_INFO_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ER_ORDER.REQ_CLIN_INFO_FLAG IS 'To store the status whether required clinical information is entered completely';


--STARTS ADDING COLUMN CBU_AVAIL_CONFIRM_FLAG TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CBU_AVAIL_CONFIRM_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CORD ADD(CBU_AVAIL_CONFIRM_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_CORD.CBU_AVAIL_CONFIRM_FLAG IS 'To store the status whether cbu availability confirmed or not';

--STARTS ADDING COLUMN SHIPMENT_SCH_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'SHIPMENT_SCH_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD(SHIPMENT_SCH_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_SHIPMENT.SHIPMENT_SCH_FLAG IS 'To store the status whether shipment scheduled or not';


--STARTS ADDING COLUMN RESOL_ACK_FLAG TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'RESOL_ACK_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER ADD(RESOL_ACK_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ER_ORDER.RESOL_ACK_FLAG IS 'To store the status whether resolution acknowledged or not';


--STARTS ADDING COLUMN CORD_SHIPPED_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'CORD_SHIPPED_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD(CORD_SHIPPED_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_SHIPMENT.CORD_SHIPPED_FLAG IS 'To store the status whether cord shipped or not';



--STARTS ADDING COLUMN ADDITI_TYPING_FLAG TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'ADDITI_TYPING_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(ADDITI_TYPING_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.ADDITI_TYPING_FLAG IS 'Stores whether additional typing has done or not';


--STARTS ADDING COLUMN PACKAGE_SLIP_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'PACKAGE_SLIP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(PACKAGE_SLIP_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_SHIPMENT.PACKAGE_SLIP_FLAG IS 'Stores whether package slip generated or not';

--STARTS ADDING COLUMN REQ_CLIN_INFO_FLAG TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'REQ_CLIN_INFO_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER ADD(REQ_CLIN_INFO_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ER_ORDER.REQ_CLIN_INFO_FLAG IS 'To store the status whether required clinical information is entered completely';


--STARTS ADDING COLUMN CBU_AVAIL_CONFIRM_FLAG TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CBU_AVAIL_CONFIRM_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CORD ADD(CBU_AVAIL_CONFIRM_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_CORD.CBU_AVAIL_CONFIRM_FLAG IS 'To store the status whether cbu availability confirmed or not';

--STARTS ADDING COLUMN SHIPMENT_SCH_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'SHIPMENT_SCH_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD(SHIPMENT_SCH_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_SHIPMENT.SHIPMENT_SCH_FLAG IS 'To store the status whether shipment scheduled or not';


--STARTS ADDING COLUMN RESOL_ACK_FLAG TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'RESOL_ACK_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER ADD(RESOL_ACK_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN ER_ORDER.RESOL_ACK_FLAG IS 'To store the status whether resolution acknowledged or not';


--STARTS ADDING COLUMN CORD_SHIPPED_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'CORD_SHIPPED_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD(CORD_SHIPPED_FLAG VARCHAR2(1))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_SHIPMENT.CORD_SHIPPED_FLAG IS 'To store the status whether cord shipped or not';



Set define off;

--STARTS ADDING COLUMN FK_CODELST_EXTERNAL TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'FK_CODELST_EXTERNAL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(FK_CODELST_EXTERNAL NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.FK_CODELST_EXTERNAL IS 'Code for customer referenced from CodeList(ER_CODELST)';


--STARTS ADDING COLUMN FK_CODELST_EXTERNAL_NAME  TO CB_CORD TABLE--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'FK_CODELST_EXTERNAL_NAME';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(FK_CODELST_EXTERNAL_NAME  NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.FK_CODELST_EXTERNAL_NAME   IS 'Code for customer-preferred name referenced from Code List(ER_CODELST)';




DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CORD_DONOR_IDENTI_NUM';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(CORD_DONOR_IDENTI_NUM  VARCHAR(50)';
  end if;
end;
/
COMMENT ON COLUMN ERES.CB_CORD.CORD_DONOR_IDENTI_NUM   IS 'DONOR IDENTIFICATION NUMBER (DIN)';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,19,'19_MISC_ALTER.sql',sysdate,'9.0.0 Build#605');

commit;