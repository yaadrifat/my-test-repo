--trigger 1

CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTSTAT_AD0" 
AFTER DELETE
ON SCH_EVENTSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTSTAT', :old.rid, 'D', :old.last_modified_by);

  deleted_data :=
   to_char(:old.pk_eventstat) || '|' ||
   to_char(:old.eventstat_dt) || '|' ||
   :old.eventstat_notes || '|' ||
   :old.fk_event || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.eventstat) || '|' ||
   to_char(:old.eventstat_enddt);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/



--trigger 2

CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AD0" AFTER DELETE ON SCH_EVENTS1
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;--KM
begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :old.rid, 'D', :old.last_modified_by);

  deleted_data :=
   :old.event_id || '|' ||
   :old.object_id || '|' ||
   :old.visit_type_id || '|' ||
   :old.child_service_id || '|' ||
   :old.session_id || '|' ||
   :old.occurence_id || '|' ||
   :old.location_id || '|' ||
   :old.notes || '|' ||
   :old.type_id || '|' ||
   to_char(:old.roles) || '|' ||
   :old.svc_type_id || '|' ||
   to_char(:old.status) || '|' ||
   :old.service_id || '|' ||
   :old.booked_by || '|' ||
   to_char(:old.isconfirmed) || '|' ||
   :old.description || '|' ||
   to_char(:old.attended) || '|' ||
   to_char(:old.start_date_time) || '|' ||
   to_char(:old.end_date_time) || '|' ||
   to_char(:old.bookedon) || '|' ||
   to_char(:old.iswaitlisted) || '|' ||
   :old.object_name || '|' ||
   :old.location_name || '|' ||
   :old.session_name || '|' ||
   :old.user_name || '|' ||
   :old.patient_id || '|' ||
   :old.svc_type_name || '|' ||
   to_char(:old.event_num) || '|' ||
   :old.visit_type_name || '|' ||
   :old.obj_location_id || '|' ||
   to_char(:old.fk_assoc) || '|' ||
   to_char(:old.event_exeon) || '|' ||
   to_char(:old.event_exeby) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.fk_patprot) || '|' ||
   to_char(:old.actual_schdate) || '|' ||
   to_char(:old.adverse_count) || '|' ||
   to_char(:old.visit) || '|' ||
   to_char(:old.alnot_sent) || '|' ||
   to_char(:old.fk_visit) || '|' ||
   to_char(:old.fk_study) || '|' ||
   :old.event_notes || '|' ||
   to_char(:old.SERVICE_SITE_ID) ||'|'|| 
   to_char(:old.FACILITY_ID) ||'|'|| 
   to_char(:old.FK_CODELST_COVERTYPE) ||'|'|| 
   :old.reason_for_coveragechange; 

	insert into audit_delete
	(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end; 
/



--trigger 3

CREATE OR REPLACE TRIGGER "ESCH"."SCH_BUDGET_AI0" 
AFTER INSERT
ON SCH_BUDGET REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
rt_str VARCHAR2(10);
rt_count NUMBER(10);
BEGIN
SELECT COUNT(*)
INTO rt_count
FROM er_ctrltab
WHERE CTRL_KEY = 'bgt_rights';
FOR i IN 0 .. rt_count - 1
LOOP
rt_str:= rt_str || '7';
END LOOP;

	INSERT INTO SCH_BGTUSERS (
			PK_BGTUSERS,
			FK_BUDGET,
			FK_USER,
			BGTUSERS_RIGHTS,
			BGTUSERS_TYPE,
			CREATOR,
			CREATED_ON,
			IP_ADD)
	VALUES( SEQ_SCH_BGTUSERS.NEXTVAL,
			:NEW.PK_BUDGET,
			:NEW.BUDGET_CREATOR,
			rt_str,
			'I',
			:NEW.CREATOR,
			SYSDATE,
			:NEW.IP_ADD);
	IF(:NEW.BUDGET_CREATOR <> :NEW.CREATOR ) THEN
			INSERT INTO SCH_BGTUSERS (
			PK_BGTUSERS,
			FK_BUDGET,
			FK_USER,
			BGTUSERS_RIGHTS,
			BGTUSERS_TYPE,
			CREATOR,
			CREATED_ON,
			IP_ADD)
	VALUES( SEQ_SCH_BGTUSERS.NEXTVAL,
			 :NEW.PK_BUDGET,
			:NEW.CREATOR,
			rt_str,
			'I',
			:NEW.CREATOR,
			SYSDATE,
			:NEW.IP_ADD);
	END IF;
		
	IF NVL(:NEW.BUDGET_COMBFLAG, 'Z')='Y' THEN
		BEGIN
			IF (:NEW.budget_rightscope = 'S') THEN  /* add access for all users of budget's study*/
		        pkg_bgtusers.sp_addteam_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_STUDY,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
			END IF;
		END;
	END IF;
		--add bgt section
/*
if :new.budget_type = 'S' then
  -- insert dummy record in budget cal for study budget

  INSERT INTO SCH_BGTCAL(PK_BGTCAL,
  FK_BUDGET,
  BGTCAL_PROTID ,
  BGTCAL_PROTTYPE,
  BGTCAL_DELFLAG,
  CREATOR,
  IP_ADD)
  VALUES  (SEQ_SCH_BGTCAL.nextval,:new.PK_BUDGET,
  null,null,'N', :new.creator, :new.IP_ADD );
  pkg_bgt.sp_study_bgtsection (
  :new.PK_BUDGET,
  :new.creator,
  :new.IP_ADD ) ;

end if; */
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,87,4,'04_triggers.sql',sysdate,'8.9.0 Build#544');

commit;