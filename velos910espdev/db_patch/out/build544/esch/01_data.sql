--Old data patch for all combined budgets to be shared with study team

Update sch_budget set BUDGET_RIGHTSCOPE = 'S' where NVL(BUDGET_COMBFLAG,'Z') = 'Y' and NVL(BUDGET_DELFLAG, 'N') != 'Y';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,87,1,'01_data.sql',sysdate,'8.9.0 Build#544');

commit;