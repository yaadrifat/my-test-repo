set define off;

delete from er_repxsl where pk_repxsl in (108,109,110,111,112,159,160,175,176);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,1,'01_data.sql',sysdate,'9.0.0 Build#625');

commit;
