set define off;

delete from er_repxsl where pk_repxsl in (161,167,168);
commit;

-- INSERTING into ER_REPORT


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 177;
  if (v_record_exists = 0) then
   Insert into ER_REPORT 				       (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (177,'CBU Evaluation Report','CBU Evaluation Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

	Update ER_REPORT set REP_SQL = 'SELECT 
CORD.CORD_REGISTRY_ID REGID,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
CORD.REGISTRY_MATERNAL_ID MATREGID,
F_CORD_ADD_IDS(~1) ADDIDS,
F_CORD_ID_ON_BAG(~1) IDONBAG,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ELIGBLE_HISTORY(~1) ELIGHSTRY,
F_CORD_LICENCE_HISTORY(~1) LICHSTRY,
F_CORD_FINAL_REVIEW(~1) FINALREVIEW
FROM
CB_CORD CORD
LEFT OUTER JOIN
ER_SITE SITE ON(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE CORD.PK_CORD=~1' where pk_report = 177;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'SELECT 
CORD.CORD_REGISTRY_ID REGID,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
CORD.REGISTRY_MATERNAL_ID MATREGID,
F_CORD_ADD_IDS(~1) ADDIDS,
F_CORD_ID_ON_BAG(~1) IDONBAG,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ELIGBLE_HISTORY(~1) ELIGHSTRY,
F_CORD_LICENCE_HISTORY(~1) LICHSTRY,
F_CORD_FINAL_REVIEW(~1) FINALREVIEW
FROM
CB_CORD CORD
LEFT OUTER JOIN
ER_SITE SITE ON(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE CORD.PK_CORD=~1' where pk_report = 177;
	COMMIT;

  end if;
end;
/





	Update ER_REPORT set REP_SQL = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM
 REP_CBU_DETAILS
WHERE
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM
 REP_CBU_DETAILS
WHERE
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes,cbbid, cbb_id, storage_loc, cbu_collection_site,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, (select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,
eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes,cbbid, cbb_id, storage_loc, cbu_collection_site,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, (select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,
eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
	COMMIT;



Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes, 
(select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) proinfo_attachments,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;




	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes, 
(select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, lab_sum_notes_visible_tc,(select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) proinfo_attachments,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,9,'09_ER_REPORT.sql',sysdate,'9.0.0 Build#625');

commit;
