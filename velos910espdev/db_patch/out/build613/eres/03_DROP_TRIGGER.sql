set define off;

DECLARE
  v_table_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_table_exists
	FROM USER_TRIGGERS 
	WHERE TRIGGER_NAME='ER_CB_CORD_AU0';

	if (v_table_exists != 0) then
		execute immediate 'drop trigger  ER_CB_CORD_AU0 ';
		dbms_output.put_line('Trigger droped');
	else
		dbms_output.put_line('Trigger not exits');
	end if;
end;
/

DECLARE
  v_table_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_table_exists
	FROM USER_TRIGGERS 
	WHERE TRIGGER_NAME='ER_CB_CORD_BI0';

	if (v_table_exists != 0) then
		execute immediate 'drop trigger  ER_CB_CORD_BI0 ';
		dbms_output.put_line('Trigger droped');
	else
		dbms_output.put_line('Trigger not exits');
	end if;
end;
/

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,156,3,'03_DROP_TRIGGER.sql',sysdate,'9.0.0 Build#613');

commit;