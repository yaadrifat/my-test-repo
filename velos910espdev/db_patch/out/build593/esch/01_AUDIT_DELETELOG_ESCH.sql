set define off;

		----------------------Scripts for create AUDIT_DELETELOG Table---------------------------------
		CREATE TABLE ESCH.AUDIT_DELETELOG 
		(
		  PK_APP_DL 	NUMBER PRIMARY KEY
		, TABLE_NAME 	VARCHAR2(30) 
		, TABLE_PK 		NUMBER
		, TABLE_RID 	NUMBER
		, DELETED_BY 	NUMBER
		, DELETED_ON 	DATE 
		, APP_MODULE 	VARCHAR2(50) 
		, IP_ADDRESS 	VARCHAR2(50) 
		, REASON_FOR_DELETION VARCHAR2(4000) 
		);
		
		----------------------Comment on AUDIT_DELETELOG Table's column---------------------------------
		COMMENT ON TABLE  ESCH.AUDIT_DELETELOG IS 'Track all deleted data from all schemas in this table';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.PK_APP_DL IS 'Primary key';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.TABLE_NAME IS 'Name of the database table';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.TABLE_PK IS 'The DELETED ROW''s Primary Key';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.TABLE_RID IS 'The DELETED ROW''s RID';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.DELETED_BY IS 'The PK of the er_user, user who deleted the row';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.DELETED_ON IS 'Stores the date on which this row was created';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.APP_MODULE IS 'Stores the Module name';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.IP_ADDRESS IS 'Stores the IP address of user';
		COMMENT ON COLUMN ESCH.AUDIT_DELETELOG.REASON_FOR_DELETION IS 'Reason why the row was deleted';
		
		----------------------AUDIT_DELETELOG Table Sequence---------------------------------
		CREATE SEQUENCE "ESCH"."SEQ_AUDIT_DELETELOG" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE ORDER NOCYCLE;

		commit;
		
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,136,1,'01_AUDIT_DELETELOG_ESCH.sql',sysdate,'9.0.0 Build#593');

commit;
