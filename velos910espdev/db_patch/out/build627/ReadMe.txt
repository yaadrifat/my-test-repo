/* This readMe is specific to Velos eResearch version 9.0 build #627 */

=====================================================================================================================================
Garuda :
DB Patch :- 
The following DB patches are specific to Garuda.
	1.13_ER_CODELST_UPDATE.sql
	2.14_ER_CODELST_INSERT.sql
	   
=====================================================================================================================================
eResearch: Enhancement INF-22327 is released by Samapada for changes at the top panel and in menus and to make the colors and styles
consistent for skins Redmond and Velos Default.
Please Refer updated UI Framework Standards and Guidelines document released in this Build. . Also, Please refer the UI feedback2.docx
document released with this build for the UI issues we have resolved at our end.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. labelBundle.properties
2. LC.java
3. MC.java
4. messageBundle.properties
5. patientschedule.jsp
6. reportsinstudy.jsp
=====================================================================================================================================
