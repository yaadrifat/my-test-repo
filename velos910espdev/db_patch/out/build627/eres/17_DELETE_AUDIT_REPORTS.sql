set define off;

DELETE FROM "ERES"."ER_REPXSL" WHERE FK_REPORT IN(175,176);
DELETE FROM "ERES"."ER_REPORT" WHERE PK_REPORT IN(175,176);

COMMIT;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,17,'17_DELETE_AUDIT_REPORTS.sql',sysdate,'9.0.0 Build#627');

commit;
