CREATE OR REPLACE FORCE VIEW "ERES"."REP_ENT_STUS_RSN_AUDT_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "COLUMN_NAME", "COLUMN_DISPLAY_NAME", "OLD_VALUE", "NEW_VALUE", "REASON", "REMARKS", "FK_ROW_ID")
AS
  SELECT reson.CORD_REGISTRY_ID,
    reson.Cord_Local_Cbu_Id,
    reson.Registry_Maternal_Id,
    reson.Registry_Maternal_Id,
    reson.Created_On ,
    reson.Creator,
    reson.Last_Modified_By,
    reson.Last_Modified_Date,
    acm.column_name,
    acm.column_display_name,
    acm.old_value,
    acm.new_value,
    f_codelst_desc(acm.FK_CODELST_REASON) AS REASON,
    acm.REMARKS,
    acm.fk_row_id
  FROM REP_ENT_STUS_RSN_VIEW reson,
    AUDIT_COLUMN_MODULE acm
  WHERE reson.pk_row_id = acm.fk_row_id;
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,4,'04_REP_ENT_STUS_RSN_AUDT_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;