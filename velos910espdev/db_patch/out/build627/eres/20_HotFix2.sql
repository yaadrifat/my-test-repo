--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='ORDER_TYPE';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25'),'PRE','ER_ORDER','ORDER_TYPE','(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' AND CODELST_SUBTYP=''OR'')','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--




create or replace
TRIGGER "ERES".ER_ORDER_AU0 after update of
PK_ORDER,
ORDER_TYPE,
FK_ORDER_HEADER,
ORDER_STATUS,
RECENT_HLA_ENTERED_FLAG,
ADDITI_TYPING_FLAG,
CBU_AVAIL_CONFIRM_FLAG,
IS_CORD_AVAIL_FOR_NMDP,
CORD_AVAIL_CONFIRM_DATE,
SHIPMENT_SCH_FLAG,
CORD_SHIPPED_FLAG,
PACKAGE_SLIP_FLAG,
NMDP_SAMPLE_SHIPPED_FLAG,
COMPLETE_REQ_INFO_TASK_FLAG,
FINAL_REVIEW_TASK_FLAG,
ORDER_LAST_VIEWED_BY,
ORDER_LAST_VIEWED_DATE,
ORDER_VIEW_CONFIRM_FLAG,
ORDER_VIEW_CONFIRM_DATE,
ORDER_PRIORITY,
FK_CURRENT_HLA,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
DELETEDFLAG,
RID,
TASK_ID,
TASK_NAME,
ASSIGNED_TO,
ORDER_STATUS_DATE,
ORDER_REVIEWED_BY,
ORDER_REVIEWED_DATE,
ORDER_ASSIGNED_DATE,
FK_ORDER_RESOL_BY_CBB,
FK_ORDER_RESOL_BY_TC,
ORDER_RESOL_DATE,
ORDER_ACK_FLAG,
ORDER_ACK_DATE,
ORDER_ACK_BY,
RESULT_REC_DATE,
ACCPT_TO_CANCEL_REQ,
CANCEL_CONFORM_DATE,
CANCELED_BY,
CLINIC_INFO_CHECKLIST_STAT,
RECENT_HLA_TYPING_AVAIL,
ADDI_TEST_RESULT_AVAIL,
ORDER_SAMPLE_AT_LAB,
FK_CASE_MANAGER,
CASE_MANAGER,
TRANS_CENTER_ID,
TRANS_CENTER_NAME,
SEC_TRANS_CENTER_NAME,
ORDER_PHYSICIAN,
SEC_TRANS_CENTER_ID,
CM_MAIL_ID,
CM_CONTACT_NO,
REQ_CLIN_INFO_FLAG,
RESOL_ACK_FLAG,
FK_SAMPLE_TYPE_AVAIL,
FK_ALIQUOTS_TYPE  ON ER_ORDER   for each row
declare
  raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
  NEW_FK_ORDER_RESOL_BY_CBB                    VARCHAR2(200);
  OLD_FK_ORDER_RESOL_BY_CBB                     VARCHAR2(200);
  NEW_FK_ORDER_RESOL_BY_TC                    VARCHAR2(200);
  OLD_FK_ORDER_RESOL_BY_TC                     VARCHAR2(200);
  NEW_FK_SAMPLE_TYPE_AVAIL                    VARCHAR2(200);
  OLD_FK_SAMPLE_TYPE_AVAIL                     VARCHAR2(200);
  NEW_FK_ALIQUOTS_TYPE                    VARCHAR2(200);
  OLD_FK_ALIQUOTS_TYPE                     VARCHAR2(200);
  NEW_ORDER_LAST_VIEWED_BY                    VARCHAR2(200);
  OLD_ORDER_LAST_VIEWED_BY                     VARCHAR2(200);
  NEW_ORDER_PRIORITY                    VARCHAR2(200);
  OLD_ORDER_PRIORITY                     VARCHAR2(200);
  NEW_FK_RESOLUTION_CODE                    VARCHAR2(200);
  OLD_FK_RESOLUTION_CODE                     VARCHAR2(200);
begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:NEW.last_modified_by);
  IF NVL(:OLD.FK_ORDER_RESOL_BY_CBB,0) !=     NVL(:NEW.FK_ORDER_RESOL_BY_CBB,0) THEN
  BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ORDER_RESOL_BY_CBB)		INTO OLD_FK_ORDER_RESOL_BY_CBB  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ORDER_RESOL_BY_CBB := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_RESOL_BY_CBB)		INTO NEW_FK_ORDER_RESOL_BY_CBB  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_RESOL_BY_CBB := '';
   END;
   END IF;
   IF NVL(:OLD.FK_ORDER_RESOL_BY_TC,0) !=     NVL(:NEW.FK_ORDER_RESOL_BY_TC,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ORDER_RESOL_BY_TC)		INTO OLD_FK_ORDER_RESOL_BY_TC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ORDER_RESOL_BY_TC := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_RESOL_BY_TC)		INTO NEW_FK_ORDER_RESOL_BY_TC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_RESOL_BY_TC := '';
   END;
   END IF;
   IF NVL(:OLD.FK_SAMPLE_TYPE_AVAIL,0) !=     NVL(:NEW.FK_SAMPLE_TYPE_AVAIL,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SAMPLE_TYPE_AVAIL)		INTO OLD_FK_SAMPLE_TYPE_AVAIL  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SAMPLE_TYPE_AVAIL := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SAMPLE_TYPE_AVAIL)		INTO NEW_FK_SAMPLE_TYPE_AVAIL  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SAMPLE_TYPE_AVAIL := '';
   END;
   END IF;
   IF NVL(:OLD.FK_ALIQUOTS_TYPE,0) !=     NVL(:NEW.FK_ALIQUOTS_TYPE,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ALIQUOTS_TYPE)		INTO OLD_FK_ALIQUOTS_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ALIQUOTS_TYPE := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ALIQUOTS_TYPE)		INTO NEW_FK_ALIQUOTS_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ALIQUOTS_TYPE := '';
   END;
   END IF;
   IF NVL(:OLD.ORDER_LAST_VIEWED_BY,0) !=     NVL(:NEW.ORDER_LAST_VIEWED_BY,0) THEN
 BEGIN
   		SELECT getuser(:OLD.ORDER_LAST_VIEWED_BY)		INTO OLD_ORDER_LAST_VIEWED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_ORDER_LAST_VIEWED_BY := '';
   END;
   BEGIN
   		SELECT getuser(:new.ORDER_LAST_VIEWED_BY)		INTO NEW_ORDER_LAST_VIEWED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_LAST_VIEWED_BY := '';
   END;
   END IF;
   IF NVL(:OLD.ORDER_PRIORITY,0) !=     NVL(:NEW.ORDER_PRIORITY,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.ORDER_PRIORITY)		INTO OLD_ORDER_PRIORITY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_ORDER_PRIORITY := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.ORDER_PRIORITY)		INTO NEW_ORDER_PRIORITY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_PRIORITY := '';
   END;
   END IF;
   IF NVL(:OLD.FK_RESOLUTION_CODE,0) !=     NVL(:NEW.FK_RESOLUTION_CODE,0) THEN
 BEGIN
   		SELECT F_GET_CBU_STATUS(:OLD.FK_RESOLUTION_CODE)		INTO OLD_FK_RESOLUTION_CODE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_RESOLUTION_CODE := '';
   END;
   BEGIN
   		SELECT F_GET_CBU_STATUS(:new.FK_RESOLUTION_CODE)		INTO NEW_FK_RESOLUTION_CODE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_RESOLUTION_CODE := '';
   END;
   END IF;
  audit_trail.record_transaction(raid, 'ER_ORDER', :old.RID, 'U', usr);

  if nvl(:old.PK_ORDER,0) != NVL(:new.PK_ORDER,0) then
     audit_trail.column_update(raid, 'PK_ORDER',:old.PK_ORDER, :new.PK_ORDER);
  end if;
  if nvl(:old.ORDER_TYPE,0) != NVL(:new.ORDER_TYPE,0) then
     audit_trail.column_update(raid, 'ORDER_TYPE',:old.ORDER_TYPE, :new.ORDER_TYPE);
  end if;
 if nvl(:old.FK_ORDER_HEADER,0) != NVL(:new.FK_ORDER_HEADER,0) then
     audit_trail.column_update(raid, 'FK_ORDER_HEADER',:old.FK_ORDER_HEADER, :new.FK_ORDER_HEADER);
  end if;
  if nvl(:old.ORDER_STATUS,0) != NVL(:new.ORDER_STATUS,0) then
     audit_trail.column_update(raid, 'ORDER_STATUS',:old.ORDER_STATUS, :new.ORDER_STATUS);
  end if;
  if nvl(:old.RECENT_HLA_ENTERED_FLAG,' ') !=  NVL(:new.RECENT_HLA_ENTERED_FLAG,' ') then
     audit_trail.column_update(raid, 'RECENT_HLA_ENTERED_FLAG',:old.RECENT_HLA_ENTERED_FLAG, :new.RECENT_HLA_ENTERED_FLAG);
  end if;
  if nvl(:old.ADDITI_TYPING_FLAG,' ') !=  NVL(:new.ADDITI_TYPING_FLAG,' ') then
     audit_trail.column_update(raid, 'ADDITI_TYPING_FLAG',:old.ADDITI_TYPING_FLAG, :new.ADDITI_TYPING_FLAG);
  end if;
  if nvl(:old.CBU_AVAIL_CONFIRM_FLAG,' ') !=  NVL(:new.CBU_AVAIL_CONFIRM_FLAG,' ') then
     audit_trail.column_update(raid, 'CBU_AVAIL_CONFIRM_FLAG',:old.CBU_AVAIL_CONFIRM_FLAG, :new.CBU_AVAIL_CONFIRM_FLAG);
  end if;
  if nvl(:old.IS_CORD_AVAIL_FOR_NMDP,' ') !=  NVL(:new.IS_CORD_AVAIL_FOR_NMDP,' ') then
     audit_trail.column_update(raid, 'IS_CORD_AVAIL_FOR_NMDP',:old.IS_CORD_AVAIL_FOR_NMDP, :new.IS_CORD_AVAIL_FOR_NMDP);
  end if;
  if nvl(:old.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=  NVL(:new.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update(raid, 'CORD_AVAIL_CONFIRM_DATE',
       to_char(:old.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.SHIPMENT_SCH_FLAG,' ') !=  NVL(:new.SHIPMENT_SCH_FLAG,' ') then
     audit_trail.column_update(raid, 'SHIPMENT_SCH_FLAG',:old.SHIPMENT_SCH_FLAG, :new.SHIPMENT_SCH_FLAG);
  end if;
  if nvl(:old.CORD_SHIPPED_FLAG,' ') !=  NVL(:new.CORD_SHIPPED_FLAG,' ') then
     audit_trail.column_update(raid, 'CORD_SHIPPED_FLAG',:old.CORD_SHIPPED_FLAG, :new.CORD_SHIPPED_FLAG);
  end if;
  if nvl(:old.PACKAGE_SLIP_FLAG,' ') !=  NVL(:new.PACKAGE_SLIP_FLAG,' ') then
     audit_trail.column_update(raid, 'PACKAGE_SLIP_FLAG',:old.PACKAGE_SLIP_FLAG, :new.PACKAGE_SLIP_FLAG);
  end if;
  if nvl(:old.NMDP_SAMPLE_SHIPPED_FLAG,' ') !=  NVL(:new.NMDP_SAMPLE_SHIPPED_FLAG,' ') then
     audit_trail.column_update(raid, 'NMDP_SAMPLE_SHIPPED_FLAG',:old.NMDP_SAMPLE_SHIPPED_FLAG, :new.NMDP_SAMPLE_SHIPPED_FLAG);
  end if;
  if nvl(:old.COMPLETE_REQ_INFO_TASK_FLAG,' ') !=  NVL(:new.COMPLETE_REQ_INFO_TASK_FLAG,' ') then
     audit_trail.column_update(raid, 'COMPLETE_REQ_INFO_TASK_FLAG',:old.COMPLETE_REQ_INFO_TASK_FLAG, :new.COMPLETE_REQ_INFO_TASK_FLAG);
  end if;
  if nvl(:old.FINAL_REVIEW_TASK_FLAG,' ') !=  NVL(:new.FINAL_REVIEW_TASK_FLAG,' ') then
     audit_trail.column_update(raid, 'FINAL_REVIEW_TASK_FLAG',:old.FINAL_REVIEW_TASK_FLAG, :new.FINAL_REVIEW_TASK_FLAG);
  end if;
  IF nvl(:OLD.ORDER_LAST_VIEWED_BY,0) !=  NVL(:NEW.ORDER_LAST_VIEWED_BY,0) THEN
     audit_trail.column_update(raid, 'ORDER_LAST_VIEWED_BY',OLD_ORDER_LAST_VIEWED_BY, NEW_ORDER_LAST_VIEWED_BY);
  end if;
  if nvl(:old.ORDER_LAST_VIEWED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_LAST_VIEWED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_LAST_VIEWED_DATE',
       to_char(:old.ORDER_LAST_VIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_LAST_VIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_VIEW_CONFIRM_FLAG,' ') !=  NVL(:new.ORDER_VIEW_CONFIRM_FLAG,' ') then
     audit_trail.column_update(raid, 'ORDER_VIEW_CONFIRM_FLAG',:old.ORDER_VIEW_CONFIRM_FLAG, :new.ORDER_VIEW_CONFIRM_FLAG);
  end if;
  if nvl(:old.ORDER_VIEW_CONFIRM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_VIEW_CONFIRM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_VIEW_CONFIRM_DATE',
       to_char(:old.ORDER_VIEW_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_VIEW_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  IF nvl(:OLD.ORDER_PRIORITY,0) != NVL(:NEW.ORDER_PRIORITY,0) THEN
     audit_trail.column_update(raid, 'ORDER_PRIORITY',OLD_ORDER_PRIORITY, NEW_ORDER_PRIORITY);
  end if;
  if nvl(:old.FK_CURRENT_HLA,0) != NVL(:new.FK_CURRENT_HLA,0) then
     audit_trail.column_update(raid, 'FK_CURRENT_HLA',:old.FK_CURRENT_HLA, :new.FK_CURRENT_HLA);
  end if;
   if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
  end if;
   if nvl(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;
   if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.ip_add,' ') !=  NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.DELETEDFLAG,0) !=
     NVL(:new.DELETEDFLAG,0) then
     audit_trail.column_update
       (raid, 'DELETEDFLAG',
       :old.DELETEDFLAG, :new.DELETEDFLAG);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.TASK_ID,0) !=
     NVL(:new.TASK_ID,0) then
     audit_trail.column_update
       (raid, 'TASK_ID',
       :old.TASK_ID, :new.TASK_ID);
  end if;
    if nvl(:old.TASK_NAME,' ') !=
     NVL(:new.TASK_NAME,' ') then
     audit_trail.column_update
       (raid, 'TASK_NAME',
       :old.TASK_NAME, :new.TASK_NAME);
  end if;
  if nvl(:old.ASSIGNED_TO,0) !=
     NVL(:new.ASSIGNED_TO,0) then
     audit_trail.column_update
       (raid, 'ASSIGNED_TO',
       :old.ASSIGNED_TO, :new.ASSIGNED_TO);
  end if;
  if nvl(:old.ORDER_STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_STATUS_DATE',
       to_char(:old.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_REVIEWED_BY,0) !=
     NVL(:new.ORDER_REVIEWED_BY,0) then
     audit_trail.column_update
       (raid, 'ORDER_REVIEWED_BY',
       :old.ORDER_REVIEWED_BY, :new.ORDER_REVIEWED_BY);
  end if;
  if nvl(:old.ORDER_REVIEWED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_REVIEWED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_REVIEWED_DATE',
       to_char(:old.ORDER_REVIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_REVIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.ORDER_ASSIGNED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_ASSIGNED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_ASSIGNED_DATE',
       to_char(:old.ORDER_ASSIGNED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_ASSIGNED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.FK_ORDER_RESOL_BY_CBB,0) !=  NVL(:new.FK_ORDER_RESOL_BY_CBB,0) then
     audit_trail.column_update
       (raid, 'FK_ORDER_RESOL_BY_CBB',OLD_FK_ORDER_RESOL_BY_CBB, NEW_FK_ORDER_RESOL_BY_CBB);
  end if;
   IF nvl(:OLD.FK_ORDER_RESOL_BY_TC,0) != NVL(:NEW.FK_ORDER_RESOL_BY_TC,0) THEN
     audit_trail.column_update(raid, 'FK_ORDER_RESOL_BY_TC',OLD_FK_ORDER_RESOL_BY_TC, NEW_FK_ORDER_RESOL_BY_TC);
  end if;
    if nvl(:old.ORDER_RESOL_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_RESOL_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_RESOL_DATE',
       to_char(:old.ORDER_RESOL_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_RESOL_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_ACK_FLAG,' ') !=
     NVL(:new.ORDER_ACK_FLAG,' ') then
     audit_trail.column_update
       (raid, 'ORDER_ACK_FLAG',
       :old.ORDER_ACK_FLAG, :new.ORDER_ACK_FLAG);
  end if;
   if nvl(:old.ORDER_ACK_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ORDER_ACK_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'ORDER_ACK_DATE',
       to_char(:old.ORDER_ACK_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_ACK_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.ORDER_ACK_BY,0) !=
     NVL(:new.ORDER_ACK_BY,0) then
     audit_trail.column_update
       (raid, 'ORDER_ACK_BY',
       :old.ORDER_ACK_BY, :new.ORDER_ACK_BY);
  end if;
   if nvl(:old.RESULT_REC_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.RESULT_REC_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'RESULT_REC_DATE',
       to_char(:old.RESULT_REC_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.RESULT_REC_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ACCPT_TO_CANCEL_REQ,' ') !=
     NVL(:new.ACCPT_TO_CANCEL_REQ,' ') then
     audit_trail.column_update
       (raid, 'ACCPT_TO_CANCEL_REQ',
       :old.ACCPT_TO_CANCEL_REQ, :new.ACCPT_TO_CANCEL_REQ);
  end if;
   if nvl(:old.CANCEL_CONFORM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.CANCEL_CONFORM_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'CANCEL_CONFORM_DATE',
       to_char(:old.CANCEL_CONFORM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CANCEL_CONFORM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.CANCELED_BY,0) !=
     NVL(:new.CANCELED_BY,0) then
     audit_trail.column_update
       (raid, 'CANCELED_BY',
       :old.CANCELED_BY, :new.CANCELED_BY);
  end if;
   if nvl(:old.CLINIC_INFO_CHECKLIST_STAT,' ') !=
     NVL(:new.CLINIC_INFO_CHECKLIST_STAT,' ') then
     audit_trail.column_update
       (raid, 'CLINIC_INFO_CHECKLIST_STAT',
       :old.CLINIC_INFO_CHECKLIST_STAT, :new.CLINIC_INFO_CHECKLIST_STAT);
  end if;
  if nvl(:old.RECENT_HLA_TYPING_AVAIL,' ') !=
     NVL(:new.RECENT_HLA_TYPING_AVAIL,' ') then
     audit_trail.column_update
       (raid, 'RECENT_HLA_TYPING_AVAIL',
       :old.RECENT_HLA_TYPING_AVAIL, :new.RECENT_HLA_TYPING_AVAIL);
  end if;
  if nvl(:old.ADDI_TEST_RESULT_AVAIL,' ') !=
     NVL(:new.ADDI_TEST_RESULT_AVAIL,' ') then
     audit_trail.column_update
       (raid, 'ADDI_TEST_RESULT_AVAIL',
       :old.ADDI_TEST_RESULT_AVAIL, :new.ADDI_TEST_RESULT_AVAIL);
  end if;
  if nvl(:old.ORDER_SAMPLE_AT_LAB,' ') !=
     NVL(:new.ORDER_SAMPLE_AT_LAB,' ') then
     audit_trail.column_update
       (raid, 'ORDER_SAMPLE_AT_LAB',
       :old.ORDER_SAMPLE_AT_LAB, :new.ORDER_SAMPLE_AT_LAB);
  end if;
   if nvl(:old.FK_CASE_MANAGER,0) !=
     NVL(:new.FK_CASE_MANAGER,0) then
     audit_trail.column_update
       (raid, 'FK_CASE_MANAGER',
       :old.FK_CASE_MANAGER, :new.FK_CASE_MANAGER);
  end if;
   if nvl(:old.CASE_MANAGER,' ') !=
     NVL(:new.CASE_MANAGER,' ') then
     audit_trail.column_update
       (raid, 'CASE_MANAGER',
       :old.CASE_MANAGER, :new.CASE_MANAGER);
  end if;
   if nvl(:old.TRANS_CENTER_ID,0) !=
     NVL(:new.TRANS_CENTER_ID,0) then
     audit_trail.column_update
       (raid, 'TRANS_CENTER_ID',
       :old.TRANS_CENTER_ID, :new.TRANS_CENTER_ID);
  end if;
    if nvl(:old.TRANS_CENTER_NAME,' ') !=
     NVL(:new.TRANS_CENTER_NAME,' ') then
     audit_trail.column_update
       (raid, 'TRANS_CENTER_NAME',
       :old.TRANS_CENTER_NAME, :new.TRANS_CENTER_NAME);
  end if;
    if nvl(:old.SEC_TRANS_CENTER_NAME,' ') !=
     NVL(:new.SEC_TRANS_CENTER_NAME,' ') then
     audit_trail.column_update
       (raid, 'SEC_TRANS_CENTER_NAME',
       :old.SEC_TRANS_CENTER_NAME, :new.SEC_TRANS_CENTER_NAME);
  end if;
 if nvl(:old.ORDER_PHYSICIAN,0) !=
     NVL(:new.ORDER_PHYSICIAN,0) then
     audit_trail.column_update
       (raid, 'ORDER_PHYSICIAN',
       :old.ORDER_PHYSICIAN, :new.ORDER_PHYSICIAN);
  end if;
   if nvl(:old.SEC_TRANS_CENTER_ID,' ') !=  NVL(:new.SEC_TRANS_CENTER_ID,' ') then
     audit_trail.column_update
       (raid, 'SEC_TRANS_CENTER_ID',
       :old.SEC_TRANS_CENTER_ID, :new.SEC_TRANS_CENTER_ID);
  end if;
if nvl(:old.CM_MAIL_ID,' ') !=
     NVL(:new.CM_MAIL_ID,' ') then
     audit_trail.column_update
       (raid, 'CM_MAIL_ID',
       :old.CM_MAIL_ID, :new.CM_MAIL_ID);
  end if;
if nvl(:old.CM_CONTACT_NO,' ') !=
     NVL(:new.CM_CONTACT_NO,' ') then
     audit_trail.column_update
       (raid, 'CM_CONTACT_NO',
       :old.CM_CONTACT_NO, :new.CM_CONTACT_NO);
  end if;
if nvl(:old.REQ_CLIN_INFO_FLAG,' ') !=
     NVL(:new.REQ_CLIN_INFO_FLAG,' ') then
     audit_trail.column_update
       (raid, 'REQ_CLIN_INFO_FLAG',
       :old.REQ_CLIN_INFO_FLAG, :new.REQ_CLIN_INFO_FLAG);
  end if;
if nvl(:old.RESOL_ACK_FLAG,' ') !=
     NVL(:new.RESOL_ACK_FLAG,' ') then
     audit_trail.column_update
       (raid, 'RESOL_ACK_FLAG',
       :old.RESOL_ACK_FLAG, :new.RESOL_ACK_FLAG);
  end if;
   if nvl(:old.FK_SAMPLE_TYPE_AVAIL,0) != NVL(:new.FK_SAMPLE_TYPE_AVAIL,0) then
     audit_trail.column_update (raid, 'FK_SAMPLE_TYPE_AVAIL',old_FK_SAMPLE_TYPE_AVAIL,new_FK_SAMPLE_TYPE_AVAIL);
  end if;
   IF nvl(:OLD.FK_ALIQUOTS_TYPE,0) != NVL(:NEW.FK_ALIQUOTS_TYPE,0) THEN
     audit_trail.column_update(raid, 'FK_ALIQUOTS_TYPE',OLD_FK_ALIQUOTS_TYPE, NEW_FK_ALIQUOTS_TYPE);
  END IF;
  IF NVL(:OLD.FK_RESOLUTION_CODE,0) !=     NVL(:NEW.FK_RESOLUTION_CODE,0) THEN
  audit_trail.column_update (raid, 'FK_RESOLUTION_CODE',OLD_FK_RESOLUTION_CODE,NEW_FK_RESOLUTION_CODE);
  END IF;
END;
/


create or replace
TRIGGER "ERES"."ER_ORDER_AU1"
AFTER UPDATE ON eres.er_order
referencing OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
  NEW_FK_ORDER_RESOL_BY_CBB                     VARCHAR2(200);
  OLD_FK_ORDER_RESOL_BY_CBB                     VARCHAR2(200);
  NEW_FK_ORDER_RESOL_BY_TC                      VARCHAR2(200);
  OLD_FK_ORDER_RESOL_BY_TC                      VARCHAR2(200);
  NEW_FK_SAMPLE_TYPE_AVAIL                      VARCHAR2(200);
  OLD_FK_SAMPLE_TYPE_AVAIL                      VARCHAR2(200);
  NEW_FK_ALIQUOTS_TYPE                          VARCHAR2(200);
  OLD_FK_ALIQUOTS_TYPE                          VARCHAR2(200);
  NEW_ORDER_LAST_VIEWED_BY                      VARCHAR2(200);
  OLD_ORDER_LAST_VIEWED_BY                      VARCHAR2(200);
  NEW_ORDER_PRIORITY                            VARCHAR2(200);
  OLD_ORDER_PRIORITY                            VARCHAR2(200);
  NEW_FK_RESOLUTION_CODE                        VARCHAR2(200);
  OLD_FK_RESOLUTION_CODE                        VARCHAR2(200);
BEGIN
IF NVL(:OLD.FK_ORDER_RESOL_BY_CBB,0) !=     NVL(:NEW.FK_ORDER_RESOL_BY_CBB,0) THEN
  BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ORDER_RESOL_BY_CBB)		INTO OLD_FK_ORDER_RESOL_BY_CBB  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ORDER_RESOL_BY_CBB := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_RESOL_BY_CBB)		INTO NEW_FK_ORDER_RESOL_BY_CBB  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_RESOL_BY_CBB := '';
   END;
   END IF;
   IF NVL(:OLD.FK_ORDER_RESOL_BY_TC,0) !=     NVL(:NEW.FK_ORDER_RESOL_BY_TC,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ORDER_RESOL_BY_TC)		INTO OLD_FK_ORDER_RESOL_BY_TC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ORDER_RESOL_BY_TC := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ORDER_RESOL_BY_TC)		INTO NEW_FK_ORDER_RESOL_BY_TC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ORDER_RESOL_BY_TC := '';
   END;
   END IF;
   IF NVL(:OLD.FK_SAMPLE_TYPE_AVAIL,0) !=     NVL(:NEW.FK_SAMPLE_TYPE_AVAIL,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SAMPLE_TYPE_AVAIL)		INTO OLD_FK_SAMPLE_TYPE_AVAIL  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SAMPLE_TYPE_AVAIL := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SAMPLE_TYPE_AVAIL)		INTO NEW_FK_SAMPLE_TYPE_AVAIL  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SAMPLE_TYPE_AVAIL := '';
   END;
   END IF;
   IF NVL(:OLD.FK_ALIQUOTS_TYPE,0) !=     NVL(:NEW.FK_ALIQUOTS_TYPE,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ALIQUOTS_TYPE)		INTO OLD_FK_ALIQUOTS_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ALIQUOTS_TYPE := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ALIQUOTS_TYPE)		INTO NEW_FK_ALIQUOTS_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ALIQUOTS_TYPE := '';
   END;
   END IF;
   IF NVL(:OLD.ORDER_LAST_VIEWED_BY,0) !=     NVL(:NEW.ORDER_LAST_VIEWED_BY,0) THEN
 BEGIN
   		SELECT getuser(:OLD.ORDER_LAST_VIEWED_BY)		INTO OLD_ORDER_LAST_VIEWED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_ORDER_LAST_VIEWED_BY := '';
   END;
   BEGIN
   		SELECT getuser(:new.ORDER_LAST_VIEWED_BY)		INTO NEW_ORDER_LAST_VIEWED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_LAST_VIEWED_BY := '';
   END;
   END IF;
   IF NVL(:OLD.ORDER_PRIORITY,0) !=     NVL(:NEW.ORDER_PRIORITY,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:OLD.ORDER_PRIORITY)		INTO OLD_ORDER_PRIORITY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_ORDER_PRIORITY := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.ORDER_PRIORITY)		INTO NEW_ORDER_PRIORITY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_PRIORITY := '';
   END;
   END IF;
   IF NVL(:OLD.FK_RESOLUTION_CODE,0) !=     NVL(:NEW.FK_RESOLUTION_CODE,0) THEN
 BEGIN
   		SELECT F_GET_CBU_STATUS(:OLD.FK_RESOLUTION_CODE)		INTO OLD_FK_RESOLUTION_CODE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_RESOLUTION_CODE := '';
   END;
   BEGIN
   		SELECT F_GET_CBU_STATUS(:new.FK_RESOLUTION_CODE)		INTO NEW_FK_RESOLUTION_CODE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_RESOLUTION_CODE := '';
   END;
   END IF;

    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;

	pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER',:OLD.rid,:OLD.PK_ORDER,'U',:NEW.creator);

IF nvl(:OLD.pk_order,0) != nvl(:NEW.pk_order,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER','PK_ORDER',:OLD.pk_order, :NEW.pk_order,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_type,0) != nvl(:NEW.order_type,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_TYPE',:OLD.order_type, :NEW.order_type,NULL,NULL);
  END IF;
 IF nvl(:OLD.fk_order_header,0) != nvl(:NEW.fk_order_header,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_ORDER_HEADER',:OLD.fk_order_header, :NEW.fk_order_header,NULL,NULL);
  END IF;
 IF nvl(:OLD.order_status,0) != nvl(:NEW.order_status,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_STATUS',:OLD.order_status, :NEW.order_status,NULL,NULL);
  END IF;
  IF nvl(:OLD.recent_hla_entered_flag,' ') !=  nvl(:NEW.recent_hla_entered_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'RECENT_HLA_ENTERED_FLAG',:OLD.recent_hla_entered_flag, :NEW.recent_hla_entered_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.additi_typing_flag,' ') !=  nvl(:NEW.additi_typing_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ADDITI_TYPING_FLAG',:OLD.additi_typing_flag, :NEW.additi_typing_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.cbu_avail_confirm_flag,' ') !=  nvl(:NEW.cbu_avail_confirm_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CBU_AVAIL_CONFIRM_FLAG',:OLD.cbu_avail_confirm_flag, :NEW.cbu_avail_confirm_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.is_cord_avail_for_nmdp,' ') !=  nvl(:NEW.is_cord_avail_for_nmdp,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'IS_CORD_AVAIL_FOR_NMDP',:OLD.is_cord_avail_for_nmdp, :NEW.is_cord_avail_for_nmdp,NULL,NULL);
  END IF;
  IF NVL(:OLD.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'ER_ORDER', 'CORD_AVAIL_CONFIRM_DATE',TO_CHAR(:OLD.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF nvl(:OLD.shipment_sch_flag,' ') !=  nvl(:NEW.shipment_sch_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'SHIPMENT_SCH_FLAG',:OLD.shipment_sch_flag, :NEW.shipment_sch_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.cord_shipped_flag,' ') !=  nvl(:NEW.cord_shipped_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CORD_SHIPPED_FLAG',:OLD.cord_shipped_flag, :NEW.cord_shipped_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.package_slip_flag,' ') !=  nvl(:NEW.package_slip_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'PACKAGE_SLIP_FLAG',:OLD.package_slip_flag, :NEW.package_slip_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.nmdp_sample_shipped_flag,' ') !=  nvl(:NEW.nmdp_sample_shipped_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'NMDP_SAMPLE_SHIPPED_FLAG',:OLD.nmdp_sample_shipped_flag, :NEW.nmdp_sample_shipped_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.complete_req_info_task_flag,' ') !=  nvl(:NEW.complete_req_info_task_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'COMPLETE_REQ_INFO_TASK_FLAG',:OLD.complete_req_info_task_flag, :NEW.complete_req_info_task_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.final_review_task_flag,' ') !=  nvl(:NEW.final_review_task_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FINAL_REVIEW_TASK_FLAG',:OLD.final_review_task_flag, :NEW.final_review_task_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_last_viewed_by,0) !=  nvl(:NEW.order_last_viewed_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_LAST_VIEWED_BY',OLD_order_last_viewed_by, NEW_order_last_viewed_by,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_last_viewed_date,to_date('31-dec-9595','DD-MON-YYYY')) !=  nvl(:NEW.order_last_viewed_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_LAST_VIEWED_DATE',
       to_char(:OLD.order_last_viewed_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_last_viewed_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_view_confirm_flag,' ') !=  nvl(:NEW.order_view_confirm_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_VIEW_CONFIRM_FLAG',:OLD.order_view_confirm_flag, :NEW.order_view_confirm_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_view_confirm_date,to_date('31-dec-9595','DD-MON-YYYY')) !=  nvl(:NEW.order_view_confirm_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_VIEW_CONFIRM_DATE',
       to_char(:OLD.order_view_confirm_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_view_confirm_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_priority,0) != nvl(:NEW.order_priority,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_PRIORITY',OLD_order_priority, NEW_order_priority,NULL,NULL);
  END IF;
  IF nvl(:OLD.fk_current_hla,0) != nvl(:NEW.fk_current_hla,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_CURRENT_HLA',:OLD.fk_current_hla, :NEW.fk_current_hla,NULL,NULL);
  END IF;
  IF nvl(:OLD.creator,0) !=    nvl(:NEW.creator,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CREATOR',:OLD.creator, :NEW.creator,NULL,NULL);
  END IF;
   IF nvl(:OLD.created_on,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.created_on,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CREATED_ON',to_char(:OLD.created_on, pkg_dateutil.f_get_dateformat), to_char(:NEW.created_on, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
 IF nvl(:OLD.last_modified_by,0) != nvl(:NEW.last_modified_by,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER','LAST_MODIFIED_BY',:OLD.last_modified_by,:NEW.last_modified_by,NULL,NULL);
      END IF;
      IF nvl(:OLD.last_modified_date,to_date('31-dec-9595','DD-MON-YYYY')) != nvl(:NEW.last_modified_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER','LAST_MODIFIED_DATE',to_char(:OLD.last_modified_date, pkg_dateutil.f_get_dateformat),to_char(:NEW.last_modified_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
      END IF;
   IF nvl(:OLD.ip_add,' ') !=  nvl(:NEW.ip_add,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'IP_ADD',:OLD.ip_add, :NEW.ip_add,NULL,NULL);
  END IF;
  IF nvl(:OLD.deletedflag,0) !=  nvl(:NEW.deletedflag,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'DELETEDFLAG', :OLD.deletedflag, :NEW.deletedflag,NULL,NULL);
  END IF;
  IF nvl(:OLD.rid,0) !=   nvl(:NEW.rid,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'RID',:OLD.rid, :NEW.rid,NULL,NULL);
  END IF;
  IF nvl(:OLD.task_id,0) !=   nvl(:NEW.task_id,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'TASK_ID',:OLD.task_id, :NEW.task_id,NULL,NULL);
  END IF;
    IF nvl(:OLD.task_name,' ') != nvl(:NEW.task_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'TASK_NAME',:OLD.task_name, :NEW.task_name,NULL,NULL);
  END IF;
  IF nvl(:OLD.assigned_to,0) != nvl(:NEW.assigned_to,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ASSIGNED_TO', :OLD.assigned_to, :NEW.assigned_to,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_status_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_status_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_STATUS_DATE',
       to_char(:OLD.order_status_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_status_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_reviewed_by,0) !=  nvl(:NEW.order_reviewed_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_REVIEWED_BY',
       :OLD.order_reviewed_by, :NEW.order_reviewed_by,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_reviewed_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_reviewed_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_REVIEWED_DATE',
       to_char(:OLD.order_reviewed_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_reviewed_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
   IF nvl(:OLD.order_assigned_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_assigned_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_ASSIGNED_DATE',
       to_char(:OLD.order_assigned_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_assigned_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_order_resol_by_cbb,0) !=     nvl(:NEW.fk_order_resol_by_cbb,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_ORDER_RESOL_BY_CBB', :OLD.fk_order_resol_by_cbb, :NEW.fk_order_resol_by_cbb,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_order_resol_by_tc,0) !=     nvl(:NEW.fk_order_resol_by_tc,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_ORDER_RESOL_BY_TC',OLD_fk_order_resol_by_tc,NEW_fk_order_resol_by_tc,NULL,NULL);
  END IF;
    IF nvl(:OLD.order_resol_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_resol_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_RESOL_DATE',
       to_char(:OLD.order_resol_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_resol_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_ack_flag,' ') !=     nvl(:NEW.order_ack_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_ACK_FLAG',
       :OLD.order_ack_flag, :NEW.order_ack_flag,NULL,NULL);
  END IF;
   IF nvl(:OLD.order_ack_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_ack_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_ACK_DATE',
       to_char(:OLD.order_ack_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_ack_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
   IF nvl(:OLD.order_ack_by,0) !=     nvl(:NEW.order_ack_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_ACK_BY',
       :OLD.order_ack_by, :NEW.order_ack_by,NULL,NULL);
  END IF;
   IF nvl(:OLD.result_rec_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.result_rec_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'RESULT_REC_DATE',
       to_char(:OLD.result_rec_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.result_rec_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.accpt_to_cancel_req,' ') !=     nvl(:NEW.accpt_to_cancel_req,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ACCPT_TO_CANCEL_REQ',
       :OLD.accpt_to_cancel_req, :NEW.accpt_to_cancel_req,NULL,NULL);
  END IF;
   IF nvl(:OLD.cancel_conform_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.cancel_conform_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CANCEL_CONFORM_DATE',
       to_char(:OLD.cancel_conform_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.cancel_conform_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
   IF nvl(:OLD.canceled_by,0) !=     nvl(:NEW.canceled_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CANCELED_BY',:OLD.canceled_by, :NEW.canceled_by,NULL,NULL);
  END IF;
   IF nvl(:OLD.clinic_info_checklist_stat,' ') !=     nvl(:NEW.clinic_info_checklist_stat,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CLINIC_INFO_CHECKLIST_STAT',:OLD.clinic_info_checklist_stat, :NEW.clinic_info_checklist_stat,NULL,NULL);
  END IF;
  IF nvl(:OLD.recent_hla_typing_avail,' ') !=     nvl(:NEW.recent_hla_typing_avail,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'RECENT_HLA_TYPING_AVAIL',   :OLD.recent_hla_typing_avail, :NEW.recent_hla_typing_avail,NULL,NULL);
  END IF;
  IF nvl(:OLD.addi_test_result_avail,' ') !=     nvl(:NEW.addi_test_result_avail,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ADDI_TEST_RESULT_AVAIL',     :OLD.addi_test_result_avail, :NEW.addi_test_result_avail,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_sample_at_lab,' ') !=     nvl(:NEW.order_sample_at_lab,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_SAMPLE_AT_LAB',    :OLD.order_sample_at_lab, :NEW.order_sample_at_lab,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_case_manager,0) !=     nvl(:NEW.fk_case_manager,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_CASE_MANAGER',  :OLD.fk_case_manager, :NEW.fk_case_manager,NULL,NULL);
  END IF;
   IF nvl(:OLD.case_manager,' ') !=     nvl(:NEW.case_manager,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CASE_MANAGER',  :OLD.case_manager, :NEW.case_manager,NULL,NULL);
  END IF;
   IF nvl(:OLD.trans_center_id,0) != nvl(:NEW.trans_center_id,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'TRANS_CENTER_ID',   :OLD.trans_center_id, :NEW.trans_center_id,NULL,NULL);
  END IF;
    IF nvl(:OLD.trans_center_name,' ') !=     nvl(:NEW.trans_center_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'TRANS_CENTER_NAME',   :OLD.trans_center_name, :NEW.trans_center_name,NULL,NULL);
  END IF;
    IF nvl(:OLD.sec_trans_center_name,' ') !=     nvl(:NEW.sec_trans_center_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'SEC_TRANS_CENTER_NAME',:OLD.sec_trans_center_name, :NEW.sec_trans_center_name,NULL,NULL);
  END IF;
 IF nvl(:OLD.order_physician,0) !=     nvl(:NEW.order_physician,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'ORDER_PHYSICIAN',   :OLD.order_physician, :NEW.order_physician,NULL,NULL);
  END IF;
   IF nvl(:OLD.sec_trans_center_id,' ') !=     nvl(:NEW.sec_trans_center_id,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'SEC_TRANS_CENTER_ID',       :OLD.sec_trans_center_id, :NEW.sec_trans_center_id,NULL,NULL);
  END IF;
IF nvl(:OLD.cm_mail_id,' ') !=     nvl(:NEW.cm_mail_id,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CM_MAIL_ID',       :OLD.cm_mail_id, :NEW.cm_mail_id,NULL,NULL);
  END IF;
IF nvl(:OLD.cm_contact_no,' ') !=     nvl(:NEW.cm_contact_no,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'CM_CONTACT_NO',       :OLD.cm_contact_no, :NEW.cm_contact_no,NULL,NULL);
  END IF;
IF nvl(:OLD.req_clin_info_flag,' ') !=     nvl(:NEW.req_clin_info_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'REQ_CLIN_INFO_FLAG',       :OLD.req_clin_info_flag, :NEW.req_clin_info_flag,NULL,NULL);
  END IF;
IF nvl(:OLD.resol_ack_flag,' ') !=     nvl(:NEW.resol_ack_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'RESOL_ACK_FLAG',  :OLD.resol_ack_flag, :NEW.resol_ack_flag,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_sample_type_avail,0) !=     nvl(:NEW.fk_sample_type_avail,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_SAMPLE_TYPE_AVAIL', OLD_fk_sample_type_avail, NEW_fk_sample_type_avail,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_aliquots_type,0) !=     nvl(:NEW.fk_aliquots_type,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_ALIQUOTS_TYPE', OLD_fk_aliquots_type, NEW_fk_aliquots_type,NULL,NULL);
  END IF;
   IF NVL(:OLD.FK_RESOLUTION_CODE,0) !=     NVL(:NEW.FK_RESOLUTION_CODE,0) THEN
   pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER', 'FK_RESOLUTION_CODE',OLD_FK_RESOLUTION_CODE, NEW_FK_RESOLUTION_CODE,NULL,NULL);
   END IF;
  END;
/


ALTER TABLE ER_ORDER ADD(ASSIGNED_TO_TEMP VARCHAR2(20 BYTE));
UPDATE ER_ORDER SET ASSIGNED_TO_TEMP=ASSIGNED_TO;
UPDATE ER_ORDER SET ASSIGNED_TO='';
ALTER TABLE ER_ORDER MODIFY(ASSIGNED_TO NUMBER(10,0));
UPDATE ER_ORDER SET ASSIGNED_TO=ASSIGNED_TO_TEMP WHERE ASSIGNED_TO_TEMP IS NOT NULL AND lower(ASSIGNED_TO_TEMP)<>'undefined';
ALTER TABLE ER_ORDER DROP(ASSIGNED_TO_TEMP);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,20,'20_HotFix2.sql',sysdate,'9.0.0 Build#627');

commit;