--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch')
    AND BROWSERCONF_COLNAME='DELLINK';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"label":"Delete","format":"delLink","key":"DELLINK"}' , BROWSERCONF_EXPLABEL = 'Delete' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='DELLINK';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='allPatient')
    AND BROWSERCONF_COLNAME='DEL_LINK';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"key":"DEL_LINK","label":"Delete","format":"delLink"}' , BROWSERCONF_EXPLABEL = 'Delete' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='allPatient') and BROWSERCONF_COLNAME='DEL_LINK';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient')
    AND BROWSERCONF_COLNAME='DEL_LINK';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"label":"Delete","format":"delLink","key":"DEL_LINK"}' , BROWSERCONF_EXPLABEL = 'Delete' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient') and BROWSERCONF_COLNAME='DEL_LINK';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch')
    AND BROWSERCONF_COLNAME='PK_STUDY' and BROWSERCONF_EXPLABEL='Study Summary';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"key":"PK_STUDY", "label":"Study Summary","resizeable":true,"format":"studyLink"}' , BROWSERCONF_EXPLABEL = 'Study Summary' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='PK_STUDY';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch');
  if (v_record_exists >= 1) then
	Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) values (SEQ_ER_BROWSERCONF.nextval,2,'STUDYPATlINK',2,'{"key":"PK_STUDY", "label":"Patient Enrollment","resizeable":true,"format":"studypatLink"}','Patient Enrollment');
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch');   
  if (v_record_exists >= 1) then
	update ER_BROWSERCONF set BROWSERCONF_SEQ='3' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_TITLE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='4' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_NUMBER';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='5' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='TAREA';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='6' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='PHASE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='7' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STATUS_TYPE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='8' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='CURRENT_STAT_DESC';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='9' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STATUS';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='10' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_TEAM_RIGHTS';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='11' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_ACTUALDT_DATESORT';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='12' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDYSTAT_NOTE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='13' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDYSTAT_DATE_DATESORT';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='14' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='SITE_NAME';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='15' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='SPONSOR_NAME';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='16' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_RESTYPE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='17' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_DISEASE_SITE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='18' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_DIVISION_DESC';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='19' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='PI_NAME';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='20' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_TYPE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='21' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='AGENT_DEVICE';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='22' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_SITES';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='23' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='PATCOUNT_NUMSORT';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='24' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='AE_COUNT_NUMSORT';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='25' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='SAE_COUNT_NUMSORT';

	update ER_BROWSERCONF set BROWSERCONF_SEQ='26' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='DELLINK';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,16,'16_UPDATE_ER_BROWSERCONF.sql',sysdate,'9.0.0 Build#627');

commit;
