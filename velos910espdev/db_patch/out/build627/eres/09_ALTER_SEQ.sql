Set define off;
declare last_value number default 0;

begin 
  
  
  Select max(PK_ENTITY_STATUS) into last_value from cb_entity_status;
  
  
  If last_value is null or last_value = 0 then last_value:=1; end if;
    execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS increment by '||last_value;
  
  Select SEQ_CB_ENTITY_STATUS.nextval into last_value from dual;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS increment by 1';
end ;

/

declare last_value number default 0;

begin 
  Select max(PK_ENTITY_STATUS_REASON) into last_value from cb_entity_status_reason;
   
   If last_value is null or last_value = 0 then last_value:=1; end if;

  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS_REASON increment by '||last_value;
  Select SEQ_CB_ENTITY_STATUS_REASON.nextval into last_value from dual;
  execute immediate 'Alter sequence SEQ_CB_ENTITY_STATUS_REASON increment by 1';
end ;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,9,'09_ALTER_SEQ.sql',sysdate,'9.0.0 Build#627');

commit;