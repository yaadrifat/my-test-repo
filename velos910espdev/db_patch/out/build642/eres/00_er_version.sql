set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.1 Build#642' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,193,0,'00_er_version.sql',sysdate,'9.0.1 Build#642');

commit;

