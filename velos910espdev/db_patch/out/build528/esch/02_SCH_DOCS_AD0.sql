create or replace TRIGGER "ESCH"."SCH_DOCS_AD0" AFTER DELETE ON SCH_DOCS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_DOCS', :old.rid, 'D');
-- SCH_DOCS converted in Capital Letters to fixed BUG 4107
  deleted_data :=
   to_char(:old.pk_docs) || '|' ||
   :old.doc_name || '|' ||
   :old.doc_desc || '|' ||
   :old.doc_type || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.doc_size);

	insert into audit_delete
	(raid, row_data) values (raid, deleted_data);
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,2,'02_SCH_DOCS_AD0.sql',sysdate,'8.9.0 Build#528');

commit;
