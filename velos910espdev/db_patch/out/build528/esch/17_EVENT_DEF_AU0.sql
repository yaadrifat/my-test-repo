CREATE OR REPLACE TRIGGER "ESCH"."EVENT_DEF_AU0" AFTER UPDATE ON EVENT_DEF FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
	(raid, 'EVENT_DEF', :OLD.rid, 'U', usr );

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID',:OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID',:OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE',:OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME',:OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES',:OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST',:OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION',:OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION',:OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID',:OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI',:OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD',:OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO',:OLD.msg_to, :NEW.msg_to);
	END IF;
	IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS',:OLD.status, :NEW.status);
	END IF;
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION',:OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT',:OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID',:OLD.org_id, :NEW.org_id);
	END IF;
	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG',:OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES',:OLD.event_res, :NEW.event_res);
	END IF;
	IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'CREATED_ON',to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG',:OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE',:OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER',:OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE',:OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER',:OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE',:OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER',:OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE',:OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER',:OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY',:OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE',to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD',:OLD.ip_add, :NEW.ip_add);
	END IF;

--JM: newly added
	IF NVL(:OLD.calendar_sharedwith,' ') !=
		NVL(:NEW.calendar_sharedwith,' ') THEN
		audit_trail.column_update
		(raid, 'CALENDAR_SHAREDWITH',:OLD.calendar_sharedwith, :NEW.calendar_sharedwith);
	END IF;

	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT',:OLD.duration_unit, :NEW.duration_unit);
	END IF;

	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT',:OLD.fk_visit, :NEW.fk_visit);
	END IF;
------------

	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER',:OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;

	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER',:OLD.event_durationafter, :NEW.event_durationafter);
	END IF;

	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE',:OLD.event_cptcode, :NEW.event_cptcode);
	END IF;

	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE',:OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;

	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB',:OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
-- Added by Manimaran for Enh#C7
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY',:OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;
-- KM - 07Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,17,'17_EVENT_DEF_AU0.sql',sysdate,'8.9.0 Build#528');

commit;



