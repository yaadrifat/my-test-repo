--This is a sample kind of db script, it needs to be manipulated for testing .....

-- Please uncomment the following line after using proper value and run the script. <logged in user's role> can be 'role_prin', 'role_dm', 'role_coord' etc.
-- Example: update er_codelst set CODELST_STUDY_ROLE = 'role_prin', CODELST_CUSTOM_COL1 ='' where CODELST_TYPE = 'studystat_type' and CODELST_SUBTYP = 'default';

--update er_codelst set CODELST_STUDY_ROLE = '<logged in user's role >', CODELST_CUSTOM_COL1 ='' where CODELST_TYPE = 'studystat_type' and CODELST_SUBTYP = 'default';



---sample data for testing: codelst_type='studystat_type' -----------------------------------
INSERT INTO "ERES"."ER_CODELST" (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE ) 
VALUES (seq_er_codelst.nextval, null, 'studystat_type', 'defaults', 'Defaults', 'N', 2, '', 'default_data');


INSERT INTO "ERES"."ER_CODELST" (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE ) 
VALUES (seq_er_codelst.nextval, null, 'studystat_type', 'sample1', 'mysample1', 'N', 3, '', 'default_data');

INSERT INTO "ERES"."ER_CODELST" (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE ) 
VALUES (seq_er_codelst.nextval, null, 'studystat_type', 'sample2', 'mysample2', 'N', 4, '', 'default_data');

INSERT INTO "ERES"."ER_CODELST" (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE ) 
VALUES (seq_er_codelst.nextval, null, 'studystat_type', 'sample3', 'mysample3', 'N', 5, '', 'default_data');


-----------codelst_type='studystat' --------------------------------------
--for testing the study status and study team role related things in combination with the study status type .
--Please uncomment the following line(s) after using proper value and run the script. <logged in user's role> can be 'role_prin', 'role_dm', 'role_coord' etc.


--update er_codelst set CODELST_STUDY_ROLE ='<logged in user's role >' where CODELST_TYPE = 'studystat' and CODELST_SUBTYP = 'temp_cls';
--update er_codelst set CODELST_STUDY_ROLE ='<logged in user's role >' where CODELST_TYPE = 'studystat' and CODELST_SUBTYP = 'irb_approved';



commit;



