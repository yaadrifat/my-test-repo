set define off;

DECLARE
countFlag NUMBER(5);
BEGIN
	SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_LABEL_TEMPLATES';
	if (countFlag > 0) then
		UPDATE ER_LABEL_TEMPLATES SET 
		TEMPLATE_SUBTYPE = 'datamatrix' WHERE TEMPLATE_NAME='DataMatrix Barcode' AND TEMPLATE_TYPE IN ('specimen','storage');
		
		UPDATE ER_LABEL_TEMPLATES SET 
		TEMPLATE_SUBTYPE = 'pdf417' WHERE TEMPLATE_NAME='PDF417 Barcode' AND TEMPLATE_TYPE IN ('specimen','storage');
		commit;
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,131,2,'02_ER_LABEL_TEMPLATES.sql',sysdate,'9.0.0 Build#588');

commit;
