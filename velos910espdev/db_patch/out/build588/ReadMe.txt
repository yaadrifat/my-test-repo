/* This readMe is specific to Velos eResearch version 9.0 build #588 */

=====================================================================================
For INV-18266 (INVP3.1) (Label Templates)

After the build #588 patch, the label templates (TEMPLATE_NAME='DataMatrix Barcode' OR TEMPLATE_NAME='PDF417 Barcode') 
in ER_LABEL_TEMPLATES table will have null in FK_ACCOUNT column. 
- IF the FK_ACCOUNT is NULL for a label template then that template will not be available at all (unless subtype is 'default').
- IF the FK_ACCOUNT is upadated with a valid number for a label template, then that template will be available 
for that customer account (and will not be available to any other accounts).

Instructions to update FK_ACCOUNT for a label template:
1. Find the FK_ACCOUNT for your eResearch user login and note it down. 
You can select FK_ACCOUNT column from ER_USER table for a specific user. 
Sample SQL -
SELECT FK_ACCOUNT FROM ER_USER WHERE PK_USER = 15;

2. Update the FK_ACCOUNT value for the desired label template records in ER_LABEL_TEMPLATES table.
Sample SQL -
UPDATE ER_LABEL_TEMPLATES SET FK_ACCOUNT = 50
WHERE TEMPLATE_NAME='DataMatrix Barcode' AND TEMPLATE_TYPE IN ('specimen','storage');

3. Commit the above operation.
4. No application server restart is needed. The change is immediate.

=====================================================================================

=====================================================================================
For INF-20084 (Datepicker Implementation)

We have completed implementation of datepicker across the application. 
Here is the List of JSP's on which this feature is implemented.

1	specimenbrowser.jsp
2	editmultiplespecimenstatus.jsp
3	specimenstatus.jsp
4	specimendetails.jsp
5	preparationAreaBrowser.jsp
6	storagestatus.jsp
7	storageunitdetails.jsp
8	editmultiplestoragestatus.jsp
9	labmodify.jsp
10	getFormSpecimenData.jsp
11	labdata.jsp
12	invoice_step1.jsp
13	editstatus.jsp
14	viewInvoice.jsp
15	milepayments.jsp
16	patstudystatus.jsp
17	pattrtarm.jsp
18	advevent_new.jsp
19	addmultipleadvevent.jsp
20	patientprotocol.jsp
21	editMulEventDetails.jsp
22	getAdvToxicity.jsp
23	studycentricpatenroll.jsp
24	addNewQuery.jsp
25	crfstatus.jsp
26	viewexpstatus.jsp
27	viewimpstatus.jsp
28	studystatus.jsp
29	editStudyTeamStatus.jsp
30	updatemultschedules.jsp
31	calendarstatus.jsp
32	formStatus.jsp
33	appendix_file_multi.jsp
34	studyver.jsp
35	studyschedule.jsp
36	study.jsp
37	selectDateRange.jsp
38	rep_activityreport.jsp
39	editPortalStatus.jsp
40	mdynfilter.jsp
41	reportcentral.jsp
42	protocolcalendar.jsp
43	MarkDone.jsp
44	patientFacility.jsp
45	perapndxfile.jsp
46	addperurl.jsp
47	patientdetails.jsp
48	irbactionwin.jsp
49	wsPatientSearch.jsp
50	morePerDetails.jsp
51	provisoDetails.jsp
52	adminauditreports.jsp
53	discdetails.jsp
54	accountbrowser.jsp
55	changedate.jsp
56	selectDateRangeGeneric.jsp
57	discontinuation.jsp
58	advevent.jsp
59	studyapndx.xsl
60	allpatientdetails.jsp
61	editStudyMulEventDetails.jsp
62	patientschedule.jsp
63	patientdetailsquick.jsp
64	changestudydates.jsp
=====================================================================================
