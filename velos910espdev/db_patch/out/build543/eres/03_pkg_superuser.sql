CREATE OR REPLACE PACKAGE "ERES"."PKG_SUPERUSER" IS

   /* Sonia Abrol  10/22/07
   checks if a user has super user access rights to a study.
   returns:
   1:yes
   0:no
   */
   function F_Is_Superuser(p_user  NUmber, p_study Number) return number;

   function F_get_Superuser_right(p_user  Number ) return varchar2;

   function F_Is_BgtSuperuser(p_user  NUmber) return number;

    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_Superuser', pLEVEL  => Plog.LFATAL);


END Pkg_Superuser ;
/

CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_SUPERUSER" 
IS

  function F_Is_Superuser(p_user  NUmber, p_study Number) return number
  IS
  v_group Number;
  v_rights Varchar2(100) := '';
  v_flag Number;

   v_acc_study Number :=0;
   v_settings_count Number :=0;
   v_usr_account Number ;

  Begin
  	-- get user group and see if its super user

  	select fk_grp_default , nvl(grp_supusr_rights,''),nvl(grp_supusr_flag,0),er_user.fk_account
  	into v_group,v_rights,v_flag,v_usr_account
  	from ER_USER,ER_GRPS
  	where pk_user = p_user and pk_grp = fk_grp_default ;

  	if v_flag = 1 then

	 select count (*)
	 into v_settings_count
	 from
	 	 ER_SETTINGS
	 WHERE settings_modname=4 AND settings_modnum=v_group
	 AND settings_keyword in ('STUDY_TAREA' ,'STUDY_DIVISION','STUDY_DISSITE') ;

	begin
	  if v_settings_count > 0 then

	   	select pk_study
  		into v_acc_study
		from ER_STUDY
  		where pk_study = p_study and fk_account = v_usr_account
  		and (
  		(((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=v_group
	 AND settings_keyword = 'STUDY_TAREA') LIKE '%'||TO_CHAR(fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL))
	 OR
	 (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=v_group
	 AND settings_keyword = 'STUDY_DIVISION') LIKE '%'||TO_CHAR(study_division)||'%') AND (study_division IS NOT NULL))
	 	 	 OR
	(1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum='||v_group||' AND settings_keyword = ''STUDY_DISSITE''',
	    study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) )
	  				 );

			 return 1 ;
		else

			select count(pk_study)
  			into v_acc_study
			from ER_STUDY
  			where pk_study = p_study and fk_account = v_usr_account ;

			return v_acc_study;

		end if;

		exception when NO_DATA_FOUND then

				  plog.fatal(pctx,'psuper' || sqlerrm);
				  return 0;
		end;


	else
		return 0;
  	end if;



  End; -- for function

  function F_get_Superuser_right(p_user Number) return varchar2
    is
    v_right varchar2(100);
  begin
	  	select nvl(grp_supusr_rights,'')
	  	into v_right
	  	from ER_USER,ER_GRPS
	  	where pk_user = p_user and pk_grp = fk_grp_default ;

		return v_right;
  end; -- for function
  
  function F_Is_BgtSuperuser(p_user  Number) return number
  IS
  v_group Number;
  v_rights Varchar2(100) := '';
  v_flag Number;
  v_usr_account Number ;

  Begin
  	-- get user group and see if its super user

  	select fk_grp_default , nvl(GRP_SUPBUD_RIGHTS,''),nvl(GRP_SUPBUD_FLAG,0),er_user.fk_account
  	into v_group,v_rights,v_flag,v_usr_account
  	from ER_USER,ER_GRPS
  	where pk_user = p_user and pk_grp = fk_grp_default ;

  	
	return v_flag;

	exception when NO_DATA_FOUND then

	  plog.fatal(pctx,'psuper' || sqlerrm);
	  return 0;
	
  End; -- for function


END Pkg_Superuser;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,86,3,'03_pkg_superuser.sql',sysdate,'8.9.0 Build#543');

commit;
