/* This readMe is specific to Velos eResearch version 9.0 build #621 */

=====================================================================================================================================
Garuda :

 
=====================================================================================================================================
eResearch:

Small UI related fixes are done for which please refer to "UI Review Quick-fixes.xls" enclosed with version.
Please note: It is a running document which would be updated accordingly in coming builds also. 


=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	115.xsl
2	adveventlookup.jsp
3	editmultipleSpecimenstatus.jsp
4	editmultipleStoragestatus.jsp
5	ereslogin.jsp
6	formsetting.jsp
7	irbnewinit.jsp
8	labelBundle.properties
9	LC.java
10	LC.jsp
11	login.css
12	MC.java
13	MC.jsp
14	messageBundle.properties
15	milestone.jsp
16	milestonepaymentbrowser.jsp
17	patientdetailsquick.jsp
18	patientlogin.jsp
19	pricinginfo.jsp
20	specimenBrowser.jsp
21	specimenDetail.jsp
22	storageadminbrowser.jsp
23	studyprotocol.jsp

One report (XSL Based) has been corrected. The related script (01_data.sql) is Specific to this.

We need to execute the following in the sequence: 
1. 01_data.sql
2. loadxsl.bat
=====================================================================================================================================
