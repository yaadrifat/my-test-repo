CREATE OR REPLACE FUNCTION        "F_GET_ER_SITE_NAME" (p_id VARCHAR2)
RETURN VARCHAR2
IS
  v_site_name VARCHAR2(200);
BEGIN
	 SELECT SITE_NAME INTO v_site_name FROM ER_SITE WHERE PK_SITE = p_id;
  RETURN v_site_name ;
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,8,'08_F_GET_ER_SITE_NAME.sql',sysdate,'9.0.0 Build#621');

commit;
