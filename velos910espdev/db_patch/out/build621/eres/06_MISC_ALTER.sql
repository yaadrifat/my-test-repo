--STARTS RENAMING COLUMN last_modified_on TO LAST_MODIFIED_DATE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'cb_funding_guidelines' AND COLUMN_NAME = 'last_modified_on';
IF (v_column_exists > 0) THEN
	EXECUTE IMMEDIATE 'alter table cb_funding_guidelines rename column last_modified_on to LAST_MODIFIED_DATE;';
END IF;
END;
/
--END

--Sql for Table CB_CORD to add column CBU_COLL_TIM
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CBU_COLL_TIM'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add CBU_COLL_TIM varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CBU_COLL_TIM IS 'store the cbu collection time'; 
commit;

--Sql for Table CB_CORD to add column CBU_BABY_BIR_TIM
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CBU_BABY_BIR_TIM'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add CBU_BABY_BIR_TIM varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CBU_BABY_BIR_TIM IS 'store the BABY BIRTH time'; 
commit;


	/*******build #621********/
	
	
	
--Sql for Table ER_USERSESSIONLOG to add column USL_ENTITY_ID
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_USERSESSIONLOG' AND column_name = 'USL_ENTITY_ID'; 
  if (v_column_exists = 0) then
      execute immediate ('ALTER TABLE ER_USERSESSIONLOG add USL_ENTITY_ID NUMBER(10,0)');
  end if;
end;
/
COMMENT ON COLUMN ER_USERSESSIONLOG.USL_ENTITY_ID IS 'store the pk of the entity'; 
commit;

--Sql for Table ER_USERSESSIONLOG to add column USL_ENTITY_TYPE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_USERSESSIONLOG' AND column_name = 'USL_ENTITY_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('ALTER TABLE ER_USERSESSIONLOG add USL_ENTITY_TYPE VARCHAR2(50 CHAR)');
  end if;
end;
/
COMMENT ON COLUMN ER_USERSESSIONLOG.USL_ENTITY_TYPE IS 'store the type of the entity'; 
commit;

--Sql for Table ER_USERSESSIONLOG to add column USL_ENTITY_IDENTIFIER
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_USERSESSIONLOG' AND column_name = 'USL_ENTITY_IDENTIFIER'; 
  if (v_column_exists = 0) then
      execute immediate ('ALTER TABLE ER_USERSESSIONLOG add USL_ENTITY_IDENTIFIER VARCHAR2(50 CHAR)');
  end if;
end;
/
COMMENT ON COLUMN ER_USERSESSIONLOG.USL_ENTITY_IDENTIFIER IS 'store the Identifier of the entity'; 
commit;

--Sql for Table ER_USERSESSIONLOG to add column USL_ENTITY_EVENT_CODE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_USERSESSIONLOG' AND column_name = 'USL_ENTITY_EVENT_CODE'; 
  if (v_column_exists = 0) then
      execute immediate ('ALTER TABLE ER_USERSESSIONLOG add USL_ENTITY_EVENT_CODE VARCHAR2(50 CHAR)');
  end if;
end;
/
COMMENT ON COLUMN ER_USERSESSIONLOG.USL_ENTITY_EVENT_CODE IS 'store the event code of the entity'; 
commit;


--STARTS ADDING COLUMN TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'POSTPROCESS_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(POSTPROCESS_CHILD varchar2(20))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'VIABILITY_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(VIABILITY_CHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."POSTPROCESS_CHILD" IS 'This is the column to store the result of postprocessing child question';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."VIABILITY_CHILD" IS 'This is the column to store the result of viability child question';

--STARTS ADD THE COLUMN CONFIRM_CBB_SIGN_FLAG TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'CONFIRM_CBB_SIGN_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (CONFIRM_CBB_SIGN_FLAG  VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN CONFIRM_CBB_SIGN_FLAG TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.CONFIRM_CBB_SIGN_FLAG  is 'Storing the CONFIRM_CBB_SIGN_FLAG 0 or 1.';
--end--

--STARTS ADD THE COLUMN CBB_SIGNATURE_CREATED_ON TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'CBB_SIGNATURE_CREATED_ON'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (CBB_SIGNATURE_CREATED_ON  DATE)';
  end if;
end;
/
--END--
--starts comment for THE COLUMN CBB_SIGNATURE_CREATED_ON TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.CBB_SIGNATURE_CREATED_ON  is 'Storing the CONFIRM CBB SIGNATURE task created date.';
--end--

--STARTS ADD THE COLUMN CBB_SIGNATURE_CREATOR from CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'CBB_SIGNATURE_CREATOR'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (CBB_SIGNATURE_CREATOR NUMBER(10,0))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN CBB_SIGNATURE_CREATOR TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.CBB_SIGNATURE_CREATOR  is 'Storing the FINAL_REVIEW of confirm cbb signature created date.';
--end--
commit;


--STARTS MODIFYING COLUMN EMAIL_FLAG FROM CB_USER_ALERTS TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'EMAIL_FLAG';
IF (v_column_exists = 1) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_USER_ALERTS MODIFY(EMAIL_FLAG VARCHAR2(1 BYTE))';
END IF;
END;
/
--END


--STARTS MODIFYING COLUMN NOTIFY_FLAG FROM CB_USER_ALERTS TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'NOTIFY_FLAG';
IF (v_column_exists = 1) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_USER_ALERTS MODIFY(NOTIFY_FLAG VARCHAR2(1 BYTE))';
END IF;
END;
/
--END


--STARTS MODIFYING COLUMN PROMPT_FLAG TO CB_USER_ALERTS TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'PROMPT_FLAG';
IF (v_column_exists = 1) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_USER_ALERTS MODIFY(PROMPT_FLAG VARCHAR2(1 BYTE))';
END IF;
END;
/
--END


commit;

set define off;

--Sql for Table CBB to add column FK_DRY_SHIPPER_ADD, ATTENTION_DRY_SHIPPER.

DECLARE
  v_column_one_exists number := 0;  
  v_column_two_exists number := 0;   
BEGIN
  Select count(*) into v_column_one_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_DRY_SHIPPER_ADD'; 
 Select count(*) into v_column_two_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'ATTENTION_DRY_SHIPPER'; 
 
  if (v_column_one_exists = 0) then
      execute immediate ('ALTER TABLE CBB ADD FK_DRY_SHIPPER_ADD NUMBER(10)');
  end if;
   if (v_column_two_exists = 0) then
      execute immediate ('ALTER TABLE CBB ADD ATTENTION_DRY_SHIPPER VARCHAR2(25)');
  end if;   
end;
/
COMMENT ON COLUMN CBB.FK_DRY_SHIPPER_ADD IS 'Values reference from ER_ADD.'; 
COMMENT ON COLUMN CBB.ATTENTION_DRY_SHIPPER IS 'Stores attention of Dry Shipper Address.'; 

commit;

--STARTS modifying length of column of ER_LKPVIEW
set define off;
--STARTS ALTERING COLUMN
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_LKPVIEW' AND COLUMN_NAME = 'LKPVIEW_NAME';
IF (v_column_exists > 0) THEN
    EXECUTE IMMEDIATE 'alter table ER_LKPVIEW modify LKPVIEW_NAME VARCHAR2(40)';
END IF;
END;
/
--END 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,6,'06_MISC_ALTER.sql',sysdate,'9.0.0 Build#621');

commit;
