CREATE OR REPLACE
TRIGGER "ERES".CB_ENTITY_STATUS_AU0 AFTER UPDATE OF PK_ENTITY_STATUS,
ENTITY_ID,
FK_ENTITY_TYPE,
STATUS_TYPE_CODE,
FK_STATUS_VALUE,
STATUS_DATE,
STATUS_REMARKS,
CREATOR,
CREATED_ON,
IP_ADD,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
RID,
DELETEDFLAG,
LICN_ELIG_CHANGE_REASON  ON CB_ENTITY_STATUS FOR EACH ROW DECLARE raid NUMBER(10);
  usr                                                                  VARCHAR2(100);
  old_modby                                                            VARCHAR2(100);
  new_modby                                                            VARCHAR2(100);
   NEW_FK_STATUS_VALUE                    VARCHAR2(200);
  OLD_FK_STATUS_VALUE                     VARCHAR2(200);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:NEW.last_modified_by);
    IF NVL(:OLD.FK_STATUS_VALUE,0) !=     NVL(:NEW.FK_STATUS_VALUE,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:new.FK_STATUS_VALUE)		INTO OLD_FK_STATUS_VALUE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_STATUS_VALUE := '';
   END;  
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_STATUS_VALUE)		INTO NEW_FK_STATUS_VALUE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_STATUS_VALUE := '';
   END;  
  END IF; 
    audit_trail.record_transaction (raid, 'CB_ENTITY_STATUS', :old.RID, 'U', usr);
	IF NVL(:old.PK_ENTITY_STATUS,0) != NVL(:new.PK_ENTITY_STATUS,0) THEN
      audit_trail.column_update (raid, 'PK_ENTITY_STATUS', :OLD.PK_ENTITY_STATUS, :NEW.PK_ENTITY_STATUS);
    END IF;	
	IF NVL(:old.ENTITY_ID,0) != NVL(:new.ENTITY_ID,0) THEN
      audit_trail.column_update (raid, 'ENTITY_ID', :OLD.ENTITY_ID, :NEW.ENTITY_ID);
    END IF;
	IF NVL(:old.FK_ENTITY_TYPE,0) != NVL(:new.FK_ENTITY_TYPE,0) THEN
      audit_trail.column_update (raid, 'FK_ENTITY_TYPE', :OLD.FK_ENTITY_TYPE, :NEW.FK_ENTITY_TYPE);
    END IF;
	IF NVL(:old.STATUS_TYPE_CODE,' ') != NVL(:new.STATUS_TYPE_CODE,' ') THEN
      audit_trail.column_update (raid, 'STATUS_TYPE_CODE', :OLD.STATUS_TYPE_CODE, :NEW.STATUS_TYPE_CODE);
    END IF;	
	IF NVL(:old.FK_STATUS_VALUE,0) != NVL(:new.FK_STATUS_VALUE,0) THEN
      audit_trail.column_update (raid, 'FK_STATUS_VALUE', OLD_FK_STATUS_VALUE, NEW_FK_STATUS_VALUE);
    END IF;
	IF NVL(:old.STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'STATUS_DATE', TO_CHAR(:OLD.STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.STATUS_REMARKS,' ') != NVL(:new.STATUS_REMARKS,' ') THEN
      audit_trail.column_update (raid, 'STATUS_REMARKS', :OLD.STATUS_REMARKS, :NEW.STATUS_REMARKS);
    END IF;
	 IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	  IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;     
    IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
    IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)   || ','  || usr_lastname   ||', '  || usr_firstname    INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)    || ','   || usr_lastname   ||', ' || usr_firstname  INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
    IF NVL(:old.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.RID, :NEW.RID);
    END IF;
	IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :old.DELETEDFLAG, :new.DELETEDFLAG);
    END IF;	 
	IF NVL(:old.LICN_ELIG_CHANGE_REASON,' ') != NVL(:new.LICN_ELIG_CHANGE_REASON,' ') THEN
      audit_trail.column_update (raid, 'LICN_ELIG_CHANGE_REASON', :old.LICN_ELIG_CHANGE_REASON, :new.LICN_ELIG_CHANGE_REASON);
    END IF;	
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,22,'22_CB_ENTITY_STATUS_AU0.sql',sysdate,'9.0.0 Build#621');

commit;
