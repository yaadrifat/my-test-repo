CREATE OR REPLACE TRIGGER "ERES".CB_MULTIPLE_VALUES_AU0 AFTER  UPDATE OF
PK_MULTIPLE_VALUES,FK_CODELST_TYPE,FK_CODELST,ENTITY_ID,ENTITY_TYPE,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,
RID,DELETEDFLAG,LAST_MODIFIED_DATE ON CB_MULTIPLE_VALUES   FOR EACH ROW 
DECLARE 
raid NUMBER(10);
  usr                                         VARCHAR2(100);
  old_modby                                   VARCHAR2(100);
  new_modby                                   VARCHAR2(100);
  NEW_FK_CODELST                              VARCHAR2(200);
  OLD_FK_CODELST                              VARCHAR2(200);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:NEW.last_modified_by);
     IF NVL(:OLD.FK_CODELST,0) !=  NVL(:NEW.FK_CODELST,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_CODELST)		INTO NEW_FK_CODELST  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_CODELST := '';
   END;       
  END IF;
   IF NVL(:OLD.FK_CODELST,0) !=  NVL(:NEW.FK_CODELST,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_CODELST)		INTO OLD_FK_CODELST  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_CODELST := '';
   END;       
  END IF;
    audit_trail.record_transaction (raid, 'CB_MULTIPLE_VALUES', :old.RID, 'U', usr);
	IF NVL(:old.PK_MULTIPLE_VALUES,0) != NVL(:new.PK_MULTIPLE_VALUES,0) THEN
      audit_trail.column_update (raid, 'PK_MULTIPLE_VALUES', :OLD.PK_MULTIPLE_VALUES, :NEW.PK_MULTIPLE_VALUES);
    END IF;
	IF NVL(:old.FK_CODELST_TYPE,' ') != NVL(:new.FK_CODELST_TYPE,' ') THEN
      audit_trail.column_update (raid, 'FK_CODELST_TYPE', :old.FK_CODELST_TYPE, :new.FK_CODELST_TYPE);
    END IF;
	IF NVL(:old.FK_CODELST,0) != NVL(:new.FK_CODELST,0) THEN
      audit_trail.column_update (raid, 'FK_CODELST', OLD_FK_CODELST, NEW_FK_CODELST);
    END IF;
	IF NVL(:old.ENTITY_ID,0) != NVL(:new.ENTITY_ID,0) THEN
      audit_trail.column_update (raid, 'ENTITY_ID', :OLD.ENTITY_ID, :NEW.ENTITY_ID);
    END IF;
	IF NVL(:old.ENTITY_TYPE,' ') != NVL(:new.ENTITY_TYPE,' ') THEN
      audit_trail.column_update (raid, 'ENTITY_TYPE', :OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE);
    END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF; 
 IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
	IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
	IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :old.DELETEDFLAG, :new.DELETEDFLAG);
    END IF;
	 IF NVL(:old.rid,0) != NVL(:new.rid,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.rid, :NEW.rid);
    END IF;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,30,'30_CB_MULTIPLE_VALUES_AU0.sql',sysdate,'9.0.0 Build#621');

commit;