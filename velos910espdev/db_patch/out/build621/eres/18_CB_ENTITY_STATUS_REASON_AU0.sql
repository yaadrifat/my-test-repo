CREATE OR REPLACE
TRIGGER "ERES".CB_ENTITY_STATUS_REASON_AU0 AFTER UPDATE OF PK_ENTITY_STATUS_REASON,
FK_ENTITY_STATUS,
FK_REASON_ID,
CREATOR,
CREATED_ON,
IP_ADD,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
RID,
DELETEDFLAG,
FK_ATTACHMENTID,
FK_FORMID,
NOT_APP_PRIOR_FLAG  ON CB_ENTITY_STATUS_REASON FOR EACH ROW DECLARE raid NUMBER(10);
  usr                                                                  VARCHAR2(100);
  old_modby                                                            VARCHAR2(100);
  new_modby                                                            VARCHAR2(100);
   NEW_FK_REASON_ID                    VARCHAR2(4000);
  OLD_FK_REASON_ID                     VARCHAR2(4000);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:NEW.last_modified_by);
    IF NVL(:OLD.FK_REASON_ID,0) !=     NVL(:NEW.FK_REASON_ID,0) THEN
 BEGIN
   		SELECT F_Codelst_Desc(:new.FK_REASON_ID)		INTO OLD_FK_REASON_ID  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_REASON_ID := '';
   END;  
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_REASON_ID)		INTO NEW_FK_REASON_ID  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_REASON_ID := '';
   END;  
  END IF; 
    audit_trail.record_transaction (raid, 'CB_ENTITY_STATUS_REASON', :old.RID, 'U', usr);
	IF NVL(:old.PK_ENTITY_STATUS_REASON,0) != NVL(:new.PK_ENTITY_STATUS_REASON,0) THEN
      audit_trail.column_update (raid, 'PK_ENTITY_STATUS_REASON', :OLD.PK_ENTITY_STATUS_REASON, :NEW.PK_ENTITY_STATUS_REASON);
    END IF;	
	IF NVL(:old.FK_ENTITY_STATUS,0) != NVL(:new.FK_ENTITY_STATUS,0) THEN
      audit_trail.column_update (raid, 'FK_ENTITY_STATUS', :OLD.FK_ENTITY_STATUS, :NEW.FK_ENTITY_STATUS);
    END IF;    
	IF NVL(:old.FK_REASON_ID,0) != NVL(:new.FK_REASON_ID,0) THEN
      audit_trail.column_update (raid, 'FK_REASON_ID', OLD_FK_REASON_ID, NEW_FK_REASON_ID);
    END IF;
	 IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	  IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;     
    IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
    IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)   || ','  || usr_lastname   ||', '  || usr_firstname    INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)    || ','   || usr_lastname   ||', ' || usr_firstname  INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
    IF NVL(:old.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.RID, :NEW.RID);
    END IF;
	IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :old.DELETEDFLAG, :new.DELETEDFLAG);
    END IF;	 
	IF NVL(:old.FK_ATTACHMENTID,0) != NVL(:new.FK_ATTACHMENTID,0) THEN
      audit_trail.column_update (raid, 'FK_ATTACHMENTID', :OLD.FK_ATTACHMENTID, :NEW.FK_ATTACHMENTID);
    END IF;
	IF NVL(:old.FK_FORMID,0) != NVL(:new.FK_FORMID,0) THEN
      audit_trail.column_update (raid, 'FK_FORMID', :OLD.FK_FORMID, :NEW.FK_FORMID);
    END IF;	
	IF NVL(:old.NOT_APP_PRIOR_FLAG,' ') != NVL(:new.NOT_APP_PRIOR_FLAG,' ') THEN
      audit_trail.column_update (raid, 'NOT_APP_PRIOR_FLAG', :old.NOT_APP_PRIOR_FLAG, :new.NOT_APP_PRIOR_FLAG);
    END IF;	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,18,'18_CB_ENTITY_STATUS_REASON_AU0.sql',sysdate,'9.0.0 Build#621');

commit;
