/* This readMe is specific to Velos eResearch version 9.0 build #632 */

=====================================================================================================================================
Garuda :
Following scripts are specific to NMDP.
05_ER_CODELST_INSERT.SQL
06_ER_CODELST_UPDATE.SQL 

11_UPDATE_CORD_STATUS_JOB.SQL - Becuase of some job scheduling one Alert message will be shown on the console repeatedly. The message is
'Alert condition message executed....'

	
	     
=====================================================================================================================================
eResearch:

 1. For Bug#10023 and 10027:-

Please refer  excel file(INF-22311-MouseoverText (QA).xls)  released  along with this build. It has the required information to test these two issues. Please find the "Developer Comments" column in excel sheet to check the status of issues mentioned.

2. For INF-22324 New Icons and mouseover implementation:-

Please refer An excel file(INF-22324-moreIcons.xlsx)  released  along with this build. we have made changes only in the places that are mentioned in the excel sheet.Please find the "Developer Comments" column in excel sheet to check the status of issues mentioned. 
Two zip files named :
1)INF-22324-screenshots.zip and 2)Latest 14 Icons.zip       are also released along with this build.

Please refer to "INF-22324-screenshots.zip" file to know the places where changes are made and "Latest 14 Icons.zip" file for new icons for reference. 

3. UI Framework Standards and Guidelines:

UI guideline document has been updated to include a style for Messages text and its background color,All the messages, like Data Saved Successfully, Wrong eSign, Any Business rule validation and Successful or failure of Delete messages, etc,  weather in a popup or main application page should be consistent and follow the same text and background color also Both Redmond and Velos Default skin will have same style. 
We have implemented the changes partially in this build.
In this Build  we have covered  Data Saved Successfully and Wrong eSign messages. The rest of the messages will be covered in next build.


=====================================================================================================================================
eResearch Localization:


=====================================================================================================================================
