--STARTS DELETING RECORDS FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from CB_ALERT_CONDITIONS where FK_CODELST_ALERT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_09','alert_10','alert_12','alert_13','alert_14','alert_21','alert_22'));
  if (v_record_exists > 1) then
    DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_09','alert_10','alert_12','alert_13','alert_14','alert_21','alert_22'));
    commit;
  end if;
end;
/
--END--



--STARTS DELETING RECORDS FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP ='alert_19');
  if (v_record_exists > 1) then
    DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP ='alert_19');
    commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,2,'02_CB_ALERT_CONDITIONS_DELETE.sql',sysdate,'9.0.0 Build#632');

commit;