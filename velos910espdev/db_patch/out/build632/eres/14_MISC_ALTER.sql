
--STARTS ADD THE COLUMN in ER_ORDER--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME = 'CORD_AVAILABILITY_CONFORMEDBY';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER add (CORD_AVAILABILITY_CONFORMEDBY  NUMBER(10,0))';
      commit;
  end if;
end;
/
--END--
--starts comment for THE COLUMN CORD_AVAILABILITY_CONFORMEDBY TO ER_ORDER TABLE--
comment on column ER_ORDER.CORD_AVAILABILITY_CONFORMEDBY  is 'Storing the pk value of submitted user who submitted cord availability conformation';
commit;
--end--


--STARTS ADD THE COLUMN in ER_ORDER--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME = 'CORD_AVAILABILITY_CONFORMEDON';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER add (CORD_AVAILABILITY_CONFORMEDON  DATE)';
      commit;
  end if;
end;
/
--END--
--starts comment for THE COLUMN CORD_AVAILABILITY_CONFORMEDON TO ER_ORDER TABLE--
comment on column ER_ORDER.CORD_AVAILABILITY_CONFORMEDON  is 'Storing date when availablility conformation performed';
commit;
--end--


--STARTS ADD THE COLUMN in CB_SHIPMENT--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_SHIPMENT'
    AND COLUMN_NAME = 'SCH_SHIPMENT_TIME';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT add (SCH_SHIPMENT_TIME  varchar2(5))';
      commit;
  end if;
end;
/
--END--
--starts comment for THE COLUMN SCH_SHIPMENT_TIME TO CB_SHIPMENT TABLE--
comment on column CB_SHIPMENT.SCH_SHIPMENT_TIME  is 'Storing sheduled shipping time';
commit;
--end--




set serveroutput off;
declare
v_date_temp varchar2(255);
v_shipmentId number;
v_time varchar2(5);
begin
for sd in(select pk_shipment,to_char(shipment_time,'Mon DD, YYYY') dat,to_char(shipment_time,'HH24:MI') tim from cb_shipment where shipment_time is not null)
loop
v_date_temp:=sd.dat;
v_time:=sd.tim;
v_shipmentid:=sd.pk_shipment;
dbms_output.put_line('Date is::'||v_date_temp||'....Time is:::::::'||v_time);
dbms_output.put_line('Date in Date format:::::'||to_date(v_date_temp,'Mon DD, YYYY'));
update cb_shipment set sch_shipment_date=to_date(v_date_temp,'Mon DD, YYYY'),sch_shipment_time=v_time where pk_shipment=v_shipmentid;
commit;
end loop;
end;
/ 


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,14,'14_MISC_ALTER.sql',sysdate,'9.0.0 Build#632');

commit;