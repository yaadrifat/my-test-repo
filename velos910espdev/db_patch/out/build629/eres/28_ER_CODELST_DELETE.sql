--DELETE SCRIPT FOR ER_CODELST

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol';
  if (v_record_exists > 0) then
      DELETE FROM ER_CODELST WHERE codelst_type = 'alert_resol';
	commit;
  end if;
end;
/
--END--

delete from er_codelst where codelst_type='temp_moniter' and codelst_subtyp in ('TEMP_MONI1','TEMP_MONI2');
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,28,'28_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#629');

commit;