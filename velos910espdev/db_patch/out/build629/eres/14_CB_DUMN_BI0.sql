create or replace TRIGGER CB_DUMN_BI0 BEFORE INSERT ON CB_DUMN 
REFERENCING OLD AS OLD NEW AS NEW
	FOR EACH ROW
	DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data VARCHAR2(4000);
	 BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_DUMN',erid, 'I',usr);
       insert_data:=:NEW.PK_DUMN || '|' ||
to_char(:NEW.MRQ_QUES_1) || '|' ||
to_char(:NEW.MRQ_QUES_1_A) || '|' ||
to_char(:NEW.MRQ_QUES_1_B) || '|' ||
to_char(:NEW.MRQ_QUES_1_AD_DETAIL) || '|' ||
to_char(:NEW.MRQ_QUES_2) || '|' ||
to_char(:NEW.MRQ_QUES_2_AD_DETAIL) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3_A) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3_B) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3_C) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3_D) || '|' ||
to_char(:NEW.PHY_FIND_QUES_3_AD_DETAIL) || '|' ||
to_char(:NEW.PHY_FIND_QUES_4) || '|' ||
to_char(:NEW.PHY_FIND_QUES_4_AD_DETAIL) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_A) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_B) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_C) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_D) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_E) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_F) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_G) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_H) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_I) || '|' ||
to_char(:NEW.PHY_FIND_QUES_5_AD_DETAIL) || '|' ||
to_char(:NEW.IDM_QUES_6) || '|' ||
to_char(:NEW.IDM_QUES_6_A) || '|' ||
to_char(:NEW.IDM_QUES_6_B) || '|' ||
to_char(:NEW.IDM_QUES_6_AD_DETAIL) || '|' ||
to_char(:NEW.IDM_QUES_7) || '|' ||
to_char(:NEW.IDM_QUES_7_A) || '|' ||
to_char(:NEW.IDM_QUES_7_B) || '|' ||
to_char(:NEW.IDM_QUES_7_AD_DETAIL) || '|' ||
to_char(:NEW.IDM_QUES_8) || '|' ||
to_char(:NEW.IDM_QUES_8_AD_DETAIL) || '|' ||
to_char(:NEW.OTHER_QUES_9) || '|' ||
to_char(:NEW.OTHER_QUES_9_A) || '|' ||
to_char(:NEW.OTHER_QUES_9_B) || '|' ||
to_char(:NEW.OTHER_QUES_10) || '|' ||
to_char(:NEW.OTHER_QUES_10_AD_DETAIL) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.ENTITY_ID) || '|' ||
to_char(:NEW.ENTITY_TYPE);      
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,14,'14_CB_DUMN_BI0.sql',sysdate,'9.0.0 Build#629');

commit;