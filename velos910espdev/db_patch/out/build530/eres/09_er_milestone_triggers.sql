create or replace
TRIGGER "ERES".ER_MILESTONE_AD0 AFTER DELETE ON ER_MILESTONE
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_MILESTONE', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_milestone) || '|' ||
  to_char(:old.fk_study) || '|' ||
  :old.milestone_type || '|' ||
  to_char(:old.fk_cal) || '|' ||
  to_char(:old.fk_codelst_rule) || '|' ||
  to_char(:old.milestone_amount) || '|' ||
  to_char(:old.milestone_visit) || '|' ||
  to_char(:old.fk_eventassoc) || '|' ||
  to_char(:old.milestone_count) || '|' ||
  :old.milestone_delflag || '|' ||
  :old.milestone_usersto || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  to_char(:old.fk_codelst_milestone_stat) || '|' ||
  :old.milestone_description; --KM
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


create or replace TRIGGER ERES.ER_MILESTONE_AU0 AFTER UPDATE OF RID,FK_CAL,IP_ADD,CREATOR,FK_STUDY,CREATED_ON,PK_MILESTONE,FK_EVENTASSOC,MILESTONE_TYPE,FK_CODELST_RULE,MILESTONE_COUNT,MILESTONE_LIMIT,MILESTONE_VISIT,MILESTONE_AMOUNT,MILESTONE_PAYFOR,MILESTONE_STATUS,MILESTONE_DELFLAG,MILESTONE_PAYTYPE,MILESTONE_USERSTO,MILESTONE_PAYDUEBY,MILESTONE_PAYBYUNIT,MILESTONE_EVENTSTATUS ON ERES.ER_MILESTONE FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  Audit_Trail.record_transaction
    (raid, 'ER_MILESTONE', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);
  IF NVL(:OLD.pk_milestone,0) !=
     NVL(:NEW.pk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILESTONE',
       :OLD.pk_milestone, :NEW.pk_milestone);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.milestone_type,' ') !=
     NVL(:NEW.milestone_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_TYPE',
       :OLD.milestone_type, :NEW.milestone_type);
  END IF;
  IF NVL(:OLD.fk_cal,0) !=
     NVL(:NEW.fk_cal,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CAL',
       :OLD.fk_cal, :NEW.fk_cal);
  END IF;
  IF NVL(:OLD.fk_codelst_rule,0) !=
     NVL(:NEW.fk_codelst_rule,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_RULE',
       :OLD.fk_codelst_rule, :NEW.fk_codelst_rule);
  END IF;
  IF NVL(:OLD.milestone_amount,0) !=
     NVL(:NEW.milestone_amount,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_AMOUNT',
       :OLD.milestone_amount, :NEW.milestone_amount);
  END IF;
  IF NVL(:OLD.milestone_visit,0) !=
     NVL(:NEW.milestone_visit,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_VISIT',
       :OLD.milestone_visit, :NEW.milestone_visit);
  END IF;
  IF NVL(:OLD.fk_eventassoc,0) !=
     NVL(:NEW.fk_eventassoc,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_EVENTASSOC',
       :OLD.fk_eventassoc, :NEW.fk_eventassoc);
  END IF;
  IF NVL(:OLD.milestone_count,0) !=
     NVL(:NEW.milestone_count,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_COUNT',
       :OLD.milestone_count, :NEW.milestone_count);
  END IF;
  IF NVL(:OLD.milestone_delflag,' ') !=
     NVL(:NEW.milestone_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DELFLAG',
       :OLD.milestone_delflag, :NEW.milestone_delflag);
  END IF;
  IF NVL(:OLD.milestone_usersto,' ') !=
     NVL(:NEW.milestone_usersto,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_USERSTO',
       :OLD.milestone_usersto, :NEW.milestone_usersto);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
      to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.milestone_limit ,0) !=
     NVL(:NEW.milestone_limit ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_limit',
       :OLD.milestone_limit , :NEW.milestone_limit );
  END IF;

  IF NVL(:OLD.milestone_status,0) !=
     NVL(:NEW.milestone_status ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_status',
       :OLD.milestone_status , :NEW.milestone_status);
  END IF;
  IF NVL(:OLD.milestone_paytype,0) !=
     NVL(:NEW.milestone_paytype,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paytype',
       :OLD.milestone_paytype, :NEW.milestone_paytype);
  END IF;
   IF NVL(:OLD.milestone_paydueby,0) !=
     NVL(:NEW.milestone_paydueby,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paydueby',
       :OLD.milestone_paydueby, :NEW.milestone_paydueby);
  END IF;

  IF NVL(:OLD.milestone_paybyunit,' ') !=
     NVL(:NEW.milestone_paybyunit,' ') THEN
     Audit_Trail.column_update
       (raid, 'milestone_paybyunit',
       :OLD.milestone_paybyunit, :NEW.milestone_paybyunit);
  END IF;
   IF NVL(:OLD.milestone_payfor,0) !=
     NVL(:NEW.milestone_payfor,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_payfor',
       :OLD.milestone_payfor, :NEW.milestone_payfor);
  END IF;
     IF NVL(:OLD.milestone_eventstatus,0) !=
     NVL(:NEW.milestone_eventstatus,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_eventstatus',
       :OLD.milestone_eventstatus, :NEW.milestone_eventstatus);
  END IF;

  /*IF NVL(:OLD.milestone_achievedcount,0) !=
     NVL(:NEW.milestone_achievedcount,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_achievedcount',
       :OLD.milestone_achievedcount, :NEW.milestone_achievedcount);
  END IF;*/

  --Added by Manimaran for milestone_description field.
  IF NVL(:OLD.milestone_description,' ') !=
     NVL(:NEW.milestone_description,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DESCRIPTION',
       :OLD.milestone_description, :NEW.milestone_description);
  END IF;
 
  --Added by Manimaran for D-FIN7	
  IF NVL(:OLD.fk_codelst_milestone_stat,0) !=
     NVL(:NEW.fk_codelst_milestone_stat,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_MILESTONE_STAT',
       :OLD.fk_codelst_milestone_stat, :NEW.fk_codelst_milestone_stat);
  END IF;

END;
/


create or replace TRIGGER "ERES".ER_MILESTONE_BI0 BEFORE INSERT ON ER_MILESTONE
FOR EACH ROW
WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
 erid NUMBER(10);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction    (raid, 'ER_MILESTONE', erid, 'I', :NEW.LAST_MODIFIED_BY );
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FK_VISIT||'|'|| :NEW.PK_MILESTONE||'|'||
    :NEW.FK_STUDY||'|'||:NEW.MILESTONE_TYPE||'|'|| :NEW.FK_CAL||'|'||
    :NEW.FK_CODELST_RULE||'|'|| :NEW.MILESTONE_AMOUNT||'|'|| :NEW.MILESTONE_VISIT||'|'||
    :NEW.FK_EVENTASSOC||'|'|| :NEW.MILESTONE_COUNT||'|'||:NEW.MILESTONE_DELFLAG||'|'||
    :NEW.MILESTONE_USERSTO||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD   ||'|'||:NEW.milestone_limit ||'|'||:NEW.milestone_status
  ||'|'||:NEW.milestone_paytype ||'|'||:NEW.milestone_paydueby ||'|'||:NEW.milestone_paybyunit   ||'|'||:NEW.milestone_payfor ||'|'||:NEW.milestone_eventstatus
  ||'|'||:NEW.milestone_achievedcount
  ||'|'||TO_CHAR(:NEW.last_checked_on ,PKG_DATEUTIL.f_get_dateformat) ||'|'||:NEW.milestone_description
  ||'|'|| :NEW.fk_codelst_milestone_stat; --KM


 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

 END ;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,73,9,'09_er_milestone_triggers.sql',sysdate,'8.9.0 Build#530');

commit;