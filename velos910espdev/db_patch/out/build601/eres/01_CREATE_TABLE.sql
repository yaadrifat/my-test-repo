Set define off;

Create table CB_ANTIGEN
(
PK_ANTIGEN NUMBER(10) Primary Key,
ANTIGEN_ID NUMBER(10),
FK_HLA_CODE_ID NUMBER(10),
FK_HLA_METHOD_ID NUMBER(10),
VALID_IND VARCHAR2(1),
ACTIVE_IND VARCHAR2(1),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_ANTIGEN" IS 'Table to store HLA Antigens';
COMMENT ON COLUMN "CB_ANTIGEN"."PK_ANTIGEN" IS 'Primary key of the table';
COMMENT ON COLUMN "CB_ANTIGEN"."ANTIGEN_ID" IS 'This column will store the Antigen ID';
COMMENT ON COLUMN "CB_ANTIGEN"."FK_HLA_CODE_ID" IS 'This column will store the code related to antigen';
COMMENT ON COLUMN "CB_ANTIGEN"."FK_HLA_METHOD_ID" IS 'This column will store the method related to antigen';
COMMENT ON COLUMN "CB_ANTIGEN"."VALID_IND" IS 'This column will store that hla antigen is valid or not.';
COMMENT ON COLUMN "CB_ANTIGEN"."ACTIVE_IND" IS 'This column will store that hla antigen is active or not.';
COMMENT ON COLUMN "CB_ANTIGEN"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_ANTIGEN"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_ANTIGEN"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_ANTIGEN"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_ANTIGEN"."LAST_MODIFIED_ON" IS 'This column stores the date on which column last modified.'; 
COMMENT ON COLUMN "CB_ANTIGEN"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'; 
COMMENT ON COLUMN "CB_ANTIGEN"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE SEQ_CB_ANTIGEN MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

Create table CB_ANTIGEN_ENCOD
(
PK_ANTIGEN_ENCOD NUMBER(10) Primary Key,
FK_ANTIGEN NUMBER(10),
VERSION varchar2(50 BYTE),
SEARCH_FORMAT varchar2(50 BYTE),
GENOMIC_FORMAT varchar2(50 BYTE),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_ANTIGEN_ENCOD" IS 'Table to store HLA Antigens Encoding';
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."PK_ANTIGEN_ENCOD" IS 'Primary key of the table';
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."FK_ANTIGEN" IS 'This column will store the fk of CB_Antigen';
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."SEARCH_FORMAT" IS 'This column will store the Search Format of HLA antigen';
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."GENOMIC_FORMAT" IS 'This column will store the Genomic Format of HLA antigen.';
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."LAST_MODIFIED_ON" IS 'This column stores the date on which column last modified.'; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'; 
COMMENT ON COLUMN "CB_ANTIGEN_ENCOD"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE SEQ_CB_ANTIGEN_ENCOD MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;



CREATE TABLE "CB_CORD_CT_STATUS"
  (
    "PK_CORD_CT_STATUS" NUMBER(10,0),
    "ENTITY_ID"         NUMBER(10,0),
    "ENTITY_TYPE"       NUMBER(10,0),
    "HLA_TYPING_STATUS" VARCHAR2(1 BYTE),
    "HLA_TYPING_COMPLETION_DATE" DATE,
    "COMPLETED_BY"     VARCHAR2(100 BYTE),
    "IP_ADD"           VARCHAR2(15 BYTE),
    "RID"              NUMBER(10,0),
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "DELETEDFLAG" VARCHAR2(1 BYTE),
    "CREATOR"     NUMBER(10,0),
    "CREATED_ON" DATE,
"FK_REPORTING_CBB" NUMBER(10),
"FK_CO_CBB" NUMBER(10),
"HLA_METHOD" NUMBER(10)
  ) ;
COMMENT ON COLUMN "CB_CORD_CT_STATUS"."PK_CORD_CT_STATUS"
IS
  'Primary Key';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."ENTITY_ID"
IS
  'Id of the entity like ERU, Doner etc';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."ENTITY_TYPE"
IS
  'Type of entity';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."DELETEDFLAG"
IS
  'This column denotes whether record is deleted. ';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';

COMMENT ON COLUMN "CB_CORD_CT_STATUS"."FK_REPORTING_CBB" IS 'This column will store the fk of cbb reporting'; 
COMMENT ON COLUMN "CB_CORD_CT_STATUS"."FK_CO_CBB" IS 'This column will store the fk of CBB'; 
COMMENT ON COLUMN "CB_CORD_CT_STATUS"."HLA_METHOD" IS 'This column will store the fk of hla method'; 


--------------------------------CB_CORD_CT_STATUS Table Ends Here-------------------------------------

--------------------------------CB_CORD_CT_STATUS Sequence----------------------------------------

CREATE SEQUENCE "SEQ_CB_CORD_CT_STATUS" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

--------------------------------CB_CORD_CT_STATUS Sequence Ends Here-------------------------

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,144,1,'01_CREATE_TABLE.sql',sysdate,'9.0.0 Build#601');

commit;