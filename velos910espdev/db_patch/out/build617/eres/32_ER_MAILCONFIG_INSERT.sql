set define off;
Insert into ER_MAILCONFIG(
	PK_MAILCONFIG,
	MAILCONFIG_MAILCODE,
	MAILCONFIG_SUBJECT,
	MAILCONFIG_CONTENT,
	MAILCONFIG_CONTENTTYPE,
	MAILCONFIG_FROM,
	MAILCONFIG_BCC,
	MAILCONFIG_CC,
	DELETEDFLAG,
	FK_CODELSTORGID,
	RID,
	MAILCONFIG_HASATTACHEMENT,
	CREATED_ON,
	CREATOR,
	LAST_MODIFIED_DATE,
	LAST_MODIFIED_BY,
	IP_ADD) 
	values (SEQ_ER_MAILCONFIG.nextval,'Clinical',
	'Clinical Note for Review for CBU ID # [CBUID]',
	'Please Review the following clinical notes for CBU ID # [CBUID]<br/><a href="[URL]">[URL]</a>',
	'text/html',
	'admin@aithent.com',
	'admin@aithent.com',
	0,0,0,1,0,sysdate,null,sysdate,null,null
	);
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,32,'32_ER_MAILCONFIG_INSERT.sql',sysdate,'9.0.0 Build#617');

commit;