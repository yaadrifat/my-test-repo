set define off;

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_reason'
    AND codelst_subtyp = 'deffer';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Deferred' where codelst_type = 'note_reason'AND codelst_subtyp = 'deffer';
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,33,'33_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#617');

commit;