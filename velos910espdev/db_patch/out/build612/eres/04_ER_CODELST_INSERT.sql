set define off;

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'lab_code'
    AND codelst_subtyp = 'L811';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'lab_code','L811','NMDP Lab - 811','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

commit;

 Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'test_source','patient','Patient','N',null,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);

commit;


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_priority'
    AND codelst_subtyp = 'URG';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_priority','URG','Urgent','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_priority'
    AND codelst_subtyp = 'STN';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_priority','STN','Standard','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


DECLARE
 index_count number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'app_msg_status'
   AND codelst_subtyp = 'active_always';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'app_msg_status','active_always','Active (Always On)','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/

DECLARE
 index_count number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'app_msg_status'
   AND codelst_subtyp = 'active_usedates';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'app_msg_status','active_usedates','Active (Use Dates)','N',2,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/


DECLARE
 index_count number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'app_msg_status'
   AND codelst_subtyp = 'inactive';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'app_msg_status','inactive','Inactive','N',3,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/

DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'page_type'
   AND codelst_subtyp = 'login_page';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'page_type','login_page','Login Page','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'page_type'
   AND codelst_subtyp = 'landing_page';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'page_type','landing_page','Landing Page','N',2,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,155,4,'04_ER_CODELST_INSERT.sql',sysdate,'9.0.0 Build#612');

commit;