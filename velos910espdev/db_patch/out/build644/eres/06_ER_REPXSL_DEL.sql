set define off;

Delete from er_repxsl where pk_repxsl in (180,109,160,110,159,124,108);

commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,196,6,'06_ER_REPXSL_DEL.sql',sysdate,'9.1 Build#644');

commit;