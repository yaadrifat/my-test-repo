set define off;

delete from sch_msgtran where (FK_STUDY is null and FK_PATPROT is null) 
and fk_event is not null;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,196,4,'04_data_patch.sql',sysdate,'9.1 Build#644');

commit;