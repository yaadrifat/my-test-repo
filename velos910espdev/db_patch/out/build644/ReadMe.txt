/* This readMe is specific to Velos eResearch version 9.1 build #644 */

=====================================================================================================================================
eResearch:
1. There were changes in version numbering  of eResearch Application. Velos eResearch 9.0.1 is now renamed to 9.1. An Update Patch has been
provided along with this Build to make the changes in  track patches tables for all the builds that were released from Build#638 to Build#642.

=====================================================================================================================================




