--Package PKG_AUDIT_TRAIL_MODULE contains stored procedure for inserting rows in tables (AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE)
CREATE OR REPLACE PACKAGE  "ESCH"."PKG_AUDIT_TRAIL_MODULE" AS 
PROCEDURE SP_row_insert
    (p_rowid      IN NUMBER,
    p_tabname     IN VARCHAR2,
    p_rid         IN NUMBER,
    p_modulepk    IN NUMBER,
    p_act         IN VARCHAR2,
    p_userid      IN NUMBER);

PROCEDURE SP_column_insert
    (p_rowid        IN NUMBER,
    p_colname       IN VARCHAR2,
    p_oldval        IN VARCHAR2,
    p_newval        IN VARCHAR2,
    p_codelstid     IN NUMBER,
    p_remarks       IN VARCHAR2); 
END PKG_AUDIT_TRAIL_MODULE;


CREATE OR REPLACE PACKAGE BODY "ESCH"."PKG_AUDIT_TRAIL_MODULE" AS
  PROCEDURE SP_row_insert
    (p_rowid     IN NUMBER,
    p_tabname   IN VARCHAR2,
    p_rid       IN NUMBER,
  	p_modulepk  IN NUMBER,
    p_act       IN VARCHAR2,
    p_userid    IN NUMBER) AS
BEGIN
    INSERT INTO AUDIT_ROW_MODULE(PK_ROW_ID,TABLE_NAME,RID,MODULE_ID,ACTION,TIMESTAMP,USER_ID) VALUES (p_rowid, p_tabname, p_rid,p_modulepk,p_act,sysdate,p_userid);
	-- Return a scary message if for some reason the statement fails.
    IF sql%NOTFOUND THEN
      RAISE_APPLICATION_ERROR
        (-20000, 'Exception occurs while inserting data into the table AUDIT_ROW_MODULE');
    END IF;
 
  END SP_row_insert;

  PROCEDURE SP_column_insert
    (p_rowid        IN NUMBER,
    p_colname       IN VARCHAR2,
    p_oldval        IN VARCHAR2,
    p_newval        IN VARCHAR2,
    p_codelstid     IN NUMBER,
    p_remarks       IN VARCHAR2) AS   
  BEGIN
   INSERT INTO AUDIT_COLUMN_MODULE(PK_COLUMN_ID,FK_ROW_ID,COLUMN_NAME,OLD_VALUE,NEW_VALUE,FK_CODELST_REASON,REMARKS) VALUES (SEQ_AUDIT_COLUMN_MODULE.nextval,p_rowid, p_colname, p_oldval, p_newval,p_codelstid,p_remarks);
  -- Return a scary message if for some reason the statement fails.
    IF sql%NOTFOUND THEN
      RAISE_APPLICATION_ERROR
        (-20000, 'Exception occurs while inserting data into the table AUDIT_COLUMN_MODULE');
    END IF;
  END SP_column_insert;

END PKG_AUDIT_TRAIL_MODULE;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,2,'02_PKG_AUDIT_TRAIL_MODULE.sql',sysdate,'9.0.0 Build#606');

commit;