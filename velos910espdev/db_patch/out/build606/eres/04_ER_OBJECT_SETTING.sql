SET DEFINE OFF;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'ref_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'ref_menu', 'top_menu', 7, 
      0, 'References', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting ref_menu for top_menu already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'lnk_sops' and OBJECT_NAME = 'ref_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'lnk_sops', 'ref_menu', 2, 
     0, 'Link to SOPs', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting lnk_sops for ref_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'lnk_mops' and OBJECT_NAME = 'ref_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'lnk_mops', 'ref_menu', 3, 
      0, 'Link to MOPs', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting lnk_mops for ref_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'usr_guide' and OBJECT_NAME = 'ref_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'usr_guide', 'ref_menu', 4, 
    0, 'User Guide', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting usr_guide for ref_menu already exists');
  end if;
END;
/



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'help_lnk' and OBJECT_NAME = 'ref_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'help_lnk', 'ref_menu', 5, 
    0, 'Helpful Links', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting help_lnk for ref_menu already exists');
  end if;
END;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,4,'04_ER_OBJECT_SETTING.sql',sysdate,'9.0.0 Build#606');

commit;
