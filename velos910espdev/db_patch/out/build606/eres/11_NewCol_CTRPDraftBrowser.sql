set define off;

DECLARE
  v_item_exists number := 0;  
  browser_Pk number := 0;  
BEGIN
  select pk_browser into browser_Pk from ER_BROWSER where browser_module = 'ctrpDraftBrowser';

  select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='RESEARCH_TYPE';
  if (v_item_exists = 0) then
	  update ER_BROWSERCONF set BROWSERCONF_COLNAME = 'RESEARCH_TYPE', 
	  BROWSERCONF_SETTINGS = replace(replace(BROWSERCONF_SETTINGS,'CTRP_DRAFT_TYPE', 'RESEARCH_TYPE'),'Trail','Trial'),
	  BROWSERCONF_EXPLABEL = 'Trial Submission Category'
	  where fk_browser = browser_Pk 
	  and BROWSERCONF_COLNAME ='CTRP_DRAFT_TYPE';
	  
	  select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='CTRP_DRAFT_TYPE';
	  if (v_item_exists = 0) then
	     INSERT INTO ER_BROWSERCONF 
	     VALUES (SEQ_ER_BROWSERCONF.nextval, browser_Pk,'CTRP_DRAFT_TYPE',12,null,null);
	  end if;
	COMMIT;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,11,'11_NewCol_CTRPDraftBrowser.sql',sysdate,'9.0.0 Build#606');

commit;
