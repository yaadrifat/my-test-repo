set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftSearch' and codelst_subtyp = 'SWD';
	if (v_item_exists = 1) then
		
    update ER_CODELST set CODELST_SEQ=2,CODELST_DESC='Studies without Drafts'where CODELST_SUBTYP='SWD' and CODELST_TYPE='ctrpDraftSearch';
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SWD Updated');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SWD not exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftSearch' and codelst_subtyp = 'SD';
	if (v_item_exists = 1) then
		update ER_CODELST set CODELST_SEQ=1,CODELST_DESC='Studies with Drafts' where CODELST_SUBTYP='SD' and CODELST_TYPE='ctrpDraftSearch';
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SD inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SD not exists');
	end if;

COMMIT;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,1,'01_Update_ctrpdraftsearchCodelist.sql',sysdate,'9.0.0 Build#606');

commit;
