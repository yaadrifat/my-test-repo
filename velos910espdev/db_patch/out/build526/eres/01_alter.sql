alter table eres.ER_GRPS add fk_codelst_st_role number;

comment on column ER_GRPS.fk_codelst_st_role is 'This is the Foreign Key to Study Team ROLE code in er_codelst';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,69,1,'01_alter.sql',sysdate,'8.9.0 Build#526');

commit;




