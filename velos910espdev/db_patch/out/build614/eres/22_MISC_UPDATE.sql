set define off;

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'HGsAg' AND LABTEST_SHORTNAME = 'HG';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_LABTEST SET LABTEST_NAME = ''HBsAg'',LABTEST_SHORTNAME = ''HB'' where  LABTEST_NAME = ''HGsAg'' AND LABTEST_SHORTNAME = ''HG''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'Syphilis Confirmatory / Supplemental Test (FTA-ABS)' AND LABTEST_SHORTNAME = 'SYP_SUPP_FT';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_LABTEST SET LABTEST_NAME = ''Was the syphilis confirmatory test performed using a FDA-cleared confirmatory test (FTA-ABS)?'',LABTEST_SHORTNAME = ''FTA_ABS'' where  LABTEST_NAME = ''Syphilis Confirmatory / Supplemental Test (FTA-ABS)'' AND LABTEST_SHORTNAME = ''SYP_SUPP_FT''';
  end if;
end;
/
--END--

commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,22,'22_MISC_UPDATE.sql',sysdate,'9.0.0 Build#614');

commit;