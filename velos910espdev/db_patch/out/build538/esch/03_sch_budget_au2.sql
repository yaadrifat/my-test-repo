set define off;

CREATE OR REPLACE TRIGGER "ESCH"."SCH_BUDGET_AU2" 
AFTER UPDATE OF BUDGET_DELFLAG
ON SCH_BUDGET
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN

/*Author: Sonika Talwar
 *Date: June 18, 04
 *Purpose: to update the delete flag for all the budget calendars
 *Triggered when: Budget_delflag is set to 'Y'
*/

  IF NVL(:OLD.budget_delflag,' ') !=  NVL(:NEW.budget_delflag,' ') THEN
     IF  NVL(:NEW.budget_delflag,' ') = 'Y' THEN
     	pkg_bgt.sp_update_budgetTemplate (:NEW.BUDGET_CALENDAR,
        :NEW.LAST_MODIFIED_BY, :NEW.IP_ADD);
        
        pkg_bgt.sp_update_budget_children (:NEW.pk_budget,
        :NEW.LAST_MODIFIED_BY, :NEW.IP_ADD);
     END IF;
  END IF;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,81,3,'03_sch_budget_au2.sql',sysdate,'8.9.0 Build#538');

commit;
