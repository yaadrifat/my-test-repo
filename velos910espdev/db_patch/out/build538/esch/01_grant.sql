grant all on pkg_bgt to eres;

ALTER TABLE ESCH.EVENT_DEF
 ADD (COVERAGE_NOTES  VARCHAR2(4000 BYTE));

COMMENT ON COLUMN ESCH.EVENT_DEF.COVERAGE_NOTES IS 'This Column represents Coverage Notes of event';

ALTER TABLE ESCH.EVENT_ASSOC
 ADD (COVERAGE_NOTES  VARCHAR2(4000 BYTE));

COMMENT ON COLUMN ESCH.EVENT_ASSOC.COVERAGE_NOTES IS 'This Column represents Coverage Notes of event';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,81,1,'01_grant.sql',sysdate,'8.9.0 Build#538');

commit;
