DROP Trigger SCH_EVENTCOST_BD0;


create or replace PACKAGE BODY Pkg_Propgt_Evtupdates AS

	PROCEDURE SP_PROPAGATE_EVT_UPDATES(
	p_protocol_id IN NUMBER,
	p_selected_event_id IN NUMBER,
	p_table_name IN VARCHAR2,
	p_primary_keyvalue IN NUMBER,
	p_copy_to_same_visits IN CHAR,
	p_copy_to_same_events IN CHAR,
	p_mode IN CHAR DEFAULT 'M',
	p_calType IN CHAR DEFAULT 'P'
	) AS

	BEGIN
		DECLARE
	    v_event_list    Types.Number_array;
   	    v_event_copyflag_list    Types.SMALL_STRING_ARRAY;
		v_upd_event_id  NUMBER;
		i NUMBER := 1;
		v_source VARCHAR2(10) := 'DEF';

-- 		If the following union statement is not acceptable, then I could set the source (default value 'DEF') using the SQL
------------ select 'ASSOC' into source from event_assoc where event_id = selected_event_id; -------------------
-- Then use the source's value to decide which table to go to.

/* Modified by Sonia Abrol, 05/30/06, changed the logic to identify the child events. 'Name' was not a good idea because the propagation stopped if user changed the event name.
Instead used 'Cost' field because all events and its respctive child instances will have same 'cost' value
*/



	    CURSOR      similar_events_cursor IS
					SELECT e1.event_id, 'DEF' FROM EVENT_DEF e1
	                        WHERE e1.chain_id = p_protocol_id AND
							e1.COST = (SELECT COST  FROM EVENT_DEF e2 WHERE e2.event_id = p_selected_event_id)	AND
							e1.event_type = 'A'
							AND (displacement <>  0 or displacement is null) --KM,30Sep09
					UNION
					SELECT e1.event_id, 'ASSOC' FROM EVENT_ASSOC e1
	                        WHERE e1.chain_id = p_protocol_id AND
							e1.COST  = (SELECT COST FROM EVENT_ASSOC e2 WHERE e2.event_id = p_selected_event_id)	AND
							e1.event_type = 'A'
							AND (displacement <>  0 or displacement is null);

--modified by sonia to use fk_visit to find the events

	    CURSOR      same_visit_cursor IS
				SELECT e1.event_id, 'DEF' FROM EVENT_DEF e1
	                        WHERE e1.chain_id = p_protocol_id AND
							e1.fk_visit = (SELECT fk_visit FROM EVENT_DEF e2 WHERE e2.event_id = p_selected_event_id)	AND
							e1.event_type = 'A' AND
							(e1.displacement <> 0 or e1.displacement is null) --KM
				UNION
				SELECT e1.event_id, 'ASSOC' FROM EVENT_ASSOC e1
	                        WHERE e1.chain_id = p_protocol_id AND
							e1.fk_visit = (SELECT fk_visit FROM EVENT_ASSOC e2 WHERE e2.event_id = p_selected_event_id)	AND
							e1.event_type = 'A' AND
							(e1.displacement <> 0 or e1.displacement is null) ;



-- SV, REDTAG, do we need to check displacement =0 or > 0 here: The difference is the event definition from SelectEvent - Event def in a calendar
-- is updating records with displacement = 0 and the customization screen is updating those with displacement > 0 (attached to specific visits)
-- Actually it just struck me that "similar events" are those that have displacement = 0 and event name same.
-- Events in a single visit will be those that have the same  displacement (or visit_key as proposed in the new design).
-- if we go into such nitty-gritty then we may have to make the above cursor, dynamic. check eventdefDao.java. This has getProtSelectedEvents
-- for eventbrowser.jsp which is the SelectEvents screen and getAllProtSelectedEvents for customization screen.This actually distinguishes the two
-- types of fetches based on displacement = 0 or > 0.

-- modified by sonia abrol, 05/30/06, added displacement  <>  0 for  similar_events_cursor, because we need to update all child records and not the main record



		BEGIN

/*
		PLOG.DEBUG(pCTX , 'SP_PROPAGATE_EVT_UPDATES.. parameters:  ');
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_protocol_id' || p_protocol_id);
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_selected_event_id' || p_selected_event_id);
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_primary_keyvalue' || p_primary_keyvalue);
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_copy_to_same_visits ' || p_copy_to_same_visits );
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_copy_to_same_events ' || p_copy_to_same_events );
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_mode  ' || p_mode );
		PLOG.DEBUG(pCTX, 'SP_PROPAGATE_EVT_UPDATES.. parameters:  p_calType ' || p_calType ); */



			v_event_list := Types.NUMBER_ARRAY ();
			v_event_copyflag_list := Types.SMALL_STRING_ARRAY();

			 IF p_copy_to_same_events = 'Y' THEN
				 OPEN similar_events_cursor;
		    	 LOOP
				        FETCH similar_events_cursor INTO v_upd_event_id, v_source;
				        EXIT WHEN similar_events_cursor%NOTFOUND;

						v_event_list.EXTEND(1);
						v_event_list(i) := v_upd_event_id;

						IF (p_table_name = 'EVENT_DEF') THEN
							v_event_copyflag_list.EXTEND(1);
							v_event_copyflag_list(i) := 'E';
						END IF;

						i := i + 1;
			    END LOOP;
				CLOSE similar_events_cursor;

			END IF;


			 IF p_copy_to_same_visits = 'Y' THEN
				 OPEN same_visit_cursor;
		    	 LOOP
				        FETCH same_visit_cursor INTO v_upd_event_id, v_source;
				        EXIT WHEN same_visit_cursor%NOTFOUND;
-- we don't have to check if the event is already on the list, as events in a visit and events (with the same name)in a calendar are mutually exclusive.
						v_event_list.EXTEND(1);
						v_event_list(i) := v_upd_event_id;

						IF (p_table_name = 'EVENT_DEF') THEN
								v_event_copyflag_list.EXTEND(1);
								v_event_copyflag_list(i) := 'V';
						END IF;


						i := i + 1;

			    END LOOP;
				CLOSE same_visit_cursor;
			END IF;

-- Now that we have a list of events, we can call corresponding update functions to propagate changes at the table level.
-- We decided to use pseudo table name, so we can handle situations like resources and users which both end up in the same table
-- (sch_event_usr). So instead of passing additional type parm, this will be identified by pseudo table_names EVENT_ROLE, EVENT_USER
			IF (p_table_name = 'EVENT_DEF') THEN
			   	  sp_update_event_detail(p_selected_event_id,v_event_list, v_source, v_event_copyflag_list);
			ELSIF (p_table_name = 'EVENT_MESSAGE') THEN
			   	 	sp_update_event_message(p_selected_event_id,v_event_list, v_source);
			ELSIF (p_table_name = 'EVENT_COST')	THEN
				  	IF (p_mode = 'N') THEN
					   sp_add_event_cost(p_selected_event_id,v_event_list, p_primary_keyvalue);
					ELSIF (p_mode = 'M' OR p_mode = 'D') THEN
					   sp_update_event_cost(p_selected_event_id,v_event_list, p_primary_keyvalue,p_mode);
					END IF;
			ELSIF (p_table_name = 'EVENT_DOC')	THEN
				  	IF (p_mode = 'N') THEN
							    sp_add_event_appendix(p_selected_event_id,v_event_list, p_primary_keyvalue);
					ELSIF (p_mode = 'M' OR p_mode = 'D') THEN
			    		  		 sp_update_event_appendix(p_selected_event_id,v_event_list, p_primary_keyvalue,p_mode);
					END IF;
			ELSIF (p_table_name = 'EVENT_CRF')	THEN
				  	IF (p_mode = 'N') THEN
					 	sp_add_event_crflib(p_selected_event_id,v_event_list, p_primary_keyvalue);
					ELSIF (p_mode = 'M' OR p_mode = 'D') THEN
   				   	 	sp_update_event_crflib(p_selected_event_id,v_event_list, p_primary_keyvalue,p_mode);
					END IF;

			ELSIF (p_table_name = 'EVENT_USER') THEN
				  IF (p_mode = 'D')  THEN
				  						   sp_manage_event_user(p_selected_event_id,v_event_list, p_primary_keyvalue, 'R', p_mode );
					ELSE
										   sp_add_event_resource(p_selected_event_id,v_event_list, p_primary_keyvalue, 'R');
					END IF;
			ELSIF (p_table_name = 'EVENT_USER_USER') THEN -- for users
			   sp_manage_event_user(p_selected_event_id,v_event_list, p_primary_keyvalue, 'U', p_mode );
			ELSIF (p_table_name = 'EVENT_MSG_USER') THEN -- for users
			   sp_manage_event_user(p_selected_event_id,v_event_list, p_primary_keyvalue, 'S',  p_mode);

			ELSIF (p_table_name = 'EVENT_CRF_FORM') THEN -- for CRF forms
			   	  sp_add_event_crf_form(p_selected_event_id,v_event_list);
			ELSIF (p_table_name = 'SCH_EVENT_KIT') THEN -- for event storage kits

			 IF (p_mode = 'D')  THEN
			 	sp_delete_event_kit(p_selected_event_id,v_event_list,p_primary_keyvalue);
			 else
			   	  sp_add_event_kit(p_selected_event_id,v_event_list);
			 end if;

			END IF;

		END ;
COMMIT;

	END SP_PROPAGATE_EVT_UPDATES;


	PROCEDURE sp_update_event_detail(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, SOURCE IN VARCHAR2, p_target_eventcopyflag_list IN Types.SMALL_STRING_ARRAY)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_name VARCHAR2(4000);--KM-18Sep09
		v_duration NUMBER;
		v_fuzzy_period VARCHAR2(10);
		v_description VARCHAR2(200);
		v_notes VARCHAR2(1000);
		v_SERVICE_SITE_ID NUMBER;
		v_FACILITY_ID NUMBER;
		v_FK_CODELST_COVERTYPE NUMBER;
		v_event_flag CHAR(1);
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_duration_unit CHAR(1);
		v_event_fuzzyafter VARCHAR2(10);
		v_event_durationafter CHAR(1);
		v_event_cptcode  VARCHAR2(255);
		v_event_durationbefore CHAR(1);
		v_copyflag VARCHAR2(2);
                          v_offline_flag number;
		BEGIN
--
-- SV, 9/17/04, to test the update statements scope, update a non-used field to update to avoid further mishap to event_def.
--Modified by Sonia Abrol, 05/30/06, to solve event propagation issues

   	   			v_selected_event_id := p_source_event_id;


	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP


					v_copyflag := p_target_eventcopyflag_list(i);
					--PLOG.DEBUG(pCTX , 'SP_PROPAGATE_EVT_UPDATES.. v_copyflag :' || v_copyflag  || '*');

					v_upd_event_id := p_target_event_list(i);


					IF (SOURCE = 'DEF') THEN

					   SELECT NAME, DURATION,
					   FUZZY_PERIOD, DESCRIPTION, NOTES,
					   SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,
					   EVENT_FLAG,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore
					   INTO v_name, v_duration,
					   v_fuzzy_period, v_description, v_notes,
					   v_SERVICE_SITE_ID, v_FACILITY_ID, v_FK_CODELST_COVERTYPE,
					   v_event_flag,	v_last_modified_by, v_last_modified_date, v_ip_add, v_duration_unit,
				   		v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore
					   FROM EVENT_DEF
					   WHERE event_id = v_selected_event_id;

--  SV : copying name may not be a good idea?? ,
-- SA - We need to copy the names too
-- SA, 10/30/06 - commented update of nam and description, requirement# C3

						UPDATE EVENT_DEF SET
						NAME = DECODE(v_copyflag,'E',v_name,NAME),
						 DURATION = v_duration,
						 FUZZY_PERIOD = v_fuzzy_period,
						 DESCRIPTION = DECODE(v_copyflag,'E',v_description,DESCRIPTION),
						 NOTES = v_notes,
						 SERVICE_SITE_ID = v_SERVICE_SITE_ID,
						 FACILITY_ID = v_FACILITY_ID,
						 FK_CODELST_COVERTYPE = v_FK_CODELST_COVERTYPE,
						 EVENT_FLAG = v_event_flag,
						 LAST_MODIFIED_BY = v_last_modified_by,
						 LAST_MODIFIED_DATE = v_last_modified_date,
						 IP_ADD = v_ip_add,
						 DURATION_UNIT = v_duration_unit,
						 event_fuzzyafter = v_event_fuzzyafter,
						 event_durationafter = v_event_durationafter,
						 event_cptcode = v_event_cptcode,
						 event_durationbefore = v_event_durationbefore
						 WHERE event_id = v_upd_event_id   ;

					ELSE

					   SELECT NAME, DURATION,
					   FUZZY_PERIOD, DESCRIPTION, NOTES,
					   SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,
					   EVENT_FLAG,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DURATION_UNIT, event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore
					   INTO v_name, v_duration,
					   v_fuzzy_period, v_description, v_notes,
					   v_SERVICE_SITE_ID, v_FACILITY_ID, v_FK_CODELST_COVERTYPE,
					   v_event_flag,	v_last_modified_by, v_last_modified_date, v_ip_add, v_duration_unit,
	   				   v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore
					   FROM EVENT_ASSOC
					   WHERE event_id = v_selected_event_id;


						UPDATE EVENT_ASSOC SET
						NAME = DECODE(v_copyflag,'E',v_name,NAME),
						 DURATION = v_duration,
						 FUZZY_PERIOD = v_fuzzy_period,
						 DESCRIPTION = DECODE(v_copyflag,'E',v_description,DESCRIPTION),
						 NOTES = v_notes,
						 SERVICE_SITE_ID = v_SERVICE_SITE_ID,
						 FACILITY_ID = v_FACILITY_ID,
						 FK_CODELST_COVERTYPE = v_FK_CODELST_COVERTYPE,
						 EVENT_FLAG = v_event_flag,
						 LAST_MODIFIED_BY = v_last_modified_by,
						 LAST_MODIFIED_DATE = SYSDATE,
						 IP_ADD = v_ip_add,
						 DURATION_UNIT = v_duration_unit,
						 event_fuzzyafter = v_event_fuzzyafter,
						 event_durationafter = v_event_durationafter,
						 event_cptcode = v_event_cptcode,
						 event_durationbefore = v_event_durationbefore
					 WHERE event_id = v_upd_event_id   ;
--JM: Modified on 12Jan2009 added offline_flag = 3, based on this flag in the events1 table the event name can be propagated...
                                         update event_assoc set offline_flag = 3 where event_id = v_upd_event_id and offline_flag = 1;

					END IF;

				END LOOP;


		END ;

	END sp_update_event_detail;


	PROCEDURE sp_update_event_message(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, SOURCE IN VARCHAR2)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;

  		v_event_flag VARCHAR2(10);
		v_pat_daysbefore NUMBER;
		v_pat_daysafter NUMBER;
		v_usr_daysbefore NUMBER;
		v_usr_daysafter NUMBER;
		v_pat_msgbefore VARCHAR2(4000);
		v_pat_msgafter VARCHAR2(4000);
		v_usr_msgbefore VARCHAR2(4000);
		v_usr_msgafter VARCHAR2(4000);
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);

		BEGIN
   	   			v_selected_event_id := p_source_event_id;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP
				v_upd_event_id := p_target_event_list(i);

				IF (SOURCE = 'DEF') THEN
					SELECT EVENT_FLAG, PAT_DAYSBEFORE,
					PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER,
					PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE,
					USR_MSGAFTER, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD
					INTO  v_event_flag, v_pat_daysbefore,
					v_pat_daysafter, v_usr_daysbefore, v_usr_daysafter,
					v_pat_msgbefore, v_pat_msgafter, v_usr_msgbefore,
					v_usr_msgafter, v_last_modified_by, v_last_modified_date, v_ip_add
					FROM EVENT_DEF
					WHERE event_id = v_selected_event_id ;

					UPDATE EVENT_DEF SET
--						EVENT_FLAG = v_event_flag,
						PAT_DAYSBEFORE = v_pat_daysbefore,
						PAT_DAYSAFTER = v_pat_daysafter,
						USR_DAYSBEFORE = v_usr_daysbefore,
						USR_DAYSAFTER = v_usr_daysafter,
						PAT_MSGBEFORE = v_pat_msgbefore,
						PAT_MSGAFTER = v_pat_msgafter,
						USR_MSGBEFORE = v_usr_msgbefore,
						USR_MSGAFTER = v_usr_msgafter,
						LAST_MODIFIED_BY = v_last_modified_by,
						LAST_MODIFIED_DATE = v_last_modified_date,
						IP_ADD = v_ip_add
					WHERE event_id = v_upd_event_id   ;
				ELSE
						SELECT EVENT_FLAG, PAT_DAYSBEFORE,
						PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER,
						PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE,
						USR_MSGAFTER, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD
						INTO  v_event_flag, v_pat_daysbefore,
						v_pat_daysafter, v_usr_daysbefore, v_usr_daysafter,
						v_pat_msgbefore, v_pat_msgafter, v_usr_msgbefore,
						v_usr_msgafter, v_last_modified_by, v_last_modified_date, v_ip_add
						FROM EVENT_ASSOC
						WHERE event_id = v_selected_event_id ;

						UPDATE EVENT_ASSOC SET
--						EVENT_FLAG = event_flag,
						PAT_DAYSBEFORE = v_pat_daysbefore,
						PAT_DAYSAFTER = v_pat_daysafter,
						USR_DAYSBEFORE = v_usr_daysbefore,
						USR_DAYSAFTER = v_usr_daysafter,
						PAT_MSGBEFORE = v_pat_msgbefore,
						PAT_MSGAFTER = v_pat_msgafter,
						USR_MSGBEFORE = v_usr_msgbefore,
						USR_MSGAFTER = v_usr_msgafter,
						LAST_MODIFIED_BY = v_last_modified_by,
						LAST_MODIFIED_DATE = v_last_modified_date,
						IP_ADD = v_ip_add
					WHERE event_id = v_upd_event_id   ;
				END IF;

					   DBMS_OUTPUT.PUT_LINE(v_upd_event_id);
					   DBMS_OUTPUT.PUT_LINE('sql code=' || SQL%ROWCOUNT);
				END LOOP;


		END ;

	END sp_update_event_message;


	PROCEDURE sp_add_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;

		v_next_pk_eventcost NUMBER;
		v_event_cost_value NUMBER(15,3);
	 	v_fk_event_id NUMBER;
		v_fk_cost_desc NUMBER;
		v_fk_currency NUMBER;
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_pk_eventcost NUMBER;
		BEGIN
--
-- SV, 9/17/04, to test the update statements scope, update a non-used field to update to avoid further mishap to event_def.
   	   			v_selected_event_id := p_source_event_id;
				v_pk_eventcost := p_pk_eventcost;

				SELECT EVENTCOST_VALUE,
				FK_COST_DESC, FK_CURRENCY,
				LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
				IP_ADD
				INTO v_event_cost_value,
				v_fk_cost_desc, v_fk_currency,
				v_last_modified_by,
				v_last_modified_date,v_ip_add
				FROM SCH_EVENTCOST
				WHERE FK_EVENT = v_selected_event_id
				AND pk_eventcost = v_pk_eventcost;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP

					v_fk_event_id := p_target_event_list(i);

					IF (v_fk_event_id <> v_selected_event_id) THEN
						SELECT sch_eventcost_seq.NEXTVAL INTO v_next_pk_eventcost  FROM dual;

						INSERT INTO SCH_EVENTCOST (
						PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT,
						FK_COST_DESC, FK_CURRENCY,
						LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
						IP_ADD, PROPAGATE_FROM)
						VALUES (v_next_pk_eventcost, v_event_cost_value, v_fk_event_id,
						v_fk_cost_desc, v_fk_currency,v_last_modified_by, v_last_modified_date,
						v_ip_add, v_pk_eventcost);
					ELSE
						-- SV, 10/20/04, update the original record's propagate_from field with it's id as well, so we can identify all of them together.
						UPDATE SCH_EVENTCOST SET
						PROPAGATE_FROM = v_pk_eventcost
						WHERE FK_EVENT = v_selected_event_id
						AND pk_eventcost = v_pk_eventcost;

					END IF;
				END LOOP;


		END ;

	END sp_add_event_cost;


PROCEDURE sp_update_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER, p_mode IN CHAR)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;

		v_next_pk_eventcost NUMBER;
		v_event_cost_value NUMBER(15,3);
	 	v_fk_event_id NUMBER;
		v_fk_cost_desc NUMBER;
		v_fk_currency NUMBER;
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_pk_eventcost NUMBER;
		v_propagate_from NUMBER;

		v_cost_desc NUMBER;
		v_name Varchar2(4000);
		v_protocol Number;
		v_visit Number;
		v_line_category number;
		v_cost_type number default 0;
		BEGIN
--
-- SV, 9/17/04, to test the update statements scope, update a non-used field to update to avoid further mishap to event_def.
   	   			v_selected_event_id := p_source_event_id;
				v_pk_eventcost := p_pk_eventcost;

				IF p_mode = 'M' THEN
								SELECT  EVENTCOST_VALUE,
								FK_COST_DESC, FK_CURRENCY,
								LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
								IP_ADD, PROPAGATE_FROM
								INTO v_event_cost_value,
								v_fk_cost_desc, v_fk_currency,
								v_last_modified_by,
								v_last_modified_date,v_ip_add, v_propagate_from
								FROM SCH_EVENTCOST
								WHERE FK_EVENT = v_selected_event_id
								AND pk_eventcost = v_pk_eventcost;

								-- case when record was updated earlier without updating propagate from field.
								IF ( NVL(v_propagate_from, 0) = 0) THEN
								   v_propagate_from := v_pk_eventcost;
									UPDATE SCH_EVENTCOST SET
									PROPAGATE_FROM = v_propagate_from
									WHERE FK_EVENT = v_selected_event_id
									AND pk_eventcost = v_pk_eventcost;

								END IF;


					   			FOR i IN 1..p_target_event_list.COUNT
								LOOP
									v_fk_event_id := p_target_event_list(i);

									IF (v_fk_event_id <> v_selected_event_id) THEN
										UPDATE SCH_EVENTCOST SET
										EVENTCOST_VALUE =  v_event_cost_value,
										FK_COST_DESC = v_fk_cost_desc,
										FK_CURRENCY = v_fk_currency,
										LAST_MODIFIED_BY = v_last_modified_by,
										LAST_MODIFIED_DATE = v_last_modified_date,
										IP_ADD = v_ip_add
										WHERE  (PROPAGATE_FROM = 	v_propagate_from )
										AND (FK_EVENT = v_fk_event_id);

										IF (SQL%NOTFOUND) THEN

											SELECT sch_eventcost_seq.NEXTVAL INTO v_next_pk_eventcost  FROM dual;

											INSERT INTO SCH_EVENTCOST (
											PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT,
											FK_COST_DESC, FK_CURRENCY,
											LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
											IP_ADD, PROPAGATE_FROM)
											VALUES (v_next_pk_eventcost, v_event_cost_value, v_fk_event_id,
											v_fk_cost_desc, v_fk_currency,v_last_modified_by, v_last_modified_date,
											v_ip_add, v_propagate_from);

										END IF;

									END IF;
								END LOOP;
 			ELSIF p_mode = 'D' THEN

	   	   	   	SELECT  PROPAGATE_FROM, FK_COST_DESC
				INTO v_propagate_from, v_cost_desc
				FROM SCH_EVENTCOST
				WHERE FK_EVENT = v_selected_event_id
				AND pk_eventcost = v_pk_eventcost;

				IF ( NVL(v_propagate_from, 0) = 0) THEN
					v_propagate_from := v_pk_eventcost;
				END IF;

				plog.debug(pctx, 'main cost pk '||v_pk_eventcost);
				
				select name, chain_id, fk_visit, event_line_category
				into v_name,v_protocol,v_visit, v_line_category
				from event_assoc
				where event_id = v_selected_event_id and event_type = 'A' and nvl(displacement,-99999999) <> 0;

				--delete the main cost
				DELETE FROM SCH_EVENTCOST WHERE pk_eventcost  = v_pk_eventcost;

				delete from sch_lineitem where pk_lineitem in (select pk_lineitem
				from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
				where l.fk_event = v_selected_event_id 
				and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
				and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget
				and budget_calendar = v_protocol
				and l.FK_CODELST_COST_TYPE = v_cost_desc
				and rownum > 2);

				SELECT  pk_codelst  INTO v_line_category FROM  SCH_CODELST
				WHERE trim (codelst_type) = 'category' AND trim (codelst_subtyp) = 'ctgry_patcare';
				
				SELECT pk_codelst INTO v_cost_type FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
				AND LOWER(codelst_subtyp)='res_cost';
				
				FOR i IN 1..p_target_event_list.COUNT
				LOOP
					v_fk_event_id := p_target_event_list(i);

					plog.debug(pctx, 'cost delete event pk '||v_fk_event_id);

					DELETE FROM SCH_EVENTCOST WHERE  FK_EVENT = v_fk_event_id  AND
					propagate_from = v_propagate_from;

					select name, fk_visit
					into v_name,v_visit
					from event_assoc
					where event_id = v_fk_event_id and event_type = 'A' and nvl(displacement,-99999999) <> 0;

					delete from sch_lineitem where pk_lineitem in (select l.pk_lineitem 
					from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
					where l.fk_event in (select cst.FK_EVENT FROM SCH_EVENTCOST cst WHERE cst.propagate_from = v_propagate_from and cst.PK_EVENTCOST <> cst.propagate_from)
					and l.fk_bgtsection = pk_budgetsec and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  
					and pk_budget = fk_budget
					and budget_calendar = v_protocol
					and l.FK_CODELST_COST_TYPE = v_cost_desc
					and rownum > 2);
					
					Update sch_lineitem 
					SET LINEITEM_SPONSORUNIT ='0.0', LINEITEM_CLINICNOFUNIT='1.00',LINEITEM_OTHERCOST='0.0',
					LINEITEM_STDCARECOST=null, LINEITEM_RESCOST=null,
					LINEITEM_INVCOST=null, LINEITEM_INCOSTDISC='0.0', 
					FK_CODELST_CATEGORY = v_line_category,
					FK_CODELST_COST_TYPE = v_cost_type
					where pk_lineitem in (select pk_lineitem
					from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
					where l.fk_event = v_fk_event_id 
					and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
					and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget
					and budget_calendar = v_protocol
					and l.FK_CODELST_COST_TYPE = v_cost_desc);
				END LOOP;

				--After cost deletion, if there is only one lineitem left for fk_event = v_selected_event_id
				--update that with default values.
				Update sch_lineitem 
				SET LINEITEM_SPONSORUNIT ='0.0', LINEITEM_CLINICNOFUNIT='1.00',LINEITEM_OTHERCOST='0.0',
				LINEITEM_STDCARECOST=null, LINEITEM_RESCOST=null,
				LINEITEM_INVCOST=null, LINEITEM_INCOSTDISC='0.0', 
				FK_CODELST_CATEGORY = v_line_category,
				FK_CODELST_COST_TYPE = v_cost_type
				where pk_lineitem in (select pk_lineitem
				from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
				where l.fk_event = v_selected_event_id 
				and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
				and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget
				and budget_calendar = v_protocol
				and l.FK_CODELST_COST_TYPE = v_cost_desc);

			END IF;

		END ;

	END sp_update_event_cost;


	PROCEDURE sp_add_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_next_pk_eventdoc NUMBER;
		v_next_pk_doc NUMBER;
		v_pk_docs NUMBER;
	 	v_fk_event_id NUMBER;
		v_doc_name VARCHAR2(255);
		v_doc_desc VARCHAR2(2000);
		v_creator  NUMBER;
		v_last_modified_by NUMBER;
		v_pk_eventdoc NUMBER;
		v_ip_add VARCHAR2(15);
		v_doc_size NUMBER;
		v_doc_type CHAR;
		v_doc BLOB;

		BEGIN
   	   			v_selected_event_id := p_source_event_id;
				v_pk_docs := p_pk_docs;

				/*SELECT LAST_MODIFIED_BY,
   				LAST_MODIFIED_DATE, IP_ADD
				INTO v_last_modified_by,
				v_last_modified_date,	v_ip_add
				FROM SCH_EVENTDOC
				WHERE FK_EVENT = v_selected_event_id
				and PK_DOCS = v_pk_docs;
				*/
   				SELECT  evdoc.PK_EVENTDOC, doc.DOC_NAME, doc.DOC_DESC, doc.doc,
   				doc.DOC_TYPE,
   				doc.CREATOR, doc.LAST_MODIFIED_BY,
   				doc.IP_ADD, doc.DOC_SIZE
				INTO  v_pk_eventdoc, v_doc_name, v_doc_desc, v_doc, v_doc_type, v_creator, v_last_modified_by, v_ip_add, v_doc_size
   				FROM SCH_DOCS doc, SCH_EVENTDOC evdoc
   				WHERE doc.pk_docs = evdoc.pk_docs
   				AND   evdoc.fk_event = v_selected_event_id
   				AND  doc.pk_docs = v_pk_docs ;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP

					v_fk_event_id := p_target_event_list(i);

					IF (v_fk_event_id <> v_selected_event_id) THEN

					SELECT sch_eventdoc_seq.NEXTVAL INTO v_next_pk_eventdoc  FROM dual;
--SV, 10/21/04, fix for problem in propagating
					SELECT sch_docs_seq.NEXTVAL INTO v_next_pk_doc  FROM dual;

						INSERT INTO SCH_DOCS (
						   PK_DOCS, DOC_NAME, DOC_DESC, DOC,
						   DOC_TYPE,
						   CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
						   CREATED_ON, IP_ADD, DOC_SIZE)
						VALUES ( v_next_pk_doc, v_doc_name, v_doc_desc, v_doc,
						    v_doc_type, v_creator, v_last_modified_by, SYSDATE, SYSDATE, v_ip_add, v_doc_size);

						INSERT INTO SCH_EVENTDOC (
						PK_EVENTDOC, PK_DOCS, FK_EVENT,
	   					LAST_MODIFIED_BY,
	   					LAST_MODIFIED_DATE, IP_ADD, PROPAGATE_FROM)
						VALUES (v_next_pk_eventdoc, v_next_pk_doc, v_fk_event_id,v_last_modified_by,
						SYSDATE, v_ip_add, v_pk_eventdoc);
					ELSE
						-- SV, 10/20/04, update the original record's propagate_from field with it's id as well, so we can identify all of them together.
						UPDATE SCH_EVENTDOC SET
						PROPAGATE_FROM = v_pk_eventdoc
						WHERE FK_EVENT = v_selected_event_id
						AND PK_EVENTDOC = v_pk_eventdoc;

					END IF;


				END LOOP;


		END ;

	END sp_add_event_appendix;



	PROCEDURE sp_update_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER,p_mode IN CHAR)

	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_next_pk_eventdoc NUMBER;
		v_next_pk_doc NUMBER;
		v_pk_docs NUMBER;
	 	v_fk_event_id NUMBER;
		v_doc_name VARCHAR2(255);
		v_doc_desc VARCHAR2(2000);
		v_creator  NUMBER;
		v_last_modified_by NUMBER;
		v_pk_eventdoc NUMBER;
		v_ip_add VARCHAR2(15);
		v_doc_size NUMBER;
		v_doc_type CHAR;
		v_doc BLOB;
		v_propagate_from NUMBER;

		BEGIN
   	   			v_selected_event_id := p_source_event_id;
				v_pk_docs := p_pk_docs;

				IF  (p_mode = 'M')  THEN
		   				SELECT  evdoc.PK_EVENTDOC, doc.DOC_NAME, doc.DOC_DESC, doc.doc,
		   				doc.DOC_TYPE,
		   				doc.CREATOR, doc.LAST_MODIFIED_BY,
		   				doc.IP_ADD, doc.DOC_SIZE, PROPAGATE_FROM
						INTO  v_pk_eventdoc, v_doc_name, v_doc_desc, v_doc, v_doc_type,
						v_creator, v_last_modified_by, v_ip_add, v_doc_size, v_propagate_from
		   				FROM SCH_DOCS doc, SCH_EVENTDOC evdoc
		   				WHERE doc.pk_docs = evdoc.pk_docs
		   				AND   evdoc.fk_event = v_selected_event_id
		   				AND  doc.pk_docs = v_pk_docs ;

						-- SV, 10/27/04, case when record was updated earlier without propagation and now it's getting propagated in edit mode.
						IF ( NVL(v_propagate_from, 0) = 0) THEN
						   v_propagate_from := v_pk_eventdoc;
							UPDATE SCH_EVENTDOC SET
							PROPAGATE_FROM = v_pk_eventdoc
							WHERE FK_EVENT = v_selected_event_id
							AND PK_EVENTDOC = v_pk_eventdoc;

						END IF;


			   			FOR i IN 1..p_target_event_list.COUNT
						LOOP
							v_fk_event_id := p_target_event_list(i);

							IF (v_fk_event_id <> v_selected_event_id) THEN

								UPDATE SCH_DOCS SET
								   DOC_NAME = v_doc_name, DOC_DESC = v_doc_desc, DOC = v_doc,
								   DOC_TYPE = v_doc_type, LAST_MODIFIED_BY = v_last_modified_by,
								   LAST_MODIFIED_DATE = SYSDATE,
								   IP_ADD = v_ip_add, DOC_SIZE = v_doc_size
								WHERE PK_DOCS IN
									  (SELECT doc.PK_DOCS
									  		  FROM SCH_EVENTDOC evdoc, SCH_DOCS doc
									  		  WHERE evdoc.PK_DOCS = doc.PK_DOCS AND
											  evdoc.FK_EVENT = v_fk_event_id
											  AND PROPAGATE_FROM = 	v_propagate_from
										);

								IF (SQL%FOUND) THEN
									UPDATE SCH_EVENTDOC SET
										LAST_MODIFIED_BY = v_last_modified_by,
									   LAST_MODIFIED_DATE = SYSDATE,
									   IP_ADD = v_ip_add
									WHERE FK_EVENT = v_selected_event_id
									AND PK_EVENTDOC IN
									(SELECT evdoc.PK_EVENTDOC
											FROM SCH_EVENTDOC evdoc, SCH_DOCS doc
											WHERE evdoc.PK_DOCS = doc.PK_DOCS AND
											evdoc.FK_EVENT = v_fk_event_id
											AND PROPAGATE_FROM = 	v_propagate_from
									);

								ELSE --NOT FOUND

								SELECT sch_eventdoc_seq.NEXTVAL INTO v_next_pk_eventdoc  FROM dual;
			--SV, 10/21/04, fix for problem in propagating
								SELECT sch_docs_seq.NEXTVAL INTO v_next_pk_doc  FROM dual;

									INSERT INTO SCH_DOCS (
									   PK_DOCS, DOC_NAME, DOC_DESC, DOC,
									   DOC_TYPE,
									   CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
									   CREATED_ON, IP_ADD, DOC_SIZE)
									VALUES ( v_next_pk_doc, v_doc_name, v_doc_desc, v_doc,
									    v_doc_type, v_creator, v_last_modified_by, SYSDATE, SYSDATE, v_ip_add, v_doc_size);

									INSERT INTO SCH_EVENTDOC (
									PK_EVENTDOC, PK_DOCS, FK_EVENT,
				   					LAST_MODIFIED_BY,
				   					LAST_MODIFIED_DATE, IP_ADD, PROPAGATE_FROM)
									VALUES (v_next_pk_eventdoc, v_next_pk_doc, v_fk_event_id,v_last_modified_by,
									SYSDATE, v_ip_add, v_propagate_from);
									END IF; --if not found

								END IF;
						END LOOP;
		 ELSIF p_mode= 'D' THEN
		 	   		 SELECT  PROPAGATE_FROM
						INTO  v_propagate_from
		   				FROM SCH_DOCS doc, SCH_EVENTDOC evdoc
		   				WHERE doc.pk_docs = v_pk_docs AND doc.pk_docs = evdoc.pk_docs
		   				AND   evdoc.fk_event = v_selected_event_id;

						IF ( NVL(v_propagate_from, 0) = 0) THEN
										   v_propagate_from := v_pk_docs;
						END IF;

						-- delete main record

						   DELETE FROM SCH_EVENTDOC  evdoc WHERE evdoc.fk_event = v_selected_event_id AND evdoc.pk_docs = v_pk_docs ;

							DELETE FROM SCH_DOCS WHERE pk_docs = v_pk_docs ;




								 FOR i IN 1..p_target_event_list.COUNT
								 				LOOP

															v_fk_event_id := p_target_event_list(i);

															DELETE FROM SCH_DOCS WHERE pk_docs IN ( SELECT pk_docs FROM SCH_EVENTDOC WHERE  FK_EVENT = v_fk_event_id  AND
															  propagate_from = v_propagate_from);

															 DELETE FROM SCH_EVENTDOC WHERE  FK_EVENT = v_fk_event_id  AND
															  propagate_from = v_propagate_from;



											END LOOP;


		END IF;
	END ;

	END sp_update_event_appendix;




	PROCEDURE sp_add_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_crflib_number VARCHAR2(50);
		v_crflib_name VARCHAR2(100);
		v_crflib_flag CHAR(1);
		v_next_pk_eventcrflib NUMBER;
	 	v_fk_event_id NUMBER;
		v_fk_cost_desc NUMBER;
		v_fk_currency NUMBER;
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_crflib_form_flag NUMBER;
		v_crflib_id NUMBER;
		v_propagate_from NUMBER;
		BEGIN
   	   			v_selected_event_id := p_source_event_id;
				v_crflib_id := pk_crflib;

				SELECT CRFLIB_NUMBER,
				CRFLIB_NAME, CRFLIB_FLAG,
   				LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
   				IP_ADD, CRFLIB_FORMFLAG
				INTO v_crflib_number, v_crflib_name, v_crflib_flag,
				v_last_modified_by,v_last_modified_date,
				v_ip_add, v_crflib_form_flag
				FROM SCH_CRFLIB
				WHERE FK_EVENTS = v_selected_event_id
				AND pk_crflib = v_crflib_id;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP

					v_fk_event_id := p_target_event_list(i);

					IF (v_fk_event_id <> v_selected_event_id) THEN

						SELECT seq_sch_crflib.NEXTVAL INTO v_next_pk_eventcrflib  FROM dual;

						INSERT INTO SCH_CRFLIB (
						PK_CRFLIB, FK_EVENTS, CRFLIB_NUMBER,
	   					CRFLIB_NAME, CRFLIB_FLAG,
	   					LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
	   					IP_ADD, CRFLIB_FORMFLAG, PROPAGATE_FROM
						)
						VALUES (v_next_pk_eventcrflib, v_fk_event_id, v_crflib_number,
						   v_crflib_name, v_crflib_flag,
						   v_last_modified_by, v_last_modified_date,
						   v_ip_add, v_crflib_form_flag, v_crflib_id);
					ELSE
						-- SV, 10/20/04, update the original record's propagate_from field with it's id as well, so we can identify all of them together.
						UPDATE SCH_CRFLIB SET
						PROPAGATE_FROM = v_crflib_id
						WHERE FK_EVENTS = v_selected_event_id
						AND PK_CRFLIB = v_crflib_id;
					END IF;


				END LOOP;



		END ;

	END sp_add_event_crflib;


	PROCEDURE sp_update_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER, p_mode IN CHAR)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_crflib_number VARCHAR2(50);
		v_crflib_name VARCHAR2(100);
		v_crflib_flag CHAR(1);
		v_next_pk_eventcrflib NUMBER;
	 	v_fk_event_id NUMBER;
		v_fk_cost_desc NUMBER;
		v_fk_currency NUMBER;
		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_crflib_form_flag NUMBER;
		v_crflib_id NUMBER;
		v_propagate_from NUMBER;
		BEGIN
   	   			v_selected_event_id := p_source_event_id;
				v_crflib_id := pk_crflib;

			IF p_mode = 'M' THEN
					SELECT CRFLIB_NUMBER,
					CRFLIB_NAME, CRFLIB_FLAG,
	   				LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
	   				IP_ADD, CRFLIB_FORMFLAG,PROPAGATE_FROM
					INTO v_crflib_number, v_crflib_name, v_crflib_flag,
					v_last_modified_by,v_last_modified_date,
					v_ip_add, v_crflib_form_flag, v_propagate_from
					FROM SCH_CRFLIB
					WHERE FK_EVENTS = v_selected_event_id
					AND pk_crflib = v_crflib_id;


					-- SV, 10/27/04, case when record was updated earlier without propagation and now it's getting propagated in edit mode.
					IF ( NVL(v_propagate_from, 0) = 0) THEN
					   v_propagate_from := v_crflib_id;
						UPDATE SCH_CRFLIB SET
						PROPAGATE_FROM = v_crflib_id
						WHERE FK_EVENTS = v_selected_event_id
						AND PK_CRFLIB = v_crflib_id;

					END IF;


	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP
					v_fk_event_id := p_target_event_list(i);

					IF (v_fk_event_id <> v_selected_event_id) THEN
						UPDATE SCH_CRFLIB SET
						CRFLIB_NUMBER = v_crflib_number,
						CRFLIB_NAME = v_crflib_name,
						CRFLIB_FLAG = v_crflib_flag,
		   				LAST_MODIFIED_BY = v_last_modified_by,
						LAST_MODIFIED_DATE = SYSDATE,
		   				IP_ADD = v_ip_add,
						CRFLIB_FORMFLAG = v_crflib_form_flag
						WHERE  (PROPAGATE_FROM = 	v_propagate_from )
						AND (FK_EVENTS = v_fk_event_id);

						IF (SQL%NOTFOUND) THEN

							SELECT seq_sch_crflib.NEXTVAL INTO v_next_pk_eventcrflib  FROM dual;

						INSERT INTO SCH_CRFLIB (
						PK_CRFLIB, FK_EVENTS, CRFLIB_NUMBER,
	   					CRFLIB_NAME, CRFLIB_FLAG,
	   					LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
	   					IP_ADD, CRFLIB_FORMFLAG, PROPAGATE_FROM
						)
						VALUES (v_next_pk_eventcrflib, v_fk_event_id, v_crflib_number,
						   v_crflib_name, v_crflib_flag,
						   v_last_modified_by, v_last_modified_date,
						   v_ip_add, v_crflib_form_flag, v_propagate_from);
						END IF;
					END IF;
				END LOOP;
	ELSIF p_mode = 'D' THEN

			SELECT PROPAGATE_FROM
				INTO v_propagate_from
				FROM SCH_CRFLIB
				WHERE FK_EVENTS = v_selected_event_id
				AND  PK_CRFLIB =   v_crflib_id;

				IF ( NVL(v_propagate_from, 0) = 0) THEN
				   v_propagate_from := v_crflib_id;
				END IF;

				--delete the main CRF

				DELETE FROM SCH_CRFLIB WHERE PK_CRFLIB =   v_crflib_id;

					 FOR i IN 1..p_target_event_list.COUNT
					 				LOOP

											v_fk_event_id := p_target_event_list(i);

											  DELETE FROM  SCH_CRFLIB  WHERE  FK_EVENTS = v_fk_event_id  AND
											  propagate_from = v_propagate_from;

						END LOOP;

	END IF;


		END ;

	END sp_update_event_crflib;



	PROCEDURE sp_add_event_resource(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventusr IN NUMBER, p_type CHAR)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_eventusr NUMBER;
		v_eventusr_type CHAR;
		v_next_pk_eventusr NUMBER;

		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);
		v_pk_eventusr NUMBER;
		v_eventusr_duration VARCHAR2(11);
		v_eventusr_notes VARCHAR2(1000);
		v_fk_event_id NUMBER;
		v_propagate_from NUMBER;

				-- SV, 10/21/04, fetch the roles for the selected event, the one we are propagating from.
				CURSOR event_roles_cursor IS
				SELECT EVENTUSR, EVENTUSR_TYPE,
				LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
				IP_ADD, EVENTUSR_DURATION, EVENTUSR_NOTES,PK_EVENTUSR, propagate_from
				FROM SCH_EVENTUSR
				WHERE FK_EVENT = v_selected_event_id AND EVENTUSR_TYPE = p_type;


		BEGIN
--		SV, 9/20/04, no need to check if it's a document or a link here, as it's stored in sch_docs table and we are only
-- storing the key to that table here.
-- SV, 9/17/04, to test the update statements scope, update a non-used field to update to avoid further mishap to event_def.
   	   			v_selected_event_id := p_source_event_id;
				v_eventusr := p_pk_eventusr;
				v_eventusr_type := p_type;



	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP


					v_fk_event_id := p_target_event_list(i);


					IF (v_fk_event_id <> v_selected_event_id) THEN

					   --SV, 10/21/04, the # of roles for an event are always fixed. So, delete what's already there
					   -- and copy from the event propagating from. This is what EventUsrAgentBean does when roles
					   -- for a single event are saved.
						DELETE FROM SCH_EVENTUSR
						WHERE FK_EVENT = v_fk_event_id AND EVENTUSR_TYPE = p_type;

						OPEN event_roles_cursor;
						LOOP
					        FETCH event_roles_cursor
			   				INTO v_eventusr, v_eventusr_type,
							v_last_modified_by, v_last_modified_date,
							v_ip_add, v_eventusr_duration, v_eventusr_notes,v_pk_eventusr, v_propagate_from ;

					        EXIT WHEN event_roles_cursor%NOTFOUND;

							IF NVL(v_propagate_from,0) = 0 THEN --update the old record's propagate from
									UPDATE SCH_EVENTUSR  SET
									PROPAGATE_FROM = v_pk_eventusr
									WHERE FK_EVENT = v_selected_event_id
									AND pk_eventusr = v_pk_eventusr;

							END IF;

							SELECT sch_eventusr_seq.NEXTVAL INTO v_next_pk_eventusr  FROM dual;
							INSERT INTO SCH_EVENTUSR (
							   PK_EVENTUSR, EVENTUSR, EVENTUSR_TYPE,
							   FK_EVENT,
							   LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
							   IP_ADD, EVENTUSR_DURATION, EVENTUSR_NOTES, PROPAGATE_FROM)
							VALUES (v_next_pk_eventusr, v_eventusr, v_eventusr_type,
							v_fk_event_id,
							v_last_modified_by, v_last_modified_date,
							v_ip_add, v_eventusr_duration, v_eventusr_notes, v_pk_eventusr)	;

					    END LOOP;
						CLOSE event_roles_cursor;

					END IF;

				END LOOP;


		END ;

	END sp_add_event_resource;



		PROCEDURE sp_manage_event_user(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventuser IN NUMBER , p_usertype IN VARCHAR2, p_mode IN CHAR)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;
		v_next_pk_eventuser NUMBER;
	 	v_fk_event_id NUMBER;
		v_ip_add VARCHAR2(15);
		v_pk_eventuser NUMBER;
		v_creator NUMBER;
		v_usr NUMBER;
		v_propagate_from NUMBER;
		BEGIN
--

   	   			v_selected_event_id := p_source_event_id;
				v_pk_eventuser := p_pk_eventuser;

				IF (p_mode = 'N') THEN

				SELECT  creator ,IP_ADD,eventusr
				INTO v_creator ,v_ip_add,v_usr
				FROM  SCH_EVENTUSR
				WHERE FK_EVENT = v_selected_event_id
				AND pk_eventusr= v_pk_eventuser;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP

					v_fk_event_id := p_target_event_list(i);

					IF (v_fk_event_id <> v_selected_event_id) THEN
						SELECT sch_eventusr_seq.NEXTVAL INTO v_next_pk_eventuser FROM dual;

						INSERT INTO SCH_EVENTUSR (
						PK_EVENTUSR,  FK_EVENT,
						CREATOR,CREATED_ON, EVENTUSR_TYPE,
						IP_ADD, PROPAGATE_FROM,eventusr)
						VALUES (v_next_pk_eventuser, v_fk_event_id,
						v_creator, SYSDATE, p_usertype,
						v_ip_add, v_pk_eventuser,v_usr);
					ELSE
						-- SV, 10/20/04, update the original record's propagate_from field with it's id as well, so we can identify all of them together.
						UPDATE SCH_EVENTUSR  SET
						PROPAGATE_FROM = v_pk_eventuser
						WHERE FK_EVENT = v_selected_event_id
						AND pk_eventusr = v_pk_eventuser;

					END IF;
				END LOOP;

				ELSIF  (p_mode = 'D') THEN --delete
				--------------------------
				SELECT PROPAGATE_FROM
				INTO v_propagate_from
				FROM SCH_EVENTUSR
				WHERE FK_EVENT = v_selected_event_id
				AND pk_eventusr = v_pk_eventuser;

				IF ( NVL(v_propagate_from, 0) = 0) THEN
				   v_propagate_from := v_pk_eventuser;
				END IF;

				--delete the main event user

				DELETE FROM SCH_EVENTUSR WHERE pk_eventusr = v_pk_eventuser;

				-------------------------
					 		 FOR i IN 1..p_target_event_list.COUNT
								 				LOOP

															v_fk_event_id := p_target_event_list(i);

															  DELETE FROM SCH_EVENTUSR WHERE  FK_EVENT = v_fk_event_id  AND
															  propagate_from = v_propagate_from;

											END LOOP;

				END IF;

		END ;

	END  sp_manage_event_user;


		PROCEDURE sp_add_event_crf_form(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array)
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;

		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);

		v_fk_event_id NUMBER;
		v_propagate_from NUMBER;

		v_pk_event_crf NUMBER;
		v_fk_from NUMBER;
		v_next_pk_eventcrf NUMBER;

		v_form_type VARCHAR2(15);
		v_other_links VARCHAR2(50);

				-- fetch the CRF forms for the selected event, the one we are propagating from.
				CURSOR event_form_cursor IS
				SELECT pk_eventcrf,  fk_form,form_type, other_links,
				LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
				IP_ADD, propagate_from
				FROM SCH_EVENT_CRF
				WHERE FK_EVENT = p_source_event_id ;


		BEGIN

   	   			v_selected_event_id := p_source_event_id;



	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP


					v_fk_event_id := p_target_event_list(i);


					IF (v_fk_event_id <> v_selected_event_id) THEN

					   --SV, 10/21/04, the # of roles for an event are always fixed. So, delete what's already there
					   -- and copy from the event propagating from. This is what EventUsrAgentBean does when roles
					   -- for a single event are saved.
						DELETE FROM SCH_EVENT_CRF
						WHERE FK_EVENT = v_fk_event_id ;

						OPEN event_form_cursor;
						LOOP
					        FETCH event_form_cursor
			   				INTO v_pk_event_crf, v_fk_from, v_form_type, v_other_links,
							v_last_modified_by, v_last_modified_date,
							v_ip_add, v_propagate_from ;

					        EXIT WHEN event_form_cursor%NOTFOUND;

							IF NVL(v_propagate_from,0) = 0 THEN --update the old record's propagate from
									UPDATE SCH_EVENT_CRF  SET
									PROPAGATE_FROM = v_pk_event_crf
									WHERE FK_EVENT = v_selected_event_id
									AND  pk_eventcrf = v_pk_event_crf;

							END IF;

							SELECT seq_sch_event_crf.NEXTVAL INTO v_next_pk_eventcrf  FROM dual;

							INSERT INTO SCH_EVENT_CRF (
								   pk_eventcrf,  fk_form,form_type, other_links,
							   FK_EVENT,
							   LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
							   IP_ADD, PROPAGATE_FROM)
							VALUES (v_next_pk_eventcrf  ,  v_fk_from, v_form_type, v_other_links,
							v_fk_event_id,
							v_last_modified_by, v_last_modified_date,
							v_ip_add,  v_pk_event_crf)	;

					    END LOOP;
						CLOSE event_form_cursor;

					END IF;

				END LOOP;


		END ;

	END sp_add_event_crf_form;

/** added by sonia abrol
	date: 05/15/08
	to propagate the storage kits*/
		PROCEDURE sp_add_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array )
	AS
		BEGIN
		DECLARE
		v_upd_event_id NUMBER;
		v_selected_event_id NUMBER;

		v_last_modified_by NUMBER;
		v_last_modified_date DATE;
		v_ip_add VARCHAR2(15);

		v_fk_event_id NUMBER;
		v_propagate_from NUMBER;

		v_pk_event_kit NUMBER;
		v_fk_storage NUMBER;
		v_next_pk_eventkit NUMBER;

		v_form_type VARCHAR2(15);
		v_other_links VARCHAR2(50);

				-- fetch the kits for the selected event, the one we are propagating from.
				CURSOR event_kit_cursor IS
				SELECT pk_eventkit,  fk_storage,
				creator, created_on,
				IP_ADD, propagate_from
				FROM SCH_EVENT_KIT
				WHERE FK_EVENT = p_source_event_id ;


		BEGIN

   	   			v_selected_event_id := p_source_event_id;



	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP


					v_fk_event_id := p_target_event_list(i);


					IF (v_fk_event_id <> v_selected_event_id) THEN


						DELETE FROM SCH_EVENT_KIT
						WHERE FK_EVENT = v_fk_event_id ;

						OPEN event_kit_cursor;
						LOOP
					        FETCH event_kit_cursor
			   				INTO v_pk_event_kit, v_fk_storage,
							v_last_modified_by, v_last_modified_date,
							v_ip_add, v_propagate_from ;

					        EXIT WHEN event_kit_cursor%NOTFOUND;

							IF NVL(v_propagate_from,0) = 0 THEN --update the old record's propagate from
									UPDATE SCH_EVENT_KIT  SET
									PROPAGATE_FROM = v_pk_event_kit
									WHERE FK_EVENT = v_selected_event_id
									AND  pk_eventkit = v_pk_event_kit;

							END IF;

							SELECT seq_sch_event_kit.NEXTVAL INTO v_next_pk_eventkit  FROM dual;

							INSERT INTO SCH_EVENT_KIT (
								   pk_eventkit,  fk_storage,
							   FK_EVENT,   LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
							   IP_ADD, PROPAGATE_FROM)
							VALUES (v_next_pk_eventkit  ,  v_fk_storage,
							v_fk_event_id,
							v_last_modified_by, v_last_modified_date,
							v_ip_add,  v_pk_event_kit)	;

					    END LOOP;
						CLOSE event_kit_cursor;

					END IF;

				END LOOP;


		END ;

	END sp_add_event_kit;

/** added by sonia abrol
	date: 05/15/08
	to propagate the storage kits deletion*/
		PROCEDURE sp_delete_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array ,p_pkeventkit in number)
	AS
		BEGIN
		DECLARE

		v_selected_event_id NUMBER;

		v_fk_event_id NUMBER;
		v_propagate_from NUMBER;

		v_pk_event_kit NUMBER;
		v_fk_storage NUMBER;
		v_next_pk_eventkit NUMBER;

		BEGIN

   	   			v_selected_event_id := p_source_event_id;

				SELECT PROPAGATE_FROM
				INTO v_propagate_from
				FROM SCH_event_kit
				WHERE FK_EVENT = v_selected_event_id
				AND  PK_eventkit =   p_pkeventkit ;


				IF ( NVL(v_propagate_from, 0) = 0) THEN
				   v_propagate_from := p_pkeventkit;
				END IF;

          		--delete the main record

				DELETE FROM SCH_event_kit WHERE PK_eventkit =   p_pkeventkit;

	   			FOR i IN 1..p_target_event_list.COUNT
				LOOP


					v_fk_event_id := p_target_event_list(i);


					IF (v_fk_event_id <> v_selected_event_id) THEN


						DELETE FROM SCH_EVENT_KIT
						WHERE FK_EVENT = v_fk_event_id  AND
						 propagate_from = v_propagate_from;


					END IF;

				END LOOP;


		END ;

	END sp_delete_event_kit;


END Pkg_Propgt_Evtupdates;
/