DROP VIEW ERES.ERV_PATIENT_LABS;

/* Formatted on 4/6/2009 4:21:30 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_LABS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   LAB_DATE,
   LAB_NAME,
   RESULT,
   LONG_TEST_RESULT,
   UNIT,
   LLN,
   ULN,
   ACCESSION_NUM,
   TOXICITY,
   LAB_STATUS,
   RESULT_TYPE,
   STUDY_NUMBER,
   STUDY_PHASE,
   NOTES,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTLAB_PAT_NAME,
   PTLAB_CATEGORY,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTLAB_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   per_code AS patient_id,
            (SELECT   patprot_patstdid
               FROM   ER_PATPROT pp
              WHERE       pp.fk_study = a.fk_study
                      AND pp.fk_per = a.fk_per
                      AND patprot_stat = 1
                      AND ROWNUM < 2)
               AS patient_study_id,
            TRUNC (test_date) AS lab_date,
            labtest_name AS lab_name,
            test_result AS result,
            test_result_tx AS long_test_result,
            test_unit AS unit,
            test_lln AS lln,
            test_uln AS uln,
            accession_num,
            er_name || NVL2 (er_name, ' - ', '') || er_grade AS toxicity,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_tststat)
               AS lab_status,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_abflag)
               AS result_type,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = a.fk_study)
               AS study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_stdphase)
               AS study_phase,
            notes,
            a.fk_per,
            a.created_on,
            fk_account,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PERSON = PK_PER)
               PTLAB_PAT_NAME,
            (SELECT   GROUP_NAME
               FROM   ER_LABGROUP
              WHERE   PK_LABGROUP = FK_TESTGROUP)
               PTLAB_CATEGORY,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            A.LAST_MODIFIED_DATE,
            A.RID,
            PK_PATLABS PTLAB_RESPONSE_ID,
            a.fk_study
     FROM   ER_PATLABS a, ER_LABTEST, ER_PER
    WHERE   pk_per = fk_per AND Pk_labtest = fk_testid;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_LABS FOR ERES.ERV_PATIENT_LABS;


CREATE OR REPLACE SYNONYM EPAT.ERV_PATIENT_LABS FOR ERES.ERV_PATIENT_LABS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_LABS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_LABS TO ESCH;
