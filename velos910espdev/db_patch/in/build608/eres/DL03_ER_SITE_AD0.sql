set define off;

CREATE OR REPLACE TRIGGER "ER_SITE_AD0" AFTER DELETE ON ER_SITE
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(2000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  Audit_Trail.record_transaction
    (raid, 'ER_SITE', :OLD.rid, 'D');
  deleted_data :=
  TO_CHAR(:OLD.pk_site) || '|' ||
  TO_CHAR(:OLD.fk_codelst_type) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  TO_CHAR(:OLD.fk_peradd) || '|' ||
  :OLD.site_name || '|' ||
  :OLD.site_info || '|' ||
  TO_CHAR(:OLD.site_parent) || '|' ||
  :OLD.site_stat || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  :OLD.SITE_ID || '|' ||
  :OLD.SITE_HIDDEN || '|' || --KM
  TO_CHAR(:OLD.GUID); -- 22-Dec-2011, New column GUID added, Ankit
INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/