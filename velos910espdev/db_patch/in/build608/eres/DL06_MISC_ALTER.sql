set define off;

--Sql for Table CB_NOTES to add column SUBJECT.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'SUBJECT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add SUBJECT VARCHAR2(25)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.SUBJECT IS 'The subject or title of a particular note entered by the user.'; 
commit;


--Sql for Table CB_NOTES to add column REQUEST DATE.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'REQUEST_DATE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add REQUEST_DATE DATE');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.REQUEST_DATE IS 'Indication of the Request Date for the current request when the note was entered.'; 
commit;


--Sql for Table CB_NOTES to add column REQUEST TYPE.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'REQUEST_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add REQUEST_TYPE NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.REQUEST_TYPE IS 'Indication of the Request Date for the current request when the note was entered.'; 
commit;




--Sql for Table ER_PATLABS to add column FK_TIMING_OF_TEST Concentration.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_PATLABS' AND column_name = 'FK_TIMING_OF_TEST'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table ER_PATLABS add FK_TIMING_OF_TEST NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN ER_PATLABS.FK_TIMING_OF_TEST IS 'Refrence from ER_CODELST.'; 
commit;


COMMENT ON COLUMN ER_PATLABS.CUSTOM002 IS 'Custom field for future used to store test decription.';


--STARTS ADDING COLUMN HIVPNAT_COMP TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'HIVPNAT_COMP';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(HIVPNAT_COMP varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."HIVPNAT_COMP" IS 'This is the column to store the result of HIV p24 or HIV NAT';

--STARTS ADDING COLUMN HLACHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'HLACHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(HLACHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."HLACHILD" IS 'This is the column to store the result of hla child question';

--STARTS ADDING COLUMN CTCOMPLETED TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CTCOMPLETED';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CTCOMPLETED varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CTCOMPLETED" IS 'This is the column to store the result of CT Completed';

--STARTS ADDING COLUMN CTCOMPLETED_CHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CTCOMPLETED_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CTCOMPLETED_CHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CTCOMPLETED_CHILD" IS 'This is the column to store the result of CT Completed child question';

--STARTS ADDING COLUMN ANTIGENCHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'ANTIGENCHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(ANTIGENCHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."ANTIGENCHILD" IS 'This is the column to store the result of Antigen child question';

--STARTS ADDING COLUMN UNITRECIPIENT TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'UNITRECIPIENT';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(UNITRECIPIENT varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."UNITRECIPIENT" IS 'This is the column to store the result of Unit Recipient';

--STARTS ADDING COLUMN UNITRECIPIENT_CHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'UNITRECIPIENT_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(UNITRECIPIENT_CHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."UNITRECIPIENT_CHILD" IS 'This is the column to store the result Unit Recipient child question';

--STARTS ADDING COLUMN CBUPLANNED TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CBUPLANNED';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CBUPLANNED varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CBUPLANNED" IS 'This is the column to store the result of CBU planned';

--STARTS ADDING COLUMN CBUPLANNED_CHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CBUPLANNED_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CBUPLANNED_CHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CBUPLANNED_CHILD" IS 'This is the column to store the result of cbu planned child question';

--STARTS ADDING COLUMN PATIENTTYPING TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'PATIENTTYPING';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(PATIENTTYPING varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."PATIENTTYPING" IS 'This is the column to store the result of Patienttyping';

--STARTS ADDING COLUMN PATIENTTYPING_CHILD TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'PATIENTTYPING_CHILD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(PATIENTTYPING_CHILD varchar2(20))';
  end if;
end;
/
--END--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."PATIENTTYPING_CHILD" IS 'This is the column to store the result of Patienttyping child question';


--Sql for Table CBB to add column FK_CBU_STORAGE, FK_CBU_COLLECTION, FK_ID_ON_BAG.

DECLARE
  v_column_one_exists number := 0;  
  v_column_two_exists number := 0;  
  v_column_three_exists number := 0;  
BEGIN
  Select count(*) into v_column_one_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_CBU_STORAGE'; 
 Select count(*) into v_column_two_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_CBU_COLLECTION'; 
 Select count(*) into v_column_three_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_ID_ON_BAG'; 
  if (v_column_one_exists = 0) then
      execute immediate ('alter table CBB add FK_CBU_STORAGE NUMBER(10)');
  end if;
   if (v_column_two_exists = 0) then
      execute immediate ('alter table CBB add FK_CBU_COLLECTION NUMBER(10)');
  end if;
   if (v_column_three_exists = 0) then
      execute immediate ('alter table CBB add FK_ID_ON_BAG NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_CBU_STORAGE IS 'Stores information about CBU Storage Location of CBB.'; 
COMMENT ON COLUMN CBB.FK_CBU_COLLECTION IS 'Stores information about CBU Collection site of CBB.'; 
COMMENT ON COLUMN CBB.FK_ID_ON_BAG IS 'Stores information about IDs on Bag.'; 

--STARTS ADDING COLUMN CORD_AVAIL_CONFIRM_DATE TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CORD_AVAIL_CONFIRM_DATE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(CORD_AVAIL_CONFIRM_DATE DATE)';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_CORD.CORD_AVAIL_CONFIRM_DATE IS 'Stores when cord avauilability is confirmed';


--STARTS ADDING COLUMN PADLOCK_COMBINATION TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'PADLOCK_COMBINATION';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(PADLOCK_COMBINATION VARCHAR2(200 CHAR))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_SHIPMENT.PADLOCK_COMBINATION IS 'Stores shippers padlock combination';


--STARTS ADDING COLUMN PAPERWORK_LOCATION TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'PAPERWORK_LOCATION';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(PAPERWORK_LOCATION NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_SHIPMENT.PAPERWORK_LOCATION IS 'Stores where paper work is located in the shipper';


--STARTS ADDING COLUMN ADDITI_SHIPPER_DET TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'ADDITI_SHIPPER_DET';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(ADDITI_SHIPPER_DET VARCHAR2(200 CHAR))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_SHIPMENT.ADDITI_SHIPPER_DET IS 'Stores additional details regarding return of shipper';


--Sql for Table CB_NOTES to add column EXPLAINATION_TU.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'EXPLAINATION_TU'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add EXPLAINATION_TU VARCHAR2(25)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.EXPLAINATION_TU IS 'This will hold explaination in case of TU.'; 
commit;

alter table CB_ANTIGEN_ENCOD  add valid_ind   varchar2(1);
alter table  CB_ANTIGEN_ENCOD add active_ind varchar2(1);
alter table CB_ANTIGEN_ENCOD rename column last_modified_on to last_modified_date;
alter table CB_ANTIGEN  rename column last_modified_on to last_modified_date;
commit;