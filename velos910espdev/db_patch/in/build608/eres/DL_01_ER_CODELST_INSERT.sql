set define off;
--STARTS INSERT ER_CODELST FOR BUG NO 8020--
DECLARE
countFlag NUMBER(5);
BEGIN
    SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
INSERT INTO ER_CODELST(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ, CODELST_CUSTOM_COL) VALUES(SEQ_ER_CODELST.NEXTVAL,'ProgramCode','OD','OD',36,'NCI');

commit;
end if;
END;
/
