set define off;

CREATE TABLE CB_CBU_STATUS(
	PK_CBU_STATUS NUMBER(10,0),
	CBU_STATUS_CODE VARCHAR2(5 CHAR),
	CBU_STATUS_DESC VARCHAR2(50 CHAR),
	CODE_TYPE VARCHAR2(50 CHAR),
	CBB_CT VARCHAR2(1 CHAR),
	CBB_HE VARCHAR2(1 CHAR),
	CBB_OR VARCHAR2(1 CHAR),
	IS_LOCAL_REG VARCHAR2(1 CHAR),
	IS_NATIONAL_REG VARCHAR2(1 CHAR),
	USA VARCHAR2(1 CHAR),
	AUS VARCHAR2(1 CHAR),
	IS_REMOVABLE_REASON VARCHAR2(1 CHAR),
	IS_TERMINAL_RELEASE VARCHAR2(1 CHAR),
	IS_AUTOCOMPLETE_STATUS VARCHAR2(1 CHAR),
	FK_CBU_STATUS NUMBER(10,0),
	CREATOR	NUMBER(10,0),
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	CREATED_ON DATE,
	IP_ADD VARCHAR2(15 BYTE),
	RID NUMBER
);

COMMENT ON TABLE CB_CBU_STATUS IS 'Stores Response codes';
COMMENT ON COLUMN CB_CBU_STATUS.PK_CBU_STATUS IS 'Stores unique sequence generated value as primary key';
COMMENT ON COLUMN CB_CBU_STATUS.CBU_STATUS_CODE IS 'Stores response code';
COMMENT ON COLUMN CB_CBU_STATUS.CBU_STATUS_DESC IS 'Stores Response Code description';
COMMENT ON COLUMN CB_CBU_STATUS.CODE_TYPE IS 'Stores whether it is Response code or resolution code';
COMMENT ON COLUMN CB_CBU_STATUS.CBB_CT IS 'Stores 1 if applicable for CT Order else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.CBB_HE IS 'Stores 1 if applicable for HE Order else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.CBB_OR IS 'Stores 1 if applicable for OR Order else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.IS_LOCAL_REG IS 'Stores 1 if it is local code else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.IS_NATIONAL_REG IS 'Stores 1 if it is national code else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.USA IS 'Stores 1 if it is available in USA else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.AUS IS 'Stores 1 if it is available in AUS else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.IS_REMOVABLE_REASON IS 'Stores 1 if it is removable reason code else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.IS_TERMINAL_RELEASE IS 'Stores 1 if it is terminal release code else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.IS_AUTOCOMPLETE_STATUS IS 'Stores 1 if it is auto complete status code else stores 0';
COMMENT ON COLUMN CB_CBU_STATUS.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN CB_CBU_STATUS.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN CB_CBU_STATUS.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN CB_CBU_STATUS.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN CB_CBU_STATUS.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN CB_CBU_STATUS.RID IS 'Uses for Audit trail. Stores sequence generated unique value';
COMMENT ON COLUMN CB_CBU_STATUS.FK_CBU_STATUS IS 'Stores reference of same table to map resolution code with response code';


CREATE SEQUENCE ERES.SEQ_CB_CBU_STATUS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;


Commit;