set define off;

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DD','Permanently Medically Deferred','Response','1','1','1','1','0','1','0','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'QR'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'QR','Quarantined','Response','1','1','1','1','0','1','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'XP'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'XP','Expired','Response','1','1','1','1','0','1','0','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CD','Cord Destroyed or Damaged','Response','1','1','1','1','0','1','0','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AG'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AG','Aliquots Gone','Response','1','1','1','1','0','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'OT'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'OT','Cord Unavail - Other Reasons','Response','1','1','1','1','0','1','1','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SO','Cord Shipped for Other','Response','1','1','1','1','0','1','1','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RSN'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RSN','Reserved for NMDP','Response','1','1','1','1','0','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AV'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AV','Available','Response','0','0','0','1','1','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RSO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RSO','Reserved for Other','Response','0','0','0','1','0','1','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'EE'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'EE','Data Entry Error','Response','0','0','0','1','0','1','0','1','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RO','Reserved for Other','Response','1','1','1','0','0','0','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DT'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DT','Discrepant typing','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SA'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SA','Recipient not ready','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SC'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SC','Cord Incompatible','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'HE'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'HE','Held','Response','0','0','0','0','0','0','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CT'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CT','Repeat CT','Response','0','0','0','0','0','0','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'OR'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'OR','On Order','Response','0','0','0','0','0','0','0','0','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SB'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SB','Different Cord Chosen','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SD','Recipient Died','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SE'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SE','Other Cell Source Chosen','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CA'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CA','Canceled','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'NR'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'NR','No Results Received in 30 days','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SH'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SH','Shipped','Response','0','0','0','0','0','1','0','1','0','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'IN'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'IN','Infused','Response','0','0','0','0','0','1','0','0','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'NA'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'NA','Not Available','Response','0','0','0','0','1','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AC'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AC','Active in Search','Response','0','0','0','0','1','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'PN'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'PN','Pending','Response','0','0','0','0','1','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'TU'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'TU','Temporarily Unavailable','Response','0','0','0','0','1','1','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CB'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CB','Infused Date Passed, Release Cord','Response','0','0','0','0','0','1','1','0','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'PC'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'PC','Patient Cond Changed, No Infusion','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'PD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'PD','Patient Died, No Infusion','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'TC'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'TC','TC Decided Against Infusion','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CC'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CC','Unknown Code','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DS'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DS','Damaged in Transit','Response','0','0','0','0','0','1','1','0','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DU'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DU','Discrepant Unit','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'LS'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'LS','Lost in Transit','Response','0','0','0','0','0','1','1','0','1','1');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'TS'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'TS','Thawed in Transit','Response','0','0','0','0','0','1','0','0','1','1');

	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'ST'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'ST','Staged','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RG'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RG','Registered','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AS'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AS','Additional Sample Testing','Response','0','0','0','0','0','1','1','1','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SR'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SR','Search Request','Response','0','0','0','0','0','1','1','1','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'QA'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'QA','QA/QC','Response','0','0','0','0','0','1','1','1','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RS'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RS','Research','Response','0','0','0','0','0','1','1','1','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DM'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DM','Sample Damaged/Destroyed','Response','0','0','0','0','0','1','1','1','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AL'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AL','Additional Listings Only','Response','0','0','0','0','0','1','1','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'WU'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'WU','Unknown Code','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AB'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 0) then
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AB','AB transferred in','Response','0','0','0','0','0','0','0','0','0','0');

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'DD'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='DD' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'DD','Permanently unavailable Medically Deferred','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'QR'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='QR' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'QR','Cord quarantined','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'CD'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CD' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'CD','Cord Destroyed or Damaged','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'AG'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AG' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'AG','Aliquots Gone','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'OT'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='OT' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'OT','Cord Unavail - Other Reasons','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SO' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'SO','Shipped for Other','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_REPONSE_CODE NUMBER:=0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where CBU_STATUS_CODE = 'RO'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 0) then
	SELECT PK_CBU_STATUS INTO V_REPONSE_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RO' AND CODE_TYPE='Response';
     insert into CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,CBB_CT,CBB_HE,CBB_OR,IS_LOCAL_REG,IS_NATIONAL_REG,USA,AUS,IS_REMOVABLE_REASON,IS_TERMINAL_RELEASE,IS_AUTOCOMPLETE_STATUS,FK_CBU_STATUS)values (SEQ_CB_CBU_STATUS.nextval,'RO','Reserved for Other','Resolution','0','0','0','0','0','0','0','0','0','0',V_REPONSE_CODE);

	commit;
  end if;
end;
/
--END--



COMMENT ON COLUMN CB_CORD.CORD_NMDP_STATUS IS 'Stores reference of CB_CBU_STATUS table value with IS_NATIONAL_REG value 1';
COMMENT ON COLUMN CB_CORD.FK_CORD_CBU_STATUS IS 'Stores reference of CB_CBU_STATUS table value with IS_LOCAL_REG value 1';

commit;