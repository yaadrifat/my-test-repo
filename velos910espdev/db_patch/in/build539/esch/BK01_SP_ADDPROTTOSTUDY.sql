create or replace
PROCEDURE        "SP_ADDPROTTOSTUDY" (
p_protocol IN NUMBER,
p_studyId IN NUMBER,
p_userid IN NUMBER ,
P_newProtocol OUT NUMBER,
p_ip IN VARCHAR,
p_calAssocTo CHAR
)
AS


/****************************************************************************************************
** Procedure to copy a protocol to a study

** user can add an existing calender to a study

** Parameter Description

** p_protocol event id of the protocol that is to be copied in a protocol
** p_studyId study id of the study in which protocol is to be copied
** P_newProtocol protocol id of the new copied protocol

** Author: Dinesh Khurana 15th June 2001
**
** Modification History
**
** Modified By			Date				Remarks
** Dinesh Khurana		23July2001			To apply check of duplicacy in a study.
** Dinesh Khurana		1Aug2001			Extra columns added in the table related to messages.
** Sam Varadarajan		30Sep2004			Calendar related enh. copying duration unit (type) and protocol visits.
** Jnanamay			15Apr2008			to provide the event sequence
** Manimaran			18Apr2008			new column to associate
** Sammie			29Apr2010			new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
** Bikash     			28Jul2010     			new column COVERAGE_NOTES
*****************************************************************************************************
*/

event_id NUMBER;
chain_id NUMBER;
event_type CHAR(1);
NAME VARCHAR2(4000); --KM 08Sep09
notes VARCHAR2(1000);
COST NUMBER;
cost_description VARCHAR2(200);
duration NUMBER;
user_id NUMBER;
linked_uri VARCHAR2(200);
fuzzy_period VARCHAR2(10);
msg_to CHAR(1);
status CHAR(1);
description VARCHAR2(1000);
displacement NUMBER;
eventMsg VARCHAR2(2000);
eventRes CHAR(1);
eventFlag NUMBER;

pat_daysbefore NUMBER(3);
pat_daysafter NUMBER(3);
usr_daysbefore NUMBER(3);
usr_daysafter NUMBER(3);
pat_msgbefore VARCHAR2(4000);
pat_msgafter VARCHAR2(4000);
usr_msgbefore VARCHAR2(4000);
usr_msgafter VARCHAR2(4000);
usr_event_sequence number;--JM: 15Apr2008

p_name VARCHAR2(4000);
org_id NUMBER;
p_chain_id NUMBER;
p_currval NUMBER;
p_status NUMBER;
p_count NUMBER;

v_duration_unit CHAR(1); -- SV, 09/30/04, added to copy visits from old protocol to new study protocol.
v_event_visit_id NUMBER; -- SV, 09/30/04, fk_visit
v_ret NUMBER; -- SV, 09/30/04,
-- The new protocol id generated from the EVENT_ASSOCIATION_SEQ sequence. This is stored in the -- p_chain_id
-- This variable is an OUT parameter
v_catlib NUMBER;
v_calAssocTo CHAR(1);
v_visit_num NUMBER;
v_cptcode VARCHAR2(255);
v_fuzzyAfter VARCHAR2(10);
v_durAfter CHAR(1);
v_durBefore CHAR(1);
v_event_category varchar2(100); --KM
v_event_library_type Number;
v_event_line_category Number;
v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;
V_COVERAGE_NOTES varchar2(4000);  --BK 28JUL2010

-- SV, 09/30/04, cal-enh- added code to copy duration_unit (type) and fk_visit (visits in event) to new protocol.
CURSOR C1 IS SELECT EVENT_ID,
		CHAIN_ID,
		EVENT_TYPE,
		NAME,
		NOTES,
		COST,
		COST_DESCRIPTION,
		DURATION,
		USER_ID,
		LINKED_URI,
		FUZZY_PERIOD,
		MSG_TO,
		STATUS,
		DESCRIPTION,
		DISPLACEMENT,
		ORG_ID ,
		EVENT_MSG,
		EVENT_RES,
		EVENT_FLAG,
		PAT_DAYSBEFORE,
		PAT_DAYSAFTER ,
		USR_DAYSBEFORE,
		USR_DAYSAFTER ,
		PAT_MSGBEFORE ,
		PAT_MSGAFTER ,
		USR_MSGBEFORE ,
		USR_MSGAFTER,
		DURATION_UNIT,
		FK_VISIT,FK_CATLIB,event_cptcode,event_fuzzyafter,event_durationafter,event_durationbefore, event_category,event_sequence,
		event_library_type ,
		event_line_category,
		SERVICE_SITE_ID,
		FACILITY_ID,
		FK_CODELST_COVERTYPE,
		COVERAGE_NOTES
		FROM EVENT_DEF WHERE chain_id = p_protocol ORDER BY event_type DESC ,COST,displacement ;

BEGIN

	IF (LENGTH(p_calAssocTo)=0) THEN
		v_calAssocTo:='P';
	ELSE
		v_calAssocTo:=p_calAssocTo;
	END IF;

	-- to check if the protocol name already exists in the study

	SELECT NAME INTO p_name FROM EVENT_DEF WHERE event_id=p_protocol;
	SELECT COUNT(event_id) INTO p_count FROM EVENT_ASSOC WHERE NAME=p_name AND EVENT_TYPE='P'AND CHAIN_ID=p_studyId
	AND event_calassocto=v_calAssocTo;

	IF p_count > 0 THEN
		p_newProtocol:=-1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		--SV, 09/30/04, ++ duration_unit, fk_vist
		FETCH C1 INTO event_id, chain_id , event_type, NAME, notes, COST, cost_description,
		duration, user_id, linked_uri, fuzzy_period, msg_to, status,description, displacement,
		org_id,eventMsg,eventRes, eventFlag,pat_daysbefore,pat_daysafter,usr_daysbefore,
		usr_daysafter,pat_msgbefore,pat_msgafter,usr_msgbefore,usr_msgafter, v_duration_unit, v_event_visit_id,v_catlib,v_cptcode,
		v_fuzzyAfter,v_durAfter,v_durBefore,v_event_category,usr_event_sequence,v_event_library_type, v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL INTO p_currval FROM dual;

		IF C1%ROWCOUNT = 1 THEN
			p_chain_id:=p_studyId;
			p_newProtocol:=p_currval;
			status:='W';

		ELSE
			p_chain_id:=p_newProtocol;
		END IF;

		--event id of the the protocol generated is the chain id of all the child events

		EXIT WHEN C1%NOTFOUND;
		--SV, ++ duration unit. visits handled separately below.
		INSERT INTO EVENT_ASSOC(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			STATUS,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID ,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER ,
			USR_DAYSBEFORE,
			USR_DAYSAFTER ,
			PAT_MSGBEFORE ,
			PAT_MSGAFTER ,
			USR_MSGBEFORE ,
			USR_MSGAFTER ,
			STATUS_DT ,
			STATUS_CHANGEBY ,
			CREATOR,
			IP_ADD, --Amarnadh
			DURATION_UNIT,FK_CATLIB,event_calassocto,event_cptcode,event_fuzzyafter,event_durationafter,event_durationbefore,event_category, event_sequence,
			event_library_type ,
			event_line_category,
			SERVICE_SITE_ID,
			FACILITY_ID,
			FK_CODELST_COVERTYPE,
			COVERAGE_NOTES
		)
 		VALUES(
			p_currval,
			p_chain_id ,
			event_type,
			NAME,
			notes,
			COST,
			cost_description,
			duration,
			user_id,
			linked_uri,
			fuzzy_period,
			msg_to,
			status,
			description,
			displacement,
			org_id,
			eventMsg,
			eventRes,
			eventFlag,
			pat_daysbefore,
			pat_daysafter,
			usr_daysbefore,
			usr_daysafter,
			pat_msgbefore,
			pat_msgafter ,
			usr_msgbefore,
			usr_msgafter ,
			SYSDATE ,
			p_userid ,
			p_userid,
			p_ip,
			v_duration_unit,v_catlib,v_calAssocTo,v_cptcode,v_fuzzyAfter,v_durAfter,v_durBefore, v_event_category,usr_event_sequence,
			v_event_library_type,
			v_event_line_category,
			v_SERVICE_SITE_ID,
			v_FACILITY_ID,
			v_FK_CODELST_COVERTYPE,
      V_COVERAGE_NOTES
		); --KM

		IF C1%ROWCOUNT = 1 THEN
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USERID,P_IP, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		end if;

		IF C1%ROWCOUNT != 1 THEN

			-- SV, 9/30, copy the visit for the event to the new protocol calendar and update the event record with the visit id. Then update the new event record with new visit id.
 			IF (v_event_visit_id > 0) THEN

 				--Get the visit no to differentiate between 2 events with same displacement
 				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT WHERE pk_protocol_visit=v_event_visit_id;

				--SV, 09/30
				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USERID,P_IP, v_ret,v_visit_num);

				IF (v_ret > 0) THEN
 					UPDATE EVENT_ASSOC
 					SET FK_VISIT = v_ret
 					WHERE EVENT_ID = P_CURRVAL;
				END IF;
	 		END IF;
 	 		
 	 		--Modified by Sonia Abrol, 06/05/06, send '0' as propagation flag, do not propagate in case of copy calendars
 			Sp_Cpevedtls(event_id,p_currval,'s',p_status,TO_CHAR(p_userid),p_ip, 0);
		END IF;
 	END LOOP;
	CLOSE C1;

	/**********************************
	TEST
	set serveroutput on
	declare
	newProtId number ;
	begin
	sp_addprottostudy(7739,1850,newProtId) ;
	dbms_output.put_line(to_char(newProtId) ) ;
	end ;
	**************************************/
	COMMIT;
END;
/