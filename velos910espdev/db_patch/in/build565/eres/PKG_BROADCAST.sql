CREATE OR REPLACE PACKAGE ERES."PKG_BROADCAST" 
IS

PROCEDURE SP_BROADCAST_STUDY(p_study in number, o_ret OUT NUMBER);

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_BROADCAST', pLEVEL  => Plog.LFATAL);


END PKG_BROADCAST;
/


CREATE OR REPLACE PACKAGE BODY ERES."PKG_BROADCAST"
IS

PROCEDURE SP_BROADCAST_STUDY(p_study in number, o_ret OUT NUMBER)
IS

v_tarea_desc varchar2(500);
v_studynumber varchar2(500);
v_study_title varchar2(1000);

v_tarea  number; 

v_msgtemplate VARCHAR2(4000);
v_mail VARCHAR2(4000);
v_fparam VARCHAR2(1000);


BEGIN

begin
  -- get study info 
    
  select study_number, fk_codelst_tarea, study_title
  into v_studynumber ,v_tarea  ,v_study_title 
  from er_study
  where pk_study = p_study; 

  -- get therapeutic area description

  select codelst_desc
  into v_tarea_desc
  from er_codelst
  where pk_codelst = v_tarea  ;



       v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('broadcast');

     v_fparam :=  v_tarea_desc ||'~'|| v_studynumber ||'~'|| v_study_title ;


     v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);


  -- get all subscribers for study's therapeutic area and prepare messages

    INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT)
            select  SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE ,0,
                        'U',
                        v_mail  ,
                        fk_user
           from er_studynotify
           where fk_codelst_tarea =v_tarea   and  studynotify_type = 'tarea';

 


   COMMIT;
   o_ret:=0;

 exception when others then

       o_ret:=-1;  
       plog.fatal(pctx,'SP_BROADCAST_STUDY.SP_BROADCAST_STUDY:'||sqlerrm);


  end;

END SP_BROADCAST_STUDY;


END PKG_BROADCAST;
/
