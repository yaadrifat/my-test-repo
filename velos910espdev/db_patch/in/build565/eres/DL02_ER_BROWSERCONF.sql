set define off;

INSERT
INTO ER_BROWSERCONF
  (
    PK_BROWSERCONF,
    FK_BROWSER,
    BROWSERCONF_COLNAME,
    BROWSERCONF_SEQ,
    BROWSERCONF_SETTINGS,
    BROWSERCONF_EXPLABEL
  )
  VALUES
  (
    SEQ_ER_BROWSERCONF.nextval,
    (SELECT PK_BROWSER 
    FROM ER_BROWSER 
    WHERE BROWSER_MODULE = 'studyPatient' AND BROWSER_NAME = 'Enrolled Patient Browser'),
   'PATSTUDYCUR_STAT',
    11,
    '{"key":"PATSTUDYCUR_STAT", "label":"Current Status", "sortable":true, "resizeable":true,"hideable":true,"format":"currStatusLink"}',
    ' Patient Current status'
  );
  commit;
  
  UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"PATSTUDYSTAT_DESC", "label":"Most Recent Status", "sortable":true, "resizeable":true,"hideable":true,"format":"statusLink"}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='PATSTUDYSTAT_DESC'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'studyPatient'
    AND BROWSER_NAME     = 'Enrolled Patient Browser'
    )
  );
  commit;
  
  