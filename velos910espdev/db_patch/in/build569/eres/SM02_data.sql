SET DEFINE OFF;

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = '12' and 
  OBJECT_NAME = 'study_tab';
  if (row_count < 1) then 
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = OBJECT_SEQUENCE + 1
  WHERE OBJECT_SEQUENCE >=6 and OBJECT_NAME = 'study_tab';
  
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', '12', 'study_tab', 6, 0, 'Milestones', 0);
  end if;
END;
/

COMMIT;