set define off;

update esch.sch_codelst set codelst_desc = 'Not done' where codelst_desc = 'Not Done';

update esch.sch_codelst set codelst_desc = 'Not Required' where codelst_desc = 'Not Required';

update esch.sch_codelst set codelst_desc = 'To be rescheduled' where codelst_desc = 'To Be Rescheduled';

commit;