DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_ALERT_CONDITIONS' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_ALERT_CONDITIONS ADD CONSTRAINT PK_ALERT_CONDITIONS PRIMARY KEY(PK_ALERT_CONDITIONS)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_ALERT_INSTANCES' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_ALERT_INSTANCES ADD CONSTRAINT PK_ALERT_INSTANCES PRIMARY KEY(PK_ALERT_INSTANCES)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_CBU_STATUS' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_CBU_STATUS ADD CONSTRAINT PK_CBU_STATUS PRIMARY KEY(PK_CBU_STATUS)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_CORD_COMPLETE_REQ_INFO' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_CORD_COMPLETE_REQ_INFO ADD CONSTRAINT PK_CORD_COMPLETE_REQINFO PRIMARY KEY(PK_CORD_COMPLETE_REQINFO)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_FORM_VERSION' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_FORM_VERSION ADD CONSTRAINT PK_FORM_VERSION PRIMARY KEY(PK_FORM_VERSION)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_PACKING_SLIP' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_PACKING_SLIP ADD CONSTRAINT PK_PACKING_SLIP PRIMARY KEY(PK_PACKING_SLIP)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_RECEIPANT_INFO' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_RECEIPANT_INFO ADD CONSTRAINT PK_RECEIPANT PRIMARY KEY(PK_RECEIPANT)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'CB_SHIPMENT' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE CB_SHIPMENT ADD CONSTRAINT PK_SHIPMENT PRIMARY KEY(PK_SHIPMENT)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'ER_ORDER_HEADER' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE ER_ORDER_HEADER ADD CONSTRAINT PK_ORDER_HEADER PRIMARY KEY(PK_ORDER_HEADER)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'ER_ORDER_SERVICES' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE ER_ORDER_SERVICES ADD CONSTRAINT PK_ORDER_SERVICES PRIMARY KEY(PK_ORDER_SERVICES)';
	commit;
  end if;
end;
/

DECLARE
  v_constraint_exists number := 0;
BEGIN
  SELECT COUNT(*) into v_constraint_exists	
  FROM ALL_CONS_COLUMNS A  JOIN ALL_CONSTRAINTS C 
  ON A.CONSTRAINT_NAME  = C.CONSTRAINT_NAME	
  WHERE C.TABLE_NAME = 'ER_ORDER_USERS' 
  AND C.CONSTRAINT_TYPE = 'P';
  if ( v_constraint_exists = 0) then
    execute immediate 'ALTER TABLE ER_ORDER_USERS ADD CONSTRAINT PK_ORDER_USER PRIMARY KEY(PK_ORDER_USER)';
	commit;
  end if;
end;
/


--STARTS ADD THE COLUMN in ER_ORDER--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'ER_ATTACHMENTS'
    AND COLUMN_NAME = 'ACTION_FLAG';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_ATTACHMENTS add ACTION_FLAG varchar2(1)';
      commit;
  end if;
end;
/


--STARTS ADD THE COLUMN in ER_ORDER--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CBU_UNIT_REPORT'
    AND COLUMN_NAME = 'ACTION_FLAG';

  if (v_column_exists = 0) then
      execute immediate 'alter table CBU_UNIT_REPORT add ACTION_FLAG varchar2(1)';
      commit;
  end if;
end;
/