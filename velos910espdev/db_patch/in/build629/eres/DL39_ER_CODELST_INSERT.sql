Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f4','F4','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f5','F5','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f6','F6','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f7','F7','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f8','F8','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','f9','F9','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'fund_cat_code','oeap','OEAP','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'switchwf_rsn'
    AND codelst_subtyp = 'NON_SEG_MAT';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'switchwf_rsn','NON_SEG_MAT','NMDP lab needs sample (Non-segment material)','N',NULL,SYSDATE,SYSDATE,1);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'switchwf_rsn'
    AND codelst_subtyp = 'SEG_MAT';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'switchwf_rsn','SEG_MAT','NMDP lab needs sample (Segment material)','N',NULL,SYSDATE,SYSDATE,2);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'switchwf_rsn'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'switchwf_rsn','other','Other','N',NULL,SYSDATE,SYSDATE,3);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'switch_to'
    AND codelst_subtyp = 'lab';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'switch_to','lab','CT - Sample at Lab','N',NULL,SYSDATE,SYSDATE,1);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'switch_to'
    AND codelst_subtyp = 'ship';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'switch_to','ship','CT - Ship Sample','N',NULL,SYSDATE,SYSDATE,1);
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'close_ordr';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Completed' where codelst_type = 'order_status'
    AND codelst_subtyp = 'close_ordr';
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_29';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_29','Canceled','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',1);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_30';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_30','Discrepant Typing','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',2);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_31';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_31','Confirmatory Testing Requested','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',3);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_32';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_32','Held Requested','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',4);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_33';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_33','No Results Received � Lab has not reported results','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',5);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_34';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_34','Order for Shipment Requested','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',6);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_35';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_35','Patient Not Ready','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',7);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_36';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_36','Different Cord Chosen','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',8);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_37';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_37','Cord Incompatible','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',9);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_38';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_38','Patient Died','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',10);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_39';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_39','Other Stem Cell Source Chosen','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)',11);
	commit;
  end if;
end;
/
--END--


-- INSERTING CODE LIST VALUES FOR MRQ RESPONSE TYPE 1--
Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res1','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res1','no','No','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res1','bankdntask','Bank did not ask question','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res1','momdntans','Mother did not answer','N',4,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR MRQ RESPONSE TYPE 2--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res2','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res2','no','No','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR MRQ UNEXPECTED RESPONSE TYPE 1--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres1','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres1','bankdntask','Bank did not ask question','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres1','momdntans','Mother did not answer','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR MRQ UNEXPECTED RESPONSE TYPE 1--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres2','no','No','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--


-- INSERTING CODE LIST VALUES FOR FORM RESPONSE TYPE-

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','dropdown','dropdown','N',1,null,null,null,null ,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','mulselc','multiselect','N',2,null,null,null,null ,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','txtbox','textfield','N',3,null,null,null,null ,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','datefield','datefield','N',4,null,null,null,null ,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'resp_type','radio','radio','N',5,null,null,null,null ,sysdate,sysdate,null,null,null,null);

--END--
commit;

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'temp_moniter';
  if (v_record_exists = 0) then

  Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
  values (SEQ_ER_CODELST.nextval,null,'temp_moniter','elect_temp','Electronic Temperature Monitor','N',1,'Y',null,null,null,sysdate,sysdate,null,null,null,null);

  Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
  values (SEQ_ER_CODELST.nextval,null,'temp_moniter','cryo_indi','Cryoguard Indicator','N',2,'Y',null,null,null,sysdate,sysdate,null,null,null,null);

  Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
  values (SEQ_ER_CODELST.nextval,null,'temp_moniter','other','OTHER','N',3,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
  
	commit;
  
  end if;
end;
/
