set define off;

delete er_repxsl where pk_repxsl in(173);
commit;

create or replace function f_lab_summary(pk_cordid number) return SYS_REFCURSOR
IS
p_lab_refcur SYS_REFCURSOR;
begin
OPEN p_lab_refcur FOR select a.pk_labtest, a.labtest_name, a.labtest_seq
,(select  test_result from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) pre_test_result
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid) ) pre_test_date
,(select test_result from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_test_result
,(select to_char(test_date,'Mon DD,YYYY') from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_test_date
, f_codelst_desc((select fk_test_method from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) post_testmeth
,(select dbms_lob.substr( notes, 4000, 1 ) from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_test_desc
,(select  test_result from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = 'post_proc_thaw' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw
, (select  custom004 from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw
, (select  custom005 from er_patlabs  where   custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw
,(select  test_result from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw1
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '1' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date1
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw1
, (select  custom004 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw1
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw1
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw1
, (select  custom005 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw1
,(select  test_result from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw2
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '2' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date2
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw2
, (select  custom004 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw2
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw2
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw2
, (select  custom005 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw2
,(select  test_result from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw3
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '3' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date3
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw3
, (select  custom004 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw3
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw3
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw3
, (select  custom005 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw3
,(select  test_result from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw4
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '4' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date4
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw4
, (select  custom004 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw4
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw4
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw4
, (select  custom005 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw4
,(select  test_result from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_thaw5
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '5' and fk_testid = a.pk_labtest and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) post_cryopres_date5
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) reason_thaw5
, (select  custom004 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) reason_desc_thaw5
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) smpltype_thaw5
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid))) testmeth_thaw5
, (select  custom005 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=pk_cordid)) test_desc_thaw5
from er_labtest a,er_labtestgrp lgrp,er_labgroup grp where lgrp.fk_labgroup=grp.pk_labgroup and lgrp.fk_testid=a.pk_labtest and grp.group_name='PrcsGrp' and grp.group_type='PC' order by labtest_seq; 
RETURN p_lab_refcur;
end;  
/

commit;
  
  
  CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_DETAILS" AS 
  SELECT pk_cord,
    cord_registry_id,
    registry_maternal_id,
    cord_local_cbu_id,
    maternal_local_id,
    CORD_ISBI_DIN_CODE,
    b.site_name cbbid,
    b.site_id cbb_id,
    f_codelst_desc(fk_cbu_stor_loc) storage_loc,
    c.site_name cbu_collection_site,
    f_codelst_desc(fk_cord_cbu_eligible_status) eligible_status,
    f_codelst_desc(fk_cord_cbu_lic_status) lic_status,
    TO_CHAR(prcsng_start_date,'Mon DD,YYYY') prcsng_start_date,
    prcsng_start_time,
    frz_time,
    f_codelst_desc(fk_cord_bact_cul_result) bact_culture,
    bact_comment,
    TO_CHAR(frz_date,'Mon DD,YYYY')frz_date,
    f_codelst_desc(fk_cord_fungal_cul_result) fungal_culture,
    TO_CHAR(a.bact_cult_date,'Mon DD,YYYY') bact_cul_strt_dt,
    TO_CHAR(a.fung_cult_date,'Mon DD,YYYY') fung_cul_strt_dt,
    fung_comment,
    f_codelst_desc(fk_cord_abo_blood_type) abo_blood_type,
    f_codelst_desc(hemoglobin_scrn) hemoglobin_scrn,
    f_codelst_desc(fk_cord_rh_type) rh_type,
    e.proc_name proc_name,
    TO_CHAR(e.proc_start_date,'Mon DD, YYYY') proc_start_date,
    TO_CHAR(e.proc_termi_date,'Mon DD, YYYY') proc_termi_date,
    f_codelst_desc(d.fk_proc_meth_id) processing,
    f_codelst_desc(d.fk_if_automated) automated_type,
    d.other_processing other_processing,
    f_codelst_desc(d.fk_product_modification) product_modification,
    d.other_prod_modi other_product_modi,
    f_codelst_desc(d.fk_stor_method) storage_method,
    f_codelst_desc(d.fk_freez_manufac) freezer_manufact,
    d.other_freez_manufac other_freezer_manufact,
    f_codelst_desc(d.fk_frozen_in) frozen_in,
    d.other_frozen_cont other_frozen_cont,
    f_codelst_desc(d.fk_num_of_bags) no_of_bags,
    d.cryobag_manufac cryobag_manufac,
    f_codelst_desc(d.fk_bagtype) bagtype,
    f_codelst_desc(d.fk_bag_1_type) bag1type,
    f_codelst_desc(d.fk_bag_2_type) bag2type,
    d.other_bag_type other_bag,
    d.THOU_UNIT_PER_ML_HEPARIN_PER heparin_thou_per,
    d.THOU_UNIT_PER_ML_HEPARIN heparin_thou_ml,
    d.FIVE_UNIT_PER_ML_HEPARIN_PER heparin_five_per,
    d.FIVE_UNIT_PER_ML_HEPARIN heparin_five_ml,
    d.TEN_UNIT_PER_ML_HEPARIN_PER heparin_ten_per,
    d.TEN_UNIT_PER_ML_HEPARIN heparin_ten_ml,
    d.SIX_PER_HYDRO_STARCH_PER heparin_six_per,
    d.SIX_PER_HYDROXYETHYL_STARCH heparin_six_ml,
    d.CPDA_PER cpda_per,
    d.CPDA cpda_ml,
    d.CPD_PER cpd_per,
    d.CPD cpd_ml,
    d.ACD_PER acd_per,
    d.ACD acd_ml,
    d.OTHER_ANTICOAGULANT_PER othr_anti_per,
    d.OTHER_ANTICOAGULANT othr_anti_ml,
    d.SPECIFY_OTH_ANTI speci_othr_anti,
    d.HUN_PER_DMSO_PER hun_dmso_per,
    d.HUN_PER_DMSO hun_dmso_ml,
    d.HUN_PER_GLYCEROL_PER hun_glycerol_per,
    d.HUN_PER_GLYCEROL hun_glycerol_ml,
    d.TEN_PER_DEXTRAN_40_PER ten_dextran_40_per,
    d.TEN_PER_DEXTRAN_40 ten_dextran_40_ml,
    d.FIVE_PER_HUMAN_ALBU_PER five_human_albu_per,
    d.FIVE_PER_HUMAN_ALBU five_human_albu_ml,
    d.TWEN_FIVE_HUM_ALBU_PER twentyfive_human_albu_per,
    d.TWEN_FIVE_HUM_ALBU twentyfive_human_albu_ml,
    d.PLASMALYTE_PER plasmalyte_per,
    d.PLASMALYTE plasmalyte_ml,
    d.OTH_CRYOPROTECTANT_PER othr_cryoprotectant_per,
    d.OTH_CRYOPROTECTANT othr_cryoprotectant_ml,
    d.SPEC_OTH_CRYOPRO spec_othr_cryoprotectant,
    d.FIVE_PER_DEXTROSE_PER five_dextrose_per,
    d.FIVE_PER_DEXTROSE five_dextrose_ml,
    d.POINNT_NINE_PER_NACL_PER point_nine_nacl_per,
    d.POINNT_NINE_PER_NACL point_nine_nacl_ml,
    d.OTH_DILUENTS_PER othr_diluents_per,
    d.OTH_DILUENTS othr_diluents_ml,
    d.SPEC_OTH_DILUENTS spec_othr_diluents,
    a.PRODUCT_CODE productcode,
    f_codelst_desc(d.fk_stor_temp) storage_temperature,
    d.max_value max_vol,
    f_codelst_desc(d.fk_contrl_rate_freezing) ctrl_rate_freezing,
    D.NO_OF_INDI_FRAC INDIVIDUAL_FRAC,
    D.NO_OF_VIABLE_SMPL_FINAL_PROD VIABLE_SAMP_FINAL_PROD,
    f.filt_pap_avail filter_paper,
    f.RBC_PEL_AVAIL rpc_pellets,
    f.NO_EXT_DNA_ALI extr_dna,
    f.NO_SER_ALI serum_aliquotes,
    F.NO_PLAS_ALI PLASMA_ALIQUOTES,
    F.NO_NON_VIA_ALI NONVIABLE_ALIQUOTES,
    F.NO_OF_SEG_AVAIL NO_OF_SEGMENTS,
    f.CBU_NO_REP_ALT_CON no_of_oth_rep_alliq_alter_cond,
    F.CBU_OT_REP_CON_FIN NO_OF_OTH_REP_ALLIQUOTS_F_PROD,
    F.NO_SER_MAT_ALI NO_OF_SERUM_MATER_ALIQUOTS,
    F.NO_PLAS_MAT_ALI NO_OF_PLASMA_MATER_ALIQUOTS,
    F.NO_EXT_DNA_MAT_ALI NO_OF_EXTR_DNA_MATER_ALIQUOTS,
    f.SI_NO_MISC_MAT NO_OF_CELL_MATER_ALIQUOTS,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') INELIGIBLEREASON,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') UNLICENSEREASON,
    a.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
    f_lic_eligi_moddt(a.pk_cord, 'eligibility') ELGIBLE_MODI_DT,
    f_lic_eligi_moddt(a.pk_cord, 'licence') LIC_MODI_DT,
    DECODE(a.FK_CORD_CBU_ELIGIBLE_STATUS, (f_codelst_id('eligibility','eligible')),'0', (f_codelst_id('eligibility','ineligible')),'1', (f_codelst_id('eligibility','incomplete')),'2', (f_codelst_id('eligibility','not_appli_prior')),'3') ELI_FLAG,
    DECODE(a.FK_CORD_CBU_LIC_STATUS, (f_codelst_id('licence','licensed')),'0', (f_codelst_id('licence','unlicensed')),'1') LIC_FLAG
  FROM cb_cord a
  LEFT OUTER JOIN er_site b
  ON (a.fk_cbb_id = b.pk_site)
  LEFT OUTER JOIN er_site c
  ON (a.fk_cbu_coll_site = c.pk_site)
  LEFT OUTER JOIN cbb_processing_procedures_info d
  ON (a.fk_cbb_procedure = d.fk_processing_id)
  LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES E
  ON (A.FK_CBB_PROCEDURE = E.PK_PROC )
  LEFT OUTER JOIN CB_ENTITY_SAMPLES F
  ON (f.entity_id=a.pk_cord);
 
commit;

update er_report set rep_sql='select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, bact_cul_strt_dt, fung_cul_strt_dt, frz_date, prcsng_start_time,frz_time,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1' where pk_report=173;
commit;

update er_report set rep_sql_clob ='select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, bact_cul_strt_dt, fung_cul_strt_dt, frz_date, prcsng_start_time,frz_time,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsum_attachments
from rep_cbu_details where pk_cord = ~1' where pk_report=173;
commit;
