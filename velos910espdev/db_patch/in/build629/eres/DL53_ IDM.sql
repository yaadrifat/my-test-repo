
--STARTS ADDING COLUMN TO CB_IDM--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_IDM'
    AND column_name = 'FK_TEST_SOURCE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_IDM ADD(FK_TEST_SOURCE NUMBER(10))';
  end if;
end;
/

COMMENT ON COLUMN "CB_IDM"."FK_TEST_SOURCE" IS 'Stores the code List value for IDM Test Source';
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'FMHQ';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'sub_entity_type','FMHQ','FMHQ','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS DELETING THE OLD IDM RECORD FROM ER_PATLABS--

delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='I');
delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='IOG');

--END--

--STARTS DELETING THE OLD IDM RECORD FROM CB_ASSESSMENT--

delete from cb_assessment where SUB_ENTITY_TYPE =(select pk_codelst from er_codelst where codelst_subtyp='IDM');

--END--
--STARTS DELETING THE OLD IDM RECORD FROM CB_IDM--
delete from cb_idm;
--END--
commit;
