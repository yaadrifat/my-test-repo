set define off;

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 4 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '6';

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 6 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '4';

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE = 7 where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '5';

commit;
