create or replace TRIGGER ESCH.SCH_SUBCOST_ITEM_VISIT_AD0 AFTER DELETE ON SCH_SUBCOST_ITEM_VISIT
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;
  USR VARCHAR2(2000);
begin
	--BUG 5818
	BEGIN
		USR := getuser(NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR));
		EXCEPTION WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;

select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'SCH_SUBCOST_ITEM_VISIT', :old.rid, 'D', USR);
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_SUBCOST_ITEM_VISIT) || '|' ||
to_char(:old.FK_SUBCOST_ITEM) || '|' ||
to_char(:old.FK_PROTOCOL_VISIT) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/