set define off;
create or replace
PROCEDURE  "SP_CPACCOUNT" (
   P_AC      IN       NUMBER,
   P_NEWAC   IN       NUMBER,
   P_RET     OUT      NUMBER
)
IS
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_CPACCOUNT', pLEVEL  => Plog.LFATAL);

   /*
   ** This procedure copies the event library (event type L and E ) for a account to another account.
   ** it also copies the section (code type 'section') records from the er_codelst table to the
   ** new account. The return value is -1 if the data has previously been copied for the new account.
   **
   ** Author: Charanjiv S Kalha 21st Sept 2001
   **
   ** Modification History
   **
   ** Modified By         Date          Remarks
   ** Sonika Talwar      April 21, 03   for passing new event_id in SP_CPEVEDTLS
   ** JM:				 09FEB2011,  	#D-FIN9: added FK_CODELST_CALSTAT and removed STATUS  

 */
   V_CAT       NUMBER;
   V_CURRVAL   NUMBER;
   V_EVEVAL    NUMBER;
   V_CNT       NUMBER;
   V_STAT      NUMBER;


   CURSOR CUR_CAT
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
             COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
             FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
             ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
             PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
             PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,FK_CATLIB,
             EVENT_LIBRARY_TYPE,EVENT_LINE_CATEGORY, fk_codelst_calstat
        FROM EVENT_DEF
       WHERE USER_ID = P_AC
         AND EVENT_TYPE = 'L';


   CURSOR CUR_EVENTS
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
             COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
             FUZZY_PERIOD, MSG_TO, DESCRIPTION, DISPLACEMENT,
             ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
             PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
             PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,FK_CATLIB,
             EVENT_LIBRARY_TYPE,EVENT_LINE_CATEGORY,fk_codelst_calstat
        FROM EVENT_DEF
       WHERE CHAIN_ID = V_CAT
         AND EVENT_TYPE = 'E'
         AND USER_ID = P_AC;
BEGIN
   BEGIN
      P_RET := 0;
      SELECT COUNT (PK_CODELST)
        INTO V_CNT
        FROM ER_CODELST
       WHERE CODELST_TYPE = 'section'
         AND FK_ACCOUNT = P_NEWAC;


      IF V_CNT > 0 THEN
         P ('codelst record count ' || TO_CHAR (V_CNT));
         P_RET := -1;
         --return;
      ELSE
         BEGIN
            P ('exception to codelst');
            SELECT COUNT (EVENT_ID)
              INTO V_CNT
              FROM EVENT_DEF
             WHERE USER_ID = P_NEWAC
               AND EVENT_TYPE = 'L';

            IF V_CNT > 0 THEN
              -- p ('event def record count ' || to_char (V_CNT));
               P_RET := -2;
               RETURN;
            END IF;
         END;
      END IF;
   END;


   BEGIN
      INSERT INTO ER_CODELST
                  (PK_CODELST,
                   FK_ACCOUNT,
                   CODELST_TYPE,
                   CODELST_SUBTYP,
                   CODELST_DESC,
                   CODELST_HIDE,
                   CODELST_SEQ,
                   CODELST_MAINT,
                   CREATOR,
                   LAST_MODIFIED_BY,
                   LAST_MODIFIED_DATE,
                   CREATED_ON,
                   IP_ADD
                  )
              (SELECT SEQ_ER_CODELST.NEXTVAL, P_NEWAC,
                      CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,
                      CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
                      LAST_MODIFIED_BY, SYSDATE, SYSDATE, IP_ADD
                 FROM ER_CODELST
                WHERE CODELST_TYPE = 'section'
                  AND FK_ACCOUNT = P_AC);
   END;


   BEGIN
      FOR CAT_REC IN CUR_CAT
      LOOP

         SELECT EVENT_DEFINITION_SEQ.NEXTVAL
           INTO V_CURRVAL
           FROM DUAL;


         INSERT INTO EVENT_DEF
                     (
                          EVENT_ID,
                          CHAIN_ID,
                          EVENT_TYPE,
                          NAME,
                          NOTES,
                          COST,
                          COST_DESCRIPTION,
                          DURATION,
                          USER_ID,
                          LINKED_URI,
                          FUZZY_PERIOD,
                          MSG_TO,                          
                          DESCRIPTION,
                          DISPLACEMENT,
                          ORG_ID,
                          EVENT_MSG,
                          EVENT_RES,
                          EVENT_FLAG,
                          PAT_DAYSBEFORE,
                          PAT_DAYSAFTER,
                          USR_DAYSBEFORE,
                          USR_DAYSAFTER,
                          PAT_MSGBEFORE,
                          PAT_MSGAFTER,
                          USR_MSGBEFORE,
                          USR_MSGAFTER,FK_CATLIB,
                          EVENT_LIBRARY_TYPE,
                          EVENT_LINE_CATEGORY,
                          fk_codelst_calstat
                     )
              VALUES(
                 V_CURRVAL,
                 V_CURRVAL,
                 CAT_REC.EVENT_TYPE,
                 CAT_REC.NAME,
                 CAT_REC.NOTES,
                 CAT_REC.COST,
                 CAT_REC.COST_DESCRIPTION,
                 CAT_REC.DURATION,
                 P_NEWAC,
                 CAT_REC.LINKED_URI,
                 CAT_REC.FUZZY_PERIOD,
                 CAT_REC.MSG_TO,                 
                 CAT_REC.DESCRIPTION,
                 CAT_REC.DISPLACEMENT,
                 CAT_REC.ORG_ID,
                 CAT_REC.EVENT_MSG,
                 CAT_REC.EVENT_RES,
                 CAT_REC.EVENT_FLAG,
                 CAT_REC.PAT_DAYSBEFORE,
                 CAT_REC.PAT_DAYSAFTER,
                 CAT_REC.USR_DAYSBEFORE,
                 CAT_REC.USR_DAYSAFTER,
                 CAT_REC.PAT_MSGBEFORE,
                 CAT_REC.PAT_MSGAFTER,
                 CAT_REC.USR_MSGBEFORE,
                 CAT_REC.USR_MSGAFTER,
                 CAT_REC.FK_CATLIB,
                 CAT_REC.EVENT_LIBRARY_TYPE,
                 CAT_REC.EVENT_LINE_CATEGORY,
                 CAT_REC.fk_codelst_calstat

              );

         --p ('event id' ||to_char (CAT_REC.EVENT_ID) ||' new id ' ||to_char (V_CURRVAL) ||' name ' || CAT_REC.NAME  );

         V_CAT := CAT_REC.EVENT_ID;


         FOR EVENTS_REC IN CUR_EVENTS
         LOOP
            SELECT EVENT_DEFINITION_SEQ.NEXTVAL
              INTO V_EVEVAL
              FROM DUAL;

            --Plog.DEBUG(pCTX,'in SP_CPACCOUNT pk: '|| V_EVEVAL || ' Name:' || EVENTS_REC.NAME);

            INSERT INTO EVENT_DEF
                        (
                          EVENT_ID,
                          CHAIN_ID,
                          EVENT_TYPE,
                          NAME,
                          NOTES,
                          COST,
                          COST_DESCRIPTION,
                          DURATION,
                          USER_ID,
                          LINKED_URI,
                          FUZZY_PERIOD,
                          MSG_TO,                          
                          DESCRIPTION,
                          DISPLACEMENT,
                          ORG_ID,
                          EVENT_MSG,
                          EVENT_RES,
                          EVENT_FLAG,
                          PAT_DAYSBEFORE,
                          PAT_DAYSAFTER,
                          USR_DAYSBEFORE,
                          USR_DAYSAFTER,
                          PAT_MSGBEFORE,
                          PAT_MSGAFTER,
                          USR_MSGBEFORE,
                          USR_MSGAFTER,FK_CATLIB,
                          EVENT_LIBRARY_TYPE,
                          EVENT_LINE_CATEGORY,
                          fk_codelst_calstat
                        )
                 VALUES(
                    V_EVEVAL,
                    V_CURRVAL,
                    EVENTS_REC.EVENT_TYPE,
                    EVENTS_REC.NAME,
                    EVENTS_REC.NOTES,
                    EVENTS_REC.COST,
                    EVENTS_REC.COST_DESCRIPTION,
                    EVENTS_REC.DURATION,
                    P_NEWAC,
                    EVENTS_REC.LINKED_URI,
                    EVENTS_REC.FUZZY_PERIOD,
                    EVENTS_REC.MSG_TO,                    
                    EVENTS_REC.DESCRIPTION,
                    EVENTS_REC.DISPLACEMENT,
                    EVENTS_REC.ORG_ID,
                    EVENTS_REC.EVENT_MSG,
                    EVENTS_REC.EVENT_RES,
                    EVENTS_REC.EVENT_FLAG,
                    EVENTS_REC.PAT_DAYSBEFORE,
                    EVENTS_REC.PAT_DAYSAFTER,
                    EVENTS_REC.USR_DAYSBEFORE,
                    EVENTS_REC.USR_DAYSAFTER,
                    EVENTS_REC.PAT_MSGBEFORE,
                    EVENTS_REC.PAT_MSGAFTER,
                    EVENTS_REC.USR_MSGBEFORE,
                    EVENTS_REC.USR_MSGAFTER,
                    EVENTS_REC.FK_CATLIB,
                    EVENTS_REC.EVENT_LIBRARY_TYPE,
                    EVENTS_REC.EVENT_LINE_CATEGORY,
                    EVENTS_REC.fk_codelst_calstat
                 );

            --p ('event id' || to_char (EVENTS_REC.EVENT_ID) || ' new id ' || to_char (V_EVEVAL) || ' name ' || EVENTS_REC.NAME );

            Sp_Cpevedtls (EVENTS_REC.EVENT_ID, V_EVEVAL,'E', V_STAT, NULL, NULL,0);

		 --p('after copy event details');

         END LOOP;
      END LOOP;
   END;

   COMMIT;
   P_RET := 0;
END;
/