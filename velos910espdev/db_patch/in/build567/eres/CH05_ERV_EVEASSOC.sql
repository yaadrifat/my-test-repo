set define off;

CREATE OR REPLACE FORCE VIEW ERV_EVEASSOC
(
   EVENT_ID,
   CHAIN_ID,
   EVENT_TYPE,
   NAME,
   NOTES,
   COST,
   COST_DESCRIPTION,
   DURATION,
   USER_ID,
   LINKED_URI,
   FUZZY_PERIOD,
   MSG_TO,
   STATUS,
   DESCRIPTION,
   DISPLACEMENT,
   ORG_ID,
   EVENT_MSG,
   EVENT_RES,
   CREATED_ON,
   EVENT_FLAG
)
AS
   SELECT   EVENT_ID,
            CHAIN_ID,
            EVENT_TYPE,
            NAME,
            NOTES,
            COST,
            COST_DESCRIPTION,
            DURATION,
            USER_ID,
            LINKED_URI,
            FUZZY_PERIOD,
            MSG_TO,
             (select trim(CODELST_SUBTYP) from sch_codelst where PK_CODELST=FK_CODELST_CALSTAT ) STATUS, --JM: 08FEB2011, #D-FIN9
            DESCRIPTION,
            DISPLACEMENT,
            ORG_ID,
            EVENT_MSG,
            EVENT_RES,
            CREATED_ON,
            EVENT_FLAG
     FROM   EVENT_ASSOC;
