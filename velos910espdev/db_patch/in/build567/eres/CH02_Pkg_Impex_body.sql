set define off;
Create or replace PACKAGE BODY "PKG_IMPEX" AS
PROCEDURE SP_EXP_TEST (
      P_EXPID          NUMBER,
      O_RESULT     OUT  Gk_Cv_Types.GenericCursorType
  	 )
	 AS
	 	 	v_sql VARCHAR2(4000);
	 BEGIN


		 --v_sql := ' Select a,b,c,d,e,f,g,h,i,j from er_temp o ';
		 v_sql := ' Select PK_EVENTCOST,
		 	   	  		   EVENTCOST_VALUE,
						   FK_EVENT,
						   FK_COST_DESC,
						   FK_CURRENCY
						 FROM sch_eventcost ';

		 --execute

		 OPEN O_RESULT FOR v_sql ;


	 END SP_EXP_TEST;


 PROCEDURE SP_EXP_STUDYSUM (
 	  P_EXPID          NUMBER,
      O_RESULT     OUT  Gk_Cv_Types.GenericCursorType,
        O_ARMRESULT     OUT  Gk_Cv_Types.GenericCursorType,
        O_SETTINGS OUT  Gk_Cv_Types.GenericCursorType

   )
   /* Author: Sonia Sahni
   Date: 23rd Jan 2004
   Purpose - Returns a ref cursor of study summary data
   */

   AS
    v_study VARCHAR2(4000);
	v_sql VARCHAR2(4000);

	v_expdetails CLOB;
	v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

	v_studyarm VARCHAR2(4000);
	v_settings VARCHAR2(1000);

    BEGIN
		 --get export request

		 SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

		 --find out the study id from the export request

  		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

 		 --v_study := 7246;


		 -- sql for study summary

		 v_sql := ' Select pk_study, study_title, study_obj, study_sum,
		 	           (SELECT lkpview_name FROM ER_LKPVIEW WHERE pk_lkpview  = study_advlkp_ver ) study_advlkp_viewname ,
		 	   	  ( SELECT codelst_subtyp FROM ER_CODELST WHERE CODELST_TYPE = ''tarea'' AND pk_codelst = fk_codelst_tarea) codelst_tarea, STUDY_PUBCOLLST,
				  study_prodname, Pkg_Util.f_getCodeSubtypes(study_disease_site,'','','','')  study_dis_site_subtyp, study_icdcode1,study_icdcode2,study_nsamplsize,study_dur,study_durunit,
				  study_estbegindt, ( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_phase)  fk_codelst_phase_subtyp,
				  ( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype)  fk_codelst_restype_subtyp,
  				  ( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_scope)  fk_codelst_scope_subtyp,
   				  ( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_blind)  fk_codelst_blind_subtyp,
				  ( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_random)  fk_codelst_random_subtyp, study_sponsor ,study_contact,
				    study_info,
					( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_type)  fk_codelst_type_subtype,study_keywrds,
					( SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = study_division)  study_division_subtype
		 	   	  FROM ER_STUDY
		 	   	  WHERE pk_study =  :1';

			-- for more study ids

			v_settings := ' select settings_modnum,settings_modname,settings_keyword,settings_value
					   	  	FROM ER_SETTINGS WHERE settings_modnum =  :1';


		-- for study TX arm
		 v_studyarm :=  ' Select pk_studytxarm, fk_study, tx_name, tx_drug_info, tx_desc from er_studytxarm where fk_study = :1';

		 --execute

		 OPEN O_RESULT FOR v_sql USING v_study ;

		 OPEN O_ARMRESULT FOR v_studyarm USING v_study ;

		 OPEN  O_SETTINGS FOR v_settings USING v_study ;

    END SP_EXP_STUDYSUM;


   PROCEDURE SP_EXP_STUDYVER (
      P_EXPID          NUMBER,
      O_RESVER     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_APNDX     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_SEC     OUT  Gk_Cv_Types.GenericCursorType
  	 )
	 /* Author: Sonia Sahni
   Date: 23rd Jan 2004
   Purpose - Returns a ref cursors for study versions data
   */

   AS
    v_study VARCHAR2(4000);
	v_sql_ver LONG;

	v_ver_ids LONG;
	v_ver_count NUMBER;
	v_success NUMBER;

	v_sql_sec LONG;
	v_sql_apndx LONG;

	v_sql_ver_select VARCHAR2(4000);
	v_sql_sec_select VARCHAR2(4000);
	v_sql_apndx_select VARCHAR2(4000);

	v_expdetails CLOB;
	v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();


    BEGIN

 		 --get export request

		 SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

		 --find out the study id from the export request

  		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

		 Pkg_Impexcommon.SP_GETMODULEKEYS(sys.XMLTYPE.createXML(v_expdetails),'study_ver',v_modkeys,v_success );

		 -- find out versions to be exported

		 v_ver_count := v_modkeys.COUNT();

		 IF v_ver_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_ver_ids);
		 END IF;



		 -- sql for study summary

		 --v_study := 7246;
		 --v_ver_count := 0;

		 v_sql_ver_select := ' Select pk_studyver,fk_study, studyver_number, studyver_notes ,status_date, ( SELECT codelst_subtyp FROM ER_CODELST WHERE CODELST_TYPE = ''studyvertype'' AND pk_codelst = studyver_type)  studyver_type_subtype,
		 ( SELECT codelst_subtyp FROM ER_CODELST WHERE CODELST_TYPE = ''studyvercat'' AND pk_codelst = studyver_category)  studyver_category_subtype, TO_DATE(TO_CHAR(studyver_date,'''||PKG_DATEUTIL.F_GET_DATEFORMAT||'''),'''||PKG_DATEUTIL.F_GET_DATEFORMAT||''' ) studyver_date';

		 v_sql_sec_select:= ' Select pk_studysec,fk_studyver,studysec_num ,studysec_name,studysec_pubflag ,studysec_seq,
		    studysec_contents2, studysec_contents3, studysec_contents4, studysec_contents5 , studysec_contents,studysec_text';

		 v_sql_apndx_select := ' Select pk_studyapndx, fk_studyver, studyapndx_uri ,  studyapndx_desc,
		     studyapndx_pubflag,   studyapndx_file, studyapndx_type,studyapndx_fileobj,
			 studyapndx_filesize ';


		 IF v_ver_count > 1 THEN

		    v_sql_ver := v_sql_ver_select || ' from er_studyver, er_status_history where fk_study = ' || v_study ||
					  ' and pk_studyver in (' || v_ver_ids || ') and PK_STUDYVER =  STATUS_MODPK and
					  STATUS_MODTABLE = ''ER_STUDYVER'' AND   STATUS_END_DATE IS NULL';

			v_sql_sec := v_sql_sec_select || ' from er_studysec where fk_studyver in (' || v_ver_ids || ')';

			v_sql_apndx := v_sql_apndx_select || ' from er_studyapndx where fk_studyver in (' || v_ver_ids || ')';


		 ELSIF v_ver_count = 1 THEN

		    v_sql_ver := v_sql_ver_select || ' from er_studyver, er_status_history  where pk_studyver = '|| v_ver_ids ||
			' and PK_STUDYVER =  STATUS_MODPK and   STATUS_MODTABLE = ''er_studyver'' and   STATUS_END_DATE is null';

 		    v_sql_sec := v_sql_sec_select || ' from er_studysec where fk_studyver = '|| v_ver_ids ;

			v_sql_apndx := v_sql_apndx_select || ' from er_studyapndx where fk_studyver = '|| v_ver_ids ;

		 ELSIF v_ver_count = 0 THEN	--get all study versions

		    v_sql_ver := v_sql_ver_select || ' from er_studyver , er_status_history,er_codelst  where fk_study = ' || v_study ||
 			' and PK_STUDYVER =  STATUS_MODPK and   STATUS_MODTABLE = ''er_studyver'' and   STATUS_END_DATE is null and
			 pk_codelst = FK_CODELST_STAT AND CODELST_SUBTYP = ''F'' ';

  		    v_sql_sec := v_sql_sec_select || ' from er_studysec, er_studyver,er_status_history ,er_codelst 	WHERE  ER_STUDYVER.fk_study = '
			 || v_study || ' AND  fk_studyver = pk_studyver and PK_STUDYVER =  STATUS_MODPK and   STATUS_MODTABLE = ''er_studyver'' and   STATUS_END_DATE is null and
			 pk_codelst = FK_CODELST_STAT AND CODELST_SUBTYP = ''F'' '  ;

			v_sql_apndx := v_sql_apndx_select || ' from er_studyapndx, er_studyver,er_status_history ,er_codelst  WHERE  ER_STUDYVER.fk_study = ' || v_study ||
			 ' AND  fk_studyver = pk_studyver  and PK_STUDYVER =  STATUS_MODPK and   STATUS_MODTABLE = ''er_studyver'' and   STATUS_END_DATE is null and
			 pk_codelst = FK_CODELST_STAT AND CODELST_SUBTYP = ''F'' '  ;

		 END IF;


		 --execute

  	 	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYVER : v_sql_ver ' || v_sql_ver , v_success );
		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYVER : v_sql_sec ' || v_sql_sec , v_success );
 		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYVER : v_sql_apndx ' || v_sql_apndx, v_success );

		 OPEN O_RESVER FOR v_sql_ver ;
		 OPEN O_RES_SEC FOR v_sql_sec ;
 		 OPEN O_RES_APNDX FOR v_sql_apndx ;


    END SP_EXP_STUDYVER;

  PROCEDURE SP_EXP_PATSTUDYFORMS (
      P_EXPID          NUMBER,
      O_RESFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_MAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_LINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
  	  O_RES_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType
  	 )
	 /* Author: Sonia Sahni
   Date: 23rd Jan 2004
   Purpose - Returns a ref cursors for patient study forms data
   Modified by Sonia Abrol, 09/15/05 to export only latest form versions
   */

   AS
    v_study VARCHAR2(4000);
	v_account VARCHAR2(4000);

	v_active_formstat NUMBER;

	v_form_ids LONG;
	v_form_count NUMBER;

	v_sql_formlib LONG;
	v_sql_mapform LONG;
	v_sql_linkform LONG;
	v_sql_mapform_count VARCHAR2(4000);
	v_sql_formlib_ver LONG;
	v_sql_form_refresh LONG;

	v_sql_form_select VARCHAR2(4000);
	v_sql_mapform_select VARCHAR2(4000);
	v_sql_linkform_select VARCHAR2(4000);
	v_sql_form_refresh_select VARCHAR2(4000);

	v_sql_mapform_count_select VARCHAR2(4000);


	v_missingmap_cur Gk_Cv_Types.GenericCursorType;
	v_refreshform_cur Gk_Cv_Types.GenericCursorType;
	V_FORMPK_CUR Gk_Cv_Types.GenericCursorType;

	v_formpk_list LONG;
	v_formpk NUMBER;

	v_missingform NUMBER;
	v_refreshform NUMBER;
	v_refreshformclob CLOB;

	v_formtype CHAR(2);
	v_success NUMBER;

	v_expdetails CLOB;
	v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

	v_formpk_select LONG;
	v_formpk_sql LONG;

	V_FORMSEC     LONG;
	V_FORMFLD     LONG;
	V_REPFORMFLD LONG;
	V_FLDRESP LONG;
	V_FLDVALIDATE LONG;
	V_REPFLDVALIDATE LONG;
	V_FLDACTION LONG;
	V_FLDACTIONINFO LONG;

	v_sql_formlib_ver_select LONG;

    BEGIN
 		 --get export request

		 SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

		 --find out the study id from the export request

  		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

		 Pkg_Impexcommon.SP_GETMODULEKEYS(sys.XMLTYPE.createXML(v_expdetails),'study_forms',v_modkeys,v_success );

		 -- find out versions to be exported

		 v_form_count := v_modkeys.COUNT();

		 IF v_form_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_form_ids);
		 END IF;


		 -- get v_account from the export request

   		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'account',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);


		-- v_study := 7246;
		 --v_form_count := 0;

		 --v_account := 1557;

		 -- find out formsto be exported


		 --get 'Aative' Status'for form

		 SELECT pk_codelst
		 INTO v_active_formstat
		 FROM ER_CODELST
		 WHERE trim(codelst_type) = 'frmstat' AND trim(codelst_subtyp) = 'A';



		 -- generate form mapping if it does not exist


		 v_sql_form_select := ' Select e.pk_formlib pk_formlib, e.fk_catlib fk_catlib,e.form_name form_name, e.form_desc form_desc,''A'' form_status_subtype, e.form_linkto form_linkto,
		 				   	    e.form_xsl form_xsl, e.form_xslrefresh form_xslrefresh, e.form_viewxsl form_viewxsl, e.form_xml.getClobVal() form_xml, e.form_sharedwith,
								e.FORM_CUSTOM_JS, e.FORM_KEYWORD ,e.FORM_ACTIVATION_JS      ';

		 v_sql_mapform_select:= ' Select pk_mp , fk_form , mp_formtype , mp_mapcolname , mp_systemid , mp_keyword , mp_uid ,
		 						mp_dispname , mp_origsysid,
		 						mp_flddatatype , mp_browser , mp_sequence';

		 v_sql_linkform_select := 'Select pk_lf, fk_formlib, lf_displaytype , lf_entrychar , er_linkedforms.fk_account fk_account, er_linkedforms.fk_study fk_study,
		 					    lf_lnkfrom , fk_calendar , fk_event , fk_crf,
								LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT ';

		 v_sql_formlib_ver_select := 'Select a.PK_FORMLIBVER,  a.FK_FORMLIB, a.FORMLIBVER_NUMBER,  a.FORMLIBVER_NOTES,    a.FORMLIBVER_DATE,
							 a.FORMLIBVER_XML.getClobVal() FORMLIBVER_XML , a.FORMLIBVER_XSL,  a.FORMLIBVER_VIEWXSL ';

    	 --v_sql_mapform_count_select := 'Select pk_formlib, LF_DISPLAYTYPE ';

		 --v_sql_form_refresh_select := 'Select pk_formlib ' ;

		 v_formpk_select := 'Select pk_formlib ' ;

		 IF v_form_count > 1 THEN

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e where e.pk_formlib in (' || v_form_ids || ')';

			v_sql_linkform := v_sql_linkform_select || ' from er_linkedforms where fk_formlib in (' || v_form_ids || ')';

			v_sql_mapform := v_sql_mapform_select || ' from er_mapform where fk_form in (' || v_form_ids || ')';

			v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver a , (SELECT MAX(i.pk_formlibver) pk , i.fk_formlib FROM ER_FORMLIBVER i  WHERE i.fk_FORMLIB IN (' || v_form_ids || ') GROUP BY fk_formlib) ids  where a.FK_FORMLIB in (' || v_form_ids || ') AND pk_formlibver = ids.pk';


			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib Where
			pk_formlib in (' || v_form_ids || ') and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

			v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e where e.pk_formlib in (' || v_form_ids || ') and FORM_XSLREFRESH = 1'; */

		 ELSIF v_form_count = 1 THEN

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e where e.pk_formlib = ' || v_form_ids ;

 		    v_sql_linkform := v_sql_linkform_select || ' from er_linkedforms where fk_formlib = ' || v_form_ids ;

			v_sql_mapform := v_sql_mapform_select || ' from er_mapform where fk_form = '|| v_form_ids ;

			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib Where
			pk_formlib = ' || v_form_ids || ' and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

		   v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e where e.pk_formlib = ' || v_form_ids || ' and FORM_XSLREFRESH = 1'; */

		   v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver a where FK_FORMLIB = '|| v_form_ids || ' and pk_formlibver = ( select max(pk_formlibver) from er_formlibver where fk_FORMLIB = '|| v_form_ids ||  ') ';

		 ELSIF v_form_count = 0 THEN	--get all sorts study forms

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e , er_linkedforms
						 WHERE  ER_LINKEDFORMS.fk_study = ' || v_study
				|| ' and LF_DISPLAYTYPE in (''S'',''SP'')  AND	 e.FORM_STATUS  = ' || v_active_formstat || ' and
				  fk_formlib = e.pk_formlib AND
				  NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D''' ;


  		    v_sql_linkform := v_sql_linkform_select || ' from er_formlib e ,er_linkedforms  where
				   ER_LINKEDFORMS.fk_study = ' || v_study	|| ' AND LF_DISPLAYTYPE IN (''S'',''SP'') AND
				   e.FORM_STATUS  = ' || v_active_formstat || ' AND
				  fk_formlib = e.pk_formlib AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D''' ;

			v_sql_mapform := v_sql_mapform_select || ' from er_formlib e ,er_mapform, er_linkedforms
						  WHERE ER_LINKEDFORMS.fk_study = ' || v_study	|| ' AND LF_DISPLAYTYPE IN (''S'',''SP'') AND
				    e.FORM_STATUS  = ' || v_active_formstat || ' AND
				  fk_formlib = e.pk_formlib AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' AND   fk_formlib = fk_form ';

			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib, er_linkedforms
				where (( er_linkedforms.fk_study = ' || v_study || ' and LF_DISPLAYTYPE in (''S'',''SP'')) OR
				 (LF_DISPLAYTYPE in (''SA'',''PS'') and er_linkedforms.fk_account = ' || v_account || ')) AND
				  fk_formlib = pk_formlib and nvl(er_formlib.record_type,''N'') <> ''D'' and FORM_STATUS  = ' || v_active_formstat || ' and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

			v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e , er_linkedforms  where (( er_linkedforms.fk_study = ' || v_study
				|| ' and LF_DISPLAYTYPE in (''S'',''SP'')) OR
				 (LF_DISPLAYTYPE in (''SA'',''PS'') and er_linkedforms.fk_account = ' || v_account || ')) AND
				  fk_formlib = e.pk_formlib and e.FORM_STATUS  = ' || v_active_formstat ||
				  ' and e.FORM_XSLREFRESH = 1 and nvl(e.record_type,''N'') <> ''D''' ; */

			v_formpk_sql := v_formpk_select || 	' from er_formlib e , er_linkedforms  where  er_linkedforms.fk_study = ' || v_study
				|| ' and LF_DISPLAYTYPE in (''S'',''SP'')	and e.FORM_STATUS  = ' || v_active_formstat ||
				  ' and fk_formlib = e.pk_formlib and nvl(er_linkedforms.record_type,''N'') <> ''D'' ' ;

		   v_sql_formlib_ver := v_sql_formlib_ver_select || '  from  er_formlibver a  ,    (select max(pk_formlibver) pk, a.fk_formlib  from er_formlibver a , er_formlib e , er_linkedforms
						  WHERE ER_LINKEDFORMS.fk_study = ' || v_study	|| ' AND LF_DISPLAYTYPE IN (''S'',''SP'') AND
				    e.FORM_STATUS  = ' || v_active_formstat || ' AND
				  ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' AND
				   a.fk_formlib = ER_LINKEDFORMS.FK_FORMLIB GROUP BY a.fk_formlib)  dat WHERE a.pk_formlibver = dat.pk  ';


		 END IF;

		 -- generate data for er_mapform for forms that do not have this data ready

		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_PATSTUDYFORMS : v_sql_formlib_ver' || v_sql_formlib_ver , v_success );
		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_PATSTUDYFORMS : v_sql_mapform_count' || v_sql_mapform_count , v_success );

		 /*OPEN v_missingmap_cur for v_sql_mapform_count;
		 LOOP
		 		 FETCH v_missingmap_cur INTO  v_missingform,v_formtype;
				 EXIT WHEN v_missingmap_cur %NOTFOUND;
				 PKG_IMPEXLOGGING.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_PATSTUDYFORMS : v_missingform' || v_missingform , v_success );
 				 PKG_IMPEXLOGGING.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_PATSTUDYFORMS : v_formtype' || v_formtype, v_success );
				 PKG_FORMREP.SP_GENFORMLAYOUT(v_missingform, v_formtype, v_success);
						--check v_success and log

		 End Loop;

		 Close v_missingmap_cur;


		 OPEN v_refreshform_cur for v_sql_form_refresh;
		 LOOP
		 	 FETCH v_refreshform_cur  INTO  v_refreshform;
			 EXIT WHEN v_refreshform_cur %NOTFOUND;
			 PKG_FORM.SP_CLUBFORMXSL(v_refreshform , v_refreshformclob );

		 END LOOP;

		 Close v_refreshform_cur ; */

		 --execute

    	 OPEN O_RESFORM FOR v_sql_formlib ;
		 OPEN O_RES_MAPFORM FOR v_sql_mapform ;
 		 OPEN O_RES_LINKFORM FOR v_sql_linkform ;
		 OPEN O_RES_FORMLIBVER FOR v_sql_formlib_ver;


		 -- execute the form id sql to export form data

		 IF v_form_count = 0 THEN
			 OPEN V_FORMPK_CUR FOR v_formpk_sql;
			 LOOP
			 	 FETCH V_FORMPK_CUR INTO v_formpk;
				 EXIT WHEN V_FORMPK_CUR %NOTFOUND;
				  	 v_formpk_list := v_formpk_list || ',' || v_formpk;
			 END LOOP;

			 v_formpk_list := SUBSTR(v_formpk_list,2);

			 CLOSE V_FORMPK_CUR;
		ELSE
			 v_formpk_list := v_form_ids;

		END IF;

		 -- get SQLs for the form field data

	 	Pkg_Impexforms.SP_GET_FORMFLD_EXP_SQLS(v_formpk_list,V_FORMSEC ,V_FORMFLD,V_REPFORMFLD ,V_FLDRESP ,V_FLDVALIDATE ,
			V_REPFLDVALIDATE, V_FLDACTION, V_FLDACTIONINFO );

		 --execute

		  OPEN O_FORMSEC  FOR  V_FORMSEC;
	      OPEN O_FORMFLD     FOR  V_FORMFLD;
		  OPEN O_REPFORMFLD FOR V_REPFORMFLD;
	  	  OPEN O_FLDRESP FOR V_FLDRESP;
	  	  OPEN O_FLDVALIDATE FOR V_FLDVALIDATE;
	  	  OPEN O_REPFLDVALIDATE FOR V_REPFLDVALIDATE;
		  OPEN O_RES_FLDACTION FOR V_FLDACTION;
		  OPEN O_RES_FLDACTIONINFO FOR V_FLDACTIONINFO;

    END SP_EXP_PATSTUDYFORMS;

	-----------------------------------------------------------------
	------------------------------for sch modules

   PROCEDURE SP_EXP_STUDYPROT (
      P_EXPID          NUMBER,
      O_RES_CAL     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_PROTSTAT     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_EVENTCRF     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFMAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_CRFLINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
      O_CRF_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
	  O_CRF_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
  	  O_CRF_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType,
	  O_PROTOCOL_VISITS OUT Gk_Cv_Types.GenericCursorType
  	 )
 /* Author: Sonia Sahni
   Date: 23rd Jan 2004
   Purpose - Returns a ref cursors for study versions data
   */
   AS
   	v_study VARCHAR2(4000);
	v_sql_cal LONG;
	v_cal_ids LONG;
	v_cal_count NUMBER;
	v_sql_protstat LONG;
	v_sql_eventcrf LONG;
	v_sql_cal_select VARCHAR2(4000);


	v_sql_protstat_select VARCHAR2(4000);
	v_sql_eventcrf_select VARCHAR2(4000);

	v_sql_form_select LONG;
	v_sql_mapform_select LONG;
	v_sql_linkform_select LONG;
	v_sql_mapform_count_select LONG;
	v_sql_form_refresh_select LONG;
	v_sql_visit_select VARCHAR2(4000);

	v_sql_formlib LONG;
	v_sql_linkform LONG;
	v_sql_mapform LONG;
	v_sql_mapform_count LONG;
	v_sql_form_refresh  LONG;

	v_sql_visit LONG;

	v_success NUMBER;

	v_expdetails CLOB;
	v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

	v_missingmap_cur Gk_Cv_Types.GenericCursorType;
	v_refreshform_cur Gk_Cv_Types.GenericCursorType;

	v_missingform NUMBER;
	v_refreshform NUMBER;
	v_refreshformclob CLOB;

	v_formtype CHAR(2);

	v_formpk_select LONG;
	v_formpk_sql LONG;
	v_formpk_list LONG;

	V_FORMSEC     LONG;
	V_FORMFLD     LONG;
	V_REPFORMFLD LONG;
	V_FLDRESP LONG;
	V_FLDVALIDATE LONG;
	V_REPFLDVALIDATE LONG;
	V_FLDACTION LONG;
	V_FLDACTIONINFO LONG;

	V_FORMPK_CUR Gk_Cv_Types.GenericCursorType;
	v_formpk NUMBER;


	v_sql_formlib_ver_select VARCHAR2(4000);
	v_sql_formlib_ver LONG;

	v_cdlst_stdActive number;


    BEGIN
 		
		 
--JM: 08FEB2011, #D-FIN9
select PK_CODELST into v_cdlst_stdActive from sch_codelst where CODELST_TYPE='calStatStd' and CODELST_SUBTYP='A'; 
		 
		 
		 --get export request

		 SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

		 --find out the study id from the export request

  		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);
		 Pkg_Impexcommon.SP_GETMODULEKEYS(sys.XMLTYPE.createXML(v_expdetails),'study_cal',v_modkeys,v_success );

		 -- find out versions to be exported

		 v_cal_count  := v_modkeys.COUNT();

		 IF v_cal_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_cal_ids);
		 END IF;



		 -- v_study := 7246;
		 -- v_cal_count := 0;

		 -- find out calendars to be exported
		 -- sql for protocol calendars
	v_sql_cal_select := ' Select a.event_id , a.chain_id , a.event_type , a.name , a.notes ,  a.cost,
	a.duration , a.fuzzy_period , a.description , a.displacement , a.event_flag , --JM: 08FEB11, D-FIN9 removed a.status 
	a.pat_daysbefore , a.pat_daysafter , a.usr_daysbefore , a.usr_daysafter , a.pat_msgbefore ,
	a.pat_msgafter , a.usr_msgbefore , a.usr_msgafter , a.status_dt, a.ORG_ID, a.duration_unit,a.fk_visit,a.event_cptcode, a.event_fuzzyafter, a.event_durationafter,a.event_durationbefore,
  a.event_calassocto, a.fk_codelst_calstat ';

    v_sql_visit_select := ' Select v.PK_PROTOCOL_VISIT   ,  v.FK_PROTOCOL  ,  v.VISIT_NO  ,  v.VISIT_NAME,  v.DESCRIPTION ,
					     v.DISPLACEMENT     ,  v.NUM_MONTHS     ,  v.NUM_WEEKS      , v. NUM_DAYS ,  v.INSERT_AFTER    ,
						     v.INSERT_AFTER_INTERVAL   ,  v.INSERT_AFTER_INTERVAL_UNIT  ,  v.PROTOCOL_TYPE   ,  v.VISIT_TYPE   ';

	v_sql_protstat_select:= 'Select pk_protstat , fk_event , FK_CODELST_CALSTAT , protstat_dt,protstat_note';

	v_sql_eventcrf_select := 'Select pk_crflib, fk_events ,crflib_number , crflib_name , crflib_flag,crflib_formflag';


	--for CRF


	 v_sql_form_select := ' Select e.pk_formlib pk_formlib, e.fk_catlib fk_catlib,e.form_name form_name, e.form_desc form_desc,''A'' form_status_subtype, e.form_linkto form_linkto,
		 				   	    e.form_xsl form_xsl, e.form_xslrefresh form_xslrefresh, e.form_viewxsl form_viewxsl, e.form_xml.getClobVal() form_xml, e.form_sharedwith,
								e.FORM_CUSTOM_JS, e.FORM_KEYWORD ,e.FORM_ACTIVATION_JS ';

	 v_sql_mapform_select:= ' Select pk_mp , fk_form , mp_formtype , mp_mapcolname , mp_systemid , mp_keyword , mp_uid ,
		 						mp_dispname , mp_origsysid,
		 						mp_flddatatype , mp_browser , mp_sequence';

	 v_sql_linkform_select := 'Select pk_lf, fk_formlib, lf_displaytype , lf_entrychar , er_linkedforms.fk_account fk_account, er_linkedforms.fk_study fk_study,
		 					    lf_lnkfrom , fk_calendar , ER_LINKEDFORMS.fk_event fk_event, ER_LINKEDFORMS.fk_crf fk_crf,
								LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT ';

   	 v_sql_mapform_count_select := 'Select pk_formlib, LF_DISPLAYTYPE ';

	 v_sql_form_refresh_select := 'Select pk_formlib ' ;

	 v_formpk_select := 'Select pk_formlib ' ;

	 v_sql_formlib_ver_select := 'Select v.PK_FORMLIBVER,  v.FK_FORMLIB, v.FORMLIBVER_NUMBER,  v.FORMLIBVER_NOTES,    v.FORMLIBVER_DATE,
							 v.FORMLIBVER_XML.getClobVal() FORMLIBVER_XML, v.FORMLIBVER_XSL, v.FORMLIBVER_VIEWXSL ';


	-----------------
	 IF v_cal_count > 1 THEN
	   v_sql_cal := v_sql_cal_select || ' from event_assoc a where a.chain_id = ' || v_study ||
					  ' and a.event_id in (' || v_cal_ids || ') UNION ' ||
			v_sql_cal_select || ' from event_assoc a where a.chain_id in (' || v_cal_ids || ')';


	   ----JM: 08FEB2011, #D-FIN9
	   --v_sql_protstat := v_sql_protstat_select || ' from sch_protstat where fk_event in (' || v_cal_ids || ') and protstat = ''A'' ';
	   v_sql_protstat := v_sql_protstat_select || ' from sch_protstat where fk_event in (' || v_cal_ids || ') and  FK_CODELST_CALSTAT = '||v_cdlst_stdActive;


	   v_sql_eventcrf := v_sql_eventcrf_select || ' from sch_crflib , event_assoc where
				 chain_id IN (' || v_cal_ids || ') AND fk_events = event_id ';

      v_sql_visit := v_sql_visit_select || ' from sch_protocol_visit v where fk_protocol in (' || v_cal_ids || ')';


	   v_sql_formlib := v_sql_form_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib';

	  v_sql_linkform := v_sql_linkform_select || ' from  sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib';


	  v_sql_mapform := v_sql_mapform_select || ' from er_mapform  , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = ER_MAPFORM.fk_form';

	  v_sql_mapform_count := v_sql_mapform_count_select || '  from er_formlib , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = pk_formlib AND
				 NOT EXISTS (SELECT 1 FROM ER_MAPFORM WHERE
					   fk_form = pk_formlib)';

	  v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND FORM_XSLREFRESH = 1';


	  v_formpk_sql  := v_sql_form_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
			 event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib';

	 v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver v, sch_crflib , event_assoc, 	 er_linkedforms
	 				  WHERE event_assoc.chain_id IN (' || v_cal_ids || ') AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = v.FK_FORMLIB';

	  ELSIF v_cal_count = 1 THEN
	    v_sql_cal := v_sql_cal_select  || ' from event_assoc a where a.chain_id = ' || v_study ||
					  ' and a.event_id = ' || v_cal_ids || ' UNION ' ||
			v_sql_cal_select || ' from event_assoc a where a.chain_id = ' || v_cal_ids ;

	    v_sql_protstat := v_sql_protstat_select || ' from sch_protstat where fk_event = ' || v_cal_ids || ' and FK_CODELST_CALSTAT = '||v_cdlst_stdActive;

   	    v_sql_eventcrf := v_sql_eventcrf_select || ' from sch_crflib , event_assoc where
				 chain_id = ' || v_cal_ids || ' AND fk_events = event_id' ;

       v_sql_visit := v_sql_visit_select || ' from sch_protocol_visit  v  where fk_protocol = ' || v_cal_ids ;

	   v_sql_formlib := v_sql_form_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib';

	   v_sql_linkform := v_sql_linkform_select || ' from  sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib';


  	  v_sql_mapform := v_sql_mapform_select || ' from er_mapform  , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = ER_MAPFORM.fk_form';

	  v_sql_mapform_count := v_sql_mapform_count_select || '  from er_formlib , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = pk_formlib AND
				 NOT EXISTS (SELECT 1 FROM ER_MAPFORM WHERE
					   fk_form = pk_formlib)';

	  v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND FORM_XSLREFRESH = 1';

	  v_formpk_sql  := v_sql_form_select || ' from er_formlib e , sch_crflib , event_assoc, er_linkedforms where
				 event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib ';

	  v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver v, sch_crflib , event_assoc,	 er_linkedforms
	 				  WHERE event_assoc.chain_id = ' || v_cal_ids || ' AND sch_crflib.fk_events = event_assoc.event_id AND
				 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = v.FK_FORMLIB';


	 ELSIF v_cal_count = 0 THEN	--get all protocol calendars
	   v_sql_cal := v_sql_cal_select || ' from event_assoc a where chain_id = ' || v_study ||
					  '  and a.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||'  UNION ' ||
			v_sql_cal_select || ' from event_assoc a, event_assoc b where	b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND a.chain_id = b.event_id';

  	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT:selecting all protocols ' || v_sql_cal , v_success );

  	   
	 v_sql_protstat := v_sql_protstat_select || ' from sch_protstat,event_assoc  Where
			chain_id = ' || v_study || ' AND event_assoc.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND fk_event = event_id  AND
			sch_protstat.FK_CODELST_CALSTAT = '||v_cdlst_stdActive ;

	 v_sql_visit := v_sql_visit_select || ' from event_assoc a , sch_protocol_visit  v where chain_id = ' || v_study ||
		  ' and a.FK_CODELST_CALSTAT = '||v_cdlst_stdActive ||' and fk_protocol = event_id'  ;

   	 
	 v_sql_eventcrf := v_sql_eventcrf_select || ' from sch_crflib , event_assoc a, event_assoc b where
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND a.chain_id = b.event_id AND
			fk_events = a.event_id' ;		
 	   

	v_sql_formlib := v_sql_form_select || ' from sch_crflib , event_assoc a, event_assoc b ,
	   	    ER_FORMLIB e ,ER_LINKEDFORMS
			WHERE	b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib
			AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' ';


	   v_sql_linkform := v_sql_linkform_select || ' from sch_crflib , event_assoc a,
	   		 event_assoc b , ER_LINKEDFORMS WHERE
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
		 ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' ';


   	   v_sql_mapform := v_sql_mapform_select || ' from sch_crflib , event_assoc a,
	   		 event_assoc b, ER_MAPFORM  , ER_LINKEDFORMS  WHERE
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND 
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = ER_MAPFORM.fk_form AND
			NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D''';

	   v_sql_mapform_count := v_sql_mapform_count_select || ' from sch_crflib , event_assoc a,
		    event_assoc b , ER_FORMLIB , ER_LINKEDFORMS
			WHERE
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT ='||v_cdlst_stdActive||' AND
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = pk_formlib
			AND NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' AND
				 NOT EXISTS (SELECT 1 FROM ER_MAPFORM WHERE
					   fk_form = pk_formlib)' ;



	   v_sql_form_refresh  := v_sql_form_refresh_select || ' from sch_crflib , event_assoc a,
	       event_assoc b, ER_FORMLIB e,ER_LINKEDFORMS  WHERE
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND 
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND
			ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND
			FORM_XSLREFRESH = 1' ;

		v_formpk_sql := v_formpk_select || 	' from sch_crflib , event_assoc a,
	       event_assoc b, ER_FORMLIB e,ER_LINKEDFORMS  WHERE
			b.chain_id = ' || v_study || ' AND b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive ||' AND 
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND
			NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' ' ;

		v_sql_formlib_ver := v_sql_formlib_ver_select ||
		  ' from er_formlibver v,sch_crflib , event_assoc a,	 event_assoc b, er_linkedforms
		    WHERE	b.chain_id = ' || v_study || ' b.FK_CODELST_CALSTAT = '||v_cdlst_stdActive||' AND
			a.chain_id = b.event_id AND
			sch_crflib.fk_events = a.event_id AND
			ER_LINKEDFORMS.fk_crf = sch_crflib.pk_crflib AND ER_LINKEDFORMS.fk_formlib = v.FK_FORMLIB AND
			NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D''';

	 END IF;
		 --execute

	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_cal ' || v_sql_cal , v_success );

	 OPEN O_RES_CAL FOR v_sql_cal ;

 	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_protstat ' || v_sql_protstat , v_success );

	 OPEN O_RES_PROTSTAT FOR v_sql_protstat ;

  	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_eventcrf' || v_sql_eventcrf , v_success );

	 OPEN O_RES_EVENTCRF FOR v_sql_eventcrf ;

	 --for CRF forms

   	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_mapform_count ' || v_sql_mapform_count , v_success );
   	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_form_refresh ' || v_sql_form_refresh , v_success );

 	 OPEN v_missingmap_cur FOR v_sql_mapform_count;
		 LOOP
		 		 FETCH v_missingmap_cur INTO  v_missingform,v_formtype;
				 EXIT WHEN v_missingmap_cur %NOTFOUND;
				 Pkg_Formrep.SP_GENFORMLAYOUT(v_missingform, v_formtype, v_success);

		 END LOOP;

		 CLOSE v_missingmap_cur;


		 OPEN v_refreshform_cur FOR v_sql_form_refresh;
		 LOOP
		 	 FETCH v_refreshform_cur  INTO  v_refreshform;
			 EXIT WHEN v_refreshform_cur %NOTFOUND;
			 Pkg_Form.SP_CLUBFORMXSL(v_refreshform , v_refreshformclob );

		 END LOOP;

	 CLOSE v_refreshform_cur ;

	 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT: v_sql_formlib ' || v_sql_formlib , v_success );

    	 OPEN O_CRFFORM FOR v_sql_formlib ;

	Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT : v_sql_mapform ' || v_sql_mapform , v_success );

   OPEN O_CRFMAPFORM FOR v_sql_mapform ;

   Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT : v_sql_linkform ' || v_sql_linkform , v_success );

   OPEN O_CRFLINKFORM FOR v_sql_linkform ;

   Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_EXP_STUDYPROT : v_sql_formlib_ver' || v_sql_formlib_ver , v_success );

   OPEN O_CRF_FORMLIBVER FOR v_sql_formlib_ver;


   		 -- execute the form id sql to export form data


			 OPEN V_FORMPK_CUR FOR v_formpk_sql;
			 LOOP
			 	 FETCH V_FORMPK_CUR INTO v_formpk;
				 EXIT WHEN V_FORMPK_CUR %NOTFOUND;
				  	 v_formpk_list := v_formpk_list || ',' || v_formpk;
			 END LOOP;

			 v_formpk_list := SUBSTR(v_formpk_list,2);

			 CLOSE V_FORMPK_CUR;

		 -- get SQLs for the form field data

	 	Pkg_Impexforms.SP_GET_FORMFLD_EXP_SQLS(v_formpk_list,V_FORMSEC ,V_FORMFLD,V_REPFORMFLD ,V_FLDRESP ,V_FLDVALIDATE ,
			V_REPFLDVALIDATE ,	V_FLDACTION , V_FLDACTIONINFO  );

		 --execute

		  OPEN O_FORMSEC  FOR  V_FORMSEC;
	      OPEN O_FORMFLD     FOR  V_FORMFLD;
		  OPEN O_REPFORMFLD FOR V_REPFORMFLD;
	  	  OPEN O_FLDRESP FOR V_FLDRESP;
	  	  OPEN O_FLDVALIDATE FOR V_FLDVALIDATE;
	  	  OPEN O_REPFLDVALIDATE FOR V_REPFLDVALIDATE;
		  OPEN O_CRF_FLDACTION FOR  V_FLDACTION ;
		  OPEN O_CRF_FLDACTIONINFO FOR  V_FLDACTIONINFO ;

		  OPEN O_PROTOCOL_VISITS FOR  V_SQL_VISIT ;

    END SP_EXP_STUDYPROT ;



	PROCEDURE SP_GET_REQDETAILS(
      P_EXPID          NUMBER,
      O_DETAILS     OUT  CLOB
  	)
	AS
	  v_detail CLOB;
	  v_success NUMBER;
	  v_sql LONG;

	BEGIN
		 BEGIN

		 	v_sql := ' Select e.EXP_XML.getClobVal() from er_expreqlog e where pk_expreqlog = :1';

			EXECUTE IMMEDIATE v_sql INTO v_detail USING P_EXPID;



		 EXCEPTION WHEN NO_DATA_FOUND  THEN
		 	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_GET_REQDETAILS : NO EXP REQUEST DETAILS FOUND', v_success );
		 END;

		 o_details := v_detail ;
	END;


  PROCEDURE SP_IMP_STUDY (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
  AS
  /** Author : Sonia Sahni
  	  Date: 02/20/04
	  Purpose: Import study data from temp tables
	  Note:

	  1. Do not need to insert a new status for the study.
	  Trigger ER_STUDY_AI0 will handle new status and super user group rights
	  2. Do not need to handle study team rights, trigger ER_STUDYTEAM_BI_ROLE will handle rights
	  according to Principal Inv role.


  */

  v_study_cur Gk_Cv_Types.GenericCursorType;
  v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

  v_orig_study NUMBER;
  v_new_study NUMBER;
  v_err VARCHAR2(4000);

  v_title VARCHAR2(1000);
  v_studyobj VARCHAR2(2000);
  v_studysum VARCHAR2(2000);
  v_studylkp_ver NUMBER;
  v_study_advlkp_viewname VARCHAR2(20);

  v_sql LONG;
  v_success NUMBER;
  V_EXPID NUMBER;
  V_expdatils CLOB;

  v_originating_sitecode VARCHAR2(10);
  v_sitecode VARCHAR2(10);
  v_count NUMBER;
  v_account VARCHAR2(50);
  v_studynumber VARCHAR2(100); --JM
  v_prinInv VARCHAR2(50);
  v_creator VARCHAR2(50);
  v_ipAdd VARCHAR2(15);
  v_prinv_role NUMBER;
  v_codelst_tarea VARCHAR2(15);
  v_studypubcol   VARCHAR2(2000);

  v_tarea_code NUMBER;

  v_ver_flag NUMBER := 0;
  o_studysitesuccess NUMBER:= 0;

  v_orig_pk_studytxarm NUMBER;
  v_tx_name VARCHAR2(250);
   v_tx_drug_info VARCHAR2(250);
   v_tx_desc VARCHAR2(4000);
   v_new_pk_studytxarm NUMBER;
    v_study_prodname VARCHAR2(100);
	v_study_dissite_subtyp VARCHAR2(2000); v_study_dissite_codes VARCHAR2(500);
	 v_study_icdcode1 VARCHAR2(2000);
	 v_study_icdcode2 VARCHAR2(2000);
	 v_study_nsamplsize NUMBER;
	 v_study_dur NUMBER;
	 v_study_durunit VARCHAR2(10);
	  v_study_estbegindt DATE;
	  v_phase_subtyp VARCHAR2(20); v_phase_code NUMBER;
	  v_restype_subtyp VARCHAR2(20); v_restype_code NUMBER;
	  v_fk_scope_subtyp VARCHAR2(20); v_scope_code NUMBER;
	  v_fk_blind_subtyp VARCHAR2(20); v_blind_code NUMBER;
	  v_random_subtyp VARCHAR2(20); v_random_code NUMBER;
	   v_study_sponsor VARCHAR2(200);
	 v_study_contact VARCHAR2(2000);
     v_study_info VARCHAR2(2000);
	  v_studytype_code NUMBER;
	  v_keywords VARCHAR2(500);
      v_codelst_type_subtype VARCHAR2(20);
      v_study_division_subtype VARCHAR2(20);
	  v_study_division NUMBER;

	v_studysettings_modname NUMBER;
	v_settingskey VARCHAR2(50);
	v_settingsvalue VARCHAR2(250);


  BEGIN
  	   -- get import request details

	   SP_GET_IMPREQDETAILS(P_IMPID, V_expdatils,v_expid);

	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;

		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get import account

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impAccountId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);
		END IF;

		--get import study number
		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'studyNumber',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_studynumber);
		END IF;

		--get import study Principal Investigator
		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'prinInv',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_prinInv);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;


		--get Principal Inv Role from codelst

		SELECT pk_codelst
		INTO v_prinv_role
		FROM ER_CODELST
		WHERE codelst_type = 'role' AND
		codelst_subtyp = 'role_prin';


		-- get data from the study temp table

		v_sql := 'Select  PK_STUDY , STUDY_TITLE , STUDY_OBJ , STUDY_SUM ,study_advlkp_viewname, codelst_tarea, STUDY_PUBCOLLST,
			  study_prodname,study_dis_site_subtyp, study_icdcode1,study_icdcode2,study_nsamplsize,study_dur,study_durunit,
 study_estbegindt,fk_codelst_phase_subtyp,fk_codelst_restype_subtyp,fk_codelst_scope_subtyp,fk_codelst_blind_subtyp,fk_codelst_random_subtyp, study_sponsor ,study_contact,  study_info,
  fk_codelst_type_subtype, study_keywrds,study_division_subtype
		 	      FROM IMPSTUDY0 ';



	 OPEN v_study_cur FOR v_sql;
		 LOOP
		 	 FETCH v_study_cur INTO  v_orig_study, v_title, v_studyobj, v_studysum, v_study_advlkp_viewname,v_codelst_tarea, v_studypubcol  ,
			   v_study_prodname,v_study_dissite_subtyp, v_study_icdcode1,v_study_icdcode2,v_study_nsamplsize,v_study_dur,v_study_durunit,
 v_study_estbegindt,v_phase_subtyp,v_restype_subtyp,v_fk_scope_subtyp,v_fk_blind_subtyp,v_random_subtyp, v_study_sponsor ,v_study_contact,  v_study_info ,v_codelst_type_subtype,
 v_keywords,v_study_division_subtype;
 			 EXIT WHEN v_study_cur %NOTFOUND;

			  -- find out if study version will be imported
			  Pkg_Impexcommon.SP_GETMODULEKEYS(sys.XMLTYPE.createXML(V_expdatils),'study_ver',v_modkeys,v_ver_flag );


			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_study, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);

			 --get therapeutic area

			 SELECT	getCodePk (v_codelst_tarea, 'tarea')
			 INTO v_tarea_code
			 FROM dual;

			 -- get study adverse events lookup view

			 BEGIN
				 SELECT  pk_lkpview
				 INTO   v_studylkp_ver
				 FROM ER_LKPVIEW WHERE LOWER(lkpview_name) = LOWER(v_study_advlkp_viewname);
			 EXCEPTION WHEN NO_DATA_FOUND THEN
				 v_studylkp_ver := NULL;
			END;


			 IF v_tarea_code = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : tarea, codelst_subtyp : ' || v_codelst_tarea, v_success );
				v_tarea_code  := NULL;
			 END IF;

			  --get disease sites

			 SELECT Pkg_Util.f_getMultipleCodePks ( v_study_dissite_subtyp,',' , ',' ,'disease_site') INTO  v_study_dissite_codes  FROM dual;

 			 IF LENGTH(NVL(v_study_dissite_codes,0)) <= 0  THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : disease_site, codelst_subtyp(s) : ' || v_study_dissite_subtyp, v_success );
				v_study_dissite_codes := NULL;
			 END IF;

			 -- get phase... v_phase_subtyp
 			 SELECT	getCodePk (v_phase_subtyp, 'phase')
			 INTO v_phase_code
			 FROM dual;

 			 IF v_phase_code = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''phase'', codelst_subtyp : ' || v_phase_subtyp, v_success );
				v_phase_code  := NULL;
			 END IF;

			 -- get research type v_restype_subtyp
			 SELECT	getCodePk (v_restype_subtyp, 'research_type')
			 INTO v_restype_code
			 FROM dual;

 			 IF v_restype_code = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''research_type'', codelst_subtyp : ' || v_restype_subtyp, v_success );
				v_restype_code  := NULL;
			 END IF;

			 -- get scope v_fk_scope_subtyp

		  	 SELECT	getCodePk (v_fk_scope_subtyp, 'studyscope')
			 INTO v_scope_code
			 FROM dual;

 			 IF v_scope_code = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''studyscope'', codelst_subtyp : ' || v_fk_scope_subtyp, v_success );
				v_scope_code  := NULL;
			 END IF;

			  ------- get blinding	  v_fk_blind_subtyp

			  	 SELECT	getCodePk (v_fk_blind_subtyp, 'blinding')
			 INTO v_blind_code
			 FROM dual;

 			 IF  v_blind_code  = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''blinding'', codelst_subtyp : ' || v_fk_blind_subtyp, v_success );
				v_blind_code := NULL;
			 END IF;

			 --get randomization v_random_subtyp

			  SELECT	getCodePk (v_random_subtyp, 'randomization')
			 INTO v_random_code
			 FROM dual;

 			 IF  v_random_code  = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''randomization'', codelst_subtyp : ' || v_random_subtyp, v_success );
				v_random_code := NULL;
			 END IF;

			 -- get study type		 v_ fk_codelst_type_subtype v_studytype_code
			  SELECT	getCodePk ( v_codelst_type_subtype, 'study_type')
			 INTO v_studytype_code
			 FROM dual;

 			 IF  v_studytype_code  = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''study_type'', codelst_subtyp : ' ||  v_codelst_type_subtype, v_success );
				v_studytype_code := NULL;
			 END IF;

			 --study division		 v_study_division_subtype
			   SELECT	getCodePk ( v_study_division_subtype, 'study_division')
			 INTO v_study_division
			 FROM dual;

 			 IF  v_study_division  = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Code not found for codelst_type : ''study_division'', codelst_subtyp : ' ||  v_study_division_subtype, v_success );
				v_study_division := NULL;
			 END IF;


			 IF v_new_study = 0 THEN -- new record

			 	SELECT seq_er_study.NEXTVAL
				INTO v_new_study
				FROM dual;

			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - New record with original PK:' || v_orig_study , v_success );


				Pkg_Impexcommon.SP_SET_SITEPK(v_orig_study, 'er_study',v_sitecode,  v_new_study,v_err,v_originating_sitecode);

				IF LENGTH(trim(v_err)) > 0 THEN

				   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY Could not create record for er_sitepkmap :' || v_err, v_success );

				ELSE
					BEGIN

						INSERT INTO ER_STUDY ( PK_STUDY, FK_ACCOUNT, STUDY_TITLE, STUDY_OBJ , STUDY_SUM , STUDY_PRINV,
							   				  FK_AUTHOR, CREATOR, CREATED_ON , STUDY_NUMBER,  IP_ADD ,
											  STUDY_ADVLKP_VER,study_pubflag, fk_codelst_tarea,STUDY_PUBCOLLST,
											  study_prodname,study_disease_site, study_icdcode1,study_icdcode2,study_nsamplsize,study_dur,study_durunit,
											   study_estbegindt,fk_codelst_phase,fk_codelst_restype,fk_codelst_scope,fk_codelst_blind,
											   fk_codelst_random, study_sponsor ,study_contact,  study_info,fk_codelst_type,study_keywrds,study_division
											  )
						VALUES (v_new_study,v_account,v_title,v_studyobj,v_studysum,v_prinInv,
							   v_prinInv,v_creator,SYSDATE,v_studynumber,v_ipAdd,v_studylkp_ver,'N',v_tarea_code , v_studypubcol , v_study_prodname, v_study_dissite_codes,
							    v_study_icdcode1,v_study_icdcode2,v_study_nsamplsize,v_study_dur,v_study_durunit, v_study_estbegindt, v_phase_code,
								v_restype_code ,  v_scope_code , v_blind_code,v_random_code,v_study_sponsor ,v_study_contact,  v_study_info,v_studytype_code,
								  v_keywords,v_study_division
							   ) ;




						-- INSERT NEW STUDY TEAM MEMBER

						INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM , FK_CODELST_TMROLE ,  FK_USER , FK_STUDY ,
				 		CREATOR ,  CREATED_ON , IP_ADD  , STUDY_TEAM_USR_TYPE  , STUDY_SITEFLAG  )
							 	VALUES(seq_er_studyteam.NEXTVAL,v_prinv_role,v_prinInv ,v_new_study,v_creator,SYSDATE,
					v_ipAdd,'D','S');

					--create super user acces

					--Pkg_Supuser.SP_GRANT_SUPUSER_FORSTUDY(v_account, v_new_study ,v_creator ,v_ipAdd, v_success);
					Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - added super user access ,original PK:' || v_orig_study , v_success );

					 -- add user's site as study site

					 INSERT INTO ER_STUDYSITES (PK_STUDYSITES, FK_STUDY,  FK_SITE,  STUDYSITE_REPINCLUDE ,	 CREATOR ,  CREATED_ON ,  IP_ADD)
					SELECT seq_er_studysites.NEXTVAL,v_new_study,fk_siteid,1,v_creator,SYSDATE,v_ipAdd
					FROM ER_USER WHERE pk_user = 	v_prinInv;


					   -- create study site rights data
					   Pkg_User.SP_CREATE_STUDYSITE_DATA   (v_prinInv,	 v_account ,	 v_new_study,v_creator,	v_ipAdd, o_studysitesuccess );
					   IF o_studysitesuccess < 0 THEN
					   			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY:Could not create study site rights data', v_success );
					   END IF;

						-- check if study version module is also to be imported, if not create a default study version

						IF v_ver_flag < 0 THEN

								Pkg_Studystat.SP_CREATE_DEFAULT_VERSION(v_new_study,v_creator,v_ipAdd);

						END IF;

					EXCEPTION WHEN OTHERS THEN
					  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY:Could not create row for Sponsor PK ' || v_orig_study || ' Error: ' || SQLERRM, v_success );
					END;
  				END IF;

			 ELSIF v_new_study > 0 THEN -- old record

			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - Old record with original PK:' || v_orig_study , v_success );

				UPDATE ER_STUDY
				SET FK_ACCOUNT = v_account,
				STUDY_TITLE = v_title ,
				STUDY_OBJ = v_studyobj , STUDY_SUM = v_studysum, STUDY_PRINV = v_prinInv,
				FK_AUTHOR = v_prinInv, LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE ,
				STUDY_NUMBER = v_studynumber,  IP_ADD = v_ipAdd,
			    STUDY_ADVLKP_VER = v_studylkp_ver, fk_codelst_tarea = v_tarea_code ,
				STUDY_PUBCOLLST = v_studypubcol,
				study_prodname = v_study_prodname , study_disease_site =  v_study_dissite_codes , study_icdcode1 = v_study_icdcode1,study_icdcode2 = v_study_icdcode2,
				study_nsamplsize = v_study_nsamplsize, study_dur = v_study_dur ,study_durunit = v_study_durunit,
				   study_estbegindt = v_study_estbegindt ,fk_codelst_phase = v_phase_code, fk_codelst_restype = v_restype_code , fk_codelst_scope = v_scope_code  , fk_codelst_blind = v_blind_code,
				   fk_codelst_random = v_random_code, study_sponsor = v_study_sponsor ,study_contact = v_study_contact,  study_info = v_study_info,
				   fk_codelst_type =  v_studytype_code,study_keywrds = v_keywords,study_division = v_study_division
				WHERE pk_study = v_new_study;

			 ELSIF v_new_study = -1 THEN

			  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY:' || v_err, v_success );

			 END IF;

		 END LOOP;

	 CLOSE v_study_cur ;

	COMMIT;

	-- for study TX ARM

	v_sql := ' Select pk_studytxarm, fk_study, tx_name, tx_drug_info, tx_desc from IMPSTUDY1 ';

	 OPEN v_study_cur FOR v_sql;
		 LOOP
		 	 FETCH v_study_cur INTO  v_orig_pk_studytxarm, v_orig_study,  v_tx_name,   v_tx_drug_info,   v_tx_desc  ;

			 EXIT WHEN v_study_cur %NOTFOUND;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_study, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_pk_studytxarm, 'er_studytxarm',v_sitecode, v_new_pk_studytxarm,v_err,v_originating_sitecode);
			 --------------------------------
			 	 IF v_new_study <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDY  Could not create study TX ARM, parent Study not found. original PK_STUDY : ' || v_orig_study, v_success );
			 ELSE
			 	 IF v_new_pk_studytxarm = 0 THEN -- new record

		  		 	 SELECT seq_er_study_tx_arm.NEXTVAL
						INTO v_new_pk_studytxarm FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY - New Study TX ARM record with new PK:' || v_new_pk_studytxarm , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_pk_studytxarm, 'er_studytxarm',v_sitecode,  v_new_pk_studytxarm,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY for studytxarmCould not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_STUDYTXARM (pk_studytxarm, fk_study, tx_name, tx_drug_info, tx_desc, CREATOR , CREATED_ON , IP_ADD )
							 VALUES (  v_new_pk_studytxarm  , v_new_study,  v_tx_name,   v_tx_drug_info,   v_tx_desc,v_creator, SYSDATE,v_ipAdd ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY:Could not create Study TX ARM record for Sponsor PK ' || v_orig_pk_studytxarm  || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF  v_new_pk_studytxarm > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDY  - Old Study TX ARM record with original PK:' || v_orig_pk_studytxarm  , v_success );

						UPDATE ER_STUDYTXARM
						SET  tx_name =  v_tx_name , tx_drug_info = v_tx_drug_info , tx_desc =v_tx_desc , LAST_MODIFIED_BY = v_creator ,
							LAST_MODIFIED_ON = SYSDATE ,IP_ADD = v_ipAdd
						WHERE pk_studytxarm = v_new_pk_studytxarm;


					 ELSIF v_new_pk_studytxarm = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY :' || v_err, v_success );

					 END IF;
			END IF; -- for parent study
			 			 ---------------------------
		 END LOOP;

	 CLOSE v_study_cur ;

	 -- for study ids

	 	v_sql := ' Select  settings_modnum,settings_modname,settings_keyword,settings_value  from IMPSTUDY2 ';

	 OPEN v_study_cur FOR v_sql;
		 LOOP
		 	 FETCH v_study_cur INTO   v_orig_study,  v_studysettings_modname,v_settingskey,v_settingsvalue ;

			 EXIT WHEN v_study_cur %NOTFOUND;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_study, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);
			 --------------------------------
			 	 IF v_new_study <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDY  Could not create study settings record , parent Study not found. original PK_STUDY : ' || v_orig_study, v_success );
			 ELSE

			 	 BEGIN
			 	 	  				--delete old setting
									DELETE FROM ER_SETTINGS WHERE settings_modnum = v_new_study AND  settings_keyword = v_settingskey;

									--check if v_settingsvalue is a number, for setting keyword = 'STUDY_ENROLL_FORWARD' , then make it nukk. Number will be a form id.

									IF  (v_settingskey = 'STUDY_ENROLL_FORWARD'  AND Pkg_Util.f_to_number(v_settingsvalue) > 0 ) THEN
													   v_settingsvalue := NULL;
									END IF;


									--insert new setting
									INSERT INTO ER_SETTINGS (settings_pk, settings_modnum,settings_modname,settings_keyword,settings_value)
									VALUES (seq_er_settings.NEXTVAL,v_new_study,  v_studysettings_modname,v_settingskey,v_settingsvalue);


			 	 EXCEPTION WHEN OTHERS THEN
					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDY : could not insert study settings:' || SQLERRM, v_success );

				 END ;



			END IF; -- for parent study
			 			 ---------------------------
		 END LOOP;

	 CLOSE v_study_cur ;
	 ---------------------------------------------------
	COMMIT;

  END;

 PROCEDURE SP_GET_IMPREQDETAILS(
      P_IMPID          NUMBER,
      O_DETAILS     OUT  CLOB,
	  O_EXPID OUT NUMBER
  	)
	AS
	  v_detail CLOB;
	  v_success NUMBER;
	  v_sql LONG;
	  v_expid NUMBER;

	BEGIN
		 BEGIN

		 	v_sql := ' Select e.EXP_XML.getClobVal(), e.EXP_ID from er_impreqlog e where pk_impreqlog = :1';

			EXECUTE IMMEDIATE v_sql INTO v_detail ,v_expid  USING P_IMPID;



		 EXCEPTION WHEN NO_DATA_FOUND  THEN
		 	  Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX.SP_GET_IMPREQDETAILS : NO EXP REQUEST DETAILS FOUND', v_success );
		 END;

		 o_details := v_detail ;
		 o_expid := v_expid ;

	END;


	FUNCTION getCodePk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2) RETURN NUMBER
	IS
	v_pk_code NUMBER;
	BEGIN

		BEGIN
			SELECT pk_codelst
			INTO v_pk_code
			FROM ER_CODELST
			WHERE trim(codelst_type) = trim(p_codetype) AND trim(codelst_subtyp) = trim(p_codelst_subtype);

			RETURN v_pk_code;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
			RETURN -1;
		END;

	END;

	------------------------------------------------------------------------------------

  PROCEDURE SP_IMP_STUDYVER (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
  AS
  /** Author : Sonia Sahni
  	  Date: 02/24/04
	  Purpose: Import study version data from temp tables
	  Note:
  */

  v_studyver_cur Gk_Cv_Types.GenericCursorType;
  v_studyapndx_cur Gk_Cv_Types.GenericCursorType;
  v_studysec_cur Gk_Cv_Types.GenericCursorType;

  v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

  v_orig_ver NUMBER;
  v_new_ver NUMBER;
  v_orig_study NUMBER;
  v_new_study NUMBER;

  v_sponsor_study VARCHAR2(100);

  v_studyver_number VARCHAR2(25);
  v_studyver_status CHAR(1);
  v_studyver_notes VARCHAR2(2000);

  v_err VARCHAR2(4000);

  v_studyver_sql LONG;
  v_section_sql LONG;
  v_appendix_sql LONG;

  v_success NUMBER;
  V_EXPID NUMBER;
  V_expdatils CLOB;

  v_sitecode VARCHAR2(10);
  v_originating_sitecode VARCHAR2(10);
  v_count NUMBER;

  v_creator VARCHAR2(50);
  v_ipAdd VARCHAR2(15);

  v_orig_pkapndx NUMBER;
  v_new_pkapndx NUMBER;
  v_apndx_uri VARCHAR2(300);
  v_apndx_desc VARCHAR2(200);
  v_apndx_pubflag CHAR(1);
  v_apndx_file VARCHAR2(500);
  v_apndx_type VARCHAR2(20);
  v_apndx_fileobj BLOB;
  v_apndx_filesize NUMBER(10);

  v_new_sec NUMBER;
  v_orig_sec NUMBER;
  v_sec_num VARCHAR2(50);
  v_sec_name VARCHAR2(100);
  v_sec_pubflag CHAR(1);
  v_sec_seq NUMBER;

  v_sec_contents2 VARCHAR2(4000);
  v_sec_contents3 VARCHAR2(4000);
  v_sec_contents4 VARCHAR2(4000);
  v_sec_contents5 VARCHAR2(4000);
  v_sec_contents VARCHAR2(4000);
  v_studysec_text CLOB;

  v_version_count NUMBER := 0;
  v_study_vercount NUMBER := 0;

  v_status_date DATE ;

   v_studyver_category_subtype VARCHAR2(20);
   v_studyver_type_subtype VARCHAR2(20);
   v_studyver_date DATE;
   v_studyver_category NUMBER;
   v_studyver_type NUMBER;

  BEGIN
  	   -- get import request details

	   SP_GET_IMPREQDETAILS(P_IMPID, V_expdatils,v_expid);
	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;


		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;

		-- get study for which we are importing


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'study',v_modkeys);
   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sponsor_study);
		END IF;

		-- get data from the study version temp table

		v_studyver_sql := 'Select  PK_STUDYVER, FK_STUDY, STUDYVER_NUMBER ,STUDYVER_NOTES, STATUS_DATE, studyver_category_subtype, studyver_type_subtype, studyver_date
		 FROM IMPSTUDY_VER0';


		v_appendix_sql := 'Select PK_STUDYAPNDX , FK_STUDYVER, STUDYAPNDX_URI, STUDYAPNDX_DESC, STUDYAPNDX_PUBFLAG,
				 STUDYAPNDX_FILE , STUDYAPNDX_TYPE ,STUDYAPNDX_FILEOBJ , STUDYAPNDX_FILESIZE
				 FROM IMPSTUDY_VER1';


		v_section_sql := ' Select PK_STUDYSEC , FK_STUDYVER , STUDYSEC_NUM , STUDYSEC_NAME  , STUDYSEC_PUBFLAG ,
		STUDYSEC_SEQ,  STUDYSEC_CONTENTS2 , STUDYSEC_CONTENTS3 , STUDYSEC_CONTENTS4  ,STUDYSEC_CONTENTS5 ,
		STUDYSEC_CONTENTS  , studysec_text FROM IMPSTUDY_VER2';

		-- import study version


	 OPEN v_studyver_cur FOR v_studyver_sql;
	 	 LOOP

		 	 FETCH v_studyver_cur INTO v_orig_ver, v_orig_study, v_studyver_number, v_studyver_notes,v_status_date,v_studyver_category_subtype, v_studyver_type_subtype, v_studyver_date ;

			 EXIT WHEN v_studyver_cur %NOTFOUND;

			 v_version_count := v_version_count + 1;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_study, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);
 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_ver, 'er_studyver',v_sitecode, v_new_ver,v_err,v_originating_sitecode);

			  SELECT	getCodePk (v_studyver_category_subtype, 'studyvercat')
			 INTO v_studyver_category
			 FROM dual;

			  SELECT	getCodePk (v_studyver_type_subtype, 'studyvertype')
			 INTO  v_studyver_type
			 FROM dual;


			 P('v_new_study' || v_new_study);
			 IF v_new_study <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create study version, parent Study not found. original PK_STUDY : ' || v_orig_study, v_success );
			 P('v_success ' || v_success );
			 ELSE
			 	 IF v_new_ver = 0 THEN -- new record
		  		 	 SELECT seq_er_studyver.NEXTVAL
						INTO v_new_ver	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - New Study Version record with original PK:' || v_orig_ver , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_ver, 'er_studyver',v_sitecode,  v_new_ver,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_STUDYVER (PK_STUDYVER , FK_STUDY , STUDYVER_NUMBER ,
								   					STUDYVER_NOTES , CREATOR , CREATED_ON , IP_ADD, studyver_category,studyver_type,studyver_date  )
							 VALUES (v_new_ver,v_new_study,v_studyver_number,
								  v_studyver_notes,v_creator, SYSDATE,v_ipAdd , v_studyver_category, v_studyver_type, v_studyver_date) ;

							-- insert study version history record
							  INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
							  	STATUS_MODPK ,	STATUS_MODTABLE,
								FK_CODELST_STAT ,
								STATUS_DATE ,
								CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD)
        						VALUES ( seq_er_status_history.NEXTVAL,
									   v_new_ver,'er_studyver',
									   Pkg_Util.f_getCodePk('F','versionStatus'),
							  v_status_date,
							  v_creator,'N',SYSDATE,v_ipAdd );

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:Could not create Study Version record for Sponsor PK ' || v_orig_ver || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_ver > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - Old Study Version record with original PK:' || v_orig_ver , v_success );

						UPDATE ER_STUDYVER
						SET STUDYVER_NUMBER = v_studyver_number,
		  					STUDYVER_NOTES = v_studyver_notes, LAST_MODIFIED_BY = v_creator ,
							LAST_MODIFIED_DATE = SYSDATE ,IP_ADD = v_ipAdd , studyver_category = v_studyver_category,studyver_type = v_studyver_type,studyver_date = v_studyver_date
					    WHERE pk_studyver = v_new_ver;



					 ELSIF v_new_ver = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study
		 END LOOP;

	 CLOSE v_studyver_cur ;

	 --import study apendix -------------------------

	  OPEN v_studyapndx_cur FOR v_appendix_sql;

		 LOOP
		 	 FETCH v_studyapndx_cur INTO 	v_orig_pkapndx, v_orig_ver, v_apndx_uri, v_apndx_desc, v_apndx_pubflag,
			 v_apndx_file, v_apndx_type, v_apndx_fileobj, v_apndx_filesize ;

			 EXIT WHEN v_studyapndx_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_ver, 'er_studyver',v_sitecode, v_new_ver,v_err,v_originating_sitecode);
 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_pkapndx, 'er_studyapndx',v_sitecode, v_new_pkapndx,v_err,v_originating_sitecode);

			 IF v_new_ver <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create study appendix, parent Study Version not found. original PK_STUDYVER : ' || v_orig_ver, v_success );
			 ELSE
			 	 IF v_new_pkapndx = 0 THEN -- new record
		  		 	 SELECT seq_er_studyapndx.NEXTVAL
						INTO v_new_pkapndx	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - New Appendix record with original PK:' || v_orig_pkapndx, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_pkapndx, 'er_studyapndx',v_sitecode,  v_new_pkapndx,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_STUDYAPNDX(PK_STUDYAPNDX, FK_STUDYVER , STUDYAPNDX_URI, STUDYAPNDX_DESC,STUDYAPNDX_PUBFLAG,
							 	STUDYAPNDX_FILE ,STUDYAPNDX_TYPE  , STUDYAPNDX_FILEOBJ , STUDYAPNDX_FILESIZE ,
							   CREATOR , CREATED_ON , IP_ADD  )
							 VALUES (v_new_pkapndx,v_new_ver,v_apndx_uri, v_apndx_desc, v_apndx_pubflag,
							 v_apndx_file, v_apndx_type, v_apndx_fileobj, v_apndx_filesize ,
							 v_creator, SYSDATE,v_ipAdd ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:Could not create Study Appendix record for Sponsor PK ' || v_orig_pkapndx || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_pkapndx > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - Old Study Appendix record with original PK:' || v_orig_pkapndx , v_success );

						UPDATE ER_STUDYAPNDX
						SET STUDYAPNDX_URI = v_apndx_uri, STUDYAPNDX_DESC = v_apndx_desc,
						STUDYAPNDX_PUBFLAG = v_apndx_pubflag, STUDYAPNDX_FILE = v_apndx_file,
						STUDYAPNDX_TYPE = v_apndx_type  , STUDYAPNDX_FILEOBJ = v_apndx_fileobj ,
						STUDYAPNDX_FILESIZE = v_apndx_filesize,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd
					    WHERE pk_studyapndx  = v_new_pkapndx;

					 ELSIF v_new_pkapndx = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study
		 END LOOP;

	 CLOSE v_studyapndx_cur ;

	 -- import study sections

	  	 OPEN v_studysec_cur FOR v_section_sql;

		 LOOP
		 	 FETCH v_studysec_cur INTO v_orig_sec, v_orig_ver, v_sec_num, v_sec_name, v_sec_pubflag, v_sec_seq ,
			 v_sec_contents2,v_sec_contents3,v_sec_contents4,v_sec_contents5,v_sec_contents , v_studysec_text;


			 EXIT WHEN v_studysec_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_ver, 'er_studyver',v_sitecode, v_new_ver,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_sec, 'er_studysec',v_sitecode, v_new_sec,v_err,v_originating_sitecode);

			 IF v_new_ver <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create study section, parent Study Version not found. original PK_STUDYVER : ' || v_orig_ver, v_success );
			 ELSE
			 	 IF v_new_sec = 0 THEN -- new record
		  		 	 SELECT seq_er_studysec.NEXTVAL
						INTO v_new_sec	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - New Study Section record with original PK:' || v_orig_sec, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_sec, 'er_studysec',v_sitecode,  v_new_sec,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_STUDYSEC (PK_STUDYSEC ,FK_STUDYVER ,STUDYSEC_NAME ,STUDYSEC_PUBFLAG ,
							 	STUDYSEC_SEQ , STUDYSEC_CONTENTS2  ,STUDYSEC_CONTENTS3 ,STUDYSEC_CONTENTS4 ,
								STUDYSEC_CONTENTS5, STUDYSEC_CONTENTS ,STUDYSEC_NUM,
							   CREATOR , CREATED_ON , IP_ADD, studysec_text  )

							 VALUES (v_new_sec, v_new_ver, v_sec_name, v_sec_pubflag, v_sec_seq,
			 				 v_sec_contents2,v_sec_contents3,v_sec_contents4,v_sec_contents5,v_sec_contents,
							 v_sec_num, v_creator, SYSDATE,v_ipAdd,v_studysec_text ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:Could not create Study Section record for Sponsor PK ' || v_orig_sec || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_sec > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER - Old Study Section record with original PK:' || v_orig_sec , v_success );

						UPDATE ER_STUDYSEC
						SET STUDYSEC_NAME = v_sec_name,STUDYSEC_PUBFLAG = v_sec_pubflag  ,
						STUDYSEC_SEQ = v_sec_seq , STUDYSEC_CONTENTS2 = v_sec_contents2 ,
						STUDYSEC_CONTENTS3 = v_sec_contents3,STUDYSEC_CONTENTS4 = v_sec_contents4,
						STUDYSEC_CONTENTS5 = v_sec_contents5, STUDYSEC_CONTENTS = v_sec_contents ,
						STUDYSEC_NUM = v_sec_num ,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd, studysec_text = v_studysec_text
					    WHERE pk_studysec  = v_new_sec;

					 ELSIF v_new_sec = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study
		 END LOOP;

		-- if there were no study versions

		IF v_version_count = 0 THEN
		   -- get the new study id and insert a default version

		    Pkg_Impexcommon.SP_GET_SITEPK(v_sponsor_study, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);

			-- get th version count for this study
			SELECT COUNT(*)
			INTO v_study_vercount
			FROM ER_STUDYVER WHERE fk_study = v_new_study;

			IF v_study_vercount = 0 THEN
				-- insert the default version

				/*	INSERT INTO ER_STUDYVER (PK_STUDYVER,  FK_STUDY,  STUDYVER_NUMBER,STUDYVER_STATUS,
												      CREATOR,  CREATED_ON, IP_ADD )
				VALUES (seq_er_studyver.NEXTVAL,  v_new_study, '1', 'W',v_creator,  SYSDATE,v_ipAdd); */

				 Pkg_Studystat.SP_CREATE_DEFAULT_VERSION (v_new_study,v_creator, v_ipAdd);

		    END IF;
		END IF;

	 CLOSE v_studysec_cur;

	 ------------------
	COMMIT;

  END;

------------------------------------------------------------------------------
  PROCEDURE SP_IMP_FORMS (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
  AS
  /** Author : Sonia Sahni
  	  Date: 02/25/04
	  Purpose: Import forms data from temp tables
	  Note:
  */

  v_form_cur Gk_Cv_Types.GenericCursorType;
 -- v_mapform_cur gk_cv_types.GenericCursorType;
  v_lf_cur Gk_Cv_Types.GenericCursorType;
  v_formlibver_cur Gk_Cv_Types.GenericCursorType;

  v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

  v_orig_form NUMBER;
  v_new_form NUMBER;

  v_formname VARCHAR2(50);
  v_formdesc VARCHAR2(255);
  v_formstatus NUMBER;
  v_codelst_formstat VARCHAR2(15);
  v_formlinkto CHAR(1);
  v_formxsl CLOB;
  v_form_xslrefresh NUMBER;
  v_form_viewxsl CLOB;
  v_formxml CLOB;
  v_sharedwith CHAR(1);

  v_orig_mapform NUMBER;
  v_new_mapform NUMBER;
  v_mpformtype  VARCHAR2(2);

  v_mapcolname VARCHAR2(100);
  v_mpsystemid  VARCHAR2(100);
  v_mpkeyword VARCHAR2(255);
  v_mpuid VARCHAR2(100);
  v_mpdispname VARCHAR2(500);
  v_mporigsysid VARCHAR2(100);
  v_mpflddatatype VARCHAR2(2);
  v_mpbrowser VARCHAR2(2);
  v_mpsequence NUMBER;



  v_err VARCHAR2(4000);

  v_form_sql LONG;
--  v_mapform_sql Long;
  v_lf_sql LONG;

  v_success NUMBER;

  V_EXPID NUMBER;
  V_expdatils CLOB;

  v_sitecode VARCHAR2(10);
  v_originating_sitecode VARCHAR2(10);
  v_count NUMBER;

  v_creator VARCHAR2(50);
  v_ipAdd VARCHAR2(15);

  v_orig_lf NUMBER;
  v_new_lf NUMBER;

  v_lfdisplaytype CHAR(2);
  v_lfentrychar CHAR(1);
  v_lfaccount NUMBER;
  v_lffkstudy NUMBER;
  v_lflnkfrom CHAR(1);

  v_new_study NUMBER;
  v_account VARCHAR2(100);

  o_out_field NUMBER;
  o_out_formfld NUMBER;

  v_form_xmltype sys.XMLTYPE;

  v_form_keyword VARCHAR2(50);
  v_form_custom_js CLOB;
  v_form_activation_js CLOB;

  v_lf_datacnt NUMBER;
  v_lf_hide NUMBER;
  v_lf_seq NUMBER;
  v_lf_display_inpat NUMBER;

  v_formlibver_sql LONG;

  v_orig_formlibver NUMBER;
  v_new_formlibver NUMBER;
  v_formlibver_number VARCHAR2(25);
  v_formlibver_notes VARCHAR2(2000);
  v_formlibver_date DATE;
  v_formlibver_xml sys.XMLTYPE;
  v_formlibver_xmlclob CLOB;
  v_formlibver_xsl CLOB;
  v_formlibver_viewxsl CLOB;

  v_importedforms Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();


  BEGIN
  	   -- get import request details

	   SP_GET_IMPREQDETAILS(P_IMPID, V_expdatils,v_expid);
	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;

		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;

		--get import account

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impAccountId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);
		END IF;


		-- get data from the form temp table


		v_form_sql := 'Select   PK_FORMLIB, FORM_NAME , FORM_DESC , FORM_LINKTO,
				    FORM_XSL, FORM_XSLREFRESH, FORM_VIEWXSL , FORM_XML, form_status_subtype, form_sharedwith,
					FORM_CUSTOM_JS, FORM_KEYWORD ,FORM_ACTIVATION_JS
		 	      FROM IMPSTUDY_FORMS0';

		/*v_mapform_sql := 'Select PK_MP , FK_FORM , MP_FORMTYPE  , MP_MAPCOLNAME , MP_SYSTEMID , MP_KEYWORD ,
					  MP_UID , MP_DISPNAME , MP_ORIGSYSID , MP_FLDDATATYPE , MP_BROWSER  , MP_SEQUENCE
					  from impstudy_forms1'; */


		v_lf_sql := ' Select PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT, FK_STUDY,
 				 	 LF_LNKFROM, LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT
					 FROM IMPSTUDY_FORMS2';

		v_formlibver_sql := 'Select PK_FORMLIBVER,  FK_FORMLIB, FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
						 FORMLIBVER_DATE, FORMLIBVER_XML , FORMLIBVER_XSL,  FORMLIBVER_VIEWXSL 	FROM IMPSTUDY_FORMS9';


		-- import form

	 OPEN v_form_cur FOR v_form_sql;
	 	 LOOP

		 	 FETCH v_form_cur INTO  v_orig_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
				  	v_form_xslrefresh, v_form_viewxsl, v_formxml ,v_codelst_formstat, v_sharedwith,
					v_form_custom_js,v_form_keyword,v_form_activation_js ;

			 EXIT WHEN v_form_cur %NOTFOUND;

 			 SELECT	getCodePk (v_codelst_formstat, 'frmstat')
			 INTO v_formstatus
			 FROM dual;

			 IF v_formstatus = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Code not found for codelst_type : frmstat, codelst_subtyp : ' || v_codelst_formstat, v_success );
				v_formstatus  := NULL;
			 END IF;



			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

			-- if v_orig_form <= 0 then
			  --	  PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYVER Could not create study version, parent Study not found. original PK_STUDY : ' || v_orig_study, v_success );
			 --else

			  IF v_new_form = 0 THEN -- new record

					 SELECT seq_er_formlib.NEXTVAL
						INTO v_new_form	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - New Form record with original PK:' || v_orig_form , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_form, 'er_formlib',v_sitecode,  v_new_form,v_err,
						v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							v_form_xmltype := NULL;

							BEGIN
								IF v_formxml IS NOT NULL THEN
							   	   SELECT XMLTYPE(v_formxml)
								   INTO v_form_xmltype FROM dual;
							    END IF;
							EXCEPTION WHEN OTHERS THEN
								 v_form_xmltype := NULL;
							END;

							 INSERT INTO ER_FORMLIB ( PK_FORMLIB , FORM_NAME ,  FORM_DESC,
            				 			 			 FORM_LINKTO,  FORM_XSL ,
													 FORM_XSLREFRESH , FORM_XML,
													 CREATOR, CREATED_ON,IP_ADD, FORM_STATUS, FK_ACCOUNT, RECORD_TYPE,
													 FORM_SHAREDWITH ,FORM_CUSTOM_JS,
													 FORM_KEYWORD, FORM_ACTIVATION_JS )
							 VALUES (v_new_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
							 	  	1, v_form_xmltype ,
								    v_creator, SYSDATE,v_ipAdd , v_formstatus, v_account,'N',v_sharedwith,
									v_form_custom_js,v_form_keyword,v_form_activation_js) ;

							 INSERT INTO ER_FORMSTAT(pk_formstat,fk_formlib,fk_codelst_stat,formstat_stdate,
							 formstat_changedby,record_type,creator,created_on,ip_add)
							 VALUES(seq_er_formstat.NEXTVAL,v_new_form, v_formstatus,SYSDATE,v_creator,'N',v_creator,
							 SYSDATE,v_ipAdd );

							 --add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
							 v_importedforms.EXTEND;
							 v_importedforms(v_importedforms.LAST) := v_new_form;



							-- insert form default date
							--PKG_FORM.SP_FORMDEFAULTDATA ( v_new_form, o_out_field, o_out_formfld  );

							--update er_formlib set form_xslrefresh = 0 where  PK_FORMLIB = v_new_form;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:Could not create Form record for Sponsor PK ' || v_orig_form || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_form > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Old Form record with original PK:' || v_orig_form, v_success );

						v_form_xmltype := NULL;

						BEGIN
							IF v_formxml IS NOT NULL THEN
						   	   SELECT XMLTYPE(v_formxml)
							   INTO v_form_xmltype FROM dual;
						    END IF;
						EXCEPTION WHEN OTHERS THEN
								 v_form_xmltype := NULL;
						END;



						UPDATE ER_FORMLIB
						SET FORM_NAME = v_formname,  FORM_DESC = v_formdesc, FORM_LINKTO = v_formlinkto,
						  FORM_XSL = v_formxsl , FORM_XSLREFRESH = 1,
						   FORM_VIEWXSL = NULL , FORM_XML =  v_form_xmltype ,
							LAST_MODIFIED_BY = v_creator ,
							LAST_MODIFIED_DATE = SYSDATE ,IP_ADD = v_ipAdd,
							FORM_STATUS = v_formstatus ,FK_ACCOUNT = v_account, RECORD_TYPE = 'M',
							FORM_SHAREDWITH = v_sharedwith,FORM_CUSTOM_JS = v_form_custom_js,
							 FORM_KEYWORD = v_form_keyword, FORM_ACTIVATION_JS = v_form_activation_js
					    WHERE pk_formlib = v_new_form;

					   --add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
						 v_importedforms.EXTEND;
						 v_importedforms(v_importedforms.LAST) := v_new_form;


					 ELSIF v_new_form = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:' || v_err, v_success );

					END IF;

		 END LOOP;

	 CLOSE v_form_cur ;

	 --import mapformdata -------------------------

	 /* OPEN v_mapform_cur for v_mapform_sql;

		 LOOP
		 	 FETCH v_mapform_cur INTO  v_orig_mapform, v_orig_form, v_mpformtype, v_mapcolname, v_mpsystemid, v_mpkeyword, v_mpuid, v_mpdispname,
					  v_mporigsysid, v_mpflddatatype, v_mpbrowser, v_mpsequence;

			 EXIT WHEN v_mapform_cur  %NOTFOUND;

			 PKG_IMPEXCOMMON.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err);
			 PKG_IMPEXCOMMON.SP_GET_SITEPK(v_orig_mapform, 'er_mapform',v_sitecode, v_new_mapform,v_err);

			 if v_new_form <= 0 then
			  	  PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_mapform, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 else
			 	 if v_new_mapform = 0 then -- new record
		  		 	 Select seq_er_mapform.nextval
						into v_new_mapform	from dual;

					 	PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORM - New Mapform record with original PK:' || v_orig_mapform, v_success );

						PKG_IMPEXCOMMON.SP_SET_SITEPK(v_orig_mapform, 'er_mapform',v_sitecode,  v_new_mapform,v_err);

						if length(trim(v_err)) > 0 then

						  PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						else
							Begin

							 INSERT INTO ER_MAPFORM( PK_MP , FK_FORM ,  MP_FORMTYPE ,  MP_MAPCOLNAME ,
							 		 MP_SYSTEMID ,  MP_KEYWORD , MP_UID ,  MP_DISPNAME ,
									 MP_ORIGSYSID , MP_FLDDATATYPE, MP_BROWSER, MP_SEQUENCE )
							 VALUES (v_new_mapform,v_new_form,v_mpformtype, v_mapcolname,
							 		v_mpsystemid, v_mpkeyword, v_mpuid, v_mpdispname,
							  v_mporigsysid, v_mpflddatatype, v_mpbrowser, v_mpsequence) ;

							Exception When Others then
							  PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:Could not create Mapform record for Sponsor PK ' || v_orig_mapform || ' Error: ' || sqlerrm, v_success );
							End;
		  				end if;

					 elsif v_new_mapform > 0 then -- old record

					 	PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Old Mapform record with original PK:' || v_orig_mapform , v_success );

						Update ER_MAPFORM
						set MP_FORMTYPE =  v_mpformtype,  MP_MAPCOLNAME = v_mapcolname ,
							 		 MP_SYSTEMID = v_mpsystemid ,  MP_KEYWORD = v_mpkeyword , MP_UID = v_mpuid,
									  MP_DISPNAME = v_mpdispname,   MP_ORIGSYSID = v_mporigsysid ,
									  MP_FLDDATATYPE = v_mpflddatatype, MP_BROWSER = v_mpbrowser,
									  MP_SEQUENCE = v_mpsequence
					    Where pk_mp  = v_new_mapform;

					 elsif v_new_mapform = -1 then

					  	 PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:' || v_err, v_success );

					 end if;
			end if; -- for parent Form
		 END LOOP;

	 CLOSE v_mapform_cur ;

	 */

	 -- import Linked forms records

	  	 OPEN v_lf_cur FOR v_lf_sql;

		 LOOP
		 	 FETCH v_lf_cur INTO v_orig_lf,v_orig_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount, v_lffkstudy,
			  v_lflnkfrom , v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat;

			 EXIT WHEN v_lf_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode, v_new_lf,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

		--	 PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS v_lffkstudy' || v_lffkstudy, v_success );

			 IF LENGTH(trim(NVL(v_lffkstudy,''))) > 0 THEN
			 	 Pkg_Impexcommon.SP_GET_SITEPK(v_lffkstudy, 'er_study',v_sitecode, v_new_study,v_err,v_originating_sitecode);
				 --PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS v_new_study: ' || v_new_study || ' v_lffkstudy' || v_lffkstudy, v_success );
			 ELSE
			 	 v_new_study := NULL;
			 END IF;

			 IF LENGTH(trim(NVL(v_lfaccount,''))) > 0 THEN
			 	v_lfaccount := v_account;
			 ELSE
			 	 v_lfaccount := NULL;
			 END IF;



			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create linked form record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSIF v_new_study = 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create linked form record, parent Study not found. original PK_STUDY : ' || v_lffkstudy, v_success );
			 ELSE

			 	 IF v_new_lf = 0 THEN -- new record
		  		 	 SELECT seq_er_linkedforms.NEXTVAL
						INTO v_new_lf	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - New Linked Forms record with original PK:' || v_orig_lf, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode,  v_new_lf,v_err,
						v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 --PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS before insert v_new_study' || v_new_study, v_success );

							 INSERT INTO ER_LINKEDFORMS (PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT,
							 FK_STUDY, LF_LNKFROM ,  CREATOR , CREATED_ON , IP_ADD , RECORD_TYPE ,
			  				 	 LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT )
							 VALUES (v_new_lf, v_new_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount, v_new_study,
					 			  v_lflnkfrom,  v_creator, SYSDATE,v_ipAdd,'N' ,
								  v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:Could not create Linked Forms record for Sponsor PK ' || v_orig_lf || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_lf > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Old Linked Forms record with original PK:' || v_orig_lf , v_success );

						UPDATE ER_LINKEDFORMS
						SET LF_DISPLAYTYPE = v_lfdisplaytype, LF_ENTRYCHAR =  v_lfentrychar, FK_ACCOUNT =  v_lfaccount,
							 FK_STUDY = v_new_study, LF_LNKFROM = v_lflnkfrom,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd,
						 RECORD_TYPE = 'M', LF_DATACNT = v_lf_datacnt, LF_HIDE = v_lf_hide , LF_SEQ = v_lf_seq,
						  LF_DISPLAY_INPAT = v_lf_display_inpat
					    WHERE pk_lf  = v_new_lf;

					 ELSIF v_new_lf = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_lf_cur;


	 ---------------- import forlib version data

	  	 OPEN v_formlibver_cur FOR v_formlibver_sql;

		 LOOP

		 	 FETCH v_formlibver_cur INTO v_orig_formlibver,v_orig_form,v_formlibver_number, v_formlibver_notes, v_formlibver_date,
			 v_formlibver_xmlclob, v_formlibver_xsl,v_formlibver_viewxsl	;

			 EXIT WHEN v_formlibver_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode, v_new_formlibver,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

		--	 PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS v_lffkstudy' || v_lffkstudy, v_success );

			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create formlib version record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSE

				--prepare xmltype

				v_formlibver_xml := NULL;

					BEGIN
						IF v_formlibver_xmlclob IS NOT NULL THEN
				   	       SELECT XMLTYPE(v_formlibver_xmlclob)
						    INTO v_formlibver_xml FROM dual;
					    END IF;
					EXCEPTION WHEN OTHERS THEN
						  v_formlibver_xml := NULL;
					END;


			 	 IF v_new_formlibver = 0 THEN -- new record

		  		 	 SELECT seq_er_formlibver.NEXTVAL
						INTO v_new_formlibver FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - New Formlib version record with original PK:' || v_orig_formlibver, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode,  v_new_formlibver,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN


							  INSERT INTO ER_FORMLIBVER (PK_FORMLIBVER,  FK_FORMLIB,  FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
							  FORMLIBVER_DATE,  FORMLIBVER_XML,   FORMLIBVER_XSL,   FORMLIBVER_VIEWXSL,
							  CREATOR, RECORD_TYPE, CREATED_ON ,IP_ADD )
							 VALUES (v_new_formlibver,v_new_form, v_formlibver_number, v_formlibver_notes, v_formlibver_date,
							 v_formlibver_xml,v_formlibver_xsl,v_formlibver_viewxsl,
					 			 v_creator, 'N' ,SYSDATE,v_ipAdd) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:Could not create FormLib ver record for Sponsor PK ' || v_orig_formlibver || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_formlibver > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Old Formlib ver record with original PK:' || v_orig_formlibver , v_success );

						UPDATE ER_FORMLIBVER
						SET  FK_FORMLIB = v_new_form,  FORMLIBVER_NUMBER = v_formlibver_number ,  FORMLIBVER_NOTES =  v_formlibver_notes,
							  FORMLIBVER_DATE = v_formlibver_date,  FORMLIBVER_XML = v_formlibver_xml,
							  FORMLIBVER_XSL = v_formlibver_xsl,   FORMLIBVER_VIEWXSL = v_formlibver_viewxsl,
							  RECORD_TYPE = 'M', LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd
						WHERE PK_FORMLIBVER = v_new_formlibver;

					 ELSIF v_new_formlibver = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_formlibver_cur;

	 ----------------


	 -- import form fields

	  V_ERR := '';

	  Pkg_Impexforms.SP_IMP_FORMFLDS('study_forms',V_EXPID,V_expdatils, V_ERR , O_SUCCESS);

	  IF LENGTH(trim(v_err)) > 0 THEN
		  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYFORMS Could not import form fields data :' || v_err, v_success );
	  END IF;

	  -- after complete forms import, regenerate mpform,xsls, xmls for each form
	  -- sonia, 12/22/04
	  Pkg_Impexforms.sp_regen_formstruct(V_EXPID, v_importedforms,'PKG_IMPEX.SP_IMP_STUDYFORMS');

	COMMIT;

  END;

---------------------------------------------------------------------------------------------------------------------


 PROCEDURE SP_IMP_STUDYCAL (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
   v_cursor Gk_Cv_Types.GenericCursorType;

   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

  v_orig_event NUMBER;
  v_new_event NUMBER;
  v_orig_study NUMBER;
  v_new_study NUMBER;

  v_success NUMBER;

  V_EXPID NUMBER;
  V_expdatils CLOB;

  v_sitecode VARCHAR2(10);
  v_originating_sitecode VARCHAR2(10);
  v_count NUMBER;
  v_creator VARCHAR2(50);
  v_ipAdd VARCHAR2(15);
  v_account VARCHAR2(100);

  v_sql LONG;

  v_orig_chain NUMBER;
  v_new_chain NUMBER;
  v_event_type CHAR(1);
  v_event_name VARCHAR2(50);
  v_event_notes VARCHAR2(1000);
  v_event_cost NUMBER;
  v_event_duration NUMBER;
  v_event_fuzzy VARCHAR2(10);
  v_event_status CHAR(1);
  v_event_desc VARCHAR2(1000);
  v_event_displacement NUMBER;
  v_event_flag NUMBER;
  v_event_pat_daysbefore NUMBER;
  v_event_pat_daysafter NUMBER;
  v_event_usr_daysbefore NUMBER;
  v_event_usr_daysafter NUMBER;
  v_event_pat_msgbefore VARCHAR2(4000);
  v_event_pat_msgafter VARCHAR2(4000);
  v_event_usr_msgbefore VARCHAR2(4000);
  v_event_usr_msgafter VARCHAR2(4000);
  v_event_statusdt DATE;
  v_err VARCHAR2(4000);


  v_orig_crf NUMBER;
  v_new_crf NUMBER;

  v_crfnumber   VARCHAR2(50);
  v_crfname   VARCHAR2(100);
  v_crfflag   CHAR(1);
  v_crfformflag NUMBER;

  v_orig_form NUMBER;
  v_new_form NUMBER;

  v_formname VARCHAR2(50);
  v_formdesc VARCHAR2(255);
  v_formstatus NUMBER;
  v_codelst_formstat VARCHAR2(15);
  v_formlinkto CHAR(1);
  v_formxsl CLOB;
  v_form_xslrefresh NUMBER;
  v_form_viewxsl CLOB;
  v_formxml CLOB;
  v_sharedwith CHAR(1);


  v_orig_mapform NUMBER;
  v_new_mapform NUMBER;
  v_mpformtype  VARCHAR2(2);

  v_mapcolname VARCHAR2(100);
  v_mpsystemid  VARCHAR2(100);
  v_mpkeyword VARCHAR2(255);
  v_mpuid VARCHAR2(100);
  v_mpdispname VARCHAR2(500);
  v_mporigsysid VARCHAR2(100);
  v_mpflddatatype VARCHAR2(2);
  v_mpbrowser VARCHAR2(2);
  v_mpsequence NUMBER;

  v_orig_lf NUMBER;
  v_new_lf NUMBER;

  v_lfdisplaytype CHAR(2);
  v_lfentrychar CHAR(1);
  v_lfaccount NUMBER;
  v_lffkstudy NUMBER;
  v_lflnkfrom CHAR(1);

  v_lfcal NUMBER;
  v_lfevent NUMBER;
  v_lfcrf NUMBER;

  v_form_xmltype sys.XMLTYPE;

  v_orgid NUMBER;
  v_formlibver_sql LONG;

  v_orig_formlibver NUMBER;
  v_new_formlibver NUMBER;
  v_formlibver_number VARCHAR2(25);
  v_formlibver_notes VARCHAR2(2000);
  v_formlibver_date DATE;
  v_formlibver_xml sys.XMLTYPE;
  v_formlibver_xmlclob CLOB;
  v_formlibver_xsl CLOB;
  v_formlibver_viewxsl CLOB;

  v_form_keyword VARCHAR2(50);
  v_form_custom_js CLOB;
  v_form_activation_js CLOB;
  v_lf_datacnt NUMBER;
  v_lf_hide NUMBER;
  v_lf_seq NUMBER;
  v_lf_display_inpat NUMBER;

  v_duration_unit CHAR(1);
  v_orig_fk_visit NUMBER;

   v_new_pk_visit NUMBER;
   v_old_pk_visit NUMBER;
   v_visit_no NUMBER;
    v_visit_name  VARCHAR2(50);
	v_description VARCHAR2(200);
	 v_displacement NUMBER;
	 v_num_months NUMBER;
	 v_num_weeks NUMBER;
	 v_num_days NUMBER;
     v_insert_after NUMBER;
	 v_insert_after_new NUMBER;
     v_insert_after_interval NUMBER;
	  v_insert_after_interval_unit CHAR(1);
	  v_protocol_type CHAR(1);
	  v_visit_type CHAR(1);
	  v_event_cptcode VARCHAR2(50);
	   v_event_fuzzyafter VARCHAR2(10);
	    v_event_durationafter CHAR(1);
		v_event_durationbefore CHAR(1);
		v_event_calassocto  VARCHAR2(255);
		
		v_codeLstId number;--JM: 08FEB2011, #D-FIN9

     v_parent_visits Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
     v_child_visits Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

  v_importedforms Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

 BEGIN
 	     -- get import request details

	   SP_GET_IMPREQDETAILS(P_IMPID, V_expdatils,v_expid);
	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;

		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;

		--get import account

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impAccountId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);
		END IF;


		-- get data from the form temp tables

		--get protocol calendar and events
		-- modified by sonia abrol 02/10/06, to add study admin cal changes, other enhancements

		v_sql :=  'Select EVENT_ID,CHAIN_ID , EVENT_TYPE ,NAME ,NOTES ,COST ,DURATION  ,FUZZY_PERIOD,STATUS ,
		DESCRIPTION,DISPLACEMENT,EVENT_FLAG  ,  PAT_DAYSBEFORE ,PAT_DAYSAFTER ,USR_DAYSBEFORE ,USR_DAYSAFTER ,
		PAT_MSGBEFORE ,PAT_MSGAFTER ,USR_MSGBEFORE ,USR_MSGAFTER , STATUS_DT , ORG_ID , duration_unit,fk_visit,
		event_cptcode, event_fuzzyafter, event_durationafter,event_durationbefore,	event_calassocto
  FROM IMPSTUDY_CAL0 ORDER BY EVENT_TYPE DESC';



		OPEN v_cursor FOR v_sql;
	 	 LOOP

		 	 FETCH v_cursor INTO v_orig_event,v_orig_chain,v_event_type,v_event_name,v_event_notes,v_event_cost,
			 v_event_duration, v_event_fuzzy, v_event_status, v_event_desc,v_event_displacement,v_event_flag,
			 v_event_pat_daysbefore, v_event_pat_daysafter,v_event_usr_daysbefore,v_event_usr_daysafter,v_event_pat_msgbefore,
			 v_event_pat_msgafter, v_event_usr_msgbefore, v_event_usr_msgafter,v_event_statusdt, v_orgid,  v_duration_unit ,  v_orig_fk_visit,
			   v_event_cptcode ,	   v_event_fuzzyafter ,	    v_event_durationafter ,		v_event_durationbefore ,		v_event_calassocto  ;



			    EXIT WHEN v_cursor %NOTFOUND;

	 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_event, 'event_assoc',v_sitecode, v_new_event,v_err,v_originating_sitecode);

				IF v_event_type = 'P' THEN -- its a protocol
				   --check for its chain_id; The chain_id for a Protocol is Study_id.

				   	 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_chain, 'er_study',v_sitecode, v_new_chain,v_err,v_originating_sitecode);

				     IF v_new_chain <= 0 THEN
			  	  	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create protocol, parent Study not found. original PK_STUDY : ' || v_orig_chain, v_success );
		   			 END IF;
			    ELSIF  v_event_type = 'A' THEN -- its an event
				   --check for its chain_id; The chain_id for an event is Protocol.

				   	 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_chain, 'event_assoc',v_sitecode, v_new_chain,v_err,v_originating_sitecode);

				     IF v_new_chain <= 0 THEN
			  	  	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create event, parent Protocol not found. original Event_id : ' || v_orig_chain, v_success );
		   			 END IF;

				END IF;

				IF v_new_chain > 0 THEN -- Chain_id exists
				    IF v_new_event = 0 THEN -- new record

					   SELECT EVENT_DEFINITION_SEQ.NEXTVAL
					   INTO v_new_event
					   FROM DUAL;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - New EVENT_ASSOC record PK:' || v_orig_event, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_event, 'event_assoc',v_sitecode,  v_new_event,v_err,
						v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN
							
--JM: 10FEB2011: #D-FIN9							
 select PK_CODELST into v_codeLstId from sch_codelst where CODELST_TYPE='calStatStd' and CODELST_SUBTYP=v_event_status;	    
	    							

							INSERT INTO EVENT_ASSOC( EVENT_ID ,  CHAIN_ID , EVENT_TYPE , NAME , NOTES ,
								    COST ,DURATION,	FUZZY_PERIOD , FK_CODELST_CALSTAT,
								    DESCRIPTION ,
									DISPLACEMENT , EVENT_FLAG , PAT_DAYSBEFORE , PAT_DAYSAFTER , USR_DAYSBEFORE ,
									USR_DAYSAFTER  , PAT_MSGBEFORE , PAT_MSGAFTER , USR_MSGBEFORE ,
									USR_MSGAFTER ,  STATUS_DT  ,  CREATED_ON , CREATOR ,
									IP_ADD,ORG_ID ,USER_ID    ,DURATION_UNIT,FK_VISIT , event_cptcode, event_fuzzyafter, event_durationafter,event_durationbefore,	event_calassocto)
							VALUES ( v_new_event,v_new_chain,v_event_type,v_event_name,v_event_notes,
							v_event_cost, v_event_duration,v_event_fuzzy, v_codeLstId,v_event_desc,
							v_event_displacement,v_event_flag,v_event_pat_daysbefore, v_event_pat_daysafter,v_event_usr_daysbefore,
							v_event_usr_daysafter,v_event_pat_msgbefore, v_event_pat_msgafter, v_event_usr_msgbefore,
							v_event_usr_msgafter,v_event_statusdt,SYSDATE,v_creator,v_ipAdd,v_orgid,v_account,
							  v_duration_unit ,  v_orig_fk_visit,
				  			   v_event_cptcode ,	   v_event_fuzzyafter ,	    v_event_durationafter ,		v_event_durationbefore ,		v_event_calassocto
							)	;

							-- v_orig_fk_visit  is not processed for new IDS. It will be processed and updated after importing visit records

							-- insert protcol status

							IF UPPER(v_event_type) = 'P' THEN
							   INSERT INTO SCH_PROTSTAT (
							     PK_PROTSTAT    ,
							     FK_EVENT       ,
							     FK_CODELST_CALSTAT,
							     PROTSTAT_DT    ,
							     PROTSTAT_BY    ,
							     PROTSTAT_NOTE  )
							     VALUES
							    (sch_protstat_seq.NEXTVAL,
							     v_new_event ,
							     v_codeLstId,
							     v_event_statusdt,
								 v_creator ,
							     v_event_notes ) ;

								-- create default alerts and notificatons records
								IF v_event_status = 'A' THEN
							       pkg_alnot.set_alnot(NULL, v_new_chain, v_new_event, 'G' , v_creator, SYSDATE, v_ipadd);
							    END IF;
							END IF;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:Could not create Event_assoc record for Sponsor PK ' || v_orig_event || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_event > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL  - Old Event_assoc record with original PK:' || v_orig_event , v_success );

						UPDATE EVENT_ASSOC
						SET NAME = v_event_name , NOTES = v_event_notes ,
								    COST = v_event_cost ,DURATION = v_event_duration,	FUZZY_PERIOD = v_event_fuzzy ,
									fk_codelst_calstat = v_codeLstId, DESCRIPTION = v_event_desc,
									DISPLACEMENT = v_event_displacement, EVENT_FLAG = v_event_flag,
									PAT_DAYSBEFORE = v_event_pat_daysbefore, PAT_DAYSAFTER = v_event_pat_daysafter,
									USR_DAYSBEFORE = v_event_usr_daysbefore ,
									USR_DAYSAFTER = v_event_usr_daysafter, PAT_MSGBEFORE = v_event_pat_msgbefore,
									PAT_MSGAFTER = v_event_pat_msgafter, USR_MSGBEFORE =  v_event_usr_msgbefore ,
									USR_MSGAFTER = v_event_usr_msgafter,  STATUS_DT = v_event_statusdt,
									LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd,
									ORG_ID = v_orgid, duration_unit = v_duration_unit, fk_visit = v_orig_fk_visit,
									event_cptcode = v_event_cptcode, event_fuzzyafter = v_event_fuzzyafter , event_durationafter = v_event_durationafter,event_durationbefore = v_event_durationbefore,
									event_calassocto = v_event_calassocto
					    WHERE event_id  = v_new_event;

						-- v_fk_visit is not process for new IDS. It will be processed and updated after importing visit records

						INSERT INTO SCH_PROTSTAT (
						    PK_PROTSTAT    , FK_EVENT   , fk_codelst_calstat  ,PROTSTAT_DT  , PROTSTAT_NOTE  )
						 VALUES (sch_protstat_seq.NEXTVAL, v_new_event , v_codeLstId ,   v_event_statusdt,
								   v_event_notes) ;

					 ELSIF v_new_event = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:' || v_err, v_success );

					 END IF;

				END IF; -- check for chain_id
		END LOOP;

	 CLOSE v_cursor;

	 -- for calendar visits

	 v_sql := ' Select  PK_PROTOCOL_VISIT   ,  FK_PROTOCOL  ,  VISIT_NO  ,  VISIT_NAME,  DESCRIPTION ,
					     DISPLACEMENT     ,  NUM_MONTHS     ,  NUM_WEEKS      ,  NUM_DAYS ,  INSERT_AFTER    ,
						     INSERT_AFTER_INTERVAL   ,  INSERT_AFTER_INTERVAL_UNIT  ,  PROTOCOL_TYPE   ,  VISIT_TYPE
	 	   FROM IMPSTUDY_CAL15' ;
	    OPEN v_cursor FOR v_sql;
	 	 LOOP

		 	 FETCH v_cursor INTO v_old_pk_visit ,v_orig_chain,v_visit_no, v_visit_name,v_description,
			 v_displacement,v_num_months,v_num_weeks,v_num_days,v_insert_after,
			v_insert_after_interval, v_insert_after_interval_unit,v_protocol_type,v_visit_type ;
		 EXIT WHEN v_cursor %NOTFOUND;

		 	  	   	Pkg_Impexcommon.SP_GET_SITEPK(v_orig_chain, 'event_assoc',v_sitecode, v_new_chain,v_err,v_originating_sitecode); -- get protocol

				     IF v_new_chain <= 0 THEN
			  	  	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create protocol visit, parent Protocol not found. original Protocol : ' || v_orig_chain, v_success );
		   			 END IF;

					IF v_new_chain > 0 THEN

	 		 	  	   	Pkg_Impexcommon.SP_GET_SITEPK(v_old_pk_visit , 'sch_protocol_visit',v_sitecode, v_new_pk_visit ,v_err,v_originating_sitecode); -- get protocol

					   IF v_new_pk_visit = 0 THEN --new visit
					   --------------------------
					   			 SELECT SCH_PROT_VISIT_SEQ.NEXTVAL
								  INTO v_new_pk_visit FROM DUAL;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - New SCH_PROTOCOL_VISIT  record PK:' || v_old_pk_visit , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_old_pk_visit , 'sch_protocol_visit',v_sitecode, v_new_pk_visit,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN
								   -- we do not process INSERT_AFTER right now, we process it later
							INSERT INTO sch_protocol_visit(PK_PROTOCOL_VISIT,  FK_PROTOCOL,  VISIT_NO, VISIT_NAME ,DESCRIPTION,
								   	  DISPLACEMENT,   NUM_MONTHS,  NUM_WEEKS,  NUM_DAYS, INSERT_AFTER  ,   	CREATOR,
									  CREATED_ON,  IP_ADD,  INSERT_AFTER_INTERVAL,  INSERT_AFTER_INTERVAL_UNIT,   PROTOCOL_TYPE ,VISIT_TYPE  )
							VALUES ( v_new_pk_visit ,v_new_chain,v_visit_no, v_visit_name,v_description,
			 				v_displacement,v_num_months,v_num_weeks,v_num_days,v_insert_after,v_creator,
							SYSDATE,v_ipAdd, v_insert_after_interval, v_insert_after_interval_unit,v_protocol_type,v_visit_type
							)	;
							  -- update event_assoc with the new visit pk

							 UPDATE event_assoc SET fk_visit = v_new_pk_visit WHERE chain_id = v_new_chain
							 AND fk_visit = v_old_pk_visit;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:Could not create Event_assoc record for Sponsor PK ' || v_orig_event || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					   -----------------------
					   ELSIF v_new_pk_visit > 0 THEN --visit exists
					   				UPDATE sch_protocol_visit
									SET  FK_PROTOCOL =  v_new_chain ,  VISIT_NO =  v_visit_no, VISIT_NAME =  v_visit_name, DESCRIPTION =  v_description,
								   	  DISPLACEMENT = v_displacement ,   NUM_MONTHS = v_num_months,  NUM_WEEKS = v_num_weeks,
									  NUM_DAYS = v_num_days, INSERT_AFTER  =  v_insert_after,
									    IP_ADD = v_ipAdd,  INSERT_AFTER_INTERVAL = v_insert_after_interval,
										  INSERT_AFTER_INTERVAL_UNIT = v_insert_after_interval_unit,   PROTOCOL_TYPE  = v_protocol_type,VISIT_TYPE  = v_visit_type,
										  LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE
									WHERE PK_PROTOCOL_VISIT = v_new_pk_visit ;


							 UPDATE event_assoc SET fk_visit = v_new_pk_visit WHERE chain_id = v_new_chain
							 AND fk_visit = v_old_pk_visit;

					 		   ELSIF v_new_pk_visit = -1 THEN
					   		  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:' || v_err, v_success );

					 END IF; --for v_new_pk_visit

							IF ( NVL(v_insert_after,0) > 0 ) THEN
							   	 -- mark this visit for updating its parent later
								  v_parent_visits.EXTEND;
								  v_parent_visits(v_parent_visits.LAST) := v_insert_after ; -- store the original insert after

								   v_child_visits.EXTEND;
   								   v_child_visits (v_child_visits .LAST) := v_new_pk_visit  ; -- store the new child visitvisit PK
							END IF;


				END IF; --for v_new_chain
		END LOOP;

	 CLOSE v_cursor;

	 -- after all visits are imported, update the parent visit ids
	    FOR i IN 1..v_child_visits.COUNT
	  LOOP

		  v_insert_after := v_parent_visits(i)     ;     -- stores the original insert after
		  v_new_pk_visit := v_child_visits(i)  ;-- STORES THE NEW CHILD visitvisit PK

		-- get new insert after
		Pkg_Impexcommon.SP_GET_SITEPK(v_insert_after , 'sch_protocol_visit',v_sitecode, v_insert_after_new ,v_err,v_originating_sitecode); -- get protocol

	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - :v_insert_after' || v_insert_after , v_success );
	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - : v_insert_after_new ' ||  v_insert_after_new , v_success );
	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - : v_new_pk_visit ' ||  v_new_pk_visit , v_success );

		IF (v_insert_after_new > 0) THEN
		      UPDATE sch_protocol_visit SET  INSERT_AFTER  = v_insert_after_new
			  WHERE PK_PROTOCOL_VISIT = v_new_pk_visit;

	  	 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - : update sql ' ||
					      ' UPDATE sch_protocol_visit SET  INSERT_AFTER  = ' || v_insert_after_new || '  WHERE PK_PROTOCOL_VISIT = ' || v_new_pk_visit
			, v_success );
		ELSE
			 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL: could not update the parent visit for visit' || v_new_pk_visit || ', the corresponding parent visit at client not found ', v_success );
 			 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'DEBUG','EXCEPTION','parent visit for visit' || v_new_pk_visit || ' old insert after visit: ' || v_insert_after || ', the corresponding parent visit at client not found ', v_success );
		END IF;


	  END LOOP;

	 -- end of calendar visits


 	 --for Event CRFS

	 v_sql := 'Select PK_CRFLIB  , FK_EVENTS, CRFLIB_NUMBER,   CRFLIB_NAME,  CRFLIB_FLAG, CRFLIB_FORMFLAG
	 	   FROM IMPSTUDY_CAL2 ';

		OPEN v_cursor FOR v_sql;
	 	 LOOP

		 	 FETCH v_cursor INTO v_orig_crf, v_orig_event,v_crfnumber,v_crfname,v_crfflag,v_crfformflag;
			    EXIT WHEN v_cursor %NOTFOUND;

				-- check for protocol

				 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_event, 'event_assoc',v_sitecode, v_new_event,v_err,v_originating_sitecode);
				 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_crf, 'sch_crflib',v_sitecode, v_new_crf,v_err,v_originating_sitecode);

				 IF v_new_event <= 0 THEN
			  	  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create protocol status, parent Protocol not found. original FK_EVENT : ' || v_orig_event, v_success );
				 ELSE
 			 	  IF v_new_crf = 0 THEN -- new record
		  		 	 SELECT seq_sch_crflib.NEXTVAL
						INTO v_new_crf	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL  - New CRFLIB record with original PK:' || v_orig_crf, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_crf, 'sch_crflib',v_sitecode,  v_new_crf,v_err,
						v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL  Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO SCH_CRFLIB (PK_CRFLIB, FK_EVENTS,CRFLIB_NUMBER,CRFLIB_NAME,
							 		CRFLIB_FLAG, CREATOR, CREATED_ON,  IP_ADD, CRFLIB_FORMFLAG )
							 VALUES (v_new_crf, v_new_event, v_crfnumber, v_crfname, v_crfflag, v_creator, SYSDATE,
							 v_ipAdd,v_crfformflag ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:Could not create Sch_CrfLib record for Sponsor PK ' || v_orig_crf || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_crf > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - Old SCH_CRFLIB record with original PK:' || v_orig_crf , v_success );

						UPDATE SCH_CRFLIB
						SET CRFLIB_NUMBER = v_crfnumber,CRFLIB_NAME = v_crfname,
							 		CRFLIB_FLAG = v_crfflag,  CRFLIB_FORMFLAG = v_crfformflag,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd
					    WHERE pk_crflib  = v_new_crf;

					 ELSIF v_new_crf = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:' || v_err, v_success );

					 END IF;



	   			 END IF;



		 END LOOP;

		 CLOSE v_cursor;

		 -- import forms

 		-- get data from the form temp table


		v_sql := 'Select   PK_FORMLIB, FORM_NAME , FORM_DESC , FORM_LINKTO,
				    FORM_XSL, FORM_XSLREFRESH, FORM_VIEWXSL , FORM_XML, form_status_subtype, form_sharedwith,
					FORM_CUSTOM_JS, FORM_KEYWORD ,FORM_ACTIVATION_JS
		 	      FROM IMPSTUDY_CAL3';



		-- import form

	 OPEN v_cursor FOR v_sql;
	 	 LOOP
		 	 FETCH v_cursor INTO  v_orig_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
				  	v_form_xslrefresh, v_form_viewxsl, v_formxml ,v_codelst_formstat, v_sharedwith,
					v_form_custom_js,v_form_keyword,v_form_activation_js ;

			 EXIT WHEN v_cursor %NOTFOUND;

 			 SELECT	getCodePk (v_codelst_formstat, 'frmstat')
			 INTO v_formstatus
			 FROM dual;

			 IF v_formstatus = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - Code not found for codelst_type : frmstat, codelst_subtyp : ' || v_codelst_formstat, v_success );
				v_formstatus  := NULL;
			 END IF;



			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

			  IF v_new_form = 0 THEN -- new record

					 SELECT seq_er_formlib.NEXTVAL
						INTO v_new_form	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - New Form record with original PK:' || v_orig_form , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_form, 'er_formlib',v_sitecode,  v_new_form,v_err,
						v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							v_form_xmltype := NULL;

							BEGIN
								IF v_formxml IS NOT NULL THEN
							   	   SELECT XMLTYPE(v_formxml)
								   INTO v_form_xmltype FROM dual;
							    END IF;
							EXCEPTION WHEN OTHERS THEN
									 v_form_xmltype := NULL;
							END;

							 INSERT INTO ER_FORMLIB ( PK_FORMLIB , FORM_NAME ,  FORM_DESC,
            				 			 			 FORM_LINKTO,  FORM_XSL ,
													 FORM_XSLREFRESH , FORM_XML,
													 CREATOR, CREATED_ON,IP_ADD, FORM_STATUS, FK_ACCOUNT, RECORD_TYPE,
													 FORM_SHAREDWITH,FORM_CUSTOM_JS,
													 FORM_KEYWORD, FORM_ACTIVATION_JS  )
							 VALUES (v_new_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
							 	  	1, v_form_xmltype,
								    v_creator, SYSDATE,v_ipAdd , v_formstatus, v_account,'N',v_sharedwith,
									v_form_custom_js,v_form_keyword,v_form_activation_js) ;

							 INSERT INTO ER_FORMSTAT(pk_formstat,fk_formlib,fk_codelst_stat,formstat_stdate,
							 formstat_changedby,record_type,creator,created_on,ip_add)
							 VALUES(seq_er_formstat.NEXTVAL,v_new_form, v_formstatus,SYSDATE,v_creator,'N',v_creator,
							 SYSDATE,v_ipAdd );

							  --add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
							 v_importedforms.EXTEND;
							 v_importedforms(v_importedforms.LAST) := v_new_form;


							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create Form record for Sponsor PK ' || v_orig_form || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_form > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - Old Form record with original PK:' || v_orig_form, v_success );

						v_form_xmltype := NULL;

						BEGIN
						  IF v_formxml IS NOT NULL THEN
						   	   SELECT XMLTYPE(v_formxml)
							   INTO v_form_xmltype FROM dual;
							    END IF;
								EXCEPTION WHEN OTHERS THEN
									 v_form_xmltype := NULL;
						  END;


						UPDATE ER_FORMLIB
						SET FORM_NAME = v_formname,  FORM_DESC = v_formdesc, FORM_LINKTO = v_formlinkto,
						  FORM_XSL = v_formxsl , FORM_XSLREFRESH = 1,
						   FORM_VIEWXSL = NULL , FORM_XML =  v_form_xmltype ,
							LAST_MODIFIED_BY = v_creator ,
							LAST_MODIFIED_DATE = SYSDATE ,IP_ADD = v_ipAdd,
							FORM_STATUS = v_formstatus ,FK_ACCOUNT = v_account, RECORD_TYPE = 'M',
							FORM_SHAREDWITH = v_sharedwith ,FORM_CUSTOM_JS = v_form_custom_js,
							 FORM_KEYWORD = v_form_keyword, FORM_ACTIVATION_JS = v_form_activation_js
					    WHERE pk_formlib = v_new_form;

						--add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
						 v_importedforms.EXTEND;
						 v_importedforms(v_importedforms.LAST) := v_new_form;


					 ELSIF v_new_form = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:' || v_err, v_success );

					END IF;

		 END LOOP;

	 CLOSE v_cursor ;

	 	 -- import Linked forms records

 		v_sql := ' Select PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT, FK_STUDY,
           	  	   	  LF_LNKFROM, FK_CALENDAR, FK_EVENT,FK_CRF,
					  LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT
					 FROM IMPSTUDY_CAL5';


	  	 OPEN v_cursor FOR v_sql;

		 LOOP
		 	 FETCH v_cursor INTO v_orig_lf,v_orig_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount, v_lffkstudy,
			  v_lflnkfrom, v_lfcal, v_lfevent,v_lfcrf ,v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat;

			 EXIT WHEN v_cursor  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode, v_new_lf,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

			 Pkg_Impexcommon.SP_GET_SITEPK(v_lfevent, 'event_assoc',v_sitecode, v_new_event,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_lfcrf, 'sch_crflib',v_sitecode, v_new_crf,v_err,v_originating_sitecode);


			 IF LENGTH(trim(NVL(v_lfaccount,''))) > 0 THEN
			 	v_lfaccount := v_account;
			 ELSE
			 	 v_lfaccount := NULL;
			 END IF;



			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create linked form record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSIF v_new_crf = 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create linked form record, parent CRF not found. original PK_CRF : ' || v_lfcrf, v_success );
  			 ELSIF v_new_event = 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create linked form record, parent Event not found. original EVENT_ID : ' || v_lfevent, v_success );

			 ELSE

			 	 IF v_new_lf = 0 THEN -- new record
		  		 	 SELECT seq_er_linkedforms.NEXTVAL
						INTO v_new_lf	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - New Linked Forms record with original PK:' || v_orig_lf, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode,  v_new_lf,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_LINKEDFORMS (PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT,
							  LF_LNKFROM ,  FK_EVENT  , FK_CRF, CREATOR , CREATED_ON , IP_ADD , RECORD_TYPE ,
							   LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT  )
							 VALUES (v_new_lf, v_new_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount,
					 			  v_lflnkfrom,  v_new_event, v_new_crf, v_creator, SYSDATE,v_ipAdd,'N' ,
								  v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:Could not create Linked Forms record for Sponsor PK ' || v_orig_lf || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_lf > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL - Old Linked Forms record with original PK:' || v_orig_lf , v_success );

						UPDATE ER_LINKEDFORMS
						SET LF_DISPLAYTYPE = v_lfdisplaytype, LF_ENTRYCHAR =  v_lfentrychar, FK_ACCOUNT =  v_lfaccount,
							 LF_LNKFROM = v_lflnkfrom,FK_EVENT = v_new_event, FK_CRF = v_new_crf,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd,
						 RECORD_TYPE = 'M',  LF_DATACNT = v_lf_datacnt, LF_HIDE = v_lf_hide , LF_SEQ = v_lf_seq,
						  LF_DISPLAY_INPAT = v_lf_display_inpat
					    WHERE pk_lf  = v_new_lf;

					 ELSIF v_new_lf = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_cursor;

	 	   --------------------------
		   ---------------- import forlib version data

		v_sql := 'Select PK_FORMLIBVER,  FK_FORMLIB, FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
			 FORMLIBVER_DATE, FORMLIBVER_XML , FORMLIBVER_XSL,  FORMLIBVER_VIEWXSL 	FROM IMPSTUDY_CAL12';


	  	 OPEN v_cursor FOR v_sql;

		 LOOP

		 	 FETCH v_cursor INTO v_orig_formlibver,v_orig_form,v_formlibver_number, v_formlibver_notes, v_formlibver_date,
			 v_formlibver_xmlclob, v_formlibver_xsl,v_formlibver_viewxsl	;

			 EXIT WHEN v_cursor  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode, v_new_formlibver,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

		--	 PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS v_lffkstudy' || v_lffkstudy, v_success );

			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create formlib version record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSE

				--prepare xmltype

				v_formlibver_xml := NULL;

					BEGIN
						IF v_formlibver_xmlclob IS NOT NULL THEN
				   	       SELECT XMLTYPE(v_formlibver_xmlclob)
						    INTO v_formlibver_xml FROM dual;
					    END IF;
					EXCEPTION WHEN OTHERS THEN
						  v_formlibver_xml := NULL;
					END;


			 	 IF v_new_formlibver = 0 THEN -- new record

		  		 	 SELECT seq_er_formlibver.NEXTVAL
						INTO v_new_formlibver FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - New Formlib version record with original PK:' || v_orig_formlibver, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode,  v_new_formlibver,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN


							  INSERT INTO ER_FORMLIBVER (PK_FORMLIBVER,  FK_FORMLIB,  FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
							  FORMLIBVER_DATE,  FORMLIBVER_XML,   FORMLIBVER_XSL,   FORMLIBVER_VIEWXSL,
							  CREATOR, RECORD_TYPE, CREATED_ON ,IP_ADD )
							 VALUES (v_new_formlibver,v_new_form, v_formlibver_number, v_formlibver_notes, v_formlibver_date,
							 v_formlibver_xml,v_formlibver_xsl,v_formlibver_viewxsl,
					 			 v_creator, 'N' ,SYSDATE,v_ipAdd) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:Could not create FormLib ver record for Sponsor PK ' || v_orig_formlibver || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_formlibver > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX.SP_IMP_FORMS - Old Formlib ver record with original PK:' || v_orig_formlibver , v_success );

						UPDATE ER_FORMLIBVER
						SET  FK_FORMLIB = v_new_form,  FORMLIBVER_NUMBER = v_formlibver_number ,  FORMLIBVER_NOTES =  v_formlibver_notes,
							  FORMLIBVER_DATE = v_formlibver_date,  FORMLIBVER_XML = v_formlibver_xml,
							  FORMLIBVER_XSL = v_formlibver_xsl,   FORMLIBVER_VIEWXSL = v_formlibver_viewxsl,
							  RECORD_TYPE = 'M', LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd
						WHERE PK_FORMLIBVER = v_new_formlibver;

					 ELSIF v_new_formlibver = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_FORMS:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_cursor;

	 ----------------

	 	   --------------------------
		   -- import form fields

		   V_ERR := '';

		   Pkg_Impexforms.SP_IMP_FORMFLDS('study_cal',V_EXPID,V_expdatils, V_ERR , O_SUCCESS);

		   IF LENGTH(trim(v_err)) > 0 THEN
			  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_IMP_STUDYCAL Could not import form fields data :' || v_err, v_success );
			END IF;

		   -- after complete forms import, regenerate mpform,xsls, xmls for each form
		   -- sonia, 12/22/04
		    Pkg_Impexforms.sp_regen_formstruct(V_EXPID, v_importedforms,'PKG_IMPEX.SP_IMP_STUDYCAL');

	 COMMIT;
 END;

 --------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_ALTER_TRIGGER(P_IMPID NUMBER, P_ACTION VARCHAR2, O_SUCCESS OUT NUMBER)
  AS
  /**
  	 Author: Sonia Sahni
	 Date: 03/05/2004
	 Purpuse : Enables/Disables triggers depending on trigger category and action

	 P_EXPDETAILS : The Export Clob for export/import details
	 P_ACTION: D- Disable, E- Enable

  */
  v_sql VARCHAR2(500);
  v_action VARCHAR2(15);
  v_name VARCHAR2(500);
  v_success NUMBER;
  v_category VARCHAR2(20);
  v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
  v_count NUMBER;
  v_expid NUMBER;
  V_EXPDETAILS CLOB;

  BEGIN
  	   IF p_Action = 'E' THEN
	   	  v_action := 'ENABLE';
		ELSE
		  v_action := 'DISABLE';
		END IF;

	   SP_GET_IMPREQDETAILS(P_IMPID, V_expdetails,v_expid);

	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_EXPDETAILS,'importCategory',v_modkeys);
   	   v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_category);
		END IF;


	   BEGIN
	  	   FOR i IN (SELECT ti_name
		   	   	 	FROM ER_TRIGGERINFO
					WHERE v_category =  v_category AND ti_user = 'ER')
		   LOOP
			   		v_name := i.ti_name;

					EXECUTE IMMEDIATE 'ALTER TRIGGER ' || v_name || ' ' || v_action  ;


		   END LOOP;

		   --PKG_SCHIMPEX.SP_ALTER_TRIGGER(P_EXPID , V_CATEGORY , P_ACTION , O_SUCCESS );

	   EXCEPTION WHEN OTHERS THEN
		  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX.SP_ALTER_TRIGGER:Could not ' || v_action || ' Triggers Error: ' || SQLERRM, v_success );
		  o_success := -1;
		END;


   END;


   PROCEDURE SP_GETELEMENT_VALUES_REMOTE(P_IMPID NUMBER,P_PARAM VARCHAR2,O_EXPID OUT NUMBER, O_VALUES OUT VARCHAR2 )
	AS

	V_expdetails CLOB;

	BEGIN

			SP_GET_IMPREQDETAILS(P_IMPID, V_expdetails,o_expid);
			Pkg_Impexcommon.SP_GETELEMENT_VALUES_STRING(V_EXPDETAILS,p_param,o_values);
	END;


	FUNCTION getSchCodePk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2) RETURN NUMBER
	IS
	v_pk_code NUMBER;
	BEGIN

		BEGIN
			SELECT pk_codelst
			INTO v_pk_code
			FROM sch_codelst
			WHERE trim(codelst_type) = trim(p_codetype) AND trim(codelst_subtyp) = trim(p_codelst_subtype);

			RETURN v_pk_code;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
			RETURN -1;
		END;

	END;



  END Pkg_Impex;
/