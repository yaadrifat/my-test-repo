create or replace
TRIGGER "ESCH"."EVENT_ASSOC_AU0" AFTER UPDATE ON ESCH.EVENT_ASSOC FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
		(raid, 'EVENT_ASSOC', :OLD.rid, 'U', usr);

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID', :OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID', :OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE', :OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME', :OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES', :OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST', :OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION', :OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION', :OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID', :OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI', :OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD', :OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO', :OLD.msg_to, :NEW.msg_to);
	END IF;
	IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS', :OLD.status, :NEW.status);
	END IF;
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION', :OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT', :OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID', :OLD.org_id, :NEW.org_id);
	END IF;

	--JM: added

	IF NVL(:OLD.orig_cal,0) !=
		NVL(:NEW.orig_cal,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_CAL', :OLD.orig_cal, :NEW.orig_cal);
	END IF;

	IF NVL(:OLD.orig_event,0) !=
		NVL(:NEW.orig_event,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_EVENT', :OLD.orig_event, :NEW.orig_event);
	END IF;

	IF NVL(:OLD.orig_study,0) !=
		NVL(:NEW.orig_study,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_STUDY', :OLD.orig_study, :NEW.orig_study);
	END IF;
	--

	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG', :OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES', :OLD.event_res, :NEW.event_res);
	END IF;

	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG', :OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE', :OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER', :OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE', :OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER', :OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE', :OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER', :OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE', :OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER', :OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.status_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.status_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'STATUS_DT', to_char(:old.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.status_changeby,0) !=
		NVL(:NEW.status_changeby,0) THEN
		audit_trail.column_update
		(raid, 'STATUS_CHANGEBY', :OLD.status_changeby, :NEW.status_changeby);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY', :OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE', to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD', :OLD.ip_add, :NEW.ip_add);
	END IF;
	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT', :OLD.duration_unit, :NEW.duration_unit);
	END IF;
	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT', :OLD.fk_visit, :NEW.fk_visit);
	END IF;
	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE', :OLD.event_cptcode, :NEW.event_cptcode);
	END IF;
	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER', :OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;
	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER', :OLD.event_durationafter, :NEW.event_durationafter);
	END IF;
	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE', :OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;
	--JM : changed    IF NVL(:OLD.fk_catlib,' ') !=
	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB', :OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
	IF NVL(:OLD.event_calassocto,' ') !=
		NVL(:NEW.event_calassocto,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CALASSOCTO', :OLD.event_calassocto, :NEW.event_calassocto);
	END IF;
	--JM: IF NVL(:OLD.event_calschdate,' ') !=
	IF NVL(:OLD.event_calschdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.event_calschdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'EVENT_CALSCHDATE', to_char(:old.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT) );
	END IF;

	--KM-28Mar08
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY', :OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;

	-- KM - 21Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;  
  IF NVL(:OLD.COVERAGE_NOTES,0) !=                      --BK 23rd july 2010
		NVL(:NEW.COVERAGE_NOTES,0) THEN
		audit_trail.column_update
		(raid, 'COVERAGE_NOTES',:OLD.COVERAGE_NOTES, :NEW.COVERAGE_NOTES);
	END IF;
END;