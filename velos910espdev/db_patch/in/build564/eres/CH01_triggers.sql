create or replace TRIGGER "ERES"."ER_DYNREP_BI0" BEFORE INSERT ON ER_DYNREP
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN
   BEGIN
	  --KM -#3635	
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_DYNREP',erid, 'I',usr);
   insert_data:= :NEW.PK_DYNREP||'|'|| :NEW.FK_FORM||'|'||:NEW.DYNREP_NAME||'|'||:NEW.DYNREP_DESC||'|'||:NEW.DYNREP_FILTER||'|'||:NEW.DYNREP_ORDERBY||'|'||
   :NEW.FK_USER||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.DYNREP_HDRNAME||'|'||:NEW.DYNREP_FTRNAME||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||:NEW.REPORT_TYPE||'|'||:NEW.FK_STUDY||'|'||
   :NEW.FK_PERSON||'|'||:NEW.DYNREP_SHAREWITH||'|'||:NEW.DYNREP_TYPE||'|'||:NEW.DYNREP_FORMLIST||'|'||:NEW.DYNREP_USEUNIQUEID||'|'||:NEW.DYNREP_USEDATAVAL;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ERES"."ER_FORMSEC_BI0" BEFORE INSERT ON ER_FORMSEC
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


             SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
      :NEW.rid := erid ;
    SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_FORMSEC',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMSEC||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FORMSEC_NAME||'|'|| :NEW.FORMSEC_SEQ||'|'||:NEW.FORMSEC_FMT||'|'||
    :NEW.FORMSEC_REPNO||'|'|| :NEW.RID||'|'||:NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'|| :NEW.FORMSEC_OFFLINE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
 /
 
 create or replace TRIGGER "ERES"."ER_STUDYFORMS_BI0" BEFORE INSERT ON ER_STUDYFORMS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
 WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      ) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   BEGIN
 --KM-#3635 
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM ER_USER
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_STUDYFORMS',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
      insert_data:= :NEW.PK_STUDYFORMS||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FK_STUDY||'|'||
    TO_CHAR(:NEW.STUDYFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.RID||'|'||
     :NEW.FORM_COMPLETED||'|'|| :NEW.ISVALID||'|'|| :NEW.CREATOR||'|'||
   :NEW.LAST_MODIFIED_BY||'|'||:NEW.RECORD_TYPE||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
      :NEW.NOTIFICATION_SENT||'|'|| :NEW.PROCESS_DATA||'|'|| :NEW.FK_FORMLIBVER || '|' || :NEW.FK_SPECIMEN;
 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ERES".ER_FORMLIB_BI0 BEFORE INSERT ON ER_FORMLIB
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM ER_USER
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

        SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_FORMLIB',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMLIB||'|'|| :NEW.FK_CATLIB||'|'||
    :NEW.FK_ACCOUNT||'|'||:NEW.FORM_NAME||'|'||:NEW.FORM_DESC||'|'||:NEW.FORM_SHAREDWITH||'|'||
    :NEW.FORM_STATUS||'|'||:NEW.FORM_LINKTO||'|'|| :NEW.FORM_NEXT||'|'||
    :NEW.FORM_NEXTMODE||'|'|| :NEW.FORM_BOLD||'|'|| :NEW.FORM_ITALICS||'|'||
    :NEW.FORM_ALIGN||'|'|| :NEW.FORM_UNDERLINE||'|'||:NEW.FORM_COLOR||'|'||:NEW.FORM_BGCOLOR||'|'||
    :NEW.FORM_FONT||'|'|| :NEW.FLD_FONTSIZE||'|'|| :NEW.RID||'|'||
    :NEW.FORM_FILLFLAG||'|'|| :NEW.CREATOR||'|'|| :NEW.FORM_XSLREFRESH||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.RECORD_TYPE||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||:NEW.FORM_KEYWORD || '|' || :new.FORM_ESIGNREQ;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/
create or replace TRIGGER ERES."ER_LINKEDFORMS_BI0" BEFORE INSERT ON ERES.ER_LINKEDFORMS
  REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_LINKEDFORMS',erid, 'I',usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_LF||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.LF_DISPLAYTYPE||'|'||:NEW.LF_ENTRYCHAR||'|'|| :NEW.FK_ACCOUNT||'|'|| :NEW.FK_STUDY||'|'||
    :NEW.LF_LNKFROM||'|'|| :NEW.FK_CALENDAR||'|'|| :NEW.FK_EVENT||'|'||
    :NEW.FK_CRF||'|'|| :NEW.RID||'|'||:NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'|| :NEW.LF_DATACNT||'|'||
    :NEW.LF_HIDE||'|'|| :NEW.LF_SEQ||'|'|| :NEW.LF_DISPLAY_INPAT||'|'||:NEW.LF_ISIRB||'|'||
    :NEW.LF_SUBMISSION_TYPE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/