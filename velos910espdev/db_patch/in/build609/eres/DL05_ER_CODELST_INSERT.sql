set define off;


insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'note_reason','deffer','Deffered','N',1,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'num_of_bags'
    AND codelst_subtyp = '1_bag';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'num_of_bags','1_bag','1 Bag','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'num_of_bags'
    AND codelst_subtyp = '2_bags';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'num_of_bags','2_bags','2 Bags','N',2,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'num_of_bags'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'num_of_bags','other','Other','N',3,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'bag_type'
    AND codelst_subtyp = '100';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'bag_type','100','100','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--
      

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hemo_trait';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','hemo_trait','Hemoglobin Trait','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
 
   DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'normal';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','normal','Normal','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END-- 
  
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'beta_thalmia';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','beta_thalmia','Beta Thalassemia plus structural variants','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
 
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'sickle_beta';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','sickle_beta','Sickle Beta Thalassemia','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END-- 
 
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'homo_no_dis';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','homo_no_dis','Homozygous C or E, no-disease','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
 

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hemo_sick_dis';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','hemo_sick_dis','Hemoglobin/Sickle Cell Disease','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END-- 


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_sil';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','alp_thal_sil','Alpha Thalassemia, Silent Carrier','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_trait';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','alp_thal_trait','Alpha Thalassemia Trait','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_sev';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','alp_thal_sev','Alpha Thalassemia, severe disease','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

 DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'mult_trait';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','mult_trait','Multiple Trait','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

  DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'unknown';
  if (v_record_exists = 0) then
     INSERT INTO 
     ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hem_path_scrn','unknown','Unknown','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END-- 




DECLARE
  record_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO record_count from er_codelst
    where codelst_type = 'alert'
    AND (codelst_subtyp = 'new_req' or codelst_subtyp = 'ass_to_chg' or codelst_subtyp = 'HLA_uploads');

  IF (record_count != 0) THEN
    delete from er_codelst where codelst_type = 'alert' AND (codelst_subtyp = 'new_req' or codelst_subtyp = 'ass_to_chg' or codelst_subtyp = 'HLA_uploads');
    dbms_output.put_line('record found');
  ELSE
    dbms_output.put_line('No record found');
  END IF;
END;
/
commit;

DECLARE
 index_count number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'resol_rec_all';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','resol_rec_all','Resolution Received (All)','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/


DECLARE
 index_count number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'spe_resol_cstm';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','spe_resol_cstm','Specific Resolution Received (Custom)','N',2,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/



DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'ctshp_newreqrec';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','ctshp_newreqrec','New Request Received (CT_Ship Sample)','N',3,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'ctsam_newreqrec';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','ctsam_newreqrec','New Request Received (CT_Sample at Lab)','N',4,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'he_newreqrec';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','he_newreqrec','New Request Received (HE)','N',5,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/



DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'or_newreqrec';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','or_newreqrec','New Request Received (OR)','N',6,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/





DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'finl_revnotcomp';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','finl_revnotcomp','Final CBU Review not complete','N',7,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/





DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'conf_shpnotcomp';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','conf_shpnotcomp','Confirm Shipment not complete','N',8,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/





DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'more_hlanotentr';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','more_hlanotentr','More Recent HLA Typing not entered','N',9,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/



DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'so_sctoddue';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','so_sctoddue','SCTOD Due (SO)','N',10,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/




DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'sh_sctoddue';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','sh_sctoddue','SCTOD Due (SH)','N',11,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END; 
/



DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'sh_sctodovrdue';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','sh_sctodovrdue','SCTOD Overdue (SH)','N',12,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/




DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'so_sctodovrdue';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','so_sctodovrdue','SCTOD Overdue (SO)','N',13,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/




DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'ct_shpovrdue';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','ct_shpovrdue','CT Shipment Overdue','N',14,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/





DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'mincr_notcompcm';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','mincr_notcompcm','Minimum Criteria Not Complete by CM','N',15,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/






DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'samp_not_avail';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','samp_not_avail','Sample Not Available','N',16,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/






DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'genrt_packslip';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','genrt_packslip','Generating Packing Slip','N',17,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/




DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'chg_userassign';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','chg_userassign','Change in User Assignment','N',18,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/




DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'alert'
   AND codelst_subtyp = 'result_recv';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'alert','result_recv','Results Received','N',null,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/
commit;




commit;