create or replace
TRIGGER "ERES".ER_CB_CORD_AU0  AFTER UPDATE OF 
PK_CORD,
CORD_CDR_CBU_ID,
CORD_EXTERNAL_CBU_ID,
CORD_TNC_FROZEN,
CORD_CD34_CELL_COUNT,
CORD_TNC_FROZEN_PAT_WT,	
CORD_BABY_BIRTH_DATE,
CORD_CBU_COLLECTION_DATE,
CORD_REGISTRY_ID,
CORD_LOCAL_CBU_ID,	
CORD_ID_NUMBER_ON_CBU_BAG,
FK_CORD_BACT_CUL_RESULT,
FK_CORD_FUNGAL_CUL_RESULT,
FK_CORD_ABO_BLOOD_TYPE,
FK_CORD_RH_TYPE,
FK_CORD_BABY_GENDER_ID,
FK_CORD_CLINICAL_STATUS,	
FK_CORD_CBU_LIC_STATUS,
CORD_AVAIL_CONFIRM_DATE,	
CREATOR,
IP_ADD,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
DELETEDFLAG,
RID,
ADDITIONAL_INFO_RECQUIRED,
CORD_ISBI_DIN_CODE,
CORD_ISIT_PRODUCT_CODE,
CORD_ELIGIBLE_ADDITIONAL_INFO,
FK_CBB_ID,
FK_CORD_CBU_ELIGIBLE_STATUS,
FK_CORD_CBU_STATUS,
FK_CORD_CBU_INELIGIBLE_REASON,
FK_CORD_CBU_UNLICENSED_REASON,	
FK_CODELST_EXTERNAL_NAME,
FK_CODELST_EXTERNAL_NUM,
FK_CODE_OTHER,
IND_OTHER_NAME,		
IND_OTHER_NUM,	
CORD_CLINICAL_STATUS_DATE,
CORD_CLINICAL_STATUS_ADDI_INFO,
FK_SPECIMEN_ID,
--FK_CODELST_EXTERNAL_NAME,
REGISTRY_MATERNAL_ID,	
MATERNAL_LOCAL_ID,	
FK_CODELST_ETHNICITY,
FK_CODELST_RACE,
FUNDING_REQUESTED,
FUNDED_CBU,
CORD_CDR_CBU_CREATED_BY,
CORD_CDR_CBU_LAST_MOD_BY,		
CORD_CDR_CBU_LAST_MOD_DT,		
CORD_CREATION_DATE,
CORD_ENTRY_PROGRESS,		
CORD_SEARCHABLE,
CORD_DONOR_IDENTI_NUM,
MULTIPLE_BIRTH,
FRZ_DATE,
HEMOGLOBIN_SCRN,
IS_CORD_AVAIL_FOR_NMDP,
CORD_NMDP_CBU_ID,
CORD_NMDP_MATERNAL_ID,
CBU_AVAIL_CONFIRM_FLAG,		
FK_UNAVAIL_RSN,
GUID,
FK_CBB_PROCEDURE,
CBB_PROCEDURE_START_DATE,
CBB_PROCEDURE_END_DATE,	
CBB_PROCEDURE_VERSION,	
ADDITI_TYPING_FLAG,
HRSA_RACE_ROLLUP,	
NMDP_RACE_ID,	
PRE_NUCL_CBU_CNT_CNCTRN,
PRE_PRCSNG_START_DATE,	
FK_CBU_STOR_LOC,
FK_CBU_COLL_SITE,		
FK_CBU_COLL_TYPE,		
FK_CBU_DEL_TYPE,	
BACT_COMMENT,	
FUNG_COMMENT  ON CB_CORD REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW
DECLARE
   raid NUMBER(10);
   usr VARCHAR(200); 
   old_modified_by VARCHAR2(100);
   new_modified_by VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
   usr := getuser(:NEW.LAST_MODIFIED_BY);
   
  audit_trail.record_transaction
    (raid, 'CB_CORD', :OLD.RID, 'U', usr);
    
  IF NVL(:OLD.PK_CORD,0) !=
     NVL(:NEW.PK_CORD,0) THEN
     audit_trail.column_update
       (raid, 'PK_CORD',
       :OLD.PK_CORD, :NEW.PK_CORD);
  END IF;
  
  IF NVL(:OLD.CORD_CDR_CBU_ID,' ') !=
     NVL(:NEW.CORD_CDR_CBU_ID,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_CDR_CBU_ID',
       :OLD.CORD_CDR_CBU_ID, :NEW.CORD_CDR_CBU_ID);
  END IF;
  
  IF NVL(:OLD.CORD_EXTERNAL_CBU_ID,' ') !=
     NVL(:NEW.CORD_EXTERNAL_CBU_ID,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_EXTERNAL_CBU_ID',
       :OLD.CORD_EXTERNAL_CBU_ID, :NEW.CORD_EXTERNAL_CBU_ID);
  END IF;
  
  
  IF NVL(:OLD.CORD_TNC_FROZEN,0) !=
     NVL(:NEW.CORD_TNC_FROZEN,0) THEN
     audit_trail.column_update
       (raid, 'CORD_TNC_FROZEN',
       :OLD.CORD_TNC_FROZEN, :NEW.CORD_TNC_FROZEN);
  END IF;
  
  IF NVL(:OLD.CORD_CD34_CELL_COUNT,0) !=
     NVL(:NEW.CORD_CD34_CELL_COUNT,0) THEN
     audit_trail.column_update
       (raid, 'CORD_CD34_CELL_COUNT',
       :OLD.CORD_CD34_CELL_COUNT, :NEW.CORD_CD34_CELL_COUNT);
  END IF;
  
   IF NVL(:OLD.CORD_TNC_FROZEN_PAT_WT,0) !=
     NVL(:NEW.CORD_TNC_FROZEN_PAT_WT,0) THEN
     audit_trail.column_update
       (raid, 'CORD_TNC_FROZEN_PAT_WT',
       :OLD.CORD_TNC_FROZEN_PAT_WT, :NEW.CORD_TNC_FROZEN_PAT_WT);
  END IF;
  
  
  
  IF NVL(:OLD.CORD_BABY_BIRTH_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CORD_BABY_BIRTH_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CORD_BABY_BIRTH_DATE',
       to_char(:OLD.CORD_BABY_BIRTH_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CORD_BABY_BIRTH_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  
  IF NVL(:OLD.CORD_CBU_COLLECTION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CORD_CBU_COLLECTION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CORD_CBU_COLLECTION_DATE',
       to_char(:OLD.CORD_CBU_COLLECTION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CORD_CBU_COLLECTION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  
  END IF;
  
  IF NVL(:OLD.CORD_REGISTRY_ID,0) !=
     NVL(:NEW.CORD_REGISTRY_ID,0) THEN
     audit_trail.column_update
       (raid, 'CORD_REGISTRY_ID',
       :OLD.CORD_REGISTRY_ID, :NEW.CORD_REGISTRY_ID);
  END IF;
  
  IF NVL(:OLD.CORD_LOCAL_CBU_ID,' ') !=
     NVL(:NEW.CORD_LOCAL_CBU_ID,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_LOCAL_CBU_ID',
       :OLD.CORD_LOCAL_CBU_ID, :NEW.CORD_LOCAL_CBU_ID);
  END IF;
  
   IF NVL(:OLD.CORD_ID_NUMBER_ON_CBU_BAG,' ') !=
     NVL(:NEW.CORD_ID_NUMBER_ON_CBU_BAG,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_ID_NUMBER_ON_CBU_BAG',
       :OLD.CORD_ID_NUMBER_ON_CBU_BAG, :NEW.CORD_ID_NUMBER_ON_CBU_BAG);
  END IF;
  
  
  IF NVL(:OLD.FK_CORD_BACT_CUL_RESULT,0) !=
     NVL(:NEW.FK_CORD_BACT_CUL_RESULT,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_BACT_CUL_RESULT',
       :OLD.FK_CORD_BACT_CUL_RESULT, :NEW.FK_CORD_BACT_CUL_RESULT);
  END IF;
  
  IF NVL(:OLD.FK_CORD_FUNGAL_CUL_RESULT,0) !=
     NVL(:NEW.FK_CORD_FUNGAL_CUL_RESULT,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_FUNGAL_CUL_RESULT',
       :OLD.FK_CORD_FUNGAL_CUL_RESULT, :NEW.FK_CORD_FUNGAL_CUL_RESULT);
  END IF;
  
  IF NVL(:OLD.FK_CORD_ABO_BLOOD_TYPE,0) !=
     NVL(:NEW.FK_CORD_ABO_BLOOD_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_ABO_BLOOD_TYPE',
       :OLD.FK_CORD_ABO_BLOOD_TYPE, :NEW.FK_CORD_ABO_BLOOD_TYPE);
  END IF;
  
  IF NVL(:OLD.FK_CORD_RH_TYPE,0) !=
     NVL(:NEW.FK_CORD_RH_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_RH_TYPE',
       :OLD.FK_CORD_RH_TYPE, :NEW.FK_CORD_RH_TYPE);
  END IF;
  
  IF NVL(:OLD.FK_CORD_BABY_GENDER_ID,0) !=
     NVL(:NEW.FK_CORD_BABY_GENDER_ID,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_BABY_GENDER_ID',
       :OLD.FK_CORD_BABY_GENDER_ID, :NEW.FK_CORD_BABY_GENDER_ID);
  END IF;
  IF NVL(:OLD.FK_CORD_CLINICAL_STATUS,0) !=
     NVL(:NEW.FK_CORD_CLINICAL_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_CLINICAL_STATUS',
       :OLD.FK_CORD_CLINICAL_STATUS, :NEW.FK_CORD_CLINICAL_STATUS);
  END IF; 
  IF NVL(:OLD.FK_CORD_CBU_LIC_STATUS,0) !=
     NVL(:NEW.FK_CORD_CBU_LIC_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CORD_CBU_LIC_STATUS',
       :OLD.FK_CORD_CBU_LIC_STATUS, :NEW.FK_CORD_CBU_LIC_STATUS);
  END IF;
  
  IF NVL(:OLD.CREATOR,0) !=
     NVL(:NEW.CREATOR,0) THEN
     audit_trail.column_update
       (raid, 'CREATOR',
       :OLD.CREATOR, :NEW.CREATOR);
  END IF;
  
  IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    
  if nvl(:old.LAST_MODIFIED_BY,0) !=
    NVL(:new.LAST_MODIFIED_BY,0) then
    Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    end if;

    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    
    IF NVL(:OLD.IP_ADD,0) !=
     NVL(:NEW.IP_ADD,0) THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);     
    END IF;
    
     IF NVL(:OLD.DELETEDFLAG,' ') !=
     NVL(:NEW.DELETEDFLAG,' ') THEN
     audit_trail.column_update
       (raid, 'DELETEDFLAG',
       :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;
    
    IF NVL(:OLD.RID,0) !=
     NVL(:NEW.RID,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.RID, :NEW.RID);     
    END IF;
    
    IF NVL(:OLD.ADDITIONAL_INFO_RECQUIRED,' ') !=
     NVL(:NEW.ADDITIONAL_INFO_RECQUIRED,' ') THEN
     audit_trail.column_update
       (raid, 'ADDITIONAL_INFO_RECQUIRED',
       :OLD.ADDITIONAL_INFO_RECQUIRED, :NEW.ADDITIONAL_INFO_RECQUIRED);
    END IF;
    
    IF NVL(:OLD.CORD_ISBI_DIN_CODE,' ') !=
     NVL(:NEW.CORD_ISBI_DIN_CODE,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_ISBI_DIN_CODE',
       :OLD.CORD_ISBI_DIN_CODE, :NEW.CORD_ISBI_DIN_CODE);
    END IF;
    
    IF NVL(:OLD.CORD_ISIT_PRODUCT_CODE,' ') !=
     NVL(:NEW.CORD_ISIT_PRODUCT_CODE,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_ISIT_PRODUCT_CODE',
       :OLD.CORD_ISIT_PRODUCT_CODE, :NEW.CORD_ISIT_PRODUCT_CODE);
    END IF;
    
    IF NVL(:OLD.CORD_ELIGIBLE_ADDITIONAL_INFO,' ') !=
     NVL(:NEW.CORD_ELIGIBLE_ADDITIONAL_INFO,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_ELIGIBLE_ADDITIONAL_INFO',
       :OLD.CORD_ELIGIBLE_ADDITIONAL_INFO, :NEW.CORD_ELIGIBLE_ADDITIONAL_INFO);
    END IF;
    
    IF NVL(:OLD.FK_CBB_ID,0) !=
      NVL(:NEW.FK_CBB_ID,0) THEN
      audit_trail.column_update
       (raid, 'FK_CBB_ID',
       :OLD.FK_CBB_ID, :NEW.FK_CBB_ID);     
    END IF;
    
    IF NVL(:OLD.FK_CORD_CBU_ELIGIBLE_STATUS,0) !=
      NVL(:NEW.FK_CORD_CBU_ELIGIBLE_STATUS,0) THEN
      audit_trail.column_update
       (raid, 'FK_CORD_CBU_ELIGIBLE_STATUS',
       :OLD.FK_CORD_CBU_ELIGIBLE_STATUS, :NEW.FK_CORD_CBU_ELIGIBLE_STATUS);     
    END IF;
    
    IF NVL(:OLD.FK_CORD_CBU_STATUS,0) !=
      NVL(:NEW.FK_CORD_CBU_STATUS,0) THEN
      audit_trail.column_update
       (raid, 'FK_CORD_CBU_STATUS',
       :OLD.FK_CORD_CBU_STATUS, :NEW.FK_CORD_CBU_STATUS);     
    END IF;
    
     IF NVL(:OLD.FK_CORD_CBU_INELIGIBLE_REASON,0) !=
      NVL(:NEW.FK_CORD_CBU_INELIGIBLE_REASON,0) THEN
      audit_trail.column_update
       (raid, 'FK_CORD_CBU_INELIGIBLE_REASON',
       :OLD.FK_CORD_CBU_INELIGIBLE_REASON, :NEW.FK_CORD_CBU_INELIGIBLE_REASON);     
    END IF;
  IF NVL(:OLD.FK_CORD_CBU_UNLICENSED_REASON,0) !=
      NVL(:NEW.FK_CORD_CBU_UNLICENSED_REASON,0) THEN
      audit_trail.column_update
       (raid, 'FK_CORD_CBU_UNLICENSED_REASON',
       :OLD.FK_CORD_CBU_UNLICENSED_REASON, :NEW.FK_CORD_CBU_UNLICENSED_REASON);     
    END IF;
    
    IF NVL(:OLD.FK_CODELST_EXTERNAL_NAME ,0) !=
      NVL(:NEW.FK_CODELST_EXTERNAL_NAME ,0) THEN
      audit_trail.column_update
       (raid, 'FK_CODELST_EXTERNAL_NAME ',
       :OLD.FK_CODELST_EXTERNAL_NAME , :NEW.FK_CODELST_EXTERNAL_NAME );     
    END IF;
    
    IF NVL(:OLD.FK_CODELST_EXTERNAL_NUM ,0) !=
      NVL(:NEW.FK_CODELST_EXTERNAL_NUM ,0) THEN
      audit_trail.column_update
       (raid, 'FK_CODELST_EXTERNAL_NUM ',
       :OLD.FK_CODELST_EXTERNAL_NUM , :NEW.FK_CODELST_EXTERNAL_NUM );     
    END IF;
    IF NVL(:OLD.FK_CODE_OTHER  ,0) !=
      NVL(:NEW.FK_CODE_OTHER  ,0) THEN
      audit_trail.column_update
       (raid, 'FK_CODE_OTHER  ',
       :OLD.FK_CODE_OTHER  , :NEW.FK_CODE_OTHER  );     
    END IF;
    
    IF NVL(:OLD.IND_OTHER_NAME,' ') !=
     NVL(:NEW.IND_OTHER_NAME,' ') THEN
     audit_trail.column_update
       (raid, 'IND_OTHER_NAME',
       :OLD.IND_OTHER_NAME, :NEW.IND_OTHER_NAME);
    END IF;
    
    IF NVL(:OLD.IND_OTHER_NUM,' ') !=
     NVL(:NEW.IND_OTHER_NUM,' ') THEN
     audit_trail.column_update
       (raid, 'IND_OTHER_NUM',
       :OLD.IND_OTHER_NUM, :NEW.IND_OTHER_NUM);
    END IF;

    IF NVL(:OLD.CORD_CLINICAL_STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.CORD_CLINICAL_STATUS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'CORD_CLINICAL_STATUS_DATE',
       to_char(:OLD.CORD_CLINICAL_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CORD_CLINICAL_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    
     IF NVL(:OLD.CORD_CLINICAL_STATUS_ADDI_INFO,' ') !=
     NVL(:NEW.CORD_CLINICAL_STATUS_ADDI_INFO,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_CLINICAL_STATUS_ADDI_INFO',
       :OLD.CORD_CLINICAL_STATUS_ADDI_INFO, :NEW.CORD_CLINICAL_STATUS_ADDI_INFO);
    END IF;
    
    IF NVL(:OLD.FK_SPECIMEN_ID  ,0) !=
      NVL(:NEW.FK_SPECIMEN_ID  ,0) THEN
      audit_trail.column_update
       (raid, 'FK_SPECIMEN_ID  ',
       :OLD.FK_SPECIMEN_ID  , :NEW.FK_SPECIMEN_ID  );     
    END IF;
     IF NVL(:OLD.REGISTRY_MATERNAL_ID,' ') !=
     NVL(:NEW.REGISTRY_MATERNAL_ID,' ') THEN
     audit_trail.column_update
       (raid, 'REGISTRY_MATERNAL_ID',
       :OLD.REGISTRY_MATERNAL_ID, :NEW.REGISTRY_MATERNAL_ID);
    END IF;
    IF NVL(:OLD.MATERNAL_LOCAL_ID,' ') !=
     NVL(:NEW.MATERNAL_LOCAL_ID,' ') THEN
     audit_trail.column_update
       (raid, 'MATERNAL_LOCAL_ID',
       :OLD.MATERNAL_LOCAL_ID, :NEW.MATERNAL_LOCAL_ID);
    END IF;
    
     IF NVL(:OLD.FK_CODELST_ETHNICITY  ,0) !=
      NVL(:NEW.FK_CODELST_ETHNICITY  ,0) THEN
      audit_trail.column_update
       (raid, 'FK_CODELST_ETHNICITY  ',
       :OLD.FK_CODELST_ETHNICITY  , :NEW.FK_CODELST_ETHNICITY  );     
    END IF;
    
     IF NVL(:OLD.FK_CODELST_RACE  ,0) !=
      NVL(:NEW.FK_CODELST_RACE  ,0) THEN
      audit_trail.column_update
       (raid, 'FK_CODELST_RACE  ',
       :OLD.FK_CODELST_RACE  , :NEW.FK_CODELST_RACE  );     
    END IF;
    
     IF NVL(:OLD.FUNDING_REQUESTED,' ') !=
     NVL(:NEW.FUNDING_REQUESTED,' ') THEN
     audit_trail.column_update
       (raid, 'FUNDING_REQUESTED',
       :OLD.FUNDING_REQUESTED, :NEW.FUNDING_REQUESTED);
    END IF;
    
    IF NVL(:OLD.FUNDED_CBU,' ') !=
     NVL(:NEW.FUNDED_CBU,' ') THEN
     audit_trail.column_update
       (raid, 'FUNDED_CBU',
       :OLD.FUNDED_CBU, :NEW.FUNDED_CBU);
    END IF;
    
     IF NVL(:OLD.CORD_CDR_CBU_CREATED_BY,' ') !=
     NVL(:NEW.CORD_CDR_CBU_CREATED_BY,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_CDR_CBU_CREATED_BY',
       :OLD.CORD_CDR_CBU_CREATED_BY, :NEW.CORD_CDR_CBU_CREATED_BY);
    END IF;
    
    IF NVL(:OLD.CORD_CDR_CBU_LAST_MOD_BY,' ') !=
     NVL(:NEW.CORD_CDR_CBU_LAST_MOD_BY,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_CDR_CBU_LAST_MOD_BY',
       :OLD.CORD_CDR_CBU_LAST_MOD_BY, :NEW.CORD_CDR_CBU_LAST_MOD_BY);
    END IF;
    
     IF NVL(:OLD.CORD_CDR_CBU_LAST_MOD_DT,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.CORD_CDR_CBU_LAST_MOD_DT,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'CORD_CDR_CBU_LAST_MOD_DT',
       to_char(:OLD.CORD_CDR_CBU_LAST_MOD_DT, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CORD_CDR_CBU_LAST_MOD_DT, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
     IF NVL(:OLD.CORD_CREATION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.CORD_CREATION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'CORD_CREATION_DATE',
       to_char(:OLD.CORD_CREATION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CORD_CREATION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
     IF NVL(:OLD.CORD_ENTRY_PROGRESS  ,0) !=
      NVL(:NEW.CORD_ENTRY_PROGRESS  ,0) THEN
      audit_trail.column_update
       (raid, 'CORD_ENTRY_PROGRESS  ',
       :OLD.CORD_ENTRY_PROGRESS  , :NEW.CORD_ENTRY_PROGRESS  );     
    END IF;
    
    IF NVL(:OLD.CORD_SEARCHABLE,' ') !=
     NVL(:NEW.CORD_SEARCHABLE,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_SEARCHABLE',
       :OLD.CORD_SEARCHABLE, :NEW.CORD_SEARCHABLE);
    END IF; 
    IF NVL(:OLD.CORD_DONOR_IDENTI_NUM,' ') !=
     NVL(:NEW.CORD_DONOR_IDENTI_NUM,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_DONOR_IDENTI_NUM',
       :OLD.CORD_DONOR_IDENTI_NUM, :NEW.CORD_DONOR_IDENTI_NUM);
    END IF;    
	IF NVL(:OLD.MULTIPLE_BIRTH,0) !=
      NVL(:NEW.MULTIPLE_BIRTH,0) THEN
      audit_trail.column_update
       (raid, 'MULTIPLE_BIRTH',
       :OLD.MULTIPLE_BIRTH, :NEW.MULTIPLE_BIRTH);     
    END IF;
	IF NVL(:OLD.FRZ_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.FRZ_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'FRZ_DATE',
       to_char(:OLD.FRZ_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.FRZ_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:OLD.HEMOGLOBIN_SCRN,0) !=
      NVL(:NEW.HEMOGLOBIN_SCRN,0) THEN
      audit_trail.column_update
       (raid, 'HEMOGLOBIN_SCRN',
       :OLD.HEMOGLOBIN_SCRN, :NEW.HEMOGLOBIN_SCRN);     
    END IF;
	IF NVL(:OLD.IS_CORD_AVAIL_FOR_NMDP,' ') !=
     NVL(:NEW.IS_CORD_AVAIL_FOR_NMDP,' ') THEN
     audit_trail.column_update
       (raid, 'IS_CORD_AVAIL_FOR_NMDP',:OLD.IS_CORD_AVAIL_FOR_NMDP, :NEW.IS_CORD_AVAIL_FOR_NMDP);
    END IF;
IF NVL(:OLD.CORD_NMDP_CBU_ID,' ') !=
     NVL(:NEW.CORD_NMDP_CBU_ID,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_NMDP_CBU_ID',:OLD.CORD_NMDP_CBU_ID, :NEW.CORD_NMDP_CBU_ID);
    END IF;
	IF NVL(:OLD.CORD_NMDP_MATERNAL_ID,' ') !=
     NVL(:NEW.CORD_NMDP_MATERNAL_ID,' ') THEN
     audit_trail.column_update
       (raid, 'CORD_NMDP_MATERNAL_ID',:OLD.CORD_NMDP_MATERNAL_ID, :NEW.CORD_NMDP_MATERNAL_ID);
    END IF;
	IF NVL(:OLD.CBU_AVAIL_CONFIRM_FLAG,' ') !=
     NVL(:NEW.CBU_AVAIL_CONFIRM_FLAG,' ') THEN
     audit_trail.column_update
       (raid, 'CBU_AVAIL_CONFIRM_FLAG',:OLD.CBU_AVAIL_CONFIRM_FLAG, :NEW.CBU_AVAIL_CONFIRM_FLAG);
    END IF;
IF NVL(:OLD.FK_UNAVAIL_RSN,0) !=
      NVL(:NEW.FK_UNAVAIL_RSN,0) THEN
      audit_trail.column_update
       (raid, 'FK_UNAVAIL_RSN',
       :OLD.FK_UNAVAIL_RSN, :NEW.FK_UNAVAIL_RSN);     
    END IF;
IF NVL(:OLD.GUID,' ') !=
     NVL(:NEW.GUID,' ') THEN
     audit_trail.column_update
       (raid, 'GUID',:OLD.GUID, :NEW.GUID);
    END IF;
IF NVL(:OLD.FK_CBB_PROCEDURE,0) !=
      NVL(:NEW.FK_CBB_PROCEDURE,0) THEN
      audit_trail.column_update
       (raid, 'FK_CBB_PROCEDURE',
       :OLD.FK_CBB_PROCEDURE, :NEW.FK_CBB_PROCEDURE);     
    END IF;
	IF NVL(:OLD.CBB_PROCEDURE_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.CBB_PROCEDURE_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'CBB_PROCEDURE_START_DATE',
       to_char(:OLD.CBB_PROCEDURE_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CBB_PROCEDURE_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;	
	IF NVL(:OLD.CBB_PROCEDURE_END_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.CBB_PROCEDURE_END_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'CBB_PROCEDURE_END_DATE',
       to_char(:OLD.CBB_PROCEDURE_END_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CBB_PROCEDURE_END_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
IF NVL(:OLD.CBB_PROCEDURE_VERSION,' ') !=
     NVL(:NEW.CBB_PROCEDURE_VERSION,' ') THEN
     audit_trail.column_update
       (raid, 'CBB_PROCEDURE_VERSION',:OLD.CBB_PROCEDURE_VERSION, :NEW.CBB_PROCEDURE_VERSION);
    END IF;
IF NVL(:OLD.ADDITI_TYPING_FLAG,' ') !=
     NVL(:NEW.ADDITI_TYPING_FLAG,' ') THEN
     audit_trail.column_update
       (raid, 'ADDITI_TYPING_FLAG',:OLD.ADDITI_TYPING_FLAG, :NEW.ADDITI_TYPING_FLAG);
    END IF;--------------------------
IF NVL(:OLD.FK_PERSON_ID,0) !=
      NVL(:NEW.FK_PERSON_ID,0) THEN
      audit_trail.column_update
       (raid, 'FK_PERSON_ID',
       :OLD.FK_PERSON_ID, :NEW.FK_PERSON_ID);     
    END IF;
	IF NVL(:OLD.HRSA_RACE_ROLLUP,0) !=
      NVL(:NEW.HRSA_RACE_ROLLUP,0) THEN
      audit_trail.column_update
       (raid, 'HRSA_RACE_ROLLUP',
       :OLD.HRSA_RACE_ROLLUP, :NEW.HRSA_RACE_ROLLUP);     
    END IF;
	IF NVL(:OLD.NMDP_RACE_ID,0) !=
      NVL(:NEW.NMDP_RACE_ID,0) THEN
      audit_trail.column_update
       (raid, 'NMDP_RACE_ID',
       :OLD.NMDP_RACE_ID, :NEW.NMDP_RACE_ID);     
    END IF;
	IF NVL(:OLD.PRE_NUCL_CBU_CNT_CNCTRN,0) !=
      NVL(:NEW.PRE_NUCL_CBU_CNT_CNCTRN,0) THEN
      audit_trail.column_update
       (raid, 'PRE_NUCL_CBU_CNT_CNCTRN',:OLD.PRE_NUCL_CBU_CNT_CNCTRN, :NEW.PRE_NUCL_CBU_CNT_CNCTRN);     
    END IF;
	IF NVL(:OLD.PRE_PRCSNG_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
        NVL(:NEW.PRE_PRCSNG_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        audit_trail.column_update
       (raid, 'PRE_PRCSNG_START_DATE',
       to_char(:OLD.PRE_PRCSNG_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PRE_PRCSNG_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
IF NVL(:OLD.FK_CBU_STOR_LOC,0) !=
      NVL(:NEW.FK_CBU_STOR_LOC,0) THEN
      audit_trail.column_update
       (raid, 'FK_CBU_STOR_LOC',
       :OLD.FK_CBU_STOR_LOC, :NEW.FK_CBU_STOR_LOC);     
    END IF;
IF NVL(:OLD.FK_CBU_COLL_SITE,0) !=
      NVL(:NEW.FK_CBU_COLL_SITE,0) THEN
      audit_trail.column_update
       (raid, 'FK_CBU_COLL_SITE',
       :OLD.FK_CBU_COLL_SITE, :NEW.FK_CBU_COLL_SITE);     
    END IF;
IF NVL(:OLD.FK_CBU_COLL_TYPE,0) !=NVL(:NEW.FK_CBU_COLL_TYPE,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'FK_CBU_COLL_TYPE',
       :OLD.FK_CBU_COLL_TYPE, :NEW.FK_CBU_COLL_TYPE);     
    END IF;
IF NVL(:OLD.FK_CBU_DEL_TYPE,0) !=    NVL(:NEW.FK_CBU_DEL_TYPE,0) THEN
      audit_trail.column_update(raid, 'FK_CBU_DEL_TYPE', :OLD.FK_CBU_DEL_TYPE, :NEW.FK_CBU_DEL_TYPE);     
    END IF;
IF NVL(:OLD.BACT_COMMENT,' ') !=
     NVL(:NEW.BACT_COMMENT,' ') THEN
     audit_trail.column_update(raid, 'BACT_COMMENT',:OLD.BACT_COMMENT, :NEW.BACT_COMMENT);
    END IF;
IF NVL(:OLD.FUNG_COMMENT,' ') !=
     NVL(:NEW.FUNG_COMMENT,' ') THEN
     audit_trail.column_update
       (raid, 'FUNG_COMMENT',:OLD.FUNG_COMMENT, :NEW.FUNG_COMMENT);
    END IF;
END ;
/