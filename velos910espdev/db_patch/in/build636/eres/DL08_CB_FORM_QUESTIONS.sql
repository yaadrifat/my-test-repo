--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2F FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='imd_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fda_lic_mfg_inst_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fda_lic_mfg_inst_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='imd_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 4.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 5.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 6.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''not_done'')',','),'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''not_done'')',','),'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''not_done'')',','),'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''not_done'')',','),'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''not_done'')',','),'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/


--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_fda'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.c',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null, null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',','),'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres3'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_cms'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'18.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='treponemal_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='treponemal_ct_ind'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''negi'')',','),'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome2'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres4'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_ind'),null,null,null,'20',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'20.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_ind'),null,null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'21.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_ind'),null,null,null,'22',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'22.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_ind'),null,null,null,'23',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'23.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_ind'),null,null,null,'24',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'24.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_ind'),null,null,null,'25',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres5'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_date'),null,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',','),'25.a',null,null,null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--

--Alter CB_Form_Question Table---

--STARTS ADDING COLUMN TO CB_FORM_QUESTIONS--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'DEFER_VALUE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(DEFER_VALUE VARCHAR2(255 BYTE))';
  end if;
end;
/
--End--
COMMENT ON COLUMN CB_FORM_QUESTIONS.DEFER_VALUE IS 'Stores value from er_codelst.codelst_type where codelst_type like responses';

--UPDATE CB_FORM_QUESTION TABLE--
--QUESTION 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
        where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--QUESTION 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET DEFER_VALUE=(rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP in(''posi'',''not_done'',''indeter'')',',')) where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--END--
