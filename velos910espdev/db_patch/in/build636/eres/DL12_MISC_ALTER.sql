set define off;

--Sql for Table CBB to add column SYSTEM_USER, BMDW, EMDIS,CBU_ASSESSMENT,.

DECLARE
	
	v_column_two_exists number := 0;  
	v_column_three_exists number := 0;
	v_column_four_exists number := 0; 
	v_column_five_exists number := 0; 
	
BEGIN
	
	Select count(*) into v_column_two_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'BMDW'; 
	Select count(*) into v_column_three_exists from user_tab_cols where TABLE_NAME =
'CBB' AND column_name ='EMDIS';
	Select count(*) into v_column_four_exists from user_tab_cols where TABLE_NAME =
'CBB' AND column_name ='CBU_ASSESSMENT';
	Select count(*) into v_column_five_exists from user_tab_cols where TABLE_NAME =
'CBB' AND column_name ='UNIT_REPORT';  


	
   if (v_column_two_exists = 0) then
      execute immediate ('alter table cbb add BMDW varchar2(15)');
  end if;
   if (v_column_three_exists = 0) then
      execute immediate ('alter table cbb add EMDIS varchar2(15)');
  end if;
   if (v_column_four_exists = 0) then
      execute immediate ('alter table cbb add CBU_ASSESSMENT varchar2(1)');
  end if;
   if (v_column_five_exists = 0) then
      execute immediate ('alter table cbb add UNIT_REPORT varchar2(1)');
  end if;
  
  
end;
/

COMMENT ON COLUMN CBB.BMDW IS 'Stores BMDW code.'; 
COMMENT ON COLUMN CBB.EMDIS IS 'Stores EMDIS code.'; 
COMMENT ON COLUMN CBB.CBU_ASSESSMENT IS 'Stores information about cbu assessment.';
COMMENT ON COLUMN CBB.UNIT_REPORT IS 'Stores information about unit report.';
commit;


--==============================================================================================

--STARTS ADDING COLUMN FROM CBB_PROCESSING_PROCEDURES_INFO TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CBB_PROCESSING_PROCEDURES_INFO ADD TEMP_COL NUMBER(19,0)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'update CBB_PROCESSING_PROCEDURES_INFO SET TEMP_COL=MAX_VALUE';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CBB_PROCESSING_PROCEDURES_INFO TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'MAX_VALUE';
  if (v_column_exists = 1) then 
      UPDATE CBB_PROCESSING_PROCEDURES_INFO SET MAX_VALUE=NULL;
	commit;
      execute immediate 'alter table CBB_PROCESSING_PROCEDURES_INFO MODIFY MAX_VALUE number(19,1)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CBB_PROCESSING_PROCEDURES_INFO SET MAX_VALUE=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CBB_PROCESSING_PROCEDURES_INFO DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/

--===============================================================================================

--STARTS ADDING COLUMN FROM CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CBB_PROCESSING_PROCEDURES_INFO ADD TEMP_COL NUMBER(19,0)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'update CBB_PROCESSING_PROCEDURES_INFO SET TEMP_COL=MAX_VALUE2';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CBB_PROCESSING_PROCEDURES_INFO TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'MAX_VALUE2';
  if (v_column_exists = 1) then 
      UPDATE CBB_PROCESSING_PROCEDURES_INFO SET MAX_VALUE2=NULL;
	commit;
      execute immediate 'alter table CBB_PROCESSING_PROCEDURES_INFO MODIFY MAX_VALUE2 number(19,1)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CBB_PROCESSING_PROCEDURES_INFO SET MAX_VALUE2=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CBB_PROCESSING_PROCEDURES_INFO TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CBB_PROCESSING_PROCEDURES_INFO DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/
