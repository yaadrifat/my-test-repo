set define off;
 Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
  (SEQ_ER_LABTEST.nextval,'CBU volume (without anticoagulant/additives)','CBU_VOL',null,null,null);
   
   Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
   (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,(select PK_LABGROUP from ER_LABGROUP where GROUP_NAME like '%PrcsGrp%' and GROUP_TYPE like '%PC%'));
    commit;