CREATE TABLE CB_PACKING_SLIP(
	PK_PACKING_SLIP NUMBER(10,0),
	FK_SHIPMENT NUMBER(10,0),
	FK_ORDER_ID NUMBER(10,0),
	SCH_SHIPMENT_DATE DATE,
	SHIPMENT_TRACKING_NO NUMBER(10,0),
	FK_SHIP_COMPANY_ID NUMBER(10,0),
	CBB_ID NUMBER(10,0),
	FK_CORD_ID NUMBER(10,0),
	FK_SAMPLE_TYPE_AVAIL NUMBER(10,0),
	FK_ALIQUOTS_TYPE NUMBER(10,0),
	PK_HLA NUMBER(10,0),
	CREATOR	NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER(10,0),
	RID NUMBER(10,0)
);

COMMENT ON TABLE CB_PACKING_SLIP IS 'Stores Packing slip Report details';


CREATE SEQUENCE SEQ_CB_PACKING_SLIP MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;


COMMENT ON COLUMN CB_PACKING_SLIP.PK_PACKING_SLIP IS 'Stores sequence generated unique value';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_SHIPMENT IS 'Stores foreign key reference to cb_shipment table';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_ORDER_ID IS 'Stores foreign key reference to er_order table';
COMMENT ON COLUMN CB_PACKING_SLIP.SCH_SHIPMENT_DATE IS 'Stores shipment scheduled date';
COMMENT ON COLUMN CB_PACKING_SLIP.SHIPMENT_TRACKING_NO IS 'Stores shipment tracking no';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_SHIP_COMPANY_ID IS 'Stores shipping company id which is foreign key reference to er_codelst table';
COMMENT ON COLUMN CB_PACKING_SLIP.CBB_ID IS 'Stores foreign key reference to er_site table';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_CORD_ID IS 'Stores foreign key reference to cb_cord table';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_SAMPLE_TYPE_AVAIL IS 'Stores foreign key reference to er_codelst table where codelst_type=sample_type';
COMMENT ON COLUMN CB_PACKING_SLIP.FK_ALIQUOTS_TYPE IS 'Stores foreign key referecne to er_codelst table where codelst_type=aliquots_type';
COMMENT ON COLUMN CB_PACKING_SLIP.PK_HLA IS 'Stores foreign key reference to cb_hla table';
COMMENT ON COLUMN CB_PACKING_SLIP.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN CB_PACKING_SLIP.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN CB_PACKING_SLIP.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN CB_PACKING_SLIP.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN CB_PACKING_SLIP.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN CB_PACKING_SLIP.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN CB_PACKING_SLIP.RID IS 'Uses for Audit trail. Stores sequence generated unique value';

