CREATE OR REPLACE FORCE VIEW "ERES"."REP_ER_ORDER_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "IP_ADD", "PK_ROW_ID", "MODULE_ID")
AS
  SELECT cord.Cord_Registry_Id,
    cord.Cord_Local_Cbu_Id,
    cord.Registry_Maternal_Id,
    cord.maternal_local_id,
    ordr.Created_On ,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = ordr.Creator
    ) Creator,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = ordr.Last_Modified_By
    ) Last_Modified_By,
    ordr.Last_Modified_Date,
    ordr.IP_ADD,
    arm.PK_ROW_ID,
    arm.module_id
  FROM AUDIT_ROW_MODULE arm,
    Cb_Cord cord,
    ER_ORDER ordr,
    ER_ORDER_HEADER hdr
  WHERE ordr.PK_ORDER    = arm.module_id
  AND hdr.order_entityid = cord.pk_cord
  AND hdr.pk_order_header= ordr.fk_order_header
  AND arm.table_name     ='ER_ORDER';