alter table CB_NOTES ADD  (REVIEWED_BY NUMBER(10), CONSULTED_BY NUMBER(10), APPROVED_BY NUMBER(10), AMENDED VARCHAR2(1));


COMMENT ON COLUMN "CB_NOTES"."REVIEWED_BY" IS 'This column will store by whom note is reviewed';	

COMMENT ON COLUMN "CB_NOTES"."CONSULTED_BY" IS 'This column will store by whom note is consulted';	

COMMENT ON COLUMN "CB_NOTES"."APPROVED_BY" IS 'This column will store by who has approved notes';	

COMMENT ON COLUMN "CB_NOTES"."AMENDED" IS 'This column will store wheather the clinical note is amendable or not';	

alter table CB_CORD add(CORD_ENTRY_PROGRESS number(5,2), CORD_SEARCHABLE varchar2(1 ));


COMMENT ON COLUMN "CB_CORD"."CORD_SEARCHABLE" IS 'This column will store wheather the cord is searchable or not.'; 

COMMENT ON COLUMN "CB_CORD"."CORD_ENTRY_PROGRESS" IS 'This will store the progress of cord entry.'; 



Set Define off
---Scripts to alter table ER_ORDER for order changes
---09/07/2011
---Ruchi/Idrush
ALTER TABLE ER_ORDER ADD (TASK_ID NUMBER(10) );
COMMENT ON COLUMN "ER_ORDER"."TASK_ID" IS 'Task Id reference from WF';


ALTER TABLE ER_ORDER ADD (TASK_NAME VARCHAR2(100 BYTE) );
COMMENT ON COLUMN "ER_ORDER"."TASK_NAME" IS 'Task name from WF - It is kept to display the task name from workflow.';

ALTER TABLE ER_ORDER ADD(ASSIGNED_TO VARCHAR2(20 BYTE));
COMMENT ON COLUMN "ER_ORDER"."ASSIGNED_TO" IS 'Cord Blood Bank assigns a search request to a CORD Link user';


ALTER TABLE CB_CORD ADD (CORD_SAMPLE_AT_LAB VARCHAR2(50 BYTE));
COMMENT ON COLUMN "CB_CORD"."CORD_SAMPLE_AT_LAB" IS 'Stores Yes/No based on sample availability at lab';

ALTER TABLE ER_ORDER ADD (ORDER_STATUS_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_STATUS_DATE" IS 'The date the request status was set/modified and saved by the system';

ALTER TABLE ER_ORDER ADD (ORDER_REVIEWED_BY NUMBER(10));
COMMENT ON COLUMN "ER_ORDER"."ORDER_REVIEWED_BY" IS 'Name of person that last reviewed any of the CBU information/file for the CT request';

ALTER TABLE ER_ORDER ADD (ORDER_REVIEWED_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_REVIEWED_DATE" IS 'Date of when the CBU CT file was last reviewed by';

ALTER TABLE ER_ORDER ADD (ORDER_ASSIGNED_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_ASSIGNED_DATE" IS 'A date is entered into this field by the system when the Assigned To field is populated.';

ALTER TABLE ER_ORDER ADD (FK_ORDER_RESOL_BY_CBB NUMBER(10));
COMMENT ON COLUMN "ER_ORDER"."FK_ORDER_RESOL_BY_CBB" IS 'Foreign key column.stores code of resolution code type value from code list table';

ALTER TABLE ER_ORDER ADD (FK_ORDER_RESOL_BY_TC NUMBER(10,0));
COMMENT ON COLUMN "ER_ORDER"."FK_ORDER_RESOL_BY_TC" IS 'Foreign key column.stores code of resolution code type value from code list table';

ALTER TABLE ER_ORDER ADD (ORDER_RESOL_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_RESOL_DATE" IS 'Date when resolution code is selected';

ALTER TABLE ER_ORDER ADD (ORDER_CORD_AVAIL_DATE DATE);
COMMENT ON COLUMN  "ER_ORDER"."ORDER_CORD_AVAIL_DATE" IS 'date when cord is available again for searching patients';

ALTER TABLE ER_ORDER ADD (ORDER_ACK_FLAG VARCHAR2(4 BYTE));
COMMENT ON COLUMN  "ER_ORDER"."ORDER_ACK_FLAG" IS 'Indication taht CBB acknowledged receipt of system generated resolution code';

ALTER TABLE ER_ORDER ADD (ORDER_ACK_DATE DATE);
COMMENT ON COLUMN "ER_ORDER"."ORDER_ACK_DATE" IS 'Date when the CBB acknowledges receipt of the system generated resolution CODE';

ALTER TABLE ER_ORDER ADD (ORDER_ACK_BY NUMBER(10,0));
COMMENT ON COLUMN  "ER_ORDER"."ORDER_ACK_BY" IS 'User Name who acknowledges receipt of the system generated resolution CODE';





