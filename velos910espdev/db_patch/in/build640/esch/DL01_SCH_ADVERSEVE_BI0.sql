CREATE OR REPLACE TRIGGER SCH_ADVERSEVE_BI0 BEFORE INSERT ON SCH_ADVERSEVE
FOR EACH ROW
 WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;

BEGIN
 BEGIN
   v_new_creator := :NEW.creator ;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction (raid, 'SCH_ADVERSEVE', erid, 'I', USR );
--   Added by Ganapathy on 06/23/05 for Audit insert
--   Modified by Khader on 29-01-2008
--   Ankit: Bug-10954  20-08-2012
 insert_data:=:NEW.AE_TREATMENT_COURSE||'|'|| :NEW.FK_CODELST_OUTACTION||'|'||
              :NEW.FK_STUDY||'|'|| :NEW.FK_PER||'|'|| :NEW.AE_SEVERITY||'|'||
     :NEW.AE_BDSYSTEM_AFF||'|'|| :NEW.AE_RELATIONSHIP||'|'|| :NEW.AE_RECOVERY_DESC||'|'||
     :NEW.AE_GRADE||'|'||:NEW.AE_NAME||'|'|| :NEW.AE_LKP_ADVID||'|'|| :NEW.FORM_STATUS||'|'||
     :NEW.CUSTOM_COL_INTERFACE||'|'|| :NEW.PK_ADVEVE||'|'||:NEW.FK_EVENTS1||'|'||
     :NEW.FK_CODLST_AETYPE||'|'||:NEW.AE_DESC||'|'|| TO_CHAR(:NEW.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.AE_ENTERBY||'|'||:NEW.AE_OUTTYPE||'|'
     ||TO_CHAR(:NEW.AE_OUTDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.AE_OUTNOTES||'|'||:NEW.AE_ADDINFO||'|'||
     :NEW.AE_NOTES||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     :NEW.IP_ADD||'|'|| :NEW.MEDDRA||'|'|| :NEW.DICTIONARY||'|'|| :NEW.AE_REPORTEDBY||'|'||:NEW.AE_DISCVRYDATE||'|'||
     :NEW.AE_LOGGEDDATE||'|'|| :NEW.FK_LINK_ADVERSEVE  ;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/