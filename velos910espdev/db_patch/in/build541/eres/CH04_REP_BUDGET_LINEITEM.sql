/* Formatted on 2/9/2010 1:39:50 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_BUDGET_LINEITEM
(
   BGTLI_BUDGET_NAME,
   BGTLI_BUDGET_STATUS,
   BGTLI_STUDY_NUM,
   BGTLI_ORGANIZATION,
   BGTLI_STUDY_TITLE,
   BGTLI_SECTION,
   BGTLI_EVENT,
   BGTLI_CATEGORY,
   BGTLI_APPLY_INDIR,
   BGTLI_SOC,
   BGTLI_UNIT_COST,
   BGTLI_NUM_UNITS,
   BGTLI_APPLY_DISCOUNT,
   BGTLI_RESPONSE_ID,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_STUDY,
   FK_ACCOUNT,
   LINEITM_TOTAL_COST,
   LINEITEM_RESCOST
)
AS
   SELECT   B.BUDGET_NAME BGTLI_BUDGET_NAME,
            F_Get_Fstat (B.BUDGET_STATUS) BGTLI_BUDGET_STATUS,
            (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = B.FK_STUDY)
               BGTLI_STUDY_NUM,
            (SELECT SITE_NAME 
               FROM  ER_SITE
              WHERE PK_SITE = B.FK_SITE)
               BGTLI_ORGANIZATION,
            (SELECT   STUDY_TITLE
               FROM   ER_STUDY
              WHERE   PK_STUDY = B.FK_STUDY)
               BGTLI_STUDY_TITLE,
            BGTSECTION_NAME BGTLI_SECTION,
            LINEITEM_NAME BGTLI_EVENT,
            F_Get_Codelstdesc (FK_CODELST_CATEGORY) BGTLI_CATEGORY,
            F_Get_Yesno (LINEITEM_APPLYINDIRECTS) BGTLI_APPLY_INDIR,
            LINEITEM_STDCARECOST BGTLI_SOC,
            LINEITEM_SPONSORUNIT BGTLI_UNIT_COST,
            LINEITEM_CLINICNOFUNIT BGTLI_NUM_UNITS,
            F_Get_Yesno (LINEITEM_INCOSTDISC) BGTLI_APPLY_DISCOUNT,
            PK_LINEITEM BGTLI_RESPONSE_ID,
            L.RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = L.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = L.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            L.LAST_MODIFIED_DATE,
            L.CREATED_ON,
            B.FK_STUDY FK_STUDY,
            B.FK_ACCOUNT FK_ACCOUNT,
            lineitem_totalcost LINEITM_TOTAL_COST,
            LINEITEM_RESCOST LINEITEM_RESCOST
     FROM   SCH_LINEITEM L,
            SCH_BGTSECTION S,
            SCH_BGTCAL Q,
            SCH_BUDGET B
    WHERE       L.FK_BGTSECTION = S.PK_BUDGETSEC
            AND S.FK_BGTCAL = Q.PK_BGTCAL
            AND Q.FK_BUDGET = B.PK_Budget
	    AND Q.BGTCAL_PROTID is not null
            AND (bgtsection_DELFLAG = 'N' OR bgtsection_delflag IS NULL);


