LOAD DATA
INFILE *
INTO TABLE SCH_MSGTXT 
APPEND
FIELDS TERMINATED BY ','
(pk_msgtxt, msgtxt_type, fk_account, msgtxt_subject,
 msg_file filler char,
"MSGTXT_LONG" LOBFILE (msg_file) TERMINATED BY EOF NULLIF msg_file = 'NONE'
)
BEGINDATA 
46,reset_pass,0,,msg_reset_pass.txt
45,user_esign,0,,msg_reset_esign.txt
44,user_id,0,,msg_id.txt
43,user_pass,0,,msg_pass.txt
42,user_reset,0,,msg_reset.txt