set define off;
declare 

qCount Number;

begin

select count(*) into qCount from ER_CODELST where CODELST_TYPE='tarea' and CODELST_SUBTYP='cutaneous';

if qCount=1 then
  Update ER_CODELST set CODELST_DESC ='Cutaneous (Skin)' where CODELST_TYPE='tarea' and CODELST_SUBTYP='cutaneous';
  commit;
end if;

end;
