set define off;

Update er_codelst set codelst_custom_col ='nonIndustrial', CODELST_SUBTYP ='other', CODELST_DESC ='Externally Peer-Reviewed' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like '%Externally Peer-Reviewed';

Update er_codelst set codelst_custom_col ='nonIndustrial', CODELST_SUBTYP ='insti' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'Institutional';

Update er_codelst set codelst_custom_col ='industrial', CODELST_DESC ='Industrial', CODELST_SUBTYP in ='indus' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'Indust%';

Update er_codelst set codelst_custom_col ='nonIndustrial', CODELST_DESC ='National', CODELST_SUBTYP in ='national' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'National';

Update er_codelst set CODELST_DESC ='Cooperative Group', CODELST_SUBTYP in ='coop' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'Cooperative%';

Update er_codelst set CODELST_DESC ='None', CODELST_SUBTYP in ='none' where 
CODELST_TYPE= 'research_type' AND CODELST_DESC like 'None';

commit;

declare
v_item_exists number default 0;
begin
	select count(*) into v_item_exists from ER_CODELST 
	where codelst_type = 'research_type' 
	and codelst_subtyp = 'national';
	
	if (v_item_exists = 0) then 
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) 
		values (SEQ_ER_CODELST.nextval,null,'research_type','national','National','N',4, 'nonIndustrial');
		dbms_output.put_line('Code-list item Type:research_type Subtype:national inserted');
	else 
		dbms_output.put_line('Code-list item Type:research_type Subtype:national already exists');
	end if;
end;
/

