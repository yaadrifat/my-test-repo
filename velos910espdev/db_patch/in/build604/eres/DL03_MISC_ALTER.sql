Set define off;

--STARTS ADDING COLUMN ADDITI_TYPING_FLAG TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'ADDITI_TYPING_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(ADDITI_TYPING_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.ADDITI_TYPING_FLAG IS 'Stores whether additional typing has done or not';


--STARTS ADDING COLUMN PACKAGE_SLIP_FLAG TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'PACKAGE_SLIP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(PACKAGE_SLIP_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_SHIPMENT.PACKAGE_SLIP_FLAG IS 'Stores whether package slip generated or not'; 


--STARTS ADDING COLUMN FK_UNAVAIL_RSN TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'FK_PERSON_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(FK_PERSON_ID NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.FK_PERSON_ID IS 'This column will store the fk of Person';

--STARTS ADDING COLUMN FK_UNAVAIL_RSN TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'HRSA_RACE_ROLLUP';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(HRSA_RACE_ROLLUP NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.HRSA_RACE_ROLLUP IS 'This column stores HRSA race rollup';



--STARTS ADDING COLUMN FK_UNAVAIL_RSN TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'NMDP_RACE_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(NMDP_RACE_ID NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_CORD.NMDP_RACE_ID IS 'Stores NMDP race id';