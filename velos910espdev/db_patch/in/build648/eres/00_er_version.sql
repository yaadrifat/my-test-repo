set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.1.0 Build#648' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,200,0,'00_er_version.sql',sysdate,'9.1.0 Build#648');

commit;

