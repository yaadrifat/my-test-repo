
Declare
	browserID INTEGER DEFAULT 0;
BEGIN

SELECT PK_BROWSER INTO browserID
FROM ER_BROWSER 
WHERE BROWSER_MODULE = 'allSchedules' AND BROWSER_NAME = 'Patient Schedule Browser';

Update ER_BROWSERCONF set BROWSERCONF_EXPLABEL ='Principal Investigator'
where FK_BROWSER = browserID AND BROWSERCONF_COLNAME='PI';

commit;

END;
/
