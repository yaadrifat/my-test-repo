set define off;

DECLARE

countFlage1 NUMBER(5);
BEGIN

SELECT COUNT(1) INTO countFlage1 FROM USER_TABLES WHERE TABLE_NAME='ER_OBJECT_SETTINGS';
if (countFlage1 > 0) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 7 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Calendar Milestone Setup';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 8 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Manage Budget';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 9 WHERE object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Manage Milestones';
  commit;
end if;

END;
/