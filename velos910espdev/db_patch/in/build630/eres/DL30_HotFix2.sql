--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'mtrn_rsk_ques';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'incomplete' AND codelst_subtyp = 'mtrn_rsk_ques';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'phy_find_ass';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'incomplete' AND codelst_subtyp = 'phy_find_ass';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'incomplete' AND codelst_subtyp = 'oth_med_rec';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'incomplete' AND codelst_subtyp = 'infe_desc_mrk';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'mtrn_rsk_ques';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'ineligible' AND codelst_subtyp = 'mtrn_rsk_ques';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'phy_find_ass';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'ineligible' AND codelst_subtyp = 'phy_find_ass';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'ineligible' AND codelst_subtyp = 'oth_med_rec';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE --
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'ineligible' AND codelst_subtyp = 'infe_desc_mrk';
	commit;
  end if;
end;
/
--END--

