set define off;


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'doc_categ'
    AND codelst_subtyp = 'shpmnt_iter_cat';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Shipment Itinerary' where codelst_type = 'doc_categ'AND codelst_subtyp = 'shpmnt_iter_cat';
	commit;
  end if;
end;
/
--END--

commit;

update ER_CODELST set CODELST_SUBTYP='post_proc_thaw' where CODELST_TYPE='timing_of_test' and codelst_desc like '%Thaw%';

commit;


