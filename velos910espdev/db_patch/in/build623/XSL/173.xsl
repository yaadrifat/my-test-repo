<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">

<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
<xsl:apply-templates select="ROWSET/ROW/LAB_SUMMARY"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program� </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4">Lab Summary Report for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/CBBID"/>
</td>
<td align="right"> Cord Blood Bank ID: 
<xsl:value-of select="ROW/CBB_ID"/>
</td>
</tr>

<tr>
<td align="left"> CBU Identification Number on Bag:   
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/> / <xsl:value-of select="ROW/CORD_LOCAL_CBU_ID"/> / <xsl:value-of select="ROW/CORD_ISBI_DIN_CODE"/> 
</td>
<td align="right"> CBU Registry ID:    
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/>
</td>
</tr>
</table>

<hr class="thickLine" width="100%"/>

<xsl:apply-templates select="ROW"/>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">

 <b><u><font size="3">Processing and Counts </font></u></b> 

<table>
<tr>
<TD> Processing Start Date:  </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PRCSNG_START_DATE" /> 
</TD>
<TD> Bacterial Culture:  </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="BACT_CULTURE" />
</TD>
</tr>

<tr>
<TD>Freeze Date:  </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FRZ_DATE" />
</TD>
<TD> Bacterial Culture Start Date:  </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="BACT_CUL_STRT_DT" />
</TD>
</tr>

<tr>
<TD> ABO Blood Type:   </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="ABO_BLOOD_TYPE" />
</TD>

<TD> Fungal Culture: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FUNGAL_CULTURE" />
</TD>
</tr>

<tr>
<TD> Rh Type:    </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="RH_TYPE" />
</TD>

<TD> Fungal Culture Start Date: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FUNG_CUL_STRT_DT" />
</TD>
</tr>

<tr>
<TD>    </TD> 
<TD class="reportData" align="left" >
</TD>

<TD> Hemoglobinopathy Screening: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="hemoglobin_scrn" />
</TD>
</tr>


</table>


</xsl:for-each>
</xsl:template> 

<xsl:template match="ROWSET/ROW/LAB_SUMMARY"> 

<br></br> <br></br>


<table>
<tr>
<td><b><font size="3">Test</font></b></td>
<td align ="center"><b><font size="3">Pre-Processing</font></b></td>
<td><b><font size="3">Post Processing/Pre-Cryopreservation </font></b></td>
</tr>
</table>

<br></br> <br></br>

<xsl:for-each select="//LAB_SUMMARY_ROW">

<table>

<tr>

<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>

<td class="reportData" align="center" >
<xsl:value-of select="PRE_TEST_RESULT" /> 
</td>

<td class="reportData" align="center" >
<xsl:value-of select="POST_TEST_RESULT" /> 
</td>

</tr>
</table>

</xsl:for-each>

<br></br> <br></br>
<table>
<tr>
<TD><b><font size="3"> Documents </font></b></TD> 
<TD></TD> 
<TD></TD> 
</tr>
</table>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="../LABSUM_ATTACHMENTS/LABSUM_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>

</xsl:template> 


</xsl:stylesheet>