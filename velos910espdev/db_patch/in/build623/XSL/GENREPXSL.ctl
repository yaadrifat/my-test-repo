LOAD DATA INFILE * INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
FK_REPORT,
FK_ACCOUNT,
REPXSL_NAME,
xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE')
BEGINDATA
70,70,0,Patient Budget by Visit,70.xsl,70.xsl
171,171,0,Licensure and Eligibility Report,171.xsl,171.xsl
172,172,0,Infectious Disease Markers Report,172.xsl,172.xsl
173,173,0,Lab Summary Report,173.xsl,173.xsl
174,174,0,Packaging Slip,174.xsl,174.xsl