set define off;

DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_PATTXARM_PATPROT' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_PATTXARM_PATPROT ON ERES.ER_PATTXARM
      (FK_PATPROT)
      LOGGING
      NOPARALLEL';
  end if;
END;
/

DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_PK_PATTXARM' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE UNIQUE INDEX ERES.IDX_PK_PATTXARM ON ERES.ER_PATTXARM
      (PK_PATTXARM)
       LOGGING
       NOPARALLEL';
  end if;
END;
/

DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_constraints WHERE
    constraint_name = 'IDX_PK_PATTXARM' and upper(owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'ALTER TABLE ERES.ER_PATTXARM
      ADD CONSTRAINT IDX_PK_PATTXARM
      PRIMARY KEY (PK_PATTXARM)';
  end if;
END;
/

DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_PK_STUDYTXARM' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE UNIQUE INDEX ERES.IDX_PK_STUDYTXARM ON ERES.ER_STUDYTXARM
      (PK_STUDYTXARM)
      LOGGING
      NOPARALLEL';
  end if;
END;
/

DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_constraints WHERE
    constraint_name = 'IDX_PK_STUDYTXARM' and upper(owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'ALTER TABLE ERES.ER_STUDYTXARM
      ADD CONSTRAINT IDX_PK_STUDYTXARM
      PRIMARY KEY (PK_STUDYTXARM)';
  end if;
END;
/
