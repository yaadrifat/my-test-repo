--To Check NO of Jobs in database 
--SELECT * FROM dba_scheduler_jobs WHERE job_name = 'AUDIT_ROW_MODULE_UPDATE';

--To drop Jobs in database
--BEGIN
--DBMS_SCHEDULER.drop_job (job_name => 'AUDIT_ROW_MODULE_UPDATE');
--END;

set define off;

declare
v_job_exists number default 0;
begin
	Select count(*) into v_job_exists
    from dba_scheduler_jobs
    where job_name = 'AUDIT_ROW_MODULE_UPDATE'; 
	
	if (v_job_exists = 0) then
		--To create Jobs in database
		BEGIN
			DBMS_SCHEDULER.CREATE_JOB (
			job_name           =>  'AUDIT_ROW_MODULE_UPDATE',
			start_date         =>  SYSTIMESTAMP,
			repeat_interval    =>  'FREQ=MINUTELY; INTERVAL=1;',
			job_type           =>  'STORED_PROCEDURE',
			job_action         =>  'ESCH.PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ROW_MODULE_UPDATE',
			enabled            =>  TRUE,
			comments           =>  'To Update Blank Module IDs in Budget Audit row table');
		END;
	end if;	
end;
/

