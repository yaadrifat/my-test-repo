set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CODELST'
    AND COLUMN_NAME = 'CODELST_KIND'; 
	
	if (v_column_exists < 1) then
		execute immediate 'ALTER TABLE ERES.ER_CODELST ADD (CODELST_KIND VARCHAR2(4000))';
		dbms_output.put_line('Column CODELST_KIND is added to ER_CODELST');
	else
		dbms_output.put_line('Column CODELST_KIND already exists in ER_CODELST');
	end if;
END;
/

COMMENT ON COLUMN ER_CODELST.CODELST_KIND IS 'Identifies Code-list item kind';


