-- Build 600
=========================================================================================================================
eResearch: 

=========================================================================================================================
Garuda :
DL01_ER_CODELST_INSERT.sql - This SQL contains the CDR/PF specific ER_CODELST entries. These are specific to NMDP.
	
=========================================================================================================================
eResearch Localization:
 
Following Files have been Modified:

1	formfldbrowser.jsp 
2	patientschedule.jsp
3	patientschedulebyvisit.jsp
4	updateNonSystemuser.jsp
5	nonSystemUserDetails.jsp
6	patientdetailsquick.jsp
7	manageVisits.jsp
8	labelBundle.properties
9	messageBundle.properties
10	LC.java
11	MC.java
12	LC.jsp
13	MC.jsp
=========================================================================================================================