create or replace
function f_cbu_history(cordid number) return SYS_REFCURSOR
IS
v_cord_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_HTY FOR      SELECT 
                             TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             CBUSTATUS.CBU_STATUS_DESC DESCRIPTION1,
                             f_getuser(STATUS.CREATOR) CREATOR,
                             '' ORDERTYPE,
                             STATUS.STATUS_TYPE_CODE TYPECODE,
                             TO_CHAR(SYSDATE,'Mon DD,YYYY') SYSTEMDATE,
                             ''  DESCRIPTION2
                             FROM 
                             CB_ENTITY_STATUS STATUS
                             LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS
                             ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS) 
                             WHERE (STATUS.ENTITY_ID=CORDID AND STATUS.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='CBU')) 
                             AND(STATUS.STATUS_TYPE_CODE='cord_status' OR STATUS.STATUS_TYPE_CODE='Cord_Nmdp_Status') 
                             UNION ALL
                             SELECT 
                             TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             '',
                             f_getuser(STAT.CREATOR),
                             NVL(F_CODELST_DESC(ORD.ORDER_TYPE),'-'),
                             STAT.STATUS_TYPE_CODE,
                             TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                             F_CODELST_DESC(STAT.FK_STATUS_VALUE) 
                             FROM 
                             CB_ENTITY_STATUS STAT  
                             LEFT OUTER JOIN ER_ORDER ORD
                             ON (ORD.PK_ORDER=STAT.ENTITY_ID)
                             WHERE  
                             STAT.ENTITY_ID IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=CORDID) 
                             AND STAT.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='ORDER') 
                             AND STAT.STATUS_TYPE_CODE='order_status' ORDER BY 1 DESC;
RETURN V_CORD_HTY;
END;                 
/



create or replace
function f_cbu_notes(cordid number,categoryval number,visibilityval VARCHAR2) return SYS_REFCURSOR
IS
v_cord_notes SYS_REFCURSOR;

BEGIN
    OPEN v_cord_notes FOR  SELECT
                         F_GETUSER(NOTES.CREATOR) NOTE_CREATOR,
                         F_CODELST_DESC(NOTES.FK_NOTES_TYPE) NOTES_TYPE,
                         NOTES.NOTE_SEQ NOTES_SEQ,
                         F_CODELST_DESC(NOTES.FK_NOTES_CATEGORY) NOTES_CATEGORY,
                        'Posted On' ||' '|| TO_CHAR(NOTES.CREATED_ON,'Mon DD, YYYY HH:MM:SS AM') NOTES_CREATEDON,
                         NOTES.NOTES NOTES_DESC,
                         DECODE(NOTES.NOTE_ASSESSMENT,
                                'tu','Temporaily Unavailable',
                                'no_cause','No Cause for Defferal',
                                'na','Not Available') NOTES_ASSESSMENT,
                        DECODE(NOTES.NOTE_ASSESSMENT,
                                'tu','tu_flag',
                                'na','na_flag') NOTES_REASON_FLAG,
                        nvl(F_CODELST_DESC(NOTES.FK_REASON),' ')NOTES_REASON,
                        NOTES.EXPLAINATION NOTES_EXPLAN1,
                        F_GETUSER(NOTES.SEND_TO) NOTES_SENDTO,
                        NOTES.AMENDED notes_amend
                        FROM 
                        CB_NOTES NOTES
                        WHERE NOTES.ENTITY_ID=CORDID AND NOTES.ENTITY_TYPE=(SELECT CL.PK_CODELST FROM ER_CODELST CL WHERE CL.CODELST_TYPE='entity_type' AND CL.CODELST_SUBTYP='CBU') 
                        AND NOTES.FK_NOTES_CATEGORY=CATEGORYVAL
                        AND notes.visibility in (select distinct visibility from cb_notes b where instr(','||VISIBILITYVAL||',',b.visibility) > 0)
                        ORDER BY NOTES.NOTE_SEQ DESC;
   RETURN v_cord_notes;
END;  
/



COMMIT;
