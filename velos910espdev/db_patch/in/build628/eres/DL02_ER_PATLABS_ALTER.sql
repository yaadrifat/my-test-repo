--STARTS Altering  COLUMN FK_TEST_REASON TABLE ER_PATLABS --
update ER_PATLABS set FK_TEST_REASON =null;
commit;

set define off;

--STARTS Altering  COLUMN FK_TEST_REASON TABLE ER_PATLABS --
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_PATLABS'
    AND column_name = 'FK_TEST_REASON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_PATLABS MODIFY (FK_TEST_REASON VARCHAR2(200))';
  end if;
end;
/
--END--
commit;
