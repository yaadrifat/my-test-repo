UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"EVENT_SCHDATE_DATESORT", "label":"Scheduled Date", 
"resizeable":true,"sortable":true}',browserconf_colname='EVENT_SCHDATE_DATESORT'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='EVENT_SCHDATE'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;
  