/* Formatted on 2/9/2010 1:39:34 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STORAGE
(
   PK_STORAGE,
   STORAGE_ID,
   STORAGE_NAME,
   STORAGE_ALTERNATEID,
   FK_ACCOUNT,
   FK_STORAGE,
   PARENT_STORAGE_ID,
   PARENT_STORAGE,
   FK_STUDY,
   STUDY_NUMBER,
   STORAGE_TYPE,
   STORAGE_COORDINATE_X,
   STORAGE_COORDINATE_Y,
   STORAGE_CAPACITY,
   STORAGE_CAPACITY_CUMM,
   UNITS,
   CHILD_STORAGE_TYPE,
   DIMENTION1,
   DIMENTION2,
   AVAIL_UNITS,
   STATUS_START_DATE,
   STORAGE_STATUS,
   TRACKING_USER,
   TRACKING_NUMBER,
   CREATOR,
   CREATED_ON,
   STORAGE_RID,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   STORAGE_SUBTYP,
   STORAGE_ISTEMPLATE,
   STORAGE_TEMPLATE_TYPE,
   STORAGE_UNIT_CLASS,
   STORAGE_NOTES
)
AS
   SELECT   pk_storage,
            storage_id,
            storage_name,
            STORAGE_ALTERNALEID as STORAGE_ALTERNATEID,
            fk_account,
            a.fk_storage fk_storage,
            (SELECT   aa.storage_id
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage_id,
            (SELECT   aa.storage_name
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage,
            fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = fk_study)
               study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_type,
            storage_coordinate_x,
            storage_coordinate_y,
            storage_cap_number storage_capacity,
            pkg_inventory.f_get_storageCapacity (pk_storage)
               storage_capacity_cumm,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_capacity_units)
               units,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_child_stype)
               child_storage_type,
               DECODE (storage_dim1_naming,
                       'azz',
                       'A>ZZ',
                       '1n',
                       '1 to n')
            || DECODE (storage_dim1_order,
                       'LR',
                       ' (Left to Right)',
                       'BF',
                       ' (Back to Front)')
            || '- '
            || storage_dim1_cellnum
               dimention1,
               DECODE (storage_dim2_naming,
                       'azz',
                       'A>ZZ',
                       '1n',
                       '1 to n')
            || DECODE (storage_dim2_order,
                       'LR',
                       ' (Left to Right)',
                       'BF',
                       ' (Back to Front)')
            || '- '
            || storage_dim2_cellnum
               dimention2,
            storage_avail_unit_count avail_units,
            ss_start_date status_start_date,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_status)
               storage_status,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = fk_user)
               AS tracking_user,
            ss_tracking_number tracking_number,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.creator)
               creator,
            a.created_on created_on,
            a.rid storage_rid,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.last_modified_by)
               last_modified_by,
            a.last_modified_date last_modified_date,
            (SELECT   codelst_subtyp
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_subtyp,
            DECODE (NVL (storage_istemplate, 0), 1, 'Yes', 'No')
               storage_istemplate,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = STORAGE_TEMPLATE_TYPE)
               STORAGE_TEMPLATE_TYPE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = STORAGE_UNIT_CLASS)
               STORAGE_UNIT_CLASS,
            storage_notes
     FROM   ER_STORAGE a, ER_STORAGE_STATUS b
    WHERE   a.pk_storage = b.fk_storage
            AND b.pk_storage_status =
                  (SELECT   MAX (c.pk_storage_status)
                     FROM   ER_STORAGE_STATUS c
                    WHERE   a.pk_storage = c.fk_storage
                            AND c.ss_start_date =
                                  (SELECT   MAX (d.ss_start_date)
                                     FROM   ER_STORAGE_STATUS d
                                    WHERE   a.pk_storage = d.fk_storage));



update er_lkpcol set lkpcol_name = 'STORAGE_ALTERNATEID', lkpcol_keyword = 'STORAGE_ALTERNATEID'
where upper(lkpcol_name) = 'STORAGE_ALTERNALEID' and lkpcol_table = 'ERES.ERV_SPECIMEN_STORAGE';

commit;