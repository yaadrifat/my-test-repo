Declare
var_text1 clob := 'SELECT (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_formlib) AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'') AS tot_forms,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''working'')) AS work_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND (codelst_subtyp = ''working'' OR codelst_subtyp = ''complete''))) AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_createdon, (SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND created_on = (SELECT MAX(created_on) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.last_modified_by) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,fk_formlib,fk_study, pk_patprot
FROM ER_PATPROT a, ER_PATFORMS b
WHERE pk_patprot = fk_patprot
AND fk_study IN (:studyId) AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x   UNION ALL
SELECT ''Adverse EVENTS'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status, (SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS tot_forms,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''working'')) AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_createdby,
(SELECT  to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ESCH.sch_adverseve b
WHERE a.FK_PER = b.FK_PER AND a.fk_study = b.fk_study
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
 UNION ALL
SELECT ''Labs'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id, (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS tot_forms,
0 AS comp_formstat,
0 AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ER_PATLABS b
WHERE a.fk_per = b.fk_per
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
ORDER BY 2,1';

BEGIN
UPDATE er_report
SET REP_SQL_CLOB = var_text1
WHERE pk_report=66;

END;
/
commit;
declare
var_text2 clob:= 'select std.study_number study_number, std.study_title study_title, to_char(std.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
st.site_name site_name,st.pk_site,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type =''al_adve'' and
      AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) ADV_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type = ''al_sadve'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) SADV_COUNT,
nvl((select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    b.ADVINFO_TYPE   = ''O'' and
    b.CODELST_SUBTYP = ''al_death'' and
    b.ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
    ),0) DEATH_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_unexp''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) UNEXP_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_violation''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and 
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) VIO_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_dopped''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) DROP_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_def'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_DEF_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_nr'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_NR_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_pos'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_POS_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_prob'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_PROB_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unknown'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) R_UNK_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unlike'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_UNL_COUNT
from er_site st, er_study std
where pk_study = :studyId and
pk_site in (Select distinct er_user.FK_SITEID
from er_studyteam team, er_user
Where team.fk_study = std.pk_study and
er_user.pk_user = team.fk_user and
nvl(study_team_usr_type,''D'') = ''D'' ) ';

BEGIN

UPDATE er_report
SET REP_SQL_CLOB = var_text2
WHERE pk_report=81;

END;
/
commit;
declare
var_text3 clob := 'SELECT study_number,
study_title,DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_agent''),''Y'',''Agent OR Device'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_beh''),''Y'',''Trials Involving other Interventions'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_na''),''Y'',''Epidemiologic OR other Observational Studies'',
					DECODE(F_Get_Sum4_Data(a.pk_study,''sum4_comp'') ,''Y'',''Companion, ANCILLARY OR Correlative Studies'','''')))) AS trial_type,
F_Get_Codelstdesc(a.fk_codelst_restype) AS research_type,F_Get_Codelstdesc(a.fk_codelst_type) AS study_type,
F_Get_Codelstdesc(a.fk_codelst_scope) AS study_scope,F_Get_Codelstdesc(a.fk_codelst_tarea) AS study_tarea,
F_Get_Sum4_Data(a.pk_study,''sum4_prg'') AS program_code,decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
F_Find_Study_Pi(a.pk_study) || '' '' || DECODE(study_otherprinv, NULL,'''', ''; '' || study_otherprinv )  AS PRINCIPAL_INVESTIGATOR,
F_Get_Codelstdesc(a.fk_codelst_phase) AS study_phase,study_samplsize AS target,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,  TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) study_end_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_12_mos,
race_ne,race_white,race_asian,race_notrep,race_indala,race_blkafr,race_hwnisl,race_unknown,ethnicity_ne,ethnicity_hispanic,ethnicity_unknown,ethnicity_nonhispanic,ethnicity_notreported,gender_ne,gender_male,gender_female,gender_other,gender_unknown
FROM (SELECT fk_study,SUM(race_ne) AS race_ne,SUM(race_white) AS race_white,SUM(race_asian) AS race_asian,SUM(race_notrep) AS  race_notrep,SUM(race_indala) AS race_indala,SUM(race_blkafr) AS race_blkafr,
SUM(race_hwnisl) AS race_hwnisl,SUM(race_unknown) AS race_unknown,SUM(ethnicity_ne) AS ethnicity_ne,SUM(ethnicity_hispanic) AS ethnicity_hispanic,SUM(ethnicity_unknown) AS ethnicity_unknown,
SUM(ethnicity_nonhispanic) AS ethnicity_nonhispanic,SUM(ethnicity_notreported) AS ethnicity_notreported,
SUM(gender_ne) AS gender_ne,SUM(gender_male) AS gender_male,SUM(gender_female) AS gender_female,SUM(gender_other) AS gender_other,
SUM(gender_unknown) AS gender_unknown FROM (SELECT fk_study, DECODE(race,NULL,1,0) AS race_ne,DECODE(race,''race_white'',1,0) AS race_white,DECODE(race,''race_asian'',1,0) AS race_asian,
DECODE(race,''race_notrep'',1,0) AS race_notrep, DECODE(race,''race_indala'',1,0) AS race_indala, DECODE(race,''race_blkafr'',1,0) AS race_blkafr,
DECODE(race,''race_hwnisl'',1,0) AS race_hwnisl, DECODE(race,''race_unknown'',1,0) AS race_unknown,DECODE(ethnicity,NULL,1,0) AS ethnicity_ne,
DECODE(ethnicity,''hispanic'',1,0) AS ethnicity_hispanic, DECODE(ethnicity,''Unknown'',1,0) AS ethnicity_unknown,DECODE(ethnicity,''nonhispanic'',1,0) AS ethnicity_nonhispanic,DECODE(ethnicity,''notreported'',1,0) AS ethnicity_notreported,
DECODE(gender,NULL,1,0) AS gender_ne, DECODE(gender,''male'',1,0) AS gender_male,
DECODE(gender,''female'',1,0) AS gender_female,
DECODE(gender,''other'',1,0) AS gender_other,
DECODE(gender,''Unknown'',1,0) AS gender_unknown FROM  (
SELECT fk_study,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender) AS gender,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity
FROM EPAT.person, ER_PATPROT a
WHERE pk_person = fk_per AND fk_site IN (:orgId) AND
patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
patprot_stat = 1
AND fk_study IN(SELECT pk_study FROM ER_STUDY WHERE pk_study IN (SELECT  DISTINCT fk_study FROM ER_STUDYTEAM WHERE
fk_codelst_tmrole =(SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp=''role_prin'')
AND fk_user IN (:userId) OR STUDY_PRINV IN (:userId) OR STUDY_PRINV IS NULL ))
)) GROUP BY fk_study
), ER_STUDY a WHERE pk_study = fk_study AND fk_account = :sessAccId
AND fk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
ORDER BY study_number';
BEGIN

UPDATE er_report
SET REP_SQL_CLOB = var_text3
WHERE pk_report=117;
END;
/
commit;