--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_01';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_01','New Request Received (CT - Ship Sample)','N',NULL,SYSDATE,SYSDATE,NULL,'A new CT - Ship Sample request has been requested for <CBU Registry ID>','User should be able to receive an alert when a new request is received for CT - Ship Sample');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_02';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_02','New Request Received (CT - Sample at Lab)','N',NULL,SYSDATE,SYSDATE,NULL,'A new CT - Sample at Lab request has been requested for <CBU Registry ID>','User should be able to receive an alert when a new request is received for CT - Sample at Lab');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_03';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_03','New Request Received (HE)','N',NULL,SYSDATE,SYSDATE,NULL,'A new HE - Sample at Lab request has been requested for <CBU Registry ID>','User should be able to receive an alert when a new request is received for HE.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_04';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_04','New Request Received (OR)','N',NULL,SYSDATE,SYSDATE,NULL,'A new OR - Sample at Lab request has been requested for <CBU Registry ID>','System should alert the user if the Scheduled Ship Date is approaching and the "Final CBU Review" is not complete - 2 day prior to Scheduled Ship Date.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_05';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_05','Final CBU Review not complete','N',NULL,SYSDATE,SYSDATE,NULL,'The Final CBU Review task must be completed prior to shipping the CBU.  Ship Date is scheduled to occur tomorrow ','User should confirm shipment either on or after day of Scheduled Ship Date.  Alert user if Scheduled Ship Date has not been confirmed after 2 days');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_06';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_06','Final CBU Review not complete','N',NULL,SYSDATE,SYSDATE,NULL,'The Final CBU Review" task must be completed prior to shipping the CBU.  Ship Date is scheduled to occur on <Shipment Scheduled Date>','User should confirm shipment either on or after day of Scheduled Ship Date.  Alert user if Scheduled Ship Date has not been confirmed after 7 days');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_07';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_07','Confirm Shipment not complete','N',NULL,SYSDATE,SYSDATE,NULL,'CBU Scheduled Ship Date has not been confirmed by CBB.  Please confirm ship date.','User should confirm shipment either on or after day of Scheduled Ship Date.  Alert user if Scheduled Ship Date has not been confirmed after 2 days');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_08';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_08','Confirm Shipment not complete','N',NULL,SYSDATE,SYSDATE,NULL,'CBU Scheduled Ship Date has not been confirmed by CBB.  Please confirm ship date.','User should confirm shipment either on or after day of Scheduled Ship Date.  Alert user if Scheduled Ship Date has not been confirmed after 7 days');
	commit;
  end if;
end;
/
--END--
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_09';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_09','More Recent HLA Typing not entered','N',NULL,SYSDATE,SYSDATE,NULL,'Updated HLA Typing has been indicated but not entered.  Updated HLA must be entered prior to shipping sample.','User may update HLA typing information at the time of CT (Additional Testing and Typing task).  However, user may not enter immediately and choose to enter later.');
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_10';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_10','More Recent HLA Typing not entered','N',NULL,SYSDATE,SYSDATE,NULL,'Updated HLA Typing has been indicated but not entered.  Updated HLA must be entered prior to generating the CT packing slip.','If user answers Yes to Is more recent HLA typing available?  The HLA typing information must be entered in CDR.  User cannot generate packing slip until typing is up-to-date.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_11';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_11','CT Shipment Overdue','N',NULL,SYSDATE,SYSDATE,NULL,'A CT Shipment Date for <CBU Registry ID> is overdue.','Two days have passed the CT Request Date and no CT Ship Date has been entered.');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_12';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_12','Minimum Criteria Not Complete by CM','N',NULL,SYSDATE,SYSDATE,NULL,'The Shipment Packet cannot be printed until the Minimum Criteria task is completed by the Case Manager. Contact the Case Manager.','Prompt the CBB if they attempt to print the Shipment Packet prior to the Minimum Criteria task being completed by the CM.  CDR will have indication that it is complete.  If not, CBB cannot print the Shipment Package.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_13';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_13','Sample Not Available','N',NULL,SYSDATE,SYSDATE,NULL,'According to the inventory, the Sample Type selected does not exist.  Please check the Sample Inventory or choose another Sample Type.','If selected Sample Type is not available according to the Sample Inventory, prompt the user to check their inventory and possibly select a different Sample Type.');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_14';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_14','Generating Packing Slip','N',NULL,SYSDATE,SYSDATE,NULL,'All required fields must be entered prior to generating the CT Packing Slip.','If CT Shipment Information is not entered, notify the user that this info is not entered yet.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_15';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_15','Change in User Assignment','N',NULL,SYSDATE,SYSDATE,NULL,'<Request Type> Request for <CBU Registry ID> has been re-assigned to you.','If CT Shipment Information is not entered, notify the user that this info is not entered yet.');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_16';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_16','Results Received','N',NULL,SYSDATE,SYSDATE,NULL,'Confirmatory Typing results are available for viewing for <CBU Registry ID>.','Alert the user when new HLA results from CT Typing (either Sample at Lab OR Ship Sample) is completed by the lab and results are available for viewing.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_17';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_17','Funding Report Available','N',NULL,SYSDATE,SYSDATE,NULL,'A new funding report for CBB <CBB ID> is ready for review.','A new funding report for CBB ID is ready for the banks review.');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_18';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_18','Product Recovery Form Received','N',NULL,SYSDATE,SYSDATE,NULL,'A product recovery form for <CBU Registry ID> has been received.','A product recovery form for CBU Registry ID has been received.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_19';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_19','CBU Infused','N',NULL,SYSDATE,SYSDATE,NULL,'The status for <CBU Registry ID> has changed to INFUSED.','The status for CBU Registry ID has changed to INFUSED.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_20';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_20','Update to Current Request Received','N',NULL,SYSDATE,SYSDATE,NULL,'The  <Request Type> Request for <CBU Registry ID> has been modified.','User should be able to receive an alert when a current request has been updated by the system (modification to the actual order).');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_21';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_21','Change in CBU Status','N',NULL,SYSDATE,SYSDATE,NULL,'You have chosen to change the local CBU status reason to <Reason/Local Status>.  This will result in a change to CBU Status (Registry) to <CBU Registry Status>. Would you like to proceed?','User should receive an on-screen alert when they choose to change the CBU local status (Reason) causing a terminal change in the CBU Registry Status (TU or NA, etc).');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_22';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_22','CBU Status Not Saved','N',NULL,SYSDATE,SYSDATE,NULL,'The change in CBU Status for  <CBU Registry ID> has not been saved.  Do you want to continue without saving?','User should receive a prompt when the CBU status has been changed AND when the user navigates away from the Update CBU Status utility without saving or selects Cancel.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_23';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_23','HLA Results Uploaded','N',NULL,SYSDATE,SYSDATE,NULL,'<CBU or Patient> HLA Results have been uploaded for <CBU Registry ID> to the clinical record.','This alert notifies the user that HLA results (either CBU or patient HLA) have been uploaded into the CDR when a CBU is active at search.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_24';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_24','Shipment Itinerary Uploaded','N',NULL,SYSDATE,SYSDATE,NULL,'The Shipment Itinerary has been uploaded for <CBU Registry ID> to the clinical record.','This alert notifies the user that the shipment itinerary has been uploaded into the CDR when a CBU is active at OR.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_25';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_25','DUMN Uploaded','N',NULL,SYSDATE,SYSDATE,NULL,'The Declaration of Urgent Medical Need (DUMN) form has been uploaded to the clinical record for <CBU Registry ID>.','This alert notifies the user that the DUMN form has been uploaded into the CDR (OR requests).');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_26';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_26','Receipt of CBU Form Uploaded','N',NULL,SYSDATE,SYSDATE,NULL,'The Receipt of Cord Blood Unit form has been uploaded to the clinical record for <CBU Registry ID>.','This alert notifies the user that the receipt for Cord Blood Unit form has been uploaded into the CDR (OR requests).');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_27';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_27','Resolution Received (All)','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when a Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_28';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert','alert_28','Specific Resolution Received (Custom)','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when a specific Resolution has been received for a request by the TC (system), NOT by the CBB user.');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_29';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_29','Permanently unavailable Medically Deferred','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_30';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_30','Aliquots Gone','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_31';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_31','Cord Unavail - Other Reasons','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_32';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_32','Shipped for Other','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_33';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_33','Reserved for Other','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_34';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_34','Cord quarantined','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_35';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
  VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_35','Cord Destroyed or Damaged','N',NULL,SYSDATE,SYSDATE,NULL,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'doc_categ'
    AND codelst_subtyp = 'post_shpmnt';
  if (v_record_exists = 0) then
     insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'doc_categ','post_shpmnt','Post-Shipment','N',12,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
	  v_record_exists number := 0;  
	BEGIN
	  Select count(*) into v_record_exists
	    from er_codelst
	    where codelst_type = 'post_shpmnt'
	    AND codelst_subtyp = 'cbu_receipt';
	  if (v_record_exists = 0) then
	    	
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'post_shpmnt','cbu_receipt','CBU Receipt','N',1,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
		commit;
	  end if;
	end;
/
--END--
	--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
	  v_record_exists number := 0;  
	BEGIN
	  Select count(*) into v_record_exists
	    from er_codelst
	    where codelst_type = 'post_shpmnt'
	    AND codelst_subtyp = 'other';
	  if (v_record_exists = 0) then
		  insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'post_shpmnt','other','Other','N',2,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
		commit;
	  end if;
	end;
/
--END--

