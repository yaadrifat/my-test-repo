<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" 
indent="yes"/>
<xsl:key name="RecordsByCategory" match="ROW" use="BGTSECTION_NAME" />
<xsl:key name="RecordsByCategorySOC" match="ROW" 
use="concat(BGTSECTION_NAME, ' ', STANDARD_OF_CARE)" /> 
<xsl:key name="RecordsBySOC" match="ROW" use="STANDARD_OF_CARE" /> 
<xsl:key name="RecordsByPersonnel" match="ROW" use="CATEGORY_SUBTYP" />
<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="mode"/>
<xsl:param name="budgetTemplate"/>
<xsl:param name="pkBgtCal"/>
<xsl:param name="budgetStatus"/>
<xsl:param name="pkBudget"/>
<xsl:param name="pageRight"/>
<xsl:param name="includedIn"/>
<xsl:param name="from"/>
<xsl:param name="ddList"/>
<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="id">fromWhere</xsl:attribute>
	<xsl:attribute name="name">fromWhere</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of 
select="$from"/></xsl:attribute>
</input>
        
<xsl:if test="$cond='T'">
    <xsl:if test="$mode='V'">
        <table width="100%" >
        <tr class="reportGreyRow">
        <td class="reportPanel"> 
        VELMESSGE[M_Download_ReportIn]<!-- Download the report in -->: 
        <A href='{$wd}' >
        VELLABEL[L_Word_Format]<!-- Word Format -->
        </A> 
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$xd}' >
        VELLABEL[L_Excel_Format]<!-- Excel Format -->
        </A>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$hd}' >
        VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format 
-->
        </A> 
        </td>
        </tr>
        </table>
    </xsl:if>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<xsl:variable name="excludeSOC_flag">
<xsl:value-of select="//BGTCAL_EXCLDSOCFLAG"/>
</xsl:variable>

<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
    <xsl:if test="$hdrflag='1'">
    <TR>
    <TD WIDTH="100%" ALIGN="CENTER">
    <img src="{$hdrFileName}"/>
    </TD>
    </TR>
    </xsl:if>
    <TR>
    <TD class="reportName" WIDTH="100%" ALIGN="CENTER">
    <xsl:value-of select="$repName" />
    </TD>
    </TR>
    </TABLE>
    <hr class="thickLine" />

    <TABLE WIDTH="100%" >
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    VELLABEL[L_Budget_Name]<!-- Budget Name -->: </TD><TD 
class="reportData"><b><xsl:value-of select="//BUDGET_NAME" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	VELLABEL[L_Budget_Version]<!-- Budget Version 
-->: </TD><TD class="reportData" align="left"><b><xsl:value-of 
select="//BUDGET_VERSION" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	VELLABEL[L_Budget_Description]<!-- Budget 
Description -->: </TD><TD class="reportData"><b><xsl:value-of 
select="//BUDGET_DESC" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="left" width="22%">
		    	VELLABEL[L_Study_Number]<!-- Study Number -->: 
</TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" 
/></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="LEFT" width="22%">
		    	VELLABEL[L_Study_Title]<!-- Study Title -->: 
</TD><TD class="reportData"><b><xsl:value-of select="//STUDY_TITLE" 
/></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="LEFT" width="22%">
		    	VELLABEL[L_Protocol_Calendar]<!-- Protocol 
Calendar -->: </TD><TD class="reportData"><b><xsl:value-of 
select="//PROT_CALENDAR" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="left" width="22%">
		    	VELLABEL[L_Organization]<!-- Organization -->: 
</TD><TD class="reportData"><b><xsl:value-of select="//SITE_NAME" /></b>
		    </TD>
	    </TR>
    </TABLE>
    <hr class="thinLine" />
</xsl:if> 
<xsl:variable name="divHt">
	<xsl:choose>
		<xsl:when test="$from='study'">300</xsl:when>
		<xsl:otherwise>320</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<div id="TableContainer">
	<xsl:if test="$mode!='V'">
		<xsl:attribute 
name="class">TableContainer</xsl:attribute>
		<xsl:attribute name="style">height:<xsl:value-of 
select="$divHt"/>px;</xsl:attribute>
	</xsl:if>
    <TABLE id="scrollTab" WIDTH="100%" border="1" CELLSPACING="1">
    	<xsl:if test="$mode!='V'">
    		<xsl:attribute name="class">scrollTable</xsl:attribute>
    		<xsl:attribute 
name="style">background-color:black;</xsl:attribute>
    	</xsl:if>
	<thead>
		<xsl:if test="$mode!='V'">
    		<xsl:attribute name="class">fixedHeader 
headerFormat</xsl:attribute>
    	</xsl:if>
        <TR>
        	<xsl:if test="$mode!='V'">
    			<xsl:attribute name="class">title</xsl:attribute>
    		</xsl:if>
	<xsl:choose>
		<xsl:when test="$mode='V'">
			<TH class="reportHeading" WIDTH="8%" 
ALIGN="CENTER">VELLABEL[L_Event]<!-- Event --></TH>
			<TH class="reportHeading" WIDTH="8%" 
ALIGN="CENTER">VELLABEL[L_Category]<!-- Category --></TH>
			<TH class="reportHeading" WIDTH="8%" 
ALIGN="CENTER">VELLABEL[L_Cost_Type]<!-- Cost Type --></TH>
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_Unit_Cost]<!-- Unit Cost --></TH>
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER" 
title="VELLABEL[L_Number_OfUnits]">VELLABEL[L_Units]</TH><!-- <TH 
class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="Number of 
Units">Units</TH> -->
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER" 
title="VELMESSGE[M_Apply_DcntOrMarkup]">VELLABEL[L_DOrM]</TH><!-- <TH 
class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="Apply Discount OR 
Markup">D/M</TH> -->
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_Direct_CostOrPat]<!-- Direct Cost/Patient 
--></TH>
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_IOrD_Applied]<!-- I/D Applied --></TH>
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_IOrD_Amount]<!-- I/D Amount --></TH>
		</xsl:when> 
		<xsl:otherwise>
			<TH class="reportHeading" WIDTH="10%" 
ALIGN="CENTER">VELLABEL[L_Event]<!-- Event --></TH>
			<TH class="reportHeading" WIDTH="10%" 
ALIGN="CENTER">VELLABEL[L_Category]<!-- Category --></TH>
			<TH class="reportHeading" WIDTH="10%" 
ALIGN="CENTER">VELLABEL[L_Cost_Type]<!-- Cost Type --></TH>
			<TH class="reportHeading" WIDTH="6%" 
ALIGN="CENTER">VELLABEL[L_Unit_Cost]<!-- Unit Cost --></TH>
			<TH class="reportHeading" WIDTH="6%" 
ALIGN="CENTER" 
title="VELLABEL[L_Number_OfUnits]">VELLABEL[L_Units]</TH><!-- <TH 
class="reportHeading" WIDTH="6%" ALIGN="CENTER" title="Number of 
Units">Units</TH> -->
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER" 
title="VELMESSGE[M_Apply_DcntOrMarkup]">VELLABEL[L_DOrM]</TH><!-- <TH 
class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="Apply Discount OR 
Markup">D/M</TH> -->
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_Direct_CostOrPat]<!-- Direct Cost/Patient 
--></TH>
			<TH class="reportHeading" WIDTH="5%" 
ALIGN="CENTER">VELLABEL[L_IOrD_Applied]<!-- I/D Applied --></TH>
		</xsl:otherwise>    
	</xsl:choose>
			<TH class="reportHeading" WIDTH="8%" 
ALIGN="CENTER">VELLABEL[L_Total_CostOrPat]<!-- Total Cost/Patient 
--></TH>
			<TH class="reportHeading" WIDTH="8%" 
ALIGN="CENTER">VELLABEL[L_CostOrAll_Pat]<!-- Cost/ All Patients --></TH>
			<xsl:if test="$budgetTemplate='C'">
				<TH class="reportHeading" WIDTH="10%" 
ALIGN="CENTER">VELLABEL[L_Sponsor_Amount]<!-- Sponsor Amount --></TH>
				<TH class="reportHeading" WIDTH="10%" 
ALIGN="CENTER">VELLABEL[L_Variance]<!-- Variance --></TH>
			</xsl:if>
		</TR>
	</thead>
	
    <tbody>
    	<xsl:if test="$mode!='V'">
    		<xsl:attribute name="class">scrollContent 
bodyFormat</xsl:attribute>
    		<xsl:attribute name="style">height:280px;</xsl:attribute>
    	</xsl:if>
	<xsl:variable name="colsp">
		<xsl:choose>
			<xsl:when test="$budgetTemplate='C' and 
$mode='V'">13</xsl:when>
			<xsl:when test="$budgetTemplate='C' and 
$mode='M'">12</xsl:when>
			<xsl:when test="$budgetTemplate!='C' and 
$mode='M'">10</xsl:when>
			<xsl:otherwise>11</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:if test="$from='study'">
	<TR style="background-color:white;">
	<TD>
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		<table width="100%" CELLSPACING="1">
			<tr>
				<TD class="reportData"><b><font 
color="red"><xsl:value-of select="//PROT_CALENDAR" /> 
</font></b>&#xa0;&#xa0;
				<!--xsl:if test="$mode='M'">
					<a>
						<xsl:attribute 
name="href">javascript:void(0);</xsl:attribute>
						<xsl:attribute 
name="onClick">openBudgetCalAttributes(<xsl:value-of 
select="$pkBgtCal"/>,
						<xsl:value-of 
select="$pkBudget"/>,<xsl:value-of select="$pageRight"/>,'<xsl:value-of 
select="$budgetStatus"/>')</xsl:attribute>
						Edit Calculation 
Attributes
					</a>
				</xsl:if-->&#xa0;&#xa0;&#xa0;&#xa0;
				</TD>
				<xsl:variable name="cur" 
select='position()' />
				<TD align="right" valign="top" 
width="60%">
					<table CELLSPACING="1">
						<tr id="calIcons" 
name="calIcons" style="display:block">
							<TD valign="top" 
width="15%">
								<font 
size ="-2" color="#696969"><b>VELLABEL[L_Reports]<!-- Reports 
--></b></font><BR/>
								
<xsl:value-of select="$ddList" disable-output-escaping="yes"/>
								
<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									
<xsl:attribute name="onClick">openCBBudReport(<xsl:value-of 
select="$cur"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of 
select="$pkBgtCal"/>)</xsl:attribute>
									
<img src="../images/jpg/preview.gif" 
alt="VELLABEL[L_Display_Report]" width="16" height="18" border="0" 
align="absbotton"/></A><!-- <img 
src="../images/jpg/preview.gif" alt="Display 
Report" width="16" height="18" border="0" align="absbotton"/></A> -->
							</TD>
							<TD 
align="center" valign="top" width="7%">
								<font 
size="-2" color="#696969"><b>VELLABEL[L_Sections]<!-- Sections 
--></b></font><BR/>
								
<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									
<xsl:attribute name="onClick">openCBSections(<xsl:value-of 
select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of 
select="$budgetTemplate"/>')</xsl:attribute>
									
<img src="./images/section.gif" alt="VELMESSGE[M_AddEdt_Sec]" width="16" 
height="18" border="1" align="absbotton"/></A><!-- <img 
src="./images/section.gif" alt="Add and Edit Sections" width="16" 
height="18" border="1" align="absbotton"/></A> -->
							</TD>
							<TD 
align="center" valign="top" width="9%">
								<font 
size="-2" color="#696969"><b>VELLABEL[L_Repeating]<!-- Repeating 
--></b></font><BR/>
								
<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									
<xsl:attribute name="onClick">openCBRepeatLineItems(<xsl:value-of 
select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of 
select="$budgetTemplate"/>')</xsl:attribute>
									
<img src="./images/RepeatingLineItems.gif" 
alt="VELMESSGE[M_AddEdt_RptLineitems]" width="16" height="18" border="1" 
align="absbotton"/></A><!-- <img src="./images/RepeatingLineItems.gif" 
alt="Add and Edit Repeating Lineitems" width="16" height="18" border="1" 
align="absbotton"/></A> -->
							</TD>
							<TD 
align="center" valign="top" width="10%">
								<font 
size ="-2" color="#696969"><b>VELLABEL[L_Personnel_Costs]<!-- Personnel 
Costs --></b></font><BR/>
								
<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
								
<xsl:attribute name="onClick">openCBEditPersonnelCost(<xsl:value-of 
select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of 
select="$budgetTemplate"/>')</xsl:attribute>
								<img 
src="./images/PersonnelCost.gif" alt="VELMESSGE[M_AddEdt_Personnel]" 
width="16" height="18" border="1" align="absbotton"/></A><!-- <img 
src="./images/PersonnelCost.gif" alt="Add and Edit Personnel Costs" 
width="16" height="18" border="1" align="absbotton"/></A> -->
							</TD>
							<TD 
align="center" valign="top" width="10%">
								<font 
size="-2" color="#696969"><b>VELLABEL[L_Exclude_Multiple]<!-- Exclude 
Multiple --></b></font><BR/>
								
<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
								
<xsl:attribute name="onClick">openCBDelete(<xsl:value-of 
select="$pkBgtCal"/>)</xsl:attribute>
								<img 
src="./images/delete.gif" alt="VELLABEL[L_Exclude_Multiple]" width="16" 
height="18" border="1" align="absbotton"/></A><!-- <img 
src="./images/delete.gif" alt="Exclude Multiple" width="16" height="18" 
border="1" align="absbotton"/></A> -->
							</TD>
						</tr>
					</table>
				</TD>	
			</tr>
		</table>
	</TD>
	</TR>
	</xsl:if>
    <xsl:for-each select="ROW[count(. | key('RecordsByCategory', 
BGTSECTION_NAME)[1])=1]">
<TR style="background-color:white;" >

<xsl:variable name="sectionType">
<xsl:value-of select="BGTSECTION_TYPE"/>
</xsl:variable>

<xsl:variable name="sectionTypeText">
<xsl:choose>
<xsl:when test="$sectionType='P'">VELLABEL[L_PerPatient_Fees]<!-- Per 
Patient Fees --></xsl:when> 
<xsl:otherwise>VELLABEL[L_One_TimeFees]<!-- One Time Fees 
--></xsl:otherwise>
</xsl:choose> 
</xsl:variable>
<xsl:variable name="section"><xsl:value-of 
select="BGTSECTION_NAME"/></xsl:variable>
<xsl:variable name="vsectionName_len">
	<xsl:value-of select="string-length(BGTSECTION_NAME)"/>
</xsl:variable>
<xsl:variable name="vsectionName">
	<xsl:choose>
		<xsl:when test="($vsectionName_len &gt; 
35)"><xsl:value-of select="concat(substring(BGTSECTION_NAME,1,35),' 
','...')"/></xsl:when> 
		<xsl:otherwise><xsl:value-of 
select="BGTSECTION_NAME"/></xsl:otherwise>
	</xsl:choose> 
</xsl:variable>

<TD class="reportData" colspan="6">
   	<xsl:attribute name="title"><xsl:value-of 
select="$section"/></xsl:attribute>
	<b><xsl:value-of select="$vsectionName" 
/></b>&#xa0;&#xa0;<i>(<xsl:value-of select="$sectionTypeText"/>)</i> 
&#xa0;&#xa0;
</TD>


<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$mode='V'">3</xsl:when>
		<xsl:otherwise>2</xsl:otherwise>
	</xsl:choose>
</xsl:variable>


<TD class="reportData" align="right">
<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
	<xsl:if test="($sectionType='P')">
		<b>VELLABEL[L_Number_OfPat]<!-- Number of Patients -->: 
<xsl:value-of select="BGTSECTION_PATNO" /></b>
	</xsl:if>&#xa0;
</TD>

<TD colspan="4">

<xsl:variable name="visit">
<xsl:value-of select="BUDGETSEC_FKVISIT"/>
</xsl:variable>

<xsl:variable name="allowSubmitSection">
<xsl:choose>
<xsl:when test="(($includedIn='P' and $visit = 0) or 
$includedIn='')">1</xsl:when> 
<xsl:otherwise>0</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<xsl:if test="($mode='M')">
    <a>
        <xsl:attribute   name="href">javascript:void(0);</xsl:attribute>  
<xsl:attribute name="onClick">openLineItem(<xsl:value-of 
select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of 
select="PK_BUDGETSEC" />,'<xsl:value-of 
select="BGTSECTION_NAME"/>','<xsl:value-of 
select="$budgetStatus"/>','M','<xsl:value-of 
select="$pageRight"/>','','','','<xsl:value-of 
select="$allowSubmitSection"/>','<xsl:value-of 
select="$budgetTemplate"/>')</xsl:attribute> 
VELLABEL[L_EditSection_Lineitems]<!-- Edit Section Lineitems --></a>
 </xsl:if>       

   <xsl:if test="($mode='M' and $allowSubmitSection=1)">
        &#xa0;&#xa0;
 
        <a>
        <xsl:attribute     
name="href">javascript:void(0);</xsl:attribute>
        <xsl:attribute name="onClick">openLineItem(<xsl:value-of 
select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of 
select="PK_BUDGETSEC" />,'<xsl:value-of 
select="BGTSECTION_NAME"/>','<xsl:value-of 
select="$budgetStatus"/>','N','<xsl:value-of 
select="$pageRight"/>','','','',1,'<xsl:value-of 
select="$budgetTemplate"/>')
            </xsl:attribute>
        VELLABEL[L_Add_Lineitem]<!-- Add New Lineitem --></a>
  </xsl:if>            
</TD>
</TR>

<xsl:variable name="str" select="key('RecordsByCategory', 
BGTSECTION_NAME)" />
<xsl:for-each select="key('RecordsByCategory', BGTSECTION_NAME)">
<xsl:variable name="vcost_custom">
	<xsl:value-of select="STANDARD_OF_CARE"/>
</xsl:variable>
<xsl:variable name="subCostItem_flag">
	<xsl:value-of select="SUBCOST_ITEM_FLAG"/>
</xsl:variable>
<xsl:variable name="class">
	<xsl:if test="$excludeSOC_flag='1' and $vcost_custom='Yes'">
		pastSchRow
	</xsl:if>            
	<xsl:if test="( $excludeSOC_flag='1' and $vcost_custom != 'Yes') 
or $excludeSOC_flag='0'">
		<xsl:choose>
		<xsl:when test="number(position() mod 2)=0" 
>reportEvenRow</xsl:when> 
		<xsl:otherwise>reportOddRow</xsl:otherwise>
		</xsl:choose> 
	</xsl:if>      
</xsl:variable>
 

<xsl:variable name="visitLine">
<xsl:value-of select="BUDGETSEC_FKVISIT"/>
</xsl:variable>

<xsl:variable name="allowSubmitLine">
<xsl:choose>
<xsl:when test="(($includedIn='P' and $visitLine = 0) or 
$includedIn='')">1</xsl:when> 
<xsl:otherwise>0</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<TR> 
<xsl:attribute name="class">
	<xsl:choose>
		<xsl:when 
test="($subCostItem_flag='1')">subCostRow</xsl:when>
		<xsl:when test="($subCostItem_flag='1' and 
$class='')">subCostRow</xsl:when>
		<xsl:when test="($class='')">reportOddRow</xsl:when> 
		<xsl:otherwise><xsl:value-of 
select="$class"/></xsl:otherwise>
	</xsl:choose> 
</xsl:attribute>
<xsl:variable name="lineitem"><xsl:value-of 
select="LINEITEM_NAME"/></xsl:variable>
<xsl:choose>
<xsl:when test="$mode='M'">
    <TD class="reportData" >
    	<xsl:attribute name="title"><xsl:value-of 
select="$lineitem"/></xsl:attribute>
	    <xsl:variable name="vlineitem_name_len">
			<xsl:value-of 
select="string-length(LINEITEM_NAME)"/>
		</xsl:variable>
		<xsl:variable name="vlineitem_name">
			<xsl:choose>
				<xsl:when test="($vlineitem_name_len &gt; 
30)"><xsl:value-of select="concat(substring(LINEITEM_NAME,1,30),' 
','...')"/></xsl:when> 
				<xsl:otherwise><xsl:value-of 
select="LINEITEM_NAME"/></xsl:otherwise>
			</xsl:choose> 
		</xsl:variable>
		<xsl:if test="($subCostItem_flag!='1')">
	    	<a>
	        <xsl:attribute 
name="href">javascript:void(0);</xsl:attribute>
			<xsl:attribute 
name="onClick">openLineItem(<xsl:value-of 
select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of 
select="PK_BUDGETSEC" />,'<xsl:value-of 
select="BGTSECTION_NAME"/>','<xsl:value-of 
select="$budgetStatus"/>','M','<xsl:value-of 
select="$pageRight"/>','<xsl:value-of 
select="PK_LINEITEM"/>','<xsl:value-of 
select="LINEITEM_NAME"/>',<xsl:value-of 
select="LINEITEM_INPERSEC"/>,<xsl:value-of 
select="$allowSubmitLine"/>,'<xsl:value-of 
select="$budgetTemplate"/>')</xsl:attribute>
	        <xsl:value-of select="$vlineitem_name" /></a>
	     </xsl:if>
	     <xsl:if test="($subCostItem_flag='1')">
	     	<xsl:value-of select="$vlineitem_name" />
	     </xsl:if>
    </TD>
</xsl:when> 
<xsl:otherwise>
    <TD class="reportData" >
    	<xsl:attribute name="title"><xsl:value-of 
select="$lineitem"/></xsl:attribute>
    	<xsl:value-of select="LINEITEM_NAME" />
    </TD>
</xsl:otherwise>    
</xsl:choose> 
           
<TD class="reportData" >
 
<xsl:value-of select="CATEGORY" /></TD>
<TD class="reportData" ><xsl:value-of select="COST_TYPE_DESC" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="UNIT_COST" 
/></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of 
select="NUMBER_OF_UNIT" /></TD>
<TD class="reportData" ALIGN="center"><xsl:value-of 
select="COST_DISCOUNT_ON_LINE_ITEM" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of 
select="format-number(LINEITEM_DIRECT_PERPAT,'##,###,###,###,###,##0.00')
" /></TD>
<TD class="reportData" ALIGN="center"><xsl:value-of 
select="LINE_ITEM_INDIRECTS_FLAG"/></TD>
<xsl:if test="$mode='V'">
   <TD class="reportData" ALIGN="right"><xsl:value-of 
select="format-number(PERPAT_INDIRECT,'##,###,###,###,###,##0.00')" 
/></TD>
</xsl:if>
<TD class="reportData" ALIGN="right"><xsl:value-of 
select="format-number(TOTAL_COST_PER_PAT,'##,###,###,###,###,##0.00')" 
/></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of 
select="format-number(TOTAL_COST_ALL_PAT,'##,###,###,###,###,##0.00')" 
/></TD>

<xsl:if test="$budgetTemplate='C'"> 
    <TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of 
select="SPONSOR_AMOUNT" /></TD>
    <TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of 
select="L_VARIANCE" /></TD>
</xsl:if>

</TR>

</xsl:for-each>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" 
class="reportGrouping">VELLABEL[L_Section_ResearchTotal]<!-- Section 
Research Total --> </td>


<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','No'))/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
   <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','No'))/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></td>

<xsl:if test="$budgetTemplate='C'">
    
     
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' 
','No'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','No'))/SPONSOR_AMOUNT) - ( 
sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' 
','No'))/TOTAL_COST_ALL_PAT)   ),'##,###,###,###,###,##0.00')"/></td>
</xsl:if>

</TR>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" 
class="reportGrouping">VELLABEL[L_Section_SocTotal]<!-- Section SOC Total 
--> </td>

<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Yes'))/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Yes'))/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></td>
 
<xsl:if test="$budgetTemplate='C'">

 
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' 
','Yes'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Yes'))/SPONSOR_AMOUNT) - ( 
sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' 
','Yes'))/TOTAL_COST_ALL_PAT)  ) ,'##,###,###,###,###,##0.00')"/></td>

</xsl:if>

</TR>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" 
class="reportGrouping">VELMESSGE[M_Section_OtherCostTotal]<!-- Section 
Other Cost Total --> </td>


<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Other'))/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
   <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Other'))/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></td>
 <xsl:if test="$budgetTemplate='C'">
 
     
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' 
','Other'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByCategorySOC', 
concat(BGTSECTION_NAME,' ','Other'))/SPONSOR_AMOUNT) - ( 
sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' 
','Other'))/TOTAL_COST_ALL_PAT)  ) ,'##,###,###,###,###,##0.00')"/></td>

</xsl:if>

</TR>


</xsl:for-each>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font 
color="red">VELLABEL[L_Grand_ResearchTotal]<!-- Grand Research Total --> 
</font></td>


<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'No')/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
   <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'No')/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">
    
    
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'No')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'No')/SPONSOR_AMOUNT) - ( 
sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT)  ) 
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font 
color="red">VELLABEL[L_Grand_SocTotal]<!-- Grand SOC Total --> 
</font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Yes')/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
   <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Yes')/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">

    
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Yes')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Yes')/SPONSOR_AMOUNT) - ( 
sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT)  )   
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font 
color="red">VELMESSGE[M_Grand_OtherCostTotal]<!-- Grand Other Cost Total 
--> </font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Other')/LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
   <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Other')/PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Other')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Other')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">

    
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 
'Other')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsBySOC', 'Other')/SPONSOR_AMOUNT) - 
( sum(key('RecordsBySOC', 'Other')/TOTAL_COST_ALL_PAT)  )   
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font 
color="red">VELLABEL[L_Grand_Total]<!-- Grand Total --> </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//LINEITEM_DIRECT_PERPAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<xsl:if test="$mode='V'">
  <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//PERPAT_INDIRECT) 
,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//TOTAL_COST_PER_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//TOTAL_COST_ALL_PAT) 
,'##,###,###,###,###,##0.00')"/></font></td>
 

<xsl:if test="$budgetTemplate='C'">

    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"
/></font></td>
    <td align="right" class="reportGrouping"><font 
color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//SPONSOR_AMOUNT) - ( sum(//TOTAL_COST_ALL_PAT) 
) ,'##,###,###,###,###,##0.00')"/></font></td>

</xsl:if>

</TR>
<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C' and 
$mode='V'">13</xsl:when>
		<xsl:when test="$budgetTemplate='C' and 
$mode='M'">12</xsl:when>
		<xsl:when test="$budgetTemplate!='C' and 
$mode='M'">10</xsl:when>
		<xsl:otherwise>11</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td><xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>&#xa0;</td>
</TR>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">9</xsl:when>
		<xsl:otherwise>7</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:for-each select="ROW[count(. | key('RecordsByPersonnel', 
CATEGORY_SUBTYP)[1])=1]">
<xsl:if test="CATEGORY_SUBTYP='ctgry_per'">
<TR style="background-color:white;" >
	<td><xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>&#xa0;</td>
	
	<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$mode='V'">2</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<td align="left" class="reportGrouping"> <xsl:attribute 
name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
	VELLABEL[L_Total_Salary]<!-- Total Salary --> </td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByPersonnel', 
CATEGORY_SUBTYP)/TOTAL_COST_PER_PAT),'##,###,###,###,###,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(key('RecordsByPersonnel', 
CATEGORY_SUBTYP)/TOTAL_COST_ALL_PAT),'##,###,###,###,###,##0.00')"/></td>
</TR>
</xsl:if>
</xsl:for-each>
<xsl:variable name="fringe_flag">
<xsl:value-of select="//FRINGE_FLAG"/>
</xsl:variable>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C' and 
$mode='V'">13</xsl:when>
		<xsl:when test="$budgetTemplate='C' and 
$mode='M'">12</xsl:when>
		<xsl:when test="$budgetTemplate!='C' and 
$mode='M'">10</xsl:when>
		<xsl:otherwise>11</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:if test="$mode='M'">
<TR style="background-color:white;" >
	<TD class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		<a>
			<xsl:attribute 
name="href">javascript:void(0);</xsl:attribute>
			<xsl:attribute 
name="onClick">openBudgetCalAttributes(<xsl:value-of 
select="$pkBgtCal"/>,
			<xsl:value-of select="$pkBudget"/>,<xsl:value-of 
select="$pageRight"/>,'<xsl:value-of 
select="$budgetStatus"/>')</xsl:attribute>
			VELLABEL[L_EditCalcu_Attributes]<!-- Edit 
Calculation Attributes -->
		</a>&#xa0;&#xa0;&#xa0;&#xa0;
	</TD>
</TR>
</xsl:if>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">9</xsl:when>
		<xsl:otherwise>7</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		VELLABEL[L_Fringe_BenefitOf]<!-- Fringe benefit of --> 
<xsl:value-of select="format-number(//FRINGE_BENEFIT,'##0.00')" />% 
		<xsl:choose>
			<xsl:when 
test="$fringe_flag='1'">VELLABEL[L_Applied]<!-- Applied --></xsl:when> 
			<xsl:otherwise>VELLABEL[L_Not_Applied]<!-- Not 
Applied --></xsl:otherwise>
		</xsl:choose> 
	 	VELMESSGE[M_ToAllLine_PersCat]<!-- to all lineitems with 
Personnel category -->
	</td>
<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$mode='V'">2</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
	<td class="reportGrouping"><xsl:attribute 
name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
	VELLABEL[L_Total_Fringe]<!-- Total Fringe --></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//PER_PATIENT_LINE_FRINGE),'##,###,###,###,###,
##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//TOTAL_LINE_FRINGE),'##,###,###,###,###,##0.00
')"/></td>
</TR>


<xsl:variable name="discount_flag">
<xsl:value-of select="//BUDGET_DISCOUNT_FLAG"/>
</xsl:variable>

<xsl:variable name="discMText">
	<xsl:choose>
		<xsl:when 
test="$discount_flag='Discount'">VELLABEL[L_Discount]<!-- Discount 
--></xsl:when> 
		<xsl:when 
test="$discount_flag='Markup'">VELLABEL[L_Markup]<!-- Markup 
--></xsl:when> 
		<xsl:otherwise>VELLABEL[L_DiscountOrMarkup]<!-- 
Discount/Markup --></xsl:otherwise>
	</xsl:choose> 
</xsl:variable>



<xsl:variable name="excludeSOC_flag_text">
	<xsl:choose>
		<xsl:when 
test="$excludeSOC_flag='1'">VELMESSGE[M_SocLineItems_ExcludedTot]<!-- SOC 
Lineitems excluded from the Totals --> </xsl:when> 
		<xsl:when 
test="$excludeSOC_flag='0'">VELMESSGE[M_SocItemsNot_ExcludedTot]<!-- SOC 
Lineitems not excluded from the Totals --> </xsl:when> 
		<xsl:otherwise>VELMESSGE[M_SocItemsNot_ExcludedTot]<!-- 
SOC Lineitems not excluded from the Totals --> </xsl:otherwise>
	</xsl:choose> 
</xsl:variable>


<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		VELLABEL[L_Cost]<!-- Cost --> <xsl:value-of 
select="$discMText" /> VELLABEL[L_Of_Lower]<!-- of --> <xsl:value-of 
select="format-number(//BUDGET_DISCOUNT,'##0.00')" />% 
		<xsl:choose>
			<xsl:when 
test="$discount_flag='Discount'">VELLABEL[L_Applied]<!-- Applied 
--></xsl:when> 
			<xsl:when 
test="$discount_flag='Markup'">VELLABEL[L_Applied]<!-- Applied 
--></xsl:when> 
			<xsl:otherwise>VELLABEL[L_Not_Applied]<!-- Not 
Applied --></xsl:otherwise>
		</xsl:choose> 
		 VELMESSGE[M_ToAll_SelectedLineItems]<!-- to all selected 
lineitems -->
	</td>
	<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$mode='V'">2</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<td class="reportGrouping"><xsl:attribute 
name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
	VELLABEL[L_Total_Cost]<!-- Total Cost --> <xsl:value-of 
select="$discMText" /></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//PER_PAT_LINE_ITEM_DISCOUNT),'##,###,###,###,#
##,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//TOTAL_COST_DISCOUNT),'##,###,###,###,###,##0.
00')"/></td>
</TR>


<xsl:variable name="indirect_flag">
<xsl:value-of select="//BUDGET_INDIRECT_FLAG"/>
</xsl:variable>


<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		VELLABEL[L_Indirects_Of]<!-- Indirects of --> 
<xsl:value-of select="format-number(//INDIRECTS,'##0.00')" />% 
		<xsl:choose>
			<xsl:when 
test="$indirect_flag='Y'">VELLABEL[L_Applied]<!-- Applied --></xsl:when> 
			<xsl:otherwise>VELLABEL[L_Not_Applied]<!-- Not 
Applied --></xsl:otherwise>
		</xsl:choose> 
		 VELMESSGE[M_ToSelect_TotalCostOrPat]<!-- to selected 
Total Cost/Patient]-->
	</td>
	<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$mode='V'">2</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<td class="reportGrouping"><xsl:attribute 
name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
	VELLABEL[L_Total_Indirects]<!-- Total Indirects --></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//PERPAT_INDIRECT),'##,###,###,###,###,##0.00')
"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of 
select="//BUDGET_CURRENCY"/><xsl:value-of 
select="format-number(sum(//TOTAL_COST_INDIRECT),'##,###,###,###,###,##0.
00')"/></td>
</TR>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C' and 
$mode='V'">13</xsl:when>
		<xsl:when test="$budgetTemplate='C' and 
$mode='M'">12</xsl:when>
		<xsl:when test="$budgetTemplate!='C' and 
$mode='M'">10</xsl:when>
		<xsl:otherwise>11</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td class="reportGrouping"> 
		<xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute>
		<xsl:value-of select="$excludeSOC_flag_text" />
	</td>
</TR>
<tr style="background-color:white;">
	<td><xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute><br/></td>
</tr>
<tr style="background-color:white;">
	<td><xsl:attribute name="colspan"><xsl:value-of 
select="$colsp"/></xsl:attribute><br/></td>
</tr>
</tbody>
</TABLE>
</div>
<hr class="thickLine" />
<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
        <TR>
        <TD class="reportFooter" WIDTH="50%" 
ALIGN="LEFT">VELLABEL[L_Last_ModifiedBy]<!-- Last Modified by -->: 
<xsl:value-of select="//LAST_MODIFIED_BY"/></TD>
        <TD class="reportFooter" WIDTH="50%" 
ALIGN="RIGHT">VELLABEL[L_Date_LastModified]<!-- Date Last Modified -->: 
<xsl:value-of select="//LAST_MODIFIED_DATE"/></TD>
        </TR>
        <TR>
        <TD class="reportFooter" WIDTH="50%" 
ALIGN="LEFT">VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of 
select="$repBy" /></TD>
        <TD class="reportFooter" WIDTH="50%" 
ALIGN="RIGHT">VELLABEL[L_Date]<!-- Date -->:<xsl:value-of 
select="$repDate" /></TD>
        </TR>
    </TABLE>
    <xsl:if test="$ftrflag='1'">
    <TABLE>
        <TR>
        <TD WIDTH="100%" ALIGN="CENTER">
        <img src="{$ftrFileName}"/>
        </TD>
        </TR>
    </TABLE>
    </xsl:if>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>