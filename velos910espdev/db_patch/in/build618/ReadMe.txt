/* This readMe is specific to Velos eResearch version 9.0 build #618 */

=====================================================================================================================================
Garuda :

	1.DCMS Configuration:-
	 
	JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/jsp/xml/dcmsAttachment.xml file has been modified according to the new DCMS encryption/decryption requirements.
	User need to update the dcmsAttachment.xml file with the current settings of DCMS <dcmsIpAddress> as per your environment. 
	By default this file points to 80.83.34.23:8080 and this server is deployed at BayaTree. 
	 
	2.DB Patch:-
	   i) DL04_ACCESS_RIGHT.sql and DL34_ACCESS_RIGHT.sql :- This SQL contains the CBU specific group access rights for widgets and static panels. These are specific to Garuda Project.
	   ii) Execute DL33_ER_CODELST_DELETE.sql before executing DL01_ER_CODELST_INSERT.sql
=====================================================================================================================================
eResearch:
         For enhancment INF-22237a-Reason-for-Delete :  Please use this sql to retrive the records from delete logs, this sql will bring the records from the different patient
         tables in the descending order. Run this query in different schemas to get the records from there.
         "select * from AUDIT_DELETELOG where table_name in ('ER_PATLABS','ER_PATFORMS','ER_PATPROT','ER_PATSTUDYSTAT','ER_PATTXARM','SCH_ADVERSEVE') order by PK_APP_DL desc;"        
         
=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. aboutus.jsp
2. forgotpwd.jsp
3. patstudystatusdelete.jsp
4. messageBundle.properties
5. labelBundle.properties
6. LC.java
7. LC.jsp
8. MC.java
9. MC.jsp
=====================================================================================================================================
