create or replace TRIGGER "ERES".ER_PATLABS_BI0 BEFORE INSERT ON ER_PATLABS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_PATLABS',erid, 'I',usr);
  INSERT_DATA:=:NEW.PK_PATLABS || '|' ||
to_char(:NEW.FK_PER) || '|' ||
to_char(:NEW.FK_TESTID) || '|' ||
to_char(:NEW.TEST_DATE) || '|' ||
to_char(:NEW.TEST_RESULT) || '|' ||
to_char(:NEW.FK_SITE) || '|' ||
to_char(:NEW.FK_TESTGROUP) || '|' ||
to_char(:NEW.FK_SPECIMEN) || '|' ||
to_char(:NEW.FK_TEST_OUTCOME) || '|' ||
to_char(:NEW.CMS_APPROVED_LAB) || '|' ||
to_char(:NEW.FDA_LICENSED_LAB) || '|' ||
to_char(:NEW.FK_TEST_REASON) || '|' ||
to_char(:NEW.FK_TEST_METHOD) || '|' ||
to_char(:NEW.FT_TEST_SOURCE) || '|' ||
to_char(:NEW.FK_TEST_SPECIMEN) || '|' ||
to_char(:NEW.FK_TIMING_OF_TEST) || '|' ||
to_char(:NEW.CUSTOM002) || '|' ||
to_char(:NEW.CUSTOM003) || '|' ||
to_char(:NEW.CUSTOM004) || '|' ||
to_char(:NEW.CUSTOM005) || '|' ||
to_char(:NEW.CUSTOM006) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.CUSTOM007);
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/