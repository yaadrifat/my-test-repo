set define off;
CREATE OR REPLACE TRIGGER "ERES"."ER_STUDY_AD0" 
AFTER DELETE
ON ER_STUDY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data CLOB;
  usr VARCHAR(2000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  /* commented by Sonia Abrol, 09/20/06
  audit_trail.record_transaction
    (raid, 'ER_STUDY', :OLD.rid, 'D'); */

	usr := Getuser(:OLD.last_modified_by);
	Audit_Trail.record_transaction(raid, 'ER_STUDY',:OLD.rid, 'D', usr);

  deleted_data :=   TO_CHAR(:OLD.pk_study) || '|' ||
  :OLD.study_sponsor || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  :OLD.study_contact || '|' ||
  :OLD.study_pubflag || '|' ||
  :OLD.study_title || '|' ||
  :OLD.study_prodname || '|' ||
  TO_CHAR(:OLD.study_samplsize) || '|' ||
  TO_CHAR(:OLD.study_dur) || '|' ||
  :OLD.study_durunit || '|' ||
  TO_CHAR(:OLD.study_estbegindt) || '|' ||
  TO_CHAR(:OLD.study_actualdt) || '|' ||
  :OLD.study_prinv || '|' ||
  :OLD.study_partcntr || '|' ||
  :OLD.study_keywrds || '|' ||
  :OLD.study_pubcollst || '|' ||
  TO_CHAR(:OLD.study_parentid) || '|' ||
  TO_CHAR(:OLD.fk_author) || '|' ||
  TO_CHAR(:OLD.fk_codelst_tarea) || '|' ||
  TO_CHAR(:OLD.fk_codelst_phase) || '|' ||
  TO_CHAR(:OLD.fk_codelst_blind) || '|' ||
  TO_CHAR(:OLD.fk_codelst_random) || '|' ||
    --added the two column by gopu for september enhancement #S7
  TO_CHAR(:OLD.fk_codelst_sponsor) || '|' ||
  TO_CHAR(:OLD.study_sponsorid) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  :OLD.study_ver_no || '|' ||
  :OLD.study_current || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.study_number || '|' ||
  TO_CHAR(:OLD.fk_codelst_restype) || '|' ||
  TO_CHAR(:OLD.fk_codelst_type) || '|' ||
  :OLD.study_info || '|' ||
  :OLD.ip_add || '|' || TO_CHAR(:OLD.study_end_date) || '|' || TO_CHAR(:OLD.STUDY_DIVISION) || '|' ||
  TO_CHAR(:OLD.STUDY_INVIND_FLAG) || '|' || :OLD.STUDY_INVIND_NUMBER || '|' ||
  TO_CHAR(:OLD.fk_codelst_sponsor) || '|' ||
  TO_CHAR(:OLD.study_sponsorid)|| '|' ||
  TO_CHAR(:OLD.STUDY_ADVLKP_VER)|| '|' ||
  :OLD.STUDY_CREATION_TYPE|| '|' ||
  -- YK DFIN20 Bug#6006
  :OLD.FK_CODELST_SET_MILESTATUS|| '|' ||
   -- Ankit CTRP-20527 24-Nov-2011
  TO_CHAR(:OLD.STUDY_CTRP_REPORTABLE)|| '|' ||
  -- Ankit INF-22247 27-Feb-2012
  TO_CHAR(:OLD.FDA_REGULATED_STUDY);



  --dbms_lob.SUBSTR(:OLD.study_obj_clob,4000) || '|' ||
  --dbms_lob.SUBSTR(:OLD.study_sum_clob,4000);



INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, SUBSTR(deleted_data,1,4000));
END;
/