set define off;

update er_object_settings set OBJECT_DISPLAYTEXT = 'Add Category'
where object_type ='M' and OBJECT_Name='callib_menu' and object_subtype ='newcal_type';

update er_object_settings set OBJECT_DISPLAYTEXT = 'Add Category'
where object_type ='M' and OBJECT_Name='formlib_menu' and object_subtype ='form_type';

commit;