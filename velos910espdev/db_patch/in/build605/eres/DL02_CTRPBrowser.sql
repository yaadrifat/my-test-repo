set define off;

DECLARE
  v_item_exists number := 0;  
  browser_Pk number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_BROWSER where browser_module = 'studydraftBrowser';
	if (v_item_exists = 0) then
		Insert into ER_BROWSER 
		values (SEQ_ER_BROWSER.nextval, 'studydraftBrowser','Study Draft Browser',null);
		dbms_output.put_line('Browser :studydraftBrowser inserted');
    
	else 
		dbms_output.put_line('Browser :studydraftBrowser already exists');
	end if;
  
SELECT COUNT(*) INTO v_item_exists FROM ER_BROWSERCONF where fk_browser IN (select pk_browser from ER_BROWSER where browser_module = 'studydraftBrowser');
if (v_item_exists = 0) then
    select pk_browser into browser_Pk from ER_BROWSER where browser_module = 'studydraftBrowser'; 
    select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='PK_STUDY';
    if (v_item_exists = 0) then
          INSERT
          INTO ER_BROWSERCONF VALUES
          (
          SEQ_ER_BROWSERCONF.nextval,
          browser_Pk,
          'PK_STUDY',
          1,
          '{"key":"PK_STUDY", "label":"","resizeable":true,"format":"studyCheckBox"}',
          ''
          );
     end if;
     select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STUDY_NUMBER';
     if (v_item_exists = 0) then   
       INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STUDY_NUMBER',
        2,
        '{"key":"STUDY_NUMBER", "label":"Study Number", "sortable":true, "resizeable":true,"format":"studypatLink"}',
        'Study Number'
        );
      end if;
      select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STUDY_TITLE';
      if (v_item_exists = 0) then   
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STUDY_TITLE',
        3,
        '{"key":"STUDY_TITLE", "label":"Study Title", "sortable":true, "resizeable":true,"hideable":true,"format":"titleLink"}',
        'Study Title'
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STATUS';
       if (v_item_exists = 0) then    
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STATUS',
        4,
        '{"key":"STATUS", "label":"Status", "sortable":true, "resizeable":true,"hideable":true,"format":"statusLink"}',
        'Status'
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='CTRP_DRAFT_TYPE';
       if (v_item_exists = 0) then  
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'CTRP_DRAFT_TYPE',
        5,
        '{"key":"CTRP_DRAFT_TYPE", "label":"Trail Submission Category", "sortable":true, "resizeable":true}',
        'Trail Submission Category'
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='FK_CODELST_STAT';
       if (v_item_exists = 0) then  
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'FK_CODELST_STAT',
        6,
        '{"key":"FK_CODELST_STAT", "label":"Draft Status", "sortable":true, "resizeable":true,"format":"draftStatusLink"}',
        'Draft Status'
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='SITE_NAME';
       if (v_item_exists = 0) then  
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'SITE_NAME',
        7,
        '',
        'Site Name'
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STUDY_TEAM_RIGHTS';
       if (v_item_exists = 0) then  
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STUDY_TEAM_RIGHTS',
        8,
        '',
        ''
        );
       end if;
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='CTRP_DRAFT_TYPE_STAT';
       if (v_item_exists = 0) then  
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'CTRP_DRAFT_TYPE_STAT',
        9,  
        '',
        ''
        );
       end if; 
       select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='FK_CODELST_STAT_SUBTYPE';
       if (v_item_exists = 0) then   
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'FK_CODELST_STAT_SUBTYPE',
        10,  
        '',
        ''
        );
       end if;
  dbms_output.put_line('Browser :studydraftBrowser EXITS');
    
	else 
		dbms_output.put_line('Browser :studydraftBrowser DOES NOT exists');
	end if;
  
COMMIT;

end;
/
