create or replace
PROCEDURE              "SP_WORKFLOW" (entity_id varchar,entity_type varchar,userid number,straction out varchar ) as

 TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
 cur_cv   CurTyp;  -- declare cursor variable

--- pk_cord = (select ORDER_ENTITYID from ER_order where PK_ORDER=#keycolumn) - >429 - workflow_Activity

sql_fieldval varchar2(100);

sql_fetch varchar2(4000);
sql_condition varchar2(4000);
sql_keycondition varchar2(4000);

sql_finalsql varchar2(4000);

-- tmp variables
tmp_workflowinstanceid number;
tmp_rescount number;
tmpseq number;
tmp_wakeycolumn varchar2(100);
tmp_terminateflag number;
tmp_fkuserorroleid number;
tmp_wauserorroletype varchar2(500);
tmp_wadisplayquery varchar2(4000);

-- variables for activity condition
varlogicflag number;
varsameseqflag number;
varlogicseq varchar2(1000);
varsameseq varchar2(1000);

tmp_fkworkflowactivity number;
tmp_workflowid number;

tmp_keycolumn varchar2(1000);

tmp_entityid varchar2(1000);
tmp_strsucessaction varchar2(1000);
tmp_strdefaction varchar2(1000);
BEGIN
varlogicseq := ',';
varsameseq := ',';
varlogicflag := 0;
varsameseqflag:= 0;
sql_condition := '';
tmpseq:=0;
straction := null;
-- update task type
update task set task_userorroleid = userid , task_userorroletype = 'USER' where fk_entity = entity_id and task_startflag = 1 and task_completeflag = 0 and deletedflag = 0;

-- single workflow change to multiple

dbms_output.put_line(' 11111111111  ' );

 select max(pk_workflow) into tmp_workflowid from workflow where upper(workflow_type) = upper(entity_type);
   dbms_output.put_line(' 1-- ' || tmp_workflowid );
-- fetch the active workflow instance id [one entity have only one workflow]



 select max(pk_wfinstanceid) into tmp_workflowinstanceid from workflow_instance where fk_entityid = entity_id and fk_workflowid = tmp_workflowid and wi_statusflag = 0;
 dbms_output.put_line(' 2-- ' || tmp_workflowinstanceid );

 -- check the running workflowinstance
if(tmp_workflowinstanceid is null) then -- workflow not init
        dbms_output.put_line(' inside if-- new instance ' );
     --   INSERT INTO TO_DEBUG VALUES ('inside if-- new instance');
        -- start the workflow
        tmp_workflowinstanceid := 0;
         insert into workflow_instance(pk_wfinstanceid,fk_entityid,fk_workflowid,wi_statusflag) values  (sq_workflowinstance.NEXTVAL,entity_id,tmp_workflowid,0);
           select sq_workflowinstance.CURRVAL into tmp_workflowinstanceid from dual;
           COMMIT;
         dbms_output.put_line( tmp_workflowinstanceid );
         tmpseq := 0;
dbms_output.put_line( 'After insert tmp_workflowid ' || tmp_workflowinstanceid || ' -  ' || entity_id || ' - ' || tmp_workflowid  );

--INSERT INTO TO_DEBUG VALUES ('After insert tmp_workflowid ' || tmp_workflowinstanceid || ' -  ' || entity_id || ' - ' || tmp_workflowid);
 -- 2  check logical conditions
   for logicalcheck in (select wa_sequence,COUNT(wa_sequence) seqcount
             from workflow_activity wa,wfactivity_condition wac
             where pk_workflowactivity=fk_workflowactivity  and upper(wac.ea_type) = 'PRE' and wa.fk_workflowid = tmp_workflowid
             and wa.deletedflag=0 and wac.deletedflag = 0 and upper(wac.ea_logicaloperator) in('OR','AND')
             group by wa_sequence order by wa_sequence)
   loop
             dbms_output.put_line( ' logical condition ' || logicalcheck.wa_sequence || ' --- ' || logicalcheck.seqcount);
             varlogicseq := varlogicseq ||  logicalcheck.wa_sequence || ',';
   end loop;

 dbms_output.put_line( '  varlogicseq ' ||  varlogicseq);
 -- more than one has same seq
   for samecheck in (select wa_sequence,COUNT(wa_sequence) seqcount
             from workflow_activity wa,wfactivity_condition wac
             where pk_workflowactivity=fk_workflowactivity  and upper(wac.ea_type) = 'PRE' and wa.fk_workflowid = tmp_workflowid
             and wa.deletedflag=0 and wac.deletedflag = 0 and wac.ea_logicaloperator is null
             group by wa_sequence order by wa_sequence)
        loop
             if( samecheck.seqcount > 1 ) then
              varsameseq := varsameseq ||  samecheck.wa_sequence || ',';
             end if;
   end loop;
 dbms_output.put_line( '  varsameseq ' ||  varsameseq);
--INSERT INTO TO_DEBUG VALUES ('  varsameseq ' ||  varsameseq);

  for activities in (select pk_workflowactivity,wa.wa_name,wa.fk_workflowid,wa_sequence,wa_terminateflag,wa.wa_keytable,wac.EA_KEYCONDITION,
             wa.wa_displayquery,wa.fk_userorroleid,wa.wa_userorroletype,
             wac.ea_type,wac.ea_startbrace,wac.ea_tablename,wac.ea_columnname,wac.ea_value,wac.ea_arithmeticoperator,wac.ea_logicaloperator,
             wac.ea_endbrace,wa.wa_sucessforwardaction,wa.wa_defaultforwardaction
             from workflow_activity wa,wfactivity_condition wac
             where pk_workflowactivity=fk_workflowactivity  and upper(wac.ea_type) = 'PRE' and wa.fk_workflowid = tmp_workflowid and wa.deletedflag=0 and wac.deletedflag =0
             order by wa_sequence,wac.pk_wfacondition)
       loop
   

            dbms_output.put_line( '  inside pre condition --->' || activities.pk_workflowactivity  );
            varlogicflag:= instr(varlogicseq,','||activities.wa_sequence  ||',');
            varsameseqflag := instr(varsameseq,','||activities.wa_sequence  ||',');
           dbms_output.put_line (' display sql  ' || activities.wa_sucessforwardaction );
               tmp_fkworkflowactivity:= activities.pk_workflowactivity;
  tmp_wakeycolumn := activities.EA_KEYCONDITION;
  tmp_fkuserorroleid := activities.fk_userorroleid;
  tmp_wauserorroletype := activities.wa_userorroletype;
  tmp_wadisplayquery:= populatesql(activities.wa_displayquery,entity_id);
  tmp_strsucessaction := activities.wa_sucessforwardaction;
  tmp_strdefaction := activities.wa_defaultforwardaction;
  dbms_output.put_line(' tmp_strsucessaction = ' || tmp_strsucessaction || '  tmp_strdefaction = ' || tmp_strdefaction);
            -- sequence must start with 1
                      
   dbms_output.put_line (' if --> ' || tmpseq || activities.wa_sequence || varsameseqflag);
  -- INSERT INTO TO_DEBUG VALUES (' if --> ' || tmpseq || activities.wa_sequence || varsameseqflag);
            if( tmpseq != activities.wa_sequence or varsameseqflag > 0) then
 dbms_output.put_line ('trace 1 ');
      sql_fetch := 'select count(' || activities.ea_columnname || ') from ' || activities.ea_tablename || ' where  ' ;

                sql_keycondition:= keyconditionstr(activities.EA_KEYCONDITION,entity_id);
                dbms_output.put_line('sql_keycondition ************* ' || sql_keycondition );
                         dbms_output.put_line('varlogicflag ' || varlogicflag );
                         --INSERT INTO TO_DEBUG VALUES ('sql_keycondition ************* ' || sql_keycondition);
            if(varlogicflag >0) then
                 -- logical condition
                 sql_condition := sql_condition || '  ' || activities.ea_startbrace || activities.ea_columnname ||' ' || activities.ea_arithmeticoperator ||' (' || activities.ea_value ||')' || activities.ea_endbrace || ' ' || activities.ea_logicaloperator ;
            else
                  sql_condition := activities.ea_columnname ||' ' || activities.ea_arithmeticoperator || ' (' || activities.ea_value ||')'  ;
            end if;
             dbms_output.put_line('sql_condition ************* ' || sql_condition );
             -- INSERT INTO TO_DEBUG VALUES ('sql_condition ************* ' || sql_condition);
                  if(length(sql_condition) >1 ) then
                       dbms_output.put_line (' display sql  ' || tmp_wadisplayquery );
                       OPEN cur_cv FOR  tmp_wadisplayquery;
                        LOOP
                          FETCH cur_cv INTO sql_fieldval ;
                          EXIT WHEN cur_cv%NOTFOUND;
                           dbms_output.put_line('display result  ' || sql_fieldval );
                      END LOOP;
                         sql_finalsql := sql_fetch || sql_condition || sql_keycondition;
                         dbms_output.put_line (' check status ' || sql_finalsql);
                       OPEN cur_cv FOR  sql_finalsql;

                       LOOP
                            FETCH cur_cv INTO tmp_rescount ;  -- fetch next row
                            EXIT WHEN cur_cv%NOTFOUND;  -- exit loop when last row is fetched
                              dbms_output.put_line ('pre condition count  ' || tmp_rescount );
                              -- INSERT INTO TO_DEBUG VALUES ('pre condition count  ' || tmp_rescount);
                               if(tmp_rescount > 0) then
                                  -- insert new task
--INSERT INTO TO_DEBUG VALUES ('BEFORE INSERT TASK ---------------------- ' ||tmp_fkworkflowactivity);
                                   insert into task (pk_taskid,fk_workflowactivity,fk_entity,fk_wfinstanceid,task_userorroleid,task_userorroletype,task_startflag,task_name,createdon,createdby)
                                                       values (sq_task.NEXTVAL,tmp_fkworkflowactivity,entity_id,tmp_workflowinstanceid,tmp_fkuserorroleid,tmp_wauserorroletype,1,sql_fieldval,sysdate,userid);
                                   sql_finalsql := '';
                                   sql_fetch :='';
                                   sql_condition:='';
                                   sql_keycondition :='';
                                   sql_fieldval := '';
                                   straction := tmp_strsucessaction;
                               end if;
                       END LOOP;

                   end if;
                 tmpseq:= activities.wa_sequence;
            end if;
           
  
        end loop;
-- INSERT INTO TO_DEBUG VALUES ('end pre condition loop : ' );
  dbms_output.put_line ('end pre condition loop : ' );
 -- insert the last condition 
  dbms_output.put_line (' pre condition '  || tmpseq || ' -sql_fetch: ' || sql_fetch ||' -sql_condition: ' || sql_condition);
  dbms_output.put_line ('sql_keycondition: ' || sql_keycondition);
 dbms_output.put_line('end of loop tmp_strsucessaction = ' || tmp_strsucessaction || '  tmp_strdefaction = ' || tmp_strdefaction);
 /*
         if(tmpseq != 0 ) then

             dbms_output.put_line ( 'tmp_wadisplayquery ' || tmp_wadisplayquery);
            if(tmp_wadisplayquery is not null) then
               OPEN cur_cv FOR  tmp_wadisplayquery;
                  LOOP
                      FETCH cur_cv INTO sql_fieldval ;
                      EXIT WHEN cur_cv%NOTFOUND;
                  dbms_output.put_line('display result  ' || sql_fieldval );
               END LOOP;
            end if;
             sql_finalsql := sql_fetch || sql_condition || sql_keycondition;
             dbms_output.put_line ( 'Final sql ' || sql_finalsql);

             OPEN cur_cv FOR  sql_finalsql;
                dbms_output.put_line (' result  for pre condition ' );
             LOOP
                  FETCH cur_cv INTO tmp_rescount ;  -- fetch next row
                  EXIT WHEN cur_cv%NOTFOUND;  -- exit loop when last row is fetched
                    dbms_output.put_line ('pre condition count  ' || tmp_rescount );
                     if(tmp_rescount > 0) then
                        -- insert new task
                         insert into task (pk_taskid,fk_workflowactivity,fk_entity,fk_wfinstanceid,task_userorroleid,task_userorroletype,task_startflag,task_name,createdon,createdby)
                                             values (sq_task.NEXTVAL,tmp_fkworkflowactivity,entity_id,tmp_workflowinstanceid,
                                             tmp_fkuserorroleid,tmp_wauserorroletype,1,sql_fieldval,sysdate,userid);
                         sql_finalsql := '';
                         sql_fetch :='';
                         sql_condition:='';
                         sql_keycondition :='';
                         straction:= tmp_strsucessaction;
                     else
                         if(straction = null ) then
                           straction:= tmp_strdefaction;
                         end if;
                     end if;
             END LOOP;
        end if;
      */  
    dbms_output.put_line('tmp_workflowinstanceid ' || tmp_workflowinstanceid );

--   select wa_defaultforwardaction into straction from workflow_activity where pk_workflowactivity in(
--     select min(fk_workflowactivity) from task where fk_entity = entity_id and fk_wfinstanceid = tmp_workflowinstanceid and task_startflag = 1 and deletedflag = 0 and task_completeflag =0);

    dbms_output.put_line('new instance straction ' || straction );
-- end of 2 (PRE Conditions).
-----------end of new instance

else
-----old instance
-- workflow already init
    dbms_output.put_line(' 22 inside else old instance ' || tmp_workflowinstanceid );
    -- select the pending taks
    for pendingtasks in (select pk_taskid,WA_SEQUENCE from task,WORKFLOW_ACTIVITY where PK_WORKFLOWACTIVITY = FK_WORKFLOWACTIVITY and fk_wfinstanceid = tmp_workflowinstanceid and task_completeflag = 0 and task_startflag = 1 order by pk_taskid)
    loop
         dbms_output.put_line(' pending taskid -->' || pendingtasks.pk_taskid );
         varlogicseq:= ',';
         tmpseq := pendingtasks.WA_SEQUENCE;
      for logicalcheck in (select wa_sequence,COUNT(wa_sequence) seqcount  from
          wfactivity_condition wfac , workflow_activity  wfa, workflow_instance wi,task t
              where wfac.deletedflag = 0
              and wfac.fk_workflowactivity = wfa.pk_workflowactivity
              and upper(wfac.ea_type) = 'POST'
              and wi.pk_wfinstanceid = t.fk_wfinstanceid
              and t.fk_workflowactivity = wfac.fk_workflowactivity
              and wfa.fk_workflowid = tmp_workflowid
              and upper(wfac.ea_logicaloperator) in('OR','AND')
              and t.pk_taskid = pendingtasks.pk_taskid
              group by wa_sequence order by wa_sequence)
          loop
             dbms_output.put_line( ' logical condition ' || logicalcheck.wa_sequence || ' --- ' || logicalcheck.seqcount);
             varlogicseq := varlogicseq ||  logicalcheck.wa_sequence || ',';
       end loop;

              dbms_output.put_line(' pending task varlogicseq ' || varlogicseq );

           sql_fetch := ''; -- clear
           sql_condition := '';
            -- check the conditions and update the tasks
           for activities in
                    ( select ea_type,ea_startbrace,ea_tablename,ea_columnname,ea_arithmeticoperator, ea_value , ea_endbrace,ea_logicaloperator ,wa_keytable,wfac.EA_KEYCONDITION,
                    wfac.pk_wfacondition,wfac.fk_workflowactivity,wfa.pk_workflowactivity,wfa.wa_sequence,wfa.wa_terminateflag,wi.fk_workflowid,t.pk_taskid,t.fk_entity,t.fk_wfinstanceid
                    from  wfactivity_condition wfac , workflow_activity  wfa, workflow_instance wi,task t
                    where wfac.deletedflag = 0
                    and wfac.fk_workflowactivity = wfa.pk_workflowactivity
                    and upper(wfac.ea_type) = 'POST'
                    and wi.pk_wfinstanceid = t.fk_wfinstanceid
                    and t.fk_workflowactivity = wfac.fk_workflowactivity
                    and t.pk_taskid = pendingtasks.pk_taskid
                    order by pk_wfacondition,ea_type,wa_sequence,wfac.pk_wfacondition)
           loop

tmp_fkworkflowactivity:= activities.fk_workflowactivity;
--tmp_wakeycolumn := activities.EA_KEYCONDITION;
tmp_terminateflag := activities.wa_terminateflag;
          sql_fieldval := null;
          dbms_output.put_line (' activities.ea_value ' || activities.ea_value);
              if( activities.ea_value is not null and activities.ea_value != 'null' ) then  
                 dbms_output.put_line ('trace 1====================================');
                OPEN cur_cv FOR  activities.ea_value ;
                    dbms_output.put_line (' result  ' );
                LOOP
                      FETCH cur_cv INTO sql_fieldval ;  -- fetch next row
                      EXIT WHEN cur_cv%NOTFOUND;  -- exit loop when last row is fetched
                        dbms_output.put_line (' SQL result  ' || sql_fieldval );
                END LOOP; -- end of cursor
              end if;  
                 dbms_output.put_line (' activities.wa_sequencet  ' || activities.wa_sequence );
                        sql_fetch := 'select count(' || activities.ea_columnname || ') from ' || activities.ea_tablename || ' where  ' ;
                        sql_keycondition:= keyconditionstr(activities.EA_KEYCONDITION, entity_id);
                        varlogicflag:= instr(varlogicseq,','||activities.wa_sequence  ||',');
                        if ( varlogicflag > 0) then
                             dbms_output.put_line( ' logical condition ');
                            sql_condition := sql_condition || '  ' || activities.ea_startbrace || activities.ea_columnname ||' ' || activities.ea_arithmeticoperator || ' ' || sql_fieldval || activities.ea_endbrace || ' ' || activities.ea_logicaloperator ;
                        else
                            sql_condition := activities.ea_columnname ||' ' || activities.ea_arithmeticoperator || ' ' || sql_fieldval ;
                        end if;
              tmpseq:= activities.wa_sequence;
            end loop; -- end of activities
                 sql_finalsql := sql_fetch || sql_condition || sql_keycondition;
                    dbms_output.put_line( pendingtasks.pk_taskid || ' sql post condition for task ------------- '  || sql_finalsql);
               if(length(sql_finalsql) >1 ) then
                    -- check the post condition status
                   OPEN cur_cv FOR  sql_finalsql;
                      dbms_output.put_line (' result  for post condition ' );
                   LOOP
                        FETCH cur_cv INTO tmp_rescount ;  -- fetch next row
                        EXIT WHEN cur_cv%NOTFOUND;  -- exit loop when last row is fetched
                          dbms_output.put_line ('post condition count  ' || tmp_rescount );
                           if(tmp_rescount > 0) then
                             dbms_output.put_line (' inside if  ' || pendingtasks.pk_taskid );
                           -- update task
                             update task set task_completeflag = 1,completedon = sysdate where pk_taskid in (pendingtasks.pk_taskid);
                              dbms_output.put_line (' **********end of update task complete  seq******************* ' || tmpseq || ' ----taskid----------' || pendingtasks.pk_taskid);
                              -- update terminate flag
                              if(tmp_terminateflag =1) then
                               dbms_output.put_line (' end of update workflow instance ' || tmp_workflowinstanceid );
                                update workflow_instance set wi_statusflag = 1 where pk_wfinstanceid = tmp_workflowinstanceid;
                              end if;
                              select wfa.wa_sucessforwardaction into straction from workflow_activity wfa where pk_workflowactivity = tmp_fkworkflowactivity;
                              dbms_output.put_line (tmp_fkworkflowactivity || ' sucess action ' || straction );
                             -- next activities pre conditions
                           else
                              select wfa.wa_defaultforwardaction into straction from workflow_activity wfa where pk_workflowactivity = tmp_fkworkflowactivity;
                              dbms_output.put_line (tmp_fkworkflowactivity || ' sucess action ' || straction );

                           end if;
                    END LOOP; -- END OF POST CONDITION STATUS
               end if;

    end loop; -- end of pending tasks



--- end of pending tasks
end if;
 dbms_output.put_line(' end 2-- ' || tmp_workflowinstanceid );
 --========end of 1

----------- next activity not and / or conditions
dbms_output.put_line(' **************************next activity ************************** '  );
for nextactivities in ( select pk_wfacondition,ea_type,ea_startbrace,ea_tablename,ea_columnname,ea_value,
                                  ea_arithmeticoperator,ea_logicaloperator,ea_endbrace,wa.fk_userorroleid,wa.wa_userorroletype,wa.pk_workflowactivity,wa.wa_keycolumn,wa.wa_displayquery
                                  ,wa.wa_defaultforwardaction
                                  from wfactivity_condition wfac, workflow_activity wa
                                  where wfac.fk_workflowactivity = wa.pk_workflowactivity and fk_workflowid = tmp_workflowid
                                  and wa.wa_sequence > tmpseq  and upper(ea_type)= 'PRE'
                                )
                              loop
                                   dbms_output.put_line(' next Activity '  );
                                    tmp_entityid := entity_id;
                                    tmp_keycolumn := nextactivities.wa_keycolumn;
                                    sql_keycondition:= keyconditionstr(nextactivities.wa_keycolumn ,entity_id);

                                    sql_fetch := 'select count(' || nextactivities.ea_columnname || ') from ' || nextactivities.ea_tablename || ' where  ' || nextactivities.ea_columnname
                                          ||' ' || nextactivities.ea_arithmeticoperator || '( ' || nextactivities.ea_value  || ') ' || sql_keycondition;

                                   dbms_output.put_line(' new sql count ------------- ' || sql_fetch);
                               /*    if(length(straction) <1) then
                                       straction:= nextactivities.wa_defaultforwardaction;
                                   end if;
                                */   EXECUTE IMMEDIATE sql_fetch;
                                       OPEN cur_cv FOR  sql_fetch;
                                          dbms_output.put_line (' result  ``````````` ' );
                                       LOOP
                                            FETCH cur_cv INTO tmp_rescount ;
                                            EXIT WHEN cur_cv%NOTFOUND;
                                                dbms_output.put_line ( 'count ' || tmp_rescount );
                                                if (tmp_rescount > 0 ) then
                                                  dbms_output.put_line('Pre condition statisfied ');
                                              -- insert the task into task table
                                                   sql_fetch := populatesql(nextactivities.wa_displayquery,entity_id);
                                                    dbms_output.put_line('display sql ' || sql_fetch);
                                                     OPEN cur_cv FOR  sql_fetch;
                                                     LOOP
                                                        FETCH cur_cv INTO sql_fieldval ;  -- fetch next row
                                                        EXIT WHEN cur_cv%NOTFOUND;  -- exit loop when last row is fetched
                                                         dbms_output.put_line('display result  ' || sql_fieldval );
                                                    END LOOP;
                                                   insert into task (pk_taskid,fk_workflowactivity,fk_entity,fk_wfinstanceid,task_userorroleid,task_userorroletype,task_startflag,task_name,createdon,createdby)
                                                   values (sq_task.NEXTVAL,nextactivities.pk_workflowactivity,entity_id,tmp_workflowinstanceid,nextactivities.fk_userorroleid,nextactivities.wa_userorroletype,1,sql_fieldval,sysdate,userid);

                                                  dbms_output.put_line(' End of Insert ' );
                                                else
                                                  dbms_output.put_line('Pre condition not statisfied else');
                                                end if;
                                      END LOOP;

                           end loop; -- end of next activities


 dbms_output.put_line(' Action ' || straction );

straction := populatesql(straction,entity_id);



dbms_output.put_line(' 44444444444 ' );

End sp_workflow;

/