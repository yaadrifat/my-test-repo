set define off;

DECLARE
 col_count NUMBER;
BEGIN
 select count(*) into col_count from ER_OBJECT_SETTINGS where object_name='protocol_tab' and OBJECT_DISPLAYTEXT IN ('Calendar Milestone Setup','Manage Budget','Manage Milestones');
 IF col_count = 3 THEN
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 7 WHERE OBJECT_NAME='protocol_tab' AND OBJECT_DISPLAYTEXT = 'Calendar Milestone Setup';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 8 WHERE OBJECT_NAME='protocol_tab' AND OBJECT_DISPLAYTEXT = 'Manage Budget';
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = 9 WHERE OBJECT_NAME='protocol_tab' AND OBJECT_DISPLAYTEXT = 'Manage Milestones';
  COMMIT;
 END IF;
END;
/