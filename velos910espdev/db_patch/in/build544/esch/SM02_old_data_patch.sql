--Old data patch for all combined budgets to be shared with study team

Update sch_budget set BUDGET_RIGHTSCOPE = 'S' where NVL(BUDGET_COMBFLAG,'Z') = 'Y' and NVL(BUDGET_DELFLAG, 'N') != 'Y';

commit;