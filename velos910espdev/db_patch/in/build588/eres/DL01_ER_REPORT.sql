set define off;

DECLARE
countFlag NUMBER(5);
BEGIN
	SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_REPORT';
	if (countFlag > 0) then
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Study Title, PI, Submission Type, Submission Status, Submission Status Date', 
		REP_FILTERBY = 'Study' WHERE REP_NAME='IRB Protocol History';
		
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Study Number, Study Title, PI, Expiration Date, Study Status, Review Type, Review Board Name, Approval Date', 
		REP_FILTERBY = 'Study' WHERE REP_NAME='IRB Continuing Review';
		
		UPDATE ER_REPORT SET 
		REP_COLUMNS = 'Organization, Patient ID, Patient Study ID, Calendar, Visit, Event, Scheduled Date, Status, Status Date, Planned Time, Actual Time', 
		REP_FILTERBY = 'Study' WHERE REP_NAME='Resource Time Tracking';
		commit;
	end if;
END;
/
