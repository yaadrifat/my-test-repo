For INV-18266 (INVP3.1)

To set the value of FK_ACCOUNT column in ER_LABEL_TEMPLATES table, 

you can select FK_ACCOUNT column from ER_USER table of specific user to see the new label templates in the UI.
Note :- If FK_ACCOUNT is NULL for any template then that template is available for all user.




Datepicker Implementation (INF-20084)


We have completed implementation of datepicker across the application. 

Here is the List of JSP's on which this feature is implemented.

1	specimenbrowser.jsp
2	editmultiplespecimenstatus.jsp
3	specimenstatus.jsp
4	specimendetails.jsp
5	preparationAreaBrowser.jsp
6	storagestatus.jsp
7	storageunitdetails.jsp
8	editmultiplestoragestatus.jsp
9	labmodify.jsp
10	getFormSpecimenData.jsp
11	labdata.jsp
12	invoice_step1.jsp
13	editstatus.jsp
14	viewInvoice.jsp
15	milepayments.jsp
16	patstudystatus.jsp
17	pattrtarm.jsp
18	advevent_new.jsp
19	addmultipleadvevent.jsp
20	patientprotocol.jsp
21	editMulEventDetails.jsp
22	getAdvToxicity.jsp
23	studycentricpatenroll.jsp
24	addNewQuery.jsp
25	crfstatus.jsp
26	viewexpstatus.jsp
27	viewimpstatus.jsp
28	studystatus.jsp
29	editStudyTeamStatus.jsp
30	updatemultschedules.jsp
31	calendarstatus.jsp
32	formStatus.jsp
33	appendix_file_multi.jsp
34	studyver.jsp
35	studyschedule.jsp
36	study.jsp
37	selectDateRange.jsp
38	rep_activityreport.jsp
39	editPortalStatus.jsp
40	mdynfilter.jsp
41	reportcentral.jsp
42	protocolcalendar.jsp
43	MarkDone.jsp
44	patientFacility.jsp
45	perapndxfile.jsp
46	addperurl.jsp
47	patientdetails.jsp
48	irbactionwin.jsp
49	wsPatientSearch.jsp
50	morePerDetails.jsp
51	provisoDetails.jsp
52	adminauditreports.jsp
53	discdetails.jsp
54	accountbrowser.jsp
55	changedate.jsp
56	selectDateRangeGeneric.jsp
57	discontinuation.jsp
58	advevent.jsp
59	studyapndx.xsl
60	allpatientdetails.jsp
61	editStudyMulEventDetails.jsp
62	patientschedule.jsp
63	patientdetailsquick.jsp
64	changestudydates.jsp
