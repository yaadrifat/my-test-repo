CREATE OR REPLACE FORCE VIEW ERV_CALENDAR_TEMPLATE ("PROT_ID","PROT_NAME",
    "PROT_DESC","COVERAGE_TYPE","PROT_DURATION","VISIT_NAME",
    "VISIT_DESC","VISIT_INTERVAL","EVENT_NAME","EVENT_DESC",
    "EVENT_DURATION","VWINDOW_BEF","VWINDOW_AFT",
    "FUZZY_DURATION_BEF","FUZZY_DURATION_AFT","SCH_DATE",
    "SCH_DATE_GRP","SCH_DATE_GRP_DISP","EVENT_COST","RESOURCES",
    "APPENDIX","MESSAGES","FORMS","DISPLACEMENT","INSERT_AFTER",
    "EVENT_SEQUENCE","EVENT_TYPE","HIDE_FLAG") AS 
    SELECT   x.PROTOCOL AS prot_id, 
            a.NAME AS prot_name, 
            a.description AS prot_desc, 
	    x.coverage_type as coverage_type , 
	    DECODE (duration_unit, 
                    'D', 
                    duration, 
                    'W', 
                    duration / 7, 
                    'M', 
                    duration / 30, 
                    'Y', 
                    duration / 365) 
            || DECODE (duration_unit, 
                       'D', 
                       ' Days', 
                       'W', 
                       ' Weeks', 
                       'M', 
                       ' Months', 
                       'Y', 
                       ' Years') 
               AS prot_duration, 
            x.VISIT_NAME, 
            x.DESCRIPTION AS visit_desc, 
            x.INTERVAL 
            || DECODE (x.INTERVAL_after, 
                       NULL, '', 
                       x.INTERVAL_after || ' After') 
               AS visit_interval, 
            x.NAME AS event_name, 
            x.event_desc, 
            x.event_duration, 
            x.vwindow_bef, 
            x.vwindow_aft, 
            x.fuzzy_duration_bef, 
            x.fuzzy_duration_aft, 
            x.SCH_DATE, 
            TO_CHAR (x.SCH_DATE, 'yyyymm') AS sch_date_grp, 
            TO_CHAR (x.SCH_DATE, 'Month yyyy') AS sch_date_grp_disp, 
            x.COST AS event_cost, 
            x.RESOURCES, 
            x.APPENDIX, 
            x.MESSAGES, 
            x.FORMS, 
            x.displacement, 
            x.insert_after, 
            x.event_sequence, 
            x.event_type, 
            x.hide_flag 
     FROM   EVENT_DEF a,  
            (SELECT   chain_id AS protocol, 
	              c.codelst_desc as coverage_type , 
	              DECODE (fuzzy_period, 
                              '0', '', 
                                 fuzzy_period 
                              || ' ' 
                              || DECODE (event_durationbefore, 
                                         'H', 
                                         'Hours', 
                                         'D', 
                                         'Days', 
                                         'W', 
                                         'Weeks', 
                                         'M', 
                                         'Months') 
                              || '(BEFORE)') 
                         AS vwindow_bef, 
                      -- Query modified by Gopu to fix the bugzilla issue #2625 
                      DECODE (TRIM (event_fuzzyafter), 
                              '', '', 
                              NULL, '', 
                              0, '', 
                                 event_fuzzyafter 
                              || ' ' 
                              || DECODE (event_durationafter, 
                                         'H', 
                                         'Hours', 
                                         'D', 
                                         'Days', 
                                         'W', 
                                         'Weeks', 
                                         'M', 
                                         'Months') 
                              || '(AFTER)') 
                         AS vwindow_aft, 
                      DECODE (event_durationbefore, 
                              'D', 
                              (fuzzy_period * 1), 
                              'H', 
                              1, 
                              'W', 
                              (fuzzy_period * 7), 
                              'M', 
                              (fuzzy_period * 30)) 
                         AS fuzzy_duration_bef, 
                      DECODE (event_durationafter, 
                              'D', 
                              (event_fuzzyafter * 1), 
                              'H', 
                              1, 
                              'W', 
                              (event_fuzzyafter * 7), 
                              'M', 
                              (event_fuzzyafter * 30)) 
                         AS fuzzy_duration_aft, 
                      DECODE (duration, 
                              0, '', 
                              duration 
                              || DECODE (duration_unit, 
                                         'D', 
                                         ' Days', 
                                         'W', 
                                         'Weeks')) 
                         AS event_duration, 
                      visit_name, 
                      a.description, 
                      DECODE (num_months, 
                              0, '', 
                              'Month ' || num_months || ', ') 
                      || DECODE (num_weeks, 
                                 0, '', 
                                 'Week ' || num_weeks || ', ') 
                      || DECODE (num_days, 0, '', 'Day ' || num_days) 
                         AS INTERVAL, 
                      DECODE (insert_after_interval, 
                              0, '', 
                                 insert_after_interval 
                              || ' ' 
                              || DECODE (Insert_after_interval_unit, 
                                         'D', 
                                         'Day(s)', 
                                         'W', 
                                         'Week(s)', 
                                         'M', 
                                         'Month(s)', 
                                         'Y', 
                                         'Year(s)')) 
                         AS INTERVAL_after, 
                       pkg_dateutil.f_get_first_dayofyear () 
                      + a.displacement 
                      - 1 
                         AS sch_date, 
                      NAME, 
                      b.description AS event_desc, 
                      lst_cost (event_id) AS COST, 
                      lst_usr (event_id) AS resources, 
                      lst_doc (event_id) AS appendix, 
                      lst_mail (event_id) AS messages, 
                      lst_forms (event_id) AS forms, 
                      a.displacement, 
                      (SELECT   visit_name 
                         FROM   SCH_PROTOCOL_VISIT 
                        WHERE   pk_protocol_visit = a.insert_after) 
                         AS insert_after, 
                      event_sequence, 
                      event_type, 
                      0 AS hide_flag 
               FROM   SCH_PROTOCOL_VISIT a, EVENT_DEF b , sch_codelst c 
              WHERE   pk_protocol_visit = fk_visit and  
            b.FK_CODELST_COVERTYPE  = c.pk_codelst (+) ) x 
    WHERE   protocol = event_id  
   UNION 
   SELECT   x.PROTOCOL AS prot_id, 
            a.NAME AS prot_name, 
            a.description AS prot_desc, 
	    x.coverage_type as coverage_type , 
            DECODE (duration_unit, 
                    'D', 
                    duration, 
                    'W', 
                    duration / 7, 
                    'M', 
                    duration / 30, 
                    'Y', 
                    duration / 365) 
            || DECODE (duration_unit, 
                       'D', 
                       ' Days', 
                       'W', 
                       ' Weeks', 
                       'M', 
                       ' Months', 
                       'Y', 
                       ' Years') 
               AS prot_duration, 
            x.VISIT_NAME, 
            x.DESCRIPTION AS visit_desc, 
            x.INTERVAL 
            || DECODE (x.INTERVAL_after, 
                       NULL, '', 
                       x.INTERVAL_after || ' After') 
               AS visit_interval, 
            x.NAME AS event_name, 
            x.event_desc, 
            x.event_duration, 
            x.vwindow_bef, 
            x.vwindow_aft, 
            x.fuzzy_duration_bef, 
            x.fuzzy_duration_aft, 
            x.SCH_DATE, 
            TO_CHAR (x.SCH_DATE, 'yyyymm') AS sch_date_grp, 
            TO_CHAR (x.SCH_DATE, 'Month yyyy') AS sch_date_grp_disp, 
            x.COST AS event_cost, 
            x.RESOURCES, 
            x.APPENDIX, 
            x.MESSAGES, 
            x.FORMS, 
            x.displacement, 
            x.insert_after, 
            x.event_sequence, 
            x.event_type, 
            x.hide_flag 
     FROM   EVENT_ASSOC a,  
            (SELECT   chain_id AS protocol, 
	              c.codelst_desc as coverage_type ,  
                      DECODE (fuzzy_period, 
                              '0', '', 
                                 fuzzy_period 
                              || ' ' 
                              || DECODE (event_durationbefore, 
                                         'H', 
                                         'Hours', 
                                         'D', 
                                         'Days', 
                                         'W', 
                                         'Weeks', 
                                         'M', 
                                         'Months') 
                              || '(BEFORE)') 
                         AS vwindow_bef, 
                      -- Query modified by Gopu to fix the bugzilla issue #2625 
                      DECODE (TRIM (event_fuzzyafter), 
                              '', '', 
                              NULL, '', 
                              0, '', 
                                 event_fuzzyafter 
                              || ' ' 
                              || DECODE (event_durationafter, 
                                         'H', 
                                         'Hours', 
                                         'D', 
                                         'Days', 
                                         'W', 
                                         'Weeks', 
                                         'M', 
                                         'Months') 
                              || '(AFTER)') 
                         AS vwindow_aft, 
                      DECODE (event_durationbefore, 
                              'D', 
                              (fuzzy_period * 1), 
                              'H', 
                              1, 
                              'W', 
                              (fuzzy_period * 7), 
                              'M', 
                              (fuzzy_period * 30)) 
                         AS fuzzy_duration_bef, 
                      DECODE (event_durationafter, 
                              'D', 
                              (event_fuzzyafter * 1), 
                              'H', 
                              1, 
                              'W', 
                              (event_fuzzyafter * 7), 
                              'M', 
                              (event_fuzzyafter * 30)) 
                         AS fuzzy_duration_aft, 
                      DECODE (duration, 
                              0, '', 
                              duration 
                              || DECODE (duration_unit, 
                                         'D', 
                                         ' Days', 
                                         'W', 
                                         'Weeks')) 
                         AS event_duration, 
                      visit_name, 
                      a.description, 
                      DECODE (num_months, 
                              0, '', 
                              'Month ' || num_months || ', ') 
                      || DECODE (num_weeks, 
                                 0, '', 
                                 'Week ' || num_weeks || ', ') 
                      || DECODE (num_days, 0, '', 'Day ' || num_days) 
                         AS INTERVAL, 
                      DECODE (insert_after_interval, 
                              0, '', 
                                 insert_after_interval 
                              || ' ' 
                              || DECODE (Insert_after_interval_unit, 
                                         'D', 
                                         'Day(s)', 
                                         'W', 
                                         'Week(s)', 
                                         'M', 
                                         'Month(s)', 
                                         'Y', 
                                         'Year(s)')) 
                         AS INTERVAL_after, 
                        pkg_dateutil.f_get_first_dayofyear () 
                      + a.displacement 
                      - 1 
                         AS sch_date, 
                      NAME, 
                      b.description AS event_desc, 
                      lst_cost (event_id) AS COST, 
                      lst_usr (event_id) AS resources, 
                      lst_doc (event_id) AS appendix, 
                      lst_mail (event_id) AS messages, 
                      lst_forms (event_id) AS forms, 
                      a.displacement, 
                      (SELECT   visit_name 
                         FROM   SCH_PROTOCOL_VISIT 
                        WHERE   pk_protocol_visit = a.insert_after) 
                         AS insert_after, 
                      event_sequence, 
                      event_type, 
                      b.hide_flag 
               FROM   SCH_PROTOCOL_VISIT a, EVENT_ASSOC b , sch_codelst c 
              WHERE   pk_protocol_visit = fk_visit  and 
            b.FK_CODELST_COVERTYPE  = c.pk_codelst (+) ) x 
    WHERE   protocol = event_id;
/

commit;


