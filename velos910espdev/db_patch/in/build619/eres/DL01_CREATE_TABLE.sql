set define off;

CREATE TABLE ER_ORDER_SERVICES(
	PK_ORDER_SERVICES NUMBER(10,0),
	FK_ORDER_ID NUMBER(10,0),
	SERVICE_CODE VARCHAR2(500 BYTE),
	REQUESTED_DATE DATE,
	RESULTS_REC_FLAG VARCHAR2(2 BYTE),
	RESULTS_REC_DATE DATE,
	CANCELLED_FLAG VARCHAR2(2 BYTE),
	CANCELLED_DATE DATE,
	SERVICE_STATUS NUMBER(10,0),
	CREATOR	NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER(10,0),
	RID NUMBER(10,0)
);

COMMENT ON TABLE ER_ORDER_SERVICES IS 'Stores info about services requested with the order';

COMMENT ON COLUMN ER_ORDER_SERVICES.PK_ORDER_SERVICES IS 'Stores sequence generated unique value';
COMMENT ON COLUMN ER_ORDER_SERVICES.FK_ORDER_ID IS 'Stores foreign key reference to er_order table';
COMMENT ON COLUMN ER_ORDER_SERVICES.SERVICE_CODE IS 'Stores service cod ewhich comes from ESB message';
COMMENT ON COLUMN ER_ORDER_SERVICES.REQUESTED_DATE IS 'Stores date on which the request is created';
COMMENT ON COLUMN ER_ORDER_SERVICES.RESULTS_REC_FLAG IS 'Stores 1 if results received else 0 or null';
COMMENT ON COLUMN ER_ORDER_SERVICES.RESULTS_REC_DATE IS 'Stores date on which the results are received';
COMMENT ON COLUMN ER_ORDER_SERVICES.CANCELLED_FLAG IS 'Stores 1 if service request is cancelled else 0 or null';
COMMENT ON COLUMN ER_ORDER_SERVICES.CANCELLED_DATE IS 'Stores date on which the service request is cancelled';
COMMENT ON COLUMN ER_ORDER_SERVICES.SERVICE_STATUS IS 'Stores reference to er_codelst table indicates the service status';
COMMENT ON COLUMN ER_ORDER_SERVICES.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_SERVICES.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_SERVICES.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_SERVICES.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_SERVICES.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_SERVICES.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_SERVICES.RID IS 'Uses for Audit trail. Stores sequence generated unique value';
	
CREATE SEQUENCE SEQ_ER_ORDER_SERVICES MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

commit;
