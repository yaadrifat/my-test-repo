set define off;

--STARTS RENAMING COLUMN last_modified_on TO LAST_MODIFIED_DATE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_FUNDING_SCHEDULE' AND COLUMN_NAME = 'last_modified_on';
IF (v_column_exists > 0) THEN
	EXECUTE IMMEDIATE 'alter table CB_FUNDING_SCHEDULE rename column last_modified_on to LAST_MODIFIED_DATE;';
END IF;
END;
/
--END


--Sql for Table CB_CORD to add column CB_IDS_CHANGE_REASON
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CB_IDS_CHANGE_REASON'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add CB_IDS_CHANGE_REASON varchar2(200 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CB_IDS_CHANGE_REASON IS 'store the reason of change the ids'; 
commit;


--Sql for Table CB_CORD to add sequence in ER_LABTEST.'; 
DECLARE 
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_LABTEST' AND column_name = 'LABTEST_SEQ'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table ER_LABTEST add LABTEST_SEQ number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN ER_LABTEST.LABTEST_SEQ IS 'This Column will store the sequence of Labtest Name.'; 
commit;

--Sql for Table CB_CORD to add column CB_RHTYPE_OTHER_SPEC
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CB_RHTYPE_OTHER_SPEC'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add CB_RHTYPE_OTHER_SPEC varchar2(200 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CB_RHTYPE_OTHER_SPEC IS 'store the desciption if the RH type choose Other'; 
commit;


--STARTS ADDING COLUMN TO CB_CORD_MINIMUM_CRITERIA--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'FK_ORDER_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(FK_ORDER_ID NUMBER(10,0))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'FK_RECIPIENT';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(FK_RECIPIENT NUMBER(10,0))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CREATOR';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CREATOR NUMBER(10,0))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CREATED_ON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CREATED_ON DATE)';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'LAST_MODIFIED_BY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(LAST_MODIFIED_BY NUMBER(10,0))';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'LAST_MODIFIED_DATE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(LAST_MODIFIED_DATE DATE)';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'IP_ADD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(IP_ADD VARCHAR2(15))';
  end if;
end;
/

--END--
--COMMENTS FOR CB_CORD_MINIMUM_CRITERIA TABLE--

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."FK_ORDER_ID" IS 'foreign key column for er_order table';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."FK_RECIPIENT" IS 'foreign key column for CB_RECEIPANT_INFO';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."LAST_MODIFIED_DATE" IS 'This column stores the name of user who last modified the row in the table.';
COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

--END--

commit;