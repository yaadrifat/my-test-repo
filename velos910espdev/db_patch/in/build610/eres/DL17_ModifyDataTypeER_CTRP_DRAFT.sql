set define off;
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND column_name = 'FK_CODELST_DISEASE_SITE';
 if (v_column_exists = 1) then
   UPDATE ER_CTRP_DRAFT SET FK_CODELST_DISEASE_SITE=null ;
  execute immediate 'ALTER TABLE ER_CTRP_DRAFT MODIFY(FK_CODELST_DISEASE_SITE VARCHAR2(500 BYTE))';
  commit;
 end if;
end;
/
