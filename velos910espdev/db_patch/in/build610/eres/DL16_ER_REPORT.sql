
-- INSERTING into ER_REPORT

Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
values (200000,'CBU Summary Report','CBU Summary Report','',0,null,null,'N',null,null,1,null,'',null,null);
COMMIT;

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_cdr_cbu_id, cord_external_cbu_id, cord_registry_id, cord_local_cbu_id, cord_id_number_on_cbu_bag, cord_isbi_din_code, cord_isit_product_code from cb_cord  order by pk_cord' where pk_report = 200000;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_cdr_cbu_id, cord_external_cbu_id, cord_registry_id, cord_local_cbu_id, cord_id_number_on_cbu_bag, cord_isbi_din_code, cord_isit_product_code from cb_cord  order by pk_cord' where pk_report = 200000;
COMMIT;

