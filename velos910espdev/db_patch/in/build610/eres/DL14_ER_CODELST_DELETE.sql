set define off;


delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='na';
delete from ER_CODELST where CODELST_TYPE ='note_assess' and codelst_subtyp='ncd';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='tu';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='ineligible';
delete from ER_CODELST where CODELST_TYPE = 'note_assess' and codelst_subtyp='incmp_eligible';

commit;
