SET DEFINE OFF;
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'faq' and OBJECT_NAME = 'ref_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'faq', 'ref_menu', 6, 
    0, 'FAQ', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting faq for ref_menu already exists');
  end if;
END;
/

COMMIT;