set define off;

delete from ER_CODELST where CODELST_TYPE = 'other_rec_cat' and CODELST_SUBTYP ='phy_assess'; 

delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='ac'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='bc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='cc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='dc'; 
delete from ER_CODELST where CODELST_TYPE = 'eligibile_cat' and CODELST_SUBTYP ='cbu_assessment'; 

commit;