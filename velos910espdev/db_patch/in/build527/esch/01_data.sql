update sch_codelst set codelst_custom_col1 ='default_data' where codelst_type ='budget_stat';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,1,'01_Data.sql',sysdate,'8.9.0 Build#527');

commit;