DECLARE 
  numCount NUMBER default 0; 
BEGIN 
	SELECT COUNT(*) INTO numCount 
	FROM ALL_INDEXES 
	WHERE TABLE_NAME ='ER_PATPROT'
	AND INDEX_NAME = 'IDX_PATPROT_STUDYPAT';
	
	IF (numCount = 0) THEN
		execute immediate 'CREATE INDEX ERES.IDX_PATPROT_STUDYPAT ON ERES.ER_PATPROT
		(FK_STUDY, FK_PER)
		LOGGING
		NOPARALLEL';
		dbms_output.put_line ('Index created');
	ELSE
		dbms_output.put_line ('Index not created');
	END IF;

	SELECT COUNT(*) INTO numCount 
	FROM ALL_INDEXES 
	WHERE TABLE_NAME ='ER_PATFORMS'
	AND INDEX_NAME = 'IDX_PATFORMS_PER';
	
	IF (numCount > 0) THEN
		execute immediate 'DROP INDEX IDX_PATFORMS_PER';
		
		dbms_output.put_line ('Index dropped');
	end if;

	SELECT COUNT(*) INTO numCount 
	FROM ALL_INDEXES 
	WHERE TABLE_NAME ='ER_PATFORMS'
	AND INDEX_NAME = 'XIF83ER_PATFORMS';
	
	IF (numCount = 0) THEN
		execute immediate 'CREATE INDEX "ERES"."XIF83ER_PATFORMS" ON "ERES"."ER_PATFORMS"
		  (
			"FK_PER"
		  )
		  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
		  (
			INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
		  )
		  TABLESPACE "ERES_LARGE_INDX"' ;
		  
		dbms_output.put_line ('System Index created');
	ELSE
		dbms_output.put_line ('System Index not created');
	END IF;
end;
/

