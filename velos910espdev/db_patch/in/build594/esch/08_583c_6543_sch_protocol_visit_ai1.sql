CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_AI1"
AFTER INSERT
ON SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_pk_budget Number;
v_pk_bgtsec Number;
v_pk_bgtcal Number;
   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AI1', pLEVEL  => Plog.LDEBUG);
v_budget_study number;
v_budget_org number;
v_budget_org_patNO number;
v_return number;

maxsection  NUMBER(38,0); 
minsection  NUMBER(38,0); 

BEGIN

--get calendar's default budget
begin

--    plog.debug(pctx,':new.fk_protocol '||:new.fk_protocol );

	select pk_budget,PK_BGTCAL  ,nvl(fk_site,0),nvl(fk_study  ,0)
	into v_pk_budget,v_pk_bgtcal,v_budget_org,v_budget_study
	from sch_budget,sch_bgtcal
	where budget_calendar = :new.fk_protocol and pk_budget = fk_budget and
	bgtcal_protid = :new.fk_protocol 
	and nvl(BUDGET_DELFLAG,'Z') <>'Y' and nvl(BGTCAL_DELFLAG,'Z') <>'Y' 
	and rownum < 2;


  if v_budget_study > 0 and v_budget_org >0 then
    -- get study site local sample size

    begin
        select nvl(STUDYSITE_LSAMPLESIZE,1)
        into v_budget_org_patNO
        from er_studysites
        where fk_study = v_budget_study and fk_site =  v_budget_org;
    exception when no_data_found then
        v_budget_org_patNO := 1;
    end;

   else

        v_budget_org_patNO := 1;
   end if;

    select DECODE(:new.NUM_DAYS,0,0,:new.displacement) into minsection from dual;
   
    select seq_SCH_BGTSECTION.nextval into v_pk_bgtsec from dual;

    Insert into SCH_BGTSECTION
       (PK_BUDGETSEC, FK_BGTCAL, BGTSECTION_NAME,
        BGTSECTION_SEQUENCE,  CREATOR, CREATED_ON, IP_ADD,
         BGTSECTION_TYPE, BGTSECTION_PATNO, BGTSECTION_NOTES , FK_VISIT)
     Values
       (v_pk_bgtsec, v_pk_bgtcal, :new.visit_name,
        minsection, :new.creator,     sysdate, :new.ip_add,
        'P', v_budget_org_patNO, :new.description,:new.pk_protocol_visit);

    PKG_BGT.sp_insert_def_lineitms_commit('N', v_pk_bgtsec, v_pk_bgtcal,
        :new.creator, :new.ip_add, v_return);
    
    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null 
    and BGTSECTION_TYPE is not null and FK_BGTCAL = v_pk_bgtcal;
        
    --plog.debug(pctx, 'maxsection: '||maxsection|| ' minsection: ' || minsection );
    while (minsection <= maxsection)
    loop
      if maxsection < 0 then
        Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + maxsection
        where FK_VISIT is null and FK_BGTCAL = v_pk_bgtcal;
      else
      	if maxsection = 0 then
          maxsection := 1;
        end if;
        
        Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - maxsection
        where FK_VISIT is null and FK_BGTCAL = v_pk_bgtcal;
      end if;
 
      select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null 
      and BGTSECTION_TYPE is not null and FK_BGTCAL = v_pk_bgtcal;
        
      select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = v_pk_bgtcal;
    end loop;

exception when no_data_found then
    v_pk_budget := 0;
    v_pk_bgtcal := 0;
    plog.fatal(pctx,'exception: :new.fk_protocol '||:new.fk_protocol );

end;

END;
/

