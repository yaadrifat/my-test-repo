Set define off;

Alter Table CB_HLA add FK_CORD_CT_STATUS NUMBER(10) ;

COMMENT ON COLUMN "CB_HLA"."FK_CORD_CT_STATUS" IS 'This column will store the fk of cord ct status table'; 

Alter Table CB_CORD add GUID varchar2(50 BYTE) ;
COMMENT ON COLUMN "CB_CORD"."GUID" IS 'This column will store the unique guid'; 


Alter Table CB_CORD add (FK_CBB_PROCEDURE NUMBER(10),CBB_PROCEDURE_START_DATE DATE,CBB_PROCEDURE_END_DATE DATE,CBB_PROCEDURE_VERSION VARCHAR2(20 BYTE));

COMMENT ON COLUMN "CB_CORD"."FK_CBB_PROCEDURE" IS 'This column will store the fk of cbb procedure'; 
COMMENT ON COLUMN "CB_CORD"."CBB_PROCEDURE_START_DATE" IS 'This column will store the start date of cbb procedure'; 
COMMENT ON COLUMN "CB_CORD"."CBB_PROCEDURE_END_DATE" IS 'This column will store the end date of cbb procedure'; 
COMMENT ON COLUMN "CB_CORD"."CBB_PROCEDURE_VERSION" IS 'This column will store the version of cbb procedure'; 

--SQL for the Cord Entry ->Lab Summary ->Processing & Counts ->Total CBU Nucleated Cell Count Frozen (unknown if UnCorrected)



DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'NUCL_CELL_CNT_FRZ_UNKNOWN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add NUCL_CELL_CNT_FRZ_UNKNOWN number(10,2)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.NUCL_CELL_CNT_FRZ_UNKNOWN IS 'This column will store Nucleated frozen unknown if uncorrected Cell Count Start Processing.'; 
commit;

--SQL for the Cord -Entry ->Lab Summary ->Processing and Counts -> CBU Post Processing nRBC Absolute Number 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'POST_PRCSNG_NUCL_RBC_CN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add POST_PRCSNG_NUCL_RBC_CN number(10,2)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.POST_PRCSNG_NUCL_RBC_CN IS 'This column will store CBU Post Processing nRBC Absolute Number.';
commit;


--SQL for the Cord-Entry -> Lab Summary -Processing and Counts ->Total CD34 Cell Count Frozen

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'CD34_CELL_CNT_FRZN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add CD34_CELL_CNT_FRZN number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CD34_CELL_CNT_FRZN IS 'This column will store Total CD34 Cell Count Frozen.';
commit;



--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> % of CD34 Viable
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'PRCNT_CD34_VIABLE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add PRCNT_CD34_VIABLE number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.PRCNT_CD34_VIABLE IS 'This column will store Percentage of CD34 Viable.';
commit;


--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> CFU Test Date

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'CFU_TST_DATE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add CFU_TST_DATE date');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.CFU_TST_DATE IS 'This column will store CFU Test Date.';
commit;



--SQL for the Cord-Entry -> Lab Summary - Procesing and Counts -> Viability Test Date

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'VIA_TST_DATE';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add VIA_TST_DATE date');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.VIA_TST_DATE IS 'This column will store Viability Test Date.';
commit;






