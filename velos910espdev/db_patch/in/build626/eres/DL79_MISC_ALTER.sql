set define off;

--Sql for Table CBB to add column LINKS_HOVER_FLAG and NMD_PRESEARCH_FLAG.

DECLARE
  v_column_one_exists number := 0;   
   v_column_two_exists number := 0;  
BEGIN
  Select count(*) into v_column_one_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'LINKS_HOVER_FLAG'; 

Select count(*) into v_column_two_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'NMD_PRESEARCH_FLAG'; 


  if (v_column_one_exists = 0) then
      execute immediate ('ALTER TABLE CBB ADD LINKS_HOVER_FLAG VARCHAR2(1)');
  end if;  
  
   if (v_column_two_exists = 0) then
      execute immediate ('ALTER TABLE CBB ADD NMD_PRESEARCH_FLAG VARCHAR2(1)');
  end if; 
end;
/


