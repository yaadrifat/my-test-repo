set define off;
create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_AU0"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
--Created by Manimaran for Audit Update
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction(raid, 'SCH_SUBCOST_ITEM', :old.rid, 'U', usr);

   if nvl(:old.PK_SUBCOST_ITEM,0) !=
     nvl(:new.PK_SUBCOST_ITEM,0) then
     audit_trail.column_update
       (raid, 'PK_SUBCOST_ITEM',
       :old.PK_SUBCOST_ITEM, :new.PK_SUBCOST_ITEM);
   end if;

   if nvl(:old.FK_CALENDAR,0) !=
     nvl(:new.FK_CALENDAR,0) then
     audit_trail.column_update
       (raid, 'FK_CALENDAR',
       :old.FK_CALENDAR, :new.FK_CALENDAR);
   end if;

   if nvl(:old.SUBCOST_ITEM_NAME,' ') !=
     nvl(:new.SUBCOST_ITEM_NAME,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_NAME',
       :old.SUBCOST_ITEM_NAME, :new.SUBCOST_ITEM_NAME);
   end if;

   if nvl(:old.SUBCOST_ITEM_COST,0) !=
     nvl(:new.SUBCOST_ITEM_COST,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_COST',
       :old.SUBCOST_ITEM_COST, :new.SUBCOST_ITEM_COST);
  end if;

  if nvl(:old.SUBCOST_ITEM_UNIT,0) !=
     nvl(:new.SUBCOST_ITEM_UNIT,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_UNIT',
       :old.SUBCOST_ITEM_UNIT, :new.SUBCOST_ITEM_UNIT);
  end if;

  if nvl(:old.SUBCOST_ITEM_SEQ,0) !=
     nvl(:new.SUBCOST_ITEM_SEQ,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_SEQ',
       :old.SUBCOST_ITEM_SEQ, :new.SUBCOST_ITEM_SEQ);
  end if;

  if nvl(:old.FK_CODELST_CATEGORY,0) !=
     nvl(:new.FK_CODELST_CATEGORY,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :old.FK_CODELST_CATEGORY, :new.FK_CODELST_CATEGORY);
  end if;

  if nvl(:old.FK_CODELST_COST_TYPE,0) !=
     nvl(:new.FK_CODELST_COST_TYPE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :old.FK_CODELST_COST_TYPE, :new.FK_CODELST_COST_TYPE);
  end if;
-- YK 09May2011 Bug# 5821 $ #5818
  IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;

   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;

   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;

   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
 END;
 /