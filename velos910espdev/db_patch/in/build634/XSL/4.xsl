<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByOrg" match="ROW" use="ORGANIZATION" />
<xsl:key name="RecordsByJob" match="ROW" use="JOBTYPE" />
<xsl:key name="RecordsByStat" match="ROW" use="USR_STAT" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" bgcolor="lightgrey" >
<tr>
<td class="reportPanel"> 
VELMESSGE[M_Download_ReportIn]:<!-- Download the report in --> 
<A href='{$wd}' >
<img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img><!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
<img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img><!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>

</tr>
</table>
</xsl:if>
<table class="reportborder" width="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>

<table border ="0" width ="100%">
	   <tr class="reportGraphRow">
	   	   
		   
				<td class="reportData" WIDTH="50%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_ByJob_Type]<!-- By Job Type --> </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByJob', JOBTYPE)[1])=1]">
			   		  <xsl:variable name="v_job">
			   		  <xsl:value-of select="JOBTYPE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_job!=''">
			   		  <xsl:variable name="str" select="key('RecordsByJob', JOBTYPE)" />	
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="RIGHT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <xsl:variable name="job_count" select="format-number(((count($str[JOBTYPE=$v_job]) div count(//ROW[JOBTYPE!='']))*100),'###.00')"/>
				   			  <td width = "40%" class="reportGraphRow" ALIGN="LEFT">
				   		  <xsl:value-of select="$v_job" />&#xa0;:
				   		  </td>
						  <td class="reportGraphRow" ALIGN="LEFT" width="10%">
				   		  <xsl:value-of select="count($str[JOBTYPE=$v_job])" />
				   		  </td>

					  <td width="50%" class="reportGraphRow" ALIGN="LEFT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <td class="reportGraphRow" ALIGN="LEFT" width="30%">
				   				 <table width="{$job_count}%" border="0">
								 <tr class="reportGraphBar">
								 <td >
								 <xsl:text>&#xa0; </xsl:text>
								 </td> 
								 </tr> 
								 </table> 
							</td>
					  </tr></table></td>

						</tr></table></td>
						</tr>
						</xsl:if>
						</xsl:for-each>
						
						</table>
				</td>
				
<!--				
				<td class="reportData" WIDTH="40%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>By Organization </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByOrg', ORGANIZATION)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByOrg', ORGANIZATION)" />	
			   		  <tr>
			   		  <xsl:variable name="v_org">
			   		  <xsl:value-of select="ORGANIZATION" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_org!= ''">
			   		  <td width = "60%" class="reportGraphRow" ALIGN="RIGHT">
				   		  <xsl:value-of select="$v_org" />&#xa0; :
				   		  </td>
						  <td class="reportGraphRow" ALIGN="CENTER" width="5%">
				   		  <xsl:value-of select="count($str[ORGANIZATION=$v_org])" />
				   		  </td>
					  <td class="reportGraphRow" ALIGN="LEFT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <td class="reportGraphRow" ALIGN="LEFT" width="30%">
				   		  <xsl:variable name="org_count" select="format-number(((count($str[ORGANIZATION=$v_org]) div count(//ROW))*100),'###.00')"/>
				   				 <table width="{$org_count}" border="0">
								 <tr class="reportGraphBar">
								 <td >
								 <xsl:text>&#xa0; </xsl:text>
								 </td> 
								 </tr> 
								 </table> 
						</td>
									  
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
-->								
							
					<td class="reportData" WIDTH="50%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_ByUser_Stat]<!-- By User Status --> </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByStat', USR_STAT)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByStat', USR_STAT)" />	
			   		  <tr>
			   		  <xsl:variable name="v_stat">
			   		  <xsl:value-of select="USR_STAT" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_stat!= ''">
			   		  <td class="reportGraphRow" ALIGN="RIGHT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <xsl:variable name="stat_count" select="format-number(((count($str[USR_STAT=$v_stat]) div count(//ROW))*100),'###.00')"/>
				   			  <td width = "50%" class="reportGraphRow" ALIGN="RIGHT">
				   		  <xsl:value-of select="$v_stat" />&#xa0;:
				   		  </td>
						  <td class="reportGraphRow" ALIGN="LEFT" width="50%">
				   		  <xsl:value-of select="count($str[USR_STAT=$v_stat])" />
				   		  </td>
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
					

</tr>
<tr><td>&#xa0;</td></tr> 
<tr>
	<td class="reportLabel" ALIGN="CENTER" colspan="3"><b>VELLABEL[L_Total_MatchingRows]<!-- Total Matching Rows -->: <xsl:value-of select="count(//ROW)" /></b></td>
</tr>

</table>

<TABLE WIDTH="100%" BORDER="1">

<TR>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_User_Name]<!-- User Name --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Organization_S]<!-- Organization(s) --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Primary_Speciality]<!-- Primary Speciality --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_User_Group_S]<!-- User Group(s) --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_Job_Type]<!-- Job Type --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_Phone]<!-- Phone --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_EHypenMail]<!-- E-mail --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_Status]<!-- Status --></TH>
</TR>

<xsl:apply-templates select="ROW"/>
</TABLE>
<hr class="thinLine" />
<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >
<TR class="reportEvenRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="USR_FIRSTNAME" />
<xsl:text>&#xa0;</xsl:text>
<xsl:value-of select="USR_LASTNAME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="ORGANIZATION" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="PRIMARY_SPL" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="USER_GROUPS" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="JOBTYPE" />
</TD>

<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ADD_PHONE" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ADD_EMAIL" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="USR_STAT" />
</TD>
</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="USR_FIRSTNAME" />
<xsl:text>&#xa0;</xsl:text>
<xsl:value-of select="USR_LASTNAME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="ORGANIZATION" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="PRIMARY_SPL" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="USER_GROUPS" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="JOBTYPE" />
</TD>

<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ADD_PHONE" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ADD_EMAIL" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="USR_STAT" />
</TD>
</TR>
</xsl:otherwise>
</xsl:choose> 
</xsl:template>
</xsl:stylesheet>
