create or replace
function f_get_dynamicformgrp(categry varchar2) return SYS_REFCURSOR IS

dynamicform_grp SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_grp FOR 
select 
  distinct pk_question_group PK_QUESTION_GRP,
  question_grp_desc QUESTION_GRP_NAME
from 
  cb_question_group quesgrp,
  cb_question_grp qgrp 
where 
  qgrp.fk_question_group=quesgrp.pk_question_group and 
  qgrp.fk_form=(select pk_form from cb_forms frms where frms.forms_desc=categry) 
order by quesgrp.pk_question_group;

return dynamicform_grp;
end;
/
