CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_BI" BEFORE INSERT ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
erid NUMBER(10);
usr VARCHAR(2000);
raid NUMBER(10);
insert_data CLOB;

BEGIN

 BEGIN
  usr := getuser(:NEW.creator);
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'SCH_PORTAL_FORMS',erid, 'I',usr);
insert_data:=:NEW.PK_PF ||'|'|| :NEW.FK_PORTAL ||'|'|| :NEW.FK_CALENDAR ||'|'||
:NEW.FK_EVENT ||'|'|| :NEW.FK_FORM
||'|'|| :NEW.RID ||'|'|| :NEW.CREATOR
||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'|| :NEW.IP_ADD;
INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

END;
/
CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_B0" 
BEFORE UPDATE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL) 
begin 
	:new.last_modified_date := sysdate;
end;
/
create or replace
TRIGGER "ESCH"."SCH_PORTAL_FORMS_AU0"
AFTER UPDATE
ON ESCH.SCH_PORTAL_FORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :OLD.rid, 'U', usr );


   IF NVL(:OLD.PK_PF,0) !=
      NVL(:NEW.PK_PF,0) THEN
      audit_trail.column_update
        (raid, 'PK_PF',
        :OLD.PK_PF, :NEW.PK_PF);
   END IF;

   IF NVL(:OLD.FK_PORTAL,0) !=
      NVL(:NEW.FK_PORTAL,0) THEN
      audit_trail.column_update
        (raid, 'FK_PORTAL',
        :OLD.FK_PORTAL, :NEW.FK_PORTAL);
   END IF;

   IF NVL(:OLD.FK_CALENDAR,0) !=
      NVL(:NEW.FK_CALENDAR,0) THEN
      audit_trail.column_update
        (raid, 'FK_CALENDAR',
        :OLD.FK_CALENDAR, :NEW.FK_CALENDAR);
   END IF;

   IF NVL(:OLD.FK_EVENT,0) !=
      NVL(:NEW.FK_EVENT,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.FK_EVENT, :NEW.FK_EVENT);
   END IF;
   IF NVL(:OLD.FK_FORM,0) !=
      NVL(:NEW.FK_FORM,0) THEN
      audit_trail.column_update
        (raid, 'FK_FORM',
        :OLD.FK_FORM, :NEW.FK_FORM);
   END IF;

   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_AD" 
AFTER DELETE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :old.rid, 'D');

  deleted_data :=
   :old.PK_PF || '|' ||
   :old.FK_PORTAL || '|' ||
   :old.FK_CALENDAR || '|' ||
   :old.FK_EVENT || '|' ||
   :old.FK_FORM || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.created_on) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


 



