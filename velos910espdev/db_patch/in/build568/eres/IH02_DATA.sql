SET DEFINE OFF;

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'addeventkit';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'addeventkit', 
     ' storage_subtyp = ''Kit'' and not exists (select * from sch_event_kit k where ' 
     || 'fk_event =?1 and pk_storage=k.fk_storage ) ', 'NUMBER', 'Event >> Resource >> Storage Kit');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'lookupType';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'lookupType', 
     ' fk_account=?1 and instr(dynrep_formlist,'';'') = 0 ' 
     || ' and pkg_user.f_getUserAdHocRight(?2 , pk_dynrep) > 0 ',
     'Add/Edit Forms >> Add Fields >> Lookup');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'mdynrep';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'mdynrep', 
     ' ab.Fk_study in ( ?1 )  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and ' ||  
     ' ab.record_type <> ''D''  and e.formstat_enddate IS NULL and e.fk_codelst_stat in ' || 
     ' (select pk_codelst from er_codelst where lower(codelst_subtyp)=''a'' ' ||
     ' and lower(codelst_type)=''frmstat'' ) and ab.lf_displaytype IN (''S'') ',
     'NUMBER_LIST', 'For Ad-hoc Queries, but possibly no longer used');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'portalDesign1';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'portalDesign1', 
     ' CHAIN_ID in (?1) AND UPPER(EVENT_TYPE) IN(''p'',''P'')and event_calassocto=''P'' ',
     'NUMBER_LIST', 'Portal Admin >> Portal Design >> Select Calendar');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'portaldetails';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'portaldetails', 
     '  PAT_STATUS_SUBTYPE = ''A''  ',
     null, 'Portal Admin >> Portal Details >> Select Patient');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'updatemultschedules';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'updatemultschedules', 
     ' CHAIN_ID in (?1) AND UPPER(EVENT_TYPE) IN(''p'',''P'') ' ||
     ' AND FK_CODELST_CALSTAT = (select PK_CODELST from SCH_CODELST where ' ||
     ' CODELST_TYPE = ''calStatStd'' and CODELST_SUBTYP = ''A'' and rownum<=1) ' ||
     ' and event_calassocto=''P'' ',
     'NUMBER_LIST', 'Manage Protocol >> Study Setup >> Update Multiple >> '||
     'Select Calendar');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'selProtCal';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'selProtCal', 
     ' CHAIN_ID in (?1) AND UPPER(EVENT_TYPE) IN(''p'',''P'') ' ||
     ' and event_calassocto=''P'' ',
     null, 'Possibly no longer used');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'emailuser';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'emailuser', 
     ' usr_account= ?1 and usr_type<>''X'' and usr_stat<>''D'' ',
     'NUMBER', 'Manage Account >> Users >> Email Users >> Select Users');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'cathpci_ICDev';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'cathpci_ICDev', 
     ' (((f_to_date(custom001) <= f_to_date(trim(''?1''))) AND (custom002 IS NULL ' ||
     ' OR f_to_date(custom002) >= f_to_date(trim(''?1''))) AND (custom002 IS NULL ' ||
     ' OR f_to_date(custom001) <> f_to_date(custom002))) AND FK_LKPLIB = ?2)',
     'VARCHAR,NUMBER', 'For eCardio ACC CathPCI ICDev');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'cathpci_ClsDev';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'cathpci_ClsDev', 
     ' (((f_to_date(custom001) <= f_to_date(trim(''?1''))) AND (custom002 IS NULL ' ||
     ' OR f_to_date(custom002) >= f_to_date(trim(''?1''))) AND (custom002 IS NULL ' ||
     ' OR f_to_date(custom001) <> f_to_date(custom002))) AND FK_LKPLIB = ?2)',
     'VARCHAR,NUMBER', 'For eCardio ACC CathPCI ClsDev');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'adult_Implant';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'adult_Implant', 
     '(custom001 IS NULL OR custom001 = ''?1'') AND (custom002 is NULL OR custom002 =''?2'')',
     'VARCHAR,VARCHAR', 'For eCardio STS Adult Implant');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'adult_VAD';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'adult_VAD', 
     ' (custom001 = ''?1'') ',
     'VARCHAR', 'For eCardio STS Adult VAD');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'study';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'study', 'Key reserved for internal use');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'formQuery';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'formQuery', 'Key reserved for internal use');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'linkFormType';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'linkFormType', 'Key reserved for internal use');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'mdynreptype';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'mdynreptype', 'Key reserved for internal use');
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_LKPFILTER where LKPFILTER_KEY = 'portalDesign';

  if (row_count < 1) then 
   Insert into ER_LKPFILTER 
    (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_COMMENT)
   Values
    (SEQ_ER_LKPFILTER.nextval, 'portalDesign', 'Key reserved for internal use');
  end if;
END;
/

COMMIT;
