Set Define Off;

Update er_codelst set codelst_desc='CBU Registry ID'  where  codelst_type='all_ids' and codelst_subtyp = 'cbu_reg_id';

Update er_codelst set codelst_desc='Maternal Registry ID'  where  codelst_type='all_ids' and codelst_subtyp = 'regis_mat_id';

Update er_codelst set codelst_desc='Maternal Local ID'  where  codelst_type='all_ids' and codelst_subtyp = 'mat_local_id';

Commit;
