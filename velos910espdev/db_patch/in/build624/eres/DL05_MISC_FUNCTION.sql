create or replace
function f_cord_eligble_history(pk_cord number) return SYS_REFCURSOR
IS
v_elig_hty SYS_REFCURSOR;
BEGIN
    OPEN v_elig_hty FOR SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') EH_DT,F_CODELST_DESC(STATUS.FK_STATUS_VALUE) EH_STATUS,ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||STATUS.PK_ENTITY_STATUS||')',',') EH_INELIGIBLEREASON,STATUS.STATUS_REMARKS EH_COMMENTS FROM CB_ENTITY_STATUS STATUS WHERE STATUS.ENTITY_ID=pk_cord AND STATUS.STATUS_TYPE_CODE='eligibility' order by STATUS.STATUS_DATE;
RETURN v_elig_hty;
END;
/




create or replace
function f_cord_licence_history(pk_cord number) return SYS_REFCURSOR
IS
v_lic_hty SYS_REFCURSOR;
BEGIN
    OPEN v_lic_hty FOR SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') LH_DT,F_CODELST_DESC(STATUS.FK_STATUS_VALUE) LH_STATUS,ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||STATUS.PK_ENTITY_STATUS||')',',') LH_UNLIC_REASON,STATUS.STATUS_REMARKS LH_COMMENTS FROM CB_ENTITY_STATUS STATUS WHERE STATUS.ENTITY_ID=pk_cord AND STATUS.STATUS_TYPE_CODE='licence'order by STATUS.STATUS_DATE;
RETURN v_lic_hty;
END;
/




create or replace
function f_cord_final_review(pk_cord number) return SYS_REFCURSOR
IS
v_final_review SYS_REFCURSOR;
BEGIN
    OPEN V_FINAL_REVIEW FOR 
                   SELECT 
                   REVIEW.LICENSURE_CONFIRM_FLAG L_CONFIRMFLAG,
                   REVIEW.ELIGIBLE_CONFIRM_FLAG E_CONFIRMFLAG,
                   TO_CHAR(REVIEW.CREATED_ON,'Mon DD, YYYY') REVIEW_DT,
                   F_CODELST_DESC(REVIEW.REVIEW_CORD_ACCEPTABLE_FLAG) REVIEW_RESULT,
                   F_GETUSER(REVIEW.ELIG_CREATOR) USERNAME,
                   REVIEW.FINAL_REVIEW_CONFIRM_FLAG REVIEWFLAG
                   FROM 
                   CB_CORD_FINAL_REVIEW REVIEW 
                   WHERE 
                   REVIEW.FK_CORD_ID=PK_CORD;
RETURN V_FINAL_REVIEW;
END;
/


create or replace
function f_cbu_history(cordid number) return SYS_REFCURSOR
IS
v_cord_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_HTY FOR SELECT 
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        F_GETUSER(STAT.CREATOR) CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,  
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2
                        FROM 
                        CB_ENTITY_STATUS STAT 
                        LEFT OUTER JOIN CB_CBU_STATUS STATUS 
                        ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS) 
                        LEFT OUTER JOIN ER_ORDER_HEADER ORDHDR 
                        ON(STAT.ENTITY_ID=ORDHDR.ORDER_ENTITYID)
                        LEFT OUTER JOIN ER_ORDER ORD1 
                        ON(ORD1.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER)
                        WHERE ( STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=CORDID) ) 
                        AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Stts' ) 
                        ORDER BY STAT.STATUS_DATE DESC;
RETURN V_CORD_HTY;
END;
/



create or replace
function f_cbu_req_history(cordid number,orderid number) return SYS_REFCURSOR
IS
v_cord_req_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_REQ_HTY FOR SELECT 
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        F_GETUSER(STAT.CREATOR) CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2
                        FROM 
                        CB_ENTITY_STATUS STAT 
                        LEFT OUTER JOIN 
                        CB_CBU_STATUS STATUS ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS) 
                        LEFT OUTER JOIN ER_ORDER ORD1 ON(STAT.ENTITY_ID=ORD1.PK_ORDER) 
                        WHERE  
                        (STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID =ORDERID) AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Stts' ) 
                        ORDER BY STAT.STATUS_DATE DESC;
RETURN V_CORD_REQ_HTY;
END;
/




create or replace
function f_getIdmDetails(cordId number) return SYS_REFCURSOR
IS
c_IdmDetails SYS_REFCURSOR;
BEGIN
open C_IDMDETAILS for 
SELECT
        PL.TESTDATE TESTDATEVAL,
        LTEST.LABTEST_NAME LABTESTNAME,
        F_CODELST_DESC(PL.FK_TEST_OUTCOME) FK_TEST_OUTCOME,
        PL.CMS_APPROVED_LAB CMS_APPROVED_LAB,
        PL.FDA_LICENSED_LAB FDA_LICENSED_LAB,
        PL.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER FLAG_FOR_LATER,
        PL.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY) ASSESSMENT_POSTEDBY,
        TO_CHAR(PL.ASSESSMENT_POSTEDON,'Mon DD, YYYY') ASSESSMENT_POSTEDON,
        F_GETUSER(PL.ASSESSMENT_CONSULTBY) ASSESSMENT_CONSULTBY,
        TO_CHAR(PL.ASSESSMENT_CONSULTON,'Mon DD, YYYY') ASSESSMENT_CONSULTON,
        PL.SOURCEVAL SOURCEVALS,
        ltest.pk_labtest lab_test
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                'Mon DD, YYYY') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=CORDID)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type='I'
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE='IOG'
            )
            ORDER BY LAB_TEST;
RETURN C_IDMDETAILS;
END;
/
commit;