--When SCH_EVENTSTAT table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTSTAT_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENTSTAT 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	V_EVENTSTAT_OLD VARCHAR2(200) :=NULL;
	V_EVENTSTAT_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTSTAT
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTSTAT',:OLD.RID,NULL,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTSTAT,0) != NVL(:NEW.PK_EVENTSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','PK_EVENTSTAT',:OLD.PK_EVENTSTAT,:NEW.PK_EVENTSTAT,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_DT,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.EVENTSTAT_DT,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_DT',TO_CHAR(:OLD.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_NOTES,' ') != NVL(:NEW.EVENTSTAT_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_NOTES',:OLD.EVENTSTAT_NOTES,:NEW.EVENTSTAT_NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,' ') != NVL(:NEW.FK_EVENT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
		IF (NVL(:OLD.EVENTSTAT,0) > 0) THEN
	      V_EVENTSTAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENTSTAT);  
		END IF;
		IF (NVL(:NEW.EVENTSTAT,0) > 0) THEN
	      V_EVENTSTAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENTSTAT);  
		END IF;
	
    IF NVL(:OLD.EVENTSTAT,0) != NVL(:NEW.EVENTSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT',V_EVENTSTAT_OLD,V_EVENTSTAT_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_ENDDT,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.EVENTSTAT_ENDDT,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_ENDDT',TO_CHAR(:OLD.EVENTSTAT_ENDDT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENTSTAT_ENDDT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTSTAT_AU1 ');
END;