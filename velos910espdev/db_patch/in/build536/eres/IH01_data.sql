SET DEFINE OFF;

Insert into ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, 
    OBJECT_DISPLAYTEXT, FK_ACCOUNT)
 Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', '6', 'protocol_tab', 6, 1, 
    'Event-Visit Grid', 0);

COMMIT;
