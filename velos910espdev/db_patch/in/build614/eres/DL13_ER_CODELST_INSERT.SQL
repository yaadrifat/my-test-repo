set define off;

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','attach','Attachments','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'idmtest_subques'
    AND codelst_subtyp = 'yes';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'idmtest_subques','yes','Yes','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'idmtest_subques'
    AND codelst_subtyp = 'none';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'idmtest_subques','none','None','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'idmtest_subques'
    AND codelst_subtyp = 'Some';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'idmtest_subques','some','Some','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'IDM';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'sub_entity_type','IDM','IDM Test','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'MRQ';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'sub_entity_type','MRQ','MRQ Test','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'NOTES';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'sub_entity_type','NOTES','CBU NOTES','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'idm_sub_ques'
    AND codelst_subtyp = 'subques1';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'idm_sub_ques','subques1','Was this test performed in a CMS approved laboratory','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'idm_sub_ques'
    AND codelst_subtyp = 'subques2';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'idm_sub_ques','subques2','Was this test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturers instructions','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'funding_status','active','Active','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'funding_status','inactive','Inactive','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'request_type'
    AND codelst_subtyp = 'data_modi_req';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'request_type','data_modi_req','Data Modification Request','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'approval_status'
    AND codelst_subtyp = 'pending';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'approval_status','pending','Pending','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'approval_status'
    AND codelst_subtyp = 'approved';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'approval_status','approved','Approved','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'approval_status'
    AND codelst_subtyp = 'completed';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'approval_status','completed','Completed','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'approval_status'
    AND codelst_subtyp = 'rejected';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'approval_status','rejected','Rejected','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


commit;