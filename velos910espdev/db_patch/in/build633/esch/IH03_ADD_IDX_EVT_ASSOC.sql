set define off;

DECLARE
  v_record_exists number := 0;  
BEGIN
  select count(*) into v_record_exists from user_indexes where UPPER(TABLE_OWNER) = 'ESCH' and
    INDEX_NAME = 'IDX_EVENT_ASSOC_FK_VISIT';
  if (v_record_exists > 0) then
    execute immediate 'DROP INDEX ESCH.IDX_EVENT_ASSOC_FK_VISIT';
  end if;
END;
/

CREATE INDEX ESCH.IDX_EVENT_ASSOC_FK_VISIT ON ESCH.EVENT_ASSOC
(FK_VISIT)
LOGGING
TABLESPACE ESCH_USER
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL
/
