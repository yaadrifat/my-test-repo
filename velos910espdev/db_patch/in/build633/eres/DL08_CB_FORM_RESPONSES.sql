--STARTS ADDING COLUMN TO CB_FORM_RESPONSES --

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_RESPONSES' AND column_name = 'FK_FORM_VERSION';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_FORM_RESPONSES ADD(FK_FORM_VERSION NUMBER(10,0))';
  end if;
end;
/


COMMENT ON COLUMN "CB_FORM_RESPONSES"."FK_FORM_VERSION" IS 'Stores reference to CB_FORM_VERSION table';

commit;
