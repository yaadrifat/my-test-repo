create table CB_FORM_VERSION(
	PK_FORM_VERSION NUMBER(10,0),
	FK_FORM NUMBER(10,0),
	ENTITY_ID NUMBER(10,0),
	DATE_MOMANS_OR_FORMFILL DATE,
	ENTITY_TYPE NUMBER(10,0),
	FORM_SEQ NUMBER(10,0),
	CREATOR NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER(10,0),
	RID NUMBER(10,0) 
);

COMMENT ON COLUMN CB_FORM_VERSION.PK_FORM_VERSION IS 'PRIMARY KEY';
COMMENT ON COLUMN CB_FORM_VERSION.FK_FORM IS 'Stores reference to CB_FORMS table';
COMMENT ON COLUMN CB_FORM_VERSION.ENTITY_ID IS 'Stores reference to tables depending on the entity_type value';
COMMENT ON COLUMN CB_FORM_VERSION.DATE_MOMANS_OR_FORMFILL IS 'Stores the Date Mom Answered Questionnaire / Form Completion Date';
COMMENT ON COLUMN CB_FORM_VERSION.ENTITY_TYPE IS 'Stores reference to er_codelst table where codelst_type=entity_type';
COMMENT ON COLUMN CB_FORM_VERSION.FORM_SEQ IS 'Stores the sequence of the FORM';
COMMENT ON COLUMN CB_FORM_VERSION.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN CB_FORM_VERSION.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN CB_FORM_VERSION.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN CB_FORM_VERSION.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN CB_FORM_VERSION.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN CB_FORM_VERSION.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN CB_FORM_VERSION.RID IS 'Uses for Audit trail. Stores sequence generated unique value'; 

CREATE SEQUENCE SEQ_CB_FORM_VERSION MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

commit;
