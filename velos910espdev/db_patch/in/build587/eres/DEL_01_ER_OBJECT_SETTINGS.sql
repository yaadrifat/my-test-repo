--First-----------------********************* Update Query ********************* ------------------
update ER_OBJECT_SETTINGS set object_type='M',object_name='report_menu' where Object_subtype='rpt_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='report_menu' where Object_subtype='adhoc_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='org_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='group_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='users_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='acclinks_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='formmgmt_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='portal_menu' and object_name='acct_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='new_menu' and object_name='study_menu';
update ER_OBJECT_SETTINGS set object_type='SM' where Object_subtype='open_menu' and object_name='study_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='manage_accnt' where Object_subtype='study_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='manage_accnt' where Object_subtype='mgpat_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='manage_accnt',object_displaytext='Budget'where Object_subtype='budget_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='accnt_menu'  where Object_subtype='personal_menu' and object_name='top_menu';
update ER_OBJECT_SETTINGS set object_type='M',object_name='accnt_menu'  where Object_subtype='acct_menu' and object_name='top_menu';

commit;
--Second-----------------********************* Insert Query ********************* ------------------

insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'TM','accnt_menu','top_menu',2,1,'Accounts',0); 
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'TM','manage_accnt','top_menu',3,1,'Manage',0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'TM','report_menu','top_menu',17,1,'Reports',0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','srch_cal','callib_menu',1,1,'Search Calendar',0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','add_newcal','callib_menu',2,1,'Create New Calendar',0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','newcal_type','callib_menu',3,1,'Add New Calendar Type' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','copy_cal','callib_menu',4,1,'Copy Existing Calender' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','srch_evnt','evtlib_menu',1,1,'Search Event' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','add_evnt','evtlib_menu',2,1,'Add New Event' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','evnt_cat','evtlib_menu',3,1,'Add New Category' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','srch_fld','fldlib_menu',1,1,'Search Field' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','add_fld','fldlib_menu',2,1,'Add Edit Field' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','add_mulfld','fldlib_menu',3,1,'Add Multiple Choice Field' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','fld_cat','fldlib_menu',4,1,'Add New Category' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','copy_fld','fldlib_menu',5,1,'Copy Existing Field' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','srch_form','formlib_menu',1,1,'Search Form' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','add_newform','formlib_menu',2,1,'Create New Form' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','form_type','formlib_menu',3,1,'Add New Form Type' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','copy_form','formlib_menu',4,1,'Copy Existing Form' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','my_prof','personal_menu',1,1,'My Profile' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','my_links','personal_menu',2,1,'My Links' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','new_trials','personal_menu',3,1,'New Trials' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','my_setting','personal_menu',4,1,'Settings' ,0);
insert into ER_OBJECT_SETTINGS values ((SEQ_ER_OBJECT_SETTINGS.nextval),'SM','pass_word','personal_menu',5,1,'Password/e-Sign' ,0);

commit;
--Third-----------------********************* Sequence Update Query ********************* ------------------

update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=1 where object_type='TM' and object_name='top_menu' and object_subtype='homepage_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=2 where object_type='TM' and object_name='top_menu' and object_subtype='accnt_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=3 where object_type='TM' and object_name='top_menu' and object_subtype='manage_accnt';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=4 where object_type='TM' and object_name='top_menu' and object_subtype='irb_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=5 where object_type='TM' and object_name='top_menu' and object_subtype='lib_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=6 where object_type='TM' and object_name='top_menu' and object_subtype='inv_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=7 where object_type='TM' and object_name='top_menu' and object_subtype='mile_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=8 where object_type='TM' and object_name='top_menu' and object_subtype='report_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=9 where object_type='TM' and object_name='top_menu' and object_subtype='dash_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=10 where object_type='TM' and object_name='top_menu' and object_subtype='grid_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=11 where object_type='TM' and object_name='top_menu' and object_subtype='help_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=12 where object_type='TM' and object_name='top_menu' and object_subtype='logout_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=1 where object_type='M' and object_name='lib_menu' and object_subtype='callib_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=2 where object_type='M' and object_name='lib_menu' and object_subtype='formlib_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=3 where object_type='M' and object_name='lib_menu' and object_subtype='evtlib_menu';
update ER_OBJECT_SETTINGS set OBJECT_SEQUENCE=4 where object_type='M' and object_name='lib_menu' and object_subtype='fldlib_menu';

commit;
