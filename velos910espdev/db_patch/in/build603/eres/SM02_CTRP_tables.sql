set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_STUDY'
    AND COLUMN_NAME = 'STUDY_CTRP_REPORTABLE'; 

	if (v_column_exists = 0) then
		execute immediate 'alter table ER_STUDY add (STUDY_CTRP_REPORTABLE number)';
		dbms_output.put_line('Column STUDY_CTRP_REPORTABLE added to table ER_STUDY');
	else
		dbms_output.put_line('Column STUDY_CTRP_REPORTABLE exists in table ER_STUDY');
	end if;
end;
/

COMMENT ON COLUMN ER_STUDY.STUDY_CTRP_REPORTABLE IS 'Identifies CTRP Reportable flag 1-Yes, 0-No';

update ER_STUDY set STUDY_CTRP_REPORTABLE = 0;

commit;
 
DECLARE
  v_column_exists number := 0;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ADD'
    AND COLUMN_NAME = 'ADD_TTY'; 

	if (v_column_exists = 0) then
		execute immediate 'alter table ER_ADD add (ADD_TTY varchar2(100))';
		dbms_output.put_line('Column ADD_TTY added to table ER_ADD');
	else
		dbms_output.put_line('Column ADD_TTY exists in table ER_ADD');
	end if;
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ADD'
    AND COLUMN_NAME = 'ADD_URL'; 

	if (v_column_exists = 0) then
		execute immediate 'alter table ER_ADD add (ADD_URL varchar2(100))';
		dbms_output.put_line('Column ADD_URL added to table ER_ADD');
	else
		dbms_output.put_line('Column ADD_URL exists in table ER_ADD');
	end if;
end;
/

COMMENT ON COLUMN ER_ADD.ADD_TTY 
 IS 'Stores TTY';
COMMENT ON COLUMN ER_ADD.ADD_URL 
 IS 'Stores URL';


DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'ER_CTRP_DRAFT'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE eres.ER_CTRP_DRAFT (
			PK_CTRP_DRAFT NUMBER primary key,
			FK_STUDY NUMBER,
			CTRP_DRAFT_TYPE NUMBER,
			FK_CODELST_SUBMISSION_TYPE NUMBER,
			AMEND_NUMBER VARCHAR2(20),
			AMEND_DATE DATE,
			DRAFT_XML_REQD NUMBER,
			FK_TRIAL_OWNER NUMBER,
			LEAD_ORG_STUDYID  VARCHAR2 (100),
			SUBMIT_ORG_STUDYID VARCHAR2 (100),
			NCT_NUMBER VARCHAR2(20),
			OTHER_NUMBER VARCHAR2 (100),
			DRAFT_PHASE NUMBER,	
			DRAFT_PILOT NUMBER,
			STUDY_TYPE NUMBER,
			FK_CODELST_INTERVENTION_TYPE NUMBER,
			INTERVENTION_NAME VARCHAR2(50),
			DISEASE_NAME NUMBER,
			STUDY_PURPOSE NUMBER,
			STUDY_PURPOSE_OTHER VARCHAR2(1000),
			LEAD_ORG_CTRPID NUMBER,
			LEAD_ORG_NAME VARCHAR2(50),
			LEAD_ORG_FK_ADD NUMBER,
			LEAD_ORG_TYPE NUMBER,
			SUBMIT_ORG_CTRPID NUMBER,
			SUBMIT_ORG_NAME VARCHAR2 (50),
			SUBMIT_ORG_FK_ADD NUMBER,
			SUBMIT_ORG_TYPE NUMBER,
			PI_CTRPID NUMBER,
			PI_FIRST_NAME VARCHAR2(30),
			PI_MIDDLE_NAME VARCHAR2(30),
			PI_LAST_NAME VARCHAR2(30),
			PI_FK_ADD NUMBER,
			SPONSOR_CTEP_ID NUMBER,
			SPONSOR_NAME VARCHAR2(30),
			SPONSOR_FK_ADD NUMBER,
			RESPONSIBLE_PARTY NUMBER,
			RP_SPONSOR_CONTACT_TYPE NUMBER,
			RP_SPONSOR_CONTACT_ID NUMBER,
			SPONSOR_PERSONAL_TITLE VARCHAR2(50),
			SPONSOR_PERSONAL_FIRST_NAME VARCHAR2(30),
			SPONSOR_PERSONAL_MIDDLE_NAME VARCHAR2(30),
			SPONSOR_PERSONAL_LAST_NAME VARCHAR2(30),
			SPONSOR_GENERIC_NAME VARCHAR2(100),
			RP_EMAIL VARCHAR2(100),
			RP_PHONE VARCHAR2(100),
			RP_EXTN NUMBER,
			SUMM4_SPONSOR_TYPE NUMBER,
			SUMM4_SPONSOR_ID VARCHAR2(50),
			SUMM4_SPONSOR_NAME  VARCHAR2 (50),
			SUMM4_SPONSOR_FK_ADD NUMBER,
			SPONSOR_PROGRAM_CODE VARCHAR2(20),
			FK_CODELST_CTRP_STUDY_STATUS NUMBER,
			CTRP_STUDY_STATUS_DATE DATE,
			CTRP_STUDY_STOP_REASON VARCHAR2(4000),
			CTRP_STUDY_START_DATE DATE,
			IS_START_ACTUAL NUMBER,
			CTRP_STUDY_COMP_DATE DATE,
			IS_COMP_ACTUAL NUMBER,
			CTRP_STUDYACC_OPEN_DATE DATE,
			CTRP_STUDYACC_CLOSED_DATE DATE,
			SITE_TARGET_ACCRUAL NUMBER,
			OVERSIGHT_COUNTRY NUMBER,
			OVERSIGHT_ORGANIZATION NUMBER,
			FDA_INTVEN_INDICATOR NUMBER,
			SECTION_801_INDICATOR NUMBER,
			DELAY_POST_INDICATOR NUMBER,
			DM_APPOINT_INDICATOR NUMBER,
			DELETED_FLAG NUMBER Default 0,
			RID NUMBER,
			IP_ADD VARCHAR2(15),
			CREATOR NUMBER,
			CREATED_ON DATE Default sysdate,
			LAST_MODIFIED_BY NUMBER,
			LAST_MODIFIED_DATE DATE)';
		dbms_output.put_line('Table ER_CTRP_DRAFT is created in eres schema');
	else
		dbms_output.put_line('Table ER_CTRP_DRAFT exists in eres schema');
	end if;
end;
/
			

COMMENT ON COLUMN ER_CTRP_DRAFT.PK_CTRP_DRAFT 
 IS 'Primary key';
COMMENT ON COLUMN ER_CTRP_DRAFT.FK_STUDY 
 IS 'Foreign Key for ER_STUDY';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_DRAFT_TYPE 
 IS 'Identifies Draft Type- 1-Industrial, 0-Non-Industrial';
COMMENT ON COLUMN ER_CTRP_DRAFT.FK_CODELST_SUBMISSION_TYPE 
 IS 'Indicates Submission Type. Maps to codelst_type � ctrpSubmType in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.AMEND_NUMBER 
 IS 'Indicates Amendment Number (Alphanumeric)';
COMMENT ON COLUMN ER_CTRP_DRAFT.AMEND_DATE 
 IS 'Amendment Date';
COMMENT ON COLUMN ER_CTRP_DRAFT.DRAFT_XML_REQD 
 IS 'Identifies XML required flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.FK_TRIAL_OWNER 
 IS 'Foreign Key for ER_USER';
COMMENT ON COLUMN ER_CTRP_DRAFT.LEAD_ORG_STUDYID  
 IS 'Lead Organization Study ID. Usually is same as Study Number';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_STUDYID 
 IS 'Submitting Organization Study ID. Usually is same as Study Number';
COMMENT ON COLUMN ER_CTRP_DRAFT.NCT_NUMBER 
 IS 'Identifies NCT Number';
COMMENT ON COLUMN ER_CTRP_DRAFT.OTHER_NUMBER 
 IS 'Identifies Any other number linked to Study';
COMMENT ON COLUMN ER_CTRP_DRAFT.DRAFT_PHASE 
 IS 'Identifies Study phase. Maps to codelst_type � ctrpStudyPhase in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.DRAFT_PILOT 
 IS 'Identifies Pilot study flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.STUDY_TYPE 
 IS 'Identifies Study Type. Maps to Study research Type. 1-Interventional and 0-Observational';
COMMENT ON COLUMN ER_CTRP_DRAFT.FK_CODELST_INTERVENTION_TYPE 
 IS 'Intervention Type.  Maps to codelst_type � ctrpIntvnType in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.INTERVENTION_NAME 
 IS 'Identifies Intervention name';
COMMENT ON COLUMN ER_CTRP_DRAFT.DISEASE_NAME 
 IS 'Identifies Disease name';
COMMENT ON COLUMN ER_CTRP_DRAFT.STUDY_PURPOSE 
 IS 'Identifies Study purpose. Maps to codelst_type - ctrpStudyPurpose in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.STUDY_PURPOSE_OTHER 
 IS 'Other Purpose Description';
COMMENT ON COLUMN ER_CTRP_DRAFT.LEAD_ORG_CTRPID 
 IS 'Identifies Lead Organization CTRP ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.LEAD_ORG_NAME 
 IS 'Identifies Lead Organization Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.LEAD_ORG_FK_ADD 
 IS 'Identifies Lead Organization Address. Foreign Key for ER_ADD';
COMMENT ON COLUMN ER_CTRP_DRAFT.LEAD_ORG_TYPE 
 IS 'Lead Organization Type. Maps to codelst_type - ctrpOrgType in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_CTRPID 
 IS 'Identifies Submitting Organization CTRP ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_NAME 
 IS 'Identifies Submitting Organization Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_FK_ADD 
 IS 'Identifies Submitting Organization Address. Foreign Key for ER_ADD';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_TYPE 
 IS 'Submitting Organization Type. Maps to codelst_type - ctrpOrgType in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.PI_CTRPID 
 IS 'Identifies PI CTRP ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.PI_FIRST_NAME 
 IS 'Identifies PI First Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.PI_MIDDLE_NAME 
 IS 'Do not have this field in application';
COMMENT ON COLUMN ER_CTRP_DRAFT.PI_LAST_NAME 
 IS 'Identifies PI Last Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.PI_FK_ADD 
 IS 'Identifies PI Address. Foreign Key for ER_ADD';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_CTEP_ID 
 IS 'Identifies Sponsor CTRP ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_NAME 
 IS 'Identifies Sponsor Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_FK_ADD 
 IS 'Identifies Sponsor Address. Foreign Key for ER_ADD';
COMMENT ON COLUMN ER_CTRP_DRAFT.RESPONSIBLE_PARTY 
 IS 'Identifies Responsible Party flag 1-PI, 0-Sponsor';
COMMENT ON COLUMN ER_CTRP_DRAFT.RP_SPONSOR_CONTACT_TYPE 
 IS 'Identifies Sponsor Contact Type 1-Personal, 0-Generic';
COMMENT ON COLUMN ER_CTRP_DRAFT.RP_SPONSOR_CONTACT_ID 
 IS 'Identifies Sponsor Contact ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PERSONAL_TITLE 
 IS 'Personal Contact Title';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PERSONAL_FIRST_NAME 
 IS 'Personal Contact First Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PERSONAL_MIDDLE_NAME 
 IS 'Personal Contact Middle Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PERSONAL_LAST_NAME 
 IS 'Personal Contact Last Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_GENERIC_NAME 
 IS 'Generic Contact Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.RP_EMAIL 
 IS 'Responsible Party Email';
COMMENT ON COLUMN ER_CTRP_DRAFT.RP_PHONE 
 IS 'Responsible Party Phone';
COMMENT ON COLUMN ER_CTRP_DRAFT.RP_EXTN 
 IS 'Responsible Party Extension';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUMM4_SPONSOR_TYPE 
 IS 'Maps to Study summary Sponsor Name. Maps to codelst_type - sponsor in er_codelst ';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUMM4_SPONSOR_ID 
 IS 'Identifies Summary4 Sponsor ID';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUMM4_SPONSOR_NAME  
 IS 'Identifies Summary4 Sponsor Name';
COMMENT ON COLUMN ER_CTRP_DRAFT.SUMM4_SPONSOR_FK_ADD 
 IS 'Identifies Summary4 Sponsor Address. Foreign Key for ER_ADD';
COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PROGRAM_CODE 
 IS 'Identifies Summary4 Sponsor Program Code';
COMMENT ON COLUMN ER_CTRP_DRAFT.FK_CODELST_CTRP_STUDY_STATUS 
 IS 'Current Study Status OR Site Recruitment Status. Maps to codelst_type - ctrpStudyStatus OR ctrpRecruitStat in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDY_STATUS_DATE 
 IS 'Current Study Status Date';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDY_STOP_REASON 
 IS 'Indicates Study Stop reason';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDY_START_DATE 
 IS 'Indicates Study Start Date';
COMMENT ON COLUMN ER_CTRP_DRAFT.IS_START_ACTUAL 
 IS 'Identifies Study Start flag 1-Actual, 0-Anticipated';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDY_COMP_DATE 
 IS 'Indicates Study Completion Date';
COMMENT ON COLUMN ER_CTRP_DRAFT.IS_COMP_ACTUAL 
 IS 'Identifies Study Completion flag 1-Actual, 0-Anticipated';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDYACC_OPEN_DATE 
 IS 'Indicates Study site accrual opening date';
COMMENT ON COLUMN ER_CTRP_DRAFT.CTRP_STUDYACC_CLOSED_DATE 
 IS 'Indicates Study site accrual closing date';
COMMENT ON COLUMN ER_CTRP_DRAFT.SITE_TARGET_ACCRUAL 
 IS 'Indicates Study site target accrual';
COMMENT ON COLUMN ER_CTRP_DRAFT.OVERSIGHT_COUNTRY 
 IS 'Identifies overseeing country. Maps to codelst_type - ctrpCountry in er_codelst';
COMMENT ON COLUMN ER_CTRP_DRAFT.OVERSIGHT_ORGANIZATION 
 IS 'Identifies overseeing organization, depends on selected country.';
COMMENT ON COLUMN ER_CTRP_DRAFT.FDA_INTVEN_INDICATOR 
 IS 'Identifies FDA Intervention flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.SECTION_801_INDICATOR 
 IS 'Identifies Section 801 Indicator flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.DELAY_POST_INDICATOR 
 IS 'Identifies Delay Posting flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.DM_APPOINT_INDICATOR 
 IS 'Identifies DM Appointment flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_CTRP_DRAFT.DELETED_FLAG 
 IS 'Identifies Deletion flag 1-Yes (Deleted), 0-No (Not deleted)';
COMMENT ON COLUMN ER_CTRP_DRAFT.RID 
 IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN ER_CTRP_DRAFT.IP_ADD 
 IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN ER_CTRP_DRAFT.CREATOR 
 IS 'This column is used for Audit Trail. Identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_CTRP_DRAFT.CREATED_ON 
 IS 'This column is used for the audit trail. Stores the date on which this row was created';
COMMENT ON COLUMN ER_CTRP_DRAFT.LAST_MODIFIED_BY 
 IS 'This column is used for the audit trail. Identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_CTRP_DRAFT.LAST_MODIFIED_DATE 
 IS 'This column is used for the audit trail. Stores the date on which this row was last modified.';

 
DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'ER_STUDY_NIH_GRANT'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE eres.ER_STUDY_NIH_GRANT (
			PK_STUDY_NIH_GRANT NUMBER primary key,
			FK_STUDY NUMBER,
			FK_CODELST_FUNDMECH NUMBER,
			FK_CODELST_INSTCODE NUMBER,
			NIH_GRANT_SERIAL VARCHAR2(10),
			NCI_PROGRAM_CODE NUMBER(22,0),
			RID NUMBER,
			IP_ADD VARCHAR2(15),
			CREATOR NUMBER,
			CREATED_ON DATE,
			LAST_MODIFIED_BY NUMBER,
			LAST_MODIFIED_DATE DATE
			)';
		dbms_output.put_line('Table ER_STUDY_NIH_GRANT is created in eres schema');
	else
		dbms_output.put_line('Table ER_STUDY_NIH_GRANT exists in eres schema');
	end if;
end;
/

COMMENT ON COLUMN ER_STUDY_NIH_GRANT.PK_STUDY_NIH_GRANT 
 IS 'Stores Primary key';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.FK_STUDY 
 IS 'Foreign Key for ER_STUDY';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.FK_CODELST_FUNDMECH 
 IS 'Identifies Funding Mechanism. Maps to codelst_type - NIHFundMech in er_codelst';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.FK_CODELST_INSTCODE 
 IS 'Identifies Institute Code. Maps to codelst_type - NIHInstCode in er_codelst';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.NIH_GRANT_SERIAL 
 IS 'Indicates NIH Grant Serial Number';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.NCI_PROGRAM_CODE 
 IS 'Identifies NCI Program Code. Maps to codelst_type � NCIProgramCode in er_codelst';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.RID 
 IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.IP_ADD 
 IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.CREATOR 
 IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.CREATED_ON 
 IS 'This column is used for Audit Trail. Stores the date on which this row was created';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.LAST_MODIFIED_BY 
 IS 'The last_modified_by identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_STUDY_NIH_GRANT.LAST_MODIFIED_DATE 
 IS 'This column is used for Audit Trail. Stores the date on which this row was last modified';

 
DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'ER_STUDY_INDIDE'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE eres.ER_STUDY_INDIDE (
			PK_STUDY_INDIIDE NUMBER,
			FK_STUDY NUMBER,
			INDIDE_TYPE NUMBER,
			INDIDE_NUMBER VARCHAR2(15),
			FK_CODELST_INDIDE_GRANTOR NUMBER,
			FK_CODELST_INDIDE_HOLDER NUMBER,
			FK_CODELST_PROGRAM_CODE NUMBER,
			INDIDE_EXPAND_ACCESS NUMBER,
			FK_CODELST_ACCESS_TYPE NUMBER,
			INDIDE_EXEMPT NUMBER,
			RID NUMBER,
			IP_ADD VARCHAR2(15),
			CREATOR NUMBER,
			CREATED_ON DATE,
			LAST_MODIFIED_BY NUMBER,
			LAST_MODIFIED_DATE DATE)';
		dbms_output.put_line('Table ER_STUDY_INDIDE is created in eres schema');
	else
		dbms_output.put_line('Table ER_STUDY_INDIDE exists in eres schema');
	end if;
end;
/

COMMENT ON COLUMN ER_STUDY_INDIDE.PK_STUDY_INDIIDE 
 IS 'Stores Primary key';
COMMENT ON COLUMN ER_STUDY_INDIDE.FK_STUDY 
 IS 'Foreign Key for ER_STUDY';
COMMENT ON COLUMN ER_STUDY_INDIDE.INDIDE_TYPE 
 IS 'Identifies INDIDE Type. 1-IND, 2-IDE';
COMMENT ON COLUMN ER_STUDY_INDIDE.INDIDE_NUMBER 
 IS 'Identifies IND/IDE Number';
COMMENT ON COLUMN ER_STUDY_INDIDE.FK_CODELST_INDIDE_GRANTOR 
 IS 'Identifies INDIDE Grantor. Maps to codelst_type - INDIDEGrantor in er_codelst Depends on INDIDE_TYPE';
COMMENT ON COLUMN ER_STUDY_INDIDE.FK_CODELST_INDIDE_HOLDER 
 IS 'Identifies INDIDE Holder. Maps to codelst_type � INDIDEHolder in er_codelst';
COMMENT ON COLUMN ER_STUDY_INDIDE.FK_CODELST_PROGRAM_CODE 
 IS 'Identifies INDIDE Program Code. Maps to codelst_type � INDIDEProgCode in er_codelst';
COMMENT ON COLUMN ER_STUDY_INDIDE.INDIDE_EXPAND_ACCESS 
 IS 'Identifies INDIDE Expanded flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_STUDY_INDIDE.FK_CODELST_ACCESS_TYPE 
 IS 'Identifies INDIDE Holder. Maps to codelst_type � INDIDEAccess in er_codelst';
COMMENT ON COLUMN ER_STUDY_INDIDE.INDIDE_EXEMPT 
 IS 'Identifies INDIDE Exempt flag 1-Yes, 0-No';
COMMENT ON COLUMN ER_STUDY_INDIDE.RID 
 IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN ER_STUDY_INDIDE.IP_ADD 
 IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN ER_STUDY_INDIDE.CREATOR 
 IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_STUDY_INDIDE.CREATED_ON 
 IS 'This column is used for Audit Trail. Stores the date on which this row was created';
COMMENT ON COLUMN ER_STUDY_INDIDE.LAST_MODIFIED_BY 
 IS 'The last_modified_by identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_STUDY_INDIDE.LAST_MODIFIED_DATE 
 IS 'This column is used for Audit Trail. Stores the date on which this row was last modified';

  
DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'ER_CTRP_DOCUMENTS'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE eres.ER_CTRP_DOCUMENTS (
			PK_CTRP_DOC NUMBER,
			FK_CTRP_DRAFT NUMBER,
			FK_STUDYAPNDX  NUMBER,
			FK_CODELST_CTRP_DOCTYPE NUMBER,
			RID NUMBER,
			IP_ADD VARCHAR2(15),
			CREATOR NUMBER,
			CREATED_ON DATE,
			LAST_MODIFIED_BY NUMBER,
			LAST_MODIFIED_DATE DATE)';
		dbms_output.put_line('Table ER_CTRP_DOCUMENTS is created in eres schema');
	else
		dbms_output.put_line('Table ER_CTRP_DOCUMENTS exists in eres schema');
	end if;
end;
/

COMMENT ON COLUMN ER_CTRP_DOCUMENTS.PK_CTRP_DOC 
 IS 'Stores Primary key';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.FK_CTRP_DRAFT 
 IS 'Foreign Key for ER_CTRP_DRAFT';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.FK_STUDYAPNDX  
 IS 'Foreign Key to ER_STUDYAPNDX';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.FK_CODELST_CTRP_DOCTYPE 
 IS 'Identifies Document Type. Maps to codelst_type - ctrpDocType in er_codelst';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.RID 
 IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.IP_ADD 
 IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.CREATOR 
 IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.CREATED_ON 
 IS 'This column is used for Audit Trail. Stores the date on which this row was created';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.LAST_MODIFIED_BY 
 IS 'The last_modified_by identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN ER_CTRP_DOCUMENTS.LAST_MODIFIED_DATE 
 IS 'This column is used for Audit Trail. Stores the date on which this row was last modified';

