set define off;
--Sql for Table CB_ENTITY_SAMPLES to add column No. of Segments Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_OF_SEG_AVAIL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_OF_SEG_AVAIL number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_OF_SEG_AVAIL IS 'This column will store the No of Segment Available .'; 
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column Filter Paper Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'FILT_PAP_AVAIL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add FILT_PAP_AVAIL number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.FILT_PAP_AVAIL IS 'This column will store the Filter Paper Available.'; 
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column RBC Pellets Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'RBC_PEL_AVAIL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add RBC_PEL_AVAIL number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.RBC_PEL_AVAIL IS 'This column will store the RBC Pellets Available.'; 
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column No of Extracted DNA Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_EXT_DNA_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_EXT_DNA_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_EXT_DNA_ALI IS 'This column will store the No of Extracted DNA Aliquots.';
commit;




--Sql for Table CB_ENTITY_SAMPLES to add column No of Non-viable Cell Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_NON_VIA_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_NON_VIA_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_NON_VIA_ALI IS 'This column will store the No of Non-viable Cell Aliquots.';
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column No of Serum Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_SER_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_SER_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_SER_ALI IS 'This column will store the No of Serum Aliquots.';
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column No of Plasma Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_PLAS_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_PLAS_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_PLAS_ALI IS 'This column will store the No of Plasma Aliquots.';
commit;



--Sql for Table CB_ENTITY_SAMPLES to add column No of Viable Cell Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_VIA_CEL_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_VIA_CEL_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_VIA_CEL_ALI IS 'This column will store the  No of Viable Cell Aliquots.';
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column Total CBU Aliquots Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'TOT_CBU_ALI_AVA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add TOT_CBU_ALI_AVA number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.TOT_CBU_ALI_AVA IS 'This column will store the Total CBU Aliquots Available.';
commit;

--Sql for Table CB_ENTITY_SAMPLES to add column No of Extracted DNA Maternal Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_EXT_DNA_MAT_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_EXT_DNA_MAT_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_EXT_DNA_MAT_ALI IS 'This column will store the No of Extracted DNA Maternal Aliquots.';
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column No of Cell Maternal Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_CEL_MAT_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_CEL_MAT_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_CEL_MAT_ALI IS 'This column will store the No of Cell Maternal Aliquots.';
commit;


--Sql for Table CB_ENTITY_SAMPLES to add column No of Serum Maternal Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_SER_MAT_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_SER_MAT_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_SER_MAT_ALI IS 'This column will store the No of Serum Maternal Aliquots.';
commit;



--Sql for Table CB_ENTITY_SAMPLES to add column No of Plasma Maternal Aliquots
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'NO_PLAS_MAT_ALI'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add NO_PLAS_MAT_ALI number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.NO_PLAS_MAT_ALI IS 'This column will store the No of Plasma Maternal Aliquots.';
commit;



--Sql for Table CB_CORD to add column Total Maternal Aliquots Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'TOT_MAT_ALI_AVA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add TOT_MAT_ALI_AVA number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.TOT_MAT_ALI_AVA IS 'This column will store the Total Maternal Aliquots Available.';
commit;