set define off;

UPDATE  track_patches
SET APP_VERSION = replace(APP_VERSION,'9.1','9.1.0') 
WHERE APP_VERSION like '9.1 %';

commit;