CREATE OR REPLACE PACKAGE BODY ESCH."PKG_AUDIT_TRAIL_MODULE" AS

---- procedure SP_AUDIT_ERROR_LOG
PROCEDURE SP_AUDIT_ERROR_LOG (    
    p_ModuleName        IN  VARCHAR2,
    p_ErrorType         IN  VARCHAR2,
    p_ErrorSource       IN  VARCHAR2,
    p_ErrorMessage      IN  VARCHAR2,
    p_ErrorSql          IN  VARCHAR2,
    p_ErrorDescription  IN  VARCHAR2
)
IS
BEGIN
INSERT INTO ESCH.AUDIT_ERROR_LOG (PK_AUDIT_ERROR_LOG,MODULE_NAME,ERROR_TYPE, ERROR_SOURCE, ERROR_MESSAGE, ERROR_SQL,ERROR_DESCRIPTION) VALUES ( SEQ_AUDIT_ERROR_LOG.NEXTVAL, p_ModuleName,p_ErrorType,p_ErrorSource, p_ErrorMessage, p_ErrorSql, p_ErrorDescription);
COMMIT;
EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('Problem In SP_AUDIT_ERROR_LOG');
raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM); 
END SP_AUDIT_ERROR_LOG;

--To insert In AUDIT_ROW_MODULE 
PROCEDURE SP_ROW_INSERT
    (p_rowid     IN NUMBER,
    p_tabname   IN VARCHAR2,
    p_rid       IN NUMBER,
    p_modulepk  IN NUMBER,
    p_act       IN VARCHAR2,
    p_userid    IN NUMBER) AS
 
    v_table_display_value VARCHAR2(200);    
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    v_table_display_value := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETMAPPEDVALUE(p_tabname); 
    INSERT INTO AUDIT_ROW_MODULE(PK_ROW_ID,TABLE_NAME,RID,MODULE_ID,ACTION,TIMESTAMP,USER_ID,TABLE_DISPLAY_NAME) VALUES (p_rowid, p_tabname, p_rid,p_modulepk,p_act,sysdate,p_userid,v_table_display_value);
    COMMIT;
    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_ROW_INSERT ');
END SP_ROW_INSERT;

--To insert In AUDIT_COLUMN_MODULE 
PROCEDURE SP_COLUMN_INSERT
    (p_rowid        IN NUMBER,
    p_tablename     IN VARCHAR2,
    p_colname       IN VARCHAR2,
    p_oldval        IN VARCHAR2,
    p_newval        IN VARCHAR2,
    p_codelstid     IN NUMBER,
    p_remarks       IN VARCHAR2) AS       
    v_column_display_value VARCHAR2(200);
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    

BEGIN
    v_column_display_value :=ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETMAPPEDVALUE(p_tablename||'.'||p_colname); 
    INSERT INTO AUDIT_COLUMN_MODULE(PK_COLUMN_ID,FK_ROW_ID,COLUMN_NAME,OLD_VALUE,NEW_VALUE,FK_CODELST_REASON,REMARKS,COLUMN_DISPLAY_NAME) VALUES (SEQ_AUDIT_COLUMN_MODULE.nextval,p_rowid, p_colname, p_oldval, p_newval,p_codelstid,p_remarks,v_column_display_value);
    COMMIT;
    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_COLUMN_INSERT ');
END SP_COLUMN_INSERT;
--To Update column of reason and remarks in budget Changes.
PROCEDURE SP_UPDATECOLUMN
  (
    p_colValue     IN VARCHAR2,
    p_type         IN VARCHAR2,
    p_tableName IN VARCHAR2,
    p_colName   IN VARCHAR2,
    p_filterStr IN VARCHAR2)
IS
  v_sqlStr       VARCHAR2(10000);
  v_intColValue  INTEGER ;
  v_strColValue  VARCHAR2(4000);
  cursor_name    INTEGER;
  rows_processed INTEGER;
  v_ErrorType VARCHAR2(20):='EXCEPTION';
  PRAGMA AUTONOMOUS_TRANSACTION;
  
BEGIN
  --first do emptyBlob
    IF p_type        = 'INTEGER' THEN
    v_intColValue := to_number(TO_CHAR(p_colValue));    
    IF v_intColValue IS NOT NULL THEN      
      IF v_intColValue = 0 THEN        
        v_intColValue := NULL;
        v_sqlStr      :='update '||p_tableName||' set '||p_colname||' = null '||p_filterStr ;        
        EXECUTE immediate v_sqlStr ;
        COMMIT;
      ELSE
        v_sqlStr:='update '||p_tableName||' set '||p_colname||'='||v_intColValue||' '||p_filterStr ;        
        EXECUTE immediate v_sqlStr ;
        COMMIT;        
      END IF ;
    END IF;
    ELSIF p_type = 'STRING' THEN    
    v_strColValue := TO_CHAR(p_colValue);    
    IF v_strColValue IS NULL THEN      
      v_intColValue := NULL;
      v_sqlStr      :='update '||p_tableName||' set '||p_colname||' = null '||p_filterStr ;
      EXECUTE IMMEDIATE v_sqlStr ;
      COMMIT;
    ELSE
        v_sqlStr:='update '||p_tableName||' set '||p_colname||'= RTRIM('''||v_strColValue||''') ' ||p_filterStr ;        
        EXECUTE IMMEDIATE v_sqlStr ;
        COMMIT;      
    END IF;
  END IF;

    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_UPDATECOLUMN ');
  
  END SP_UPDATECOLUMN;

--procedure AUDIT_ROW_MODULE_UPDATE
PROCEDURE SP_AUDIT_ROW_MODULE_UPDATE
AS
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
    V_ChainsqlSql VARCHAR2(2000);
    V_ChainId NUMBER(10);
 
   CURSOR c_audit_row_module IS SELECT pk_row_id,table_name,rid,module_id,user_id FROM AUDIT_ROW_MODULE WHERE module_id IS NULL ORDER BY pk_row_id;
   r_audit_row_module c_audit_row_module%ROWTYPE;
BEGIN
    OPEN c_audit_row_module;
    LOOP
        FETCH c_audit_row_module INTO r_audit_row_module;
        EXIT WHEN c_audit_row_module%NOTFOUND;
        BEGIN
            --Update Module ID for table name SCH_BGTSECTION in AUDIT_ROW_MODULE table
            IF r_audit_row_module.table_name = 'SCH_BGTSECTION' THEN
                UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT fk_budget FROM SCH_BGTCAL WHERE pk_bgtcal = 
                (SELECT fk_bgtcal FROM SCH_BGTSECTION WHERE rid = r_audit_row_module.rid)) 
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
            --Update Module ID for table name SCH_LINEITEM in AUDIT_ROW_MODULE table
            ELSIF r_audit_row_module.table_name = 'SCH_LINEITEM' THEN
                UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT fk_budget FROM SCH_BGTCAL WHERE PK_bgtcal=(SELECT fk_bgtcal FROM sch_bgtsection WHERE PK_BUDGETSEC = (SELECT fk_bgtsection FROM sch_lineitem WHERE rid = r_audit_row_module.rid )))
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
            --Update Module ID And User ID for table name SCH_BGTAPNDX in AUDIT_ROW_MODULE table
            ELSIF r_audit_row_module.table_name = 'SCH_BGTAPNDX' THEN
                UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT fk_budget FROM sch_bgtapndx WHERE rid = r_audit_row_module.rid),
                    user_id = (SELECT creator FROM sch_bgtapndx WHERE rid = r_audit_row_module.rid)
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
                         
            ELSIF r_audit_row_module.table_name = 'SCH_EVENTSTAT' THEN
                UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT SESSION_ID FROM sch_events1 WHERE event_id =(SELECT fk_event FROM sch_eventstat WHERE rid= r_audit_row_module.rid))                  
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
            ELSIF r_audit_row_module.table_name = 'SCH_DOCS' THEN
        BEGIN
        V_ChainsqlSql := 'SELECT count(CHAIN_ID) FROM EVENT_ASSOC WHERE EVENT_ID =(SELECT FK_EVENT FROM SCH_EVENTDOC EDOC,SCH_DOCS DOC WHERE EDOC.PK_DOCS=DOC.PK_DOCS AND DOC.RID ='||r_audit_row_module.rid||')';
        
        EXECUTE IMMEDIATE V_ChainsqlSql INTO V_ChainId;
        IF(V_ChainId = 1) THEN 
         UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT CHAIN_ID FROM EVENT_ASSOC WHERE EVENT_ID =(SELECT FK_EVENT FROM SCH_EVENTDOC EDOC,SCH_DOCS DOC WHERE EDOC.PK_DOCS=DOC.PK_DOCS AND DOC.RID =r_audit_row_module.rid))                  
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
        ELSIF(V_ChainId = 0 ) THEN 
         UPDATE AUDIT_ROW_MODULE
                SET module_id = (SELECT CHAIN_ID FROM EVENT_DEF WHERE EVENT_ID =(SELECT FK_EVENT FROM SCH_EVENTDOC EDOC,SCH_DOCS DOC WHERE EDOC.PK_DOCS=DOC.PK_DOCS AND DOC.RID =r_audit_row_module.rid))                  
                WHERE pk_row_id = r_audit_row_module.pk_row_id;
        END IF;
        END;
        END IF;
     END;
    END LOOP;
    COMMIT;
    CLOSE c_audit_row_module; 
    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_AUDIT_ROW_MODULE_UPDATE ');
END SP_AUDIT_ROW_MODULE_UPDATE;

FUNCTION F_GETCHAINID(P_EVENTID NUMBER)
    RETURN NUMBER
IS
   v_chainid NUMBER;
   v_ErrorType VARCHAR2(20):='EXCEPTION';
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
        SELECT chain_id INTO v_chainid FROM esch.event_assoc WHERE event_id = p_eventid;
        RETURN v_chainid;
        EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN 0;        
        WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'',NULL,'Error In FUNCTION F_GETCHAINID ');        
END;  
--To get Location Of Changes done in budget
FUNCTION F_GETMAPPEDVALUE( p_fromvalue IN varchar2 )
   RETURN varchar2 
IS
    v_tovalue  varchar2(200);
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT to_value INTO v_tovalue FROM esch.AUDIT_MAPPEDVALUES WHERE from_value =''||p_fromvalue||'';
    RETURN v_tovalue;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN '';
WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'',NULL,'Error In FUNCTION F_GETCHAINID '); 
END F_GETMAPPEDVALUE;

--To fetch value from CSH_CODELST
FUNCTION F_GETCODELSTVALUE(P_CODELST NUMBER)
    RETURN VARCHAR2
IS
   v_codelstdesc VARCHAR2(200);
   --v_exception   VARCHAR2(100):='';
   v_ErrorType VARCHAR2(20):='EXCEPTION';
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT CODELST_DESC INTO v_codelstdesc FROM ESCH.SCH_CODELST WHERE pk_codelst = P_CODELST;
    RETURN v_codelstdesc;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN NULL;
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'',NULL,'Error In FUNCTION F_GETCHAINID ');
    --return v_exception;
END F_GETCODELSTVALUE;

--To get Record for report.
FUNCTION RPT_BUDGETAUDITREPORT (
   P_BudgetID      NUMBER,
   P_STATUS1       VARCHAR2,
   P_STATUS2       VARCHAR2,
   P_STATUSTEXT    VARCHAR2,
   P_SELECT        VARCHAR2,
   P_DATEFROM      VARCHAR2,
   P_DATETO        VARCHAR2,
   P_DATEFILTER    VARCHAR2,
   P_CHKBOXVAL     VARCHAR2
)
   RETURN CLOB
AS
   V_Cursor            Types.cursortype;
   V_AuditReason       Types.cursortype;
   V_SQLStr            VARCHAR2 (4000);

   V_ReasonId          NUMBER;
   V_ReasonName        VARCHAR2 (100);
   V_Temp              VARCHAR2 (200);
   V_ReasonDropDown    VARCHAR2 (1000);

   V_FieldName         VARCHAR2 (50);
   V_DBLocation        VARCHAR2 (200);
   V_Action            VARCHAR2 (50);
   V_ModifiedOn        VARCHAR2 (100);
   V_ModifiedBy        VARCHAR2 (100);
   V_ModifiedById      NUMBER;
   V_ChangeLoc         VARCHAR2 (2000);
   V_OldValue          VARCHAR2 (4000);
   V_NewValue          VARCHAR2 (4000);
   V_PkBudget          NUMBER;
   V_BudgetName        VARCHAR2 (100);
   V_PkColumnID        NUMBER;
   V_FkCodelstReason   NUMBER;
   V_Remarks           VARCHAR2 (4000);
   --   variable created on 19.1.2012
   V_Status1Sql        VARCHAR2 (4000);
   V_Status2Sql        VARCHAR2 (4000);
   V_Status1           VARCHAR2 (100);
   V_Status2           VARCHAR2 (100);
   V_MinDateSql        VARCHAR2 (4000);
   V_MaxDateSql        VARCHAR2 (4000);
   V_MinDate           VARCHAR2 (100);
   V_MaxDate           VARCHAR2 (100);
   V_DateFrom          VARCHAR2 (100);
   V_DateTo            VARCHAR2 (100);
   V_Select            VARCHAR2 (100);
   --CLOB Variable to generate the XML
   V_RowValue          CLOB;
   V_RowSet            CLOB;
   V_Result            CLOB;
   --Varibles For Log  
   v_ErrorType VARCHAR2(20):='EXCEPTION';
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN   
    V_DateFrom := P_DATEFROM;
    V_DateTo := P_DATETO;
    V_Status1Sql :=
      'select trim(codelst_desc) from esch.sch_codelst where pk_codelst = '
      || P_STATUS1;
       EXECUTE IMMEDIATE V_Status1Sql INTO   V_Status1;
    V_Status2Sql :=
      'select trim(codelst_desc) from esch.sch_codelst where pk_codelst = '
      || P_STATUS2;
    EXECUTE IMMEDIATE V_Status2Sql INTO   V_Status2;
    V_MinDateSql :=
      'select MIN(to_char(arm.timestamp,PKG_DATEUTIL.F_GET_DATEFORMAT)) from esch.audit_row_module arm,esch.audit_column_module acm where arm.pk_row_id=ACM.FK_ROW_ID and ARM.MODULE_ID='
      || P_BUDGETID
      || '  and acm.column_name=''FK_CODELST_STATUS'' and acm.new_value='
      || ''''
      || V_Status1
      || '''';
    EXECUTE IMMEDIATE V_MinDateSql INTO   V_MinDate;
    V_MaxDateSql :=
      'select MAX(to_char(arm.timestamp,PKG_DATEUTIL.F_GET_DATEFORMAT)) from esch.audit_row_module arm,esch.audit_column_module acm where arm.pk_row_id=ACM.FK_ROW_ID and ARM.MODULE_ID='
      || P_BUDGETID
      || '  and acm.column_name=''FK_CODELST_STATUS'' and acm.new_value='
      || ''''
      || V_Status2
      || '''';
    EXECUTE IMMEDIATE V_MaxDateSql INTO   V_MaxDate;
    V_ReasonDropDown := '0|Select an Option;';
    --Cursor to get the Reason values from the Codelst Table
   V_SQLStr :=
      'select pk_codelst,codelst_desc from esch.sch_codelst where codelst_type = ''audit_reason''';
    OPEN V_AuditReason FOR V_SQLStr;
    LOOP
        FETCH V_AuditReason INTO   V_ReasonId, V_ReasonName;
        EXIT WHEN V_AuditReason%NOTFOUND;
        BEGIN
            V_Temp := V_ReasonId || '|' || V_ReasonName;
            V_ReasonDropDown := V_ReasonDropDown || V_Temp || ';';
        END;
    END LOOP;
    V_ReasonDropDown :=
    SUBSTR (V_ReasonDropDown, 1, LENGTH (V_ReasonDropDown) - 1);
    IF (P_CHKBOXVAL = 'ON') THEN
    V_SQLStr :=
      ' SELECT * FROM (SELECT 
            acm.COLUMN_DISPLAY_NAME,
            arm.table_name||'', ''||acm.column_name as DBLOCATION,
           to_char(arm.timestamp,''mm/dd/yyyy:hh:mi:ss''),
           (select usr_firstname||'' ''||usr_lastname from eres.er_user where pk_user = arm.user_id) username,
           arm.user_id userid,
           CASE(arm.Action)
           WHEN ''I'' THEN ''INSERT''
           WHEN ''D'' THEN ''DELETE''
           WHEN ''U'' THEN ''UPDATE''
           END,
           ARM.TABLE_DISPLAY_NAME,
           NVL(acm.old_value,'''') AS old_value,
           NVL(acm.new_value,'''') AS new_value,
           pk_budget,
           budget_name,
           ACM.PK_COLUMN_ID,
           fk_codelst_reason,
           to_char(remarks)
        FROM esch.audit_row_module arm,
            esch.sch_budget sb,
            esch.audit_column_module acm
        WHERE sb.pk_budget = arm.module_id AND arm.pk_row_id = acm.fk_row_id and sb.pk_budget ='
      || P_BudgetID
      || ' and (acm.old_value is not null or acm.new_value is not null) 
          and (((to_date(arm.timestamp)) between to_date('''
      || V_MinDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_MaxDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)) '
      || P_SELECT
      || ' ((to_date(arm.timestamp)) between to_date('''
      || V_DateFrom
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_DateTo
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT))) order by arm.timestamp desc) WHERE rownum <=500';
   ELSIF(P_CHKBOXVAL = 'OFF') THEN
   V_SQLStr :=
      ' SELECT * FROM (SELECT 
            acm.COLUMN_DISPLAY_NAME,
            arm.table_name||'', ''||acm.column_name as DBLOCATION,
           to_char(arm.timestamp,''mm/dd/yyyy:hh:mi:ss'' ),
           (select usr_firstname||'' ''||usr_lastname from eres.er_user where pk_user = arm.user_id) username,
           arm.user_id userid,
           CASE(arm.Action)
           WHEN ''I'' THEN ''INSERT''
           WHEN ''D'' THEN ''DELETE''
           WHEN ''U'' THEN ''UPDATE''
           END,
           ARM.TABLE_DISPLAY_NAME,
           NVL(acm.old_value,'''') AS old_value,
           NVL(acm.new_value,'''') AS new_value,
           pk_budget,
           budget_name,
           ACM.PK_COLUMN_ID,
           fk_codelst_reason,
           to_char(remarks)
        FROM esch.audit_row_module arm,
            esch.sch_budget sb,
            esch.audit_column_module acm
        WHERE sb.pk_budget = arm.module_id AND arm.pk_row_id = acm.fk_row_id and sb.pk_budget ='
      || P_BudgetID
      || ' and (acm.old_value is not null or acm.new_value is not null) 
            and acm.COLUMN_DISPLAY_NAME is not null
        and arm.TABLE_DISPLAY_NAME is not null
        and (((to_date(arm.timestamp)) between to_date('''
      || V_MinDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_MaxDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)) '
      || P_SELECT
      || ' ((to_date(arm.timestamp)) between to_date('''
      || V_DateFrom
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_DateTo
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT))) order by arm.timestamp desc) WHERE rownum <=500';
    END IF;
    OPEN V_Cursor FOR V_SQLStr;
    LOOP
        FETCH V_Cursor
            INTO
                V_FieldName,
                V_DBLocation, 
                V_ModifiedOn, 
                V_ModifiedBy, 
                V_ModifiedById, 
                V_Action, 
                V_ChangeLoc, 
                V_OldValue, 
                V_NewValue, 
                V_PkBudget,
                V_BudgetName, 
                V_PkColumnID, 
                V_FkCodelstReason, 
                V_Remarks;
        EXIT WHEN V_Cursor%NOTFOUND;
    BEGIN
        IF V_FkCodelstReason IS NULL
        THEN
           V_FkCodelstReason := 0;
        END IF;
        V_RowValue :=
            '<row  V_FieldName = "'
            || V_FieldName
            || '" V_DBLocation = "'
            || V_DBLocation
            || '" V_ModifiedOn = "'
            || V_ModifiedOn
            || '" V_ModifiedBy = "'
            || V_ModifiedBy
            || '" V_ModifiedById = "'
            || V_ModifiedById
            || '" V_Action = "'
            || V_Action
            || '" V_ChangeLoc = "'
            || V_ChangeLoc
            || '" V_OldValue = "'
            || V_OldValue
            || '" V_NewValue = "'
            || V_NewValue
            || '" V_PkBudget = "'
            || V_PkBudget
            || '" V_BudgetName = "'
            || V_BudgetName
            || '" V_PkColumnID = "'
            || V_PkColumnID
            || '" V_FkCodelstReason = "'
            || V_FkCodelstReason
            || '" V_Remarks = "'
            || V_Remarks
            || '" V_ReasonDropDown = "'
            || V_ReasonDropDown
            || '" V_Select = "'
            || P_Select
            || '" V_DateFrom = "'
            || P_DATEFROM
            || '" V_DateTo = "'
            || P_DATETO
            || '" V_Stat = "'
            || P_STATUSTEXT
            || '" V_DateFilter = "'
            || P_DATEFILTER
            || '" V_CheckBox = "'
            || P_CHKBOXVAL
            || '" />';
        V_RowSet := V_RowSet || V_RowValue;
    END;
    END LOOP;
    CLOSE V_Cursor;
    V_Result := '<ROWSET>' || V_RowSet || '</ROWSET>';
    RETURN V_Result;
    EXCEPTION
       WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'','','Error In FUNCTION RPT_BUDGETAUDITREPORT ');
END RPT_BUDGETAUDITREPORT;

-- Report function for Calendar Audit Trail Report
FUNCTION RPT_CALENDARAUDITREPORT (
   P_EVENTID      NUMBER,
   P_STATUS1       VARCHAR2,
   P_STATUS2       VARCHAR2,
   P_STATUSTEXT    VARCHAR2,
   P_SELECT        VARCHAR2,
   P_DATEFROM      VARCHAR2,
   P_DATETO        VARCHAR2,
   P_DATEFILTER    VARCHAR2,
   P_CHKBOXVAL     VARCHAR2
)
   RETURN CLOB
AS
   V_Cursor            Types.cursortype;
   V_AuditReason       Types.cursortype;
   V_SQLStr            VARCHAR2 (4000);
   V_ReasonId          NUMBER;
   V_ReasonName        VARCHAR2 (100);
   V_Temp              VARCHAR2 (200);
   V_ReasonDropDown    VARCHAR2 (1000);
   V_DBLocation        VARCHAR2 (200);
   V_FieldName         VARCHAR2 (50);
   V_Action            VARCHAR2 (50);
   V_ModifiedOn        VARCHAR2 (100);
   V_ModifiedBy        VARCHAR2 (100);
   V_ModifiedById      NUMBER;
   V_ChangeLoc         VARCHAR2 (2000);
   V_OldValue          VARCHAR2 (4000);
   V_NewValue          VARCHAR2 (4000);
   V_EventId           NUMBER;
   V_Name              VARCHAR2 (100);
   V_PkColumnID        NUMBER;
   V_FkCodelstReason   NUMBER;
   V_Remarks           VARCHAR2 (4000);
   --   variable created on 19.1.2012
   V_Status1Sql        VARCHAR2 (4000);
   V_Status2Sql        VARCHAR2 (4000);
   V_Status1           VARCHAR2 (100);
   V_Status2           VARCHAR2 (100);
   V_MinDateSql        VARCHAR2 (4000);
   V_MaxDateSql        VARCHAR2 (4000);
   V_MinDate           VARCHAR2 (100);
   V_MaxDate           VARCHAR2 (100);
   V_DateFrom          VARCHAR2 (100);
   V_DateTo            VARCHAR2 (100);
   V_Select            VARCHAR2 (100);   
 --CLOB Variable to generate the XML
   V_RowValue          CLOB;
   V_RowSet            CLOB;
   V_Result            CLOB;   
   -- variable dated 20.03.2012
   V_TableName VARCHAR2(100);
   V_TablesNameSql varchar2(2000);
   V_CalId NUMBER(10);
   --Varibles For Error Log  dated 20.03.2012
   V_ErrorType VARCHAR2(20):='EXCEPTION';
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    BEGIN
        V_TablesNameSql := 'select Count(*) from ESCH.EVENT_ASSOC where event_id ='||P_EVENTID;
        EXECUTE IMMEDIATE V_TablesNameSql INTO V_CalId;
        IF(V_CalId = 1) THEN 
         V_TableName := 'ESCH.EVENT_ASSOC';
        ELSIF(V_CalId = 0 ) THEN 
         V_TableName := 'ESCH.EVENT_DEF';
        END IF;
    END;
    V_DateFrom := P_DATEFROM;
    V_DateTo := P_DATETO;  
    V_Status1Sql :=
      'select trim(codelst_desc) from esch.sch_codelst where pk_codelst = '
      || P_STATUS1;
    EXECUTE IMMEDIATE V_Status1Sql INTO   V_Status1;
    V_Status2Sql :=
      'select trim(codelst_desc) from esch.sch_codelst where pk_codelst = '
      || P_STATUS2;
    EXECUTE IMMEDIATE V_Status2Sql INTO   V_Status2;
    V_MinDateSql :=
      'select MIN(to_char(arm.timestamp,PKG_DATEUTIL.F_GET_DATEFORMAT)) from esch.audit_row_module arm,esch.audit_column_module acm where arm.pk_row_id=ACM.FK_ROW_ID and ARM.MODULE_ID='
      || P_EVENTID
      || '  and acm.column_name=''FK_CODELST_CALSTAT'' and acm.new_value='
      || ''''
      || V_Status1
      || '''';   
    EXECUTE IMMEDIATE V_MinDateSql INTO   V_MinDate;
    V_MaxDateSql :=
      'select MAX(to_char(arm.timestamp,PKG_DATEUTIL.F_GET_DATEFORMAT)) from esch.audit_row_module arm,esch.audit_column_module acm where arm.pk_row_id=ACM.FK_ROW_ID and ARM.MODULE_ID='
      || P_EVENTID
      || '  and acm.column_name=''FK_CODELST_CALSTAT'' and acm.new_value='
      || ''''
      || V_Status2
      || '''';
    EXECUTE IMMEDIATE V_MaxDateSql INTO   V_MaxDate;   
    V_ReasonDropDown := '0|Select an Option;';
    --Cursor to get the Reason values from the Codelst Table
    V_SQLStr :=
    'select pk_codelst,codelst_desc from esch.sch_codelst where codelst_type = ''audit_reason''';
    OPEN V_AuditReason FOR V_SQLStr;
    LOOP
        FETCH V_AuditReason INTO   V_ReasonId, V_ReasonName;
        EXIT WHEN V_AuditReason%NOTFOUND;
        BEGIN
            V_Temp := V_ReasonId || '|' || V_ReasonName;
            V_ReasonDropDown := V_ReasonDropDown || V_Temp || ';';
        END;
    END LOOP;
    V_ReasonDropDown :=
      SUBSTR (V_ReasonDropDown, 1, LENGTH (V_ReasonDropDown) - 1);   
    IF (P_CHKBOXVAL = 'ON') THEN
    V_SQLStr :=
      ' SELECT * FROM (SELECT 
            acm.COLUMN_DISPLAY_NAME,
            arm.table_name||'', ''||acm.column_name as DBLOCATION,
           to_char(arm.timestamp,''mm/dd/yyyy:hh:mi:ss''),
           (select usr_firstname||'' ''||usr_lastname from eres.er_user where pk_user = arm.user_id) username,
           arm.user_id userid,
           CASE(arm.Action)
           WHEN ''I'' THEN ''INSERT''
           WHEN ''D'' THEN ''DELETE''
           WHEN ''U'' THEN ''UPDATE''
           END,
           ARM.TABLE_DISPLAY_NAME,
           NVL(acm.old_value,'''') AS old_value,
           NVL(acm.new_value,'''') AS new_value,
           EVENT_ID,
           NAME,
           ACM.PK_COLUMN_ID,
           fk_codelst_reason,
           remarks
        FROM esch.audit_row_module arm,'
            ||V_TableName||' ea,
            esch.audit_column_module acm
        WHERE ea.EVENT_ID = arm.module_id AND arm.pk_row_id = acm.fk_row_id and ea.EVENT_ID ='
      || P_EVENTID
      || ' and (acm.old_value is not null or acm.new_value is not null)
        and (((to_date(arm.timestamp)) between to_date('''
      || V_MinDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_MaxDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)) '
      || P_SELECT
      || ' ((to_date(arm.timestamp)) between to_date('''
      || V_DateFrom
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_DateTo
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT))) order by arm.timestamp desc) WHERE rownum <=500';
    ELSIF(P_CHKBOXVAL = 'OFF') THEN
    V_SQLStr :=
      ' SELECT * FROM (SELECT 
            acm.COLUMN_DISPLAY_NAME,
            arm.table_name||'', ''||acm.column_name as DBLOCATION,
           to_char(arm.timestamp,''mm/dd/yyyy:hh:mi:ss''),
           (select usr_firstname||'' ''||usr_lastname from eres.er_user where pk_user = arm.user_id) username,
           arm.user_id userid,
           CASE(arm.Action)
           WHEN ''I'' THEN ''INSERT''
           WHEN ''D'' THEN ''DELETE''
           WHEN ''U'' THEN ''UPDATE''
           END,
           ARM.TABLE_DISPLAY_NAME,
           NVL(acm.old_value,'''') AS old_value,
           NVL(acm.new_value,'''') AS new_value,
           EVENT_ID,
           NAME,
           ACM.PK_COLUMN_ID,
           fk_codelst_reason,
           remarks
        FROM esch.audit_row_module arm,
            '||V_TableName||' ea,
            esch.audit_column_module acm
        WHERE ea.EVENT_ID = arm.module_id AND arm.pk_row_id = acm.fk_row_id and ea.EVENT_ID ='
      || P_EVENTID
      || ' and (acm.old_value is not null or acm.new_value is not null)
            and acm.COLUMN_DISPLAY_NAME is not null
        and arm.TABLE_DISPLAY_NAME is not null
        and (((to_date(arm.timestamp)) between to_date('''
      || V_MinDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_MaxDate
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)) '
      || P_SELECT
      || ' ((to_date(arm.timestamp)) between to_date('''
      || V_DateFrom
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT) and to_date('''
      || V_DateTo
      || ''',PKG_DATEUTIL.F_GET_DATEFORMAT))) order by arm.timestamp desc) WHERE rownum <=500';
    END IF;
    OPEN V_Cursor FOR V_SQLStr;
    LOOP
        FETCH V_Cursor
            INTO
                V_FieldName,
                V_DBLocation, 
                V_ModifiedOn, 
                V_ModifiedBy, 
                V_ModifiedById, 
                V_Action, 
                V_ChangeLoc, 
                V_OldValue, 
                V_NewValue, 
                V_EventId, 
                V_Name,  
                V_PkColumnID, 
                V_FkCodelstReason, 
                V_Remarks;
        EXIT WHEN V_Cursor%NOTFOUND;
    BEGIN
        IF V_FkCodelstReason IS NULL
        THEN
        V_FkCodelstReason := 0;
        END IF;
        V_RowValue := '<row  V_FieldName = "'
            || V_FieldName
            || '" V_DBLocation = "'
            || V_DBLocation
            || '" V_ModifiedOn = "'
            || V_ModifiedOn
            || '" V_ModifiedBy = "'
            || V_ModifiedBy
            || '" V_ModifiedById = "'
            || V_ModifiedById
            || '" V_Action = "'
            || V_Action
            || '" V_ChangeLoc = "'
            || V_ChangeLoc
            || '" V_OldValue = "'
            || V_OldValue
            || '" V_NewValue = "'
            || V_NewValue
            || '" V_EventId = "'
            || V_EventId
            || '" V_Name = "'
            || V_Name
            || '" V_PkColumnID = "'
            || V_PkColumnID
            || '" V_FkCodelstReason = "'
            || V_FkCodelstReason
            || '" V_Remarks = "'
            || V_Remarks
            || '" V_ReasonDropDown = "'
            || V_ReasonDropDown
            || '" V_Select = "'
            || P_Select
            || '" V_DateFrom = "'
            || P_DATEFROM
            || '" V_DateTo = "'
            || P_DATETO
            || '" V_Stat = "'
            || P_STATUSTEXT
            || '" V_DateFilter = "'
            || P_DATEFILTER
            || '" V_CheckBox = "'
            || P_CHKBOXVAL
            || '" />';         
        V_RowSet := V_RowSet || V_RowValue;
    END;
   END LOOP;
   CLOSE V_Cursor;
   V_Result := '<ROWSET>' || V_RowSet || '</ROWSET>';
   RETURN V_Result;
    EXCEPTION
       WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'','','Error In FUNCTION RPT_CALENDARAUDITREPORT ');
END RPT_CALENDARAUDITREPORT;
END PKG_AUDIT_TRAIL_MODULE;
/

GRANT EXECUTE, DEBUG ON PKG_AUDIT_TRAIL_MODULE TO ERES;
