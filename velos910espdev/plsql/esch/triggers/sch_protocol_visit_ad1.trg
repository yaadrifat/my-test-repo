create or replace TRIGGER esch."SCH_PROTOCOL_VISIT_AD1" AFTER DELETE ON SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
declare
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AD1', pLEVEL  => Plog.LFATAL);
 begin

 --Delete the event from calendar's default budget
 begin
     --KM-#5949
     --DELETE FROM SCH_SUBCOST_ITEM_VISIT WHERE FK_PROTOCOL_VISIT = :old.pk_protocol_visit;

     delete from sch_lineitem l
     where l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 )
             );

     delete from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 );

 exception when others then
    plog.fatal(pctx,'Exception:'||sqlerrm);
 end;
end;
/


