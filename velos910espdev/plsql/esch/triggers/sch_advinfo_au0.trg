CREATE OR REPLACE TRIGGER SCH_ADVINFO_AU0
  AFTER UPDATE OF
  pk_advinfo,
  fk_adverse,
  fk_codelst_info,
  advinfo_value,
  advinfo_type,
  creator,
  created_on,
  ip_add,
  rid
  ON sch_advinfo
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVINFO', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_advinfo,0) !=
     NVL(:NEW.pk_advinfo,0) THEN
     audit_trail.column_update
       (raid, 'PK_ADVINFO',
       :OLD.pk_advinfo, :NEW.pk_advinfo);
  END IF;
  IF NVL(:OLD.fk_adverse,0) !=
     NVL(:NEW.fk_adverse,0) THEN
     audit_trail.column_update
       (raid, 'FK_ADVERSE',
       :OLD.fk_adverse, :NEW.fk_adverse);
  END IF;
  IF NVL(:OLD.fk_codelst_info,0) !=
     NVL(:NEW.fk_codelst_info,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_INFO',
       :OLD.fk_codelst_info, :NEW.fk_codelst_info);
  END IF;
  IF NVL(:OLD.advinfo_value,0) !=
     NVL(:NEW.advinfo_value,0) THEN
     audit_trail.column_update
       (raid, 'ADVINFO_VALUE',
       :OLD.advinfo_value, :NEW.advinfo_value);
  END IF;
  IF NVL(:OLD.advinfo_type,' ') !=
     NVL(:NEW.advinfo_type,' ') THEN
     audit_trail.column_update
       (raid, 'ADVINFO_TYPE',
       :OLD.advinfo_type, :NEW.advinfo_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
      to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/


