CREATE OR REPLACE TRIGGER "ESCH"."SCH_BUDGET_AI0" 
AFTER INSERT
ON SCH_BUDGET REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
rt_str VARCHAR2(10);
rt_count NUMBER(10);
BEGIN
SELECT COUNT(*)
INTO rt_count
FROM er_ctrltab
WHERE CTRL_KEY = 'bgt_rights';
FOR i IN 0 .. rt_count - 1
LOOP
rt_str:= rt_str || '7';
END LOOP;

	INSERT INTO SCH_BGTUSERS (
			PK_BGTUSERS,
			FK_BUDGET,
			FK_USER,
			BGTUSERS_RIGHTS,
			BGTUSERS_TYPE,
			CREATOR,
			CREATED_ON,
			IP_ADD)
	VALUES( SEQ_SCH_BGTUSERS.NEXTVAL,
			:NEW.PK_BUDGET,
			:NEW.BUDGET_CREATOR,
			rt_str,
			'I',
			:NEW.CREATOR,
			SYSDATE,
			:NEW.IP_ADD);
	IF(:NEW.BUDGET_CREATOR <> :NEW.CREATOR ) THEN
			INSERT INTO SCH_BGTUSERS (
			PK_BGTUSERS,
			FK_BUDGET,
			FK_USER,
			BGTUSERS_RIGHTS,
			BGTUSERS_TYPE,
			CREATOR,
			CREATED_ON,
			IP_ADD)
	VALUES( SEQ_SCH_BGTUSERS.NEXTVAL,
			 :NEW.PK_BUDGET,
			:NEW.CREATOR,
			rt_str,
			'I',
			:NEW.CREATOR,
			SYSDATE,
			:NEW.IP_ADD);
	END IF;
		
	IF NVL(:NEW.BUDGET_COMBFLAG, 'Z')='Y' THEN
		BEGIN
			IF (:NEW.budget_rightscope = 'S') THEN  /* add access for all users of budget's study*/
		        pkg_bgtusers.sp_addteam_users(:NEW.PK_BUDGET,:NEW.BUDGET_RIGHTS,'G',:NEW.FK_STUDY,:NEW.CREATOR, :NEW.CREATED_ON, :NEW.IP_ADD );
			END IF;
		END;
	END IF;
		--add bgt section
/*
if :new.budget_type = 'S' then
  -- insert dummy record in budget cal for study budget

  INSERT INTO SCH_BGTCAL(PK_BGTCAL,
  FK_BUDGET,
  BGTCAL_PROTID ,
  BGTCAL_PROTTYPE,
  BGTCAL_DELFLAG,
  CREATOR,
  IP_ADD)
  VALUES  (SEQ_SCH_BGTCAL.nextval,:new.PK_BUDGET,
  null,null,'N', :new.creator, :new.IP_ADD );
  pkg_bgt.sp_study_bgtsection (
  :new.PK_BUDGET,
  :new.creator,
  :new.IP_ADD ) ;

end if; */
END;
/


