CREATE OR REPLACE TRIGGER "ESCH"."EVENT_DEF_AD0" AFTER DELETE ON EVENT_DEF
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob; --KM

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'EVENT_DEF', :old.rid, 'D');

  deleted_data :=
   to_char(:old.event_id) || '|' ||
   to_char(:old.chain_id) || '|' ||
   :old.event_type || '|' ||
   :old.name || '|' ||
   :old.notes || '|' ||
   to_char(:old.cost) || '|' ||
   :old.cost_description || '|' ||
   to_char(:old.duration) || '|' ||
   to_char(:old.user_id) || '|' ||
   :old.linked_uri || '|' ||
   :old.fuzzy_period || '|' ||
   :old.msg_to || '|' ||
   --:old.status || '|' ||
   :old.description || '|' ||
   to_char(:old.displacement) || '|' ||
   to_char(:old.org_id) || '|' ||
   :old.event_msg || '|' ||
   :old.event_res || '|' ||
   to_char(:old.created_on) || '|' ||
   to_char(:old.event_flag) || '|' ||
   to_char(:old.pat_daysbefore) || '|' ||
   to_char(:old.pat_daysafter) || '|' ||
   to_char(:old.usr_daysbefore) || '|' ||
   to_char(:old.usr_daysafter) || '|' ||
   :old.pat_msgbefore || '|' ||
   :old.pat_msgafter || '|' ||
   :old.usr_msgbefore || '|' ||
   :old.usr_msgafter || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   :old.ip_add|| '|' ||
   :old.calendar_sharedwith||'|'||
   :old.duration_unit||'|'||
   :old.fk_visit||'|'||
--JM: 02/15/2007
   :old.event_fuzzyafter|| '|' ||
   :old.event_durationafter|| '|' ||
   :old.event_cptcode|| '|' ||
   :old.event_durationbefore|| '|' ||
   to_char(:old.fk_catlib) || '|' ||
   --KM-Mar28,08
   :old.event_category || '|' ||
   to_char(:old.EVENT_LIBRARY_TYPE) || '|' ||
   to_char(:old.EVENT_LINE_CATEGORY) || '|' || --KM: 07Aug09
   to_char(:old.SERVICE_SITE_ID) ||'|'||
   to_char(:old.FACILITY_ID) ||'|'||
   to_char(:old.FK_CODELST_COVERTYPE)||'|'||  --BK 23rd july 2010
   to_char(:old.COVERAGE_NOTES)||'|'||
   to_char(:old.FK_CODELST_CALSTAT);

insert into audit_delete
(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));

delete from er_moredetails where fk_modPK = :old.event_id;--AK 10th March 2011
  
end;
/