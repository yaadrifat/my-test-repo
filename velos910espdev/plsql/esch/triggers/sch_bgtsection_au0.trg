CREATE OR REPLACE TRIGGER SCH_BGTSECTION_AU0
AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_BGTCAL,CREATED_ON,PK_BUDGETSEC,BGTSECTION_NAME,BGTSECTION_TYPE,BGTSECTION_PATNO,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,BGTSECTION_PERSONLFLAG,FK_VISIT
ON SCH_BGTSECTION REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BGTSECTION', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_budgetsec,0) !=
     NVL(:NEW.pk_budgetsec,0) THEN
     audit_trail.column_update
       (raid, 'PK_BUDGETSEC',
       :OLD.pk_budgetsec, :NEW.pk_budgetsec);
  END IF;
  IF NVL(:OLD.fk_bgtcal,0) !=
     NVL(:NEW.fk_bgtcal,0) THEN
     audit_trail.column_update
       (raid, 'FK_BGTCAL',
       :OLD.fk_bgtcal, :NEW.fk_bgtcal);
  END IF;
  IF NVL(:OLD.bgtsection_name,' ') !=
     NVL(:NEW.bgtsection_name,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_NAME',
       :OLD.bgtsection_name, :NEW.bgtsection_name);
  END IF;
  IF NVL(:OLD.bgtsection_visit,' ') !=
     NVL(:NEW.bgtsection_visit,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_VISIT',
       :OLD.bgtsection_visit, :NEW.bgtsection_visit);
  END IF;
  IF NVL(:OLD.bgtsection_delflag,' ') !=
     NVL(:NEW.bgtsection_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_DELFLAG',
       :OLD.bgtsection_delflag, :NEW.bgtsection_delflag);
  END IF;
  IF NVL(:OLD.bgtsection_sequence,0) !=
     NVL(:NEW.bgtsection_sequence,0) THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_SEQUENCE',
       :OLD.bgtsection_sequence, :NEW.bgtsection_sequence);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.bgtsection_type,' ') !=
     NVL(:NEW.bgtsection_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_TYPE',
       :OLD.bgtsection_type, :NEW.bgtsection_type);
  END IF;
  IF NVL(:OLD.bgtsection_patno, ' ') !=
     NVL(:NEW.bgtsection_patno,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_PATNO',
       :OLD.bgtsection_patno, :NEW.bgtsection_patno);
  END IF;
  IF NVL(:OLD.bgtsection_notes, ' ') !=
     NVL(:NEW.bgtsection_notes,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_NOTES',
       :OLD.bgtsection_notes, :NEW.bgtsection_notes);
  END IF;

  IF NVL(:OLD.bgtsection_personlflag,0) !=
     NVL(:NEW.bgtsection_personlflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_PERSONLFLAG',
       :OLD.bgtsection_personlflag, :NEW.bgtsection_personlflag);
  END IF;
  IF NVL(:OLD.srt_cost_total,' ') !=
     NVL(:NEW.srt_cost_total,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_TOTAL',
       :OLD.srt_cost_total, :NEW.srt_cost_total);
  END IF;
  IF NVL(:OLD.srt_cost_grandtotal,' ') !=
     NVL(:NEW.srt_cost_grandtotal,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_GRANDTOTAL',
       :OLD.srt_cost_grandtotal, :NEW.srt_cost_grandtotal);
  END IF;
  IF NVL(:OLD.soc_cost_total,' ') !=
     NVL(:NEW.soc_cost_total,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_TOTAL',
       :OLD.soc_cost_total, :NEW.soc_cost_total);
  END IF;
  IF NVL(:OLD.soc_cost_grandtotal,' ') !=
     NVL(:NEW.soc_cost_grandtotal,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_GRANDTOTAL',
       :OLD.soc_cost_grandtotal, :NEW.soc_cost_grandtotal);
  END IF;
  IF NVL(:OLD.srt_cost_sponsor,' ') !=
     NVL(:NEW.srt_cost_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_SPONSOR',
       :OLD.srt_cost_sponsor, :NEW.srt_cost_sponsor);
  END IF;
  IF NVL(:OLD.srt_cost_variance,' ') !=
     NVL(:NEW.srt_cost_variance,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_VARIANCE',
       :OLD.srt_cost_variance, :NEW.srt_cost_variance);
  END IF;
  IF NVL(:OLD.soc_cost_sponsor,' ') !=
     NVL(:NEW.soc_cost_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_SPONSOR',
       :OLD.soc_cost_sponsor, :NEW.soc_cost_sponsor);
  END IF;
  IF NVL(:OLD.soc_cost_variance,' ') !=
     NVL(:NEW.soc_cost_variance,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_VARIANCE',
       :OLD.soc_cost_variance, :NEW.soc_cost_variance);
  END IF;

    IF NVL(:OLD.FK_VISIT,0) !=
     NVL(:NEW.FK_VISIT,0) THEN
     audit_trail.column_update
       (raid, 'FK_VISIT',
       :OLD.FK_VISIT, :NEW.FK_VISIT);
  END IF;


END;
/


