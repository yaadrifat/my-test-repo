CREATE OR REPLACE TRIGGER ESCH."SCH_EVENTCOST_AU1"
AFTER UPDATE
ON ESCH.SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_EVENTCOST_AU1', pLEVEL  => Plog.LFATAL);
  v_soc_costpk number;
  v_apply_indirects number;

BEGIN

 BEGIN

     update sch_lineitem l
     set LINEITEM_SPONSORUNIT = :new.eventcost_value,
     last_modified_by = :new.creator, last_modified_date = sysdate, 
     fk_codelst_cost_type = :new.fk_cost_desc,
     lineitem_othercost = :new.eventcost_value * lineitem_clinicnofunit
     where fk_event = :old.fk_event and fk_codelst_cost_type = :old.fk_cost_desc 
     and lineitem_sponsorunit = :old.eventcost_value
     and exists ( select * from sch_bgtsection s , sch_bgtcal l,sch_budget b where 
                 l.fk_bgtsection = s.pk_budgetsec and l.pk_bgtcal = s.fk_bgtcal and b.pk_budget = l.fk_budget
                 and b.budget_calendar is not null);

  exception when others then
    plog.fatal(pCTX,'Exception:'||sqlerrm);
 END;

END;
/

