CREATE OR REPLACE TRIGGER "SCH_CRFLIB_AD0" 
AFTER DELETE
ON SCH_CRFLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_CRFLIB', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_crflib) || '|' ||
   to_char(:old.fk_events) || '|' ||
   :old.crflib_number || '|' ||
   :old.crflib_name || '|' ||
   :old.crflib_flag || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


