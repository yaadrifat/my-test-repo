CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_BI" BEFORE INSERT ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
erid NUMBER(10);
usr VARCHAR(2000);
raid NUMBER(10);
insert_data CLOB;

BEGIN

 BEGIN
  usr := getuser(:NEW.creator);
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'SCH_PORTAL_FORMS',erid, 'I',usr);
insert_data:=:NEW.PK_PF ||'|'|| :NEW.FK_PORTAL ||'|'|| :NEW.FK_CALENDAR ||'|'||
:NEW.FK_EVENT ||'|'|| :NEW.FK_FORM
||'|'|| :NEW.RID ||'|'|| :NEW.CREATOR
||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'|| :NEW.IP_ADD;
INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

END;

/