CREATE OR REPLACE TRIGGER "SCH_BGTCAL_AD0"
AFTER DELETE
ON SCH_BGTCAL
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BGTCAL', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_bgtcal) || '|' ||
  TO_CHAR(:OLD.fk_budget) || '|' ||
  TO_CHAR(:OLD.bgtcal_protid) || '|' ||
  :OLD.bgtcal_prottype || '|' ||
  :OLD.bgtcal_delflag || '|' ||
  :OLD.bgtcal_sponsorflag || '|' ||
  :OLD.bgtcal_clinicflag || '|' ||
  TO_CHAR(:OLD.bgtcal_sponsorohead) || '|' ||
  TO_CHAR(:OLD.bgtcal_clinicohead) || '|' ||
  TO_CHAR(:OLD.bgtcal_indirectcost) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  TO_CHAR(:OLD.bgtcal_frgbenefit) || '|' ||
  TO_CHAR(:OLD.bgtcal_frgflag) || '|' ||
  TO_CHAR(:OLD.bgtcal_discount) || '|' ||
  TO_CHAR(:OLD.bgtcal_discountflag) || '|' ||
  TO_CHAR(:OLD.bgtcal_excldsocflag) || '|' ||
  :OLD.grand_res_cost || '|' ||
  :OLD.grand_res_totalcost || '|' ||
  :OLD.grand_soc_cost || '|' ||
  :OLD.grand_soc_totalcost || '|' ||
  :OLD.grand_total_cost || '|' ||
  :OLD.grand_total_totalcost || '|' ||
  :OLD.grand_salary_cost || '|' ||
  :OLD.grand_salary_totalcost || '|' ||
  :OLD.grand_fringe_cost || '|' ||
  :OLD.grand_fringe_totalcost || '|' ||
  :OLD.grand_disc_cost || '|' ||
  :OLD.grand_disc_totalcost || '|' ||
  :OLD.grand_ind_cost || '|' ||
  :OLD.grand_ind_totalcost || '|' ||
  :OLD.grand_res_sponsor || '|' ||
  :OLD.grand_res_variance || '|' ||
  :OLD.grand_soc_sponsor || '|' ||
  :OLD.grand_soc_variance || '|' ||
  :OLD.grand_total_sponsor || '|' ||
  :OLD.grand_total_variance || '|' || :new.bgtcal_sp_overhead
      || '|' || :new.bgtcal_sp_overhead_flag;



INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END;
/


