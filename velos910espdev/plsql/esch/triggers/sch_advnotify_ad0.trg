CREATE OR REPLACE TRIGGER "SCH_ADVNOTIFY_AD0" 
  after delete
  on sch_advnotify
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVNOTIFY', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_advnotify) || '|' ||
  to_char(:old.fk_adverseve) || '|' ||
  to_char(:old.fk_codelst_not_type) || '|' ||
  to_char(:old.advnotify_date) || '|' ||
  :old.advnotify_notes || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  to_char(:old.advnotify_value);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


