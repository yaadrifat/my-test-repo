CREATE OR REPLACE TRIGGER "PERSON_AD0" 
AFTER DELETE
ON PERSON REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data CLOB;
 usr VARCHAR(2000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:OLD.last_modified_by);


  Audit_Trail.record_transaction
    (raid, 'PERSON', :OLD.rid, 'D',usr);

  deleted_data :=
   TO_CHAR(:OLD.pk_person) || '|' ||
   :OLD.person_code || '|' ||
   :OLD.person_altid || '|' ||
   :OLD.person_lname || '|' ||
   :OLD.person_fname || '|' ||
   :OLD.person_mname || '|' ||
   :OLD.person_suffix || '|' ||
   :OLD.person_prefix || '|' ||
   :OLD.person_degree || '|' ||
   TO_CHAR(:OLD.person_dob) || '|' ||
   TO_CHAR(:OLD.fk_codelst_gender) || '|' ||
   :OLD.person_aka || '|' ||
   TO_CHAR(:OLD.fk_codelst_race) || '|' ||
   TO_CHAR(:OLD.fk_codelst_prilang) || '|' ||
   TO_CHAR(:OLD.fk_codelst_marital) || '|' ||
   TO_CHAR(:OLD.fk_account) || '|' ||
   :OLD.person_ssn || '|' ||
   :OLD.person_driv_lic || '|' ||
   TO_CHAR(:OLD.person_ethgrp) || '|' ||
   :OLD.person_birth_place || '|' ||
   TO_CHAR(:OLD.person_multi_birth) || '|' ||
   TO_CHAR(:OLD.fk_codelst_citizen) || '|' ||
   :OLD.person_milvet || '|' ||
   TO_CHAR(:OLD.fk_codelst_nationality) || '|' ||
   TO_CHAR(:OLD.person_deathdt) || '|' ||
   TO_CHAR(:OLD.fk_site) || '|' ||
   TO_CHAR(:OLD.created_on) || '|' ||
   TO_CHAR(:OLD.fk_codelst_employment) || '|' ||
   TO_CHAR(:OLD.fk_codelst_edu) || '|' ||
   :OLD.person_notes || '|' ||
   TO_CHAR(:OLD.fk_codelst_pstat) || '|' ||
   TO_CHAR(:OLD.fk_codelst_bloodgrp) || '|' ||
   :OLD.person_address1 || '|' ||
   :OLD.person_address2 || '|' ||
   :OLD.person_city || '|' ||
   :OLD.person_state || '|' ||
   :OLD.person_zip || '|' ||
   :OLD.person_country || '|' ||
   :OLD.person_hphone || '|' ||
   :OLD.person_bphone || '|' ||
   :OLD.person_email || '|' ||
   :OLD.person_county || '|' ||
   TO_CHAR(:OLD.person_regdate) || '|' ||
   TO_CHAR(:OLD.person_regby) || '|' ||
   TO_CHAR(:OLD.fk_codlst_ntype) || '|' ||
   :OLD.person_mother_name || '|' ||
   TO_CHAR(:OLD.fk_person_mother) || '|' ||
   TO_CHAR(:OLD.fk_codelst_religion) || '|' ||
   TO_CHAR(:OLD.rid) || '|' ||
   TO_CHAR(:OLD.creator) || '|' ||
   TO_CHAR(:OLD.last_modified_by) || '|' ||
   TO_CHAR(:OLD.last_modified_date) || '|' ||
   :OLD.ip_add || '|' ||
   TO_CHAR(:OLD.fk_timezone) || '|' ||
   :OLD.person_phyother||'|'||
   :OLD.person_orgother||'|'||
   :OLD.person_splaccess||'|'||
   TO_CHAR(:OLD.fk_codelst_ethnicity)||'|'||
   :OLD.person_add_race||'|'||
   :OLD.PERSON_add_ethnicity||'|'||
   TO_CHAR(:OLD.fk_codelst_pat_dth_cause) || '|' ||
   :OLD.cause_of_death_other ||'|'||
   :OLD.pat_facilityid;

   --commented by Gopu to fix the bugzilla Issue #2958
   --|| '|'||dbms_lob.SUBSTR(:OLD.person_notes_clob,4000);


INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, dbms_lob.SUBSTR(deleted_data,4000));
END;
/


