CREATE OR REPLACE PROCEDURE SP_DELETE_PORTAL(portalId in number)
is
begin
   delete from ER_STATUS_HISTORY where status_modpk = portalId;
   delete from ER_PORTAL_POPLEVEL where fk_portal=portalId;

--modified by Sonia Abrol, 7/23/07, delete records from logins as well as portal design

   --delete from portal logins

   delete from er_portal_logins where fk_portal = portalId;

   --delete from portal modules/portal design
   delete from er_portal_modules where fk_portal = portalId;

   delete from ER_PORTAL where pk_portal = portalId;

   
   --delete from SCH_PORTAL_FORMS table
   delete from SCH_PORTAL_FORMS where fk_portal = portalId;

end;
/