CREATE OR REPLACE PROCEDURE        "SP_FORM_RETRIEVE" (p_pk_form NUMBER,p_link_from CHAR, o_sql OUT VARCHAR2, o_cnt_sql OUT VARCHAR2 )
   AS
/****************************************************************************************************
   ** Procedure to get the data of forms to make the filled form browser based on the fields selected by the user for showing
   ** in browser. Data is extracted from XML of the respective table based on the location of linked form
   ** Author: Sonika Talwar 27th Aug 2003
   ** Input parameter: form id
   ** Input parameter: type of link form A-account, S-study, P-patient, according to this the appropriate table name is made dynamically
   ** Output parameter: Sql for extracting data from form xml
   ** Output parameter: Sql for getting total # of records for that form
   **/
   v_sysid varchar2(500);
   v_xmlsql varchar2(4000);
   v_sql varchar2(4000);
   v_cnt number;
   v_table_name varchar2(100);
   v_xml_colname varchar2(100);
   v_pk_colname varchar2(100);

   v_header_str varchar2(1000);

   v_header_str_val varchar2(4000);
   v_params varchar2(4000);
   v_paramstr varchar2(4000);

   v_cur_header  gk_cv_types.GenericCursorType;

   V_HEADER_ARRAY    TYPES.SMALL_STRING_ARRAY:= TYPES.SMALL_STRING_ARRAY ();

   v_header_count Number;
   v_pos Number;
   v_colname varchar2(100);

   BEGIN

   if (p_link_from = 'A') then
        v_table_name := 'er_acctforms e';
	   v_xml_colname := 'e.acctforms_xml';
	   v_pk_colname := 'pk_acctforms';
   elsif (p_link_from = 'S') or (p_link_from = 'SA') then
        v_table_name := 'er_studyforms e';
   	   v_xml_colname := 'e.studyforms_xml';
	   v_pk_colname := 'pk_studyforms';
   elsif (p_link_from = 'SP') or (p_link_from = 'PA')then
        v_table_name := 'er_patforms e';
	   v_xml_colname := 'e.patforms_xml';
	   v_pk_colname := 'pk_patforms';
   end if;

   v_cnt:=0;

   --get the system_id for the fields whose browser_flag is 1
   --these system ids would be used to make the XPATH for extracting data from XML

   /* for i in
     (select a.fk_field, d.fld_systemid
      from er_formfld a, er_formsec b,  er_formlib c,
	   er_fldlib d
      where a.fk_formsec = b.pk_formsec
      and b.fk_formlib = c.pk_formlib
      and d.pk_field = a.fk_field
      and c.pk_formlib = p_pk_form)
   loop
     v_cnt := v_cnt +1;
     v_sysid := i.fld_systemid;

      --make the sql dynamcailly using the system_id, table name and column name
	 if nvl(v_xmlsql,'0') = '0' then
        v_xmlsql := v_xmlsql || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@name'').getStringVal() header' || v_cnt ;
	 else
        v_xmlsql := v_xmlsql || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@name'').getStringVal() header' || v_cnt  ;
	 end if;

   END LOOP; */




   v_header_str := 'Select ' || v_xml_colname || '.extract(''/rowset/*/@name'').getStringVal()  from ' || v_table_name || ' where fk_formlib = ' || p_pk_form || ' and rownum < 2 ';

   -- execute and get data in result set

   execute immediate v_header_str  into v_header_str_val ;
   p(' v_xmlsql headers ' ||  v_header_str_val);
   p(' ************');

   v_header_count := 0;

   v_params := v_header_str_val;
   v_paramstr := v_header_str_val;

   LOOP
       V_POS := instr (v_paramstr , ',');
       V_params := substr (V_paramstr, 1, V_POS - 1);
        exit when V_PARAMS Is null;

 	    v_header_count := v_header_count + 1;
		V_HEADER_ARRAY.extend;
		V_HEADER_ARRAY(v_header_count) := V_PARAMS;

	    V_paramstr := substr (V_paramstr, V_POS + 1);
   END LOOP ;


  /* open v_cur_header for v_header_str;


    LOOP
        FETCH v_cur_header INTO v_right ;
              EXIT WHEN v_cur_header%NOTFOUND  ;
			  	  v_header_count := v_header_count + 1;
				  V_HEADER_ARRAY.extend;
				  V_HEADER_ARRAY(v_header_count) := i.

	END LOOP; */



   v_cnt := 0;
   v_xmlsql := '';





   for i in
     (select a.fk_field, d.fld_systemid
      from er_formfld a,
        er_formsec b,
	   er_formlib c,
	   er_fldlib d
      where a.fk_formsec = b.pk_formsec
      and b.fk_formlib = c.pk_formlib
      and d.pk_field = a.fk_field
      and c.pk_formlib = p_pk_form)
   loop
      v_cnt := v_cnt +1;

      v_sysid := i.fld_systemid;

	  v_colname := trim( V_HEADER_ARRAY(v_cnt - 1) ) ;

      --make the sql dynamcailly using the system_id, table name and column name
	 if nvl(v_xmlsql,'0') = '0' then
        -- v_xmlsql := v_xmlsql || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@name'').getStringVal() header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() col' || v_cnt ;
		v_xmlsql := v_xmlsql ||  v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() ' || v_colname ;
	 else
        --v_xmlsql := v_xmlsql || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@name'').getStringVal() header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() col' || v_cnt ;
		v_xmlsql := v_xmlsql || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() ' || v_colname ;
	 end if;
   end loop;

   v_sql := 'select ' || v_pk_colname || ', ' || v_xmlsql || ' from '|| v_table_name || ' where fk_formlib = ' || p_pk_form;


    p('sql ' || v_sql);
   o_sql := v_sql;
   o_cnt_sql := 'select count(*) from ' || v_table_name || ' where fk_formlib = ' || p_pk_form ;
       p('cnt sql ' || o_cnt_sql);

 END; --end of SP_FORM_DATA_BROWSER
/


CREATE SYNONYM ESCH.SP_FORM_RETRIEVE FOR SP_FORM_RETRIEVE;


CREATE SYNONYM EPAT.SP_FORM_RETRIEVE FOR SP_FORM_RETRIEVE;


GRANT EXECUTE, DEBUG ON SP_FORM_RETRIEVE TO EPAT;

GRANT EXECUTE, DEBUG ON SP_FORM_RETRIEVE TO ESCH;

