CREATE OR REPLACE PROCEDURE SP_DELETE_SPECIMENS(p_ids ARRAY_STRING,o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;
   v_id_str VARCHAR2(100);
   v_inStr long;     --KM
   v_temp VARCHAR2(100);
   v_sql long;
   v_sql1 long;
   v_sql2 long;
   v_sql3 long;
   v_childlist ARRAY_STRING := ARRAY_STRING();
   indx NUMBER :=1 ;

BEGIN

   v_cnt := p_ids.COUNT; --get the # of elements in array

   i:=1;
WHILE i <= v_cnt LOOP
   v_temp := to_number(p_ids(i));
   --Modified by Manimaran to delete the child specimens.
   for m in(select pk_specimen from er_specimen start with pk_specimen = v_temp connect by prior pk_specimen = fk_specimen)
   loop
      v_childlist.EXTEND;
      v_childlist(indx) := m.pk_specimen;
      indx := indx + 1;
   end loop;
   i := i+1;
END LOOP;

for n in 1..v_childlist.count
  loop
  IF (LENGTH(v_inStr) > 0) THEN
     v_inStr:=v_inStr||', '||v_childlist(n);
  ELSE
     v_inStr:= '('||v_childlist(n) ;
  END IF;
end loop;
v_inStr:=v_inStr||')' ;

BEGIN

      --JM: 10Jul2009: #INVP2.11: Labs module in Specimens
      v_sql3 := 'delete from er_patlabs where fk_specimen in ' || v_inStr;
      EXECUTE IMMEDIATE v_sql3;
      commit;

      v_sql2 := 'delete from er_specimen_appndx where fk_specimen in ' || v_inStr; --KM
      EXECUTE IMMEDIATE v_sql2;
      commit;

          v_sql1:= 'delete from er_specimen_status  where  fk_specimen in ' || v_inStr;
     EXECUTE IMMEDIATE v_sql1 ;
   commit;
          v_sql:='delete from er_specimen where pk_specimen in ' || v_inStr;
   EXECUTE IMMEDIATE v_sql ;
   COMMIT;
      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
        o_ret:=-1;
    RETURN;
      END ;--end of insert begin
   o_ret:=0;

 END;
/


