CREATE OR REPLACE PROCEDURE        "SP_STOPMAIL" (
 p_study IN NUMBER ,
 p_per IN NUMBER,
 p_dmlby NUMBER ,
 p_ipadd VARCHAR2 ,
 p_stat VARCHAR2 DEFAULT NULL)
AS
/**************************************************************************************************
   **
   ** Author: Charanjiv S Kalha 09/29/2001
   ** This procedure marks the mails to be sent to a user as deactivated/sent '-1' when the user is deactivated
   ** from a study. The user/person id and the study is is passed as a parameter to the proc.
   **
   ** Modification History
   **
   ** Modified By         Date             Remarks
   ** Charanjiv           12th Oct 2001    Removed the msg_type filter from the WHERE clause of the
   **                                      update SQL.
   ** Charanjiv           16th Oct 2001    The msg Status is now being updated to "-1"
   ** Charanjiv           22th Oct 2001    The procedure will now also work for reactivated patients
   **                                      for this a additional parameter has been added p_stat. A value of
   **                                      'active' means the patient is being activated for the study, Thus all
   **                                      messages which were in-active (-1) [if any, there will be none for new patients
   **                                      will be again updated to 0 again. A check is also made that the messages
   **                                      had to be sent on or after the current date
   **
****************************************************************************************************/
BEGIN
    DBMS_OUTPUT.PUT_LINE(p_stat) ;


 IF p_stat = 'active' THEN

    --changed for clubbed mail

  UPDATE sch_msgtran
     SET msg_status = 0
   WHERE fk_user = p_per
     AND fk_study = p_study
     AND msg_status = -1
	AND msg_sendon >= SYSDATE  ;
  ELSE
  UPDATE sch_msgtran
     SET msg_status = -1
   WHERE fk_user = p_per
     AND fk_study = p_study
     AND msg_status = 0 ;
  END IF ;
END ;
/


CREATE SYNONYM ESCH.SP_STOPMAIL FOR SP_STOPMAIL;


CREATE SYNONYM EPAT.SP_STOPMAIL FOR SP_STOPMAIL;


GRANT EXECUTE, DEBUG ON SP_STOPMAIL TO EPAT;

GRANT EXECUTE, DEBUG ON SP_STOPMAIL TO ESCH;

