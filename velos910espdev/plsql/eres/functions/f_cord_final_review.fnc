create or replace
function f_cord_final_review(pk_cord number) return SYS_REFCURSOR
IS
v_final_review SYS_REFCURSOR;
BEGIN
    OPEN V_FINAL_REVIEW FOR 
                   SELECT 
                   REVIEW.LICENSURE_CONFIRM_FLAG L_CONFIRMFLAG,
                   REVIEW.ELIGIBLE_CONFIRM_FLAG E_CONFIRMFLAG,
                   TO_CHAR(REVIEW.CREATED_ON,'Mon DD, YYYY') REVIEW_DT,
                   F_CODELST_DESC(REVIEW.REVIEW_CORD_ACCEPTABLE_FLAG) REVIEW_RESULT,
                   F_GETUSER(REVIEW.ELIG_CREATOR) USERNAME,
                   REVIEW.FINAL_REVIEW_CONFIRM_FLAG REVIEWFLAG
                   FROM 
                   CB_CORD_FINAL_REVIEW REVIEW 
                   WHERE 
                   REVIEW.FK_CORD_ID=PK_CORD;
RETURN V_FINAL_REVIEW;
END;
/