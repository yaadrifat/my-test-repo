create or replace
function f_cbu_history(cordid number) return SYS_REFCURSOR
IS
v_cord_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_HTY FOR      SELECT 
                             TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             CBUSTATUS.CBU_STATUS_DESC DESCRIPTION1,
                             f_getuser(STATUS.CREATOR) CREATOR,
                             '' ORDERTYPE,
                             STATUS.STATUS_TYPE_CODE TYPECODE,
                             TO_CHAR(SYSDATE,'Mon DD,YYYY') SYSTEMDATE,
                             ''  DESCRIPTION2
                             FROM 
                             CB_ENTITY_STATUS STATUS
                             LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS
                             ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS) 
                             WHERE (STATUS.ENTITY_ID=CORDID AND STATUS.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='CBU')) 
                             AND(STATUS.STATUS_TYPE_CODE='cord_status' OR STATUS.STATUS_TYPE_CODE='Cord_Nmdp_Status') 
                             UNION ALL
                             SELECT 
                             TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             '',
                             f_getuser(STAT.CREATOR),
                             NVL(F_CODELST_DESC(ORD.ORDER_TYPE),'-'),
                             STAT.STATUS_TYPE_CODE,
                             TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                             F_CODELST_DESC(STAT.FK_STATUS_VALUE) 
                             FROM 
                             CB_ENTITY_STATUS STAT  
                             LEFT OUTER JOIN ER_ORDER ORD
                             ON (ORD.PK_ORDER=STAT.ENTITY_ID)
                             WHERE  
                             STAT.ENTITY_ID IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=CORDID) 
                             AND STAT.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='ORDER') 
                             AND STAT.STATUS_TYPE_CODE='order_status' ORDER BY 1 DESC;
RETURN V_CORD_HTY;
END; 
/