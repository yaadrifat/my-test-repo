CREATE OR REPLACE FUNCTION        "F_GET_SCHVISIT" (p_pk_sch_events1 NUMBER ) RETURN VARCHAR IS

/*Function to get the event visit linked with a schedule record
*Author: Sonia Abrol, on 08/20/06
*/
v_visit_name VARCHAR2(100);

BEGIN

	 IF p_pk_sch_events1 > 0 THEN

		BEGIN

		 	SELECT visit_name
			INTO v_visit_name
			FROM sch_protocol_visit WHERE pk_protocol_visit IN (SELECT fk_visit FROM sch_events1 WHERE LPAD(event_id,10,0) = p_pk_sch_events1);

		EXCEPTION WHEN NO_DATA_FOUND THEN

			v_visit_name  := '';

		END;

	ELSE
		v_visit_name := '';
	END IF;

	RETURN v_visit_name ;
END   ;
/


