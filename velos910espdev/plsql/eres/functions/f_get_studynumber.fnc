CREATE OR REPLACE FUNCTION        "F_GET_STUDYNUMBER" (p_studyid number)
return varchar
is
v_studynumber varchar2(100);
Begin
	 select study_number into v_studynumber from er_study where pk_study = p_studyid;
  return v_studynumber ;
end ;
/


CREATE SYNONYM ESCH.F_GET_STUDYNUMBER FOR F_GET_STUDYNUMBER;


CREATE SYNONYM EPAT.F_GET_STUDYNUMBER FOR F_GET_STUDYNUMBER;


GRANT EXECUTE, DEBUG ON F_GET_STUDYNUMBER TO EPAT;

GRANT EXECUTE, DEBUG ON F_GET_STUDYNUMBER TO ESCH;

