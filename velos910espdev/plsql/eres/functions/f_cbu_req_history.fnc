create or replace
function f_cbu_req_history(cordid number,orderid number) return SYS_REFCURSOR
IS
v_cord_req_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_REQ_HTY FOR SELECT 
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        F_GETUSER(STAT.CREATOR) CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2
                        FROM 
                        CB_ENTITY_STATUS STAT 
                        LEFT OUTER JOIN 
                        CB_CBU_STATUS STATUS ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS) 
                        LEFT OUTER JOIN ER_ORDER ORD1 ON(STAT.ENTITY_ID=ORD1.PK_ORDER) 
                        WHERE  
                        (STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID =ORDERID) AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Stts' ) 
                        ORDER BY STAT.STATUS_DATE DESC;
RETURN V_CORD_REQ_HTY;
END;
/