CREATE OR REPLACE FUNCTION        "F_REP_STUDY_ENROLOG" (p_study NUMBER, p_patient NUMBER)
RETURN VARCHAR2
IS
v_retval VARCHAR2(250);
v_statdate DATE;
v_count NUMBER;
BEGIN
	 v_retval := '';

	 SELECT MAX(patstudystat_date) INTO v_statdate FROM ER_PATSTUDYSTAT WHERE fk_study = p_study AND fk_per = p_patient;
	 SELECT COUNT(*) INTO v_count FROM ER_PATSTUDYSTAT WHERE fk_study = p_study AND fk_per = p_patient AND patstudystat_date = v_statdate;
	 IF v_count = 1 THEN
	 	SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) INTO v_retval FROM ER_PATSTUDYSTAT WHERE fk_study = p_study AND fk_per = p_patient AND patstudystat_date = v_statdate;
	 ELSE
	 	SELECT codelst_desc INTO v_retval
		FROM ER_PATSTUDYSTAT, ER_CODELST
		WHERE fk_study = p_study AND fk_per = p_patient AND patstudystat_date = v_statdate AND pk_codelst = fk_codelst_stat AND
		codelst_seq = (SELECT MAX(a.codelst_seq) FROM ER_CODELST a WHERE pk_codelst IN
					  		  					 (SELECT fk_codelst_stat FROM ER_PATSTUDYSTAT b WHERE b.fk_study = p_study AND b.fk_per = p_patient AND b.patstudystat_date = v_statdate  )
					  		  					 	  ) and rownum = 1;
	 END IF;
  RETURN v_retval ;
END ;
/


