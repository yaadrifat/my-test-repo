create or replace
function f_cbu_notes(cordid number,categoryval number,visibilityval VARCHAR2) return SYS_REFCURSOR
IS
v_cord_notes SYS_REFCURSOR;

BEGIN
    OPEN v_cord_notes FOR  SELECT
                         F_GETUSER(NOTES.CREATOR) NOTE_CREATOR,
                         F_CODELST_DESC(NOTES.FK_NOTES_TYPE) NOTES_TYPE,
                         NOTES.NOTE_SEQ NOTES_SEQ,
                         F_CODELST_DESC(NOTES.FK_NOTES_CATEGORY) NOTES_CATEGORY,
                        'Posted On' ||' '|| TO_CHAR(NOTES.CREATED_ON,'Mon DD, YYYY HH:MM:SS AM') NOTES_CREATEDON,
                         NOTES.NOTES NOTES_DESC,
                         DECODE(NOTES.NOTE_ASSESSMENT,
                                'tu','Temporaily Unavailable',
                                'no_cause','No Cause for Defferal',
                                'na','Not Available') NOTES_ASSESSMENT,
                        DECODE(NOTES.NOTE_ASSESSMENT,
                                'tu','tu_flag',
                                'na','na_flag') NOTES_REASON_FLAG,
                        nvl(F_CODELST_DESC(NOTES.FK_REASON),' ')NOTES_REASON,
                        NOTES.EXPLAINATION NOTES_EXPLAN1,
                        F_GETUSER(NOTES.SEND_TO) NOTES_SENDTO,
                        NOTES.AMENDED notes_amend
                        FROM 
                        CB_NOTES NOTES
                        WHERE NOTES.ENTITY_ID=CORDID AND NOTES.ENTITY_TYPE=(SELECT CL.PK_CODELST FROM ER_CODELST CL WHERE CL.CODELST_TYPE='entity_type' AND CL.CODELST_SUBTYP='CBU') 
                        AND NOTES.FK_NOTES_CATEGORY=CATEGORYVAL
                        AND notes.visibility in (select distinct visibility from cb_notes b where instr(','||VISIBILITYVAL||',',b.visibility) > 0)
                        ORDER BY NOTES.NOTE_SEQ DESC;
   RETURN v_cord_notes;
END;  
/
