CREATE OR REPLACE FUNCTION        "F_GETAE" (p_aedate DATE, p_per NUMBER, p_study NUMBER)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(4000);
v_counter NUMBER := 0 ;
BEGIN
  FOR i IN (SELECT 'Adverse Event : ' || AE_NAME || DECODE(fk_codlst_aetype,(SELECT pk_codelst FROM sch_codelst WHERE codelst_type = 'adve_type' AND codelst_subtyp='al_sadve'),'X@*@X','') || 'X@@@XGrade : ' || AE_GRADE || 'X@@@XStop date : ' ||  TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) || 'X@@@XRelationship to the study : ' || (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst=ae_relationship) || 'X@@@XOutcome Type : ' || DECODE(SUBSTR(AE_OUTTYPE,1,1),'1','[ Death ' || ae_outdate || ']    ') || DECODE(SUBSTR(AE_OUTTYPE,2,1),'1','[ Life-threatening ]    ') || DECODE(SUBSTR(AE_OUTTYPE,3,1),'1','[ Hospitalization ]    ')  || DECODE(SUBSTR(AE_OUTTYPE,4,1),'1','[ Disability ]    ') || DECODE(SUBSTR(AE_OUTTYPE,5,1),'1','[ Congenital Anomaly ]    ') || DECODE(SUBSTR(AE_OUTTYPE,6,1),'1','[ Required Intervention ]    ') || DECODE(SUBSTR(AE_OUTTYPE,7,1),'1','[ Recovered ]  ') AS adverse_event FROM sch_adverseve WHERE fk_per = p_per AND fk_study = p_study AND ae_stdate = p_aedate)
  LOOP
  	  v_counter := v_counter + 1;
	  IF v_counter > 1 THEN
	  	 v_retvalues := v_retvalues || 'X@@@X';
	  END IF;
  	  v_retvalues := v_retvalues || i.adverse_event;
  END LOOP;
  RETURN v_retvalues ;
END ;
/


CREATE SYNONYM ESCH.F_GETAE FOR F_GETAE;


CREATE SYNONYM EPAT.F_GETAE FOR F_GETAE;


GRANT EXECUTE, DEBUG ON F_GETAE TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETAE TO ESCH;

