/* Formatted on 2/9/2010 1:40:01 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_PAT_SCHEDULE
(
   PTSCH_PAT_ID,
   PTSCH_PAT_STUDY_ID,
   PTSCH_PAT_NAME,
   PTSCH_STUDY_NUM,
   PTSCH_CALENDAR,
   PTSCH_VISIT,
   PTSCH_SUGSCH_DATE,
   PTSCH_ACTSCH_DATE,
   PTSCH_VISIT_WIN,
   PTSCH_EVENT,
   PTSCH_EVENT_STAT,
   PRSCH_RESPONSE_ID,
   FK_ACCOUNT,
   EVENT_ID,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FK_PER,
   FK_STUDY,
   EVENTSTAT_DATE,
   EVENTSTAT_NOTES
)
AS
   SELECT   PERSON_CODE PTSCH_PAT_ID,
            PATPROT_PATSTDID PTSCH_PAT_STUDY_ID,
            PERSON_FNAME || ' ' || PERSON_LNAME PTSCH_PAT_NAME,
            (SELECT   STUDY_NUMBER
               FROM   ERES.ER_STUDY
              WHERE   PK_STUDY = c.FK_STUDY)
               PTSCH_STUDY_NUM,
            (SELECT   NAME
               FROM   EVENT_ASSOC
              WHERE   EVENT_ASSOC.EVENT_ID = a.SESSION_ID)
               PTSCH_CALENDAR,
            (SELECT   VISIT_NAME
               FROM   SCH_PROTOCOL_VISIT
              WHERE   PK_PROTOCOL_VISIT = FK_VISIT)
               PTSCH_VISIT,
            (CASE
                WHEN ACTUAL_SCHDATE IS NULL THEN NULL
                ELSE START_DATE_TIME
             END)
               AS PTSCH_SUGSCH_DATE,
            ACTUAL_SCHDATE PTSCH_ACTSCH_DATE,
            (SELECT      NVL (FUZZY_PERIOD, 0)
                      || ' '
                      || NVL (F_Get_Durunit (EVENT_DURATIONBEFORE), 'days')
                      || ' Before / '
                      || NVL (EVENT_FUZZYAFTER, 0)
                      || ' '
                      || NVL (F_Get_Durunit (EVENT_DURATIONAFTER), 'days')
                      || ' After'
               FROM   EVENT_ASSOC
              WHERE   EVENT_ASSOC.EVENT_ID = a.fk_assoc)
               PTSCH_VISIT_WIN,
            DESCRIPTION PTSCH_EVENT,
            (SELECT   CODELST_DESC
               FROM   SCH_CODELST
              WHERE   ISCONFIRMED = PK_CODELST)
               PTSCH_EVENT_STAT,
            EVENT_ID PRSCH_RESPONSE_ID,
            (SELECT   FK_ACCOUNT
               FROM   ER_PER
              WHERE   PK_PER = PATIENT_ID)
               fk_ACCOUNT,
            EVENT_ID,
            a.RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = a.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            a.CREATED_ON,
            c.FK_PER,
            c.FK_STUDY,
            EVENT_EXEON EVENTSTAT_DATE,
            notes EVENTSTAT_NOTES
     FROM   SCH_EVENTS1 a, person b, ER_PATPROT c
    WHERE   pk_person = c.fk_per AND pk_patprot = fk_patprot;


