/* Formatted on 2/9/2010 1:39:37 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDY_SPECIMEN
(
   PK_SPECIMEN,
   SPEC_ID,
   SPEC_ALTERNATE_ID,
   SPEC_DESCRIPTION,
   EXP_SPEC_QTY,
   EXP_QTY_UNITS,
   COLL_SPEC_QTY,
   COLL_QTY_UNITS,
   SPEC_QTY,
   SPEC_QTY_UNITS,
   SPEC_TYPE,
   SPEC_COLLECTION_DATE,
   OWNER,
   SPEC_STORAGE,
   SPEC_KIT_COMPONENT,
   FK_STUDY,
   SPEC_STUDY_NUMBER,
   FK_PER,
   SPEC_PATIENT_CODE,
   SPEC_PATIENT_NAME,
   SPEC_ORGN,
   FK_VISIT,
   EVENT_DESCRIPTION,
   SPEC_ANATOMIC_SITE,
   SPEC_TISSUE_SIDE,
   SPEC_PATHOLOGY_STAT,
   SPEC_STORAGE_KIT,
   SPEC_NOTES,
   PATHOLOGIST,
   SURGEON,
   COLLECTING_TECHNICIAN,
   PROCESSING_TECHNICIAN,
   SPEC_PROCESSING_TYPE,
   SPEC_REMOVAL_TIME,
   SPEC_FREEZE_TIME,
   PARENT_SPEC_ID,
   SPEC_STATUS,
   STATUS_START_DATE,
   STATUS_QUANTITY,
   STATUS_QTY_UNITS,
   ACTION,
   STATUS_STUDY_NUMBER,
   RECEPIENT,
   SS_TRACKING_NUMBER,
   STATUS_NOTES,
   STATUS_BY,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT
)
AS
   SELECT   a.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               spec_qty_units,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            (SELECT   c.storage_name
               FROM   ER_STORAGE c
              WHERE   c.pk_storage = a.fk_storage)
               spec_storage,
            (SELECT   d.storage_name
               FROM   ER_STORAGE d
              WHERE   d.pk_storage = a.FK_STORAGE_KIT_COMPONENT)
               spec_kit_component,
            a.FK_STUDY fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = a.FK_STUDY)
               spec_study_number,
            FK_PER,
            (SELECT   person_code
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_code,
            (SELECT   person_FNAME || ' ' || person_LNAME
               FROM   epat.person
              WHERE   PK_person = fk_per)
               AS spec_patient_name,
            (SELECT   site_name
               FROM   ER_SITE
              WHERE   pk_site = FK_SITE)
               AS spec_orgn,
            FK_VISIT,
            (SELECT   description
               FROM   sch_events1
              WHERE   event_id = FK_SCH_EVENTS1)
               Event_Description,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   STORAGE_NAME
               FROM   ER_STORAGE
              WHERE   PK_STORAGE = FK_STORAGE_KIT)
               SPEC_STORAGE_KIT,
            SPEC_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = b.SS_PROC_TYPE)
               spec_processing_type,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   aa.SPEC_ID
               FROM   ER_SPECIMEN aa
              WHERE   aa.pk_specimen = a.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = b.FK_CODELST_STATUS)
               spec_status,
            SS_DATE status_start_date,
            SS_QUANTITY status_quantity,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_QUANTITY_UNITS)
               status_qty_units,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_ACTION)
               action,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = b.FK_STUDY)
               status_study_number,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_RECEPIENT)
               AS RECEPIENT,
            SS_TRACKING_NUMBER,
            SS_NOTES status_notes,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SS_STATUS_BY)
               AS Status_By,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = a.CREATOR)
               creator,
            a.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            a.LAST_MODIFIED_DATE,
            FK_ACCOUNT
     FROM   ER_SPECIMEN a, ER_SPECIMEN_STATUS b
    WHERE   a.PK_SPECIMEN = b.FK_SPECIMEN
            AND b.PK_SPECIMEN_STATUS =
                  (SELECT   MAX (c.PK_SPECIMEN_STATUS)
                     FROM   ER_SPECIMEN_STATUS c
                    WHERE   a.pk_SPECIMEN = c.fk_SPECIMEN
                            AND c.ss_date =
                                  (SELECT   MAX (d.ss_date)
                                     FROM   ER_SPECIMEN_STATUS d
                                    WHERE   a.pk_SPECIMEN = d.fk_SPECIMEN)
                            AND (a.FK_STUDY IS NOT NULL AND a.FK_PER IS NULL))
            AND a.FK_ACCOUNT IS NOT NULL;


