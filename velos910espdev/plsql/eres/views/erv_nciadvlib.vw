/* Formatted on 2/9/2010 1:39:21 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_NCIADVLIB
(
   PK_LKPDATA,
   FK_LKPLIB,
   CATEGORY,
   ADV_EVENT,
   ISLAB,
   GRADE,
   DESCRIPTION,
   ADV_LAB,
   TOXICITY_DESC,
   TOXICITY_GROUPID,
   LABRANGE_LLN,
   LABRANGE_ULN,
   LABRANGE_UNIT,
   LABRANGE_VALIDFROM,
   LABRANGE_VALIDTILL,
   FK_SITE,
   NCI_OPERATOR1,
   NCI_EXPR1,
   NCI_OPERATOR2,
   NCI_EXPR2,
   NCI_UNIT,
   LKPTYPE_VERSION,
   LABRANGE_GENDER,
   LABRANGE_AGEFRM,
   LABRANGE_AGETO
)
AS
   SELECT   pk_lkpdata,
            fk_lkplib,
            custom001 category,
            custom002 adv_event,
            custom005 isLab,
            custom004 grade,
            custom006 description,
            custom007 adv_lab,
            custom003 toxicity_desc,
            custom008 toxicity_groupid,
            labrange.labrange_lln,
            labrange.labrange_uln,
            labrange.labrange_unit,
            NVL (
               labrange.labrange_validfrom,
               TO_DATE (pkg_dateUTIL.f_get_null_date_str (),
                        pkg_dateUTIL.f_get_dateformat ())
            ),
            labrange.labrange_validtill,
            labrange.fk_site,
            xpr.nci_operator1,
            xpr.nci_expr1,
            xpr.nci_operator2,
            xpr.nci_expr2,
            DECODE (TRIM (xpr.nci_unit), '', NULL, xpr.nci_unit),
            lkptype_version,
            labrange_gender,
            labrange_agefrm,
            labrange_ageto
     FROM   er_lkplib,
            er_lkpdata,
            er_ncigradexpr xpr,
            ER_LABRANGE labrange
    WHERE       lkptype_name = 'NCI'
            AND fk_lkplib = pk_lkplib
            AND xpr.fk_lkpdata_adv = pk_lkpdata
            AND labrange.fk_labtest = custom007
            AND xpr.nci_unit = labrange_unit
   UNION
   SELECT   pk_lkpdata,
            fk_lkplib,
            custom001 category,
            custom002 adv_event,
            custom005 isLab,
            custom004 grade,
            custom006 description,
            custom007 adv_lab,
            custom003 toxicity_desc,
            custom008 toxicity_groupid,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            lkptype_version,
            NULL,
            NULL,
            NULL
     FROM   er_lkplib, er_lkpdata
    WHERE       lkptype_name = 'NCI'
            AND fk_lkplib = pk_lkplib
            AND custom004 = '5';


CREATE SYNONYM ESCH.ERV_NCIADVLIB FOR ERV_NCIADVLIB;


CREATE SYNONYM EPAT.ERV_NCIADVLIB FOR ERV_NCIADVLIB;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_NCIADVLIB TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_NCIADVLIB TO ESCH;

