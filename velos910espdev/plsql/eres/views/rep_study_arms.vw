/* Formatted on 2/9/2010 1:40:04 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_STUDY_ARMS
(
   STARM_STUDY_NUM,
   STARM_NAME,
   STARM_DESC,
   STARM_DRUG_INFO,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_ON,
   FK_ACCOUNT,
   RID,
   STARM_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               STARM_STUDY_NUM,
            TX_NAME STARM_NAME,
            TX_DESC STARM_DESC,
            TX_DRUG_INFO STARM_DRUG_INFO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYTXARM.CREATOR)
               CREATOR,
            CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYTXARM.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_ON,
            (SELECT   FK_ACCOUNT
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               FK_ACCOUNT,
            RID,
            PK_STUDYTXARM STARM_RESPONSE_ID,
            FK_STUDY
     FROM   ER_STUDYTXARM;


