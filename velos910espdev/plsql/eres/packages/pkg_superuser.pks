CREATE OR REPLACE PACKAGE        "PKG_SUPERUSER" IS

   /* Sonia Abrol  10/22/07
   checks if a user has super user access rights to a study.
   returns:
   1:yes
   0:no
   */
   function F_Is_Superuser(p_user  NUmber, p_study Number) return number;

   function F_get_Superuser_right(p_user  Number ) return varchar2;

   function F_Is_BgtSuperuser(p_user  NUmber) return number;

    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_Superuser', pLEVEL  => Plog.LFATAL);


END Pkg_Superuser ;
/


CREATE SYNONYM ESCH.PKG_SUPERUSER FOR PKG_SUPERUSER;


GRANT EXECUTE, DEBUG ON PKG_SUPERUSER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_SUPERUSER TO ESCH;

