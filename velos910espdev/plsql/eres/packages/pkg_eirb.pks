CREATE OR REPLACE PACKAGE      PKG_EIRB AS
/******************************************************************************
   NAME:       PKG_EIRB
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  PROCEDURE sp_checkSubmission(p_study IN NUMBER, p_submission_board in number,
  o_results OUT  Gk_Cv_Types.GenericCursorType,o_pass_submission OUT Number);


PROCEDURE sp_submit_ongoing(p_study IN NUMBER  ,p_lf_submission_type IN VARCHAR2,p_creator in number,p_ipadd in varchar2,
o_ret OUT NUMBER,p_formstatus in number
  );


END PKG_EIRB;
/


