CREATE OR REPLACE PACKAGE      PKG_IRBMOM_LOGIC AS
/******************************************************************************
   NAME:       PKG_IRBMOM_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        7/27/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  Function f_generateMOM(p_board in number,p_meeting_datepk in Number) return CLOB;

  Function f_get_othertopic(p_meeting_datepk in Number,p_account in number) return CLOB;

  Function f_get_momFooterTemplate(p_account in Number) return CLOB;

  Function f_get_submissionContent(p_board in number,p_meeting_datepk in Number, p_account in Number) return CLOB;

  Function f_get_submissionProvisos(p_submission Number,p_template clob) return CLOB;

END PKG_IRBMOM_LOGIC ;
/


