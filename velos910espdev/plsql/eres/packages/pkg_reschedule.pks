CREATE OR REPLACE PACKAGE ERES."PKG_RESCHEDULE" IS

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_RESCHEDULE', pLEVEL  => Plog.LFATAL);

/* author: Sonia Abrol
 date : 02/25/08 - package with utility procedures for patient schedule management

 There will be no commit statements in any of these procedures.
 Since these procedures can be called independently of in conjunction with other procedures, 'COMMIT' will be handled by calling code.

*/


/* SP_DISC_ALLSCHS   : Discontinue schedules for all patients on the given protocol. no commit added.
  commit to be handled by calling routine

   P_CURRENT_PROTOCOL : The Primary Key of protocol calendar
   P_DISCDATE   : Date when schedules are discontinued
   P_DISCREASON : Discontinuation Reason
   P_NEW_PROTOCOL : new protocol on which patients should be added. optional, pass 0 when patients need
        not be put on a new protocol
   P_NEW_SCHDATE_FLAG : Schedule start date option for new Protocol. optional, will be used only when  P_NEW_PROTOCOL is passed
          Possible values:

          E: Same as patient enrollment date. In this case, Generate option will only work for patients that have enrolled status added
          D: Date of Discontinuation for previous protocol
          C: Current Date
          S: Same as start date of the current protocol that is being discontinued

   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_DISC_ALLSCHS  (
   P_CURRENT_PROTOCOL    IN   NUMBER,
   P_DISCDATE   IN   DATE,
   P_DISCREASON IN VARCHAR2,
   P_NEW_PROTOCOL IN NUMBER,
   P_NEW_SCHDATE_FLAG IN VARCHAR2,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/* SP_UPDATE_EVENTS  : Updates event statuses for all schedules generated for a Calendar :  will be used by both b and c checkbox options

   P_PROTOCOL : The Primary Key of protocol calendar
   P_SCHEDULE_TYPE_FLAG :flag to indicate whether the status should be updated for a 'Current' schedule or a 'discontinued' schedule.
          Possible values:  C: Currrent schedule, D: Discontinued schedule

          if P_SCHEDULE_TYPE_FLAG is 'D', and a patient has multiple discontinued schedules for the selected protocol,
           all those schedules will be affected by this update

   P_EVENT_RANGE_FLAG : Flag to indicate what scheduled events will be updated:
          Possible values: B: before the given date - P_EVENT_DATE
                           OB: on and before the given date - P_EVENT_DATE
                           A: After given date -P_EVENT_DATE
                           OA: on and After given date - P_EVENT_DATE
   P_EVENT_DATE : Event Date, to be used with parameter P_EVENT_RANGE_FLAG
   P_EVENT_STATUS : The codelist PK of the new status set for the selected events (using parameters P_EVENT_RANGE_FLAG and P_EVENT_DATE)
   P_EVENT_STATUS_DATE : The new 'event status date' for the selected events. If not passed, it will be defaulted to system date
      (using parameters P_EVENT_RANGE_FLAG and P_EVENT_DATE)
   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_UPDATE_EVENTS  (
   P_PROTOCOL    IN   NUMBER,
   P_SCHEDULE_TYPE_FLAG IN VARCHAR2,
   P_EVENT_RANGE_FLAG  IN VARCHAR2,
   P_EVENT_DATE  IN DATE,
   P_EVENT_STATUS IN NUMBER,
   P_EVENT_STATUS_DATE  IN DATE,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/* SP_GENERATE_SCHEDULES : Put all patients on the selected study calendar and generate schedules. This feature will work for patients
that do not have any current schedules (or are not put on any protocol calendar on the study )

   P_PROTOCOL : The Primary Key of protocol calendar
   P_SCH_START_DATE : Schedule Start Date. This date will be used as the start date for all schedules
   P_USE_ENROLL_DATE : Flag to indicate whether 'enrollment date' should be used as start date for the schedules. Possible values:
     1:Use enrollment date,0: do not use enrollment date. when passed as 1,P_SCH_START_DATE will be ignored
     if P_USE_ENROLL_DATE is passed , schedules for only enrolled patients will be generated
   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_GENERATE_SCHEDULES  (
   P_PROTOCOL    IN   NUMBER,
   P_SCH_START_DATE IN DATE,
   P_USE_ENROLL_DATE IN NUMBER,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/*
 Procedure to deactivate the current schedule. again, no commit added in the procedure, committing the transaction is calling
 routine's responsibility

 P_PROTOCOL : The Primary Key of protocol calendar
    P_ID : The PK of the patient or study. depending on the type of schedule being deactivated. Used to identify 'study admin schedule'.
      Can be passed NULL for patient schedule
    p_date :  The schedule start date for the given protocol /P_ID combination. Used to identify 'study admin schedule'.
      Can be passed NULL for patient schedule
    p_patprot : PK_PATPROT for the patient schedule. Can be passed as NULL for admin schedule
    p_sch_type : flag to indicate schedule type: S:Study Admin, P:Patient Schedule
   p_last_modified_by : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful

*/

PROCEDURE SP_DEACTIVATE_SCHEDULE(
p_protocol IN NUMBER,
p_id IN NUMBER,
p_date IN DATE,
p_patprot IN NUMBER,
p_sch_type CHAR :='',
p_last_modified_by IN NUMBER,
p_ipadd IN VARCHAR2,
o_success OUT NUMBER
);


/*
 Procedure to generate patient schedule. again, no commit added in the procedure, committing the transaction is calling
 routine's responsibility

 p_protocol   : The Primary Key of protocol calendar
 p_patientid : The Primary Key of Patient for patient schedule. for study admin schedule, pass 0
    p_date :  The schedule start date for the given protocol /p_patientid combination.
    p_patprotid  : PK_PATPROT for the patient schedule. for admin schedule pass PK of the study

   p_days : The 'Start Day' from where the schedule should be generated.
   p_dmlby : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful

*/
  PROCEDURE sp_generate_schedule (
     p_protocol    IN   NUMBER,
      p_patientid   IN   NUMBER,
      p_date        IN   DATE,
      p_patprotid   IN   NUMBER,
      p_days        IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
     o_success OUT NUMBER
   );


function f_get_eventstatus_newpk return Number;
/*
Get the displacement after checking whether the particular calendar has to generate schedule according to day0 interval.
Added by Bikash kumar,Feb-17-2011 for Req D-FIN-25-DAY0
*/
function f_get_displacement(p_protocol NUMBER,p_visit NUMBER) return Number;
function f_check_dayZero(p_protocol NUMBER,p_visit NUMBER) return Number;
END PKG_RESCHEDULE ;
/
