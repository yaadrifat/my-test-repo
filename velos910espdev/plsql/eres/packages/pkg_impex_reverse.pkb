CREATE OR REPLACE PACKAGE BODY      "PKG_IMPEX_REVERSE"
AS

   FUNCTION F_GET_PAT_ACCOUNT (P_PER NUMBER) RETURN NUMBER
    IS
 v_pk_per NUMBER;
 v_account NUMBER;
 /*
   Austhor: Sonia Sahni
   Returns patient's Account Id
 */
 BEGIN
   v_pk_per := p_per;

  BEGIN
   SELECT fk_account
   INTO v_account
   FROM ER_PER
   WHERE pk_per = p_per;

   RETURN v_account;
     EXCEPTION WHEN NO_DATA_FOUND THEN
   RETURN -1;
  END;

 END;



   PROCEDURE SP_GET_STUDYSPONSORINFO(P_SITEPK NUMBER, O_SPONSORPK OUT NUMBER , O_SITECODE OUT VARCHAR2, O_SPONSOR_ACCOUNT OUT NUMBER, O_ERRMSG OUT VARCHAR2)
   AS
  /** Author : Sonia Sahni
        Date: 04/26/04
      Purpose: Gets following information for a site study
       Sponsor pk_study
      Sponsor fk_account
      Site code
  */

  v_sql LONG;

 BEGIN
    v_sql := 'Select  FK_SPONSOR_STUDY, FK_SPONSOR_ACCOUNT, SITE_CODE  from er_rev_studysponsor
        WHERE FK_SITESTUDY = :1' ;
 BEGIN

 EXECUTE IMMEDIATE v_sql INTO O_SPONSORPK, O_SPONSOR_ACCOUNT,O_SITECODE USING P_SITEPK;


   EXCEPTION
 WHEN NO_DATA_FOUND THEN

  O_ERRMSG := SQLERRM;
  O_SPONSORPK := -1;
  O_SPONSOR_ACCOUNT := -1;
  O_SITECODE := '';
 WHEN OTHERS THEN
  O_ERRMSG := SQLERRM;
  O_SPONSORPK := -1;
  O_SPONSOR_ACCOUNT := -1;
  O_SITECODE := '';
 END;

 END;

  PROCEDURE SP_GET_SITEPK(P_ORIGPK NUMBER, P_TABLE VARCHAR2, P_SITECODE VARCHAR2, O_PK OUT NUMBER , O_ERRMSG OUT VARCHAR2,P_ORIGINATING_SITECODE IN VARCHAR2)
 AS
 /** Author : Sonia Sahni
    Date: 02/20/04
  Purpose: Used in import procedures in reverse A2A. Finds out the SITE PK generated for exported data's SPONSOR PK
  Returns the same PK  if the pk_does not exist. This is possible when sponsor is the same site where it's aggregated.
  */
  v_sql LONG;
  v_sitepk NUMBER;

 BEGIN
  IF P_ORIGPK IS NOT NULL AND P_ORIGPK > 0 THEN
    v_sql := 'Select SITEPKMAP_SITEPK from er_sitepkmap
        WHERE SITEPKMAP_TABLE = :1 AND SITEPKMAP_SITECODE = :2 AND SITEPKMAP_SPONSORPK  = :3 AND ORIGINATING_SITECODE = :4';

 BEGIN
     EXECUTE IMMEDIATE v_sql INTO v_sitepk USING P_TABLE, P_SITECODE , P_ORIGPK ,P_ORIGINATING_SITECODE;
        O_PK := v_sitepk ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     O_ERRMSG := SQLERRM;
   O_PK := P_ORIGPK ; --possible when sponsor and collecting sites are smae
   WHEN OTHERS THEN
   O_ERRMSG := SQLERRM;
   O_PK := -1;
 END;
END IF;
 END;



/* only used to get PKs for study,forms, etc. at sponsor. The sponsor may be a participating the site */
   PROCEDURE SP_GET_SPONSORPK(P_SITEPK NUMBER, P_TABLE VARCHAR2 , P_SITECODE VARCHAR2,  O_ORIGPK OUT NUMBER , O_ERRMSG OUT VARCHAR2)
AS
  /** Author : Sonia Sahni
        Date: 04/05/04
      Purpose: Finds out the original SPONSOR PK for a site PK
     Returns -1 if the pk_doesnot exist
  */
  v_sql LONG;
  v_sitepk NUMBER;
  v_site_code VARCHAR2(250);

 BEGIN
    v_sql := 'Select SITEPKMAP_SPONSORPK from er_sitepkmap
        WHERE SITEPKMAP_TABLE = :1 AND SITEPKMAP_SITECODE = :2 AND SITEPKMAP_SITEPK  = :3';
 BEGIN
 EXECUTE IMMEDIATE v_sql INTO v_sitepk USING P_TABLE, P_SITECODE ,P_SITEPK;
  O_ORIGPK := v_sitepk ;
   EXCEPTION
 WHEN NO_DATA_FOUND THEN
  O_ERRMSG := SQLERRM;
  O_ORIGPK := P_SITEPK; -- in this case the sponsor is also a participating site
 WHEN OTHERS THEN
  O_ERRMSG := SQLERRM;
  O_ORIGPK := P_SITEPK; -- in this case the sponsor is also a participating site
 END;

 END;

 PROCEDURE SP_EXP_PATENROLL (
    P_EXPID          NUMBER,
      O_RESULT_ENR     OUT  Gk_Cv_Types.GenericCursorType,
      O_RESULT_PAT    OUT  Gk_Cv_Types.GenericCursorType,
   O_RESULT_PATSTUDYSTAT OUT  Gk_Cv_Types.GenericCursorType,
   O_RESULT_PATLABS OUT  Gk_Cv_Types.GenericCursorType,
   O_RESULT_PATFACILITY  OUT  Gk_Cv_Types.GenericCursorType,
   O_RESULT_PATXARM  OUT  Gk_Cv_Types.GenericCursorType
    )
   /* Author: Sonia Sahni
   Date: 04/05/04
   Purpose - Returns a ref cursor of study enrollment data
   */
   AS
        v_rec_enr rec_er_patprot   := rec_er_patprot(0,0,0,0,SYSDATE,0,SYSDATE,'',0,
      SYSDATE,'',0,0,'',SYSDATE,SYSDATE, SYSDATE, 0, 0, 0,'',0,'',0,NULL,NULL ,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL); --record type
    v_table_enr tab_er_patprot := tab_er_patprot(); --index-by table type


 v_rec_patstudystat rec_er_patstudystat := rec_er_patstudystat(0,'',0,0,SYSDATE,SYSDATE,'','','',0,'',0,'','','',NULL,NULL,NULL);
 v_table_patstudystat tab_er_patstudystat := tab_er_patstudystat();

    v_rec_patlabs rec_er_patlabs   := rec_er_patlabs(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
           NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
         NULL,NULL,NULL,NULL,NULL,NULL,NULL); --record type
    v_table_patlabs tab_er_patlabs := tab_er_patlabs(); --index-by table type


   v_cnt NUMBER := 0;
   v_sponsor_study NUMBER;
   v_sponsor_protocol NUMBER;
   v_err VARCHAR2(4000);

   v_enroltable_name VARCHAR2(50);
   v_pat_table_name VARCHAR2(15);

   v_patstudystat_table_name VARCHAR2(15);
   v_pattxarm_table_name VARCHAR2(15);

   v_site_code VARCHAR2(255);

   v_pat_sql LONG;

   v_study VARCHAR2(4000);

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

    v_success NUMBER;

 v_study_export_settings sys.XMLTYPE;
    v_exportall NUMBER := 0;

 v_lowerLimitNumberOfDays NUMBER;
    v_upperLimitNumberOfDays NUMBER;
 v_upperLimitEnrollStatus VARCHAR2(20);

 v_stat NUMBER;
 v_sitecode VARCHAR2(20);
 v_module_subtype VARCHAR2(20);
 v_patstat_codelst_type VARCHAR2(15);
 v_originating_sitecode varchar2(10);

 v_sponsor_txarm Number;
 v_rec_pattxarm rec_pat_txarm := rec_pat_txarm(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

 v_table_pattxarm TAB_PAT_TXARMS := TAB_PAT_TXARMS();


   BEGIN

  --get export request

  Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

  --find out the study id from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

  --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);


  -- get study export settings

  BEGIN
   SELECT export_settings, originating_sitecode
   INTO  v_study_export_settings,v_originating_sitecode
   FROM ER_REV_STUDYSPONSOR
   WHERE fk_sitestudy = v_study;
   EXCEPTION WHEN NO_DATA_FOUND THEN
     v_study_export_settings := NULL;
  END;
   --get patients

    v_pat_table_name := 'er_per';
   v_module_subtype := 'pat_enr';

 UPDATE ER_REV_PENDINGDATA
 SET rp_processedflag = 1
 WHERE rp_module = v_module_subtype AND rp_sitecode = v_sitecode;

 COMMIT;


 OPEN  O_RESULT_PAT
     FOR
  SELECT  PK_PER, PER_CODE, ER_PER.FK_SITE , PERSON_DEATHDT,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_PSTAT) PAT_STAT_SUBTYPE,
  RP_SITECODE    ,
  SITE_NAME  ,
  PERSON_DOB,
  FK_SPONSOR_ACCOUNT , person.FK_TIMEZONE,person.pat_facilityid,person_lname,person_fname,person_mname,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_GENDER) PAT_FK_CODELST_GENDER,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_MARITAL) PAT_FK_CODELST_MARITAL,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_BLOODGRP) PAT_FK_CODELST_BLOODGRP,
  PERSON_SSN, person_email,person_address1,person_address2,person_city,person_state, person_country,person_zip,person_hphone,
  person_bphone,  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_RACE) PAT_FK_CODELST_RACE,
  Pkg_Util.f_getCodeSubtypes(person_add_race,',',',')  person_add_race_subtypes,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_ETHNICITY) PAT_FK_CODELST_ETHNICITY,
  Pkg_Util.f_getCodeSubtypes(person_add_ethnicity,',',',') person_add_ethnicity_subtypes,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_EMPLOYMENT) PAT_FK_CODELST_EMPLOYMENT,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_EDU) PAT_FK_CODELST_EDU, person_notes,
  (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_pat_dth_cause) PAT_fk_codelst_pat_dth_cause, cause_of_death_other,person_county,
  originating_sitecode
  FROM ER_PER, ER_REV_PENDINGDATA, person person, ER_SITE, ER_REV_STUDYSPONSOR
  WHERE fk_study = v_study AND RP_TABLENAME = v_pat_table_name AND ER_PER.pk_per =  RP_TABLEPK AND
  person.pk_person = ER_PER.pk_per  AND
  ER_PER.fk_site = pk_site AND
  fk_study = FK_SITESTUDY AND rp_processedflag = 1;

  -- get patfacility data

  OPEN  O_RESULT_PATFACILITY
     FOR
  SELECT PK_PATFACILITY,    FK_PER,     FK_SITE,     PAT_FACILITYID,       PATFACILITY_REGDATE,    PATFACILITY_ACCESSRIGHT,   PATFACILITY_DEFAULT,   IS_READONLY,originating_sitecode
  FROM ER_PATFACILITY, ER_REV_PENDINGDATA,  ER_REV_STUDYSPONSOR
  WHERE fk_study = v_study AND RP_TABLENAME = 'er_patfacility'  AND pk_patfacility  =  RP_TABLEPK AND
    fk_study = FK_SITESTUDY AND rp_processedflag = 1;




   -- Get enrollment data


   v_enroltable_name := 'er_patprot';

  -- get data --
 FOR i IN ( SELECT   PK_PATPROT ,FK_PROTOCOL ,ER_PATPROT.FK_STUDY FK_STUDY, FK_USER , PATPROT_ENROLDT,
   FK_PER ,PATPROT_START , PATPROT_NOTES ,PATPROT_STAT , PATPROT_DISCDT ,
   PATPROT_REASON   , PATPROT_SCHDAY , FK_TIMEZONE, PATPROT_PATSTDID,
   PATPROT_CONSIGNDT  , PATPROT_RESIGNDT1  , PATPROT_RESIGNDT2  , PATPROT_CONSIGN ,
   FK_USERASSTO   ,(SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELSTLOC ) FK_CODELSTLOC_SUBTYP,
   RP_SITECODE , FK_SPONSOR_ACCOUNT, PATPROT_RANDOM,  PATPROT_PHYSICIAN,
   FK_SITE_ENROLLING, (DECODE(FK_SITE_ENROLLING,NULL,'',(SELECT site_name FROM ER_SITE WHERE pk_site = FK_SITE_ENROLLING))) FK_SITE_ENROLLING_NAME,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_ptst_eval_flag ) fk_code_ptst_eval_flag,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_ptst_eval ) fk_code_ptst_eval,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_ptst_ineval ) fk_code_ptst_ineval,
     (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_ptst_survival ) fk_code_ptst_survival,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = fk_codelst_ptst_dth_stdrel ) fk_code_ptst_dth_stdrel,
    death_std_rltd_other, date_of_death,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE
   FROM ER_PATPROT, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_enroltable_name  AND pk_patprot = RP_TABLEPK
   AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study AND rp_processedflag = 1 AND NVL(rp_record_type,'O') <> 'D'
   UNION
      SELECT  RP_TABLEPK  PK_PATPROT, NULL,ER_REV_PENDINGDATA.fk_study,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
      ,NULL,NULL,NULL,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE
   FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_enroltable_name AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND NVL(rp_record_type,'O') = 'D' AND rp_processedflag = 1


   )
 LOOP
  v_cnt := v_cnt + 1;

     v_site_code := i.RP_SITECODE;


   --get sponsor study id
    SP_GET_SPONSORPK(i.FK_STUDY, 'er_study' , v_site_code,  v_sponsor_study,v_err);

       Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_patprot v_sponsor_study  ' || v_sponsor_study , v_success );

    --if i.FK_PROTOCOL is not null and i.FK_PROTOCOL > 0 then
       -- get sponsor protocol id
       SP_GET_SPONSORPK(i.FK_PROTOCOL, 'event_assoc' , v_site_code ,  v_sponsor_protocol , v_err);
    --end if;

       Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_patprot v_sponsor_protocol   ' || v_sponsor_protocol  , v_success );

    IF v_sponsor_study > 0 THEN

     v_rec_enr.PK_PATPROT  := i.PK_PATPROT ;
     v_rec_enr.FK_PROTOCOL :=  v_sponsor_protocol;
     v_rec_enr.FK_STUDY   :=  v_sponsor_study ;
     v_rec_enr.FK_USER    := i.FK_USER;
     v_rec_enr.PATPROT_ENROLDT   := i.PATPROT_ENROLDT;
     v_rec_enr.FK_PER      :=       i.FK_PER;
     v_rec_enr.PATPROT_START  :=    i.PATPROT_START;
     v_rec_enr.PATPROT_NOTES   :=   i.PATPROT_NOTES;
     v_rec_enr.PATPROT_STAT    :=   i.PATPROT_STAT;
     v_rec_enr.PATPROT_DISCDT   :=  i.PATPROT_DISCDT;
     v_rec_enr.PATPROT_REASON   :=  i.PATPROT_REASON;
     v_rec_enr.PATPROT_SCHDAY   :=  i.PATPROT_SCHDAY;
     v_rec_enr.FK_TIMEZONE      :=  i.FK_TIMEZONE;
     v_rec_enr.PATPROT_PATSTDID :=  i.PATPROT_PATSTDID;
     v_rec_enr.PATPROT_CONSIGNDT  := i.PATPROT_CONSIGNDT;
     v_rec_enr.PATPROT_RESIGNDT1  := i.PATPROT_RESIGNDT1;
     v_rec_enr.PATPROT_RESIGNDT2  := i.PATPROT_RESIGNDT2;
     v_rec_enr.PATPROT_CONSIGN    := i.PATPROT_CONSIGN;
     v_rec_enr.FK_USERASSTO      := i.FK_USERASSTO;
     v_rec_enr.FK_CODELSTLOC   := i.FK_CODELSTLOC_SUBTYP ;
     v_rec_enr.SITE_CODE := v_site_code;
     v_rec_enr.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;

     v_rec_enr.PATPROT_RANDOM := i.PATPROT_RANDOM ;
     v_rec_enr.PATPROT_PHYSICIAN  := i.PATPROT_PHYSICIAN;
     v_rec_enr.FK_SITE_ENROLLING := i.FK_SITE_ENROLLING;
     v_rec_enr.FK_SITE_ENROLLING_NAME := i.FK_SITE_ENROLLING_NAME;
     v_rec_enr.fk_codelst_ptst_eval_flag := i.fk_code_ptst_eval_flag;
     v_rec_enr.fk_codelst_ptst_eval := i.fk_code_ptst_eval;
     v_rec_enr.fk_codelst_ptst_ineval := i.fk_code_ptst_ineval;
     v_rec_enr.fk_codelst_ptst_survival := i.fk_code_ptst_survival ;
     v_rec_enr.fk_codelst_ptst_dth_stdrel := i.fk_code_ptst_dth_stdrel;
     v_rec_enr.death_std_rltd_other := i.death_std_rltd_other;
     v_rec_enr.date_of_death := i.date_of_death;
     v_rec_enr.originating_sitecode :=    v_originating_sitecode ;
     v_rec_enr.record_type := i.RP_RECORD_TYPE;

     --store each record in a table
           v_table_enr.EXTEND;
     v_table_enr(v_cnt) := v_rec_enr;
     v_cnt := v_table_enr.COUNT();
    END IF;

 END LOOP;
 -- populate out cursor
  OPEN  O_RESULT_ENR
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_enr AS tab_er_patprot));


 -- get patient study status

 v_patstudystat_table_name := 'er_patstudystat';


 v_cnt := 0;

 FOR j IN (
   SELECT  PK_PATSTUDYSTAT ,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_STAT ) FK_CODELST_STAT_SUBTYP,
   FK_PER     ,
   ER_PATSTUDYSTAT.FK_STUDY  FK_STUDY  ,
   PATSTUDYSTAT_DATE,
   PATSTUDYSTAT_ENDT ,
   PATSTUDYSTAT_NOTE  ,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = PATSTUDYSTAT_REASON) PATSTUDYSTAT_REASON_SUBTYP,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,
   SCREEN_NUMBER,
   SCREENED_BY,
   (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = SCREENING_OUTCOME)  SCREENING_OUTCOME_SUBTYP,
   NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE, inform_consent_ver, next_followup_on,current_stat
   FROM ER_PATSTUDYSTAT, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patstudystat_table_name AND
   ER_PATSTUDYSTAT.pk_patstudystat =  RP_TABLEPK AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND rp_processedflag = 1 AND NVL(RP_RECORD_TYPE,'O') <> 'D'
   UNION
   SELECT  RP_TABLEPK  PK_PATSTUDYSTAT,
   NULL,NULL,NULL,NULL,NULL,NULL,NULL,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,NULL,NULL,NULL,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE,
   NULL inform_consent_ver, NULL next_followup_on,NULL
   FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patstudystat_table_name AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND NVL(rp_record_type,'O') = 'D' AND rp_processedflag = 1

   )
 LOOP
  v_cnt := v_cnt + 1;

  v_site_code := j.RP_SITECODE;

  --get sponsor study id
  SP_GET_SPONSORPK(v_study , 'er_study' , v_site_code,  v_sponsor_study,v_err);
  Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_patstudystat row ' || v_cnt , v_success );

  Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_patstudystat v_sponsor_study ' || v_sponsor_study , v_success );

  IF v_sponsor_study > 0 THEN

   v_rec_patstudystat.PK_PATSTUDYSTAT  := j.PK_PATSTUDYSTAT ;
   v_rec_patstudystat.SITE_CODE := v_site_code;
   v_rec_patstudystat.FK_SPONSOR_ACCOUNT := j.FK_SPONSOR_ACCOUNT;
   v_rec_patstudystat.RECORD_TYPE := j.RP_RECORD_TYPE;
            v_rec_patstudystat.FK_STUDY := v_sponsor_study;

   v_rec_patstudystat.FK_CODELST_STAT_SUBTYP := j.FK_CODELST_STAT_SUBTYP ;
   v_rec_patstudystat.FK_PER     := j.FK_PER ;
   v_rec_patstudystat.PATSTUDYSTAT_DATE     := j.PATSTUDYSTAT_DATE ;
   v_rec_patstudystat.PATSTUDYSTAT_ENDT     := j.PATSTUDYSTAT_ENDT ;
   v_rec_patstudystat.PATSTUDYSTAT_NOTE     := j.PATSTUDYSTAT_NOTE;
   v_rec_patstudystat.PATSTUDYSTAT_REASON_SUBTYP := j.PATSTUDYSTAT_REASON_SUBTYP;
   v_rec_patstudystat.SCREEN_NUMBER := j.SCREEN_NUMBER ;
   v_rec_patstudystat.SCREENED_BY     := j.SCREENED_BY;
   v_rec_patstudystat.SCREENING_OUTCOME_SUBTYP :=    j.SCREENING_OUTCOME_SUBTYP;
   v_rec_patstudystat.INFORM_CONSENT_VER := j.INFORM_CONSENT_VER ;
     v_rec_patstudystat.NEXT_FOLLOWUP_ON := j.NEXT_FOLLOWUP_ON;
    v_rec_patstudystat.originating_sitecode := v_originating_sitecode;
    v_rec_patstudystat.current_stat := j.current_stat;

   --store each record in a table
      v_table_patstudystat.EXTEND;
   v_table_patstudystat(v_cnt) := v_rec_patstudystat;
   v_cnt := v_table_patstudystat.COUNT();


  END IF;


 END LOOP;
    -- populate out cursor
  OPEN  O_RESULT_PATSTUDYSTAT
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_patstudystat AS tab_er_patstudystat));

 ---------get patient treatment arms


 v_pattxarm_table_name := 'er_pattxarm';


 v_cnt := 0;

 FOR j IN (
   SELECT  PK_pattxarm ,
    FK_Patprot     ,
   fk_studytxarm,
   tx_drug_info,
   tx_start_date,
   tx_end_date,
   notes,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,
   NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE
   FROM ER_PATTXARM, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_pattxarm_table_name  AND
   ER_PATTXARM.pk_pattxarm =  RP_TABLEPK AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND rp_processedflag = 1 AND NVL(RP_RECORD_TYPE,'O') <> 'D'
   UNION
   SELECT  RP_TABLEPK  PK_pattxarm,
   NULL,NULL,NULL,NULL,NULL,NULL,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE
   FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_pattxarm_table_name  AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND NVL(rp_record_type,'O') = 'D' AND rp_processedflag = 1

   )
 LOOP
  v_cnt := v_cnt + 1;

  v_site_code := j.RP_SITECODE;

  --get sponsor study id
  SP_GET_SPONSORPK(j.fk_studytxarm , 'er_studytxarm' , v_site_code,  v_sponsor_txarm ,v_err);

  Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_pattxarm row ' || v_cnt , v_success );

  Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: er_pattxarm v_sponsor_txarm ' || v_sponsor_txarm , v_success );

  IF v_sponsor_txarm > 0 or NVL(j.rp_record_type,'O') = 'D' THEN

   v_rec_pattxarm.pk_pattxarm  := j.PK_pattxarm ;
   v_rec_pattxarm.SITE_CODE := v_site_code;
   v_rec_pattxarm.FK_SPONSOR_ACCOUNT := j.FK_SPONSOR_ACCOUNT;
   v_rec_pattxarm.RECORD_TYPE := j.RP_RECORD_TYPE;
   v_rec_pattxarm.FK_Patprot := j.FK_Patprot;
   v_rec_pattxarm.fk_studytxarm := v_sponsor_txarm;
   v_rec_pattxarm.tx_drug_info := j.tx_drug_info;
   v_rec_pattxarm.tx_start_date := j.tx_start_date;
   v_rec_pattxarm.tx_end_date := j.tx_end_date;
   v_rec_pattxarm.notes := j.notes;
   v_rec_pattxarm.originating_sitecode := v_originating_sitecode;


   --store each record in a table
   v_table_pattxarm.EXTEND;
   v_table_pattxarm(v_cnt) := v_rec_pattxarm;
   v_cnt := v_table_pattxarm.COUNT();


  END IF;


 END LOOP;
    -- populate out cursor
  OPEN  O_RESULT_PATXARM
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_pattxarm AS TAB_PAT_TXARMS));



 --end of patient treatment arms


  -- get the 'patient all labs' setting for the study
  BEGIN
   IF v_study_export_settings IS NOT NULL THEN
      v_exportall := NVL(v_study_export_settings.EXTRACT('/modules/labs/exportAll/text()').getNumberVal(),0);
   END IF;
  EXCEPTION WHEN OTHERS THEN
     v_exportall := 0;
      Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'FATAL','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATENROLL: could not read export settings ' || SQLERRM , v_success );
  END ;

  -- if export all labs, then get all patient labs using the settings;

  IF v_exportall =  1 THEN

     v_lowerLimitNumberOfDays := NVL(v_study_export_settings.EXTRACT('/modules/labs/lowerLimitNumberOfDays/text()').getNumberVal(),0);
     v_upperLimitNumberOfDays := NVL(v_study_export_settings.EXTRACT('/modules/labs/upperLimitNumberOfDays/text()').getNumberVal(),0);
     v_upperLimitEnrollStatus := v_study_export_settings.EXTRACT('/modules/labs/upperLimitEnrollStatus/text()').getStringVal();

   -- get data
      v_cnt := 0;

     v_patstat_codelst_type :=    'patStatus';

     SELECT pk_codelst INTO v_stat
     FROM ER_CODELST WHERE trim(codelst_type) = v_patstat_codelst_type AND trim(codelst_subtyp) = trim(v_upperLimitEnrollStatus);

   FOR i IN ( SELECT * FROM (SELECT   RP_SITECODE , FK_SPONSOR_ACCOUNT,
   NVL((SELECT MAX(PATSTUDYSTAT_DATE) FROM ER_PATSTUDYSTAT ps WHERE ps.fk_study = v_study AND
   ps.fk_per = ER_PATPROT.fk_per AND  FK_CODELST_STAT = v_stat ),SYSDATE) PATSTUDYSTAT_UPPER_DATE,
   PK_PATLABS , ER_PATPROT.FK_PER, (SELECT labtest_shortname FROM ER_LABTEST WHERE pk_labtest = FK_TESTID ) FK_TESTID_SHORTNAME,
   TEST_DATE, TEST_RESULT,  TEST_RESULT_TX,  TEST_TYPE,TEST_UNIT, TEST_RANGE, TEST_LLN,  TEST_ULN, FK_SITE,
      (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_TSTSTAT) FK_CODELST_TSTSTAT_SUBTYP ,
    ACCESSION_NUM  , FK_TESTGROUP,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_ABFLAG) FK_CODELST_ABFLAG_SUBTYP,
    FK_PATPROT,  FK_EVENT_ID,  CUSTOM001,CUSTOM002,  CUSTOM003, CUSTOM004,  CUSTOM005, CUSTOM006,
    CUSTOM007, CUSTOM008,  CUSTOM009,  CUSTOM010, NOTES,ER_NAME ,ER_GRADE,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst =  FK_CODELST_STDPHASE)  FK_CODELST_STDPHASE_SUBTYP
      FROM ER_PATPROT, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR, ER_PATLABS
      WHERE ER_REV_PENDINGDATA.fk_study = v_study AND
      RP_TABLENAME = v_enroltable_name  AND pk_patprot = RP_TABLEPK AND
   ER_PATPROT.LAST_MODIFIED_DATE IS NULL AND ER_PATPROT.LAST_MODIFIED_BY IS NULL AND
       FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study AND NVL(rp_processedflag,0) = 1 AND
   ER_PATLABS.fk_per = ER_PATPROT.fk_per AND
   TEST_DATE >=  PATPROT_ENROLDT - v_lowerLimitNumberOfDays ) cond1
   WHERE cond1.TEST_DATE <= cond1.PATSTUDYSTAT_UPPER_DATE +  v_upperLimitNumberOfDays  )
   LOOP
    v_cnt := v_cnt + 1;

      v_site_code := i.RP_SITECODE;


          v_rec_patlabs.PK_PATLABS := i.PK_PATLABS;
      v_rec_patlabs.FK_PER := i.FK_PER;
      v_rec_patlabs.FK_TESTID_SHORTNAME := i. FK_TESTID_SHORTNAME;
      v_rec_patlabs.TEST_DATE := i.TEST_DATE;
      v_rec_patlabs.TEST_RESULT := i.TEST_RESULT;
      v_rec_patlabs.TEST_RESULT_TX := i.TEST_RESULT_TX;
      v_rec_patlabs.TEST_TYPE := i.TEST_TYPE;
      v_rec_patlabs.TEST_UNIT := i.TEST_UNIT;
      v_rec_patlabs.TEST_RANGE := i.TEST_RANGE;
      v_rec_patlabs.TEST_LLN := i.TEST_LLN;
      v_rec_patlabs.TEST_ULN := i.TEST_ULN;
      v_rec_patlabs.FK_SITE := i.FK_SITE;

      v_rec_patlabs.FK_CODELST_TSTSTAT_SUBTYP := i.FK_CODELST_TSTSTAT_SUBTYP;
      v_rec_patlabs.ACCESSION_NUM := i.ACCESSION_NUM;
      v_rec_patlabs.FK_TESTGROUP := i.FK_TESTGROUP;

      v_rec_patlabs.FK_CODELST_ABFLAG_SUBTYP := i.FK_CODELST_ABFLAG_SUBTYP;
      v_rec_patlabs.FK_PATPROT := i.FK_PATPROT;
      v_rec_patlabs.FK_EVENT_ID := i.FK_EVENT_ID;
      v_rec_patlabs.CUSTOM001 := i.CUSTOM001;
      v_rec_patlabs.CUSTOM002 := i.CUSTOM002;
      v_rec_patlabs.CUSTOM003 := i.CUSTOM003;
      v_rec_patlabs.CUSTOM004 := i.CUSTOM004;
      v_rec_patlabs.CUSTOM005 := i.CUSTOM005;
      v_rec_patlabs.CUSTOM006 := i.CUSTOM006;
      v_rec_patlabs.CUSTOM007 := i.CUSTOM007;
       v_rec_patlabs.CUSTOM008 := i.CUSTOM008;
      v_rec_patlabs.CUSTOM009 := i.CUSTOM009;
      v_rec_patlabs.CUSTOM010 := i.CUSTOM010;

      v_rec_patlabs.NOTES := i.NOTES;
      v_rec_patlabs.ER_NAME  := i.ER_NAME ;
      v_rec_patlabs.ER_GRADE := i.ER_GRADE;

      v_rec_patlabs.FK_CODELST_STDPHASE_SUBTYP := i.FK_CODELST_STDPHASE_SUBTYP;

      v_rec_patlabs.SITE_CODE := v_site_code;
      v_rec_patlabs.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;
      v_rec_patlabs.originating_sitecode := v_originating_sitecode;

     --store each record in a table
           v_table_patlabs.EXTEND;
     v_table_patlabs(v_cnt) := v_rec_patlabs;
     v_cnt := v_table_patlabs.COUNT();

   END LOOP ;
  END IF; -- end if for v_exportall =  1

   OPEN  O_RESULT_PATLABS
       FOR
        SELECT *
        FROM TABLE( CAST(v_table_patlabs AS tab_er_patlabs));

      ----------------------------------------

    END SP_EXP_PATENROLL;

  PROCEDURE SP_EXP_COMMONDATA (
     P_EXPID          NUMBER,
      O_RESULT_USER     OUT  Gk_Cv_Types.GenericCursorType,
      O_RESULT_SITE   OUT  Gk_Cv_Types.GenericCursorType
    )
   /* Author: Sonia Sahni
   Date: 04/19/04
   Purpose - Returns a ref cursor of common data like users, site
   */
   AS
   v_user_table_name VARCHAR2(15);
   v_study VARCHAR2(4000);

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
   v_sitecode VARCHAR2(20);
  v_module_subtype VARCHAR2(20);

   BEGIN

    --get export request

 Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

 --find out the study id from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);


   --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);

  v_module_subtype := 'pat_common';

    UPDATE ER_REV_PENDINGDATA
 SET rp_processedflag = 1
 WHERE rp_module = v_module_subtype AND rp_sitecode = v_sitecode;

 COMMIT;


   --get sites

    v_user_table_name := 'er_user';


 OPEN  O_RESULT_USER
     FOR
   SELECT   RP_TABLEPK PK_USER,
  USR_LASTNAME, USR_FIRSTNAME,
  USR_MIDNAME,
  ER_USER.last_modified_date,
  ER_USER.created_on,
  RD_SENT_ON,
  add_email,
  FK_SITEID,
  SITE_NAME ,
  RP_SITECODE ,
  FK_SPONSOR_ACCOUNT,
  USR_STAT,
  ER_USER.FK_PERADD
   FROM
     (SELECT DISTINCT  RP_TABLEPK,RP_SITECODE,FK_SPONSOR_ACCOUNT
     FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR WHERE  fk_study = v_study AND RP_TABLENAME   =  v_user_table_name AND fk_study = FK_SITESTUDY )  pendingTab ,
   ER_USER,
   (SELECT MAX(RD_SENT_ON) RD_SENT_ON, RD_TABLEPK
  FROM ER_REV_DATASENT WHERE fk_study = v_study  AND RD_TABLENAME   =  v_user_table_name
  GROUP BY  RD_TABLEPK
   ) sentTab,  ER_ADD,
  ER_SITE
  WHERE sentTab.RD_TABLEPK (+) = pendingTab.RP_TABLEPK AND
  ER_USER.pk_user = pendingTab.RP_TABLEPK AND
   pk_Add = ER_USER.fk_peradd AND
   pk_site = FK_SITEID
    AND
   (  RD_SENT_ON  <   NVL(ER_USER.last_modified_date,ER_USER.created_on)
  OR RD_SENT_ON IS NULL
  OR RD_SENT_ON  <   NVL(ER_ADD.last_modified_date,ER_ADD.created_on) ) ;


   END SP_EXP_COMMONDATA;

   PROCEDURE SP_GET_EXPSTUDIES (
      O_RESULT_STUDIES    OUT  Gk_Cv_Types.GenericCursorType
    )
 AS
  /* Author: Sonia Sahni
     Date: 04/20/04
    Purpose - Returns a ref cursor of studies ready for export
      */
 BEGIN

  --Select studies ready for export
  -- Also get the data count from erv_pendingdata.

 OPEN  O_RESULT_STUDIES
     FOR
   SELECT   pk_rev_studysponsor, A.FK_STUDY FK_STUDY, A.FK_MODULE FK_MODULE ,B.MODULE_SUBTYPE MODULE_SUBTYPE,A.PK_RS PK_RS,(SELECT COUNT(*) FROM ER_REV_PENDINGDATA
                     WHERE fk_study = A.fk_study AND RP_MODULE = B.MODULE_SUBTYPE) DATA_COUNT ,
  A.RS_INTERVAL RS_INTERVAL, A.RS_INTERVAL_UNIT RS_INTERVAL_UNIT, revSponsor.SITE_CODE SITE_CODE,revSponsor.backup_msgfolder backup_msgfolder
   FROM ER_REVMODEXP_SETUP A, ER_MODULES B , ER_REV_STUDYSPONSOR revSponsor
   WHERE (RS_NEXT_EXPORT_DUE < SYSDATE OR
   RS_NEXT_EXPORT_DUE IS NULL) AND
   PK_MODULE = FK_MODULE AND
   revSponsor.FK_SITESTUDY =  FK_STUDY AND revSponsor.pk_rev_studysponsor = fk_rev_studysponsor
   ORDER BY pk_rev_studysponsor, FK_STUDY, MODULE_SEQUENCE        ;


 END;

 PROCEDURE SP_SET_NEXT_EXPDATA (p_arr_studysponsor ARRAY_STRING, p_arr_modules ARRAY_STRING, p_arr_intervals ARRAY_STRING, p_arr_interval_units ARRAY_STRING, o_str OUT VARCHAR2)
 AS
 /* Author - Sonia Sahni
    Date - 21 Apr 04
   Purpose - Calculates next export due date for reverse A2A studies and modules */

   v_count NUMBER;
   v_counter NUMBER := 1;
   v_study NUMBER;
   v_module NUMBER;
   v_interval NUMBER;
     v_interval_unit VARCHAR2(2);

   v_return_String LONG;

   v_date DATE;
   v_next_date DATE;

   v_site_code VARCHAR2(20);
   v_studysponsorpk NUMBER;

 BEGIN

   v_count := p_arr_studysponsor.COUNT();

   v_date := SYSDATE;

   BEGIN

    FOR i IN 1..v_count
   LOOP
    v_studysponsorpk := p_arr_studysponsor(i);

    v_module := p_arr_modules(i);
       v_interval := p_arr_intervals(i);
    v_interval_unit := p_arr_interval_units(i);

    IF v_interval = NULL THEN
       v_interval := 5;
    END IF;

    IF v_interval_unit = NULL THEN
       v_interval_unit := 'M';
    END IF;


       IF  v_interval_unit = 'M' THEN -- The interval unit is Minutes

        SELECT v_date + v_interval/(24*60)
      INTO v_next_date
      FROM dual;

    ELSIF v_interval_unit = 'H' THEN -- The interval unit is Hours

        SELECT v_date + v_interval/24
      INTO v_next_date
      FROM dual;


    ELSE --The interval unit is days

      SELECT v_date + v_interval
      INTO v_next_date
      FROM dual;

    END IF;

    -- get study and site_code

    SELECT fk_sitestudy, site_code
    INTO v_study, v_site_code
    FROM ER_REV_STUDYSPONSOR
    WHERE pk_rev_studysponsor = v_studysponsorpk;

    UPDATE  ER_REVMODEXP_SETUP
    SET RS_LAST_EXPORTED_ON = v_date,
        RS_NEXT_EXPORT_DUE = v_next_date
    WHERE fk_rev_studysponsor = v_studysponsorpk AND
    fk_study = v_study AND fk_module = v_module;

    -- update the processed data flag

     UPDATE ER_REV_PENDINGDATA
     SET rp_processedflag = 2
     WHERE rp_module = (SELECT module_subtype FROM ER_MODULES WHERE pk_module = v_module) AND
     rp_processedflag = 1 AND rp_sitecode = v_site_code;


   END LOOP;

   COMMIT;

      EXCEPTION WHEN OTHERS THEN
     o_str := SQLERRM;
    END ;
 END;

 -------------------------------------------------------------------------------------


 PROCEDURE SP_IMP_PATCOMMON (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  04/27/04
    Purpose : Import common data from temp tables
  */

 v_sql_user LONG;
 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_user NUMBER;
 v_orig_site NUMBER;
 v_site_name VARCHAR2(50);
 v_fk_account_sponsor NUMBER;
 v_new_user NUMBER;

 v_err VARCHAR2(4000);
 v_new_site NUMBER;


 V_EXPID NUMBER;

 v_success NUMBER;

 v_new_Add NUMBER;
 v_timezone NUMBER;

 v_lastname VARCHAR2(30);
 v_firstname VARCHAR2(30);
 v_midname VARCHAR2(30);
 v_addemail VARCHAR2(100);
 v_site_code VARCHAR2(20);
 v_usr_stat CHAR(1);
 v_orig_useradd NUMBER;
 v_new_useradd NUMBER;


 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;

   v_sql_user := 'SELECT  PK_USER ,USR_LASTNAME, USR_FIRSTNAME,  USR_MIDNAME,  ADD_EMAIL,
      FK_SITEID,  SITE_NAME ,  RP_SITECODE, FK_SPONSOR_ACCOUNT, USR_STAT, FK_PERADD  FROM IMPPAT_COMMON0';


  --get data

   OPEN v_cur FOR v_sql_user;
    LOOP

     FETCH v_cur INTO v_orig_user, v_lastname, v_firstname, v_midname, v_addemail,
    v_orig_site, v_site_name,v_site_code,v_fk_account_sponsor,v_usr_stat,v_orig_useradd  ;

      EXIT WHEN v_cur %NOTFOUND;

     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_user, 'er_user',v_site_code, v_new_user,v_err,v_fk_account_sponsor);
       Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site, 'er_site',v_site_code, v_new_site,v_err,v_fk_account_sponsor);
        Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_useradd, 'er_add',v_site_code, v_new_useradd,v_err,v_fk_account_sponsor);

     ---------

     IF v_new_site = 0 THEN -- site does not exist

    Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON - New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site , v_success );

    SELECT seq_er_site.NEXTVAL INTO v_new_site FROM dual;

    Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_site, 'er_site',v_site_code,  v_new_site, v_err ,v_fk_account_sponsor);

    IF LENGTH(trim(v_err)) > 0 THEN

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON Could not create record for er_sponsorpkmap :' || v_err, v_success );

    ELSE
     BEGIN

      SELECT seq_er_add.NEXTVAL INTO v_new_add FROM dual;

      INSERT INTO ER_ADD (PK_ADD, CREATED_ON)
      VALUES (v_new_add, SYSDATE);

      INSERT INTO ER_SITE ( PK_SITE, FK_ACCOUNT,  FK_PERADD , SITE_NAME, CREATED_ON)
      VALUES (v_new_site,v_fk_account_sponsor,v_new_add,v_site_name,SYSDATE ) ;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
       REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
     SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
     v_orig_site,SYSDATE, 'pat_common', 'N');


      EXCEPTION WHEN OTHERS THEN
       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON:Could not create New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site || ' Error: ' || SQLERRM, v_success );
      END;

     END IF;

     ELSIF v_new_site > 0 THEN -- old record

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON - Old Site record for site '|| v_site_code || ' with original PK:' || v_orig_site , v_success );

      UPDATE ER_SITE
      SET SITE_NAME = v_site_name, FK_ACCOUNT = v_fk_account_sponsor,
       LAST_MODIFIED_DATE = SYSDATE
         WHERE pk_site = v_new_site;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
       REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
     SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
     v_orig_site,SYSDATE, 'pat_common', 'M');


      ELSIF v_new_site = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON:' || v_err, v_success );

       -- end if for v_new_site = 0
       END IF;

      IF v_new_user = 0 AND v_new_site > 0 THEN -- new record

         SELECT seq_er_user.NEXTVAL INTO v_new_user FROM dual;

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON - New User record for site '|| v_site_code || ' with original PK:' || v_orig_user , v_success );

      Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_user, 'er_user',v_site_code,  v_new_user,v_err,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

         SELECT seq_er_add.NEXTVAL INTO v_new_useradd FROM dual;

         Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_useradd, 'er_add',v_site_code,  v_new_useradd, v_err,v_fk_account_sponsor);

         IF LENGTH(trim(v_err)) > 0 THEN

          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON Could not create record for er_sponsorpkmap :' || v_err, v_success );

        ELSE


           INSERT INTO ER_ADD (PK_ADD,ADD_EMAIL,CREATED_ON)
         VALUES (v_new_useradd, v_addemail, SYSDATE);

         INSERT INTO ER_USER ( PK_USER, FK_ACCOUNT, FK_SITEID,  USR_LASTNAME,
         USR_FIRSTNAME,USR_TYPE ,USR_STAT , FK_PERADD, CREATED_ON)
         VALUES (v_new_user,v_fk_account_sponsor,v_new_site,v_lastname,
          v_firstname, 'N','A',v_new_useradd, SYSDATE ) ;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
       REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
      SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_user',
      v_orig_user,SYSDATE, 'pat_common', 'N');


         END IF;

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON:Could not create New User record for site '|| v_site_code || ' with original PK:' || v_orig_user || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

      ELSIF v_new_user > 0 AND v_new_site > 0 THEN -- old record

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON - Old User record for site '|| v_site_code || ' with original PK:' || v_orig_user , v_success );

      UPDATE ER_USER
      SET FK_SITEID = v_new_site,  USR_LASTNAME = v_lastname,
       USR_FIRSTNAME = v_firstname,USR_STAT = v_usr_stat  ,
       LAST_MODIFIED_DATE = SYSDATE
         WHERE pk_user = v_new_user;

     INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
       REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
      SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_user',
      v_orig_user,SYSDATE, 'pat_common', 'M');


      UPDATE ER_ADD
      SET ADD_EMAIL = v_addemail,
       LAST_MODIFIED_DATE = SYSDATE
      WHERE PK_ADD = v_new_useradd;



      ELSIF v_new_user = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATCOMMON:' || v_err, v_success );

      END IF;


     ---------



   END LOOP;

  CLOSE v_cur ;
  COMMIT;

 END;

 -------------------------------------------------------------------------------------

 PROCEDURE SP_IMP_PATENROLL (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  04/26/04
    Purpose : Import patient enrollment data from temp tables
  */

 v_sql_patient LONG;
 v_sql_enrol LONG;
 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_per NUMBER;
 v_per_code VARCHAR2(20);
 v_orig_site NUMBER;
 v_person_deathdt DATE;
 v_pat_stat_subtype VARCHAR2(20);
 v_site_code VARCHAR2(20);
 v_sponsor_code VARCHAR2(20);
 v_site_name VARCHAR2(50);
 v_pat_dob DATE;
 v_fk_account_sponsor NUMBER;
 v_new_per NUMBER;
 v_err VARCHAR2(4000);
 v_new_site NUMBER;
 v_patstat NUMBER;
 V_EXPID NUMBER;
 v_success NUMBER;
 v_new_Add NUMBER;
 v_timezone NUMBER;


 v_orig_patprot NUMBER;
 v_orig_protocol NUMBER;
 v_orig_study NUMBER;
 v_orig_user NUMBER;
 v_patprot_enroldt DATE;
 v_patprot_start DATE;
 v_patprot_notes  VARCHAR2(4000);
 v_patprot_stat NUMBER;
 v_patprot_discdate DATE;
 v_patprot_reason  VARCHAR2(2000);
 v_patprot_schday NUMBER;

 v_patprot_patstdid VARCHAR2(20);
 v_patprot_consigndt DATE;
 v_patprot_resigndt1 DATE;
 v_patprot_resigndt2 DATE;
 v_patprot_consign NUMBER;
 v_orig_user_assignto NUMBER;
 v_codelst_loc_subtype VARCHAR2(20);

 v_new_patprot NUMBER;
 v_new_protocol NUMBER;
 v_new_study NUMBER;
 v_new_user NUMBER;
 v_new_user_assignto NUMBER;
 v_codelstloc NUMBER;

 v_sql_patstudystat LONG;

 v_orig_patstudystat NUMBER;
 v_codelst_stat_subtype VARCHAR2(20);
 v_patstudystat_date DATE;
 v_patstudystat_endt DATE;
 v_patstudystat_note VARCHAR2(4000);
 v_patstudystat_reason_subtyp  VARCHAR2(20);

 v_new_patstudystat NUMBER;
 v_codelst_stat NUMBER;
 v_codelst_stat_reason NUMBER;
 v_current_stat Number;

 v_creator NUMBER;

 v_orig_physician NUMBER;
 v_new_physician NUMBER;
 v_patprot_random VARCHAR2(50);
 v_screen_number VARCHAR2(50);
 v_orig_screened_by NUMBER;
 v_new_screened_by NUMBER;
 v_screening_outcome_subtyp VARCHAR(20);
 v_code_screening_outcome NUMBER;

 v_code_type VARCHAR2(15);
     v_record_type CHAR (1);

  v_patient_site NUMBER;
  v_studysitecount NUMBER;

     v_patfacilityid VARCHAR2(30);
  vstat_inform_consent_ver VARCHAR2(255);
  vstat_next_followup_on TIMESTAMP;

  v_orig_site_enrolling NUMBER;
  v_new_site_enrolling NUMBER;
  v_site_enrolling_name VARCHAR2(50);

  v_orig_patfacility NUMBER;
   v_new_patfacility NUMBER;
   v_patfacility_id VARCHAR2(20);
    v_patfacility_regdate DATE ;
     v_patfacilty_acc NUMBER;
  v_patfacility_default NUMBER;
   v_isreadonly NUMBER;

    v_code_enroll_pending NUMBER;
   v_code_enroll_appr NUMBER;
    v_patstat_site_enrolling NUMBER;
    v_is_reviewbased NUMBER;
    v_aut_enddt DATE;
   v_person_lname VARCHAR2(30);
  v_person_fname VARCHAR2(30);
  v_person_mname VARCHAR2(30);
  v_gender_subtyp VARCHAR2(20); v_gender_pk NUMBER;
  v_marital_subtyp VARCHAR2(20); v_marital_pk NUMBER;
  v_bloodgrp_subtyp VARCHAR2(20); v_bloodgr_pk NUMBER;
  v_ssn  VARCHAR2(16);
  v_person_email VARCHAR2(100);   v_person_address1 VARCHAR2(100);
  v_person_address2 VARCHAR2(100); v_person_city VARCHAR2(100);  v_person_state VARCHAR2(100);  v_person_country VARCHAR2(100);
  v_person_zip VARCHAR2(20); v_person_hphone VARCHAR2(100);   v_person_bphone VARCHAR2(100);
  v_race_subtyp VARCHAR2(20);  v_add_race_subtyp VARCHAR2(4000);  v_ethnicity_subtyp VARCHAR2(20);  v_add_ethnicity_subtyp VARCHAR2(4000);
  v_employment_subtyp VARCHAR2(20); v_edu_subtyp VARCHAR2(20); v_person_notes  VARCHAR2(4000); v_person_death_subtyp VARCHAR2(20);
  v_person_cause_death VARCHAR2(2000);
  v_race_pk NUMBER; v_add_race_str VARCHAR2(500); v_ethnicity_pk NUMBER; v_add_ethnicity_str VARCHAR(500);v_emp_pk NUMBER; v_edu_pk NUMBER;
  v_person_death_pk NUMBER;
  v_county VARCHAR2(100);
  v_fk_codelst_ptst_eval_flag VARCHAR2(20);  v_fk_codelst_ptst_eval_flag_pk NUMBER;
  v_fk_codelst_ptst_eval VARCHAR2(20); v_fk_codelst_ptst_eval_pk NUMBER;
  v_fk_codelst_ptst_ineval VARCHAR2(20); v_fk_codelst_ptst_ineval_pk  NUMBER;
  v_fk_codelst_ptst_survival VARCHAR2(20); v_fk_codelst_ptst_survival_pk NUMBER;
  v_fk_codelst_ptst_dth_stdrel VARCHAR2(20); v_fk_code_ptst_dth_stdrel_pk NUMBER;
  v_death_std_rltd_other VARCHAR2(2000);
  v_date_of_death DATE;

  v_originating_sitecode Varchar2(10);
  v_enroll_record_type Char(1);
v_fk_per  Number;
  v_automated_enr_setting NUMBER := 2; -- 2 means it doesnt have a value populated, otherwise possible values can be 0, 1

  v_sql_pattx Varchar2(2000);
v_orig_pattxarm Number;
v_new_pattxarm Number;
v_orig_studytxarm Number;
v_new_studytxarm Number;

v_drug_info Varchar2(250);

v_tx_startdate date;
v_tx_enddate date;
v_tx_notes Varchar2(4000);



 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;

   v_sql_patient := 'SELECT PK_PER, PER_CODE, FK_SITE,PERSON_DEATHDT, PAT_STAT_SUBTYPE,
         RP_SITECODE,SITE_NAME,PERSON_DOB, FK_SPONSOR_ACCOUNT,  FK_TIMEZONE   , pat_facilityid ,person_lname,person_fname,person_mname,
        PAT_FK_CODELST_GENDER, PAT_FK_CODELST_MARITAL, PAT_FK_CODELST_BLOODGRP,PERSON_SSN, person_email,
        person_address1,person_address2,person_city,person_state, person_country,person_zip,person_hphone,
       person_bphone, PAT_FK_CODELST_RACE, person_add_race_subtypes, PAT_FK_CODELST_ETHNICITY,
person_add_ethnicity_subtypes,PAT_FK_CODELST_EMPLOYMENT, PAT_FK_CODELST_EDU, person_notes, PAT_fk_codelst_pat_dth_cause, cause_of_death_other,person_county
         FROM IMPPAT_ENR1';


  --get data
  BEGIN
   OPEN v_cur FOR v_sql_patient;
    LOOP

     FETCH v_cur INTO v_orig_per, v_per_code, v_orig_site, v_person_deathdt, v_pat_stat_subtype,
    v_site_code,v_site_name,v_pat_dob, v_fk_account_sponsor, v_timezone,v_patfacilityid,  v_person_lname ,  v_person_fname ,  v_person_mname ,  v_gender_subtyp ,  v_marital_subtyp ,  v_bloodgrp_subtyp , v_ssn  ,  v_person_email ,
      v_person_address1 ,  v_person_address2 ,  v_person_city ,v_person_state ,v_person_country ,
    v_person_zip , v_person_hphone , v_person_bphone ,  v_race_subtyp ,v_add_race_subtyp , v_ethnicity_subtyp ,v_add_ethnicity_subtyp ,
    v_employment_subtyp , v_edu_subtyp , v_person_notes  , v_person_death_subtyp ,  v_person_cause_death ,v_county;

     EXIT WHEN v_cur %NOTFOUND;

    --get creator
    SELECT AC_USRCREATOR
    INTO v_creator
    FROM ER_ACCOUNT WHERE pk_account = v_fk_account_sponsor;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err,v_fk_account_sponsor);
       Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site, 'er_site',v_site_code, v_new_site,v_err,v_fk_account_sponsor);

     ---------

     -- get patient status code

     v_code_type := 'patient_status';

     SELECT Pkg_Impex.getCodePk (v_pat_stat_subtype, v_code_type)
     INTO v_patstat
     FROM dual;

     IF v_patstat = -1 THEN
      v_patstat := NULL;
    END IF;

    --gender
     SELECT Pkg_Impex.getCodePk (v_gender_subtyp, 'gender')   INTO v_gender_pk  FROM dual;
     IF v_gender_pk = -1 THEN
      v_gender_pk := NULL;
    END IF;

    -- marital status
     SELECT Pkg_Impex.getCodePk (v_marital_subtyp, 'marital_st' )   INTO v_marital_pk  FROM dual;
     IF v_marital_pk = -1 THEN
      v_marital_pk := NULL;
    END IF;

     -- blood group    ,'bloodgr'
     SELECT Pkg_Impex.getCodePk (v_bloodgrp_subtyp, 'bloodgr' )   INTO v_bloodgr_pk FROM dual;
      IF v_bloodgr_pk  = -1 THEN
       v_bloodgr_pk := NULL;
     END IF;

    -- race  'race',
     SELECT Pkg_Impex.getCodePk (v_race_subtyp , 'race' )   INTO v_race_pk FROM dual;
      IF v_race_pk  = -1 THEN
       v_race_pk := NULL;
     END IF;

     --ethnicity  'ethnicity', v_ethnicity_pk
     SELECT Pkg_Impex.getCodePk (v_ethnicity_subtyp , 'ethnicity' )   INTO v_ethnicity_pk  FROM dual;
      IF v_ethnicity_pk  = -1 THEN
       v_ethnicity_pk  := NULL;
     END IF;

     --v_employment_subtyp , 'employment' v_emp_pk
      SELECT Pkg_Impex.getCodePk (v_employment_subtyp , 'employment' )   INTO v_emp_pk  FROM dual;
      IF v_emp_pk = -1 THEN
       v_emp_pk  := NULL;
     END IF;

     --v_edu_subtyp , 'education' v_edu_pk
     SELECT Pkg_Impex.getCodePk (v_edu_subtyp , 'education' )   INTO v_edu_pk  FROM dual;
      IF v_edu_pk = -1 THEN
       v_edu_pk  := NULL;
     END IF;

     --v_person_death_subtyp 'pat_dth_cause' v_person_death_pk
     SELECT Pkg_Impex.getCodePk (v_person_death_subtyp , 'pat_dth_cause' )   INTO v_person_death_pk FROM dual;

      IF v_person_death_pk = -1 THEN
       v_person_death_pk  := NULL;
     END IF;

      SELECT Pkg_Util.f_getMultipleCodePks (v_add_race_subtyp,',' , ',' ,'race') INTO  v_add_race_str FROM dual;

      SELECT Pkg_Util.f_getMultipleCodePks (v_add_ethnicity_subtyp ,',' , ',' ,'ethnicity') INTO  v_add_ethnicity_str FROM dual;


     IF v_new_site = 0 THEN -- site does not exist

    Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site , v_success );

    SELECT seq_er_site.NEXTVAL INTO v_new_site FROM dual;

    Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_site, 'er_site',v_site_code,  v_new_site, v_err,v_fk_account_sponsor);

    IF LENGTH(trim(v_err)) > 0 THEN

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

    ELSE
     BEGIN

      SELECT seq_er_add.NEXTVAL INTO v_new_add FROM dual;

      INSERT INTO ER_ADD (PK_ADD,CREATOR)
      VALUES (v_new_add,v_creator);

      INSERT INTO ER_SITE ( PK_SITE, FK_ACCOUNT,  FK_PERADD , SITE_NAME, CREATED_ON,CREATOR)
      VALUES (v_new_site,v_fk_account_sponsor,v_new_add,v_site_name,SYSDATE,v_creator ) ;

      INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
     SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
     v_orig_site,SYSDATE, 'pat_enr', 'N');

      EXCEPTION WHEN OTHERS THEN
       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site || ' Error: ' || SQLERRM, v_success );
      END;

     END IF;

     ELSIF v_new_site > 0 THEN -- old record

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Site record for site '|| v_site_code || ' with original PK:' || v_orig_site , v_success );

      UPDATE ER_SITE
      SET SITE_NAME = v_site_name, FK_ACCOUNT = v_fk_account_sponsor,
       LAST_MODIFIED_DATE = SYSDATE
         WHERE pk_site = v_new_site;

      INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
     SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
     v_orig_site,SYSDATE, 'pat_enr', 'M');

      ELSIF v_new_site = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );

       -- end if for v_new_site = 0
       END IF;

      IF v_new_per = 0 AND v_new_site > 0 THEN -- new record
         SELECT seq_er_per.NEXTVAL INTO v_new_per FROM dual;

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Patient record for site '|| v_site_code || ' with original PK:' || v_orig_per , v_success );

      Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_per, 'er_per',v_site_code,  v_new_per,v_err,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

        /*INSERT INTO ER_PER ( PK_PER,  PER_CODE,  FK_ACCOUNT, FK_SITE,  CREATED_ON,CREATOR)
        VALUES (v_new_per,v_per_code,v_fk_account_sponsor,v_new_site,sysdate ,v_creator) ; */

        INSERT INTO PERSON(PK_PERSON,PERSON_CODE ,FK_SITE  , PERSON_DEATHDT , PERSON_DOB, FK_CODELST_PSTAT, CREATED_ON, FK_TIMEZONE ,CREATOR, FK_ACCOUNT,pat_facilityid,
       person_lname,person_fname,person_mname, FK_CODELST_GENDER, FK_CODELST_MARITAL, FK_CODELST_BLOODGRP,
       PERSON_SSN, person_email,  person_address1,person_address2,person_city,person_state, person_country,person_zip,person_hphone,
       person_bphone,FK_CODELST_RACE, person_add_race, FK_CODELST_ETHNICITY, person_add_ethnicity,FK_CODELST_EMPLOYMENT,
       FK_CODELST_EDU, person_notes, fk_codelst_pat_dth_cause, cause_of_death_other,person_county  )
        VALUES (v_new_per,v_per_code,v_new_site,v_person_deathdt, v_pat_dob,v_patstat,SYSDATE, v_timezone, v_creator , v_fk_account_sponsor,v_patfacilityid,
        v_person_lname ,  v_person_fname ,  v_person_mname ,  v_gender_pk, v_marital_pk ,  v_bloodgr_pk , v_ssn  ,  v_person_email ,
           v_person_address1 ,  v_person_address2 ,  v_person_city ,v_person_state ,v_person_country , v_person_zip , v_person_hphone , v_person_bphone ,  v_race_pk ,
         v_add_race_str , v_ethnicity_pk ,v_add_ethnicity_str, v_emp_pk, v_edu_pk , v_person_notes  , v_person_death_pk ,  v_person_cause_death,
        v_county
         );

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_per',
        v_orig_per ,SYSDATE, 'pat_enr', 'N');

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Patient record for site '|| v_site_code || ' with original PK:' || v_orig_per || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

      ELSIF v_new_per > 0 AND v_new_site > 0 THEN -- old record

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Patient record for site '|| v_site_code || ' with original PK:' || v_orig_per , v_success );

      UPDATE ER_PER
      SET PER_CODE = v_per_code, FK_ACCOUNT = v_fk_account_sponsor,
       LAST_MODIFIED_DATE = SYSDATE, FK_TIMEZONE  = v_timezone, pat_facilityid = v_patfacilityid
         WHERE pk_per = v_new_per;


      UPDATE person
      SET PERSON_CODE = v_per_code ,FK_SITE = v_new_site , PERSON_DEATHDT = v_person_deathdt ,
      PERSON_DOB = v_pat_dob, FK_CODELST_PSTAT = v_patstat ,
      LAST_MODIFIED_DATE = SYSDATE,  pat_facilityid = v_patfacilityid,
      person_lname = v_person_lname ,person_fname = v_person_fname,person_mname = v_person_mname,
      FK_CODELST_GENDER = v_gender_pk, FK_CODELST_MARITAL =  v_marital_pk, FK_CODELST_BLOODGRP = v_bloodgr_pk,
      PERSON_SSN = v_ssn , person_email = v_person_email,  person_address1 = v_person_address1,person_address2 = v_person_address2,
      person_city = v_person_city,person_state = v_person_state, person_country = v_person_country,person_zip = v_person_zip , person_hphone = v_person_hphone,
       person_bphone =  v_person_bphone , FK_CODELST_RACE  = v_race_pk,
       person_add_race = v_add_race_str , FK_CODELST_ETHNICITY = v_ethnicity_pk , person_add_ethnicity = v_add_ethnicity_str,
       FK_CODELST_EMPLOYMENT = v_emp_pk, FK_CODELST_EDU = v_edu_pk,
       person_notes = v_person_notes , fk_codelst_pat_dth_cause = v_person_death_pk, cause_of_death_other = v_person_cause_death , person_county = v_county
         WHERE pk_PERSON = v_new_per;

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_per',
       v_orig_per ,SYSDATE, 'pat_enr', 'M');

      ELSIF v_new_per = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );

      END IF;


      ---------

   END LOOP;

  CLOSE v_cur ;
  EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || SQLERRM, v_success );

  END; -- for patient
 ------------------------------------------------------

 --------pat facility
  v_sql_patient := 'SELECT PK_PATFACILITY,    FK_PER,     FK_SITE,     PAT_FACILITYID,       PATFACILITY_REGDATE,    PATFACILITY_ACCESSRIGHT,   PATFACILITY_DEFAULT,   IS_READONLY
            FROM IMPPAT_ENR4';


  --get data
  BEGIN
   OPEN v_cur FOR v_sql_patient;
    LOOP

     FETCH v_cur INTO v_orig_patfacility, v_orig_per, v_orig_site, v_patfacility_id, v_patfacility_regdate, v_patfacilty_acc, v_patfacility_default,v_isreadonly  ;

     EXIT WHEN v_cur %NOTFOUND;

    --get creator
    SELECT AC_USRCREATOR
    INTO v_creator
    FROM ER_ACCOUNT WHERE pk_account = v_fk_account_sponsor;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err,v_fk_account_sponsor);
       Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site, 'er_site',v_site_code, v_new_site,v_err,v_fk_account_sponsor);
       Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patfacility, 'er_patfacility',v_site_code, v_new_patfacility,v_err,v_fk_account_sponsor);

     ---------

    IF v_new_per <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient facility  record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
     ELSIF v_new_site <= 0 THEN -- site does not exist
             Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient facility  record, site (' || v_site_code || ') does not exist. Original PK_SITE:' || v_orig_site, v_success );
    ELSE --all fk exists
      IF  v_new_patfacility = 0  THEN -- new record

         SELECT seq_er_patfacility.NEXTVAL INTO v_new_patfacility FROM dual;

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Patient Facility record for site '|| v_site_code || ' with original PK:' || v_orig_patfacility , v_success );

      Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patfacility, 'er_patfacility',v_site_code,  v_new_patfacility,v_err,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

       INSERT INTO ER_PATFACILITY (PK_PATFACILITY,    FK_PER,     FK_SITE,     PAT_FACILITYID,       PATFACILITY_REGDATE,    PATFACILITY_ACCESSRIGHT,
       PATFACILITY_DEFAULT,   IS_READONLY,CREATED_ON,CREATOR)
       VALUES(  v_new_patfacility,v_new_per,v_new_site,v_patfacility_id, v_patfacility_regdate, v_patfacilty_acc, v_patfacility_default,v_isreadonly,SYSDATE, v_creator ) ;

          INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_patfacility',
        v_orig_patfacility ,SYSDATE, 'pat_enr', 'N');

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Patient facility record for site '|| v_site_code || ' with original PK:' || v_orig_patfacility || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

      ELSIF v_new_patfacility  > 0 THEN -- old record

       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Patient facility record for site '|| v_site_code || ' with original PK:' || v_orig_patfacility , v_success );

      UPDATE ER_PATFACILITY
      SET FK_PER = v_new_per,     FK_SITE = v_new_site,     PAT_FACILITYID = v_patfacility_id,       PATFACILITY_REGDATE = v_patfacility_regdate,    PATFACILITY_ACCESSRIGHT = v_patfacilty_acc ,
       PATFACILITY_DEFAULT = v_patfacility_default,   IS_READONLY = v_isreadonly, LAST_MODIFIED_DATE = SYSDATE, LAST_MODIFIED_BY = v_creator
       WHERE pk_patfacility = v_new_patfacility;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_patfacility',
        v_orig_patfacility ,SYSDATE, 'pat_enr', 'M');

      ELSIF v_new_patfacility = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );

      END IF;

    END IF ;-- for all FKS
      ---------

   END LOOP;

  CLOSE v_cur ;
  EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || SQLERRM, v_success );

  END; -- for patient


 ------------------- end for patfacility

   v_sql_enrol := 'Select PK_PATPROT, FK_PROTOCOL,  FK_STUDY,  nvl(FK_USER,0) FK_USER,  PATPROT_ENROLDT,   FK_PER,  PATPROT_START, PATPROT_NOTES,
 PATPROT_STAT , PATPROT_DISCDT,  PATPROT_REASON,  PATPROT_SCHDAY,  FK_TIMEZONE,   PATPROT_PATSTDID,
    PATPROT_CONSIGNDT ,  PATPROT_RESIGNDT1,  PATPROT_RESIGNDT2, PATPROT_CONSIGN,  NVL(FK_USERASSTO,0) FK_USERASSTO,
    FK_CODELSTLOC,   SITE_CODE,  FK_SPONSOR_ACCOUNT,PATPROT_RANDOM, NVL(PATPROT_PHYSICIAN,0) PATPROT_PHYSICIAN,
 NVL(fk_site_enrolling,0) fk_site_enrolling , fk_site_enrolling_name, fk_codelst_ptst_eval_flag, fk_codelst_ptst_eval, fk_codelst_ptst_ineval,fk_codelst_ptst_survival,
 fk_codelst_ptst_dth_stdrel, death_std_rltd_other,date_of_death,originating_sitecode,record_type
    FROM IMPPAT_ENR0';

 BEGIN

  OPEN v_cur FOR  v_sql_enrol ;

   LOOP
    FETCH v_cur INTO
    v_orig_patprot,v_orig_protocol, v_orig_study, v_orig_user,v_patprot_enroldt,v_orig_per,v_patprot_start,v_patprot_notes,
  v_patprot_stat,v_patprot_discdate,v_patprot_reason, v_patprot_schday, v_timezone, v_patprot_patstdid,
  v_patprot_consigndt, v_patprot_resigndt1, v_patprot_resigndt2,v_patprot_consign,v_orig_user_assignto,
  v_codelst_loc_subtype, v_site_code, v_fk_account_sponsor,v_patprot_random ,v_orig_physician , v_orig_site_enrolling ,v_site_enrolling_name,
    v_fk_codelst_ptst_eval_flag, v_fk_codelst_ptst_eval, v_fk_codelst_ptst_ineval,v_fk_codelst_ptst_survival,
    v_fk_codelst_ptst_dth_stdrel, v_death_std_rltd_other,v_date_of_death,v_originating_sitecode,v_enroll_record_type;

    EXIT WHEN v_cur %NOTFOUND;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patprot, 'er_patprot',v_site_code, v_new_patprot,v_err,v_fk_account_sponsor);

      -- get the site code for the sponsor account


     SELECT site_code , AC_USRCREATOR
     INTO v_sponsor_code, v_creator
     FROM ER_ACCOUNT
     WHERE pk_account  = v_fk_account_sponsor;


    if (nvl(v_enroll_record_type,'O') <> 'D') then
         IF  v_orig_site_enrolling > 0 THEN
                      Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site_enrolling, 'er_site',v_site_code, v_new_site_enrolling,v_err,v_fk_account_sponsor);
         ELSE
        v_new_site_enrolling := NULL;
        END IF;

        IF v_orig_user > 0 THEN
           Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_user, 'er_user',v_site_code, v_new_user,v_err,v_fk_account_sponsor);
        END IF;

        IF  v_orig_user_assignto > 0 THEN
                 Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_user_assignto, 'er_user',v_site_code, v_new_user_assignto,v_err,v_fk_account_sponsor);
        END IF;

        IF v_orig_physician  > 0 THEN
                 Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_physician , 'er_user',v_site_code, v_new_physician ,v_err,v_fk_account_sponsor);
        END IF;

         -- for study and protocol, we need to get the SITE PK for the original sponsor's Study and Protocol


          -- get patient treatment location

           v_code_type :=  'treatloc';
           SELECT Pkg_Impex.getCodePk (v_codelst_loc_subtype,v_code_type)
           INTO v_codelstloc
           FROM dual;
            IF v_codelstloc = -1 THEN
              v_codelstloc := NULL;
           END IF;

           --evaluable flag 'pst_eval_flag'

           SELECT Pkg_Impex.getCodePk (v_fk_codelst_ptst_eval_flag ,'ptst_eval_flag')  INTO  v_fk_codelst_ptst_eval_flag_pk FROM dual;

            IF v_fk_codelst_ptst_eval_flag_pk= -1 THEN
              v_fk_codelst_ptst_eval_flag_pk  := NULL;
           END IF;

           ------------- 'ptst_eval'
           SELECT Pkg_Impex.getCodePk (v_fk_codelst_ptst_eval ,'ptst_eval')  INTO   v_fk_codelst_ptst_eval_pk FROM dual;

            IF  v_fk_codelst_ptst_eval_pk = -1 THEN
               v_fk_codelst_ptst_eval_pk  := NULL;
           END IF;

           --ineval 'ptst_ineval'
           SELECT Pkg_Impex.getCodePk (v_fk_codelst_ptst_ineval,'ptst_ineval')  INTO  v_fk_codelst_ptst_ineval_pk FROM dual;

            IF  v_fk_codelst_ptst_ineval_pk  = -1 THEN
              v_fk_codelst_ptst_ineval_pk   := NULL;
           END IF;

             --survival 'ptst_survival'
           SELECT Pkg_Impex.getCodePk (v_fk_codelst_ptst_survival ,'ptst_survival')  INTO  v_fk_codelst_ptst_survival_pk  FROM dual;

            IF v_fk_codelst_ptst_survival_pk = -1 THEN
              v_fk_codelst_ptst_survival_pk  := NULL;
           END IF;

             --study rel death  'ptst_dth_stdrel'
           SELECT Pkg_Impex.getCodePk (v_fk_codelst_ptst_dth_stdrel ,'ptst_dth_stdrel')  INTO  v_fk_code_ptst_dth_stdrel_pk  FROM dual;

            IF v_fk_code_ptst_dth_stdrel_pk = -1 THEN
              v_fk_code_ptst_dth_stdrel_pk  := NULL;
           END IF;

        end if; --for recordtype<>'D'

     SP_GET_SITEPK(v_orig_study, 'er_study',v_sponsor_code, v_new_study,v_err,v_originating_sitecode);
     SP_GET_SITEPK(v_orig_protocol, 'event_assoc',v_sponsor_code, v_new_protocol,v_err,v_originating_sitecode);

     IF v_orig_protocol = NULL OR LENGTH(v_orig_protocol) = 0 THEN
         v_new_protocol := NULL;
     END IF;

     -- get new IDs
     IF v_new_study <= 0 THEN  -- original sponsor study not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient enrollment record, Sponsor study for site (' || v_site_code || ') does not exist. Original Study:' || v_orig_study, v_success );
     ELSIF v_orig_protocol IS NOT  NULL AND  v_orig_protocol > 0 AND v_new_protocol <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient enrollment record, Sponsor Protocol for site (' || v_site_code || ') does not exist. Original Protocol:' || v_orig_protocol, v_success );
        ELSIF v_new_per <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient enrollment record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
     ELSE -- all required FKs exist

     -- for enrolling site----------------------------
if (nvl(v_enroll_record_type,'O') <> 'D') then
      IF  v_orig_site_enrolling > 0 THEN

     IF  v_new_site_enrolling = 0 THEN -- site does not exist

    Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site_enrolling , v_success );

    SELECT seq_er_site.NEXTVAL INTO v_new_site_enrolling FROM dual;

    Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_site_enrolling, 'er_site',v_site_code,  v_new_site_enrolling, v_err,v_fk_account_sponsor);

    IF LENGTH(trim(v_err)) > 0 THEN

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

    ELSE
     BEGIN

      SELECT seq_er_add.NEXTVAL INTO v_new_add FROM dual;

      INSERT INTO ER_ADD (PK_ADD, CREATED_ON)
      VALUES (v_new_add, SYSDATE);

      INSERT INTO ER_SITE ( PK_SITE, FK_ACCOUNT,  FK_PERADD , SITE_NAME, CREATED_ON)
      VALUES (v_new_site_enrolling,v_fk_account_sponsor,v_new_add,v_site_enrolling_name,SYSDATE ) ;

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
        v_orig_site_enrolling ,SYSDATE, 'pat_enr', 'N');

      EXCEPTION WHEN OTHERS THEN
       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_ENROLL:Could not create New Site record for site '|| v_site_code || ' with original PK:' || v_orig_site_enrolling || ' Error: ' || SQLERRM, v_success );
      END;

     END IF;

     ELSIF v_new_site_enrolling > 0 THEN -- old record

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Site record for site '|| v_site_code || ' with original PK:' || v_orig_site_enrolling, v_success );

      UPDATE ER_SITE
      SET SITE_NAME = v_site_enrolling_name, FK_ACCOUNT = v_fk_account_sponsor,
       LAST_MODIFIED_DATE = SYSDATE
         WHERE pk_site = v_new_site_enrolling;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
       SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_site',
        v_orig_site_enrolling ,SYSDATE, 'pat_enr', 'M');


      ELSIF v_new_site_enrolling = -1 THEN

         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL :' || v_err, v_success );

       -- end if for v_new_site = 0
       END IF;

    END IF; -- for    v_orig_site_enrolling > 0
 END IF ; -- for checking the record type
  ----------------------------------------------------------

if (nvl(v_enroll_record_type,'O') <> 'D') then
    -- check if there is an enrollment record

    -- get the fk_site of this patient and insert data in er_studysite

            BEGIN
              SELECT fk_site INTO v_patient_site FROM ER_PER WHERE pk_per = v_new_per;
                  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - patient site '|| v_patient_site || ' for study:' || v_new_study , v_success );

              SELECT COUNT(*) INTO v_studysitecount  FROM ER_STUDYSITES WHERE fk_study = v_new_study AND fk_site = v_patient_site;

              Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - v_studysitecount  '|| v_studysitecount  || ' for study:' || v_new_study , v_success );

            EXCEPTION WHEN NO_DATA_FOUND THEN
               v_patient_site := 0;
                 v_studysitecount  := 0;
            END;

            IF    v_studysitecount = 0 AND v_patient_site > 0 THEN
              INSERT INTO ER_STUDYSITES (pk_studysites, fk_study, fk_site,creator,created_on)
              VALUES(SEQ_ER_STUDYSITES.NEXTVAL,  v_new_study, v_patient_site,v_creator,SYSDATE);

              Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - CREATED STUDY SITE FOR for site '|| v_patient_site || ' for study:' || v_new_study , v_success );

            END IF;

            IF v_studysitecount > 0  THEN
                 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - STUDY SITE FOR for site '|| v_patient_site || ' for study:' || v_new_study || ' already exists', v_success );
            END IF;

             IF v_new_user <= 0 AND v_orig_user > 0 THEN
             Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Foreign Key missing. User for site (' || v_site_code || ') does not exist. Original PK_USER:' || v_orig_user, v_success );
             END IF;

             IF ( v_orig_user_assignto > 0 AND v_new_user_assignto <= 0 ) THEN
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Foreign Key missing. User for site (' || v_site_code || ') does not exist. Original PK_USER:' || v_orig_user_assignto , v_success );

             END IF;

             IF ( v_orig_physician > 0 AND v_new_physician <= 0 ) THEN
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Foreign Key missing. User for site (' || v_site_code || ') does not exist. Original PK_USER:' || v_orig_physician , v_success );

             END IF;
    END IF; --checking the record type

    if (nvl(v_enroll_record_type,'O') = 'D') then --delete enrollment
        --delete patient enrollment from study
        --get patient pk for the enrollment

            begin
                select fk_per
                into v_fk_per
                from er_patprot where pk_patprot = v_new_patprot;

                SP_DELETE_PATSTUDY( v_new_study,v_fk_per ) ;

               INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
                REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
               SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patprot',
               v_orig_patprot ,SYSDATE, 'pat_enr', 'D');


                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'DEBUG','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Deleted patient enrollment record, v_new_patprot:' || v_new_patprot , v_success );

            exception when no_data_found then

                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Foreign Key missing. patient not found, v_new_patprot:' || v_new_patprot , v_success );

            end;


    ELSE

                 IF v_new_patprot = 0 THEN -- new enrollment record

                SELECT seq_er_patprot.NEXTVAL INTO v_new_patprot FROM dual;

                 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Patient Enrollment record for site '|| v_site_code || ' with original PK:' || v_orig_patprot , v_success );

                 Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patprot, 'er_patprot',v_site_code,  v_new_patprot,v_err,v_fk_account_sponsor);

                  IF LENGTH(trim(v_err)) > 0 THEN

                    Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

                  ELSE
                   BEGIN



                    INSERT INTO ER_PATPROT ( PK_PATPROT,FK_PROTOCOL, FK_STUDY,FK_USER, PATPROT_ENROLDT,
                    FK_PER, PATPROT_START, PATPROT_NOTES, PATPROT_STAT, CREATED_ON, PATPROT_DISCDT,
                    PATPROT_REASON, PATPROT_SCHDAY, FK_TIMEZONE, PATPROT_PATSTDID, PATPROT_CONSIGN,
                    PATPROT_CONSIGNDT, PATPROT_RESIGNDT1, PATPROT_RESIGNDT2, FK_USERASSTO, FK_CODELSTLOC  ,CREATOR ,
                    PATPROT_RANDOM , PATPROT_PHYSICIAN,FK_SITE_ENROLLING,
                    fk_codelst_ptst_eval_flag,fk_codelst_ptst_eval,
                    fk_codelst_ptst_ineval,fk_codelst_ptst_survival,fk_codelst_ptst_dth_stdrel,death_std_rltd_other,date_of_death  )
                    VALUES (v_new_patprot,v_new_protocol,v_new_study, v_new_user, v_patprot_enroldt,
                    v_new_per,v_patprot_start,v_patprot_notes,v_patprot_stat, SYSDATE,
                    v_patprot_discdate,v_patprot_reason, v_patprot_schday, v_timezone, v_patprot_patstdid,
                    v_patprot_consign,v_patprot_consigndt, v_patprot_resigndt1, v_patprot_resigndt2,
                    v_new_user_assignto, v_codelstloc, v_creator,
                    v_patprot_random ,v_new_physician ,v_new_site_enrolling, v_fk_codelst_ptst_eval_flag_pk, v_fk_codelst_ptst_eval_pk,v_fk_codelst_ptst_ineval_pk ,
                   v_fk_codelst_ptst_survival_pk ,v_fk_code_ptst_dth_stdrel_pk, v_death_std_rltd_other,  v_date_of_death) ;

                    INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
                    REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
                   SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patprot',
                   v_orig_patprot ,SYSDATE, 'pat_enr', 'N');

                   EXCEPTION WHEN OTHERS THEN
                     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Patient Enrollment record for site '|| v_site_code || ' with original PK:' || v_orig_patprot || ' Error: ' || SQLERRM, v_success );
                   END;
                    END IF;

               ELSIF v_new_patprot > 0 THEN -- old enrollment record

               Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Patient Enrollment record for site ' || v_site_code || ' with original PK:' || v_orig_patprot , v_success );

                   UPDATE ER_PATPROT
                 SET  FK_PROTOCOL = v_new_protocol,
                 FK_STUDY = v_new_study,
                 FK_USER = v_new_user,
                 PATPROT_ENROLDT = v_patprot_enroldt, FK_PER = v_new_per,
                    PATPROT_START = v_patprot_start, PATPROT_NOTES = v_patprot_notes,
                    PATPROT_STAT = v_patprot_stat, PATPROT_DISCDT = v_patprot_discdate,
                 PATPROT_REASON = v_patprot_reason, PATPROT_SCHDAY = v_patprot_schday,
                 FK_TIMEZONE = v_timezone, PATPROT_PATSTDID = v_patprot_patstdid,
                 PATPROT_CONSIGN = v_patprot_consign,
                 PATPROT_CONSIGNDT = v_patprot_consigndt, PATPROT_RESIGNDT1 = v_patprot_resigndt1,
                 PATPROT_RESIGNDT2 = v_patprot_resigndt2, FK_USERASSTO = v_new_user_assignto,
                 FK_CODELSTLOC = v_codelstloc ,  LAST_MODIFIED_DATE = SYSDATE,
                 PATPROT_RANDOM = v_patprot_random,
                 PATPROT_PHYSICIAN  = v_new_physician, fk_site_enrolling = v_new_site_enrolling,
                  fk_codelst_ptst_eval_flag = v_fk_codelst_ptst_eval_flag_pk ,fk_codelst_ptst_eval = v_fk_codelst_ptst_eval_pk,
                    fk_codelst_ptst_ineval = v_fk_codelst_ptst_ineval_pk,fk_codelst_ptst_survival = v_fk_codelst_ptst_survival_pk ,fk_codelst_ptst_dth_stdrel = v_fk_code_ptst_dth_stdrel_pk,
                   death_std_rltd_other = v_fk_code_ptst_dth_stdrel_pk,date_of_death = v_date_of_death
                 WHERE pk_patprot = v_new_patprot;

                  INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
                    REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
                   SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patprot',
                   v_orig_patprot ,SYSDATE, 'pat_enr', 'M');

                 --check the value of v_patprot_stat, if its 0, deactivate events of patprot
                 IF (v_patprot_stat = 0) THEN
                   UPDATE sch_events1
                   SET status = 5, last_modified_date = SYSDATE
                   WHERE  FK_PATPROT = v_new_patprot;

                   DELETE FROM  sch_dispatchmsg
                   WHERE fk_schevent IN ( SELECT event_id
                                   FROM sch_events1
                              WHERE FK_PATPROT = v_new_patprot   ) ;

                 END IF;

                  ELSIF v_new_patprot = -1 THEN
                     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );
               END IF; -- for checking v_new_patprot

    END IF; -- checking record type


       END IF; --for checking the FKs


   END LOOP;

  CLOSE v_cur ;
     EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || SQLERRM, v_success );

  END; -- patient enrollment
 ------------------------------------------------------

 -- get patient status

 v_sql_patstudystat := 'Select PK_PATSTUDYSTAT, FK_CODELST_STAT_SUBTYP, FK_PER, FK_STUDY,  PATSTUDYSTAT_DATE,
      PATSTUDYSTAT_ENDT,  PATSTUDYSTAT_NOTE,  PATSTUDYSTAT_REASON_SUBTYP,
      SITE_CODE, FK_SPONSOR_ACCOUNT,SCREEN_NUMBER,
      SCREENED_BY,
      SCREENING_OUTCOME_SUBTYP,RECORD_TYPE,  inform_consent_ver, next_followup_on,originating_sitecode,current_stat FROM IMPPAT_ENR2 ORDER BY RECORD_TYPE DESC';


 BEGIN

  OPEN v_cur FOR v_sql_patstudystat;

   LOOP
    FETCH v_cur INTO  v_orig_patstudystat, v_codelst_stat_subtype,v_orig_per,v_orig_study, v_patstudystat_date,
   v_patstudystat_endt, v_patstudystat_note, v_patstudystat_reason_subtyp, v_site_code ,v_fk_account_sponsor,
   v_screen_number,v_orig_screened_by,v_screening_outcome_subtyp,v_record_type, vstat_inform_consent_ver, vstat_next_followup_on,v_originating_sitecode,
   v_current_stat;

  EXIT WHEN v_cur %NOTFOUND;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patstudystat, 'er_patstudystat',v_site_code, v_new_patstudystat,v_err,v_fk_account_sponsor);


    -- for study need to get the SITE PK for the original sponsor's Study

    -- get the site code for the sponsor account


   SELECT site_code  , AC_USRCREATOR
   INTO v_sponsor_code, v_creator
   FROM ER_ACCOUNT
   WHERE pk_account  = v_fk_account_sponsor;


   IF (v_record_type <> 'D') THEN

     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_screened_by, 'er_user',v_site_code, v_new_screened_by,v_err,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err,v_fk_account_sponsor);

        -- get patient study status code

    v_code_type := 'patStatus';

     SELECT Pkg_Impex.getCodePk (v_codelst_stat_subtype, v_code_type)
     INTO v_codelst_stat
     FROM dual;

     -- get patient study status reason
     IF v_patstudystat_reason_subtyp IS NOT NULL AND LENGTH(trim(v_patstudystat_reason_subtyp)) > 0 THEN

       SELECT Pkg_Impex.getCodePk (v_patstudystat_reason_subtyp, v_codelst_stat_subtype)
       INTO v_codelst_stat_reason
       FROM dual;
      ELSE
            v_codelst_stat_reason := NULL;
     END IF;

       -- get patient study status screening outcome
     IF v_screening_outcome_subtyp IS NOT NULL AND LENGTH(trim(v_screening_outcome_subtyp)) > 0 THEN

          v_code_type :=  'screenOutcome';

       SELECT Pkg_Impex.getCodePk (v_screening_outcome_subtyp, v_code_type )
       INTO v_code_screening_outcome
       FROM dual;
      ELSE
           v_code_screening_outcome := NULL;
     END IF;

      IF v_codelst_stat = -1 THEN
          v_codelst_stat:= NULL;
     END IF;
      IF v_codelst_stat_reason = -1 THEN
       v_codelst_stat_reason:= NULL;
     END IF;

   IF v_code_screening_outcome = -1 THEN
      v_code_screening_outcome := NULL;
   END IF;
  END IF; -- for check if record_tye <>' D'

        SP_GET_SITEPK(v_orig_study, 'er_study',v_sponsor_code, v_new_study,v_err,v_originating_sitecode);

    IF v_new_study <= 0 THEN  -- original sponsor study not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient study status record, Sponsor study for site (' || v_site_code || ') does not exist. Original Study:' || v_orig_study, v_success );
    ELSIF v_new_per <= 0 AND v_record_type <> 'D' THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient study status record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
    ELSE
        -- FK exists

     IF v_record_type = 'D' THEN
          IF  v_new_patstudystat > 0 THEN
       DELETE FROM ER_PATSTUDYSTAT WHERE pk_patstudystat = v_new_patstudystat ;

        INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patstudystat',
        v_orig_patstudystat ,SYSDATE, 'pat_enr', 'D');


     ELSE
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not delete patient study status record, Status record does not exist. Original PK_PATSTUDYSTAT:' || v_orig_patstudystat, v_success );
     END IF;


     ELSE

        IF v_new_screened_by <= 0 AND v_orig_screened_by IS NOT  NULL AND v_orig_screened_by > 0 THEN
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Foreign Key missing, User for site (' || v_site_code || ') does not exist. Original PK_USER:' || v_orig_screened_by, v_success );
        END IF;

        IF  v_new_patstudystat = 0 THEN -- new enrollment record

       SELECT seq_er_patstudystat.NEXTVAL INTO v_new_patstudystat FROM dual;

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Patient Study Status record for site ' || v_site_code || ' with original PK:' || v_orig_patstudystat , v_success );

        Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patstudystat, 'er_patstudystat',v_site_code,  v_new_patstudystat,v_err ,v_fk_account_sponsor);

         IF LENGTH(trim(v_err)) > 0 THEN

           Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

         ELSE
          BEGIN

          INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY,
          PATSTUDYSTAT_DATE,  PATSTUDYSTAT_ENDT, PATSTUDYSTAT_NOTE,CREATED_ON,PATSTUDYSTAT_REASON,CREATOR,
          SCREEN_NUMBER, SCREENED_BY,SCREENING_OUTCOME ,inform_consent_ver, next_followup_on,current_stat)
          VALUES (v_new_patstudystat, v_codelst_stat,v_new_per,v_new_study,
          v_patstudystat_date, v_patstudystat_endt, v_patstudystat_note,SYSDATE,
          v_codelst_stat_reason,v_creator,
          v_screen_number,v_new_screened_by,v_code_screening_outcome ,vstat_inform_consent_ver, vstat_next_followup_on,v_current_stat);

           INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patstudystat',
        v_orig_patstudystat ,SYSDATE, 'pat_enr', 'N');

          -- for approval based enrollment
          IF v_codelst_stat_subtype = 'enrolled' THEN --an enrolled status is added

         --send email
          Pkg_Patient.SP_PATENRMAIL(v_new_per,'enrolled' ,v_new_study, v_patstudystat_date );

               --check account settings for automated enrollment
                      IF NVL(v_automated_enr_setting,2) =  2 THEN
                         BEGIN
                             SELECT SETTINGS_VALUE INTO v_automated_enr_setting
                             FROM ER_SETTINGS WHERE SETTINGS_KEYWORD = 'automated_enr'
                             AND SETTINGS_MODNAME= '1' AND  SETTINGS_MODNUM = v_fk_account_sponsor;

                         EXCEPTION WHEN NO_DATA_FOUND THEN
                             v_automated_enr_setting := 0; --automated enrollment is off
                         END ;

                     END IF; -- for settings for automated enrollment

              IF (v_automated_enr_setting = 1) THEN -- added by sonia

                SELECT Pkg_Impex.getCodePk ('enroll_pending',  'patStatus')
                INTO v_code_enroll_pending
                FROM dual;

                BEGIN

                  -- get patient's enrolling site

                SELECT fk_site_enrolling INTO  v_patstat_site_enrolling FROM ER_PATPROT
                WHERE fk_per = v_new_per AND fk_study = v_new_study AND patprot_stat = 1;

                -- get enrolling site enrollment settings

                SELECT NVL(IS_REVIEWBASED_ENR,0) INTO v_is_reviewbased
                FROM ER_STUDYSITES WHERE fk_study = v_new_study AND fk_site = v_patstat_site_enrolling ;

                 -- insert an enrollment pending status

               IF v_is_reviewbased = 1 THEN --review based
                  v_aut_enddt := NULL;
                  ELSE
                  v_aut_enddt := SYSDATE;
                 END IF;

                IF v_code_enroll_pending > 0 THEN

                    UPDATE ER_PATSTUDYSTAT SET patstudystat_endt = SYSDATE WHERE fk_study = v_new_study AND fk_per = v_new_per AND patstudystat_endt = NULL;

                -- insert default 'enrollment pending' status
                 INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY,
                 PATSTUDYSTAT_DATE,  PATSTUDYSTAT_ENDT,  CREATED_ON,CREATOR )
                 VALUES (seq_er_patstudystat.NEXTVAL,v_code_enroll_pending,v_new_per,v_new_study,
                 SYSDATE, v_aut_enddt ,SYSDATE,
                 v_creator);
                 ELSE
                  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create ;Enrollment Pending Status for site (' || v_site_code || '). Original v_orig_patstudystat :' || v_orig_patstudystat || ' code for pat study status, code subtype - enroll_pending  not found'  , v_success );
                END IF;

                IF v_is_reviewbased = 0 THEN --not a  review based enrollment
                 SELECT Pkg_Impex.getCodePk ('enroll_appr',  'patStatus')
                INTO v_code_enroll_appr
                FROM dual;

                IF v_code_enroll_appr  > 0 THEN
                  -- insert default 'enrollment approved status'
                 INSERT INTO ER_PATSTUDYSTAT (PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER, FK_STUDY,
                 PATSTUDYSTAT_DATE,  PATSTUDYSTAT_ENDT,  CREATED_ON,CREATOR )
                 VALUES (seq_er_patstudystat.NEXTVAL,v_code_enroll_appr,v_new_per,v_new_study,
                 SYSDATE, NULL ,SYSDATE,
                 v_creator);

                 --send email
                  Pkg_Patient.SP_PATENRMAIL(v_new_per,'enroll_appr' ,v_new_study,SYSDATE);

                 ELSE
                   -- update the end date of maximum status to null:
                    UPDATE ER_PATSTUDYSTAT SET patstudystat_endt = NULL
                    WHERE fk_study = v_new_study AND fk_per = v_new_per
                    AND pk_patstudystat = ( SELECT MAX(pk_patstudystat)  FROM ER_PATSTUDYSTAT
                                          WHERE fk_study = v_new_study AND fk_per = v_new_per AND
                              patstudystat_date = (SELECT MAX(patstudystat_date)  FROM ER_PATSTUDYSTAT  WHERE fk_study = v_new_study AND fk_per = v_new_per )
                                                       ) ;


                   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create ;Enrollment Pending Status for site (' || v_site_code || '). Original v_orig_patstudystat :' || v_orig_patstudystat || ' code for pat study status, code subtype - enroll_appr   not found'  , v_success );
                 END IF;

                END IF; -- for is_review_based

                 EXCEPTION WHEN NO_DATA_FOUND THEN
                     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create review based / automated enrollment records for site (' || v_site_code || '). Original v_orig_patstudystat :' || v_orig_patstudystat , v_success );
                  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create review based / automated enrollment records for site (' || v_site_code || ').v_is_reviewbased  :' ||v_is_reviewbased || ' Enrolling SITE:' ||   v_patstat_site_enrolling , v_success );
                END;
                END IF; -- for automated enrollment setting
          END IF; --for enrolled status
          ---------------------------------------

          EXCEPTION WHEN OTHERS THEN
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Patient Enrollment record for site '|| v_site_code || ' with original PK:' || v_orig_patstudystat || ' Error: ' || SQLERRM, v_success );
          END;
           END IF;

      ELSIF v_new_patstudystat > 0 THEN -- old enrollment record


         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Patient Study Status record for site ' || v_site_code || ' with original PK:' || v_orig_patstudystat , v_success );

         BEGIN
          UPDATE ER_PATSTUDYSTAT
        SET   FK_CODELST_STAT = v_codelst_stat,
              FK_STUDY = v_new_study,
           PATSTUDYSTAT_DATE = v_patstudystat_date,  PATSTUDYSTAT_ENDT = v_patstudystat_endt,
           PATSTUDYSTAT_NOTE = v_patstudystat_note,
           PATSTUDYSTAT_REASON = v_codelst_stat_reason ,
           LAST_MODIFIED_DATE = SYSDATE,
           SCREEN_NUMBER =  v_screen_number,
           SCREENED_BY   =  v_new_screened_by,
           SCREENING_OUTCOME  = v_code_screening_outcome,
           inform_consent_ver = vstat_inform_consent_ver, next_followup_on = vstat_next_followup_on,current_stat = v_current_stat
        WHERE pk_patstudystat = v_new_patstudystat   ;

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patstudystat',
        v_orig_patstudystat ,SYSDATE, 'pat_enr', 'M');


           -- since there may be a difference of status in participating site and CCD, update the end date of maximum status to null:,
           UPDATE ER_PATSTUDYSTAT SET patstudystat_endt = NULL
           WHERE fk_study = v_new_study AND fk_per = v_new_per
           AND pk_patstudystat = ( SELECT MAX(pk_patstudystat)  FROM ER_PATSTUDYSTAT
                                 WHERE fk_study = v_new_study AND fk_per = v_new_per AND
                     patstudystat_date = (SELECT MAX(patstudystat_date)  FROM ER_PATSTUDYSTAT  WHERE fk_study = v_new_study AND fk_per = v_new_per )
                                              ) ;


        EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not Update Patient Enrollment record for site '|| v_site_code || ' with original PK:' || v_orig_patstudystat || ' Error: ' || SQLERRM, v_success );
        END;

         ELSIF v_new_patstudystat = -1 THEN
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );
      END IF; -- for checking v_new_patprot
    END IF; -- for if v_record_type = 'D'
       END IF; -- end if for v_new_study < 0

    END LOOP;

   CLOSE v_cur ;
  EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || SQLERRM, v_success );

  END; -- patient study status


  -- import patient treatment arm information

   -- get patient treatment arm info

 v_sql_pattx:= 'Select pk_pattxarm, fk_patprot, fk_studytxarm,tx_drug_info,tx_start_date,tx_end_date,notes,
 site_code,fk_sponsor_account,record_type, originating_sitecode
 FROM IMPPAT_ENR5 ORDER BY RECORD_TYPE DESC';


 BEGIN

  OPEN v_cur FOR v_sql_pattx;

   LOOP
    FETCH v_cur INTO  v_orig_pattxarm , v_orig_patprot, v_orig_studytxarm,v_drug_info,v_tx_startdate, v_tx_enddate,
    v_tx_notes, v_site_code ,v_fk_account_sponsor,
   v_record_type,  v_originating_sitecode;



  EXIT WHEN v_cur %NOTFOUND;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_pattxarm, 'er_pattxarm',v_site_code, v_new_pattxarm,v_err,v_fk_account_sponsor);


    -- for study TX ARM need to get the SITE PK for the original sponsor's Study TX ARM

    -- get the site code for the sponsor account


   SELECT site_code  , AC_USRCREATOR
   INTO v_sponsor_code, v_creator
   FROM ER_ACCOUNT
   WHERE pk_account  = v_fk_account_sponsor;


   IF (v_record_type <> 'D') THEN

     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patprot, 'er_patprot',v_site_code, v_new_patprot,v_err,v_fk_account_sponsor);

        -- get patient study status code

      END IF; -- for check if record_tye <>' D'

    SP_GET_SITEPK(v_orig_studytxarm, 'er_studytxarm',v_sponsor_code, v_new_studytxarm,v_err,v_originating_sitecode);

    IF v_new_studytxarm <= 0 AND v_record_type <> 'D' THEN  -- original sponsor study TX not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient TX record, Sponsor study treatment ARM for site (' || v_site_code || ') does not exist. Original Study:' || v_orig_studytxarm, v_success );
    ELSIF v_new_patprot <= 0 AND v_record_type <> 'D' THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create patient TX record, Patient Enrollment for site (' || v_site_code || ') does not exist. Original  patprot:' || v_orig_patprot, v_success );
    ELSE
        -- FK exists



     IF v_record_type = 'D' THEN
          IF  v_new_pattxarm > 0 THEN

                   --get study

                select fk_study
                into v_new_study
                from er_patprot , er_pattxarm
                where pk_pattxarm = v_new_pattxarm and fk_patprot = pk_patprot and rownum < 2;

               DELETE FROM ER_PATTXARM WHERE pk_pattxarm = v_new_pattxarm ;

            INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
             REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
            SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_pattxarm',
            v_orig_pattxarm ,SYSDATE, 'pat_enr', 'D');


         ELSE
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not delete patient study TX record,  record does not exist. Original PK_PATTXARM:' || v_orig_pattxarm, v_success );
         END IF;


     ELSE

                   --get study

                select fk_study
                into v_new_study
                from er_patprot
                where pk_patprot = v_new_patprot ;


        IF  v_new_pattxarm = 0 THEN -- new TX record

           SELECT seq_er_pat_tx_arm.NEXTVAL INTO v_new_pattxarm FROM dual;

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - New Patient Study TX record for site ' || v_site_code || ' with original PK:' || v_orig_pattxarm , v_success );

        Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_pattxarm, 'er_pattxarm',v_site_code,  v_new_pattxarm ,v_err ,v_fk_account_sponsor);

         IF LENGTH(trim(v_err)) > 0 THEN

           Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL Could not create record for er_sponsorpkmap :' || v_err, v_success );

         ELSE
              BEGIN

                  INSERT INTO ER_PATTXARM(PK_PATTXARM, FK_PATPROT, FK_STUDYTXARM, TX_DRUG_INFO, TX_START_DATE,
                TX_END_DATE, NOTES, CREATOR, CREATED_ON)
                  VALUES (v_new_pattxarm, v_new_patprot, v_new_studytxarm ,v_drug_info ,v_tx_startdate ,
                  v_tx_enddate ,v_tx_notes , v_creator ,sysdate);

                   INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
                 REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
                SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_pattxarm',
                v_orig_pattxarm ,SYSDATE, 'pat_enr', 'N');


              EXCEPTION WHEN OTHERS THEN
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not create New Patient TX record for site '|| v_site_code || ' with original PK:' || v_orig_pattxarm || ' Error: ' || SQLERRM, v_success );
              END;
        END IF;

      ELSIF v_new_pattxarm > 0 THEN -- old enrollment record


         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL - Old Patient Study TX record for site ' || v_site_code || ' with original PK:' || v_orig_pattxarm , v_success );

         BEGIN


        UPDATE ER_PATTXARM
        SET   FK_PATPROT = v_new_patprot, FK_STUDYTXARM = v_new_studytxarm,TX_DRUG_INFO = v_drug_info,
        TX_START_DATE = v_tx_startdate, TX_END_DATE = v_tx_enddate,NOTES = v_tx_notes,
           LAST_MODIFIED_DATE = SYSDATE,last_modified_by = v_creator
        WHERE pk_pattxarm = v_new_pattxarm   ;

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_pattxarm',
        v_orig_pattxarm ,SYSDATE, 'pat_enr', 'M');

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:Could not Update Patient TX record for site '|| v_site_code || ' with original PK:' || v_orig_pattxarm || ' Error: ' || SQLERRM, v_success );
        END;

         ELSIF v_new_pattxarm = -1 THEN
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || v_err, v_success );
      END IF; -- for v_new_studytxarm
    END IF; -- for if v_record_type = 'D'
       END IF; -- v_new_studytxarm

    END LOOP;

   CLOSE v_cur ;
  EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATENROLL:' || SQLERRM, v_success );

  END;


  -- end of import patient treatment arm information

  COMMIT;

   -- import labs (labs that were entered before this enrollment)

  SP_IMP_PATLABS_NOSTUDY_COMMON ('imppat_enr3', P_IMPID, O_SUCCESS);

 END;

 ---------------------------------------------------------------------------------------------------------

 PROCEDURE SP_EXP_PATFORMS (
    P_EXPID          NUMBER,
      O_RESULT_PATFORMS     OUT  Gk_Cv_Types.GenericCursorType,
   O_LINEAR OUT  Gk_Cv_Types.GenericCursorType,
   O_MAP OUT  Gk_Cv_Types.GenericCursorType
    )
   /* Author: Sonia Sahni
   Date: 04/30/04
   Purpose - Returns a ref cursor of patient forms data
   */
   AS
    v_rec_patforms rec_er_patforms   := rec_er_patforms(0,0,0,0,SYSDATE,NULL,'','',0,
      0,0,'',0,0,NULL,NULL); --record type
    v_table_patforms tab_er_patforms := tab_er_patforms(); --index-by table type

    v_rec_mapform mapform_record   := mapform_record (0, '','' ,0,''); --record type
    v_table_mapform tab_mapform := tab_mapform(); --index-by table type

   v_cnt NUMBER := 0;
   v_sponsor_form NUMBER;

   v_err VARCHAR2(4000);

   v_patformtab_name VARCHAR2(50);
   v_site_code VARCHAR2(255);

   v_sql LONG;
   v_study VARCHAR2(4000);

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
   v_success NUMBER;
   v_sponsor_formlibver NUMBER;
   v_sitecode VARCHAR2(20);
 v_module_subtype VARCHAR2(20);

 v_formname varchar2(1000);

   BEGIN

  --get export request

  Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

  --find out the study id from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

  --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);

    -- Get patient forms data



    v_patformtab_name := 'er_patforms';
    v_module_subtype  := 'pat_filledforms';

    UPDATE ER_REV_PENDINGDATA
 SET rp_processedflag = 1
 WHERE rp_module = v_module_subtype  AND rp_sitecode = v_sitecode;

 COMMIT;


  -- get data --
 FOR i IN ( SELECT PK_PATFORMS, FK_FORMLIB, FK_PER,  FK_PATPROT, PATFORMS_FILLDATE,e.PATFORMS_XML.getClobVal() PATFORMS_XML,
      (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FORM_COMPLETED) FORM_COMPLETED_SUBTYP , RECORD_TYPE, ISVALID, NOTIFICATION_SENT,
    RP_SITECODE , FK_SPONSOR_ACCOUNT  ,FK_FORMLIBVER, originating_sitecode
     FROM ER_PATFORMS e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
       WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patformtab_name AND
    pk_patforms = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study)
 LOOP
  v_cnt := v_cnt + 1;

     v_site_code := i.RP_SITECODE;

     select form_name
     into v_formname
     from er_formlib where pk_formlib = i.fk_formlib;


     Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: er_patientforms v_cnt ' || v_cnt , v_success );

   --get sponsor form id
    SP_GET_SPONSORPK(i.FK_FORMLIB, 'er_formlib' , v_site_code,  v_sponsor_form,v_err);

    --get sponsor formlib ver id
     SP_GET_SPONSORPK(i.FK_FORMLIBVER, 'er_formlibver' , v_site_code,  v_sponsor_formlibver,v_err);



   Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: er_patientforms v_sponsor_form' || v_sponsor_form , v_success );
   Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: er_patientforms v_site_form' || i.FK_FORMLIB , v_success );

    IF v_sponsor_form > 0 AND v_sponsor_formlibver > 0 THEN

      v_rec_patforms.PK_PATFORMS := i.PK_PATFORMS;
      v_rec_patforms.FK_FORMLIB := v_sponsor_form;

      v_rec_patforms.FK_PER := i.FK_PER;
      v_rec_patforms.FK_PATPROT   := i.FK_PATPROT;
      v_rec_patforms.PATFORMS_FILLDATE  := i.PATFORMS_FILLDATE;
      v_rec_patforms.PATFORMS_XML  := i.PATFORMS_XML;

      v_rec_patforms.FORM_COMPLETED_SYUBTYP := i.FORM_COMPLETED_SUBTYP;

       v_rec_patforms.RECORD_TYPE := i.RECORD_TYPE;
         v_rec_patforms.ISVALID := i.ISVALID;

         v_rec_patforms.NOTIFICATION_SENT := i.NOTIFICATION_SENT;
      v_rec_patforms.PROCESS_DATA  := 2; -- modified by sonia abrol, 03/20/06, the xml will not be processed now

      v_rec_patforms.SITE_CODE := v_site_code;
      v_rec_patforms.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;

       v_rec_patforms.FK_FORMLIBVER :=  v_sponsor_formlibver;
       v_rec_patforms.originating_sitecode := i.originating_sitecode;

       v_rec_patforms.form_name := v_formname;

     --store each record in a table
        v_table_patforms.EXTEND;
     v_table_patforms(v_cnt) := v_rec_patforms;
     v_cnt := v_table_patforms.COUNT();
    END IF;

 END LOOP;
 -- populate out cursor
  OPEN  O_RESULT_PATFORMS
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_patforms AS tab_er_patforms));

  --get linear data for these forms:
  OPEN o_linear FOR
  SELECT e.*,
    RP_SITECODE , FK_SPONSOR_ACCOUNT,originating_sitecode
     FROM ER_FORMSLINEAR e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
       WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patformtab_name AND
    fk_filledform = RP_TABLEPK AND form_type = 'P' AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study;

    --get mapform data-------------------------

     -- get data --

     v_cnt := 0;

 FOR i IN ( SELECT   FK_FORM , mp_systemid , mp_mapcolname FROM ER_MAPFORM WHERE fk_form IN (SELECT DISTINCT fk_formlib
     FROM ER_PATFORMS e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
       WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patformtab_name AND
    pk_patforms = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study  ))
 LOOP
     v_cnt := v_cnt + 1;

    -- v_site_code := i.RP_SITECODE;

     Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: mapform v_cnt ' || v_cnt , v_success );

   --get sponsor form id
    SP_GET_SPONSORPK(i.FK_FORM , 'er_formlib' , v_site_code,  v_sponsor_form,v_err);


   Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: mapforms v_sponsor_form' || v_sponsor_form , v_success );
   Pkg_Impexlogging.SP_RECORDLOG (p_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_EXP_PATFORMS: er_patientforms v_site_form' || i.FK_FORM  , v_success );

    IF v_sponsor_form > 0   THEN

      v_rec_mapform.FK_FORM  := v_sponsor_form;

     -- v_rec_mapform.SITE_CODE := v_site_code;
     -- v_rec_mapform.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;
      v_rec_mapform.mp_systemid := i.mp_systemid ;
       v_rec_mapform.mp_mapcolname := i.mp_mapcolname;

     --store each record in a table

           v_table_mapform.EXTEND;
     v_table_mapform(v_cnt) := v_rec_mapform;
     v_cnt := v_table_mapform.COUNT();
    END IF;

 END LOOP;

    OPEN  O_MAP
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_mapform AS  TAB_MAPFORM ));


    -------------------------------------------

    END SP_EXP_PATFORMS;

 ---------------------------------------------------------------------------------------------------------

  PROCEDURE SP_IMP_PATFORMS (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  04/30/04
    Purpose : Import patient forms data from temp tables
  */

 v_sql LONG;
 v_sql_enrol LONG;
 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_per NUMBER;
 v_site_code VARCHAR2(20);
 v_sponsor_code VARCHAR2(20);
 v_fk_account_sponsor NUMBER;
 v_new_per NUMBER;
 v_err VARCHAR2(4000);

 V_EXPID NUMBER;
 v_success NUMBER;

 v_orig_patprot NUMBER;
 v_new_patprot NUMBER;

 v_creator NUMBER;

 v_orig_patforms NUMBER;
 v_orig_form NUMBER;
 v_patform_filldate DATE;
 v_patforms_clob CLOB;
 v_form_completed_subtyp VARCHAR2(20);
 v_record_type CHAR(1);
 v_isvalid NUMBER;
 v_notification_sent NUMBER;
 v_process_data NUMBER;

 v_new_patforms NUMBER;
 v_new_form NUMBER;
 v_form_xml sys.XMLTYPE;
 v_code_form_completed NUMBER;

 v_orig_formlibver NUMBER;
 v_new_formlibver NUMBER;

 v_code_type VARCHAR2(15);
 v_ver_count NUMBER := 0;
 v_getformfield_count NUMBER := 0;
 v_form_cols VARCHAR2(32767);
 v_form_cols_data  VARCHAR2(32767);
 v_linear_sql VARCHAR2(32767);
 v_updatecol_string VARCHAR2(32767);

 v_linear_update_sql VARCHAR2(32767);
 formColvalue VARCHAR2(4000);
 source_cursor      INTEGER;
     destination_cursor INTEGER;
     ignore             INTEGER;

  v_new_study NUMBER;
  rec_tab dbms_sql.desc_tab;
  v_desc_rec  dbms_sql.desc_rec;
  v_originating_sitecode Varchar2(10);

  v_orig_formname varchar2(1000);
  v_valid_count number;

 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;

   v_sql := 'SELECT PK_PATFORMS,  FK_FORMLIB,  FK_PER,  FK_PATPROT,  PATFORMS_FILLDATE,  PATFORMS_XML, FORM_COMPLETED_SYUBTYP,
          RECORD_TYPE,  ISVALID, NOTIFICATION_SENT,  PROCESS_DATA,  SITE_CODE,  FK_SPONSOR_ACCOUNT  ,FK_FORMLIBVER,originating_sitecode,
          form_name
        FROM IMPPAT_FILLEDFORMS0';

  --get data

   OPEN v_cur FOR v_sql;
    LOOP

     FETCH v_cur INTO v_orig_patforms, v_orig_form, v_orig_per,v_orig_patprot,v_patform_filldate,v_patforms_clob,v_form_completed_subtyp,
    v_record_type,v_isvalid, v_notification_sent,v_process_data,v_site_code,v_fk_account_sponsor,
    v_orig_formlibver ,v_originating_sitecode, v_orig_formname ;

     EXIT WHEN v_cur %NOTFOUND;

     -- get the site code , creator for the sponsor account

           SELECT site_code , AC_USRCREATOR
        INTO v_sponsor_code, v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;

       -- get patient treatment location

    v_code_type := 'fillformstat';

     SELECT Pkg_Impex.getCodePk (v_form_completed_subtyp, v_code_type)
     INTO v_code_form_completed
     FROM dual;

     IF v_code_form_completed = -1 THEN
         v_code_form_completed:= NULL;
     END IF;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patforms, 'er_patforms',v_site_code, v_new_patforms,v_err,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patprot, 'er_patprot',v_site_code, v_new_patprot,v_err,v_fk_account_sponsor);

     --SA, use the reverse A2A's, SP_GET_SPONSORPK, to handle the originating site's data

   SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sponsor_code, v_new_form,v_err,v_originating_sitecode);
   SP_GET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sponsor_code, v_new_formlibver,v_err,v_originating_sitecode);

    if (v_new_form > 0 ) then --check if it is a valid form

        select count(*)
        into v_valid_count
        from er_formlib where pk_formlib = v_new_form and
        lower(trim(form_name)) = lower(trim(v_orig_formname))
        and fk_account = v_fk_account_sponsor;

        if v_valid_count =0 then
          v_new_form := -1;
        end if;

    end if;

    ---------
     -- get new IDs
     IF v_new_form <= 0 THEN  -- original sponsor study not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS Could not create patient form record, Sponsor Form for site (' || v_site_code || ') does not exist. Original Form:' || v_orig_form || ', from name : ' || v_orig_formname, v_success );
          ELSIF v_new_formlibver <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS Could not create patient form record, Form Version for site (' || v_site_code || ') does not exist. Original formlibver:' || v_orig_formlibver, v_success );
        ELSIF v_new_per <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS Could not create patient form record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
     ELSIF v_new_patprot <= 0 THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMSL Could not create patient form record, Patient enrollment record for site (' || v_site_code || ') does not exist. Original PK_PATPROT:' || v_orig_patprot, v_success );
     ELSE -- all required FKs exist
    -- check if there is a patient filledform record

    SELECT fk_study INTO v_new_study FROM ER_PATPROT WHERE pk_patprot = v_new_patprot;

    --SA, 08/02/2005, check if the pk_fromliba and forlibversion match

   BEGIN
    SELECT COUNT(*)
    INTO v_ver_count
    FROM ER_FORMLIBVER WHERE pk_formlibver = v_new_formlibver AND
    fk_formlib = v_new_form;
   EXCEPTION WHEN NO_DATA_FOUND THEN
    v_ver_count := 0;
   END;

   IF v_ver_count = 0 THEN --the version is incorrect, may be because a new version was generated at the site
      --get the latest version

      SELECT MAX(pk_formlibver)
      INTO v_new_formlibver FROM ER_FORMLIBVER
      WHERE fk_formlib = v_new_form;

   END IF;

   -- get the number of fields in the form
   SELECT COUNT(*)
   INTO  v_getformfield_count
   FROM ER_MAPFORM WHERE fk_form = v_new_form;

   IF (v_getformfield_count = 0) THEN  --generate the map form
       Pkg_Formrep.SP_GENFORMLAYOUT(v_new_form);

    -- get the count
          SELECT COUNT(*)
       INTO  v_getformfield_count
       FROM ER_MAPFORM WHERE fk_form = v_new_form;

   END IF;

   IF (v_getformfield_count > 0) THEN  --create sql
        getFormColumnString(v_getformfield_count,  v_new_form,v_orig_form ,  v_form_cols ,  v_form_cols_data,'N' )  ;
   END IF;

      -- create xmltype instance of v_patforms_clob


   BEGIN
    IF v_patforms_clob IS NOT NULL THEN
    SELECT XMLTYPE(v_patforms_clob)
    INTO v_form_xml FROM dual;
    END IF;
   EXCEPTION WHEN OTHERS THEN
    Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS Could not create xmltype instance for patient form record, for site (' || v_site_code || '). Original PK_PATFORMS:' || v_orig_patforms, v_success );
    v_form_xml := NULL;
   END;

   IF  v_new_patforms = 0 THEN -- new form  record

    SELECT seq_er_patforms.NEXTVAL INTO v_new_patforms FROM dual;

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - New Patient Form record for site '|| v_site_code || ' with original PK:' || v_orig_patforms , v_success );

     Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patforms, 'er_patforms',v_site_code,  v_new_patforms,v_err ,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

          INSERT INTO ER_PATFORMS ( PK_PATFORMS,  FK_FORMLIB,  FK_PER,   FK_PATPROT,  PATFORMS_FILLDATE,
           PATFORMS_XML, FORM_COMPLETED, RECORD_TYPE, CREATOR, ISVALID,  CREATED_ON, NOTIFICATION_SENT,
         PROCESS_DATA,FK_FORMLIBVER)
       VALUES(v_new_patforms,v_new_form,v_new_per,v_new_patprot,v_patform_filldate ,
       v_form_xml ,v_code_form_completed, v_record_type,v_creator, v_isvalid,SYSDATE,v_notification_sent,
        v_process_data, v_new_formlibver );

           INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patforms',
        v_orig_patforms ,SYSDATE, 'pat_filledforms', 'N');


       IF (v_getformfield_count > 0) THEN
         v_linear_sql := ' INSERT INTO ER_FORMSLINEAR ( PK_FORMSLINEAR, FK_FILLEDFORM, FK_FORM, FILLDATE, FORM_COMPLETED,IS_VALID, NOTIFICATION_SENT,
         FORM_TYPE, EXPORT_FLAG, ID, FK_PATPROT,  CREATED_ON ' ||  v_form_cols || ' )';

          v_linear_sql := v_linear_sql  || '  Select  SEQ_ER_FORMSLINEAR.nextval, ' || v_new_patforms ||', '|| v_new_form ||', FILLDATE, ' || v_code_form_completed ||' , '|| v_isvalid ||', NOTIFICATION_SENT,
         FORM_TYPE, EXPORT_FLAG, ' || v_new_per || ', ' || v_new_patprot ||  ', SYSDATE ' ||  v_form_cols_data ||  ' FROM IMPPAT_FILLEDFORMS1 WHERE  FK_FILLEDFORM = ' || v_orig_patforms || ' AND ROWNUM < 2 ' ;

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - New Patient FormLinear record for site '|| v_site_code || ' with original PK:' || v_orig_patforms || 'v_linear_sql' || v_linear_sql , v_success );

        EXECUTE IMMEDIATE v_linear_sql;

       END IF;

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS:Could not create New Patient Forms record for site '|| v_site_code || ' with original PK:' || v_orig_patforms || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

   ELSIF v_new_patforms > 0 THEN -- old enrollment record

   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - Old Patient Forms record for site ' || v_site_code || ' with original PK:' || v_orig_patforms , v_success );

       UPDATE ER_PATFORMS
     SET  FK_FORMLIB = v_new_form,  FK_PATPROT = v_new_patprot,  PATFORMS_FILLDATE = v_patform_filldate ,
           PATFORMS_XML = v_form_xml, FORM_COMPLETED = v_code_form_completed,
        RECORD_TYPE = v_record_type, ISVALID = v_isvalid,
        NOTIFICATION_SENT = v_notification_sent,
         PROCESS_DATA = v_process_data, LAST_MODIFIED_DATE = SYSDATE,
        FK_FORMLIBVER = v_new_formlibver
     WHERE pk_patforms = v_new_patforms;

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patforms',
        v_orig_patforms ,SYSDATE, 'pat_filledforms', 'M');


      IF (v_getformfield_count  > 0) THEN

               getFormColumnString(v_getformfield_count,  v_new_form,v_orig_form ,  v_form_cols ,  v_form_cols_data,'M' )  ;

            v_linear_sql := ' update er_formslinear set FILLDATE = :FILLDATE, FORM_COMPLETED = :FORM_COMPLETED, IS_VALID = :IS_VALID, NOTIFICATION_SENT = :NOTIFICATION_SENT,
              ID = :ID, FK_PATPROT = :FK_PATPROT ' || v_form_cols || ' WHERE  FK_FILLEDFORM  = :FK_FILLEDFORM AND form_type = ''P''' ;

            IF (LENGTH(v_form_cols_data )) > 1 THEN
              v_form_cols_data := SUBSTR(v_form_cols_data ,2);
            END IF;



            v_linear_update_sql := 'select ' || v_form_cols_data  || ' from IMPPAT_FILLEDFORMS1 where  FK_FILLEDFORM = ' || v_orig_patforms || ' and rownum < 2 ';

            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - OLD Patient FormLinear record for site '|| v_site_code || ' with original PK:' || v_orig_patforms || 'v_linear_sql' || v_linear_sql , v_success );
             Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - OLD Patient FormLinear record for site '|| v_site_code || ' with original PK:' || v_orig_patforms || 'v_linear_update_sql ' || v_linear_update_sql  , v_success );


            source_cursor := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(source_cursor,   v_linear_update_sql ,  DBMS_SQL.native);


                  FOR  i IN 1..v_getformfield_count
             LOOP
                   DBMS_SQL.DEFINE_COLUMN(source_cursor, i,formColvalue,4000);
           END LOOP ;

          ignore := DBMS_SQL.EXECUTE(source_cursor);
          DBMS_SQL.DESCRIBE_COLUMNS(source_cursor,v_getformfield_count, rec_tab);


           -- Prepare a cursor to insert into the destination table:
             destination_cursor := DBMS_SQL.OPEN_CURSOR;
             DBMS_SQL.PARSE(destination_cursor, v_linear_sql,DBMS_SQL.native);

          -- Fetch a row from the source table and insert it into the destination table:
             LOOP
               IF DBMS_SQL.FETCH_ROWS(source_cursor)>0 THEN
                 -- get column values of the row
              FOR  i IN 1..v_getformfield_count
              LOOP
                      DBMS_SQL.COLUMN_VALUE(source_cursor, i, formColvalue);

                      -- get column name from desc_tab
                      v_desc_rec := rec_tab(i);


                     -- Bind the column into the destination cursor
                DBMS_SQL.BIND_VARIABLE(destination_cursor, v_desc_rec.col_name ,formColvalue);
                 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS - OLD Patient FormLinear record for site '|| v_site_code || ' with original PK:' || v_orig_patforms || 'value of  col-' ||v_desc_rec.col_name || ':' ||  formColvalue  , v_success );
            END LOOP;

                  DBMS_SQL.BIND_VARIABLE(destination_cursor, ':FK_FILLEDFORM', v_new_patforms);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':FILLDATE', v_patform_filldate);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':FORM_COMPLETED', v_code_form_completed);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':IS_VALID', v_isvalid);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':NOTIFICATION_SENT',  v_notification_sent);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':ID',v_new_per);
             DBMS_SQL.BIND_VARIABLE(destination_cursor, ':FK_PATPROT', v_new_patprot);

                   ignore := DBMS_SQL.EXECUTE(destination_cursor);
              ELSE
                 -- No more rows to copy:
                EXIT;
              END IF;
            END LOOP;

          -- Commit and close all cursors:
             COMMIT;
             DBMS_SQL.CLOSE_CURSOR(source_cursor);
             DBMS_SQL.CLOSE_CURSOR(destination_cursor);
          -------forms linear updated
     END IF;

      ELSIF v_new_patforms = -1 THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATFORMS:' || v_err, v_success );
   END IF; -- for checking v_new_patprot

       END IF; --for checking the FKs

   ---------

   END LOOP;

  CLOSE v_cur ;

  COMMIT;

 END;

  -----------------------------------------------

    PROCEDURE SP_EXP_PATADVERSE (
    P_EXPID          NUMBER,
      O_RESULT_PATADVERSE     OUT  Gk_Cv_Types.GenericCursorType,
   O_RESULT_PATADVERSE_INFO     OUT  Gk_Cv_Types.GenericCursorType,
     O_RESULT_PATADVERSE_NOTIFY     OUT  Gk_Cv_Types.GenericCursorType
    )
   /* Author: Sonia Sahni
   Date: 05/03/04
   Purpose - Returns a ref cursors of patient adverse events data
   */
   AS
    v_rec_adv rec_sch_adverseve   := rec_sch_adverseve(0,'','','',SYSDATE,SYSDATE,0,'',SYSDATE,'','','',0,0,'','','','',0,'',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL); --record type
    v_table_schadverse tab_sch_adverseve := tab_sch_adverseve(); --index-by table type

   v_cnt NUMBER := 0;


   v_err VARCHAR2(4000);

   v_adverseve_table_name VARCHAR2(50);
   v_site_code VARCHAR2(255);

   v_sql LONG;
   v_study VARCHAR2(4000);
   v_sponsor_study NUMBER;

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

   v_sitecode VARCHAR2(20);
   v_module_subtype VARCHAR2(20);


   BEGIN

  --get export request

  Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

  --find out the study id from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

 --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);

    -- Get patient forms data


   v_adverseve_table_name := 'sch_adverseve';

   v_module_subtype := 'pat_adverse';

    UPDATE ER_REV_PENDINGDATA
 SET rp_processedflag = 1
 WHERE rp_module = v_module_subtype AND rp_sitecode = v_sitecode ;

 COMMIT;


  -- get data --
 FOR i IN ( SELECT PK_ADVEVE,FK_EVENTS1, (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = FK_CODLST_AETYPE) FK_CODLST_AETYPE_SUBTYP,
      AE_DESC,AE_STDATE, AE_ENDDATE,  AE_ENTERBY,
      AE_OUTTYPE,  AE_OUTDATE, AE_OUTNOTES,  AE_ADDINFO, AE_NOTES, e.FK_STUDY,
    FK_PER,  (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = AE_SEVERITY) AE_SEVERITY_SUBTYP,
    (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = AE_BDSYSTEM_AFF) AE_BDSYSTEM_AFF_SUBTYP,
    (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = AE_RELATIONSHIP) AE_RELATIONSHIP_SUBTYP,
    (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = AE_RECOVERY_DESC) AE_RECOVERY_DESC_SUBTYP,
    AE_GRADE, AE_NAME,  AE_LKP_ADVID ,
    RP_SITECODE , FK_SPONSOR_ACCOUNT      ,
    (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = FORM_STATUS) FORM_STATUS,  AE_TREATMENT_COURSE,
    (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = FK_CODELST_OUTACTION)   AE_ACTIONTAKEN_SUBTYPE,
    ORIGINATING_SITECODE,meddra,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE,dictionary
     FROM sch_adverseve e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
       WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_adverseve_table_name AND
    PK_ADVEVE = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
    AND NVL(RP_RECORD_TYPE,'O') <> 'D' and  nvl(rp_processedflag,0) = 1
   UNION
   SELECT  RP_TABLEPK  PK_ADVEVE,
   NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,ER_REV_PENDINGDATA.fk_study,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
   RP_SITECODE, FK_SPONSOR_ACCOUNT,NULL,NULL,NULL,ORIGINATING_SITECODE,NULL,NVL(RP_RECORD_TYPE,'O') RP_RECORD_TYPE,
   NULL
   FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_adverseve_table_name AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND NVL(rp_record_type,'O') = 'D' AND rp_processedflag = 1
)
 LOOP
    v_cnt := v_cnt + 1;

     v_site_code := i.RP_SITECODE;

   --get sponsor study id
    SP_GET_SPONSORPK(i.FK_STUDY, 'er_study' , v_site_code,  v_sponsor_study,v_err);

    IF v_sponsor_study > 0 THEN

      v_rec_adv.PK_ADVEVE := i.PK_ADVEVE;
       --  v_rec_adv.FK_EVENTS1 := i.FK_EVENTS1;

      v_rec_adv.FK_CODLST_AETYPE_SUBTYP := i.FK_CODLST_AETYPE_SUBTYP;
      v_rec_adv.AE_DESC := i.AE_DESC;
      v_rec_adv.AE_STDATE := i.AE_STDATE;
      v_rec_adv.AE_ENDDATE := i.AE_ENDDATE;
      v_rec_adv.AE_ENTERBY := i.AE_ENTERBY;

         v_rec_adv.AE_OUTTYPE := i.AE_OUTTYPE;
      v_rec_adv.AE_OUTDATE  := i.AE_OUTDATE;
      v_rec_adv.AE_OUTNOTES  := i.AE_OUTNOTES;
      v_rec_adv.AE_ADDINFO  := i.AE_ADDINFO;
      v_rec_adv.AE_NOTES  := i.AE_NOTES;
      v_rec_adv.FK_STUDY  := v_sponsor_study;
      v_rec_adv.FK_PER := i.FK_PER;
      v_rec_adv.AE_SEVERITY_SUBTYP := i.AE_SEVERITY_SUBTYP;
      v_rec_adv.AE_BDSYSTEM_AFF_SUBTYP := i.AE_BDSYSTEM_AFF_SUBTYP;
      v_rec_adv.AE_RELATIONSHIP_SUBTYP := i.AE_RELATIONSHIP_SUBTYP;

      v_rec_adv.AE_RECOVERY_DESC_SUBTYP := i.AE_RECOVERY_DESC_SUBTYP;
      v_rec_adv.AE_GRADE := i.AE_GRADE;
      v_rec_adv.AE_NAME := i.AE_NAME ;
      v_rec_adv.AE_LKP_ADVID := i.AE_LKP_ADVID;

      v_rec_adv.SITE_CODE := v_site_code;
      v_rec_adv.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;

      v_rec_adv.FORM_STATUS := i.FORM_STATUS;
       v_rec_adv.AE_TREATMENT_COURSE := i.AE_TREATMENT_COURSE;
       v_rec_adv.AE_ACTIONTAKEN_SUBTYPE  := i.AE_ACTIONTAKEN_SUBTYPE  ;
       v_rec_adv.ORIGINATING_SITECODE  := i.ORIGINATING_SITECODE;
       v_rec_adv.meddra  := i.meddra;
       v_rec_adv.record_type := i.RP_RECORD_TYPE;
       v_rec_adv.dictionary := i.dictionary;

     --store each record in a table
           v_table_schadverse.EXTEND;
     v_table_schadverse (v_cnt) := v_rec_adv;
     v_cnt := v_table_schadverse.COUNT();
    END IF;

 END LOOP;
 -- populate out cursor
  OPEN  O_RESULT_PATADVERSE
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_schadverse AS tab_sch_adverseve));


 -- get sch_advinfo data

 OPEN  O_RESULT_PATADVERSE_INFO
 FOR
   SELECT PK_ADVINFO ,FK_ADVERSE,
 (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = FK_CODELST_INFO) FK_CODELST_INFO_SUBTYP,
 ADVINFO_VALUE,ADVINFO_TYPE, RP_SITECODE SITE_CODE, FK_SPONSOR_ACCOUNT,ORIGINATING_SITECODE
  FROM sch_advinfo e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
     WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_adverseve_table_name AND
  FK_ADVERSE = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
  and nvl(RP_RECORD_TYPE,'O') <> 'D'  ;


 OPEN O_RESULT_PATADVERSE_NOTIFY
 FOR
 SELECT
 PK_ADVNOTIFY,  FK_ADVERSEVE,
 (SELECT  codelst_subtyp FROM  sch_codelst WHERE pk_codelst = FK_CODELST_NOT_TYPE) FK_CODELST_NOT_TYPE_SUBTYP,
 ADVNOTIFY_DATE,
 ADVNOTIFY_NOTES,
 ADVNOTIFY_VALUE ,  RP_SITECODE SITE_CODE, FK_SPONSOR_ACCOUNT,ORIGINATING_SITECODE
  FROM sch_advnotify e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
     WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_adverseve_table_name AND
  FK_ADVERSEVE = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
  and nvl(RP_RECORD_TYPE,'O') <> 'D'  ;

  END SP_EXP_PATADVERSE;

 -----------------------------------------------

  PROCEDURE SP_IMP_PATADVERSE (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  05/03/04
    Purpose : Import patient adverse events data from temp tables
  */


 v_sql_adv LONG;
 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_per NUMBER;
 v_new_per NUMBER;

 v_site_code VARCHAR2(20);
 v_sponsor_code VARCHAR2(20);
 v_fk_account_sponsor NUMBER;

 v_err VARCHAR2(4000);

 V_EXPID NUMBER;
 v_success NUMBER;

 v_orig_study NUMBER;
 v_new_study NUMBER;

 v_creator NUMBER;

 v_orig_adveve NUMBER;
 v_new_adveve NUMBER;
 v_orig_event CHAR(10);
 v_ae_type_subtyp VARCHAR2(15);
 v_ae_type_fk_code NUMBER;

 v_ae_desc VARCHAR2(2000);
 v_ae_stdate DATE;
 v_ae_enddate DATE;
 v_ae_orig_enterby NUMBER;
 v_ae_new_enterby NUMBER;

 v_ae_outtype VARCHAR2(20);
 v_ae_outdate DATE;
 v_ae_outnotes  VARCHAR2(1000);
 v_ae_addinfo VARCHAR2(20);
 v_ae_notes    VARCHAR2(4000);


 v_ae_severity_subtyp VARCHAR2(20);
 v_ae_severity_fk_code NUMBER;

 v_ae_bdsystem_subtype VARCHAR2(20);
 v_ae_bdsystem_fk_code NUMBER;

 v_ae_relationship_subtyp VARCHAR2(20);
 v_ae_relationship_fk_code NUMBER;

 v_ae_recovery_subtyp VARCHAR2(20);
 v_ae_recovery_fk_code NUMBER;

 v_ae_grade NUMBER;
 v_ae_name   VARCHAR2(500);
 v_ae_lkpadvid NUMBER;

 v_outcome_count NUMBER;
 v_advinfo_count NUMBER;

 v_sql_advinfo LONG;
 v_orig_advinfo NUMBER;
 v_new_advinfo NUMBER;
    v_adv_info_subtyp VARCHAR2(20);
    v_adv_info_fk_code NUMBER;
 v_advinfo_value NUMBER;
 v_advinfo_type CHAR(1);

 v_sql_advnotify LONG;
 v_orig_advnotify NUMBER;
 v_new_advnotify NUMBER;
 v_advnot_subtyp VARCHAR2(20);
 v_advnot_fk_code NUMBER;
 v_advnot_date DATE;
    v_advnotify_notes VARCHAR2(500);
 v_advnotify_value NUMBER;

 v_formstatus_pk NUMBER;
 v_formstatus_subtyp VARCHAR2(255);

 v_code_type VARCHAR2(15);
 v_AE_TREATMENT_COURSE VARCHAR2(150);

 V_AE_ACTIONTAKEN_SUBTYPE VARCHAR2(15);
  V_AE_ACTIONTAKEN_CODE NUMBER;
  v_originating_sitecode varchar2(10);
  v_meddra varchar2(255);
  v_record_type char(1);
  v_dictionary varchar2(50);

 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;


   v_sql_adv := 'Select PK_ADVEVE,
        FK_EVENTS1,FK_CODLST_AETYPE_SUBTYP,  AE_DESC, AE_STDATE, AE_ENDDATE,
       AE_ENTERBY, AE_OUTTYPE,AE_OUTDATE,  AE_OUTNOTES,
       AE_ADDINFO, AE_NOTES,  FK_STUDY, FK_PER,  AE_RELATIONSHIP_SUBTYP,
       AE_RECOVERY_DESC_SUBTYP,  AE_GRADE,  AE_NAME,
       AE_LKP_ADVID, SITE_CODE,FK_SPONSOR_ACCOUNT, FORM_STATUS, AE_TREATMENT_COURSE, AE_ACTIONTAKEN_SUBTYPE,originating_sitecode,meddra,
       record_type, dictionary
       FROM IMPPAT_ADVERSE0';

  --get data

   OPEN v_cur FOR v_sql_adv;
    LOOP

     FETCH v_cur INTO v_orig_adveve,
    v_orig_event,v_ae_type_subtyp,v_ae_desc, v_ae_stdate, v_ae_enddate,
    v_ae_orig_enterby , v_ae_outtype, v_ae_outdate,v_ae_outnotes,
    v_ae_addinfo,v_ae_notes,v_orig_study,v_orig_per, v_ae_relationship_subtyp,
    v_ae_recovery_subtyp,v_ae_grade, v_ae_name,
    v_ae_lkpadvid, v_site_code,v_fk_account_sponsor,v_formstatus_subtyp, V_AE_TREATMENT_COURSE, V_AE_ACTIONTAKEN_SUBTYPE,v_originating_sitecode,v_meddra,
    v_record_type, v_dictionary;

     EXIT WHEN v_cur %NOTFOUND;

     -- get the site code , creator for the sponsor account


           SELECT site_code , AC_USRCREATOR
        INTO v_sponsor_code, v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;


     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_adveve, 'sch_adverseve',v_site_code, v_new_adveve,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_ae_orig_enterby, 'er_user',v_site_code, v_ae_new_enterby,v_err ,v_fk_account_sponsor);

         SP_GET_SITEPK(v_orig_study, 'er_study',v_sponsor_code, v_new_study,v_err,v_originating_sitecode);

     ---------
     -- get new IDs

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE -v_new_study' || v_new_study , v_success);
Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE -v_new_per' || v_new_per , v_success);


     IF v_new_study <= 0 THEN  -- original sponsor study not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create patient adverse event record, Sponsor Study for site (' || v_site_code || ') does not exist. Original Study:' || v_orig_study, v_success );
        ELSIF v_new_per <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create patient adverse event record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );

     ELSE -- all required FKs exist

 IF (v_record_type <> 'D') THEN -- check only if the records is not to be deleted
               IF v_ae_new_enterby <= 0 THEN
                   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE User for site (' || v_site_code || ') does not exist. Original PK_USER:' || v_ae_orig_enterby, v_success );
                v_ae_new_enterby := NULL;
               END IF;

                  -- get patient adverse event type

                v_code_type := 'adve_type';

                IF v_ae_type_subtyp IS NOT NULL THEN
                 SELECT Pkg_Impex.getSchCodePk (v_ae_type_subtyp, v_code_type)
                 INTO v_ae_type_fk_code
                 FROM dual;
                END IF;

                  IF v_ae_type_fk_code = -1 THEN
                     v_ae_type_fk_code:= NULL;
                 END IF;

                 v_code_type := 'adve_relation';

                 IF v_ae_relationship_subtyp IS NOT NULL THEN
                 SELECT Pkg_Impex.getSchCodePk (v_ae_relationship_subtyp, v_code_type)
                 INTO v_ae_relationship_fk_code
                 FROM dual;
                END IF;

                   IF v_ae_relationship_fk_code = -1 THEN
                     v_ae_relationship_fk_code:= NULL;
                 END IF;


                 v_code_type := 'adve_recovery';

                 IF v_ae_recovery_subtyp IS NOT NULL THEN
                 SELECT Pkg_Impex.getSchCodePk (v_ae_recovery_subtyp, v_code_type )
                 INTO v_ae_recovery_fk_code
                 FROM dual;
                END IF;

                    IF v_ae_recovery_fk_code = -1 THEN
                     v_ae_recovery_fk_code:= NULL;
                 END IF;

                 --get adv action

                 IF V_AE_ACTIONTAKEN_SUBTYPE IS NOT NULL THEN
                     SELECT Pkg_Impex.getSchCodePk (V_AE_ACTIONTAKEN_SUBTYPE , 'outaction' )
                   INTO V_AE_ACTIONTAKEN_code
                   FROM dual;
                END IF;

                      IF V_AE_ACTIONTAKEN_code  = -1 THEN
                       V_AE_ACTIONTAKEN_code := NULL;
                   END IF;



                 -- get from status

                 v_code_type :=  'fillformstat';

                   IF v_formstatus_subtyp IS NOT NULL THEN
                 SELECT Pkg_Impex.getSchCodePk (v_formstatus_subtyp,v_code_type)
                 INTO v_formstatus_pk
                 FROM dual;
                END IF;

                    IF v_formstatus_pk = -1 THEN
                     v_formstatus_pk := NULL;
                 END IF;



                -- get the code count for 'outcome'' from sch_codelst

                v_code_type := 'outcome';

                SELECT COUNT(*)
                INTO v_outcome_count
                FROM sch_codelst WHERE trim(codelst_type) = v_code_type;

                IF LENGTH(NVL(v_ae_outtype,'')) > v_outcome_count THEN
                 -- extract the characters till v_outcome_count
                v_ae_outtype := SUBSTR(v_ae_outtype,1,v_outcome_count);
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE for site (' || v_site_code || '), value of v_ae_outtype truncated after :' || v_outcome_count || ' characters', v_success );

               END IF;

                -- get the code count for 'outcome'' from sch_codelst

                v_code_type := 'adve_info';

                SELECT COUNT(*)
                INTO v_advinfo_count
                FROM sch_codelst WHERE trim(codelst_type) = v_code_type;

                IF LENGTH(NVL(v_ae_addinfo,'')) > v_advinfo_count THEN
                 -- extract the characters till v_outcome_count
                v_ae_addinfo := SUBSTR(v_ae_addinfo,1,v_advinfo_count);
                Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE for site (' || v_site_code || '), value of v_ae_addinfo truncated after :' || v_advinfo_count || ' characters', v_success );

               END IF;
End if; -- end of delete check

  IF v_record_type = 'D' THEN
     IF  v_new_adveve > 0 THEN
              DELETE FROM SCH_ADVERSEVE WHERE PK_ADVEVE = v_new_adveve ;

              delete from sch_advinfo where fk_adverse = v_new_adveve;

              delete from sch_advnotify where FK_ADVERSEVE = v_new_adveve;

              INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_adverseve',
        v_orig_adveve ,SYSDATE, 'pat_adverse', 'D');

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE - Deleted Patient Adverse Events record for site ' || v_site_code || ' with original PK:' || v_orig_adveve , v_success );

     ELSE
            Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not delete patient adverse event record, record does not exist. Original PK_SCHADVERSEVE:' || v_orig_adveve, v_success );
     END IF;


 ELSE --not delete type record

   IF  v_new_adveve = 0 THEN -- new enrollment record

    SELECT seq_sch_adverseve.NEXTVAL INTO v_new_adveve FROM dual;

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE - New Patient Adverse Events record for site ' || v_site_code || ' with original PK:' || v_orig_adveve , v_success );
    Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_adveve, 'sch_adverseve',v_site_code,v_new_adveve,v_err ,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN
       Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create record for er_sponsorpkmap :' || v_err, v_success );
     ELSE
         BEGIN
       INSERT INTO SCH_ADVERSEVE (PK_ADVEVE, FK_EVENTS1,  FK_CODLST_AETYPE,
        AE_DESC,  AE_STDATE, AE_ENDDATE, AE_ENTERBY,
         AE_OUTTYPE,  AE_OUTDATE, AE_OUTNOTES, AE_ADDINFO, AE_NOTES, CREATOR,
          CREATED_ON,  FK_STUDY,  FK_PER, AE_RELATIONSHIP,
       AE_RECOVERY_DESC,  AE_GRADE,  AE_NAME, AE_LKP_ADVID,FORM_STATUS,AE_TREATMENT_COURSE,FK_CODELST_OUTACTION,meddra,dictionary)
      VALUES(v_new_adveve,v_orig_event,v_ae_type_fk_code,
      v_ae_desc, v_ae_stdate, v_ae_enddate, v_ae_new_enterby ,
      v_ae_outtype, v_ae_outdate,v_ae_outnotes,v_ae_addinfo,v_ae_notes,v_creator,
      SYSDATE,v_new_study,v_new_per, v_ae_relationship_fk_code,
      v_ae_recovery_fk_code,v_ae_grade, v_ae_name, v_ae_lkpadvid, v_formstatus_pk, v_AE_TREATMENT_COURSE,V_AE_ACTIONTAKEN_code,v_meddra,
      v_dictionary);

        INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_adverseve',
        v_orig_adveve ,SYSDATE, 'pat_adverse', 'N');

       EXCEPTION WHEN OTHERS THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVRESE:Could not create New Patient Adverse Events record for site '|| v_site_code || ' with original PK:' || v_orig_adveve || ' Error: ' || SQLERRM, v_success );
      END;
         END IF;
      ELSIF v_new_adveve > 0 THEN -- old enrollment record

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE- Old Patient Adverse Events record for site ' || v_site_code || ' with original PK:' || v_orig_adveve , v_success );

       UPDATE  SCH_ADVERSEVE
     SET  FK_EVENTS1 = v_orig_event,  FK_CODLST_AETYPE = v_ae_type_fk_code,
         AE_DESC = v_ae_desc,  AE_STDATE = v_ae_stdate, AE_ENDDATE = v_ae_enddate, AE_ENTERBY = v_ae_new_enterby,
        AE_OUTTYPE = v_ae_outtype,  AE_OUTDATE = v_ae_outdate, AE_OUTNOTES = v_ae_outnotes,
        AE_ADDINFO = v_ae_addinfo, AE_NOTES = v_ae_notes, CREATOR = v_creator,
         FK_STUDY = v_new_study,  FK_PER = v_new_per, AE_RELATIONSHIP = v_ae_relationship_fk_code ,
        AE_RECOVERY_DESC = v_ae_recovery_fk_code,  AE_GRADE = v_ae_grade,  AE_NAME = v_ae_name,
        AE_LKP_ADVID = v_ae_lkpadvid,LAST_MODIFIED_DATE = SYSDATE ,LAST_MODIFIED_BY = v_creator,
        FORM_STATUS = v_formstatus_pk, AE_TREATMENT_COURSE = v_AE_TREATMENT_COURSE,FK_CODELST_OUTACTIOn = V_AE_ACTIONTAKEN_code,
        meddra = v_meddra, dictionary = v_dictionary
     WHERE PK_ADVEVE = v_new_adveve;

        INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
        REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_adverseve',
        v_orig_adveve ,SYSDATE, 'pat_adverse', 'M');


        ELSIF v_new_adveve = -1 THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE:' || v_err, v_success );
     END IF; -- for checking v_new_adveve

   END IF; -- for checking record_type = 'D'

  END IF; --for checking the FKs

   ---------

   END LOOP;

  CLOSE v_cur ;

  v_sql_advinfo := 'Select PK_ADVINFO ,  FK_ADVERSE,  FK_CODELST_INFO_SUBTYP, ADVINFO_VALUE, ADVINFO_TYPE,
           SITE_CODE , FK_SPONSOR_ACCOUNT
           FROM IMPPAT_ADVERSE1 ';

  OPEN v_cur FOR v_sql_advinfo;
    LOOP

     FETCH v_cur INTO v_orig_advinfo, v_orig_adveve ,v_adv_info_subtyp,v_advinfo_value,v_advinfo_type,v_site_code, v_fk_account_sponsor;
     EXIT WHEN v_cur %NOTFOUND;

      Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_advinfo, 'sch_advinfo',v_site_code, v_new_advinfo,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_adveve, 'sch_adverseve',v_site_code, v_new_adveve,v_err ,v_fk_account_sponsor);

     IF v_new_adveve <= 0 THEN
           Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create patient adverse event Info record, Adverse Event for site (' || v_site_code || ') does not exist. Original PK_ADVEVE:' || v_orig_adveve, v_success );
     ELSE -- all required FKs exist

     SELECT fk_study INTO v_new_study FROM sch_adverseve WHERE pk_adveve = v_new_adveve;

     IF v_adv_info_subtyp IS NOT NULL THEN

    IF v_Advinfo_type = 'O' THEN

    v_code_type := 'outcome';

     SELECT Pkg_Impex.getSchCodePk (v_adv_info_subtyp, v_code_type)
     INTO v_adv_info_fk_code
     FROM dual;
    ELSE

    v_code_type :='adve_info';

       SELECT Pkg_Impex.getSchCodePk (v_adv_info_subtyp, v_code_type)
     INTO v_adv_info_fk_code
     FROM dual;
    END IF;


     END IF;

        IF v_adv_info_fk_code = -1 THEN
         v_adv_info_fk_code:= NULL;
     END IF;

       -- get the creator for the sponsor account

           SELECT AC_USRCREATOR
        INTO v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;


      IF v_new_advinfo = 0 THEN -- new adverse event info record

     SELECT seq_sch_advinfo.NEXTVAL INTO v_new_advinfo FROM dual;

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE - New Patient Adverse Event Info record for site ' || v_site_code || ' with original PK:' || v_orig_advinfo , v_success );
     Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_advinfo, 'sch_advinfo',v_site_code,v_new_advinfo,v_err ,v_fk_account_sponsor);

       IF LENGTH(trim(v_err)) > 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create record for er_sponsorpkmap :' || v_err, v_success );
      ELSE
          BEGIN

        INSERT INTO SCH_ADVINFO (PK_ADVINFO,
        FK_ADVERSE,  FK_CODELST_INFO, ADVINFO_VALUE,
        ADVINFO_TYPE,CREATOR, CREATED_ON)
        VALUES (v_new_advinfo,v_new_adveve ,v_adv_info_fk_code,v_advinfo_value,v_advinfo_type,v_creator,SYSDATE);

          INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_advinfo',
        v_orig_advinfo ,SYSDATE, 'pat_adverse', 'N');


        EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVRESE:Could not create New Patient Adverse Event Info record for site '|| v_site_code || ' with original PK:' || v_orig_advinfo || ' Error: ' || SQLERRM, v_success );
       END;
          END IF;
       ELSIF v_new_advinfo > 0 THEN -- old  adverse event info record
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE- Old Patient Adverse Event Info record for site ' || v_site_code || ' with original PK:' || v_orig_advinfo , v_success );

        UPDATE  SCH_ADVINFO
      SET  FK_CODELST_INFO = v_adv_info_fk_code, ADVINFO_VALUE = v_advinfo_value,
        ADVINFO_TYPE = v_advinfo_type, LAST_MODIFIED_DATE = SYSDATE ,LAST_MODIFIED_BY = v_creator
      WHERE PK_ADVINFO = v_new_advinfo;

         INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_advinfo',
        v_orig_advinfo ,SYSDATE, 'pat_adverse', 'M');

         ELSIF v_new_advinfo = -1 THEN
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE:' || v_err, v_success );
    END IF; -- for checking v_new_advinfo


     END IF; -- for FK check
   END LOOP;

      CLOSE v_cur ;


  v_sql_advnotify := 'Select PK_ADVNOTIFY, FK_ADVERSEVE,  FK_CODELST_NOT_TYPE_SUBTYP, ADVNOTIFY_DATE ,
           ADVNOTIFY_NOTES,ADVNOTIFY_VALUE  , SITE_CODE ,FK_SPONSOR_ACCOUNT
           FROM IMPPAT_ADVERSE2 ';

  OPEN v_cur FOR v_sql_advnotify;
    LOOP

     FETCH v_cur INTO v_orig_advnotify, v_orig_adveve ,v_advnot_subtyp,v_advnot_date,
               v_advnotify_notes,v_advnotify_value, v_site_code,v_fk_account_sponsor;
     EXIT WHEN v_cur %NOTFOUND;

      Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_advnotify, 'sch_advnotify',v_site_code, v_new_advnotify,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_adveve, 'sch_adverseve',v_site_code, v_new_adveve,v_err ,v_fk_account_sponsor);

     IF v_new_adveve <= 0 THEN
           Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create patient adverse event Info record, Adverse Event Notification for site (' || v_site_code || ') does not exist. Original PK_ADVEVE:' || v_orig_adveve, v_success );
     ELSE -- all required FKs exist

       SELECT fk_study INTO v_new_study FROM sch_adverseve WHERE pk_adveve = v_new_adveve;

     IF v_advnot_subtyp IS NOT NULL THEN
        v_code_type := 'adve_notify';

       SELECT Pkg_Impex.getSchCodePk (v_advnot_subtyp, v_code_type)
     INTO v_advnot_fk_code
     FROM dual;
     END IF;

      IF v_advnot_fk_code = -1 THEN
         v_advnot_fk_code:= NULL;
     END IF;

        -- get the creator for the sponsor account

           SELECT AC_USRCREATOR
        INTO v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;



      IF v_new_advnotify = 0 THEN -- new adverse event notification record

     SELECT seq_sch_advnotify.NEXTVAL INTO v_new_advnotify FROM dual;

      Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE - New Patient Adverse Event Notification record for site ' || v_site_code || ' with original PK:' || v_orig_advnotify , v_success );
     Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_advnotify, 'sch_advnotify',v_site_code,v_new_advnotify,v_err ,v_fk_account_sponsor);

       IF LENGTH(trim(v_err)) > 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE Could not create record for er_sponsorpkmap :' || v_err, v_success );
      ELSE
          BEGIN

        INSERT INTO SCH_ADVNOTIFY ( PK_ADVNOTIFY,
         FK_ADVERSEVE, FK_CODELST_NOT_TYPE,
         ADVNOTIFY_DATE, ADVNOTIFY_NOTES, CREATOR,
          CREATED_ON, ADVNOTIFY_VALUE  )
        VALUES (v_new_advnotify,v_new_adveve ,v_advnot_fk_code,v_advnot_date, v_advnotify_notes,v_creator,SYSDATE,v_advnotify_value);

          INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_advnotify',
        v_orig_advnotify ,SYSDATE, 'pat_adverse', 'N');

        EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVRESE:Could not create New Patient Adverse Event Notification record for site '|| v_site_code || ' with original PK:' || v_orig_advnotify || ' Error: ' || SQLERRM, v_success );
       END;
          END IF;
       ELSIF v_new_advnotify > 0 THEN -- old  adverse event notification record
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE- Old Patient Adverse Event Notification record for site ' || v_site_code || ' with original PK:' || v_orig_advnotify , v_success );

        UPDATE  SCH_ADVNOTIFY
      SET  FK_CODELST_NOT_TYPE = v_advnot_fk_code,
         ADVNOTIFY_DATE = v_advnot_date, ADVNOTIFY_NOTES =  v_advnotify_notes,
         ADVNOTIFY_VALUE = v_advnotify_value , LAST_MODIFIED_DATE = SYSDATE ,LAST_MODIFIED_BY = v_creator
      WHERE PK_ADVNOTIFY = v_new_advnotify;

        INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'sch_advnotify',
        v_orig_advnotify ,SYSDATE, 'pat_adverse', 'M');

         ELSIF v_new_advnotify = -1 THEN
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATADVERSE:' || v_err, v_success );
    END IF; -- for checking v_new_advinfo


     END IF; -- for FK check
   END LOOP;

      CLOSE v_cur ;

  COMMIT;

 END;

  -----------------------------------------------
   PROCEDURE SP_BACKUP_EXPDATA (p_expid NUMBER, p_studysponsorpk NUMBER,o_str OUT VARCHAR2)
 AS
 /* Author - Sonia Sahni
    Date - 05/04/2004
   Purpose - transfers the exported data to er_rev_datasent */

 v_senton_dt DATE;
 v_study NUMBER;
 v_site_code VARCHAR2(20);
    v_rowcount NUMBER;

 v_pat_count NUMBER := 0;
 v_patenr_count NUMBER := 0;
 v_patstat_count NUMBER := 0;
 v_patform_count NUMBER := 0;
 v_patadv_count NUMBER := 0;
 v_patfacility_count NUMBER := 0;
 v_patlabs_count NUMBER := 0;
 v_users VARCHAR2(250);
 v_message_template CLOB;
 v_message_text CLOB;

 V_POS  NUMBER :=0 ;
   v_params VARCHAR2(1000);
    V_USER_STR VARCHAR2(500);
    v_cnt NUMBER ;

-- v_studynumber VARCHAR2(30);
  v_studynumber VARCHAR2(100);--JM: 120406
  v_fparam VARCHAR2(1000);

 BEGIN
   BEGIN

    SELECT SYSDATE INTO v_senton_dt FROM dual;

   SELECT FK_SITESTUDY , site_code,NVL(notify_users,'0')
   INTO  v_study, v_site_code,v_users
   FROM ER_REV_STUDYSPONSOR
   WHERE pk_rev_studysponsor = p_studysponsorpk;

   BEGIN
      SELECT study_number INTO v_studynumber FROM ER_STUDY WHERE pk_study = v_study;

   EXCEPTION WHEN NO_DATA_FOUND THEN
       v_studynumber := ' ';
   END;

    SELECT COUNT(*) INTO v_rowcount  FROM ER_REV_PENDINGDATA
    WHERE fk_study = v_study AND RP_PROCESSEDFLAG = 2 AND rp_sitecode = v_site_code;


    INSERT INTO ER_REV_DATASENT (PK_RD, RD_TABLENAME, RD_TABLEPK, RD_SENT_ON, SITE_CODE,  FK_STUDY,
    EXP_ID, RD_MODULE,RD_RECORD_TYPE)
    SELECT seq_er_rev_datasent.NEXTVAL,RP_TABLENAME,  RP_TABLEPK,v_senton_dt,RP_SITECODE,  FK_STUDY,
             p_expid, RP_MODULE,RP_RECORD_TYPE
    FROM ER_REV_PENDINGDATA
    WHERE fk_study = v_study AND RP_PROCESSEDFLAG = 2 AND rp_sitecode = v_site_code;

    --get the sent data statistics and prepare notification
    IF v_rowcount  > 0 AND v_users <> '0'  THEN
     FOR k IN (SELECT  rp_tablename,rp_module, COUNT(*) AS ct
     FROM (
     SELECT DISTINCT rp_tablename,rp_module,rp_tablepk
     FROM ER_REV_PENDINGDATA
     WHERE fk_study = v_study AND RP_PROCESSEDFLAG = 2 AND rp_sitecode = v_site_code )
     GROUP BY rp_tablename,rp_module)
     LOOP
            IF k.rp_tablename = 'er_per' THEN
                  v_pat_count :=  k.ct;
        ELSIF k.rp_tablename = 'er_patprot' THEN
                v_patenr_count  :=  k.ct;
        ELSIF k.rp_tablename = 'er_patstudystat' THEN
                v_patstat_count  :=  k.ct;
        ELSIF k.rp_tablename = 'er_patforms' THEN
                 v_patform_count  := k.ct;
        ELSIF k.rp_tablename = 'sch_adverseve' THEN
                v_patadv_count   :=  k.ct;
        ELSIF k.rp_tablename = 'er_patfacility' THEN
                v_patfacility_count  :=  k.ct;
        ELSIF k.rp_tablename = 'er_patlabs' THEN
               v_patlabs_count :=  k.ct;
        END IF;
     END LOOP;

      v_message_template := PKG_COMMON.SCH_GETMAILMSG('a2a_sent');

       v_fparam :=  v_studynumber || '~'||  Pkg_Util.date_to_char(SYSDATE)  ||'~'||  p_expid  ||'~'|| v_site_code ||'~'|| v_pat_count  ||'~'||   v_patenr_count
                ||'~'||   v_patstat_count   || '~' ||   v_patform_count   || '~' ||  v_patadv_count
              || '~' ||  v_patlabs_count   || '~' ||      v_patfacility_count   ;

            v_message_text:=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);

        V_USERS :=  V_USERS|| ',' ;
         v_params := V_USERS ;

          LOOP
        V_CNT := V_CNT + 1;
        V_POS := INSTR (V_USERS, ',');
        V_PARAMS := SUBSTR (V_USERS, 1, V_POS - 1);
        EXIT WHEN V_PARAMS IS NULL;

        INSERT INTO sch_dispatchmsg (
      PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE ,  MSG_TEXT ,FK_PAT          )
           VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'U',  v_message_text,v_params ) ;

        V_USERS := SUBSTR (V_USERS, V_POS + 1);
             --   p('DONE');

    END LOOP ; /* end loop for multiple users*/


   END IF;   --for if v_users <>'0' and rowcount > 0

    ----------------------------------------

    DELETE FROM ER_REV_PENDINGDATA WHERE fk_study = v_study AND  RP_PROCESSEDFLAG = 2
    AND rp_sitecode = v_site_code ;

    UPDATE ER_EXPREQLOG
    SET exp_msg_status = 2 --message delivered
    WHERE pk_expreqlog = p_expid;

    COMMIT;

      EXCEPTION WHEN OTHERS THEN
     o_str := SQLERRM;
    END ;
 END;

 ------------------------------------------------------------

 ---------------------------------------------------------------------------------------------------------

 PROCEDURE SP_EXP_PATLABS (
    P_EXPID          NUMBER,
      O_RESULT_PATLABS     OUT  Gk_Cv_Types.GenericCursorType
    )
   /* Author: Sonia Sahni
   Date: 05/05/04
   Purpose - Returns a ref cursor of patient labs data
   */
   AS
    v_rec_patlabs rec_er_patlabs   := rec_er_patlabs(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
           NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
         NULL,NULL,NULL,NULL,NULL,NULL,NULL); --record type
    v_table_patlabs tab_er_patlabs := tab_er_patlabs(); --index-by table type

   v_cnt NUMBER := 0;
   v_sponsor_study NUMBER;

   v_err VARCHAR2(4000);

   v_patlabs_name VARCHAR2(50);
   v_site_code VARCHAR2(255);

   v_sql LONG;
   v_study VARCHAR2(4000);

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

   v_sitecode VARCHAR2(20);
   v_module_subtype VARCHAR2(20);

   BEGIN

  --get export request

  Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

  --find out the study id from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'study',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_study);

  --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);

    -- Get patient forms data


    v_patlabs_name := 'er_patlabs';
 v_module_subtype := 'pat_labs';

    UPDATE ER_REV_PENDINGDATA
 SET rp_processedflag = 1
 WHERE rp_module = v_module_subtype AND rp_sitecode = v_sitecode;

 COMMIT;


  -- get data --
 FOR i IN ( SELECT  e.FK_STUDY, PK_PATLABS , FK_PER , (SELECT labtest_shortname FROM ER_LABTEST WHERE pk_labtest = FK_TESTID ) FK_TESTID_SHORTNAME,
   TEST_DATE, TEST_RESULT,  TEST_RESULT_TX,  TEST_TYPE,TEST_UNIT, TEST_RANGE, TEST_LLN,  TEST_ULN, FK_SITE,
      (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_TSTSTAT) FK_CODELST_TSTSTAT_SUBTYP ,
    ACCESSION_NUM  , FK_TESTGROUP,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_ABFLAG) FK_CODELST_ABFLAG_SUBTYP,
    FK_PATPROT,  FK_EVENT_ID,  CUSTOM001,CUSTOM002,  CUSTOM003, CUSTOM004,  CUSTOM005, CUSTOM006,
    CUSTOM007, CUSTOM008,  CUSTOM009,  CUSTOM010, NOTES,ER_NAME ,ER_GRADE,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst =  FK_CODELST_STDPHASE)  FK_CODELST_STDPHASE_SUBTYP,
    RP_SITECODE , FK_SPONSOR_ACCOUNT,ORIGINATING_SITECODE,nvl(RP_RECORD_TYPE,'O') RP_RECORD_TYPE
     FROM ER_PATLABS e, ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
       WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patlabs_name AND
    pk_patlabs = RP_TABLEPK AND FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
    AND NVL(rp_record_type,'O') <> 'D' AND rp_processedflag = 1
      )
 LOOP
  v_cnt := v_cnt + 1;

     v_site_code := i.RP_SITECODE;

   --get sponsor form id
    SP_GET_SPONSORPK(i.FK_STUDY, 'er_study' , v_site_code,  v_sponsor_study,v_err);

    IF v_sponsor_study > 0 THEN

      v_rec_patlabs.fk_study := v_sponsor_study;
          v_rec_patlabs.PK_PATLABS := i.PK_PATLABS;
      v_rec_patlabs.FK_PER := i.FK_PER;
      v_rec_patlabs.FK_TESTID_SHORTNAME := i. FK_TESTID_SHORTNAME;
      v_rec_patlabs.TEST_DATE := i.TEST_DATE;
      v_rec_patlabs.TEST_RESULT := i.TEST_RESULT;
      v_rec_patlabs.TEST_RESULT_TX := i.TEST_RESULT_TX;
      v_rec_patlabs.TEST_TYPE := i.TEST_TYPE;
      v_rec_patlabs.TEST_UNIT := i.TEST_UNIT;
      v_rec_patlabs.TEST_RANGE := i.TEST_RANGE;
      v_rec_patlabs.TEST_LLN := i.TEST_LLN;
      v_rec_patlabs.TEST_ULN := i.TEST_ULN;
      v_rec_patlabs.FK_SITE := i.FK_SITE;

      v_rec_patlabs.FK_CODELST_TSTSTAT_SUBTYP := i.FK_CODELST_TSTSTAT_SUBTYP;
      v_rec_patlabs.ACCESSION_NUM := i.ACCESSION_NUM;
      v_rec_patlabs.FK_TESTGROUP := i.FK_TESTGROUP;

      v_rec_patlabs.FK_CODELST_ABFLAG_SUBTYP := i.FK_CODELST_ABFLAG_SUBTYP;
      v_rec_patlabs.FK_PATPROT := i.FK_PATPROT;
      v_rec_patlabs.FK_EVENT_ID := i.FK_EVENT_ID;
      v_rec_patlabs.CUSTOM001 := i.CUSTOM001;
      v_rec_patlabs.CUSTOM002 := i.CUSTOM002;
      v_rec_patlabs.CUSTOM003 := i.CUSTOM003;
      v_rec_patlabs.CUSTOM004 := i.CUSTOM004;
      v_rec_patlabs.CUSTOM005 := i.CUSTOM005;
      v_rec_patlabs.CUSTOM006 := i.CUSTOM006;
      v_rec_patlabs.CUSTOM007 := i.CUSTOM007;
       v_rec_patlabs.CUSTOM008 := i.CUSTOM008;
      v_rec_patlabs.CUSTOM009 := i.CUSTOM009;
      v_rec_patlabs.CUSTOM010 := i.CUSTOM010;

      v_rec_patlabs.NOTES := i.NOTES;
      v_rec_patlabs.ER_NAME  := i.ER_NAME ;
      v_rec_patlabs.ER_GRADE := i.ER_GRADE;

      v_rec_patlabs.FK_CODELST_STDPHASE_SUBTYP := i.FK_CODELST_STDPHASE_SUBTYP;

      v_rec_patlabs.SITE_CODE := v_site_code;
      v_rec_patlabs.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;
      v_rec_patlabs.ORIGINATING_SITECODE := i.ORIGINATING_SITECODE;
      v_rec_patlabs.record_type := i.RP_RECORD_TYPE;

     --store each record in a table
     v_table_patlabs.EXTEND;
     v_table_patlabs(v_cnt) := v_rec_patlabs;
     v_cnt := v_table_patlabs.COUNT();
    END IF;

 END LOOP;

 --get deleted labs

 FOR i IN (   SELECT  ER_REV_PENDINGDATA.fk_study, RP_TABLEPK  PK_PATLABS,
    RP_SITECODE, FK_SPONSOR_ACCOUNT,ORIGINATING_SITECODE, RP_RECORD_TYPE
    FROM ER_REV_PENDINGDATA, ER_REV_STUDYSPONSOR
   WHERE ER_REV_PENDINGDATA.fk_study = v_study AND RP_TABLENAME = v_patlabs_name AND
   ER_REV_STUDYSPONSOR.FK_SITESTUDY = ER_REV_PENDINGDATA.fk_study
   AND NVL(rp_record_type,'O') = 'D' AND rp_processedflag = 1
    )
 LOOP
      v_cnt := v_cnt + 1;

     v_site_code := i.RP_SITECODE;

   --get sponsor form id
    SP_GET_SPONSORPK(i.FK_STUDY, 'er_study' , v_site_code,  v_sponsor_study,v_err);

    IF v_sponsor_study > 0 THEN

      v_rec_patlabs.fk_study := v_sponsor_study;
      v_rec_patlabs.PK_PATLABS := i.PK_PATLABS;

      v_rec_patlabs.SITE_CODE := v_site_code;
      v_rec_patlabs.FK_SPONSOR_ACCOUNT := i.FK_SPONSOR_ACCOUNT;
      v_rec_patlabs.ORIGINATING_SITECODE := i.ORIGINATING_SITECODE;
      v_rec_patlabs.record_type := i.RP_RECORD_TYPE;

     --store each record in a table
     v_table_patlabs.EXTEND;
     v_table_patlabs(v_cnt) := v_rec_patlabs;
     v_cnt := v_table_patlabs.COUNT();
    END IF;

 END LOOP; --deleted labs


 -- populate out cursor
  OPEN  O_RESULT_PATLABS
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_patlabs AS tab_er_patlabs));


    END SP_EXP_PATLABS;

 ---------------------------------------------------------------------------------------------------------
 FUNCTION getRevPendingDataCount (p_modulename VARCHAR2, p_study NUMBER, p_site_code VARCHAR2, p_table_name VARCHAR2, p_tablepk NUMBER) RETURN NUMBER
 IS
 v_count NUMBER;
 BEGIN

  BEGIN
   SELECT COUNT(9)
   INTO v_count
   FROM ER_REV_PENDINGDATA
   WHERE FK_STUDY = p_study AND RP_MODULE = p_modulename  AND
    RP_SITECODE = p_site_code AND rp_tablename = p_table_name AND
    rp_tablepk = p_tablepk AND NVL(rp_processedflag,0) = 0;
   RETURN v_count;
     EXCEPTION WHEN NO_DATA_FOUND THEN
   RETURN 0;
  END;

 END;



  -------------------------------------------------------------
 PROCEDURE SP_IMP_PATLABS (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  045/05/04
    Purpose : Import patient labs data from temp tables
  */

 v_sql LONG;

 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_per NUMBER;
 v_site_code VARCHAR2(20);
 v_sponsor_code VARCHAR2(20);
 v_fk_account_sponsor NUMBER;
 v_new_per NUMBER;
 v_err VARCHAR2(4000);

 V_EXPID NUMBER;
 v_success NUMBER;

 v_orig_patprot NUMBER;
 v_new_patprot NUMBER;

 v_creator NUMBER;


 v_orig_patlabs NUMBER;
 v_new_patlabs NUMBER;

 v_testid_shortname VARCHAR2(100);
 v_test_date DATE;
 v_test_result VARCHAR2(100);
 v_test_result_tx CLOB;
 v_test_type CHAR(2);
 v_test_unit VARCHAR2(20);
 v_test_range  VARCHAR2(50);
 v_test_lln VARCHAR2(20);
 v_test_uln VARCHAR2(20);
 v_orig_site NUMBER;
 v_new_site NUMBER;
 v_codelst_tstat_subtyp VARCHAR2(20);
    v_accession_num VARCHAR2(20);
 v_test_group NUMBER;
 v_codelst_abflag_subtyp VARCHAR2(20);

 v_event_id NUMBER;
 v_notes CLOB;
 v_er_name VARCHAR2(500);
 v_er_grade NUMBER;
    v_codelst_stdphase_subtyp VARCHAR2(20);
 v_orig_study NUMBER;
 v_new_study  NUMBER;

 v_codelst_stdphase_fk_code NUMBER;
 v_codelst_abflag_fk_code NUMBER;
 v_codelst_tstat_fk_code NUMBER;
 v_test_id NUMBER;

 v_sponsor_test_group NUMBER;
 v_code_type VARCHAR2(15);
 v_originating_sitecode varchar2(10);
 v_record_type char(1);

 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;

   v_sql := 'SELECT PK_PATLABS ,FK_PER,FK_TESTID_SHORTNAME, TEST_DATE,  TEST_RESULT,  TEST_RESULT_TX,
       TEST_TYPE, TEST_UNIT, TEST_RANGE, TEST_LLN,  TEST_ULN,  FK_SITE, FK_CODELST_TSTSTAT_SUBTYP,
      ACCESSION_NUM, FK_TESTGROUP,   FK_CODELST_ABFLAG_SUBTYP,  FK_PATPROT, FK_EVENT_ID,
      NOTES,  ER_NAME,  ER_GRADE,
      FK_CODELST_STDPHASE_SUBTYP,   FK_STUDY, SITE_CODE,
      FK_SPONSOR_ACCOUNT,originating_sitecode,record_type FROM IMPPAT_LABS0';

  --get data

   OPEN v_cur FOR v_sql;
    LOOP


     FETCH v_cur INTO v_orig_patlabs,v_orig_per, v_testid_shortname, v_test_date,v_test_result,v_test_result_tx,
    v_test_type, v_test_unit,v_test_range,v_test_lln,v_test_uln,v_orig_site,v_codelst_tstat_subtyp,
    v_accession_num,v_test_group,v_codelst_abflag_subtyp,v_orig_patprot,v_event_id,
    v_notes, v_er_name,v_er_grade,
    v_codelst_stdphase_subtyp, v_orig_study, v_site_code,v_fk_account_sponsor,v_originating_sitecode,v_record_type;

     EXIT WHEN v_cur %NOTFOUND;

     -- get the site code , creator for the sponsor account

           SELECT site_code , AC_USRCREATOR
        INTO v_sponsor_code, v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;




     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patlabs, 'er_patlabs',v_site_code, v_new_patlabs,v_err ,v_fk_account_sponsor);
       Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site, 'er_site',v_site_code, v_new_site,v_err ,v_fk_account_sponsor);

        Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patprot, 'er_patprot',v_site_code, v_new_patprot,v_err ,v_fk_account_sponsor);

        SP_GET_SITEPK(v_orig_study, 'er_study',v_sponsor_code, v_new_study,v_err,v_originating_sitecode);

     ---------
     -- get new IDs
     IF v_new_study <= 0 THEN  -- original sponsor study not found
          Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Could not create patient labs record, Sponsor Study for site (' || v_site_code || ') does not exist. Original Study:' || v_orig_study, v_success );
        ELSIF v_new_per <= 0 THEN
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Could not create patient labs record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
     ELSE -- all required FKs exist

 IF (nvl(v_record_type,'O') <> 'D') THEN -- check only if the records is not to be deleted

    IF v_orig_site IS NOT NULL AND v_new_site <= 0 THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Sponsor Site record, for site (' || v_site_code || ') not found. Original FK_SITE:' || v_orig_site, v_success );
     v_new_site := NULL;
       END IF;

    IF v_orig_patprot IS NOT NULL AND v_new_patprot <= 0 THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Sponsor Patient Enrollment record, for site (' || v_site_code || ') not found. Original FK_PATPROT:' || v_orig_patprot, v_success );
     v_new_patprot := NULL;
       END IF;

         -- get patient labs study phase

    v_code_type :=  'stdphase';

     SELECT Pkg_Impex.getCodePk (v_codelst_stdphase_subtyp, v_code_type)
     INTO v_codelst_stdphase_fk_code
     FROM dual;

     v_code_type := 'abflag';

       SELECT Pkg_Impex.getCodePk (v_codelst_abflag_subtyp, v_code_type)
     INTO v_codelst_abflag_fk_code
     FROM dual;

     v_code_type := 'tststat';

      SELECT Pkg_Impex.getCodePk (v_codelst_tstat_subtyp, v_code_type)
     INTO v_codelst_tstat_fk_code
     FROM dual;

      IF v_codelst_stdphase_fk_code = -1 THEN
       v_codelst_stdphase_fk_code := NULL;
     END IF;

     IF v_codelst_abflag_fk_code = -1 THEN
       v_codelst_abflag_fk_code := NULL;
     END IF;

     IF v_codelst_tstat_fk_code = -1 THEN
       v_codelst_tstat_fk_code := NULL;
     END IF;


     -- get labtest


     IF v_testid_shortname IS NOT NULL AND LENGTH(trim(v_testid_shortname)) > 0 THEN
      BEGIN


        SELECT pk_labtest
        INTO v_test_id
        FROM ER_LABTEST
        WHERE LOWER(trim(labtest_shortname)) = LOWER(trim(v_testid_shortname)) ;

       EXCEPTION WHEN NO_DATA_FOUND THEN
          v_test_id := NULL;
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Labtest not found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );
      END ;

       BEGIN
        SELECT MIN(fk_labgroup)
        INTO v_sponsor_test_group
     FROM ER_LABTESTGRP
     WHERE  fk_testid = v_test_id;
      EXCEPTION WHEN NO_DATA_FOUND THEN
          v_sponsor_test_group  := NULL;
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS No Labtest Group found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );
      END;

     ELSE
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Labtest not found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );

         v_test_id := NULL;
          v_sponsor_test_group  := NULL;
     END IF;
  end if ; --check for record type <> D

 IF (nvl(v_record_type,'O') = 'D') THEN -- check if the record is to be deleted
     delete from er_patlabs where pk_patlabs = v_new_patlabs;

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS - Deleted patient labs record v_new_patlabs'  || v_new_patlabs, v_success );


  INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
     REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
    SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patlabs',
    v_orig_patlabs ,SYSDATE, 'pat_labs', 'D');

 ELSE --not a deleted recordd
   IF  v_new_patlabs = 0 THEN -- new patient labs record

    SELECT seq_er_patlabs.NEXTVAL INTO v_new_patlabs FROM dual;

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS - New Patient Labs record for site '|| v_site_code || ' with original PK:' || v_orig_patlabs , v_success );

     Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patlabs, 'er_patlabs',v_site_code,  v_new_patlabs,v_err ,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

       INSERT INTO ER_PATLABS (  PK_PATLABS, FK_PER,   FK_TESTID,  TEST_DATE,  TEST_RESULT,
        TEST_RESULT_TX,  TEST_TYPE,  TEST_UNIT, TEST_RANGE,   TEST_LLN,   TEST_ULN,
         FK_SITE,  FK_CODELST_TSTSTAT,  ACCESSION_NUM,   FK_TESTGROUP,   FK_CODELST_ABFLAG,
         FK_EVENT_ID,  NOTES,  CREATOR,  CREATED_ON, ER_NAME,  ER_GRADE,
          FK_CODELST_STDPHASE,  FK_STUDY,FK_PATPROT )
       VALUES(v_new_patlabs, v_new_per,v_test_id,v_test_date,v_test_result,
       v_test_result_tx,v_test_type, v_test_unit,v_test_range,v_test_lln,v_test_uln,
       v_new_site,v_codelst_tstat_fk_code,v_accession_num,v_sponsor_test_group,v_codelst_abflag_fk_code,
       v_event_id, v_notes, v_creator, SYSDATE,v_er_name,v_er_grade,
       v_codelst_stdphase_fk_code, v_new_study,v_new_patprot
       );

       INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patlabs',
        v_orig_patlabs ,SYSDATE, 'pat_labs', 'N');

       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS:Could not create New Patient Forms record for site '|| v_site_code || ' with original PK:' || v_orig_patlabs || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

   ELSIF v_new_patlabs > 0 THEN -- old patient labs record

   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS - Old Patient Labs record for site ' || v_site_code || ' with original PK:' || v_orig_patlabs , v_success );


       UPDATE ER_PATLABS
     SET  FK_TESTID = v_test_id,  TEST_DATE = v_test_date,  TEST_RESULT = v_test_result,
       TEST_RESULT_TX = v_test_result_tx,  TEST_TYPE = v_test_type,  TEST_UNIT = v_test_unit,
       TEST_RANGE = v_test_range,   TEST_LLN = v_test_lln,   TEST_ULN = v_test_uln,
       FK_SITE = v_new_site,  FK_CODELST_TSTSTAT = v_codelst_tstat_fk_code,
       ACCESSION_NUM = v_accession_num,   FK_TESTGROUP = v_sponsor_test_group ,   FK_CODELST_ABFLAG = v_codelst_abflag_fk_code,
         FK_EVENT_ID = v_event_id ,  NOTES = v_notes,  ER_NAME = v_er_name,  ER_GRADE = v_er_grade,
        FK_CODELST_STDPHASE = v_codelst_stdphase_fk_code,  FK_STUDY =  v_new_study, LAST_MODIFIED_DATE = SYSDATE,
        FK_PATPROT = v_new_patprot
     WHERE pk_patlabs = v_new_patlabs;

     INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,v_new_study,v_site_code, v_fk_account_sponsor,'er_patlabs',
        v_orig_patlabs ,SYSDATE, 'pat_labs', 'M');


      ELSIF v_new_patlabs = -1 THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS:' || v_err, v_success );
     END IF; -- for checking v_new_patlabs

     END IF; -- to check if its not a deleted record

  END IF; --for checking the FKs

   ---------

   END LOOP;

  CLOSE v_cur ;

  COMMIT;

 END;

 -- procedure for 'patient all labs''.. when labs are not linked to a study....

 PROCEDURE SP_IMP_PATLABS_NOSTUDY_COMMON (P_TABLENAME VARCHAR2, P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  06/23/04
    Purpose : Import patient labs data from temp tables... when labs are not linked to a study
  */

 v_sql LONG;

 v_cur Gk_Cv_Types.GenericCursorType;

 v_orig_per NUMBER;
 v_site_code VARCHAR2(20);
 v_sponsor_code VARCHAR2(20);
 v_fk_account_sponsor NUMBER;
 v_new_per NUMBER;
 v_err VARCHAR2(4000);

 V_EXPID NUMBER;
 v_success NUMBER;


 v_creator NUMBER;


 v_orig_patlabs NUMBER;
 v_new_patlabs NUMBER;

 v_testid_shortname VARCHAR2(100);
 v_test_date DATE;
 v_test_result VARCHAR2(100);
 v_test_result_tx CLOB;
 v_test_type CHAR(2);
 v_test_unit VARCHAR2(20);
 v_test_range  VARCHAR2(50);
 v_test_lln VARCHAR2(20);
 v_test_uln VARCHAR2(20);
 v_orig_site NUMBER;
 v_new_site NUMBER;
 v_codelst_tstat_subtyp VARCHAR2(20);
    v_accession_num VARCHAR2(20);
 v_test_group NUMBER;
 v_codelst_abflag_subtyp VARCHAR2(20);

 v_event_id NUMBER;
 v_notes CLOB;
 v_er_name VARCHAR2(500);
 v_er_grade NUMBER;
    v_codelst_stdphase_subtyp VARCHAR2(20);

 v_codelst_stdphase_fk_code NUMBER;
 v_codelst_abflag_fk_code NUMBER;
 v_codelst_tstat_fk_code NUMBER;
 v_test_id NUMBER;

 v_sponsor_test_group NUMBER;
   v_code_type VARCHAR2(15);

 BEGIN

   SELECT EXP_ID  INTO v_expid FROM ER_IMPREQLOG
    WHERE  pk_impreqlog = P_IMPID ;

   v_sql := 'SELECT PK_PATLABS ,FK_PER,FK_TESTID_SHORTNAME, TEST_DATE,  TEST_RESULT,  TEST_RESULT_TX,
       TEST_TYPE, TEST_UNIT, TEST_RANGE, TEST_LLN,  TEST_ULN,  FK_SITE, FK_CODELST_TSTSTAT_SUBTYP,
      ACCESSION_NUM, FK_TESTGROUP,   FK_CODELST_ABFLAG_SUBTYP, FK_EVENT_ID,
      NOTES,  ER_NAME,  ER_GRADE,
      FK_CODELST_STDPHASE_SUBTYP, SITE_CODE,
      FK_SPONSOR_ACCOUNT FROM  ' || P_TABLENAME;

  --get data
 BEGIN
   OPEN v_cur FOR v_sql ;
    LOOP

     FETCH v_cur INTO v_orig_patlabs,v_orig_per, v_testid_shortname, v_test_date,v_test_result,v_test_result_tx,
    v_test_type, v_test_unit,v_test_range,v_test_lln,v_test_uln,v_orig_site,v_codelst_tstat_subtyp,
    v_accession_num,v_test_group,v_codelst_abflag_subtyp,v_event_id,
    v_notes, v_er_name,v_er_grade,
    v_codelst_stdphase_subtyp, v_site_code,v_fk_account_sponsor;

     EXIT WHEN v_cur %NOTFOUND;

     -- get the site code , creator for the sponsor account

           SELECT site_code , AC_USRCREATOR
        INTO v_sponsor_code, v_creator
        FROM ER_ACCOUNT
        WHERE pk_account  = v_fk_account_sponsor;



     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_per, 'er_per',v_site_code, v_new_per,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_patlabs, 'er_patlabs',v_site_code, v_new_patlabs,v_err ,v_fk_account_sponsor);
     Pkg_Impexcommon.SP_GET_SPONSORPK(v_orig_site, 'er_site',v_site_code, v_new_site,v_err ,v_fk_account_sponsor);


    -- get new IDs
       IF v_new_per <= 0 THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY Could not create patient labs record, Patient for site (' || v_site_code || ') does not exist. Original PK_PER:' || v_orig_per, v_success );
     ELSE -- all required FKs exist

    IF v_orig_site IS NOT NULL AND v_new_site <= 0 THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY Sponsor Site record, for site (' || v_site_code || ') not found. Original FK_SITE:' || v_orig_site, v_success );
     v_new_site := NULL;
       END IF;

         -- get patient labs study phase

    v_code_type :=  'stdphase';

     SELECT Pkg_Impex.getCodePk (v_codelst_stdphase_subtyp, v_code_type)
     INTO v_codelst_stdphase_fk_code
     FROM dual;

     v_code_type := 'abflag';
       SELECT Pkg_Impex.getCodePk (v_codelst_abflag_subtyp, v_code_type)
     INTO v_codelst_abflag_fk_code
     FROM dual;

     v_code_type := 'tststat';

     SELECT Pkg_Impex.getCodePk (v_codelst_tstat_subtyp, v_code_type)
     INTO v_codelst_tstat_fk_code
     FROM dual;

      IF v_codelst_stdphase_fk_code = -1 THEN
       v_codelst_stdphase_fk_code := NULL;
     END IF;

     IF v_codelst_abflag_fk_code = -1 THEN
       v_codelst_abflag_fk_code := NULL;
     END IF;

     IF v_codelst_tstat_fk_code = -1 THEN
       v_codelst_tstat_fk_code := NULL;
     END IF;

     -- get labtest


     IF v_testid_shortname IS NOT NULL AND LENGTH(trim(v_testid_shortname)) > 0 THEN
      BEGIN


        SELECT pk_labtest
        INTO v_test_id
        FROM ER_LABTEST
        WHERE LOWER(trim(labtest_shortname)) = LOWER(trim(v_testid_shortname)) ;

       EXCEPTION WHEN NO_DATA_FOUND THEN
          v_test_id := NULL;
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY Labtest not found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );
      END ;

       BEGIN
        SELECT MIN(fk_labgroup)
        INTO v_sponsor_test_group
     FROM ER_LABTESTGRP
     WHERE  fk_testid = v_test_id;



      EXCEPTION WHEN NO_DATA_FOUND THEN
          v_sponsor_test_group  := NULL;
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY No Labtest Group found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );
      END;

     ELSE
        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'WARNING','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY Labtest not found for site (' || v_site_code || ') not found. testid_shortname:' || v_testid_shortname, v_success );
        v_test_id := null;
        v_sponsor_test_group := null;
     END IF;


   IF  v_new_patlabs = 0 THEN -- new patient labs record

    SELECT seq_er_patlabs.NEXTVAL INTO v_new_patlabs FROM dual;

     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY - New Patient Labs record for site '|| v_site_code || ' with original PK:' || v_orig_patlabs , v_success );

     Pkg_Impexcommon.SP_SET_SPONSORPK(v_orig_patlabs, 'er_patlabs',v_site_code,  v_new_patlabs,v_err ,v_fk_account_sponsor);

      IF LENGTH(trim(v_err)) > 0 THEN

        Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY Could not create record for er_sponsorpkmap :' || v_err, v_success );

      ELSE
       BEGIN

       INSERT INTO ER_PATLABS (  PK_PATLABS, FK_PER,   FK_TESTID,  TEST_DATE,  TEST_RESULT,
        TEST_RESULT_TX,  TEST_TYPE,  TEST_UNIT, TEST_RANGE,   TEST_LLN,   TEST_ULN,
         FK_SITE,  FK_CODELST_TSTSTAT,  ACCESSION_NUM,   FK_TESTGROUP,   FK_CODELST_ABFLAG,
         FK_EVENT_ID,  NOTES,  CREATOR,  CREATED_ON, ER_NAME,  ER_GRADE,
          FK_CODELST_STDPHASE )
       VALUES(v_new_patlabs, v_new_per,v_test_id,v_test_date,v_test_result,
       v_test_result_tx,v_test_type, v_test_unit,v_test_range,v_test_lln,v_test_uln,
       v_new_site,v_codelst_tstat_fk_code,v_accession_num,v_sponsor_test_group,v_codelst_abflag_fk_code,
       v_event_id, v_notes, v_creator, SYSDATE,v_er_name,v_er_grade,
       v_codelst_stdphase_fk_code
       );


        INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
         REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_patlabs',
        v_orig_patlabs ,SYSDATE, 'pat_nostudy', 'N');


       EXCEPTION WHEN OTHERS THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY:Could not create New Patient Forms record for site '|| v_site_code || ' with original PK:' || v_orig_patlabs || ' Error: ' || SQLERRM, v_success );
       END;
        END IF;

   ELSIF v_new_patlabs > 0 THEN -- old patient labs record

   Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY - Old Patient Labs record for site ' || v_site_code || ' with original PK:' || v_orig_patlabs , v_success );


       UPDATE ER_PATLABS
     SET  FK_TESTID = v_test_id,  TEST_DATE = v_test_date,  TEST_RESULT = v_test_result,
       TEST_RESULT_TX = v_test_result_tx,  TEST_TYPE = v_test_type,  TEST_UNIT = v_test_unit,
       TEST_RANGE = v_test_range,   TEST_LLN = v_test_lln,   TEST_ULN = v_test_uln,
       FK_SITE = v_new_site,  FK_CODELST_TSTSTAT = v_codelst_tstat_fk_code,
       ACCESSION_NUM = v_accession_num,   FK_TESTGROUP = v_sponsor_test_group ,   FK_CODELST_ABFLAG = v_codelst_abflag_fk_code,
         FK_EVENT_ID = v_event_id ,  NOTES = v_notes,  ER_NAME = v_er_name,  ER_GRADE = v_er_grade,
        FK_CODELST_STDPHASE = v_codelst_stdphase_fk_code,   LAST_MODIFIED_DATE = SYSDATE
     WHERE pk_patlabs = v_new_patlabs;

    INSERT INTO ER_REV_RECVD ( PK_REC, EXP_ID, IMP_ID, FK_STUDY, SITE_CODE, FK_ACCOUNT, REC_TABLENAME,
      REC_SITETABLEPK, REC_RECVD_ON, REC_MODULE, REC_RECORD_TYPE ) VALUES (
        SEQ_ER_REV_RECVD.NEXTVAL,V_EXPID, P_IMPID,NULL,v_site_code, v_fk_account_sponsor,'er_patlabs',
        v_orig_patlabs ,SYSDATE, 'pat_nostudy', 'M');

      ELSIF v_new_patlabs = -1 THEN
         Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY:' || v_err, v_success );
   END IF; -- for checking v_new_patprot

       END IF; --for checking the FKs

   ---------

   END LOOP;

  CLOSE v_cur ;

  COMMIT;
  EXCEPTION WHEN OTHERS THEN
     Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX_REVERSE.SP_IMP_PATLABS_NOSTUDY:' || SQLERRM, v_success );

  END; --
 END;
   ----------------------

    PROCEDURE SP_EXP_PATNOSTUDY (
    P_EXPID          NUMBER,
      O_RESULT_PATLABS     OUT  Gk_Cv_Types.GenericCursorType
    )
 /* Author: Sonia Sahni
   Date: 06/24/04
   Purpose - Returns a ref cursor of patient labs data
   */
   AS
    v_rec_patlabs rec_er_patlabs   := rec_er_patlabs(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
           NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
         NULL,NULL,NULL,NULL,NULL,NULL,NULL); --record type
    v_table_patlabs tab_er_patlabs := tab_er_patlabs(); --index-by table type

   v_cnt NUMBER := 0;

   v_err VARCHAR2(4000);

   v_sql LONG;

   v_expdetails CLOB;
   v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();

   v_sitecode VARCHAR2(20);
   v_revstudysponsorpk VARCHAR2(4000);
   V_SPONSOR_ACCOUNT NUMBER;
   v_stat NUMBER;
   v_last_exported_date DATE;

   v_module_subtype VARCHAR2(20);

   BEGIN

  --get export request

  Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);


  --find out the site_code from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'siteCode',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);

   --find out the studysponsorpk from the export request

    Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'expStudySponsor',v_modkeys);
  Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_revstudysponsorpk);



  SELECT FK_SPONSOR_ACCOUNT
  INTO V_SPONSOR_ACCOUNT
  FROM  ER_REV_STUDYSPONSOR
  WHERE pk_rev_studysponsor = v_revstudysponsorpk;

  -- get last date/time when the data was exported

  v_module_subtype:= 'pat_nostudy';

  SELECT  RS_LAST_EXPORTED_ON
  INTO v_last_exported_date
  FROM ER_REVMODEXP_SETUP, ER_MODULES
  WHERE FK_REV_STUDYSPONSOR  =  v_revstudysponsorpk AND
  FK_MODULE  = pk_module AND module_subtype = v_module_subtype;
  --

  -- get data --
 FOR i IN ( SELECT * FROM (SELECT  PK_PATLABS , ER_PATPROT.FK_PER , (SELECT labtest_shortname FROM ER_LABTEST WHERE pk_labtest = FK_TESTID ) FK_TESTID_SHORTNAME,
   TEST_DATE, TEST_RESULT,  TEST_RESULT_TX,  TEST_TYPE,TEST_UNIT, TEST_RANGE, TEST_LLN,  TEST_ULN, FK_SITE,
      (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_TSTSTAT) FK_CODELST_TSTSTAT_SUBTYP ,
    ACCESSION_NUM  , FK_TESTGROUP,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_ABFLAG) FK_CODELST_ABFLAG_SUBTYP,
    FK_PATPROT,  FK_EVENT_ID,  CUSTOM001,CUSTOM002,  CUSTOM003, CUSTOM004,  CUSTOM005, CUSTOM006,
    CUSTOM007, CUSTOM008,  CUSTOM009,  CUSTOM010, NOTES,ER_NAME ,ER_GRADE,e.created_on,
    e.last_modified_date,
    (SELECT  codelst_subtyp FROM  ER_CODELST WHERE pk_codelst =  FK_CODELST_STDPHASE)  FK_CODELST_STDPHASE_SUBTYP,
     NVL((SELECT MAX(PATSTUDYSTAT_DATE) FROM ER_PATSTUDYSTAT ps
      WHERE ps.fk_study = ER_PATPROT.fk_study AND
     ps.fk_per = ER_PATPROT.fk_per AND
     FK_CODELST_STAT = ( SELECT pk_codelst  FROM ER_CODELST
             WHERE trim(codelst_type) = 'patStatus' AND
          trim(codelst_subtyp) = trim(revinfo.UPPER_LIMIT_STATUS) ) ),SYSDATE) PATSTUDYSTAT_UPPER_DATE,
       UPPER_LIMIT_DAYS
     FROM
    (SELECT NVL(TO_NUMBER(sys.XMLTYPE.EXTRACT(EXPORT_SETTINGS,'/modules/labs/upperLimitNumberOfDays/text()')),0) UPPER_LIMIT_DAYS,
          sys.XMLTYPE.EXTRACT(EXPORT_SETTINGS,'/modules/labs/upperLimitEnrollStatus/text()').getStringVal() UPPER_LIMIT_STATUS,
         FK_SITESTUDY
         FROM ER_REV_STUDYSPONSOR
         WHERE  SITE_CODE = v_sitecode AND
         NVL(sys.XMLTYPE.EXTRACT(EXPORT_SETTINGS,'/modules/labs/exportAll/text()').getNumberVal(),0) = 1 -- get all studies for the site
    ) revinfo , ER_PATPROT, ER_PATLABS e
    WHERE ER_PATPROT.FK_STUDY = revinfo.FK_SITESTUDY AND
    ER_PATPROT.patprot_stat = 1 AND
    e.fk_per =  ER_PATPROT.fk_per AND  e.fk_study IS NULL AND
     ( e.created_on >= v_last_exported_date OR
    e.last_modified_date >= v_last_exported_date OR
    v_last_exported_date IS NULL) ) cond1
   WHERE cond1.created_on <= cond1.PATSTUDYSTAT_UPPER_DATE + UPPER_LIMIT_DAYS  OR
     cond1.last_modified_date <= cond1.PATSTUDYSTAT_UPPER_DATE + UPPER_LIMIT_DAYS
    )
     LOOP
        v_cnt := v_cnt + 1;

          v_rec_patlabs.PK_PATLABS := i.PK_PATLABS;
      v_rec_patlabs.FK_PER := i.FK_PER;
      v_rec_patlabs.FK_TESTID_SHORTNAME := i. FK_TESTID_SHORTNAME;
      v_rec_patlabs.TEST_DATE := i.TEST_DATE;
      v_rec_patlabs.TEST_RESULT := i.TEST_RESULT;
      v_rec_patlabs.TEST_RESULT_TX := i.TEST_RESULT_TX;
      v_rec_patlabs.TEST_TYPE := i.TEST_TYPE;
      v_rec_patlabs.TEST_UNIT := i.TEST_UNIT;
      v_rec_patlabs.TEST_RANGE := i.TEST_RANGE;
      v_rec_patlabs.TEST_LLN := i.TEST_LLN;
      v_rec_patlabs.TEST_ULN := i.TEST_ULN;
      v_rec_patlabs.FK_SITE := i.FK_SITE;

      v_rec_patlabs.FK_CODELST_TSTSTAT_SUBTYP := i.FK_CODELST_TSTSTAT_SUBTYP;
      v_rec_patlabs.ACCESSION_NUM := i.ACCESSION_NUM;
      v_rec_patlabs.FK_TESTGROUP := i.FK_TESTGROUP;

      v_rec_patlabs.FK_CODELST_ABFLAG_SUBTYP := i.FK_CODELST_ABFLAG_SUBTYP;
      v_rec_patlabs.FK_PATPROT := i.FK_PATPROT;
      v_rec_patlabs.FK_EVENT_ID := i.FK_EVENT_ID;
      v_rec_patlabs.CUSTOM001 := i.CUSTOM001;
      v_rec_patlabs.CUSTOM002 := i.CUSTOM002;
      v_rec_patlabs.CUSTOM003 := i.CUSTOM003;
      v_rec_patlabs.CUSTOM004 := i.CUSTOM004;
      v_rec_patlabs.CUSTOM005 := i.CUSTOM005;
      v_rec_patlabs.CUSTOM006 := i.CUSTOM006;
      v_rec_patlabs.CUSTOM007 := i.CUSTOM007;
       v_rec_patlabs.CUSTOM008 := i.CUSTOM008;
      v_rec_patlabs.CUSTOM009 := i.CUSTOM009;
      v_rec_patlabs.CUSTOM010 := i.CUSTOM010;

      v_rec_patlabs.NOTES := i.NOTES;
      v_rec_patlabs.ER_NAME  := i.ER_NAME ;
      v_rec_patlabs.ER_GRADE := i.ER_GRADE;

      v_rec_patlabs.FK_CODELST_STDPHASE_SUBTYP := i.FK_CODELST_STDPHASE_SUBTYP;

      v_rec_patlabs.SITE_CODE := v_sitecode;
      v_rec_patlabs.FK_SPONSOR_ACCOUNT := V_SPONSOR_ACCOUNT;

     --store each record in a table
           v_table_patlabs.EXTEND;
     v_table_patlabs(v_cnt) := v_rec_patlabs;
     v_cnt := v_table_patlabs.COUNT();

 END LOOP;
 -- populate out cursor
  OPEN  O_RESULT_PATLABS
     FOR
         SELECT *
         FROM TABLE( CAST(v_table_patlabs AS tab_er_patlabs));


    END SP_EXP_PATNOSTUDY;

  PROCEDURE SP_IMP_PATNOSTUDY (P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
 AS
 /* Author : Sonia Sahni
    Date:  06/25/04
    Purpose : Import patient data that is not linked to a study
  */
 BEGIN
    -- import labs (labs that were entered before this enrollment)
     SP_IMP_PATLABS_NOSTUDY_COMMON ('imppat_nostudy0', P_IMPID, O_SUCCESS);

 END;


   --modified by sonia, 07/05/06, to make the import according to sposnor form mapping
   -- p_sponsor_form- form id of the sponsor where patform is being imported
   -- p_site_form -- formid of the site that has sent the data
     PROCEDURE getFormColumnString(colcount NUMBER, p_sponsor_form NUMBER , p_site_form NUMBER,o_column_string OUT VARCHAR2, o_coldata_string OUT VARCHAR2, p_mode VARCHAR2)
   AS
   v_cols VARCHAR2(32767);
   v_col_at_sponsor VARCHAR2(100);
   v_systemid VARCHAR2(100) ;
   v_col_at_site  VARCHAR2(100);
    v_cur Gk_Cv_Types.GenericCursorType;
    v_sql_mapsite VARCHAR2(500 );
    v_cols_data VARCHAR2(32767);

    BEGIN



      v_sql_mapsite := 'SELECT mp_systemid , mp_mapcolname from IMPPAT_FILLEDFORMS2 where fk_form = :form'  ;

          --get data
         OPEN v_cur FOR v_sql_mapsite USING  p_site_form ;

            LOOP

             FETCH v_cur INTO v_systemid, v_col_at_site;

             EXIT WHEN v_cur %NOTFOUND;

               -- check how the field is stores in the sponsor db
               BEGIN
               SELECT mp_mapcolname INTO v_col_at_sponsor
               FROM ER_MAPFORM WHERE fk_form = p_sponsor_form AND mp_systemid = v_systemid;

               IF p_mode = 'N' THEN
                       v_cols := v_cols || ', ' || v_col_at_sponsor ;
                     ELSE -- update
                        v_cols := v_cols || ' , ' || v_col_at_sponsor || ' = :' || v_col_at_site ;

                END IF;

                 v_cols_data := v_cols_data || ',' ||  v_col_at_site;

                EXCEPTION WHEN NO_DATA_FOUND THEN
                   v_cols := v_cols ;
                   v_cols_data := v_cols_data ;
               END;

            END LOOP;
            o_column_string :=  v_cols ;
            o_coldata_string := v_cols_data ;

         CLOSE v_cur ;





   END;

   /* FUNCTION getFormColumnUpdateString(colcount NUMBER ) RETURN VARCHAR2
   IS
   v_cols VARCHAR2(32767);

    BEGIN
         FOR  i IN 1..colcount
    LOOP
        v_cols := v_cols || ' , col'||i || ' = :col'||i ;
    END LOOP;
        RETURN v_cols;
     END; */

  /* By Sonia Abrol; 04/10/06, to prepare notification when data received at collecting site/sponsor is processed*/
   PROCEDURE sp_notifySponsor(p_imp IN NUMBER, o_ret OUT VARCHAR2)
     AS
  v_pat_count NUMBER := 0;
 v_patenr_count NUMBER := 0;
 v_patstat_count NUMBER := 0;
 v_patform_count NUMBER := 0;
 v_patadv_count NUMBER := 0;
 v_patfacility_count NUMBER := 0;
 v_patlabs_count NUMBER := 0;
 v_users VARCHAR2(250);
 v_message_template CLOB;
 v_message_text CLOB;

 V_POS  NUMBER :=0 ;
   v_params VARCHAR2(1000);
    V_USER_STR VARCHAR2(500);
    v_cnt NUMBER ;

-- v_studynumber VARCHAR2(30);
  v_studynumber VARCHAR2(100);--JM:
  v_fparam VARCHAR2(1000);
  v_expid NUMBER;
  v_site_code VARCHAR2(20);

   v_total_count NUMBER;

   BEGIN
   BEGIN
        -- get user
     BEGIN
         SELECT NVL(ctrl_value,'0')
        INTO V_USERS FROM ER_CTRLTAB WHERE ctrl_key = 'revA2A_usrs';

        SELECT site_code, exp_id
        INTO v_site_code ,v_expid
        FROM ER_IMPREQLOG e
        WHERE pk_impreqlog = p_imp;

        SELECT COUNT(*) INTO v_total_count FROM ER_REV_RECVD  WHERE imp_id = p_imp;

    EXCEPTION WHEN NO_DATA_FOUND THEN
         V_USERS := '0';
    END;

     IF (V_USERS <>  '0' AND LENGTH(trim(V_USERS)) > 0 AND v_total_count > 0) THEN
       FOR k IN (SELECT  rec_tablename,rec_module, COUNT(*) ct
      FROM (  SELECT DISTINCT rec_tablename,rec_module,rec_sitetablepk
           FROM ER_REV_RECVD  WHERE imp_id = p_imp) o
          GROUP BY rec_tablename,rec_module )
      LOOP
             IF k.rec_tablename = 'er_per' THEN
                   v_pat_count :=  k.ct;
         ELSIF k.rec_tablename = 'er_patprot' THEN
                 v_patenr_count  :=  k.ct;
         ELSIF k.rec_tablename = 'er_patstudystat' THEN
                 v_patstat_count  :=  k.ct;
         ELSIF k.rec_tablename = 'er_patforms' THEN
                  v_patform_count  := k.ct;
         ELSIF k.rec_tablename = 'sch_adverseve' THEN
                 v_patadv_count   :=  k.ct;
         ELSIF k.rec_tablename = 'er_patfacility' THEN
                 v_patfacility_count  :=  k.ct;
         ELSIF k.rec_tablename = 'er_patlabs' THEN
                v_patlabs_count :=  k.ct;
         END IF;
      END LOOP;

       v_message_template := PKG_COMMON.SCH_GETMAILMSG('a2a_recvd');

        v_fparam :=  Pkg_Util.date_to_char(SYSDATE)  ||'~'|| v_expid  ||'~'|| v_site_code ||'~'|| v_pat_count  ||'~'||   v_patenr_count
                 ||'~'||   v_patstat_count   || '~' ||   v_patform_count   || '~' ||  v_patadv_count
               || '~' ||  v_patlabs_count   || '~' ||      v_patfacility_count   ;

             v_message_text:=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);

         V_USERS :=  V_USERS|| ',' ;
          v_params := V_USERS ;

           LOOP
         V_CNT := V_CNT + 1;
         V_POS := INSTR (V_USERS, ',');
         V_PARAMS := SUBSTR (V_USERS, 1, V_POS - 1);
         EXIT WHEN V_PARAMS IS NULL;

         INSERT INTO sch_dispatchmsg (
       PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE ,  MSG_TEXT ,FK_PAT          )
            VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'U',  v_message_text,v_params ) ;

         V_USERS := SUBSTR (V_USERS, V_POS + 1);
              --   p('DONE');

     END LOOP ; /* end loop for multiple users*/
     END IF; --user check
       COMMIT;
            o_ret := '';
  EXCEPTION WHEN OTHERS THEN
       o_ret := 'Exception in PKG_IMPEX_REVERSE.sp_notifySponsor : ' || SQLERRM;
  END ;

   END; --end of proc

   /* Function to return Reverse A2A status, returns 0 is Reverse A2A is not running, returns 1 if Reverse A2A is running*/
  FUNCTION f_get_ReverseA2AStatus RETURN NUMBER
  IS
  v_total_count NUMBER;
    v_count NUMBER;
    v_return NUMBER;
    reva2a VARCHAR2(10);

  BEGIN
    BEGIN
        select nvl(ctrl_value,'Y') into reva2a from er_ctrltab where ctrl_key='check_reva2a';
        EXCEPTION WHEN NO_DATA_FOUND THEN reva2a := 'Y';
    END;

 IF reva2a <> 'N' THEN

         SELECT COUNT(*)
       INTO v_total_count
       FROM ER_REVMODEXP_SETUP;

       IF v_total_count > 0 THEN
                    SELECT COUNT(*)
                    INTO     v_count
                       FROM ER_REVMODEXP_SETUP WHERE (rs_next_export_due + 1/24) < SYSDATE; --check for how many rows the current date/time is more 2 hours more than rs_next_export_due
                    -- remove
                    --v_count := 1;

                    IF v_count > 0 THEN
                               v_return :=  0;
                    ELSE
                            v_return :=  1;
                    END IF;

    ELSE
            v_return :=  1;
    END IF;

    ELSE
    v_return :=  1;
    END IF;

          RETURN v_return;
  END;

  /* By Sonia Abrol; 05/26/06, to prepare notification when reverse A2A is restarted automatically*/
  PROCEDURE sp_notifyA2AStatus (o_ret OUT VARCHAR2)
     AS

 v_users VARCHAR2(250);
 v_message_template CLOB;
 v_message_text CLOB;

 V_POS  NUMBER :=0 ;
 v_params VARCHAR2(1000);
  V_USER_STR VARCHAR2(500);
    v_cnt NUMBER ;

  v_fparam VARCHAR2(1000);

   BEGIN
   BEGIN
        -- get user
     BEGIN

         SELECT NVL(ctrl_value,'0')
        INTO V_USERS FROM ER_CTRLTAB WHERE ctrl_key = 'revA2A_usrs';


    EXCEPTION WHEN NO_DATA_FOUND THEN
         V_USERS := '0';
    END;

     IF (V_USERS <>  '0' AND LENGTH(trim(V_USERS)) > 0) THEN

       v_message_template := PKG_COMMON.SCH_GETMAILMSG('a2a_status');

        v_fparam :=  TO_CHAR(SYSDATE,PKG_DATEUTIL.f_get_datetimeformat)   ; --JM: 19MAR2009

         v_message_text:=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);

         V_USERS :=  V_USERS|| ',' ;
          v_params := V_USERS ;

           LOOP
         V_CNT := V_CNT + 1;
         V_POS := INSTR (V_USERS, ',');
         V_PARAMS := SUBSTR (V_USERS, 1, V_POS - 1);
         EXIT WHEN V_PARAMS IS NULL;

         INSERT INTO sch_dispatchmsg (
       PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE ,  MSG_TEXT ,FK_PAT      ,MSG_SUBJECT    )
            VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'U',  v_message_text,v_params,'Reverse A2A Status' ) ;

         V_USERS := SUBSTR (V_USERS, V_POS + 1);
              --   p('DONE');

     END LOOP ; /* end loop for multiple users*/
     END IF; --user check
       COMMIT;
            o_ret := '';
  EXCEPTION WHEN OTHERS THEN
       o_ret := 'Exception in PKG_IMPEX_REVERSE.sp_notifyA2AStatus  : ' || SQLERRM;
  END ;

   END; --end of proc


-- Sonia Abrol 09/03/08 - to send connection failure email so that connection issues can be resolved
  PROCEDURE sp_notifyA2AConnFail (p_connadd Varchar2, o_ret OUT VARCHAR2)
     AS

     v_users VARCHAR2(250);
     v_message_template CLOB;
     v_message_text CLOB;

     V_POS  NUMBER :=0 ;
     v_params VARCHAR2(1000);
     V_USER_STR VARCHAR2(500);
     v_cnt NUMBER ;
     v_fparam VARCHAR2(1000);

   BEGIN
   BEGIN
        -- get user
     BEGIN

         SELECT NVL(ctrl_value,'0')
        INTO V_USERS FROM ER_CTRLTAB WHERE ctrl_key = 'revA2A_usrs';


    EXCEPTION WHEN NO_DATA_FOUND THEN
         V_USERS := '0';
    END;

     IF (V_USERS <>  '0' AND LENGTH(trim(V_USERS)) > 0) THEN

       v_message_template := PKG_COMMON.SCH_GETMAILMSG('a2a_conn');

        v_fparam :=  p_connadd || '~' || TO_CHAR(SYSDATE,PKG_DATEUTIL.f_get_datetimeformat)   ;  --JM: 19MAR2009

         v_message_text:=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);

         V_USERS :=  V_USERS|| ',' ;
          v_params := V_USERS ;

           LOOP
         V_CNT := V_CNT + 1;
         V_POS := INSTR (V_USERS, ',');
         V_PARAMS := SUBSTR (V_USERS, 1, V_POS - 1);
         EXIT WHEN V_PARAMS IS NULL;

         INSERT INTO sch_dispatchmsg (
       PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE ,  MSG_TEXT ,FK_PAT      ,MSG_SUBJECT    )
            VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'U',  v_message_text,v_params,'Reverse A2A Connection Failure' ) ;

         V_USERS := SUBSTR (V_USERS, V_POS + 1);
              --   p('DONE');

     END LOOP ; /* end loop for multiple users*/
     END IF; --user check
       COMMIT;
            o_ret := '';
  EXCEPTION WHEN OTHERS THEN
       o_ret := 'Exception in PKG_IMPEX_REVERSE.sp_notifyA2AConnFail : ' || SQLERRM;
  END ;

   END; --end of proc


  END Pkg_Impex_Reverse;
/


CREATE SYNONYM ESCH.PKG_IMPEX_REVERSE FOR PKG_IMPEX_REVERSE;


CREATE SYNONYM EPAT.PKG_IMPEX_REVERSE FOR PKG_IMPEX_REVERSE;


GRANT EXECUTE, DEBUG ON PKG_IMPEX_REVERSE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEX_REVERSE TO ESCH;

