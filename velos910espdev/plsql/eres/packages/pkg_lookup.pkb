CREATE OR REPLACE PACKAGE BODY        "PKG_LOOKUP" AS


PROCEDURE SP_REPEAT_LOOKUP ( p_lkp_field Number, p_lkp_form_field Number , p_section Number, o_ret OUT Number)
IS

    v_orig_lkp_dispval VARCHAR2(4000);
	v_orig_lkp_formfld Number;
	V_FLD_KEYWORD TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
	v_count Number;
	V_KEYWORDSETSTR Varchar2(4000);
	V_KEYWORDSET Varchar2(4000);
	V_POS Number;
	v_mapset Varchar2(200);
	v_pipepos Number;
	v_mapfld_sysid Varchar2(50);
	v_mapfld_pk_field Number;
	V_ARRDISP_FLDID TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
	V_ARRDISP_SYSID TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();

	v_lkp_set Number;
	v_lkp_rep_fld Number;
	v_lkp_rep_lkpdispval varchar2(4000);
    v_new_mapfld_sysid Varchar2(50);
   	v_lkp_old_rep_lkpdispval Varchar2(4000);
	v_pk_repformfld Number;
	v_fld_datatype Varchar2(3);

	v_sec_rep_num Number;


BEGIN
	o_ret := -1;

	-- check if the rection has repeat number > 0

	Select nvl(FORMSEC_REPNO,0)
	into v_sec_rep_num
	from er_formsec
	Where pk_formsec = p_section;

	if v_sec_rep_num <= 0 then
	   o_ret:= 0;
	   return;
	end if;
	 -- get field lookup display val for the field


	select fld_lkpdispval
	into v_orig_lkp_dispval
    from er_fldlib
    Where pk_field =  p_lkp_field and
	record_type <> 'D';

	 v_orig_lkp_formfld := p_lkp_form_field;


	--parse the v_orig_lkp_dispval to find out the fields this lookup is associated to
	 v_count := 0;

	 V_KEYWORDSETSTR := v_orig_lkp_dispval || '~';
	 V_KEYWORDSET := v_orig_lkp_dispval || '~';
	 V_POS := 0;

	 LOOP
	    V_POS := instr (V_KEYWORDSETSTR, '~');
		V_KEYWORDSET := substr (V_KEYWORDSETSTR, 1, V_POS - 1);
		      exit when V_KEYWORDSET Is null;
	 	V_COUNT := V_COUNT + 1;
		V_FLD_KEYWORD.extend;
		V_FLD_KEYWORD(V_COUNT) := V_KEYWORDSET;
 		--p('in trigger loop 2 V_FLD_KEYWORD ' || V_KEYWORDSET );
		V_KEYWORDSETSTR := substr (V_KEYWORDSETSTR, V_POS + 1);


	 END LOOP ;

	 v_count := 0;
	 FOR s IN 1..V_FLD_KEYWORD.COUNT -- put old sys id and pk_field in array
	   LOOP
	  	  v_mapset := trim(V_FLD_KEYWORD(s));
		  V_PIPEPOS := instr (v_mapset, '|');
		  v_mapfld_sysid := substr (v_mapset ,1,V_PIPEPOS - 1); --got system id
		  	  -- get field id for this system id


		  BEGIN
			  Select pk_field
			  into v_mapfld_pk_field
			  from er_fldlib where fld_systemid = trim(v_mapfld_sysid);
			  V_COUNT := V_COUNT + 1;
			  V_ARRDISP_SYSID.extend;
			  V_ARRDISP_SYSID(v_count) :=  v_mapfld_sysid;
			  V_ARRDISP_FLDID.extend;
			  V_ARRDISP_FLDID(v_count) :=  v_mapfld_pk_field;
		  Exception When NO_DATA_FOUND  THEN
		 	   v_mapfld_pk_field := 0;
	      END ;



	  END LOOP;

	  ----get all lookup fields from er_repformfld that are rep of the orig


	  FOR a in (select pk_repformfld,fk_field,fld_lkpdispval,repformfld_set
   	       	   from er_repformfld rep, er_fldlib
			   where rep.fk_formfld = v_orig_lkp_formfld  and
			   pk_field =  rep.fk_field order by repformfld_set )
	  LOOP
		v_lkp_set := a.repformfld_set;
		v_lkp_rep_lkpdispval := a.fld_lkpdispval;
		v_lkp_old_rep_lkpdispval := a.fld_lkpdispval;
		v_lkp_rep_fld := a.fk_field;
		v_pk_repformfld := a.pk_repformfld;


		--change the v_lkp_rep_lkpdispval for sys id of new mapped fields

			 FOR k IN 1..V_ARRDISP_FLDID.COUNT
				LOOP
		  			v_mapfld_sysid    := trim(V_ARRDISP_SYSID(k));
					v_mapfld_pk_field := trim(V_ARRDISP_FLDID(k));



				     BEGIN
					 	  Select fld_systemid
						  into v_new_mapfld_sysid
						  from er_fldlib, er_repformfld rf, er_formfld ff
						  where  ff.fk_field = v_mapfld_pk_field and ff.fk_formsec = p_section and
						  ff.pk_formfld = rf.fk_formfld and rf.repformfld_set = v_lkp_set  and
						  rf.fk_field = pk_field;
					 Exception When NO_DATA_FOUND  THEN
					   	   v_new_mapfld_sysid := '';
					 END ;



				    v_lkp_rep_lkpdispval := replace(v_lkp_rep_lkpdispval,v_mapfld_sysid,v_new_mapfld_sysid);



		   	    END LOOP;
		  -- update the rep lkp field with new lookuyp disp val

		  UPDATE er_fldlib
		  set fld_lkpdispval = v_lkp_rep_lkpdispval
		  Where pk_field = v_lkp_rep_fld;
		-- update the lookup fld xsl with new fld-keyword displayval
	 	  Update er_repformfld
		  set repformfld_xsl = replace(repformfld_xsl, v_lkp_old_rep_lkpdispval, v_lkp_rep_lkpdispval)
		  Where fk_field = v_lkp_rep_fld and pk_repformfld = v_pk_repformfld;
	  END LOOP;

    o_ret := 0;
	COMMIT;
		------------------------------------------------------------------

   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       Null;
      -- Consider logging the error and then re-raise

END SP_REPEAT_LOOKUP ;


Function SP_GET_LKP_SYSIDS ( p_lkpstr Varchar2 ) return TYPES.SMALL_STRING_ARRAY
AS

  	V_FLD_SYS TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();
	V_FLD_SET TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();

	o_str_arr TYPES.SMALL_STRING_ARRAY := TYPES.SMALL_STRING_ARRAY ();

	v_count Number;
	V_KEYWORDSETSTR Varchar2(4000);
	V_KEYWORDSET Varchar2(4000);
	V_POS Number;
	v_mapset Varchar2(200);
	v_pipepos Number;
	v_mapfld_sysid Varchar2(50);
BEGIN
	 v_count := 0;

	 V_KEYWORDSETSTR := p_lkpstr || '~';
	 V_KEYWORDSET := p_lkpstr || '~';

	 V_POS := 0;

	 -- split the keyword-field sets

	 LOOP
	    V_POS := instr (V_KEYWORDSETSTR, '~');
		V_KEYWORDSET := substr (V_KEYWORDSETSTR, 1, V_POS - 1);
		      exit when V_KEYWORDSET Is null;
	 	V_COUNT := V_COUNT + 1;
		V_FLD_SET.extend;
		V_FLD_SET(V_COUNT) := V_KEYWORDSET;
 		--p('in trigger loop 2 V_FLD_KEYWORD ' || V_KEYWORDSET );
		V_KEYWORDSETSTR := substr (V_KEYWORDSETSTR, V_POS + 1);

	 END LOOP ;

	 v_count := 0;

	 FOR s IN 1..V_FLD_SET.COUNT -- put old sys id and pk_field in array
	   LOOP
	      V_COUNT := V_COUNT + 1;
	  	  v_mapset := trim(V_FLD_SET(s));
		  V_PIPEPOS := instr (v_mapset, '|');
		  v_mapfld_sysid := substr (v_mapset ,1,V_PIPEPOS - 1); --got system id

		  o_str_arr.extend;
		  o_str_arr(v_count) := v_mapfld_sysid;

	  END LOOP;

	 return o_str_arr;

END SP_GET_LKP_SYSIDS ;



END PKG_LOOKUP;
/


CREATE SYNONYM ESCH.PKG_LOOKUP FOR PKG_LOOKUP;


CREATE SYNONYM EPAT.PKG_LOOKUP FOR PKG_LOOKUP;


GRANT EXECUTE, DEBUG ON PKG_LOOKUP TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_LOOKUP TO ESCH;

