SELECT disease_site,SUM(COUNT) AS COUNT FROM (
SELECT codelst_desc AS disease_site, 0 AS COUNT FROM ER_CODELST WHERE codelst_type = 'disease_site'
UNION
SELECT disease_site,SUM(COUNT) AS COUNT FROM
(
SELECT DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst =
study_disease_site),'MULTIPLE') AS disease_site,
(SELECT COUNT(*) FROM ER_PATPROT WHERE patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
 fk_study = pk_study AND fk_site_enrolling = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat =
'Y')) AS COUNT
FROM ER_STUDY,ER_STUDYID
WHERE pk_study = fk_study AND fk_account = :sessAccId AND
fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent') AND studyId_id='Y'
)
GROUP BY disease_site)
GROUP BY disease_site
