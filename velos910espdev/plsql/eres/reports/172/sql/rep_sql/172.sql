SELECT
  '' TESTDATEVAL,
  '' LABTESTNAME,
  '' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '' ASSESSMENT_REMARKS,
  '' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''  TC_VISIBILITY_FLAG,
  ''  CONSULTE_FLAG,
  ''  ASSESSMENT_POSTEDBY,
  ''  ASSESSMENT_POSTEDON,
  ''  ASSESSMENT_CONSULTBY,
  '' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '' SOURCEVAL,
  1 LAB_TEST,
  '' lastval
FROM
  CB_CORD CRD,
  er_site site
WHERE
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,'Mon DD, YYYY'),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,'Mon DD, YYYY'),
        '',
        '',
        '',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '',
        '',
        PL.SOURCEVAL,
        ltest.pk_labtest lab_test,
        DECODE(PL.FK_TEST_OUTCOME,
                '0','YES',
                '1','No',
                null,'')
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                'Mon DD, YYYY') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type='I'
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE='IOG'
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST
