 SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS trial_type_sort,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic OR other Observational Studies',DECODE(trial_type_comp,'Y','Companion, ANCILLARY OR Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,
study_sponsor
,DISEASE_SITE,STUDY_NUMBER,PRIMARY_INVESTIGATOR,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_12_MOS, CLOSED_DATE
 FROM (
SELECT
f_get_sum4_data(a.pk_study,'sum4_agent') AS trial_type_agent,
f_get_sum4_data(a.pk_study,'sum4_beh') AS trial_type_nonagent,
f_get_sum4_data(a.pk_study,'sum4_na') AS trial_type_epi,
f_get_sum4_data(a.pk_study,'sum4_comp') AS trial_type_comp,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS research_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS study_scope,
decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PRIMARY_INVESTIGATOR,
study_maj_auth, (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_prg')) AS tarea,
TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS date_open,
(CASE WHEN study_end_date >= TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND study_end_date <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
THEN TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ELSE NULL
END) AS date_closed,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS study_phase,
SUBSTR(study_title,1,125) AS study_title,
study_nsamplsize,(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y')) AS target,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) ) AS ctr_12_mos,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'active_cls')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS closed_date
FROM ER_STUDY a, ER_STUDYSTAT b
WHERE pk_study = fk_study AND
a.fk_account = :sessAccId AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active') AND
TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) <= NVL((SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND (codelst_subtyp = 'prmnt_cls' OR codelst_subtyp = 'active_cls'))),TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) AND
TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) >= (SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active'))
)
)
WHERE trial_type <> 'NA'
ORDER BY trial_type_sort,research_type
