 Select
STUDY_NUMBER,
TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE,
STUDY_TITLE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) AS TA,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_phase) AS PHASE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) AS RESEARCH_TYPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_type) AS STUDY_TYPE,
(SELECT  ER_USER.usr_firstname || ' ' ||  ER_USER.usr_lastname
             FROM ER_USER
            WHERE pk_user = ER_STUDY.study_prinv) || DECODE(STUDY_OTHERPRINV,NULL,'',', ' || STUDY_OTHERPRINV) AS PI,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
  FROM ER_STUDY  WHERE fk_account = :sessAccId
    AND study_actualdt <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_codelst_tarea IN (:tAreaId)
    AND pk_study IN (:studyId)
    AND (study_division IN (:studyDivId) OR study_division IS NULL)
    AND ( study_prinv IN (:userId) OR study_prinv IS NULL )
    AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
    AND EXISTS (SELECT * FROM ER_STUDYSTAT WHERE pk_study = fk_study AND fk_codelst_studystat IN
    (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active' )
     AND studystat_date <=  TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    AND NOT EXISTS (SELECT * FROM ER_STUDYSTAT WHERE pk_study = fk_study AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp IN ('prmnt_cls','active_cls') )
     AND studystat_date <  TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    ORDER BY TA
