select inv_number,
(select study_number from er_study where pk_study = a.fk_study) as studynum,
(select study_title from er_study where pk_study = a.fk_study) as studytitle,
to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date, 
DECODE(trim(inv_payunit), 'D' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ),                           
'W' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ),                           
'M' ,  to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT),                                 
to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT)
) as duedays,
(SELECT USR_LASTNAME || ', '|| USR_FIRSTNAME FROM ER_USER WHERE PK_USER = INV_ADDRESSEDTO) AS INVOICE_TO,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add,
(SELECT add_city || ', ' || add_state || ', ' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add1,
(SELECT add_country FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add2,
(SELECT USR_LASTNAME || ', ' || USR_FIRSTNAME FROM ER_USER WHERE PK_USER = inv_sentfrom) AS PAYMENT_TO,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add,
(SELECT add_city || ', ' || add_state || ', ' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add1,
(SELECT add_country FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add2,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE ER_PATPROT.fk_study = a.fk_study AND ER_PATPROT.fk_per = b.fk_per AND patprot_stat = 1) fk_per,
decode(milestones_achieved,null,1,0,1,milestones_achieved) milestones_achieved,
decode(DETAIL_TYPE,'D',to_char(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT),
(Select to_char(MAX(ACH_DATE),PKG_DATEUTIL.F_GET_DATEFORMAT) from ER_MILEACHIEVED where FK_MILESTONE=PK_MILESTONE )
) achieved_on,round(amount_invoiced,2) amount_invoiced, detail_type, display_detail,
inv_notes,
decode(MILESTONE_TYPE,'AM',
(Select MILESTONE_DESCRIPTION from ER_MILESTONE where b.FK_MILESTONE=PK_MILESTONE),
'VM',(SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit),
'EM',(SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = FK_VISIT)
|| '; '||(SELECT name FROM esch.EVENT_ASSOC WHERE EVENT_ID = FK_EVENTASSOC),
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status)) AS milestone_type ,
decode(detail_type,'H',round(amount_invoiced,2),0.00) AMOUNT_INVOICED_FOR_GT,
(SELECT u.USR_LASTNAME || ', '|| u.USR_FIRSTNAME FROM er_study,er_user u WHERE pk_study = a.fk_study AND er_study.STUDY_PRINV=u.pk_user
  ) AS PI
FROM ER_INVOICE a, ER_INVOICE_DETAIL b, er_milestone c
WHERE fk_inv = pk_invoice
and fk_milestone = pk_milestone
and pk_invoice = ~1 