
select  u.USR_LASTNAME,
         u.USR_FIRSTNAME,
         a.pk_account,
         (select site_name from er_site where pk_site = u.fk_siteid) site_name,
         u.usr_stat status,
         (select codelst_desc from er_codelst where pk_codelst = u.fk_codelst_spl)
	speciality,
         (select count(*) from er_user
	 where fk_account = a.pk_account) user_count,
         u1.usr_lastname usr_list_lname,
         u1.usr_firstname usr_list_fname,
         (select add_email from er_add where pk_add = u1.fk_peradd) usr_list_email,
         (select count(*) from er_study where fk_account = a.pk_account) study_count,
         round(((ac_maxstorage*1024*1024 - ac_free_space(a.pk_account))/1024)/1024,2)
	 size_used                                       from  er_account a,
         er_user u,
         er_user u1
        where  u.pk_user = a.ac_usrcreator
        and  u1.fk_account = a.pk_account
        order by  UPPER(USR_FIRSTNAME),UPPER(USR_LASTNAME),
        pk_account,
UPPER(usr_list_fname),
        UPPER(usr_list_lname)
         
