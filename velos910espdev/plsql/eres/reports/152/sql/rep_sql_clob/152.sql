select st.pk_study, st.study_number, st.study_title,
TO_CHAR(trunc(stat.studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT) studystat_validt,
(select F_CODELST_DESC(stat.fk_codelst_studystat) from dual) studystat,
(select usr_firstname||' '||usr_lastname from er_user where pk_user = st.study_prinv) study_prinv,
(select F_CODELST_DESC(sb.submission_review_type) from dual) review_type,
sub.pk_submission,
sb.pk_submission_board, rb.review_board_name,
TO_CHAR(trunc(stat.studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) studystat_date
from  er_study st
inner join er_studystat stat on st.pk_study = stat.fk_study
left outer join er_submission sub on sub.fk_study = st.pk_study
left outer join er_submission_board sb on sb.fk_submission = sub.pk_submission
left outer join er_review_board rb on rb.pk_review_board = sb.fk_review_board
where
st.fk_account = :sessAccId
and st.pk_study in (:studyId)
and sb.fk_review_board = (select max(pk_review_board) from er_review_board where
fk_account = st.fk_account and review_board_default = 1)
and stat.studystat_validt is not null
and stat.studystat_validt = (select max(studystat_validt) from er_studystat where
fk_study = st.pk_study and FK_CODELST_STUDYSTAT =
(select pk_codelst from er_codelst where codelst_type = 'studystat'
  and codelst_subtyp = (select ctrl_value from er_ctrltab where ctrl_key = 'irb_app_stat'))
)
and stat.FK_CODELST_STUDYSTAT =
(select pk_codelst from er_codelst where codelst_type = 'studystat'
  and codelst_subtyp = (select ctrl_value from er_ctrltab where ctrl_key = 'irb_app_stat'))
and trim(sysdate) <= stat.studystat_validt and stat.studystat_validt <= add_months(trim(sysdate),3)
group by st.pk_study, st.study_number, st.study_title, stat.studystat_validt,
stat.fk_codelst_studystat, st.study_prinv, sb.submission_review_type,
sub.pk_submission, sb.pk_submission_board, rb.review_board_name,
stat.studystat_date
order by st.study_number, stat.studystat_validt desc, sb.pk_submission_board desc

