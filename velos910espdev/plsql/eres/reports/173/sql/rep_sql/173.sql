select cbbid, cbb_id, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, prcsng_start_date, bact_culture, bact_comment,
fungal_culture, fung_comment, abo_blood_type, hemoglobin_scrn, rh_type, bact_cul_strt_dt, fung_cul_strt_dt, frz_date,
(select f_lab_summary(~1) from dual) lab_summary, f_get_attachments(~1,(f_codelst_id('doc_categ','lab_sum_cat'))) labsum_attachments
from rep_cbu_details where pk_cord = ~1