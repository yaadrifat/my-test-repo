SELECT
(SELECT per_code FROM ER_PER WHERE pk_per = g.fk_per) AS patient_id,
patprot_patstdid AS patient_study_id,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
form_name, formsec_name,
(SELECT fld_name FROM ER_FLDLIB WHERE pk_field = a.fk_field) AS field,
DECODE (formquery_type, '1','System Query','2','Manual Query','3','Response') AS query_type,
(SELECT codelst_desc FROM ER_CODELST WHERE fk_codelst_querytype = pk_codelst)AS querydesc,
query_notes,
(SELECT codelst_desc FROM ER_CODELST WHERE fk_codelst_querystatus = pk_codelst)AS fk_codelst_querystatus,
(SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE pk_user = a.creator) AS creator,
to_char(a.created_on,PKG_DATEUTIL.F_GET_DATEtimeFORMAT) as created_on,
fk_formquery,
fk_querymodule,to_char(patforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT) as patforms_filldate
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE b.fk_formquery = a.pk_formquery
AND b.PK_FORMQUERYSTATUS = (select max(b1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS b1
where a.pk_formquery = b1.fk_formquery
AND trim(b1.ENTERED_ON) = (select max(trim(b2.ENTERED_ON)) from ER_FORMQUERYSTATUS b2
where a.pk_formquery = b2.fk_formquery))
AND a.fk_field = c.fk_field
AND d.pk_formsec = c.fk_formsec
AND e.pk_formlib = d.fk_formlib
AND pk_formlib = f.fk_formlib AND f.RECORD_TYPE <> 'D'
AND pk_patforms = fk_querymodule
AND pk_patprot = fk_patprot
AND fk_study IN (:studyId) AND g.fk_per in (:patientId)
and g.fk_site_enrolling in (:orgId)
and (a.created_on BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) )
ORDER BY patient_id, form_name, formsec_name, field
