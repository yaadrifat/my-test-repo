select s.pk_storage PPK_Storage, Storage_id, storage_name,
s.STORAGE_ALTERNALEID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = s.STORAGE_UNIT_CLASS) STORAGE_UNIT_CLASS,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.FK_CODELST_STORAGE_TYPE) STORAGE_TYPE,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = ss.FK_CODELST_STORAGE_STATUS) STORAGE_STATUS,
STORAGE_CAP_NUMBER CAPACITY_SPECIFIED,
STORAGE_DIM1_CELLNUM * STORAGE_DIM2_CELLNUM CAPACITY_CALCULATED
from er_storage s, er_storage_status ss
WHERE s.pk_storage = ss.fk_storage
AND FK_CODELST_CAPACITY_UNITS in (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp ='Items')
AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1 WHERE ss1.FK_STORAGE = s.PK_STORAGE
AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) FROM er_storage_status ss2 WHERE ss2.FK_STORAGE = s.PK_STORAGE))
AND STORAGE_CAP_NUMBER <> (STORAGE_DIM1_CELLNUM * STORAGE_DIM2_CELLNUM)
AND FK_CODELST_CHILD_STYPE is not null
AND s.pk_storage in (:storageId)
order by s.storage_name,s.Storage_id
