
  CREATE OR REPLACE TRIGGER "ERES"."ER_STUDYSITES_AD"
AFTER DELETE
ON ER_STUDYSITES
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  orid number(10);
  usr VARCHAR(2000);
BEGIN

    usr := Getuser(:OLD.last_modified_by);
   orid:=nvl(:old.rid,0);
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYSITES', orid, 'D',usr);
 deleted_data :=TO_CHAR(:OLD.PK_STUDYSITES)||'|'||
 TO_CHAR(:OLD.FK_STUDY)||'|'||
 TO_CHAR(:OLD.FK_SITE)||'|'||
 TO_CHAR(:OLD.FK_CODELST_STUDYSITETYPE)||'|'||
 :OLD.STUDYSITE_LSAMPLESIZE||'|'||
 TO_CHAR(:OLD.STUDYSITE_PUBLIC)||'|'||
 TO_CHAR(:OLD.STUDYSITE_REPINCLUDE)||'|'||
 TO_CHAR(orid)||'|'||
 TO_CHAR(:OLD.CREATOR)||'|'||
 TO_CHAR(:OLD.CREATED_ON)||'|'||
 TO_CHAR(:OLD.LAST_MODIFIED_BY)||'|'||
 TO_CHAR(:OLD.LAST_MODIFIED_ON)||'|'||
 :OLD.IP_ADD||'|'||
 TO_CHAR(:OLD.STUDYSITE_ENRCOUNT);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);

END;
/


