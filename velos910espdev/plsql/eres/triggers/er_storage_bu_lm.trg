CREATE OR REPLACE TRIGGER "ER_STORAGE_BU_LM" BEFORE UPDATE ON ER_STORAGE
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
begin
--Created by Manimaran for Auditing
:new.last_modified_date := sysdate;
end;
/


