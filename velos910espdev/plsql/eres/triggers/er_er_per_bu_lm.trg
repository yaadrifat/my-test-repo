CREATE OR REPLACE TRIGGER "ER_ER_PER_BU_LM" BEFORE UPDATE ON ER_PER
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
begin
:new.last_modified_date := sysdate ;
end;
/


