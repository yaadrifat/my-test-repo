CREATE OR REPLACE TRIGGER ER_PATFORMS_AU0
AFTER UPDATE OF RID,FK_PER,IP_ADD,ISVALID,FK_FORMLIB,FK_PATPROT,PK_PATFORMS,RECORD_TYPE,FORM_COMPLETED,LAST_MODIFIED_BY,PATFORMS_FILLDATE,LAST_MODIFIED_DATE,FK_SPECIMEN
ON ER_PATFORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);

   usr VARCHAR2(100);

   old_modby VARCHAR2(100);

   new_modby VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_PATFORMS', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_patforms,0) !=
     NVL(:NEW.pk_patforms,0) THEN
     audit_trail.column_update
       (raid, 'PK_PATFORMS',
       :OLD.pk_patforms, :NEW.pk_patforms);
  END IF;
  IF NVL(:OLD.fk_formlib,0) !=
     NVL(:NEW.fk_formlib,0) THEN
     audit_trail.column_update
       (raid, 'FK_FORMLIB',
       :OLD.fk_formlib, :NEW.fk_formlib);
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.fk_patprot,0) !=
     NVL(:NEW.fk_patprot,0) THEN
     audit_trail.column_update
       (raid, 'FK_PATPROT',
       :OLD.fk_patprot, :NEW.fk_patprot);
  END IF;
  IF NVL(:OLD.patforms_filldate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patforms_filldate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATFORMS_FILLDATE',
       to_char(:OLD.patforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.form_completed,0) !=
     NVL(:NEW.form_completed,0) THEN
     audit_trail.column_update
       (raid, 'FORM_COMPLETED',
       :OLD.form_completed, :NEW.form_completed);
  END IF;
  IF NVL(:OLD.record_type,' ') !=
     NVL(:NEW.record_type,' ') THEN
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :OLD.record_type, :NEW.record_type);
  END IF;
  IF NVL(:OLD.isvalid,0) !=
     NVL(:NEW.isvalid,0) THEN
     audit_trail.column_update
       (raid, 'ISVALID',
       :OLD.isvalid, :NEW.isvalid);
  END IF;

 IF NVL(:OLD.notification_sent,0) !=

     NVL(:NEW.notification_sent,0) THEN

     audit_trail.column_update

       (raid, 'NOTIFICATION_SENT',

       :OLD.notification_sent, :NEW.notification_sent);

 END IF;


 IF NVL(:OLD.process_data,0) !=

     NVL(:NEW.process_data,0) THEN

     audit_trail.column_update

       (raid, 'PROCESS_DATA',

       :OLD.process_data, :NEW.process_data);

 END IF;


 IF NVL(:OLD.fk_formlibver,0) !=

     NVL(:NEW.fk_formlibver,0) THEN

     audit_trail.column_update

       (raid, 'FK_FORMLIBVER',

       :OLD.fk_formlibver, :NEW.fk_formlibver);

  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
              to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
 IF NVL(:OLD.LAST_MODIFIED_BY,0) !=
 NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.last_modified_by ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			old_modby := NULL;
	END ;
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			new_modby := NULL;
	 END ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 END IF;

 IF NVL(:OLD.fk_specimen,0) !=

     NVL(:NEW.fk_specimen,0) THEN

     audit_trail.column_update

       (raid, 'FK_SPECIMEN',

       :OLD.fk_specimen, :NEW.fk_specimen);

  END IF;


END;
/


