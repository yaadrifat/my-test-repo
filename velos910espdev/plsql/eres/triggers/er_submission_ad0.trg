CREATE OR REPLACE TRIGGER "ER_SUBMISSION_AD0"
  after delete
  on ER_SUBMISSION
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);
  usr VARCHAR2(500);

begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(NVL(:OLD.LAST_MODIFIED_BY,:OLD.CREATOR));

  audit_trail.record_transaction
    (raid, 'ER_SUBMISSION', :old.rid, 'D', usr);

  deleted_data :=
  to_char(:old.PK_SUBMISSION) || '|' ||
  to_char(:old.FK_STUDY) || '|' ||
  to_char(:old.SUBMISSION_FLAG) || '|' ||
  to_char(:old.SUBMISSION_TYPE) || '|' ||
  to_char(:old.SUBMISSION_STATUS) || '|' ||
  to_char(:old.CREATED_ON) || '|' ||
  to_char(:old.CREATOR) || '|' ||
  to_char(:old.LAST_MODIFIED_DATE) || '|' ||
  to_char(:old.LAST_MODIFIED_BY) || '|' ||
  to_char(:old.RID) || '|' ||
  :old.IP_ADD;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


