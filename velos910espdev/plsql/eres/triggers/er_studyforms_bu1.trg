CREATE OR REPLACE TRIGGER "ER_STUDYFORMS_BU1"
BEFORE UPDATE OF STUDYFORMS_XML
ON ER_STUDYFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  datestr Varchar2(20);
  v_form_xml  sys.xmlType;
  i   NUMBER := 1;
 begin
  :new.NOTIFICATION_SENT := 0; --reset the notification flag
  v_form_xml := :new.STUDYFORMS_XML;
          datestr := v_form_xml.extract('/rowset/er_def_date_01/text()').getStringVal();
       if datestr is not null then
       :new.STUDYFORMS_FILLDATE  := to_date(datestr,PKG_DATEUTIL.F_GET_DATEFORMAT);
    end if ;
 if :new.STUDYFORMS_FILLDATE is null then
   :new.STUDYFORMS_FILLDATE  := sysdate;
     END IF;
 end;
/


