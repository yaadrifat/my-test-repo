CREATE OR REPLACE TRIGGER "ER_PATFACILITY_AI0" 
AFTER INSERT
ON ER_PATFACILITY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE


/**************************************************************************************************
   **
   ** Author: Sonia Abrol 09/28/2007
   ** call procedure to create portal auto login if exists
*/

BEGIN

 -- call procedure to create portal auto login if exists

pkg_portal.sp_create_deflogin('F', :new.fk_per,0,
  :new.fk_site,:new.creator,:new.ip_add  );

END;
/


