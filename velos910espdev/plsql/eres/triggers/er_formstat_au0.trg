CREATE OR REPLACE TRIGGER ER_FORMSTAT_AU0
  after update of
  pk_formstat,
  fk_formlib,
  fk_codelst_stat,
  formstat_changedby,
  formstat_stdate,
  formstat_enddate,
  formstat_notes,
  record_type,
  last_modified_by,
  last_modified_date,
  ip_add,
  rid
  on er_formstat
  for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FORMSTAT', :old.rid, 'U', usr);

  if nvl(:old.pk_formstat,0) !=
     NVL(:new.pk_formstat,0) then
     audit_trail.column_update
       (raid, 'PK_FORMSTAT',
       :old.pk_formstat, :new.pk_formstat);
  end if;
  if nvl(:old.fk_formlib,0) !=
     NVL(:new.fk_formlib,0) then
     audit_trail.column_update
       (raid, 'FK_FORMLIB',
       :old.fk_formlib, :new.fk_formlib);
  end if;
  if nvl(:old.fk_codelst_stat,0) !=
     NVL(:new.fk_codelst_stat,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_STAT',
       :old.fk_codelst_stat, :new.fk_codelst_stat);
  end if;
  if nvl(:old.formstat_changedby,0) !=
     NVL(:new.formstat_changedby,0) then
     audit_trail.column_update
       (raid, 'FORMSTAT_CHANGEDBY',
       :old.formstat_changedby, :new.formstat_changedby);
  end if;
  if nvl(:old.formstat_stdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.formstat_stdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'FORMSTAT_STDATE',
       to_char(:old.formstat_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.formstat_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.formstat_enddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.formstat_enddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'FORMSTAT_ENDDATE',
       to_char(:old.formstat_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.formstat_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.formstat_notes,' ') !=
     NVL(:new.formstat_notes,' ') then
     audit_trail.column_update
       (raid, 'FORMSTAT_NOTES',
       :old.formstat_notes, :new.formstat_notes);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/


