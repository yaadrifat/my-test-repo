create or replace
TRIGGER "ERES".ER_MILESTONE_AD0 AFTER DELETE ON ER_MILESTONE
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_MILESTONE', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_milestone) || '|' ||
  to_char(:old.fk_study) || '|' ||
  :old.milestone_type || '|' ||
  to_char(:old.fk_cal) || '|' ||
  to_char(:old.fk_codelst_rule) || '|' ||
  to_char(:old.milestone_amount) || '|' ||
  to_char(:old.milestone_visit) || '|' ||
  to_char(:old.fk_eventassoc) || '|' ||
  to_char(:old.milestone_count) || '|' ||
  :old.milestone_delflag || '|' ||
  :old.milestone_usersto || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  to_char(:old.fk_codelst_milestone_stat) || '|' ||
  :old.milestone_description; --KM
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/