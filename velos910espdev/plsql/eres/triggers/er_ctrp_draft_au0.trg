CREATE OR REPLACE TRIGGER ERES.ER_CTRP_DRAFT_AU0
AFTER UPDATE OF PK_CTRP_DRAFT,FK_STUDY,CTRP_DRAFT_TYPE,FK_CODELST_SUBMISSION_TYPE,AMEND_NUMBER,AMEND_DATE,DRAFT_XML_REQD,FK_TRIAL_OWNER,NCI_STUDYID,
LEAD_ORG_STUDYID,SUBMIT_ORG_STUDYID,NCT_NUMBER,OTHER_NUMBER,FK_CODELST_PHASE,DRAFT_PILOT,STUDY_TYPE,FK_CODELST_INTERVENTION_TYPE,INTERVENTION_NAME,
FK_CODELST_DISEASE_SITE,STUDY_PURPOSE,STUDY_PURPOSE_OTHER,LEAD_ORG_CTRPID,LEAD_ORG_FK_SITE,LEAD_ORG_FK_ADD,LEAD_ORG_TYPE,SUBMIT_ORG_CTRPID,SUBMIT_ORG_FK_SITE,
SUBMIT_ORG_FK_ADD,SUBMIT_ORG_TYPE,SUBMIT_NCI_DESIGNATED,PI_CTRPID,PI_FIRST_NAME,PI_FK_USER,PI_LAST_NAME,PI_FK_ADD,SPONSOR_CTRPID,SPONSOR_FK_SITE,SPONSOR_FK_ADD,
RESPONSIBLE_PARTY,RP_SPONSOR_CONTACT_TYPE,RP_SPONSOR_CONTACT_ID,SPONSOR_PERSONAL_FIRST_NAME,SPONSOR_PERSONAL_FK_USER,SPONSOR_PERSONAL_LAST_NAME,
SPONSOR_PERSONAL_FK_ADD,SPONSOR_GENERIC_TITLE,RP_EMAIL,RP_PHONE,RP_EXTN,SUMM4_SPONSOR_TYPE,SUMM4_SPONSOR_ID,SUMM4_SPONSOR_FK_SITE,
SUMM4_SPONSOR_FK_ADD,SPONSOR_PROGRAM_CODE,FK_CODELST_CTRP_STUDY_STATUS,CTRP_STUDY_STATUS_DATE,CTRP_STUDY_STOP_REASON,CTRP_STUDY_START_DATE,
IS_START_ACTUAL,CTRP_STUDY_COMP_DATE,IS_COMP_ACTUAL,CTRP_STUDYACC_OPEN_DATE,CTRP_STUDYACC_CLOSED_DATE,SITE_TARGET_ACCRUAL,OVERSIGHT_COUNTRY,
OVERSIGHT_ORGANIZATION,FDA_INTVEN_INDICATOR,SECTION_801_INDICATOR,DELAY_POST_INDICATOR,DM_APPOINT_INDICATOR,DELETED_FLAG,
IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE
ON ERES.ER_CTRP_DRAFT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);
	old_modby VARCHAR2(100);
	new_modby VARCHAR2(100);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);
	audit_trail.record_transaction
	(raid, 'ER_CTRP_DRAFT', :OLD.rid, 'U', usr);

	IF NVL(:OLD.PK_CTRP_DRAFT,0) != NVL(:NEW.PK_CTRP_DRAFT,0) then
		audit_trail.column_update (raid, 'PK_CTRP_DRAFT',:OLD.PK_CTRP_DRAFT,:NEW.PK_CTRP_DRAFT);
	end if;
	IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) then
		audit_trail.column_update (raid, 'FK_STUDY',:OLD.FK_STUDY,:NEW.FK_STUDY);
	end if;
	IF NVL(:OLD.CTRP_DRAFT_TYPE,0) != NVL(:NEW.CTRP_DRAFT_TYPE,0) then
		audit_trail.column_update (raid, 'CTRP_DRAFT_TYPE',:OLD.CTRP_DRAFT_TYPE,:NEW.CTRP_DRAFT_TYPE);
	end if;
	IF NVL(:OLD.FK_CODELST_SUBMISSION_TYPE,0) != NVL(:NEW.FK_CODELST_SUBMISSION_TYPE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_SUBMISSION_TYPE',:OLD.FK_CODELST_SUBMISSION_TYPE,:NEW.FK_CODELST_SUBMISSION_TYPE);
	end if;
	IF NVL(:OLD.AMEND_NUMBER,' ') != NVL(:NEW.AMEND_NUMBER,' ') then
		audit_trail.column_update (raid, 'AMEND_NUMBER',:OLD.AMEND_NUMBER,:NEW.AMEND_NUMBER);
	end if;
	IF NVL(:OLD.AMEND_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.AMEND_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'AMEND_DATE',to_char(:OLD.AMEND_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.AMEND_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.DRAFT_XML_REQD,0) != NVL(:NEW.DRAFT_XML_REQD,0) then
		audit_trail.column_update (raid, 'DRAFT_XML_REQD',:OLD.DRAFT_XML_REQD,:NEW.DRAFT_XML_REQD);
	end if;
	IF NVL(:OLD.FK_TRIAL_OWNER,0) != NVL(:NEW.FK_TRIAL_OWNER,0) then
		audit_trail.column_update (raid, 'FK_TRIAL_OWNER',:OLD.FK_TRIAL_OWNER,:NEW.FK_TRIAL_OWNER);
	end if;
	IF NVL(:OLD.NCI_STUDYID,0) != NVL(:NEW.NCI_STUDYID,0) then
		audit_trail.column_update (raid, 'NCI_STUDYID',:OLD.NCI_STUDYID,:NEW.NCI_STUDYID);
	end if;
	IF NVL(:OLD.LEAD_ORG_STUDYID,' ') != NVL(:NEW.LEAD_ORG_STUDYID,' ') then
		audit_trail.column_update (raid, 'LEAD_ORG_STUDYID',:OLD.LEAD_ORG_STUDYID,:NEW.LEAD_ORG_STUDYID);
	end if;
	IF NVL(:OLD.SUBMIT_ORG_STUDYID,' ') != NVL(:NEW.SUBMIT_ORG_STUDYID,' ') then
		audit_trail.column_update (raid, 'SUBMIT_ORG_STUDYID',:OLD.SUBMIT_ORG_STUDYID,:NEW.SUBMIT_ORG_STUDYID);
	end if;
	IF NVL(:OLD.NCT_NUMBER,' ') != NVL(:NEW.NCT_NUMBER,' ') then
		audit_trail.column_update (raid, 'NCT_NUMBER',:OLD.NCT_NUMBER,:NEW.NCT_NUMBER);
	end if;
	IF NVL(:OLD.OTHER_NUMBER,' ') != NVL(:NEW.OTHER_NUMBER,' ') then
		audit_trail.column_update (raid, 'OTHER_NUMBER',:OLD.OTHER_NUMBER,:NEW.OTHER_NUMBER);
	end if;
	IF NVL(:OLD.FK_CODELST_PHASE,0) != NVL(:NEW.FK_CODELST_PHASE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_PHASE',:OLD.FK_CODELST_PHASE,:NEW.FK_CODELST_PHASE);
	end if;
	IF NVL(:OLD.DRAFT_PILOT,0) != NVL(:NEW.DRAFT_PILOT,0) then
		audit_trail.column_update (raid, 'DRAFT_PILOT',:OLD.DRAFT_PILOT,:NEW.DRAFT_PILOT);
	end if;
	IF NVL(:OLD.STUDY_TYPE,0) != NVL(:NEW.STUDY_TYPE,0) then
		audit_trail.column_update (raid, 'STUDY_TYPE',:OLD.STUDY_TYPE,:NEW.STUDY_TYPE);
	end if;
	IF NVL(:OLD.FK_CODELST_INTERVENTION_TYPE,0) != NVL(:NEW.FK_CODELST_INTERVENTION_TYPE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_INTERVENTION_TYPE',:OLD.FK_CODELST_INTERVENTION_TYPE,:NEW.FK_CODELST_INTERVENTION_TYPE);
	end if;
	IF NVL(:OLD.INTERVENTION_NAME,' ') != NVL(:NEW.INTERVENTION_NAME,' ') then
		audit_trail.column_update (raid, 'INTERVENTION_NAME',:OLD.INTERVENTION_NAME,:NEW.INTERVENTION_NAME);
	end if;
	IF NVL(:OLD.FK_CODELST_DISEASE_SITE,0) != NVL(:NEW.FK_CODELST_DISEASE_SITE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_DISEASE_SITE',:OLD.FK_CODELST_DISEASE_SITE,:NEW.FK_CODELST_DISEASE_SITE);
	end if;
	IF NVL(:OLD.STUDY_PURPOSE,0) != NVL(:NEW.STUDY_PURPOSE,0) then
		audit_trail.column_update (raid, 'STUDY_PURPOSE',:OLD.STUDY_PURPOSE,:NEW.STUDY_PURPOSE);
	end if;
	IF NVL(:OLD.STUDY_PURPOSE_OTHER,' ') != NVL(:NEW.STUDY_PURPOSE_OTHER,' ') then
		audit_trail.column_update (raid, 'STUDY_PURPOSE_OTHER',:OLD.STUDY_PURPOSE_OTHER,:NEW.STUDY_PURPOSE_OTHER);
	end if;
	IF NVL(:OLD.LEAD_ORG_CTRPID,' ') != NVL(:NEW.LEAD_ORG_CTRPID,' ') then
		audit_trail.column_update (raid, 'LEAD_ORG_CTRPID',:OLD.LEAD_ORG_CTRPID,:NEW.LEAD_ORG_CTRPID);
	end if;
	IF NVL(:OLD.LEAD_ORG_FK_SITE,0) != NVL(:NEW.LEAD_ORG_FK_SITE,0) then
		audit_trail.column_update (raid, 'LEAD_ORG_FK_SITE',:OLD.LEAD_ORG_FK_SITE,:NEW.LEAD_ORG_FK_SITE);
	end if;
	IF NVL(:OLD.LEAD_ORG_FK_ADD,0) != NVL(:NEW.LEAD_ORG_FK_ADD,0) then
		audit_trail.column_update (raid, 'LEAD_ORG_FK_ADD',:OLD.LEAD_ORG_FK_ADD,:NEW.LEAD_ORG_FK_ADD);
	end if;
	IF NVL(:OLD.LEAD_ORG_TYPE,0) != NVL(:NEW.LEAD_ORG_TYPE,0) then
		audit_trail.column_update (raid, 'LEAD_ORG_TYPE',:OLD.LEAD_ORG_TYPE,:NEW.LEAD_ORG_TYPE);
	end if;
	IF NVL(:OLD.SUBMIT_ORG_CTRPID,' ') != NVL(:NEW.SUBMIT_ORG_CTRPID,' ') then
		audit_trail.column_update (raid, 'SUBMIT_ORG_CTRPID',:OLD.SUBMIT_ORG_CTRPID,:NEW.SUBMIT_ORG_CTRPID);
	end if;
	IF NVL(:OLD.SUBMIT_ORG_FK_SITE,0) != NVL(:NEW.SUBMIT_ORG_FK_SITE,0) then
		audit_trail.column_update (raid, 'SUBMIT_ORG_FK_SITE',:OLD.SUBMIT_ORG_FK_SITE,:NEW.SUBMIT_ORG_FK_SITE);
	end if;
	IF NVL(:OLD.SUBMIT_ORG_FK_ADD,0) != NVL(:NEW.SUBMIT_ORG_FK_ADD,0) then
		audit_trail.column_update (raid, 'SUBMIT_ORG_FK_ADD',:OLD.SUBMIT_ORG_FK_ADD,:NEW.SUBMIT_ORG_FK_ADD);
	end if;
	IF NVL(:OLD.SUBMIT_ORG_TYPE,0) != NVL(:NEW.SUBMIT_ORG_TYPE,0) then
		audit_trail.column_update (raid, 'SUBMIT_ORG_TYPE',:OLD.SUBMIT_ORG_TYPE,:NEW.SUBMIT_ORG_TYPE);
	end if;
	IF NVL(:OLD.SUBMIT_NCI_DESIGNATED,0) != NVL(:NEW.SUBMIT_NCI_DESIGNATED,0) then
		audit_trail.column_update (raid, 'SUBMIT_NCI_DESIGNATED',:OLD.SUBMIT_NCI_DESIGNATED,:NEW.SUBMIT_NCI_DESIGNATED);
	end if;
	IF NVL(:OLD.PI_CTRPID,' ') != NVL(:NEW.PI_CTRPID,' ') then
		audit_trail.column_update (raid, 'PI_CTRPID',:OLD.PI_CTRPID,:NEW.PI_CTRPID);
	end if;
	IF NVL(:OLD.PI_FIRST_NAME,' ') != NVL(:NEW.PI_FIRST_NAME,' ') then
		audit_trail.column_update (raid, 'PI_FIRST_NAME',:OLD.PI_FIRST_NAME,:NEW.PI_FIRST_NAME);
	end if;
	IF NVL(:OLD.PI_FK_USER,0) != NVL(:NEW.PI_FK_USER,0) then
		audit_trail.column_update (raid, 'PI_FK_USER',:OLD.PI_FK_USER,:NEW.PI_FK_USER);
	end if;
	IF NVL(:OLD.PI_LAST_NAME,' ') != NVL(:NEW.PI_LAST_NAME,' ') then
		audit_trail.column_update (raid, 'PI_LAST_NAME',:OLD.PI_LAST_NAME,:NEW.PI_LAST_NAME);
	end if;
	IF NVL(:OLD.PI_FK_ADD,0) != NVL(:NEW.PI_FK_ADD,0) then
		audit_trail.column_update (raid, 'PI_FK_ADD',:OLD.PI_FK_ADD,:NEW.PI_FK_ADD);
	end if;
	IF NVL(:OLD.SPONSOR_CTRPID,' ') != NVL(:NEW.SPONSOR_CTRPID,' ') then
		audit_trail.column_update (raid, 'SPONSOR_CTRPID',:OLD.SPONSOR_CTRPID,:NEW.SPONSOR_CTRPID);
	end if;
	IF NVL(:OLD.SPONSOR_FK_SITE,0) != NVL(:NEW.SPONSOR_FK_SITE,0) then
		audit_trail.column_update (raid, 'SPONSOR_FK_SITE',:OLD.SPONSOR_FK_SITE,:NEW.SPONSOR_FK_SITE);
	end if;
	IF NVL(:OLD.SPONSOR_FK_ADD,0) != NVL(:NEW.SPONSOR_FK_ADD,0) then
		audit_trail.column_update (raid, 'SPONSOR_FK_ADD',:OLD.SPONSOR_FK_ADD,:NEW.SPONSOR_FK_ADD);
	end if;
	IF NVL(:OLD.RESPONSIBLE_PARTY,0) != NVL(:NEW.RESPONSIBLE_PARTY,0) then
		audit_trail.column_update (raid, 'RESPONSIBLE_PARTY',:OLD.RESPONSIBLE_PARTY,:NEW.RESPONSIBLE_PARTY);
	end if;
	IF NVL(:OLD.RP_SPONSOR_CONTACT_TYPE,0) != NVL(:NEW.RP_SPONSOR_CONTACT_TYPE,0) then
		audit_trail.column_update (raid, 'RP_SPONSOR_CONTACT_TYPE',:OLD.RP_SPONSOR_CONTACT_TYPE,:NEW.RP_SPONSOR_CONTACT_TYPE);
	end if;
	IF NVL(:OLD.RP_SPONSOR_CONTACT_ID,' ') != NVL(:NEW.RP_SPONSOR_CONTACT_ID,' ') then
		audit_trail.column_update (raid, 'RP_SPONSOR_CONTACT_ID',:OLD.RP_SPONSOR_CONTACT_ID,:NEW.RP_SPONSOR_CONTACT_ID);
	end if;
	IF NVL(:OLD.SPONSOR_PERSONAL_FIRST_NAME,' ') != NVL(:NEW.SPONSOR_PERSONAL_FIRST_NAME,' ') then
		audit_trail.column_update (raid, 'SPONSOR_PERSONAL_FIRST_NAME',:OLD.SPONSOR_PERSONAL_FIRST_NAME,:NEW.SPONSOR_PERSONAL_FIRST_NAME);
	end if;
	IF NVL(:OLD.SPONSOR_PERSONAL_FK_USER,0) != NVL(:NEW.SPONSOR_PERSONAL_FK_USER,0) then
		audit_trail.column_update (raid, 'SPONSOR_PERSONAL_FK_USER',:OLD.SPONSOR_PERSONAL_FK_USER,:NEW.SPONSOR_PERSONAL_FK_USER);
	end if;
	IF NVL(:OLD.SPONSOR_PERSONAL_LAST_NAME,' ') != NVL(:NEW.SPONSOR_PERSONAL_LAST_NAME,' ') then
		audit_trail.column_update (raid, 'SPONSOR_PERSONAL_LAST_NAME',:OLD.SPONSOR_PERSONAL_LAST_NAME,:NEW.SPONSOR_PERSONAL_LAST_NAME);
	end if;
	IF NVL(:OLD.SPONSOR_PERSONAL_FK_ADD,0) != NVL(:NEW.SPONSOR_PERSONAL_FK_ADD,0) then 
		audit_trail.column_update (raid, 'SPONSOR_PERSONAL_FK_ADD',:OLD.SPONSOR_PERSONAL_FK_ADD,:NEW.SPONSOR_PERSONAL_FK_ADD);
	end if;
	IF NVL(:OLD.SPONSOR_GENERIC_TITLE,' ') != NVL(:NEW.SPONSOR_GENERIC_TITLE,' ') then
		audit_trail.column_update (raid, 'SPONSOR_GENERIC_TITLE',:OLD.SPONSOR_GENERIC_TITLE,:NEW.SPONSOR_GENERIC_TITLE);
	end if;
	IF NVL(:OLD.RP_EMAIL,' ') != NVL(:NEW.RP_EMAIL,' ') then
		audit_trail.column_update (raid, 'RP_EMAIL',:OLD.RP_EMAIL,:NEW.RP_EMAIL);
	end if;
	IF NVL(:OLD.RP_PHONE,' ') != NVL(:NEW.RP_PHONE,' ') then
		audit_trail.column_update (raid, 'RP_PHONE',:OLD.RP_PHONE,:NEW.RP_PHONE);
	end if;
	IF NVL(:OLD.RP_EXTN,0) != NVL(:NEW.RP_EXTN,0) then
		audit_trail.column_update (raid, 'RP_EXTN',:OLD.RP_EXTN,:NEW.RP_EXTN);
	end if;
	IF NVL(:OLD.SUMM4_SPONSOR_TYPE,0) != NVL(:NEW.SUMM4_SPONSOR_TYPE,0) then
		audit_trail.column_update (raid, 'SUMM4_SPONSOR_TYPE',:OLD.SUMM4_SPONSOR_TYPE,:NEW.SUMM4_SPONSOR_TYPE);
	end if;
	IF NVL(:OLD.SUMM4_SPONSOR_ID,' ') != NVL(:NEW.SUMM4_SPONSOR_ID,' ') then
		audit_trail.column_update (raid, 'SUMM4_SPONSOR_ID',:OLD.SUMM4_SPONSOR_ID,:NEW.SUMM4_SPONSOR_ID);
	end if;
	IF NVL(:OLD.SUMM4_SPONSOR_FK_SITE,0) != NVL(:NEW.SUMM4_SPONSOR_FK_SITE,0) then
		audit_trail.column_update (raid, 'SUMM4_SPONSOR_FK_SITE',:OLD.SUMM4_SPONSOR_FK_SITE,:NEW.SUMM4_SPONSOR_FK_SITE);
	end if;
	IF NVL(:OLD.SUMM4_SPONSOR_FK_ADD,0) != NVL(:NEW.SUMM4_SPONSOR_FK_ADD,0) then
		audit_trail.column_update (raid, 'SUMM4_SPONSOR_FK_ADD',:OLD.SUMM4_SPONSOR_FK_ADD,:NEW.SUMM4_SPONSOR_FK_ADD);
	end if;
	IF NVL(:OLD.SPONSOR_PROGRAM_CODE,' ') != NVL(:NEW.SPONSOR_PROGRAM_CODE,' ') then
		audit_trail.column_update (raid, 'SPONSOR_PROGRAM_CODE',:OLD.SPONSOR_PROGRAM_CODE,:NEW.SPONSOR_PROGRAM_CODE);
	end if;
	IF NVL(:OLD.FK_CODELST_CTRP_STUDY_STATUS,0) != NVL(:NEW.FK_CODELST_CTRP_STUDY_STATUS,0) then
		audit_trail.column_update (raid, 'FK_CODELST_CTRP_STUDY_STATUS',:OLD.FK_CODELST_CTRP_STUDY_STATUS,:NEW.FK_CODELST_CTRP_STUDY_STATUS);
	end if;
	IF NVL(:OLD.CTRP_STUDY_STATUS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CTRP_STUDY_STATUS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'CTRP_STUDY_STATUS_DATE',to_char(:OLD.CTRP_STUDY_STATUS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_STUDY_STATUS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.CTRP_STUDY_STOP_REASON,' ') != NVL(:NEW.CTRP_STUDY_STOP_REASON,' ') then
		audit_trail.column_update (raid, 'CTRP_STUDY_STOP_REASON',:OLD.CTRP_STUDY_STOP_REASON,:NEW.CTRP_STUDY_STOP_REASON);
	end if;
	IF NVL(:OLD.CTRP_STUDY_START_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CTRP_STUDY_START_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'CTRP_STUDY_START_DATE',to_char(:OLD.CTRP_STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.IS_START_ACTUAL,0) != NVL(:NEW.IS_START_ACTUAL,0) then
		audit_trail.column_update (raid, 'IS_START_ACTUAL',:OLD.IS_START_ACTUAL,:NEW.IS_START_ACTUAL);
	end if;
	IF NVL(:OLD.CTRP_STUDY_COMP_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CTRP_STUDY_COMP_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'CTRP_STUDY_COMP_DATE',to_char(:OLD.CTRP_STUDY_COMP_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_STUDY_COMP_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.IS_COMP_ACTUAL,0) != NVL(:NEW.IS_COMP_ACTUAL,0) then
		audit_trail.column_update (raid, 'IS_COMP_ACTUAL',:OLD.IS_COMP_ACTUAL,:NEW.IS_COMP_ACTUAL);
	end if;
	IF NVL(:OLD.CTRP_STUDYACC_OPEN_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CTRP_STUDYACC_OPEN_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'CTRP_STUDYACC_OPEN_DATE',to_char(:OLD.CTRP_STUDYACC_OPEN_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_STUDYACC_OPEN_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.CTRP_STUDYACC_CLOSED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CTRP_STUDYACC_CLOSED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'CTRP_STUDYACC_CLOSED_DATE',to_char(:OLD.CTRP_STUDYACC_CLOSED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_STUDYACC_CLOSED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
	IF NVL(:OLD.SITE_TARGET_ACCRUAL,0) != NVL(:NEW.SITE_TARGET_ACCRUAL,0) then
		audit_trail.column_update (raid, 'SITE_TARGET_ACCRUAL',:OLD.SITE_TARGET_ACCRUAL,:NEW.SITE_TARGET_ACCRUAL);
	end if;
	IF NVL(:OLD.OVERSIGHT_COUNTRY,0) != NVL(:NEW.OVERSIGHT_COUNTRY,0) then
		audit_trail.column_update (raid, 'OVERSIGHT_COUNTRY',:OLD.OVERSIGHT_COUNTRY,:NEW.OVERSIGHT_COUNTRY);
	end if;
	IF NVL(:OLD.OVERSIGHT_ORGANIZATION,0) != NVL(:NEW.OVERSIGHT_ORGANIZATION,0) then
		audit_trail.column_update (raid, 'OVERSIGHT_ORGANIZATION',:OLD.OVERSIGHT_ORGANIZATION,:NEW.OVERSIGHT_ORGANIZATION);
	end if;
	IF NVL(:OLD.FDA_INTVEN_INDICATOR,0) != NVL(:NEW.FDA_INTVEN_INDICATOR,0) then
		audit_trail.column_update (raid, 'FDA_INTVEN_INDICATOR',:OLD.FDA_INTVEN_INDICATOR,:NEW.FDA_INTVEN_INDICATOR);
	end if;
	IF NVL(:OLD.SECTION_801_INDICATOR,0) != NVL(:NEW.SECTION_801_INDICATOR,0) then
		audit_trail.column_update (raid, 'SECTION_801_INDICATOR',:OLD.SECTION_801_INDICATOR,:NEW.SECTION_801_INDICATOR);
	end if;
	IF NVL(:OLD.DELAY_POST_INDICATOR,0) != NVL(:NEW.DELAY_POST_INDICATOR,0) then
		audit_trail.column_update (raid, 'DELAY_POST_INDICATOR',:OLD.DELAY_POST_INDICATOR,:NEW.DELAY_POST_INDICATOR);
	end if;
	IF NVL(:OLD.DM_APPOINT_INDICATOR,0) != NVL(:NEW.DM_APPOINT_INDICATOR,0) then
		audit_trail.column_update (raid, 'DM_APPOINT_INDICATOR',:OLD.DM_APPOINT_INDICATOR,:NEW.DM_APPOINT_INDICATOR);
	end if;
	IF NVL(:OLD.DELETED_FLAG,0) != NVL(:NEW.DELETED_FLAG,0) then
		audit_trail.column_update (raid, 'DELETED_FLAG',:OLD.DELETED_FLAG,:NEW.DELETED_FLAG);
	end if;
	IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') then
		audit_trail.column_update (raid, 'IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) then
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
			INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				old_modby := NULL;
		END ;
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
			INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				new_modby := NULL;
		 END ;
		audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
END;
/