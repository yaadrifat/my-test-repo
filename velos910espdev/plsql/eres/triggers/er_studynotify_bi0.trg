CREATE OR REPLACE TRIGGER "ER_STUDYNOTIFY_BI0" 
BEFORE INSERT
ON ER_STUDYNOTIFY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.rid is null or new.rid = 0
      )
Declare
  raid number(10);
  erid number(10);
  USR varchar2(100);

begin
  usr := getuser(:new.creator);
  select trunc(seq_rid.nextval)
  into erid from dual;
  :new.rid := erid ;
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_STUDYNOTIFY', erid, 'I', USR );
end;
/


