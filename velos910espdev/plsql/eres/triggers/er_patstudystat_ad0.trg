CREATE OR REPLACE TRIGGER "ER_PATSTUDYSTAT_AD0" 
  AFTER DELETE
  ON er_patstudystat
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_PATSTUDYSTAT', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_patstudystat) || '|' ||
  TO_CHAR(:OLD.fk_codelst_stat) || '|' ||
  TO_CHAR(:OLD.fk_per) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  TO_CHAR(:OLD.patstudystat_date) || '|' ||
  TO_CHAR(:OLD.patstudystat_endt) || '|' ||
  :OLD.patstudystat_note || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  TO_CHAR(:OLD.PATSTUDYSTAT_REASON) || '|' ||
  :OLD.SCREEN_NUMBER || '|' ||
  TO_CHAR(:OLD.SCREENED_BY) || '|' ||
  TO_CHAR(:OLD.SCREENING_OUTCOME) || '|' ||
  TO_CHAR(:OLD.NEXT_FOLLOWUP_ON) || '|' ||  -- Ankit Bug-11010
  TO_CHAR(:OLD.INFORM_CONSENT_VER) || '|' ||  -- Ankit Bug-11010
  TO_CHAR(:OLD.CURRENT_STAT);--KM

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END;
/