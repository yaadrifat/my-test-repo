CREATE OR REPLACE TRIGGER "ER_STUDY_SITE_RIGHTS_AU0" AFTER UPDATE OF
PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,RID,LAST_MODIFIED_BY,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD ON ER_STUDY_SITE_RIGHTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu for Audit Data After Update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction (raid, 'ER_STUDY_SITE_RIGHTS', :old.rid, 'U', usr);
   if nvl(:old.PK_STUDY_SITE_RIGHTS,0) !=
      NVL(:new.PK_STUDY_SITE_RIGHTS,0) then
      audit_trail.column_update
       (raid, 'PK_STUDY_SITE_RIGHTS',
       :old.PK_STUDY_SITE_RIGHTS, :new.PK_STUDY_SITE_RIGHTS);
   end if;

   if nvl(:old.FK_SITE,0) !=
      NVL(:new.FK_SITE,0) then
      audit_trail.column_update
       (raid, 'FK_SITE',
       :old.FK_SITE, :new.FK_SITE);
   end if;

   if nvl(:old.FK_STUDY,0) !=
      NVL(:new.FK_STUDY,0) then
      audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.FK_STUDY, :new.FK_STUDY);
   end if;

   if nvl(:old.FK_USER,0) !=
      NVL(:new.FK_USER,0) then
      audit_trail.column_update
       (raid, 'FK_USER',
       :old.FK_USER, :new.FK_USER);
   end if;

   if nvl(:old.USER_STUDY_SITE_RIGHTS,0) !=
      NVL(:new.USER_STUDY_SITE_RIGHTS,0) then
      audit_trail.column_update
       (raid, 'USER_STUDY_SITE_RIGHTS',
       :old.USER_STUDY_SITE_RIGHTS, :new.USER_STUDY_SITE_RIGHTS);
   end if;

   if nvl(:old.RID,0) !=
      NVL(:new.RID,0) then
      audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
   end if;

   if nvl(:old.LAST_MODIFIED_BY,0) !=
      NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
           Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	   into old_modby from er_user  where pk_user = :old.last_modified_by ;
	   Exception When NO_DATA_FOUND then
	   old_modby := null;
	End ;
	Begin
	   Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	   into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
	   Exception When NO_DATA_FOUND then
	   new_modby := null;
        End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    end if;

    if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
       NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
       audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

   if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
   end if;

end;
/


