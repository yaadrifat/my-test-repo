CREATE OR REPLACE TRIGGER "ER_STUDY_SITE_ADD_INFO_BI0" BEFORE INSERT ON ER_STUDY_SITE_ADD_INFO
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_STUDY_SITE_ADD_INFO',erid, 'I',usr);
   insert_data:=
:NEW.PK_STUDYSITES_ADDINFO||'|'||:NEW.FK_STUDYSITES||'|'||:NEW.FK_CODELST_SITEADDINFO||'|'||:NEW.STUDYSITE_DATA||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||:NEW.IP_ADD;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


