create or replace TRIGGER "ER_FORMQUERYSTATUS_BI0" BEFORE INSERT ON ER_FORMQUERYSTATUS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FORMQUERYSTATUS',erid, 'I',usr);
     -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMQUERYSTATUS||'|'|| :NEW.FK_FORMQUERY||'|'||
    :NEW.FORMQUERY_TYPE||'|'|| :NEW.FK_CODELST_QUERYTYPE||'|'||
    :NEW.QUERY_NOTES||'|'|| :NEW.ENTERED_BY||'|'|| TO_CHAR(:NEW.ENTERED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.FK_CODELST_QUERYSTATUS||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
   TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'|| :NEW.RID;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


