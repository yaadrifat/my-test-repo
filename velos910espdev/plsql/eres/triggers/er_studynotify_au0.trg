CREATE OR REPLACE TRIGGER ER_STUDYNOTIFY_AU0
AFTER UPDATE
ON ER_STUDYNOTIFY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_codetype varchar2(200) ;
  new_codetype varchar2(200) ;
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYNOTIFY', :old.rid, 'U', usr);
  if nvl(:old.pk_studynotify,0) !=
     NVL(:new.pk_studynotify,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYNOTIFY',
       :old.pk_studynotify, :new.pk_studynotify);
  end if;
  if nvl(:old.fk_codelst_tarea,0) !=
     NVL(:new.fk_codelst_tarea,0) then
    Begin
 select trim(codelst_desc) into old_codetype
 from er_codelst where pk_codelst =  :old.fk_codelst_tarea ;
    Exception When NO_DATA_FOUND then
     old_codetype := null ;
    End ;
    Begin
 select trim(codelst_desc) into new_codetype
 from er_codelst where pk_codelst =  :new.fk_codelst_tarea ;
    Exception When NO_DATA_FOUND then
     new_codetype := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_CODELST_TAREA',
       old_codetype, new_codetype);
  end if;
  if nvl(:old.fk_user,0) !=
     NVL(:new.fk_user,0) then
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.fk_user ;
    Exception When NO_DATA_FOUND then
     old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||','||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.fk_user ;
    Exception When NO_DATA_FOUND then
     new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_USER',
       old_modby, new_modby);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
    Exception When NO_DATA_FOUND then
     old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
    Exception When NO_DATA_FOUND then
     new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.STUDYNOTIFY_TYPE,' ') !=
     NVL(:new.STUDYNOTIFY_TYPE,' ') then
     audit_trail.column_update
       (raid, 'STUDYNOTIFY_TYPE',
       :old.STUDYNOTIFY_TYPE, :new.STUDYNOTIFY_TYPE);
  end if;

  if nvl(:old.STUDYNOTIFY_KEYWRDS,' ') !=
     NVL(:new.STUDYNOTIFY_KEYWRDS,' ') then
     audit_trail.column_update
       (raid, 'STUDYNOTIFY_KEYWRDS',
       :old.STUDYNOTIFY_KEYWRDS, :new.STUDYNOTIFY_KEYWRDS);
  end if;

  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
end;
/


