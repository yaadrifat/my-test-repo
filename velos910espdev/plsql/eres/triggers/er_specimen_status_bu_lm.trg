CREATE OR REPLACE TRIGGER "ER_SPECIMEN_STATUS_BU_LM" BEFORE UPDATE ON ER_SPECIMEN_STATUS
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
begin
:new.last_modified_date := sysdate;
end;
/


