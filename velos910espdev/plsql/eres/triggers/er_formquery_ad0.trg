CREATE OR REPLACE TRIGGER "ER_FORMQUERY_AD0" 
  AFTER DELETE
  ON er_formquery
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FORMQUERY', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_formquery) || '|' ||
  TO_CHAR(:OLD.fk_querymodule) || '|' ||
  TO_CHAR(:OLD.querymodule_linkedto) || '|' ||
  TO_CHAR(:OLD.fk_field) || '|' ||
  :OLD.query_name || '|' ||
  :OLD.query_desc || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


