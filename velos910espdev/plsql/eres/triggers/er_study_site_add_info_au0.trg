CREATE OR REPLACE TRIGGER "ER_STUDY_SITE_ADD_INFO_AU0" AFTER UPDATE OF
PK_STUDYSITES_ADDINFO,FK_STUDYSITES,FK_CODELST_SITEADDINFO,STUDYSITE_DATA,RID,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD ON ER_STUDY_SITE_ADD_INFO REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu for Audit Data After Update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction (raid, 'ER_STUDY_SITE_ADD_INFO', :old.rid, 'U', usr);
   if nvl(:old.PK_STUDYSITES_ADDINFO,0) !=
      NVL(:new.PK_STUDYSITES_ADDINFO,0) then
      audit_trail.column_update
       (raid, 'PK_STUDYSITES_ADDINFO',
       :old.PK_STUDYSITES_ADDINFO, :new.PK_STUDYSITES_ADDINFO);
   end if;

   if nvl(:old.FK_STUDYSITES,0) !=
      NVL(:new.FK_STUDYSITES,0) then
      audit_trail.column_update
       (raid, 'FK_STUDYSITES',
       :old.FK_STUDYSITES, :new.FK_STUDYSITES);
   end if;

   if nvl(:old.FK_CODELST_SITEADDINFO,0) !=
      NVL(:new.FK_CODELST_SITEADDINFO,0) then
      audit_trail.column_update
       (raid, 'FK_CODELST_SITEADDINFO',
       :old.FK_CODELST_SITEADDINFO, :new.FK_CODELST_SITEADDINFO);
   end if;

   if nvl(:old.STUDYSITE_DATA,' ') !=
      NVL(:new.STUDYSITE_DATA,' ') then
      audit_trail.column_update
       (raid, 'STUDYSITE_DATA',
       :old.STUDYSITE_DATA, :new.STUDYSITE_DATA);
   end if;

   if nvl(:old.RID,0) !=
      NVL(:new.RID,0) then
      audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
   end if;

   if nvl(:old.LAST_MODIFIED_BY,0) !=
      NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
           Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	   into old_modby from er_user  where pk_user = :old.last_modified_by ;
	   Exception When NO_DATA_FOUND then
	   old_modby := null;
	End ;
	Begin
	   Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	   into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
	   Exception When NO_DATA_FOUND then
	   new_modby := null;
        End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    end if;

    if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
       NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
       audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

   if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
   end if;

end;
/


