CREATE OR REPLACE TRIGGER "ER_ADD_AD0" 
AFTER DELETE
ON ER_ADD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
  deleted_addtype varchar2(200) ;
begin
  select seq_audit.nextval into raid from dual;
  select codelst_desc into deleted_addtype
   from er_codelst where pk_codelst = :old.fk_codelst_addtype ;
  audit_trail.record_transaction
    (raid, 'ER_ADD', :old.rid, 'D');
  deleted_data :=
  deleted_addtype || '|' ||
  to_char(:old.pk_add) || '|' ||
  :old.address || '|' ||
  :old.add_city || '|' ||
  :old.add_state || '|' ||
  :old.add_zipcode || '|' ||
  :old.add_country || '|' ||
  :old.add_county || '|' ||
  to_char(:old.add_eff_dt) || '|' ||
  :old.add_phone || '|' ||
  :old.add_email || '|' ||
  :old.add_pager || '|' ||
  :old.add_mobile || '|' ||
  :old.add_fax || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on)  || '|' ||
  :old.ip_add ;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


