CREATE OR REPLACE TRIGGER "ER_PATPROT_BI0" BEFORE INSERT ON ER_PATPROT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   usr := getuser(:NEW.creator);
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_PATPROT',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FK_CODELST_PTST_EVAL_FLAG||'|'||
    :NEW.FK_CODELST_PTST_EVAL||'|'|| :NEW.FK_CODELST_PTST_INEVAL||'|'||
    :NEW.FK_CODELST_PTST_SURVIVAL||'|'|| :NEW.FK_CODELST_PTST_DTH_STDREL||'|'||
    :NEW.DEATH_STD_RLTD_OTHER||'|'||:NEW.INFORM_CONSENT_VER||'|'||
     TO_CHAR(:NEW.NEXT_FOLLOWUP_ON,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.DATE_OF_DEATH,PKG_DATEUTIL.f_get_dateformat)||'|'||
  :NEW.PATPROT_RANDOM||'|'|| :NEW.PATPROT_PHYSICIAN||'|'||
  :NEW.PK_PATPROT||'|'|| :NEW.FK_PROTOCOL||'|'|| :NEW.FK_STUDY||'|'||
   :NEW.FK_USER||'|'|| TO_CHAR(:NEW.PATPROT_ENROLDT,PKG_DATEUTIL.f_get_dateformat)||'|'|| :NEW.FK_PER||'|'||
   TO_CHAR(:NEW.PATPROT_START,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.PATPROT_NOTES||'|'||
    :NEW.PATPROT_STAT||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.IP_ADD||'|'|| TO_CHAR(:NEW.PATPROT_DISCDT,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.PATPROT_REASON||'|'|| :NEW.PATPROT_SCHDAY||'|'|| :NEW.FK_TIMEZONE||'|'||
    :NEW.PATPROT_PATSTDID||'|'|| :NEW.PATPROT_CONSIGN||'|'||
     TO_CHAR(:NEW.PATPROT_CONSIGNDT,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.PATPROT_RESIGNDT1,PKG_DATEUTIL.f_get_dateformat)||'|'||
      TO_CHAR(:NEW.PATPROT_RESIGNDT2,PKG_DATEUTIL.f_get_dateformat)||'|'|| :NEW.FK_USERASSTO||'|'||
    :NEW.FK_CODELSTLOC ||'|'|| :new.FK_SITE_ENROLLING ||'|'|| :new.PATPROT_TREATINGORG;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


