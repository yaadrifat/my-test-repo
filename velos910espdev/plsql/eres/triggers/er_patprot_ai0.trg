CREATE OR REPLACE TRIGGER "ER_PATPROT_AI0" 
AFTER INSERT
ON ER_PATPROT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  v_new_fk_study NUMBER;
  v_new_fk_protocol NUMBER;
  v_new_pk_patprot  NUMBER;
  v_new_creator NUMBER;
  v_new_created_on DATE;
  v_new_ip_add  VARCHAR2 (15) ;



/**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/06/2002
   ** insert default alert notifications for patient enrollment
   ** according to the study's global settings - global flag and edit lock
*/

BEGIN

  v_new_fk_study := :NEW.FK_STUDY;
  v_new_fk_protocol :=  :NEW.FK_PROTOCOL ;
  v_new_pk_patprot  := :NEW.PK_PATPROT;
  v_new_creator := :NEW.CREATOR;
  v_new_created_on := :NEW.CREATED_ON;
  v_new_ip_add :=  :NEW.IP_ADD;

  pkg_alnot.set_alertnotify_for_enrollment(v_new_fk_study ,  v_new_fk_protocol,  v_new_pk_patprot , v_new_creator , v_new_created_on , v_new_ip_add );

 -- call procedure to create portal auto login if exists

 pkg_portal.sp_create_deflogin('S', :new.fk_per,:new.fk_study,
  0,:new.creator,:new.ip_add  );

END;
/


