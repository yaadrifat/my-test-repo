CREATE OR REPLACE TRIGGER "ER_STATUS_HISTORY_AD0" 
  AFTER UPDATE--DELETE
  ON ER_STATUS_HISTORY
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_STATUS_HISTORY', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_status) || '|' ||
  TO_CHAR(:OLD.status_modpk) || '|' ||
  :OLD.status_modtable || '|' ||
  TO_CHAR(:OLD.fk_codelst_stat) || '|' ||
  TO_CHAR(:OLD.status_date) || '|' ||
  TO_CHAR(:OLD.status_end_date) || '|' ||
  :OLD.status_notes || '|' ||
  :OLD.status_custom1 || '|' ||
  -- Added the column status_iscurrent for july-august enhancement #S4
  TO_CHAR(:OLD.status_iscurrent) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


