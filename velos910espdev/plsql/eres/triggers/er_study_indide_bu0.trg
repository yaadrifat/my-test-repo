CREATE OR REPLACE TRIGGER "ERES"."ER_STUDY_INDIDE_BU0"
BEFORE UPDATE
ON ERES.ER_STUDY_INDIDE
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW WHEN ( NEW.last_modified_by is not null) 
BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/