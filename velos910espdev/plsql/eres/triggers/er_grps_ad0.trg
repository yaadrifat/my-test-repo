CREATE OR REPLACE TRIGGER "ER_GRPS_AD0" AFTER DELETE ON ER_GRPS
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_GRPS', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_grp) || '|' ||
  to_char(:old.fk_account) || '|' ||
  :old.grp_name || '|' ||
  :old.grp_desc || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.grp_rights || '|' ||
  :old.ip_add || '|' ||
  :old.GRP_SUPUSR_RIGHTS  || '|' ||
   :old.GRP_SUPUSR_FLAG || '|' ||
   :old.GRP_HIDDEN || '|' ||
   :old.FK_CODELST_ST_ROLE;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


