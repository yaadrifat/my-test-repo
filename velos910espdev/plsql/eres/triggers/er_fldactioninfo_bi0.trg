create or replace TRIGGER "ER_FLDACTIONINFO_BI0" BEFORE INSERT ON ER_FLDACTIONINFO
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FLDACTIONINFO',erid, 'I',usr);
   insert_data:= :NEW.PK_FLDACTIONINFO||'|'||:NEW.FK_FLDACTION||'|'||:NEW.INFO_TYPE||'|'||:NEW.INFO_VALUE||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


