CREATE OR REPLACE TRIGGER "ER_STUDYTXARM_AD0" 
AFTER DELETE
ON ER_STUDYTXARM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYTXARM', :OLD.rid, 'D');
  deleted_data :=
  TO_CHAR(:OLD.pk_studytxarm) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  :OLD.tx_name || '|' ||
  :OLD.tx_drug_info || '|' ||
  :OLD.tx_desc || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_on) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;
INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


