CREATE OR REPLACE TRIGGER ERES.ER_STUDY_INDIDE_AU0
AFTER UPDATE OF PK_STUDY_INDIIDE,FK_STUDY,INDIDE_TYPE,INDIDE_NUMBER,FK_CODELST_INDIDE_GRANTOR,FK_CODELST_INDIDE_HOLDER,FK_CODELST_PROGRAM_CODE,
INDIDE_EXPAND_ACCESS,FK_CODELST_ACCESS_TYPE,INDIDE_EXEMPT,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE
ON ERES.ER_STUDY_INDIDE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);
	old_modby VARCHAR2(100);
	new_modby VARCHAR2(100);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);
	audit_trail.record_transaction
	(raid, 'ER_STUDY_INDIDE', :OLD.rid, 'U', usr);

	IF NVL(:OLD.PK_STUDY_INDIIDE,0) != NVL(:NEW.PK_STUDY_INDIIDE,0) then
		audit_trail.column_update (raid, 'PK_STUDY_INDIIDE',:OLD.PK_STUDY_INDIIDE,:NEW.PK_STUDY_INDIIDE);
	end if;
	IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) then
		audit_trail.column_update (raid, 'FK_STUDY',:OLD.FK_STUDY,:NEW.FK_STUDY);
	end if;
	IF NVL(:OLD.INDIDE_TYPE,0) != NVL(:NEW.INDIDE_TYPE,0) then
		audit_trail.column_update (raid, 'INDIDE_TYPE',:OLD.INDIDE_TYPE,:NEW.INDIDE_TYPE);
	end if;
	IF NVL(:OLD.INDIDE_NUMBER,' ') != NVL(:NEW.INDIDE_NUMBER,' ') then
		audit_trail.column_update (raid, 'INDIDE_NUMBER',:OLD.INDIDE_NUMBER,:NEW.INDIDE_NUMBER);
	end if;
	IF NVL(:OLD.FK_CODELST_INDIDE_GRANTOR,0) != NVL(:NEW.FK_CODELST_INDIDE_GRANTOR,0) then
		audit_trail.column_update (raid, 'FK_CODELST_INDIDE_GRANTOR',:OLD.FK_CODELST_INDIDE_GRANTOR,:NEW.FK_CODELST_INDIDE_GRANTOR);
	end if;
	IF NVL(:OLD.FK_CODELST_INDIDE_HOLDER,0) != NVL(:NEW.FK_CODELST_INDIDE_HOLDER,0) then
		audit_trail.column_update (raid, 'FK_CODELST_INDIDE_HOLDER',:OLD.FK_CODELST_INDIDE_HOLDER,:NEW.FK_CODELST_INDIDE_HOLDER);
	end if;
	IF NVL(:OLD.FK_CODELST_PROGRAM_CODE,0) != NVL(:NEW.FK_CODELST_PROGRAM_CODE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_PROGRAM_CODE',:OLD.FK_CODELST_PROGRAM_CODE,:NEW.FK_CODELST_PROGRAM_CODE);
	end if;
	IF NVL(:OLD.INDIDE_EXPAND_ACCESS,0) != NVL(:NEW.INDIDE_EXPAND_ACCESS,0) then
		audit_trail.column_update (raid, 'INDIDE_EXPAND_ACCESS',:OLD.INDIDE_EXPAND_ACCESS,:NEW.INDIDE_EXPAND_ACCESS);
	end if;
	IF NVL(:OLD.FK_CODELST_ACCESS_TYPE,0) != NVL(:NEW.FK_CODELST_ACCESS_TYPE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_ACCESS_TYPE',:OLD.FK_CODELST_ACCESS_TYPE,:NEW.FK_CODELST_ACCESS_TYPE);
	end if;
	IF NVL(:OLD.INDIDE_EXEMPT,0) != NVL(:NEW.INDIDE_EXEMPT,0) then
		audit_trail.column_update (raid, 'INDIDE_EXEMPT',:OLD.INDIDE_EXEMPT,:NEW.INDIDE_EXEMPT);
	end if;
	IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') then
		audit_trail.column_update (raid, 'IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD);
	end if;

	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) then
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
			INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				old_modby := NULL;
		END ;
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
			INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				new_modby := NULL;
		 END ;
		audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
END;
/
