CREATE OR REPLACE TRIGGER "ER_DYNREPFILTER_AD0" AFTER DELETE ON ER_DYNREPFILTER
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_DYNREPFILTER', :old.rid, 'D');

  deleted_data :=
  to_char(:old.PK_DYNREPFILTER) || '|' ||
  to_char(:old.FK_DYNREP) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  :old.DYNREPFILTER_NAME || '|' ||
  :old.DYNREPFILTER_FILTER || '|' ||
  to_char(:old.FK_FORM) || '|' ||
  to_char(:old.FK_PARENTFILTER) || '|' ||
  :old.FORM_TYPE || '|' ||
  :old.DATERANGE_TYPE || '|' ||
  :old.DATERANGE_FROM || '|' ||
  :old.DATERANGE_TO ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


