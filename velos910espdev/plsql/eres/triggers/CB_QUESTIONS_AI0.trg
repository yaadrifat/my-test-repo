CREATE OR REPLACE TRIGGER "ERES"."CB_QUESTIONS_AI0" AFTER INSERT ON CB_QUESTIONS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW                           
DECLARE 
	V_ROWID NUMBER(10);

BEGIN
   SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;
	
	  PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (V_ROWID, 'CB_QUESTIONS', :NEW.RID,:NEW.PK_QUESTIONS,'I',:NEW.CREATOR);        
	IF :NEW.PK_QUESTIONS IS NOT NULL THEN
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','PK_QUESTIONS',NULL,:NEW.PK_QUESTIONS,NULL,NULL);
    END IF;
    IF :NEW.QUES_DESC IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','QUES_DESC',NULL,:NEW.QUES_DESC,NULL,NULL);
    END IF;	
   IF :NEW.QUES_HELP IS NOT NULL THEN
    PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','QUES_HELP',NULL,:NEW.QUES_HELP,NULL,NULL);
    END IF;  
     IF :NEW.UNLICEN_REQ_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','UNLICEN_REQ_FLAG',NULL,:NEW.UNLICEN_REQ_FLAG,NULL,NULL);
    END IF;
    IF :NEW.LICEN_REQ_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','LICEN_REQ_FLAG',NULL,:NEW.LICEN_REQ_FLAG,NULL,NULL);
    END IF;
    IF :NEW.UNLICN_PRIOR_TO_SHIPMENT_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','UNLICN_PRIOR_TO_SHIPMENT_FLAG',NULL,:NEW.UNLICN_PRIOR_TO_SHIPMENT_FLAG,NULL,NULL);
    END IF;
    IF :NEW.LICEN_PRIOR_SHIPMENT_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','LICEN_PRIOR_SHIPMENT_FLAG',NULL,:NEW.LICEN_PRIOR_SHIPMENT_FLAG,NULL,NULL);
    END IF;
    IF :NEW.CBB_NOT_USE_SYSTEM IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','CBB_NOT_USE_SYSTEM',NULL,:NEW.CBB_NOT_USE_SYSTEM,NULL,NULL);
    END IF;
    IF :NEW.QUES_HOVER IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','QUES_HOVER',NULL,:NEW.QUES_HOVER,NULL,NULL);
    END IF;
    IF :NEW.ADD_COMMENT_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','ADD_COMMENT_FLAG',NULL,:NEW.ADD_COMMENT_FLAG,NULL,NULL);
    END IF;
    IF :NEW.ASSESMENT_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','ASSESMENT_FLAG',NULL,:NEW.ASSESMENT_FLAG,NULL,NULL);
    END IF;
    IF :NEW.FK_MASTER_QUES IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','FK_MASTER_QUES',NULL,:NEW.FK_MASTER_QUES,NULL,NULL);
    END IF;	
    IF :NEW.FK_DEPENDENT_QUES IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','FK_DEPENDENT_QUES',NULL,:NEW.FK_DEPENDENT_QUES,NULL,NULL);
    END IF;	
     IF :NEW.FK_DEPENDENT_QUES_VALUE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','FK_DEPENDENT_QUES_VALUE',NULL,:NEW.FK_DEPENDENT_QUES_VALUE,NULL,NULL);
    END IF;	
     IF :NEW.RESPONSE_TYPE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','RESPONSE_TYPE',NULL,:NEW.RESPONSE_TYPE,NULL,NULL);
    END IF;	
     IF :NEW.QUES_CODE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','QUES_CODE',NULL,:NEW.QUES_CODE,NULL,NULL);
    END IF;	
     IF :NEW.QUES_SEQ IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_QUESTIONS','QUES_SEQ',NULL,:NEW.QUES_SEQ,NULL,NULL);
    END IF;	
   IF :NEW.CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
    END IF;	
	IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;	
	IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;	
	IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;	
	IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;	
	IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;	
	IF :NEW.DELETEDFLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_QUESTIONS','DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;	
END;
/