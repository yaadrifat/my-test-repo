CREATE OR REPLACE TRIGGER "ER_STUDY_BI2" 
BEFORE INSERT
ON ER_STUDY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_autogen number;
v_studynumber varchar2(100);
begin
  if :new.study_number is null then
      select ac_autogen_study into v_autogen from er_account where pk_account = :new.fk_account;
      if v_autogen = 1 then
        sp_autogen_study(:new.fk_account,v_studynumber) ;
        :new.study_number := v_studynumber;
      end if;
  end if;
end;
/


