/*
 * Classname				AuditReport
 * 
 * Version information		1.0
 *
 * Copyright notice			Aithent Technologies Pvt Ltd
 */

package com.aithent.audittrail.reports;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * A class to be used to be used for generating reports of Audit Trail Data.
 * This class assumes that the Audit Trail has been implemented as per Aithent
 * standards in the application using this class.
 * 
 * @see
 * @see
 * @version 1.0 24 Oct, 2001
 * @author Sajal
 */

public class AuditReport {

    public AuditReport() {

    }

    public static void main(String[] args) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = EJBUtil.getConnection();
            pstmt = conn
                    .prepareStatement("Select count(*) count from er_account");
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            int count = rs.getInt("count");
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }

    }

    public String getXML(String repNum, String repParams) {
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";
        int i = 0;

        try {

            conn = EJBUtil.getConnection();
            cstmt = conn.prepareCall("{call SP_AUDREP(?,?,?) }");
            Rlog.debug("auditreport", "In getXML in AuditReport line number 2");
            cstmt.setInt(1, EJBUtil.stringToNum(repNum));
            cstmt.setString(2, repParams);
            cstmt.registerOutParameter(3, java.sql.Types.CLOB);
            cstmt.execute();

            XmlClob = cstmt.getClob(3);

            if (!(XmlClob == null)) {

                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());

                if ((XmlString.length()) == 34) { // no data found
                    XmlString = null;
                }

            }

            Rlog.debug("auditreport", "In getXML in AuditReport line number 7");

        } catch (SQLException ex) {
            Rlog.fatal("auditreport", "getXML in AuditReport EXCEPTION" + ex
                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return XmlString;

    }

    public String getReportOutput(String repNum, String repParams) {

        String repHTML = "";
        String xslPath = "";
        String repXml = getXML(repNum, repParams);

        if (repXml == null) {
            return "No Data Found";
        }

        try {
            xslPath = EJBUtil.getEnvVariable("AUDITREPORT_HOME")
                    + Configuration.getXSLName(repNum);

            TransformerFactory tFactory = TransformerFactory.newInstance();

            Reader mR = new StringReader(repXml);

            Source xmlSource = new StreamSource(mR);

            // Reader sR1=new StringReader(xslPath);

            File xslFile = new File(xslPath);

            Source xslSource = new StreamSource(xslFile);

            if (xslSource == null) {
                System.out
                        .println("*/*/*/*/*/*/*/*/*/*/*/*/*/*/*getReportOutput line 1");
            }
            Transformer transformer = tFactory.newTransformer(xslSource);

            Writer writer = new StringWriter();
            if (xslSource == null) {
                System.out
                        .println("*/*/*/*/*/*/*/*/*/*/*/*/*/*/*getReportOutput line 2");
            }

            transformer.transform(xmlSource, new StreamResult(writer));

            StringWriter strWr = (StringWriter) writer;

            repHTML = strWr.toString();

        } catch (Exception e) {
            Rlog.fatal("auditreport", "AuditReport.getReportHTML EXCEPTION "
                    + e);
        }
        return repHTML;
    }

    public String getReportHTML(String repNum, String dateFrom, String dateTo,
            String action, String userId, String accountId) {
        if (dateFrom != null)
            dateFrom = DateUtil.formatChange(dateFrom, "dd-MMM-yyyy");
        if (dateTo != null)
            dateTo = DateUtil.formatChange(dateTo, "dd-MMM-yyyy");
        String repParams = "";
        int reportNum = EJBUtil.stringToNum(repNum);

        switch (reportNum) {
        case 1:
            repParams = dateFrom + "," + dateTo + "," + accountId;
            break;
        case 2:
            repParams = action;
            break;
        case 3:
            repParams = action + "," + userId;
            break;
        case 4:
            repParams = action + "," + dateFrom + "," + dateTo + ","
                    + accountId;
            break;
        case 5:
            repParams = action + "," + userId + "," + dateFrom + "," + dateTo;
            break;
        case 6:
            repParams = userId;
            break;
        case 7:
            repParams = userId + "," + dateFrom + "," + dateTo;
            break;
        case 51:
            repParams = dateFrom + "," + dateTo;
            break;
        case 54:
            repParams = action + "," + dateFrom + "," + dateTo;
            break;
        }

        String repHTML = getReportOutput(repNum, repParams);

        return repHTML;
    }

}
