/**
 * This class reads the configuration parameters from xml files and stores
 * in the staic variables. When starting up the function of this class
 * is called to setup the values.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

/**
 * This class reads the configuration parameters from xml files and stores in
 * the staic variables. Any function requiring these values access it from this
 * class directly.
 */

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Configuration {

    /**
     * Stores the Database Driver being used
     */
    public static String DBDriverName;

    /**
     * Stores the database URL or the IP Address
     */
    public static String DBUrl;

    /**
     * Stores the database login Id
     */
    public static String DBUser;

    /**
     * Stores the database password
     */
    public static String DBPwd;

    /**
     * Stores the value of the environment variable FILE_UPLOAD_DOWNLOAD
     */
    public static String FILE_UPLOAD_DOWNLOAD;

    /**
     * Stores the the path of the folder where the file should be saved
     * temporarily before being saved in the database
     */
    public static String UPLOADFOLDER;

    /**
     * Stores the path of the folder where the file is temporarily stored while
     * downloading the same from the database
     */
    public static String DOWNLOADFOLDER;

    /**
     * Stores the address of Upload servlet
     */
    public static String UPLOADSERVLET;

    /**
     * Stores the address of Download servlet
     */
    public static String DOWNLOADSERVLET;

    /**
     * Reads the XML file and sets the value of the static variables. Makes call
     * to readDBParam method to accomplish this.
     * 
     */
    public static void readSettings(String db) {
        String fileName = null;
        String eHome = null;
        try {
            eHome = EJBUtil.getEnvVariable("FILE_UPLOAD_DOWNLOAD");
            eHome = (eHome == null) ? "" : eHome;
            System.out.println("Configuration.readSettings eHome" + eHome);
            if (eHome.trim().equals("%FILE_UPLOAD_DOWNLOAD%"))
                eHome = System.getProperty("FILE_UPLOAD_DOWNLOAD");
            eHome = eHome.trim();
            FILE_UPLOAD_DOWNLOAD = eHome;
            fileName = eHome + "fileUploadDownload.xml";
            System.out
                    .println("Configuration.readSettings fileName" + fileName);
            Document root = DOMUtil.readDocument(fileName);
            readDBParam(root, db);
        } catch (IOException ie) {
            System.out.println("io ex" + ie);
            ie.printStackTrace();
        } catch (SAXException se) {
            System.out.println("sax ex" + se);
            se.printStackTrace();
        } catch (Exception e) {
            System.out.println("e ex" + e);
            e.printStackTrace();
        }
    }

    /**
     * Reads the XML file for database connection parameters and sets the value
     * of corresponding static variables.
     * 
     * @param root
     *            a Document type object containing the XML consisiting of
     *            Database parameters
     */
    private static void readDBParam(Document root, String db) {
        Node dbNode = null;

        dbNode = DOMUtil.findNode(root, "DBURL");
        dbNode = DOMUtil.findNode(dbNode, "DB");
        dbNode = DOMUtil.findNode(dbNode, db);

        DBDriverName = DOMUtil.findNode(dbNode, "DRIVER").getFirstChild()
                .getNodeValue();
        DBUrl = DOMUtil.findNode(dbNode, "DB_URL").getFirstChild()
                .getNodeValue();
        DBUser = DOMUtil.findNode(dbNode, "USERID").getFirstChild()
                .getNodeValue();
        DBPwd = DOMUtil.findNode(dbNode, "PWD").getFirstChild().getNodeValue();
    }

    /**
     * Reads the XML file for parameters for upload and download and sets the
     * value of corresponding static variables.
     * 
     * @param file
     *            a String value containing the absolute path of the file that
     *            contains the parameters for Upload and Download
     */
    public static void readUploadDownloadParam(String file, String module) {
        try {
            Document root = DOMUtil.readDocument(file);

            if (!(module == null) && (!module.trim().equals("null"))) {
                Node fileupload = DOMUtil.findNode(root, "fileupload");
                fileupload = DOMUtil.findNode(fileupload, module);

                Node name = DOMUtil.findNode(fileupload, "name");
                String name_val = (name.getFirstChild()).getNodeValue();
                if (name_val.equals("folder")) {
                    UPLOADFOLDER = DOMUtil.findNode(fileupload, "value")
                            .getFirstChild().getNodeValue();
                }
                Node us = DOMUtil.findNode(fileupload, "servlet");
                UPLOADSERVLET = (us.getFirstChild()).getNodeValue();
            }

            Node filedownload = DOMUtil.findNode(root, "filedownload");
            Node name1 = DOMUtil.findNode(filedownload, "name");
            String name1_val = (name1.getFirstChild()).getNodeValue();
            if (name1_val.equals("folder")) {
                DOWNLOADFOLDER = DOMUtil.findNode(filedownload, "value")
                        .getFirstChild().getNodeValue();
            }
            Node ds = DOMUtil.findNode(filedownload, "servlet");
            DOWNLOADSERVLET = (ds.getFirstChild()).getNodeValue();
            /*
             * System.out.println("before getting jsppath"); Node
             * jsppath=DOMUtil.findNode(appendix,"jsppath");
             * JSPPATH=(jsppath.getFirstChild()).getNodeValue();
             * .debug("common","jsp path="+JSPPATH);
             */
        } catch (IOException ie) {
            System.out.println("io ex" + ie);
        } catch (SAXException se) {
            System.out.println("sax ex" + se);
        }
    }

}