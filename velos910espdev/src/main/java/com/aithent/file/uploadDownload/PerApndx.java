/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.velos.eres.web.perApndx.PerApndxJB;
import com.velos.eres.web.user.UserJB;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 * 
 */
public class PerApndx extends FileUploadServlet {

    public void init(ServletConfig sc) throws ServletException {

    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     * 
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     * 
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     * 
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
    	if (!req.getContentType().toLowerCase().startsWith(
                "multipart/form-data")) {
            // Since this servlet only handles File Upload, bail out
            // Note: this isn't strictly legal - I should send
            // a custom error message
            return;
        }
        int ind = req.getContentType().indexOf("boundary=");
        if (ind == -1) {
            return;
        }
        String boundary = req.getContentType().substring(ind + 9);
        if (boundary == null) {
            return;
        }
        try {
            table = parseMulti(boundary, req.getInputStream(), out);
        	
        } catch (Throwable t) {
            t.printStackTrace(new PrintStream(res.getOutputStream()));
            return;
        }
        
        //Added by Manimaran to fix the issue 3044.
        
        String file_name = null;
        for (Enumeration fields = table.keys(); fields.hasMoreElements();) {
            String name = (String) fields.nextElement();
            Object obj = table.get(name);
            if (obj instanceof Hashtable) {
                // its a file!
                Hashtable filehash = (Hashtable) obj;
                file_name = (String) filehash.get("filename");
            }
        }
                
        String[] eSign = new String[1];
        String[] userId = new String[1];
        String[] nextPage = new String[1];
        String[] ipAdd = new String[1];//KM

        eSign = (String[]) table.get("eSign");
        userId = (String[]) table.get("userId");
        nextPage = (String[]) table.get("nextPage");
        ipAdd = (String[]) table.get("ipAdd");
        
        UserJB userB = new UserJB();
        userB.setUserId(EJBUtil.stringToNum(userId[0]));
        userB.getUserDetails();
        String actualESign = userB.getUserESign();
        out = res.getOutputStream();

        if (eSign[0].equals(actualESign)) {

            System.out.println("PerApndx.doPost line 1");
            PerApndxJB perApndxJB = new PerApndxJB();
            System.out.println("PerApndx.doPost line 2");
        
            String[] desc = (String[]) table.get("desc");
            String[] type = (String[]) table.get("type");
            // String[] milestoneId = (String[])table.get("milestoneId");
            String[] perId = (String[]) table.get("patId");
            String[] perDate = (String[]) table.get("perApndxDate");
           
            System.out.println("PerApndx.doPost line 10");
            //perApndxJB.setPerApndxId(perApndxId);
            System.out.println("PerApndx.doPost line 11");
            //perApndxJB.getPerApndxDetails();
            System.out.println("PerApndx.doPost line 12");
            perApndxJB.setPerApndxDesc(desc[0]);
            System.out.println("PerApndx.doPost line 13");
            perApndxJB.setPerApndxType(type[0]);
            System.out.println("PerApndx.doPost line 14");
            perApndxJB.setPerApndxPerId(perId[0]);
            perApndxJB.setPerApndxDate(perDate[0]);
            // perApndxJB.setMilestoneId(milestoneId[0]);
            perApndxJB.setPerApndxUri(file_name);
            perApndxJB.setCreator(userId[0]);//KM
            perApndxJB.setIpAdd(ipAdd[0]);//KM

            int perApndxId = perApndxJB.setPerApndxDetails();
            System.out.println("PerApndx.doPost line 3");

            pkBase[0] = String.valueOf(perApndxId);
            System.out.println("PerApndx.doPost line 4");

            System.out.println("in side do post of PerApndxdsdsdsdsds");
            System.out.println(pkBase[0]);
            System.out.println("PerApndx.doPost line 5");
            super.doPost(req, res);
            if (deleteFlag == false) {
            
            	/*System.out.println("PerApndx.doPost line 6");
                String[] desc = (String[]) table.get("desc");
                System.out.println("PerApndx.doPost line 7");
                String[] type = (String[]) table.get("type");
                System.out.println("PerApndx.doPost line 8");
                // String[] milestoneId = (String[])table.get("milestoneId");
                String[] perId = (String[]) table.get("patId");
                System.out.println("PerApndx.doPost line 9");
                String[] perDate = (String[]) table.get("perApndxDate");
                
                System.out.println(file_name);
                System.out.println("PerApndx.doPost line 10"); 
                perApndxJB.setPerApndxId(perApndxId);
                System.out.println("PerApndx.doPost line 11");
                perApndxJB.getPerApndxDetails();
                
                System.out.println("PerApndx.doPost line 12");
                perApndxJB.setPerApndxDesc(desc[0]);
                System.out.println("PerApndx.doPost line 13");
                perApndxJB.setPerApndxType(type[0]);
                System.out.println("PerApndx.doPost line 14");
                perApndxJB.setPerApndxPerId(perId[0]);
                perApndxJB.setPerApndxDate(perDate[0]);
                // perApndxJB.setMilestoneId(milestoneId[0]);
                perApndxJB.setPerApndxUri(file_name);
                perApndxJB.updatePerApndx(); */
                
            } else {
                perApndxJB.removePerApndx(perApndxId);
            }
            //	
            // System.out.println(desc[0]);
            // System.out.println(type[0]);
            // System.out.println(studyId[0]);
            // System.out.println(milestoneId[0]);
            // System.out.println(file_name);

            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh CONTENT=\"0;"+path+"\""); out.println("</BODY>");
             * out.println("</HTML>");
             */
            res.sendRedirect("../../" + path);
        } else {
            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh
             * CONTENT=\"0;URL=../../eres/jsp/incorrectesign.jsp\"");
             * out.println("</BODY>"); out.println("</HTML>");
             */
            res.sendRedirect("../../velos/jsp/incorrectesign.jsp");
        }

    }

    /**
     * 
     * 
     * Obtain information on this servlet.
     * 
     * 
     * @return String describing this servlet.
     * 
     * 
     */

    public String getServletInfo() {

        return "File upload servlet -- used to receive files";

    }

}
