package com.velos.login;
import java.io.Serializable;

public class ConnectObj implements Serializable {
	private String userName;

	private String password;

	private String Protocol;

	private String host;

	private int port;

	private String authMode;
	
	private String velosUser;

	/**
	 * @return the velosUser
	 */
	public String getVelosUser() {
		return velosUser;
	}

	/**
	 * @param velosUser the velosUser to set
	 */
	public void setVelosUser(String velosUser) {
		this.velosUser = velosUser;
	}

	public String getAuthMode() {
		return authMode;
	}

	public void setAuthMode(String authMode) {
		this.authMode = authMode;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getProtocol() {
		return Protocol;
	}

	public void setProtocol(String protocol) {
		Protocol = protocol;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
