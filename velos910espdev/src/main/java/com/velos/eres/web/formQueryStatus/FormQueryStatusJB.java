/**
 * Classname : FormQueryStatusJB
 * 
 * Version information: 1.0
 *
 * Date: 01/03/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.formQueryStatus;

/* IMPORT STATEMENTS */

import com.velos.eres.business.formQueryStatus.impl.FormQueryStatusBean;
import com.velos.eres.service.formQueryStatusAgent.FormQueryStatusAgentRObj;
import com.velos.eres.service.formQueryStatusAgent.impl.FormQueryStatusAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for FormQueryStatus
 * 
 * @author Anu Khanna
 * @version 1.0 01/03/2005
 */

public class FormQueryStatusJB {

    /**
     * The primary key:
     */

    private int formQueryStatusId;

    /**
     * form query id
     */
    private String formQueryId;

    /**
     * form query type
     */
    private String formQueryType;

    /**
     * query type id
     */
    private String queryTypeId;

    /**
     * query notes
     */

    private String queryNotes;

    /**
     * query entered by
     */
    private String enteredBy;

    /**
     * query entered on
     */

    private String enteredOn;

    /**
     * query status id
     */

    private String queryStatusId;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * Get the form Query Status Id
     * 
     * @return int formQueryStatusId
     */
    public int getFormQueryStatusId() {
        return this.formQueryStatusId;
    }

    /**
     * sets formQueryStatusId
     * 
     * @param formQueryStatusId
     */
    public void setFormQueryStatusId(int formQueryStatusId) {
        this.formQueryStatusId = formQueryStatusId;
    }

    /**
     * Get the form Query Id
     * 
     * @return int formQueryId
     */
    public String getFormQueryId() {
        return this.formQueryId;
    }

    /**
     * sets formQueryId
     * 
     * @param formQueryId
     */
    public void setFormQueryId(String formQueryId) {
        this.formQueryId = formQueryId;
    }

    /**
     * Get form Query Type
     * 
     * @return String formQueryType
     */

    public String getFormQueryType() {
        return this.formQueryType;
    }

    /**
     * sets formQueryType
     * 
     * @param formQueryType
     */
    public void setFormQueryType(String formQueryType) {
        this.formQueryType = formQueryType;
    }

    /**
     * Get query Type Id
     * 
     * @return String queryTypeId
     */

    public String getQueryTypeId() {
        return this.queryTypeId;
    }

    /**
     * sets query Type Id
     * 
     * @param queryTypeId
     */
    public void setQueryTypeId(String queryTypeId) {
        this.queryTypeId = queryTypeId;
    }

    /**
     * Get query Notes
     * 
     * @return String queryNotes
     */

    public String getQueryNotes() {
        return this.queryNotes;
    }

    /**
     * sets query Notes
     * 
     * @param queryNotes
     */
    public void setQueryNotes(String queryNotes) {
        this.queryNotes = queryNotes;
    }

    /**
     * Get query entered By
     * 
     * @return String enteredBy
     */

    public String getEnteredBy() {
        return this.enteredBy;
    }

    /**
     * sets query entered By
     * 
     * @param enteredBy
     */

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;

    }

    /**
     * Get query entered on
     * 
     * @return String enteredOn
     */

    public String getEnteredOn() {
        return this.enteredOn;
    }

    /**
     * sets query entered on
     * 
     * @param enteredOn
     */

    public void setEnteredOn(String enteredOn) {
        this.enteredOn = enteredOn;

    }

    /**
     * Get query Status Id
     * 
     * @return String queryStatusId
     */

    public String getQueryStatusId() {
        return this.queryStatusId;
    }

    /**
     * sets query Status Id
     * 
     * @param queryStatusId
     */

    public void setQueryStatusId(String queryStatusId) {
        this.queryStatusId = queryStatusId;

    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param formQueryStatusId:
     *            Unique Id constructor
     * 
     */

    public FormQueryStatusJB(int formQueryStatusId) {
        setFormQueryStatusId(formQueryStatusId);
    }

    /**
     * Default Constructor
     */

    public FormQueryStatusJB() {
        Rlog.debug("formQueryStatus", "FormQueryStatusJB.FormQueryStatusJB() ");
    }

    public FormQueryStatusJB(int formQueryStatusId, String formQueryId,
            String formQueryType, String queryTypeId, String queryNotes,
            String enteredBy, String enteredOn, String queryStatusId,
            String creator, String modifiedBy, String ipAdd) {
        setFormQueryStatusId(formQueryStatusId);
        setFormQueryId(formQueryId);
        setFormQueryType(formQueryType);
        setQueryTypeId(queryTypeId);
        setQueryNotes(queryNotes);
        setEnteredBy(enteredBy);
        setEnteredOn(enteredOn);
        setQueryStatusId(queryStatusId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getFormQueryStatusDetails of FormQueryStatusId Session Bean:
     * FormQueryStatusAgentBean
     * 
     * @return FormQueryStatusStatKeeper
     * @see FormQueryStatusAgentBean
     */
    public FormQueryStatusBean getFormQueryStatusDetails() {
        FormQueryStatusBean formQueryStatussk = null;
        try {
            FormQueryStatusAgentRObj formQueryStatusAgentRObj = EJBUtil
                    .getFormQueryStatusAgentHome();
            formQueryStatussk = formQueryStatusAgentRObj
                    .getFormQueryStatusDetails(this.formQueryStatusId);
            Rlog.debug("formQueryStatus",
                    "FormQueryStatusJB.getFormQueryStatusDetails() FormQueryStatusStateKeeper "
                            + formQueryStatussk);
        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "Error in FormQueryStatus getFormQueryStatusDetails" + e);
        }

        if (formQueryStatussk != null) {

            setFormQueryStatusId(formQueryStatussk.getFormQueryStatusId());
            setFormQueryId(formQueryStatussk.getFormQueryId());
            setFormQueryType(formQueryStatussk.getFormQueryType());
            setQueryTypeId(formQueryStatussk.getQueryTypeId());
            setQueryNotes(formQueryStatussk.getQueryNotes());
            setEnteredBy(formQueryStatussk.getEnteredBy());
            setEnteredOn(formQueryStatussk.getEnteredOn());
            setQueryStatusId(formQueryStatussk.getQueryStatusId());
            setCreator(formQueryStatussk.getCreator());
            setModifiedBy(formQueryStatussk.getModifiedBy());
            setIpAdd(formQueryStatussk.getIpAdd());

        }

        return formQueryStatussk;

    }

    /**
     * Calls setFormQueryStatusDetails() of Form Query Session Bean:
     * FormQueryStatusAgentBean
     * 
     */

    public int setFormQueryStatusDetails() {
        int toReturn;
        try {

            FormQueryStatusAgentRObj formQueryStatusAgentRObj = EJBUtil
                    .getFormQueryStatusAgentHome();

            FormQueryStatusBean tempStateKeeper = new FormQueryStatusBean();

            tempStateKeeper = this.createFormQueryStatusStateKeeper();

            toReturn = formQueryStatusAgentRObj
                    .setFormQueryStatusDetails(tempStateKeeper);
            this.setFormQueryStatusId(toReturn);

            Rlog.debug("formQueryStatus",
                    "FormQueryStatusJB.setFormQueryStatusDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "Error in setFormQueryStatusDetails() in FormQueryStatusJB "
                            + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the FormQueryStatus Record with the
     *         current values of the bean
     */
    public FormQueryStatusBean createFormQueryStatusStateKeeper() {

        return new FormQueryStatusBean(formQueryStatusId, formQueryId,
                formQueryType, queryTypeId, queryNotes, enteredBy, enteredOn,
                queryStatusId, creator, modifiedBy, ipAdd);

    }

    /**
     * Calls updateFormStatusQuery() of FormQueryStatus Session Bean:
     * FormQueryStatusAgentBean
     * 
     * @return The status as an integer
     */
    public int updateFormQueryStatus() {
        int output;
        try {
            FormQueryStatusAgentRObj formQueryStatusAgentRObj = EJBUtil
                    .getFormQueryStatusAgentHome();

            output = formQueryStatusAgentRObj.updateFormQueryStatus(this
                    .createFormQueryStatusStateKeeper());
        } catch (Exception e) {
            Rlog.debug("formQueryStatus",
                    "EXCEPTION IN SETTING FormQueryStatus DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeFormQueryStatus() {

        int output;

        try {

            FormQueryStatusAgentRObj formQueryStatusAgentRObj = EJBUtil
                    .getFormQueryStatusAgentHome();

            output = formQueryStatusAgentRObj
                    .removeFormQueryStatus(this.formQueryStatusId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "in FormQueryJB removeFormQueryStatus() method" + e);
            return -1;
        }

    }

}
