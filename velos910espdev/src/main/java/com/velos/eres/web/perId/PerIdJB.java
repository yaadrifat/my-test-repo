/*
 * Classname : PerIdJB
 * 
 * Version information: 1.0
 *
 * Date: 16/02/04
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.perId;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.PerIdDao;
import com.velos.eres.business.perId.impl.PerIdBean;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.perIdAgent.impl.PerIdAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for PerId
 * 
 * @author Sonia Sahni
 * @version 1.0 09/23/2003
 */

public class PerIdJB {

    /**
     * The primary key:pk_perid
     */

    private int id;

    /**
     * The foreign key reference to er_per
     */
    private String perId;

    /**
     * The Id Type
     */

    private String perIdType;

    /**
     * The Alternate Per Id
     */
    private String alternatePerId;

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPerId(String perId) {
        this.perId = perId;
    }

    public String getPerId() {
        return perId;
    }

    public void setPerIdType(String perIdType) {
        this.perIdType = perIdType;
    }

    public String getPerIdType() {
        return perIdType;
    }

    public void setAlternatePerId(String alternatePerId) {
        this.alternatePerId = alternatePerId;
    }

    public String getAlternatePerId() {
        return alternatePerId;
    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param perIdId:
     *            Unique Id constructor
     * 
     */

    public PerIdJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public PerIdJB() {
        Rlog.debug("perId", "PerIdJB.PerIdJB() ");
    }

    public PerIdJB(int id, String perId, String perIdType,
            String alternatePerId, String creator, String modifiedBy,
            String ipAdd) {
        setId(id);
        setPerId(perId);
        setPerIdType(perIdType);
        setAlternatePerId(alternatePerId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getPerIdDetails of PerId Session Bean: PerIdAgentBean
     * 
     * @return PerIdStatKeeper
     * @see PerIdAgentBean
     */

    public PerIdBean getPerIdDetails() {
        PerIdBean perIdsk = null;
        try {
            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();
            perIdsk = perIdAgentRObj.getPerIdDetails(this.id);
            Rlog.debug("perId", "PerIdJB.getPerIdDetails() PerIdStateKeeper "
                    + perIdsk);
        } catch (Exception e) {
            Rlog.fatal("perId", "Error in perId getPerIdDetails" + e);
        }

        if (perIdsk != null) {
            setId(perIdsk.getId());
            setPerId(perIdsk.getPerId());
            setPerIdType(perIdsk.getPerIdType());
            setAlternatePerId(perIdsk.getAlternatePerId());
            setCreator(perIdsk.getCreator());
            setModifiedBy(perIdsk.getModifiedBy());
            setIpAdd(perIdsk.getIpAdd());

        }

        return perIdsk;

    }

    /**
     * Calls setPerIdDetails() of Per Id Session Bean: PerIdAgentBean
     * 
     */

    public int setPerIdDetails() {
        int toReturn;
        try {

            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();

            PerIdBean tempStateKeeper = new PerIdBean();

            tempStateKeeper = this.createPerIdStateKeeper();

            toReturn = perIdAgentRObj.setPerIdDetails(tempStateKeeper);
            this.setId(toReturn);

            Rlog.debug("perId", "PerIdJB.setPerIdDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("perId", "Error in setPerIdDetails() in PerIdJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the PerId Record with the current values
     *         of the bean
     */
    public PerIdBean createPerIdStateKeeper() {

        return new PerIdBean(id, perId, perIdType, alternatePerId, creator,
                modifiedBy, ipAdd);

    }

    /**
     * Calls updatePerId() of PerId Session Bean: PerIdAgentBean
     * 
     * @return The status as an integer
     */
    public int updatePerId() {
        int output;
        try {

            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();
            output = perIdAgentRObj.updatePerId(this.createPerIdStateKeeper());
        } catch (Exception e) {
            Rlog.debug("perId",
                    "EXCEPTION IN SETTING perId DETAILS TO  SESSION BEAN" + e);
            return -2;
        }
        return output;
    }

    public int removePerId() {

        int output;

        try {

            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();
            output = perIdAgentRObj.removePerId(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("perId", "in PerIdJB -removePerId() method" + e);
            return -1;
        }

    }

    public PerIdDao getPerIds(int perId,String defUserGroup) {

        PerIdDao pd = new PerIdDao();

        try {
            Rlog.debug("perId", "PerIdJB.getPerIds");

            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();

            pd = perIdAgentRObj.getPerIds(perId,defUserGroup);

            return pd;
        } catch (Exception e) {
            Rlog.fatal("perId", "getPerIds() in PerIdJB " + e);
            return null;
        }

    }

    /**
     * Method to add the multiple per Ids
     * 
     * @param PerIdStateKeeper
     * 
     */

    public int createMultiplePerIds(PerIdBean psk) {

        int ret = 0;

        try {
            Rlog.debug("perId", "PerIdJB.createMultiplePerIds ");

            PerIdAgentRObj perIdAgentRObj = EJBUtil.getPerIdAgentHome();
            ret = perIdAgentRObj.createMultiplePerIds(psk);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("perId", "createMultiplePerIds in PerIdJB " + e);
            return -2;
        }

    }

}
