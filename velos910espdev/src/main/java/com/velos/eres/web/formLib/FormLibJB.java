/*
 * Classname : 				FormLibJB
 * 
 * Version information : 	1.0
 *
 * Date 					07/22/2003
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Anu		
 */

package com.velos.eres.web.formLib;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.jms.ObjectMessage;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.FieldLibDao;
import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.fieldLib.FieldLibJB;
import com.velos.eres.web.formField.FormFieldJB;
import com.velos.eres.web.objectShare.ObjectShareJB;
import com.velos.jms.Publisher;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for form library module. This class is used for any kind of
 * manipulation for the form .
 * 
 * @author Anu
 * @version 1.0, 07/22/2003
 */

public class FormLibJB {

    /**
     * the form Library Id
     */
    private int formLibId;

    /**
     * the category Id
     */
    private String catLibId;

    /**
     * the account Id
     */
    private String accountId;

    /**
     * the form name
     */
    private String name;

    /**
     * the form description
     */
    private String desc;

    /**
     * the shared with flag
     */
    private String sharedWith;

    /**
     * the form status
     */
    private String status;

    /**
     * form link attribute
     */
    private String linkTo;

    /**
     * next form
     */
    private String formNext;

    /**
     * flag for the mode
     */
    private String formNextMode;

    /**
     * bold attribute
     */
    private String bold;

    /**
     * italics attribute
     */
    private String italics;

    /**
     * align attribute
     */
    private String align;

    /**
     * the alignment
     */
    private String underLine;

    /**
     * the color
     */
    private String color;

    /**
     * the background color
     */
    private String bgColor;

    /**
     * the Font
     */
    private String font;

    /**
     * the Font Size
     */
    private String fontSize;

    /**
     * fill flag
     */
    private String fillFlag;

    /**
     * refresh from xsl
     */
    private String xslRefresh;

    /**
     * user who last modified
     */
    private String lastModifiedBy;

    /**
     * the Creator
     */
    private String creator;

    /**
     * the Ip address of the creator
     */
    private String ipAdd;

    private String sharedWithIds;

    /**
     * Record Type
     */
    private String recordType;

    private String shrdWithIds;

    private String shrdWithNames;

    private boolean booleanForFrmStat = true;
    
    private boolean migrateVersion = false;
    
    private boolean skipNameCheck = false;
    
    /**
     * eSign required
     */
    private String esignRequired;


    // GETTER SETTER METHODS
    
    
	public String getEsignRequired() {
		return esignRequired;
	}

	public void setEsignRequired(String esignRequired) {
		this.esignRequired = esignRequired;
	}

    public String getAccountId() {
        return this.accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFormLibAlign() {
        return this.align;
    }

    public void setFormLibAlign(String align) {
        this.align = align;
    }

    public String getFormLibBgColor() {
        return this.bgColor;
    }

    public void setFormLibBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFormLibBold() {
        return this.bold;
    }

    public void setFormLibBold(String bold) {
        this.bold = bold;
    }

    public String getCatLibId() {
        return this.catLibId;
    }

    public void setCatLibId(String catLibId) {
        this.catLibId = catLibId;
    }

    public String getFormLibColor() {
        return this.color;
    }

    public void setFormLibColor(String color) {
        this.color = color;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getFormLibDesc() {
        return this.desc;
    }

    public void setFormLibDesc(String desc) {
        this.desc = desc;
    }

    public String getFormLibFillFlag() {
        return this.fillFlag;
    }

    public void setFormLibFillFlag(String fillFlag) {
        this.fillFlag = fillFlag;
    }

    public String getFormLibFont() {
        return this.font;
    }

    public void setFormLibFont(String font) {
        this.font = font;
    }

    public String getFormLibFontSize() {
        return this.fontSize;
    }

    public void setFormLibFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public int getFormLibId() {
        return this.formLibId;
    }

    public void setFormLibId(int formLibId) {
        this.formLibId = formLibId;
    }

    public String getFormLibNext() {
        return this.formNext;
    }

    public void setFormLibNext(String formNext) {
        this.formNext = formNext;
    }

    public String getFormLibNextMode() {
        return this.formNextMode;
    }

    public void setFormLibNextMode(String formNextMode) {
        this.formNextMode = formNextMode;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getFormLibItalics() {
        return this.italics;
    }

    public void setFormLibItalics(String italics) {
        this.italics = italics;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getFormLibLinkTo() {
        return this.linkTo;
    }

    public void setFormLibLinkTo(String linkTo) {
        this.linkTo = linkTo;
    }

    public String getFormLibName() {
        return this.name;
    }

    public void setFormLibName(String name) {
        this.name = name;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getFormLibSharedWith() {
        return this.sharedWith;
    }

    public void setFormLibSharedWith(String sharedWith) {
        this.sharedWith = sharedWith;
    }

    public String getFormLibStatus() {
        return this.status;
    }

    public void setFormLibStatus(String status) {
        this.status = status;
    }

    public String getFormLibUnderLine() {
        return this.underLine;
    }

    public void setFormLibUnderLine(String underLine) {
        this.underLine = underLine;
    }

    public String getFormLibXslRefresh() {
        return this.xslRefresh;
    }

    public void setFormLibXslRefresh(String xslRefresh) {
        this.xslRefresh = xslRefresh;
    }

    public String getSharedWithIds() {
        return this.sharedWithIds;
    }

    public void setSharedWithIds(String sharedWithIds) {
        this.sharedWithIds = sharedWithIds;
    }

    public String getShrdWithIds() {
        return shrdWithIds;

    }

    public void setShrdWithIds(String shrdWithIds) {
        this.shrdWithIds = shrdWithIds;
    }

    public String getShrdWithNames() {
        return this.shrdWithNames;
    }

    public void setShrdWithNames(String shrdWithNames) {
        this.shrdWithNames = shrdWithNames;
    }

    public boolean getBooleanForFrmStat() {
        return this.booleanForFrmStat;
    }

    public void setBooleanForFrmStat(boolean booleanForFrmStat) {
        Rlog.debug("formlib", "FormLibJB.booleanFormStatus:"
                + booleanForFrmStat);
        this.booleanForFrmStat = booleanForFrmStat;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Defines an FormLibJB object with the specified form Id
     */
    public FormLibJB(int formLibId) {
        setFormLibId(formLibId);
    }

    /**
     * Defines an FormLibJB object with default values for form
     */
    public FormLibJB() {
        Rlog.debug("formlib", "FormLibJB.FormLibJB() ");
    };

    /**
     * Defines an FormLibJB object with the specified values for the form
     * 
     */
    public FormLibJB(int formLibId, String catLibId, String accountId,
            String name, String desc, String sharedWith, String status,
            String linkTo, String formNext, String formNextMode, String bold,
            String italics, String align, String underLine, String color,
            String bgColor, String font, String fontSize, String fillFlag,
            String creator, String xslRefresh, String lastModifiedBy,
            String recordType, String ipAdd,String esignreq) {
        setFormLibId(formLibId);
        setCatLibId(catLibId);
        setAccountId(accountId);
        setFormLibName(name);
        setFormLibDesc(desc);
        setFormLibSharedWith(sharedWith);
        setFormLibStatus(status);
        setFormLibLinkTo(linkTo);
        setFormLibNext(formNext);
        setFormLibNextMode(formNextMode);
        setFormLibBold(bold);
        setFormLibItalics(italics);
        setFormLibAlign(align);
        setFormLibUnderLine(underLine);
        setFormLibColor(color);
        setFormLibBgColor(bgColor);
        setFormLibFont(font);
        setFormLibFontSize(fontSize);
        setFormLibFillFlag(fillFlag);
        setCreator(creator);
        setFormLibXslRefresh(xslRefresh);
        setLastModifiedBy(lastModifiedBy);
        setRecordType(recordType);
        setIpAdd(ipAdd);
        setEsignRequired(esignreq);
        Rlog.debug("formlib", "FormLibJB.FormLibJB(all parameters)");
    }

    /**
     * Retrieves the details of form in FormLibStateKeeper Object
     * 
     * @return FormLibStateKeeper consisting of the form details
     * @see FormLibStateKeeper
     */
    public FormLibBean getFormLibDetails() {
        FormLibBean flsk = null;
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            Rlog.debug("formlib",
                    " in JB FormLibJB.getFormLibDetails() formLibAgentRObj "
                            + formLibAgentRObj);
            flsk = formLibAgentRObj.getFormLibDetails(this.formLibId);
            Rlog.debug("formlib",
                    " in JB FormLibJB.getFormLibDetails() FormLibStateKeeper "
                            + flsk);
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getFormLibDetails() in FormLibJB "
                    + e);
        }
        if (flsk != null) {
            this.formLibId = flsk.getFormLibId();
            this.catLibId = flsk.getCatLibId();
            this.accountId = flsk.getAccountId();
            this.name = flsk.getFormLibName();
            this.desc = flsk.getFormLibDesc();
            this.sharedWith = flsk.getFormLibSharedWith();
            this.status = flsk.getFormLibStatus();
            this.linkTo = flsk.getFormLibLinkTo();
            this.formNext = flsk.getFormLibNext();
            this.formNextMode = flsk.getFormLibNextMode();
            this.bold = flsk.getFormLibBold();
            this.italics = flsk.getFormLibItalics();
            this.align = flsk.getFormLibAlign();
            this.underLine = flsk.getFormLibUnderLine();
            this.color = flsk.getFormLibColor();
            this.bgColor = flsk.getFormLibBgColor();
            this.font = flsk.getFormLibFont();
            this.fontSize = flsk.getFormLibFontSize();
            this.fillFlag = flsk.getFormLibFillFlag();
            this.creator = flsk.getCreator();
            this.xslRefresh = flsk.getFormLibXslRefresh();
            this.lastModifiedBy = flsk.getLastModifiedBy();
            this.recordType = flsk.getRecordType();
            this.ipAdd = flsk.getIpAdd();
            this.setEsignRequired(flsk.getEsignRequired());

            // Rlog.debug("formlib","FormLibJB.sharedwith" + sharedWith);

            /*
             * if (sharedWith.equals("G")||sharedWith.equals("S") ||
             * sharedWith.equals("O")) { String selNames=""; String selIds="";
             * String names=""; ArrayList arrIds=new ArrayList(); ArrayList
             * arrNames=new ArrayList(); SharedWithDao sharedWithDao=new
             * SharedWithDao();
             * sharedWithDao.getSharedWithInfo(formLibId,sharedWith);
             * arrNames=sharedWithDao.getSharedWithNames();
             * arrIds=sharedWithDao.getSharedWithIds();
             * 
             * 
             * int len=arrNames.size();
             * 
             * Rlog.debug("formlib","FormLibJB.getFormLibDetails() arrIds " +
             * arrIds); if(len==1) { selNames=(String)arrNames.get(0);
             * selIds=EJBUtil.integerToString((Integer)arrIds.get(0)); } else {
             * if ( len > 0 ) { for(int count=0;count<len;count++) { selNames =
             * selNames + (String)arrNames.get(count); selIds = selIds +
             * EJBUtil.integerToString((Integer)arrIds.get(count));
             * 
             * 
             * selNames = selNames + ","; selIds = selIds + ","; }
             * 
             * int pos = 0;
             * 
             * pos = selNames.lastIndexOf(","); selNames =
             * selNames.substring(0,pos);
             * 
             * pos = selIds.lastIndexOf(","); selIds= selIds.substring(0,pos); }
             * Rlog.debug("formlib","FormLibJB.getFormLibDetails() selNames " +
             * selNames); Rlog.debug("formlib","FormLibJB.getFormLibDetails()
             * selIds " + selIds); }
             * 
             * 
             * 
             * setShrdWithNames(selNames); setShrdWithIds(selIds);
             * 
             * setSharedWithIds(selIds);
             * Rlog.debug("formlib","FormLibJB.getFormLibDetails() anu selNames " +
             * selNames); Rlog.debug("formlib","FormLibJB.getFormLibDetails()
             * anu selIds " + selIds); } else if (sharedWith.equals("P")) {
             * setSharedWithIds(getCreator()); setShrdWithIds(getCreator()); }
             * else if (sharedWith.equals("A")) {
             * setSharedWithIds(getAccountId()); setShrdWithIds(getAccountId()); }
             */

        }
        return flsk;
    }

    /**
     * Stores the details of form in the database
     */
    public int setFormLibDetails() {
        int id = 0;
        int ret = 0;
        String formId = "";
        int formFldId = 0;
        Rlog.debug("formlib", " inside set details of FormLibJB");
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            id = formLibAgentRObj.setFormLibDetails(this
                    .createFormLibStateKeeper());
            Rlog.debug("formlib", "id##" + id);

            Rlog.debug("formlib", "FormLibJB.setFormLibDetails()");

        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in setFormLibDetails() in FormLibJB "
                    + e);
        }

        if (id < 0) {
            return id;
        } else if (id > 0) {

            this.setFormLibId(id);

            formId = Integer.toString(id);
            Rlog.debug("formlib", "form id" + formId);
            // String sharedWithIds=getSharedWithIds();
            // String type=getFormLibSharedWith();
            String user = getCreator();
            String ipAdd = getIpAdd();

            String accId = getAccountId();

            // set form sharewith to "All Accout Users" by default
            try {
                Rlog.debug("formlib", "saving form sharewith");
                ObjectShareJB objShareB = new ObjectShareJB();

                objShareB.setIpAdd(ipAdd);
                objShareB.setCreator(user);
                objShareB.setObjNumber("1");// number of module
                objShareB.setFkObj(formId);// formId, CalId, reportId
                objShareB.setObjSharedId(accId);// shared ids
                objShareB.setObjSharedType("A");// U-user id,A -account id,S -
                // study id,G - group id,O -
                // organization id
                objShareB.setRecordType("N");
                ret = objShareB.setObjectShareDetails();
            } catch (Exception e) {
                Rlog.fatal("formlib",
                        "Exception in setting Form share with in ObjectShare "
                                + e);
            }

            FormLibDao formlibDao = new FormLibDao();

            Rlog.debug("formlib", "value of formId " + formId);
            formFldId = formlibDao.insertFormDefaultData(EJBUtil
                    .stringToNum(formId));
            Rlog.debug("formlib", "value of formFldId " + formFldId);

            if (formFldId > 0) {
                // formFldId contains primary key of the default formfield added

                try {
                    int dateFieldId = 0;
                    String isMandatory = "";
                    boolean mandatory = false;

                    FormFieldJB formFld = new FormFieldJB();

                    formFld.setFormFieldId(formFldId);
                    formFld.getFormFieldDetails();

                    dateFieldId = EJBUtil.stringToNum(formFld.getFieldId());
                    isMandatory = formFld.getFormFldMandatory();

                    if (!EJBUtil.isEmpty(isMandatory)) {
                        if (isMandatory.equals("1"))
                            mandatory = true;
                        else
                            mandatory = false;

                    }

                    FieldLibJB fldJB = new FieldLibJB();
                    fldJB.setFieldLibId(dateFieldId);
                    fldJB.getFieldLibDetails();
                    fldJB.toXSL(mandatory);

                    formFld.setFormFldXsl(fldJB.getFldXSL());
                    formFld.setFormFldJavaScr(fldJB.getFldJavaScript());

                    //KM-#3634
                    formFld.setModifiedBy(user);
                    formFld.setIpAdd(ipAdd);
                   
                    formFld.updateFormField();

                } catch (Exception ex) {
                    Rlog.fatal("formlib",
                            "Error in setFormLibDetails() while updating form field "
                                    + ex);
                }
            }

        }
        return id;

    }

    /**
     * Saves the modified details of the form to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateFormLib() {
        int output;
        try {
            Rlog.debug("formlib", "form inside updateformlib");
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            FormLibBean formLibSk = new FormLibBean();
            formLibSk = this.createFormLibStateKeeper();
            formLibSk.setBooleanForFrmStat(this.getBooleanForFrmStat());
            
            if (this.skipNameCheck) {
                output = formLibAgentRObj.updateFormLib(formLibSk, true);
            } else {
                output = formLibAgentRObj.updateFormLib(formLibSk);
            }

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception in update form inside updateformlib " + e);
            return -2;
        }
        if (output == -3)
            return -3;
        if (output >= 0) {

            int formId = getFormLibId();
            // String sharedWithIds=getSharedWithIds();
            // String type=getFormLibSharedWith();
            // String user=getCreator();
            // String ipAdd=getIpAdd();

            /*
             * if (type != null) { FormLibDao formlibDao=new FormLibDao();
             * formlibDao.insertFormSharedWith(formId,sharedWithIds,type,user,ipAdd,"M"); }
             */

            // if the status is Active then create version,
            // generate Form Printer Friendly XSL, report data
            CodeDao codeDao = new CodeDao();
            int activeStatId = codeDao.getCodeId("frmstat", "A");
            int newStat = EJBUtil.stringToNum(this.status);

            Rlog.debug("formlib", "in updateformlib in FormLibJB activeStatId "
                    + activeStatId + " new stat " + newStat);
            if (newStat == activeStatId) {
                SaveFormDao saveFormDao = new SaveFormDao();
                saveFormDao.doFormActiveStatusTasks(formId,isMigrateVersion());
                
                // post a message to MDB to start migration!!
                
                if (isMigrateVersion())
                {
                	CtrlDao ctrl = new CtrlDao();
                	String connFactory = "";
                	String mdbJNDI = "";
                	FormLibDao fd = new FormLibDao();
                	ArrayList connFactoryAR = new ArrayList();
                	ArrayList mdbJNDIAR = new ArrayList();
                	String latestFormLibVerPk = "";
                	
                	LinkedFormsDao lf = new LinkedFormsDao();
                	String frmTypeStr = "";
                	
                	latestFormLibVerPk = fd.getLatestFormLibVersionPK(formId);
                	frmTypeStr = lf.getLinkedFormType(formId);
                	
                	ctrl.getControlValues("form_migration");
                	
                	connFactoryAR = ctrl.getCValue();
                	mdbJNDIAR = ctrl.getCDesc();
                	
                	if (connFactoryAR != null && connFactoryAR.size() > 0)
                	{
                		connFactory = (String) connFactoryAR.get(0);
                		mdbJNDI = (String) mdbJNDIAR.get(0);
                		
                	}
                	
                	if (! StringUtil.isEmpty(connFactory))
                	{
                		
                		try{
                			Publisher publisher = new Publisher(connFactory,
                					mdbJNDI);
                            ObjectMessage objMessage = (publisher.getSession())
                                    .createObjectMessage();

                            objMessage.setStringProperty("modifiedBy", this.getLastModifiedBy());
                            objMessage.setStringProperty("formId", String.valueOf(this.getFormLibId()));
                            objMessage.setStringProperty("formLibVer", latestFormLibVerPk);
                            objMessage.setStringProperty("formType", frmTypeStr);
                            objMessage.setStringProperty("ipAdd", this.getIpAdd());
                            
                            
//                          Publish message
                            publisher.PublishMessage(objMessage);

                            // Close down your publisher
                            publisher.closeJMSTopicPublisher();
                            publisher.closeJMSConnection();
                			
                		} catch (Exception ex) {
                			
                			   Rlog.fatal("formlib","An exception occurred while sending message via JMS: "
                                       + ex);
                	        
                }
                		
                		
                	}//(! StringUtil.isEmpty(connFactory))
                	
                	
                }
                
                //
            }
        } else {
            Rlog.fatal("formlib",
                    "Exception in updateformlib - Data Is not saved ");
        }
        return output;
    }
    
	// Overloaded for INF-18183 ::: AGodara
	public int updateFormLib(Hashtable<String, String> auditInfo) {

		int output;
		try {
			FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
			FormLibBean formLibSk = new FormLibBean();
			formLibSk = this.createFormLibStateKeeper();
			formLibSk.setBooleanForFrmStat(this.getBooleanForFrmStat());
			output = formLibAgentRObj.updateFormLib(formLibSk, auditInfo);
		} catch (Exception e) {
			Rlog.fatal("formlib",
					"Exception in update form inside updateformlib " + e);
			return -2;
		}
		if (output == -3)
			return -3;

		if (output >= 0) {
			int formId = getFormLibId();
			CodeDao codeDao = new CodeDao();
			int activeStatId = codeDao.getCodeId("frmstat", "A");
			int newStat = EJBUtil.stringToNum(this.status);
			if (newStat == activeStatId) {
				SaveFormDao saveFormDao = new SaveFormDao();
				saveFormDao.doFormActiveStatusTasks(formId, isMigrateVersion());

				if (isMigrateVersion()) {
					CtrlDao ctrl = new CtrlDao();
					String connFactory = "";
					String mdbJNDI = "";
					FormLibDao fd = new FormLibDao();
					ArrayList connFactoryAR = new ArrayList();
					ArrayList mdbJNDIAR = new ArrayList();
					String latestFormLibVerPk = "";

					LinkedFormsDao lf = new LinkedFormsDao();
					String frmTypeStr = "";

					latestFormLibVerPk = fd.getLatestFormLibVersionPK(formId);
					frmTypeStr = lf.getLinkedFormType(formId);

					ctrl.getControlValues("form_migration");

					connFactoryAR = ctrl.getCValue();
					mdbJNDIAR = ctrl.getCDesc();

					if (connFactoryAR != null && connFactoryAR.size() > 0) {
						connFactory = (String) connFactoryAR.get(0);
						mdbJNDI = (String) mdbJNDIAR.get(0);

					}
					if (!StringUtil.isEmpty(connFactory)) {
						try {
							Publisher publisher = new Publisher(connFactory,
									mdbJNDI);
							ObjectMessage objMessage = (publisher.getSession())
									.createObjectMessage();

							objMessage.setStringProperty("modifiedBy", this
									.getLastModifiedBy());
							objMessage.setStringProperty("formId", String
									.valueOf(this.getFormLibId()));
							objMessage.setStringProperty("formLibVer",
									latestFormLibVerPk);
							objMessage
									.setStringProperty("formType", frmTypeStr);
							objMessage.setStringProperty("ipAdd", this
									.getIpAdd());
							// Publish message
							publisher.PublishMessage(objMessage);
							// Close down your publisher
							publisher.closeJMSTopicPublisher();
							publisher.closeJMSConnection();
						} catch (Exception ex) {
							Rlog.fatal("formlib",
									"An exception occurred while sending message via JMS: "
											+ ex);
						}
					}
				}
			}
		} else {
			Rlog.fatal("formlib",
					"Exception in updateformlib - Data Is not saved ");
		}
		return output;
	}
    
    
    /**
     * Uses the values of the current FormLibJB object to create an
     * FormLibStateKeeper Object
     * 
     * @return FormLibStateKeeper object with all the details of the form
     * @see FormLibStateKeeper
     */
    public FormLibBean createFormLibStateKeeper() {
        Rlog.debug("formlib", "FormLibJB.createFormLibStateKeeper ");
        return new FormLibBean(formLibId, catLibId, accountId, name, desc,
                sharedWith, status, linkTo, formNext, formNextMode, bold,
                italics, align, underLine, color, bgColor, font, fontSize,
                fillFlag, xslRefresh, lastModifiedBy, creator, ipAdd,
                recordType,esignRequired);

    }

    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType) {
        int count = 0;
        FormLibDao formLibDao = new FormLibDao();
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            formLibDao = formLibAgentRObj.getFormDetails(accountId, userId,
                    formName, formType);
            Rlog.debug("formlib", "FormLibJB.getFormDetails after Dao");
            return formLibDao;
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getFormDetails in FormLibJB " + e);
        }
        return formLibDao;
    }

    //added by IA Bug 2614 multiple organizations
    
    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType, String orgId) {
        int count = 0;
        FormLibDao formLibDao = new FormLibDao();
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            formLibDao = formLibAgentRObj.getFormDetails(accountId, userId,
                    formName, formType, orgId);
            Rlog.debug("formlib", "FormLibJB.getFormDetails after Dao");
            return formLibDao;
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getFormDetails in FormLibJB " + e);
        }
        return formLibDao;
    }
    
    public int insertCopyFieldToForm(int pkForm, int pkSection, int pkField,
            String user, String ipAdd) {
        int ret;
        try {
            Rlog.debug("formlib", "FormLibJB.insertCopyFieldToForm starting");
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
            int retArray[] = formLibAgentRObj.insertCopyFieldToForm(pkForm, pkSection,
                    pkField, user, ipAdd);
            int retFieldId = retArray[0];
            int retFormFldId = retArray[1];
            FieldLibDao fieldLibDao = new FieldLibDao();
            fieldLibDao.copyFieldFromFormToForm(retFieldId, retFormFldId);
            ret = retArray[0];
        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in insertCopyFieldToForm in FormlibJB " + e);
            return -2;
        }
        return ret;
    }

    public FormLibDao getSectionFieldInfo(int formId) {
        int count = 0;
        FormLibDao formLibDao = new FormLibDao();
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            formLibDao = formLibAgentRObj.getSectionFieldInfo(formId);
            Rlog.debug("formlib", "FormLibJB.getSectionFieldInfo after Dao");
            return formLibDao;
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getSectionFieldInfo in FormLibJB "
                    + e);
        }
        return formLibDao;
    }

    public int copyFormsFromLib(String[] idArray, String formType,
            String formName, String creator, String ipAdd) {
        int newFormPk = 0;
        FormLibDao formLibDao = new FormLibDao();
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            newFormPk = formLibDao.copyFormsFromLib(idArray, formType,
                    formName, creator, ipAdd);
            Rlog.debug("formlib", "FormLibJB.copyFormsFromLib ");
            return newFormPk;
        } catch (Exception e) {
            Rlog
                    .fatal("formlib", "Error in copyFormsFromLib in FormLibJB "
                            + e);
        }
        return -1;
    }

    /**
     * This method creates er_objectshare data for all the forms who have the
     * user primary organization in sharewith also to create record for the new
     * user's account called when a new user is created
     * 
     * @param userId
     * @param accountId
     * @param siteId
     * @param creator
     * @param ipAdd
     * @return 0 if successful, -1 if error
     */
    public int createFormShareWithData(int userId, int accountId, int siteId,
            int creator, String ipAdd) {
        int ret = 0;
        try {
            Rlog.debug("formlib", "in FormLibJB.createFormShareWithData ");
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            ret = formLibAgentRObj.createFormShareWithData(userId, accountId,
                    siteId, creator, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in createFormShareWithData in FormLibJB " + e);
            return -1;
        }

    }

    /*
     * public int updateFormLibfromFormFld() { int output; try{
     * Rlog.debug("formlib","updateFormLibfromFormFld"); FormLibAgentHome
     * formLibAgentHome = EJBUtil.getFormLibAgentHome(); FormLibAgentRObj
     * formLibAgentRObj = formLibAgentHome.create(); output =
     * formLibAgentRObj.updateFormLib(this.createFormLibStateKeeper());
     * Rlog.debug("formlib","NOW OUTPUT VALUE " +output); } catch(Exception e){
     * Rlog.fatal("formlib","Exception in update form inside updateformlib " +
     * e); return -2; } return output; }
     * 
     */
    public int insertFilledFormBySP(SaveFormStateKeeper sk) {
        int ret;
        try {
            Rlog.debug("formlib", "FormLibJB.insertFilledFormBySP starting");
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
            ret = formLibAgentRObj.insertFilledFormBySP(sk);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in insertFilledFormBySP in FormlibJB " + e);
            return -2;
        }
        return ret;
}
    public int updateFilledFormBySP(SaveFormStateKeeper sk) {
        int ret;
        try {
            Rlog.debug("formlib", "FormLibJB.updateFilledFormBySP starting");
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
            ret = formLibAgentRObj.updateFilledFormBySP(sk);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in updateFilledFormBySP in FormlibJB " + e);
            return -2;
        }
        return ret;
}
    
    //Added by Manimaran for the Enhancement F6
    public String getFormLibVersionNumber(int formlibverPK) {
    	String formLibVer = "";
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
            formLibVer = formLibAgentRObj.getFormLibVersionNumber(formlibverPK);
            Rlog.debug("formlib", "FormLibJB.getFormLibVersionNumber ");
            return formLibVer;
        } catch (Exception e) {
            Rlog
                    .fatal("formlib", "Error in getFormLibVersionNumber in FormLibJB "
                            + e);
        }
        return formLibVer;
    }

	public boolean isMigrateVersion() {
		return migrateVersion;
	}

	public void setMigrateVersion(boolean migrateVersion) {
		this.migrateVersion = migrateVersion;
	}
    
    
	////  added by Sonia, 06/15/07, to get the first PK of a filled form answered for a patient by a creator
    public int getPatFilledFormPKForCreator(int form,int patientPK,int creator) 
    {
    	int filledformid = 0;
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            filledformid = formLibAgentRObj.getPatFilledFormPKForCreator(form,patientPK,creator);
            
            return filledformid;
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getFormLibVersionNumber in FormLibJB "
                            + e);
        }
        return filledformid;
        
    }
    public int validateFormField(int formID,int responseID,String systemID,int studyID,int patientID,int accountID,String datavalue,String operator)
    {
    	int ret=0;
    	try{
    		 FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
    		 ret = formLibAgentRObj.validateFormField(formID,responseID,systemID,studyID,patientID,accountID,datavalue,operator);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		 Rlog.fatal("formlib", "Error in validateFormField in FormLibJB "+ e);
    	}
    	return ret;
    }
    public String validateFormField(int formID,String systemID,int studyID,int patientID,int accountID,String datavalue)
    {
    	String ret="";
    	try{
    		 FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
    		 ret = formLibAgentRObj.validateFormField(formID,systemID,studyID,patientID,accountID,datavalue);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		 Rlog.fatal("formlib", "Error in validateFormField in FormLibJB "+ e);
    	}
    	return ret;
    }
  
    public int getPatFilledFormPKForCreatorForParam(int form,int patientPK,int creator,Hashtable htParam) 
    {
    	int filledformid = 0;
        try {
            FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();

            filledformid = formLibAgentRObj.getPatFilledFormPKForCreatorForParam(form,patientPK,creator,htParam);
            
            return filledformid;
        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in getFormLibVersionNumber in FormLibJB "
                            + e);
        }
        return filledformid;
        
    }

	public boolean isSkipNameCheck() {
		return skipNameCheck;
	}

	public void setSkipNameCheck(boolean skipNameCheck) {
		this.skipNameCheck = skipNameCheck;
	}
    
}
