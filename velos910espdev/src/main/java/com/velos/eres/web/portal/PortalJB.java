/*

 * Classname : 				PortalJB

 * 

 * Version information : 	1.0

 *

 * Date 					04/03/07

 * 

 * Copyright notice: 		Velos Inc

 * 

 * Author 					Manimaran

 */


package com.velos.eres.web.portal;


/* IMPORT STATEMENTS */

import com.velos.eres.business.common.PortalDao;
import com.velos.eres.business.portal.impl.PortalBean;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.portalAgent.PortalAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;

import java.sql.*;
import java.util.Hashtable;

import javax.persistence.Column;
/* END OF IMPORT STATEMENTS */

/**
 * 
 * Client side bean for Portal module. This class is used for any kind of
 * manipulation for the Portal.
 * 
 * @author Manimaran
 * 
 * @version 1.0, 04/03/2007
 * 
 */
public class PortalJB {
	
	private int portalId;
	
	private String portalName;
	
	private String accountId;
	
	private String portalCreatedBy;
	
	private String portalDesc;
	
	private String portalLevel;
	
	private String portalNotificationFlag;
	
	private String portalStatus;
	
	private String portalDispType;
	
	private String portalBgColor;
	
	private String portalTxtColor;
	
	private String portalHeader;
	
	private String portalFooter;
	
	private String portalAuditUser;
	
    /**
     * the study linked with the portal (if any)
     */
    private String portalStudy;

    
	
	 /**
     * 
     * 
     * The creator of the field type : CREATOR
     * 
     */

    private String creator;

    /**
     * 
     * The id of the user who last modified the field : LAST_MODIFIED_BY
     * 
     */

    private String modifiedBy;

    /**
     * 
     * The IP Address of the site which made the change : IP_ADD
     * 
     */

    private String ipAdd;
    
    /** 
     * Flag to indicate if user will be automatically logged out after answering any of portal forms.Possible values: 0:dont logout,1:Self logout
    */

    private String portalSelfLogout;
    
    
    /**
     *  Flag to indicate if logins will be created by default.Possible values: 0:no,1:yes
     * 
     */
    private String portalCreateLogins;
    
    /**
     *  Default password for all logins that will be created for this portal
     * 
     */
    private String portalDefaultPassword;
	
    /**
     *  Portal consenting form
     * 
     */
    public String portalConsentingForm;
    

    
//  GETTERS AND SETTERS

    /**
     * 
     * 
     * @return portalId
     */
    public int getPotalId() {
        return this.portalId;
    }

    /**
     * 
     * 
     * @param portalId
     */
    public void setPortalId(int portalId) {
        this.portalId = portalId;
    }
    
    /**
     * 
     * 
     * @return portalName
     */
    public String getPortalName() {
        return this.portalName;
    }

    /**
     * 
     * 
     * @param portalName
     */
    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }
    
    /**
     * 
     * 
     * @return accountId
     */
    public String getAccountId() {
        return this.accountId;
    }

    /**
     * 
     * 
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    
    /**
     * 
     * 
     * @return portalCreatedBy
     */
    public String getPortalCreatedBy() {
        return this.portalCreatedBy;
    }

    /**
     * 
     * 
     * @param portalCreatedBy
     */
    public void setPortalCreatedBy(String portalCreatedBy) {
        this.portalCreatedBy = portalCreatedBy;
    }

    /**
     * 
     * 
     * @return portalDesc
     */
    public String getPortalDesc() {
        return this.portalDesc;
    }

    /**
     * 
     * 
     * @param portalDesc
     */
    public void setPortalDesc(String portalDesc) {
        this.portalDesc = portalDesc;
    }
    
    /**
     * 
     * 
     * @return portalLevel
     */
    public String getPortalLevel() {
        return this.portalLevel;
    }

    /**
     * 
     * 
     * @param portalLevel
     */
    public void setPortalLevel(String portalLevel) {
        this.portalLevel = portalLevel;
    }
    
    /**
     * 
     * 
     * @return portalNotificationFlag
     */
    public String getPortalNotificationFlag() {
        return this.portalNotificationFlag;
    }

    /**
     * 
     * 
     * @param portalNotificationFlag
     */
    public void setPortalNotificationFlag(String portalNotificationFlag) {
        this.portalNotificationFlag = portalNotificationFlag;
    }
    
    /**
     * 
     * 
     * @return portalStatus
     */
    public String getPortalStatus() {
        return this.portalStatus;
    }

    /**
     * 
     * 
     * @param portalStatus
     */
    public void setPortalStatus(String portalStatus) {
        this.portalStatus = portalStatus;
    }
    
    /**
     * 
     * 
     * @return portalDispType
     */
    public String getPortalDispType() {
        return this.portalDispType;
    }

    /**
     * 
     * 
     * @param portalDispType
     */
    public void setPortalDispType(String portalDispType) {
        this.portalDispType = portalDispType;
    }
    
    /**
     * 
     * 
     * @return portalBgColor
     */
    public String getPortalBgColor() {
        return this.portalBgColor;
    }

    /**
     * 
     * 
     * @param portalBgColor
     */
    public void setPortalBgColor(String portalBgColor) {
        this.portalBgColor = portalBgColor;
    }

    /**
     * 
     * 
     * @return portalTxtColor
     */
    public String getPortalTxtColor() {
        return this.portalTxtColor;
    }

    /**
     * 
     * 
     * @param portalTxtColor
     */
    public void setPortalTxtColor(String portalTxtColor) {
        this.portalTxtColor = portalTxtColor;
    }
    
    /**
     * 
     * 
     * @return portalHeader
     */
    public String getPortalHeader() {
        return this.portalHeader;
    }

    /**
     * 
     * 
     * @param portalHeader
     */
    public void setPortalHeader(String portalHeader) {
        this.portalHeader = portalHeader;
    }
    
    /**
     * 
     * 
     * @return portalFooter
     */
    public String getPortalFooter() {
        return this.portalFooter;
    }

    /**
     * 
     * 
     * @param portalFooter
     */
    public void setPortalFooter(String portalFooter) {
        this.portalFooter = portalFooter;
    }
    
    /**
     * 
     * 
     * @return portalAuditUser
     */
    public String getPortalAuditUser() {
        return this.portalAuditUser;
    }

    /**
     * 
     * 
     * @param portalAuditUser
     */
    public void setPortalAuditUser(String portalAuditUser) {
        this.portalAuditUser = portalAuditUser;
    }
    
    /**
     * 
     * 
     * @return creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * 
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 
     * 
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * 
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * 
     * @return ipAdd
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * 
     * 
     * @param ipAdd
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    
    
    // END OF THE GETTER AND SETTER METHODS
    
    /**
     * Constructor
     * 
     * @param portalId:
     *            Unique Id constructor
     * 
     */

    public PortalJB(int portalId) {
        setPortalId(portalId);
    }

    /**
     * Default Constructor
     */

    public PortalJB() {
        Rlog.debug("portal", "PortalJB.PortalJB() ");
    }
    
    
    public PortalJB(int portalId, String portalName, String accountId,
            String portalCreatedBy, String portalDesc, String portalLevel,
            String portalNotificationFlag, String portalStatus, String portalDispType,
            String portalBgColor, String portalTxtColor, String portalHeader, String portalFooter,
            String portalAuditUser, String creator, String modifiedBy, String ipAdd, String study,String selflogout,String createlogin,String defpass,String form) {
        setPortalId(portalId);
        setPortalName(portalName);
        setAccountId(accountId);
        setPortalCreatedBy(portalCreatedBy);
        setPortalDesc(portalDesc);
        setPortalLevel(portalLevel);
        setPortalNotificationFlag(portalNotificationFlag);
        setPortalStatus(portalStatus);
        setPortalDispType(portalDispType);
        setPortalBgColor(portalBgColor);
        setPortalTxtColor(portalTxtColor);
        setPortalHeader(portalHeader);
        setPortalFooter(portalFooter);
        setPortalAuditUser(portalAuditUser);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPortalStudy(study);
        setPortalSelfLogout(selflogout);
        setPortalCreateLogins(createlogin);
        setPortalDefaultPassword(defpass);
        
        setPortalConsentingForm(form);
        
        Rlog.debug("portal", "PortalJB.PortalJB(all parameters)");
    }
    
    /**
     * Retrieves the details of an portal in PortalStateKeeper Object
     * 
     * @return PortalStateKeeper consisting of the portal details
     * @see PortalStateKeeper
     */
    public PortalBean getPortalDetails() {
    	PortalBean apsk = null;
        try {
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            apsk = portalAgentRObj.getPortalDetails(this.portalId);
            Rlog.debug("portal", "In PortalJB.getPortalDetails() " + apsk);
        } catch (Exception e) {
            Rlog
                    .fatal("portal", "Error in getPortalDetails() in PortalJB "
                            + e);
        }
        if (apsk != null) {
            this.portalId = apsk.getPortalId();
            this.portalName = apsk.getPortalName();
            this.accountId = apsk.getAccountId();
            this.portalCreatedBy = apsk.getPortalCreatedBy();
            this.portalDesc = apsk.getPortalDesc();
            this.portalLevel = apsk.getPortalLevel();
            this.portalNotificationFlag = apsk.getPortalNotificationFlag();
            this.portalStatus = apsk.getPortalStatus();
            this.portalDispType = apsk.getPortalDispType();
            this.portalBgColor = apsk.getPortalBgColor();
            this.portalTxtColor = apsk.getPortalTxtColor();
            this.portalHeader = apsk.getPortalHeader();
            this.portalFooter = apsk.getPortalFooter();
            this.portalAuditUser = apsk.getPortalAuditUser();
            this.creator = apsk.getCreator();
            this.modifiedBy = apsk.getModifiedBy();
            this.ipAdd = apsk.getIpAdd();
            setPortalStudy(apsk.getPortalStudy());
            setPortalSelfLogout(apsk.getPortalSelfLogout());
            setPortalCreateLogins(apsk.getPortalCreateLogins());
            setPortalDefaultPassword(apsk.getPortalDefaultPassword());
            setPortalConsentingForm(apsk.getPortalConsentingForm());
        }
        return apsk;
    }
    
    public int setPortalDetails() {
        int output = 0;
        try {
        	PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            output = portalAgentRObj.setPortalDetails(this
                    .createPortalStateKeeper());
        	           
            this.setPortalId(output);
            Rlog.debug("portal", "PortalJB.setPortalDetails()");

        }catch (Exception e) {
            Rlog
                    .fatal("portal", "Error in setPortalDetails() in PortalJB "
                            + e);
          return -2;
        }
        return output;
    }
    
    public int updatePortal() {
        int output;
        try {
        	PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            output = portalAgentRObj.updatePortal(this
                    .createPortalStateKeeper());
        } catch (Exception e) {
            Rlog.debug("portal",
                    "EXCEPTION IN SETTING PORTAL DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    
    public PortalBean createPortalStateKeeper() {
        Rlog.debug("portal", "PortalJB.createPortalStateKeeper ");
        return new PortalBean(this.portalId, this.portalName, this.accountId,
                this.portalCreatedBy, this.portalDesc, this.portalLevel,
                this.portalNotificationFlag, this.portalStatus, this.portalDispType,
                this.portalBgColor,  this.portalTxtColor,  this.portalHeader,
                this.portalFooter,  this.portalAuditUser, this.creator,  this.modifiedBy,  this.ipAdd, this.portalStudy,this.portalSelfLogout,
                this.portalCreateLogins,this.portalDefaultPassword,this.portalConsentingForm);
    }
    
    
    public PortalDao getPortalValues(int accountId, int userId) {
    	PortalDao portalDao = new PortalDao();
        try {
            Rlog.debug("portal", "PortalJB.getPortalValues starting");
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            portalDao = portalAgentRObj.getPortalValues(accountId,userId);

            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "getPortalValues(int accountId) in PortalJB "
                            + e);
        }
        return portalDao;
    }
    
   
    
    public PortalDao getPortalValue(int portalId) {
    	PortalDao portalDao = new PortalDao();
        try {
            Rlog.debug("portal", "PortalJB.getPortalValue starting");
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            portalDao = portalAgentRObj.getPortalValue(portalId);

            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "getPortalValue(int portalId) in PortalJB "
                            + e);
        }
        return portalDao;
    }
    
    
    
    public PortalDao insertPortalValues(int pid,String portalPatPop,String selIds,int usr,int user,String ipAdd) {
    	PortalDao portalDao = new PortalDao();
        try {
            Rlog.debug("portal", "PortalJB.insertPortalValues starting");
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            portalDao = portalAgentRObj.insertPortalValues(pid,portalPatPop,selIds,usr,user,ipAdd);

            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "insertPortalValues(int pid,String portalPatPop,String selIds,int usr,int user,String ipAdd) in PortalJB "
                            + e);
        }
        return portalDao;
    }
    
    
    public PortalDao deletePortalPop(int portalId) {
    	PortalDao portalDao = new PortalDao();
        try {
            Rlog.debug("portal", "PortalJB.deletePortalPop starting");
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            portalDao = portalAgentRObj.deletePortalPop(portalId);

            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "deletePortalPop(int portalId) in PortalJB "
                            + e);
        }
        return portalDao;
    }
 // Overloaded for INF-18183 ::: Akshi
    public PortalDao deletePortalPop(int portalId, Hashtable<String, String> args) {
    	PortalDao portalDao = new PortalDao();
        try {
            Rlog.debug("portal", "PortalJB.deletePortalPop starting");
            PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
            portalDao = portalAgentRObj.deletePortalPop(portalId,args);

            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "deletePortalPop(int portalId) in PortalJB "
                            + e);
        }
        return portalDao;
    }
    public void deletePortal(int portalId) {
        try {

            PortalAgentRObj portalAgent = EJBUtil.getPortalAgentHome();
            
            Rlog.debug("portal", "PortalJB.deletePortal after remote");
            portalAgent.deletePortal(portalId);
            Rlog.debug("portal", "PortalJB.deletePortal after Dao");
        } catch (Exception e) {
            Rlog.fatal("portal", "Error in deletePortal in PortalJB " + e);
            e.printStackTrace();
        }
    }    
 // Overloaded for INF-18183 ::: Akshi
    public void deletePortal(int portalId, Hashtable<String, String> args) {
        try {

            PortalAgentRObj portalAgent = EJBUtil.getPortalAgentHome();
            
            Rlog.debug("portal", "PortalJB.deletePortal after remote");
            portalAgent.deletePortal(portalId,args);
            Rlog.debug("portal", "PortalJB.deletePortal after Dao");
        } catch (Exception e) {
            Rlog.fatal("portal", "Error in deletePortal in PortalJB " + e);
            e.printStackTrace();
        }
    }
    
    // PP-18306 AGodara
	public PortalDao getDesignPreview(int portalId, int patProtProtocolId) {
		
		PortalDao portalDao = new PortalDao();
		
		try {
			PortalAgentRObj portalAgentRObj = EJBUtil.getPortalAgentHome();
			portalDao = portalAgentRObj.getDesignPreview(portalId, patProtProtocolId);
			return portalDao;
		} catch (Exception e) {
			Rlog.fatal("portal",
					"Exception in getDesignPreview(int portalId,int patProtId) in PortalJB "
							+ e);
		}
		return portalDao;
	}
       
	public String getPortalStudy() {
		return portalStudy;
	}

	public void setPortalStudy(String portalStudy) {
		this.portalStudy = portalStudy;
	}
	
	public String getPortalSelfLogout() {
		return portalSelfLogout;
	}

	public void setPortalSelfLogout(String portalSelfLogout) {
		this.portalSelfLogout = portalSelfLogout;
	}
	

	public String getPortalCreateLogins() {
		return portalCreateLogins;
	}

	public void setPortalCreateLogins(String portalCreateLogins) {
		this.portalCreateLogins = portalCreateLogins;
	}

	
	public String getPortalDefaultPassword() {
		return portalDefaultPassword;
	}

	public void setPortalDefaultPassword(String portalDefaultPassword) {
		this.portalDefaultPassword = portalDefaultPassword;
	}
	
	/* create default logins for the portal according to patient population option*/
    public int createPatientLogins(int portalId, int creator, String ipAdd)
    {
    	int ret = 0;	
    	   try {
               PortalAgentRObj portalAgent = EJBUtil.getPortalAgentHome();
               ret =   portalAgent.createPatientLogins(portalId,creator,ipAdd);
 
           } catch (Exception e) {
               Rlog.fatal("portal", "Exception in createPatientLogins in PortalJB " + e);
               e.printStackTrace();
           }
          return ret; 
			
    }

	public String getPortalConsentingForm() {
		return portalConsentingForm;
	}

	public void setPortalConsentingForm(String portalConsentingForm) {
		this.portalConsentingForm = portalConsentingForm;
	}
}
