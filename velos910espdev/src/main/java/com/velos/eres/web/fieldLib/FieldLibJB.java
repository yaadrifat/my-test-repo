/*
 * Classname : FieldLibJB
 /*
 * Classname : FieldLibJB
 *
 * Version information: 1.0
 *
 * Date: 03/07/2003
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.web.fieldLib;

/* IMPORT STATEMENTS */
import com.velos.eres.business.common.FieldLibDao;
import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.common.Style;
import com.velos.eres.business.fieldLib.impl.FieldLibBean;
import com.velos.eres.business.fldValidate.impl.FldValidateBean;
import com.velos.eres.service.fieldLibAgent.FieldLibAgentRObj;
import com.velos.eres.service.fieldLibAgent.impl.FieldLibAgentBean;
import com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.tags.VEditBox;
import com.velos.eres.tags.VHr;
import com.velos.eres.tags.VLabel;
import com.velos.eres.tags.VLinkForm;
import com.velos.eres.tags.VLookup;
import com.velos.eres.tags.VMultiInput;
import com.velos.eres.tags.VSelect;
import com.velos.eres.tags.VTag;
import com.velos.eres.tags.VTextArea;
import com.velos.eres.web.formField.FormFieldJB;
import com.velos.eres.web.formLib.FormLibJB;
import com.velos.eres.web.formSec.FormSecJB;
import com.velos.eres.web.linkedForms.LinkedFormsJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for FieldLib
 *
 * @author Sonia Kaura
 * @version 1.0 03/07/2003
 */
/*
 * Modified by Sonia Abrol , 02/10/05- to escape spl char '<' and '>' for
 * comments and field validations
 */
public class FieldLibJB {

    /**
     * The primary key for the Field Library Table : pk_field
     */
    private int fieldLibId;

    /**
     * The foreign key reference to the Category Library: FK_CATLIB
     */

    private String catLibId;

    /**
     * The foreign key reference to the Account Table: FK_ACCOUNT
     */

    private String accountId;

    /**
     * The Library Flag to display if the field is within the library or belongs
     * <br>
     * to a form : FLD_LIBFLAG
     */

    private String libFlag;

    /**
     * The Name of the Field Component : FLD_NAME
     */

    private String fldName;

    /**
     * The description of the Field Component: FLD_DESC
     */

    private String fldDesc;

    /**
     * The Field Component Unique ID : FLD_UNIQUEID
     */

    private String fldUniqId;

    /**
     * The Field Component System ID: FLD_SYSTEMID
     *
     */

    private String fldSysID;

    /**
     *
     * The Field Component Key Word :FLD_KEYWORD
     */

    private String fldKeyword;

    /**
     *
     * The Field Type of the Field Component
     */

    private String fldType;

    /**
     *
     * The Datatype of the Field Component :FLD_DATATYPE
     *
     */

    private String fldDataType;

    /**
     *
     * The Instructions for the Field Component : FLD_INSTRUCTIONS
     *
     */

    private String fldInstructions;

    /**
     *
     * The length of the Field Component :FLD_LENGTH
     *
     */

    private String fldLength;

    /**
     * : FLD_DECIMAL
     *
     */

    private String fldDecimal;

    /**
     *
     * The number of lines in the field : FLD_LINESNO
     *
     */

    private String fldLinesNo;

    /**
     *
     * The number of characters in the field : FLD_CHARSNO
     */

    private String fldCharsNo;

    /**
     *
     * The Default Response of the Field Component: FLD_DEFRESP
     *
     */

    private String fldDefResp;

    /**
     * The foreign key reference to the Look up table : FK_LOOKUP
     *
     */

    private String lookup;

    /**
     *
     * The attribute of the field component if its uniqe: FLD_ISUNIQUE
     *
     *
     */

    private String fldIsUniq;

    /**
     *
     * The attribute of the field component if its read only : FLD_ISREADONLY
     *
     */

    private String fldIsRO;

    /**
     *
     * The attribute of the field component if its visible : FLD_ISVISIBLE
     *
     */

    private String fldIsVisible;

    /**
     *
     * The attribute of the field component the number of columns it has:
     * FLD_COLCOUNT
     *
     */

    private String fldColCount;

    /**
     *
     * The attribute of the field component of format of the field: FLD_FORMAT
     *
     */

    private String fldFormat;

    /**
     *
     * The attribute of the field component of the number of times it repeats:
     * FLD_REPEATFLAG
     *
     */

    private String fldRepeatFlag;

    /**
     *
     *
     * The record type of the field type : RECORD_TYPE
     *
     */

    private String recordType;

    /**
     *
     *
     * The creator of the field type : CREATOR
     *
     */

    private String creator;

    /**
     *
     * The id of the user who last modified the field : LAST_MODIFIED_BY
     *
     */

    private String modifiedBy;

    /**
     *
     * The IP Address of the site which made the change : IP_ADD
     *
     */

    private String ipAdd;

    /**
     * The Style of the field kept as a Style object
     */

    private Style aStyle;

    /**
     * The Lookup Display Value to be retrieved
     */

    private String lkpDisplayVal;

    /**
     * The Lookip Data Value to be retrieved
     */

    private String lkpDataVal;

    /**
     * Data greater than Today's date or not
     */
    private String todayCheck;

    /**
     * override mandatory validation for a field or not
     */

    private String overRideMandatory;

    /**
     * override number range validation for a field or not
     */

    private String overRideRange;

    /**
     * override number format validation for a field or not
     */

    private String overRideFormat;

    /**
     * override date validation for a field or not
     */

    private String overRideDate;

    /**
     * Field Lookup type
     */

    private String fldLkpType;

    /**
     * expand field or not
     */

    private String expLabel;

    private String fldXSL;

    private String fldJavaScript;

    private String browserFlag;

    private FldValidateBean fldValStatekeeper;

    /**
     * flag to hide field label in preview, Possible values - 0:No, 1:Yes'
     */

    private String fldHideLabel;

    /**
     * flag to hide response label in form preview, Possible values - 0:No,
     * 1:Yes
     */

    private String fldHideResponseLabel;

    /**
     * This width of the column in which field label is displayed in preview
     * mode
     */

    private String fldDisplayWidth;

    /**
     * This column stores pk_formlib of the form linked with this field, for
     * fldType='F'
     */

    private String fldLinkedForm;

    /**
     * Aligment atribute for field response label
     */

    private String fldResponseAlign;

    /**
     * Formatted Field name definition
     */
    private String fldNameFormat;

    /**
     * Sort Order defined for field
     */
    private String fldSortOrder;

    private String jsNumberRange;

    private static final String lineSeparator = System.getProperty("line.separator") != null ?
            System.getProperty("line.separator") : "\n";

    // GETTERS AND SETTERS

    /**
     *
     *
     * @return fieldLibId
     */
    public int getFieldLibId() {
        return this.fieldLibId;
    }

    /**
     *
     *
     * @param fieldLibId
     */
    public void setFieldLibId(int fieldLibId) {
        this.fieldLibId = fieldLibId;
    }

    /**
     *
     *
     * @return catLibId
     */
    public String getCatLibId() {
        return this.catLibId;
    }

    /**
     *
     *
     * @param catLibId
     */
    public void setCatLibId(String catLibId) {
        this.catLibId = catLibId;
    }

    /**
     *
     *
     * @return accountId
     */
    public String getAccountId() {
        return this.accountId;
    }

    /**
     *
     *
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     *
     *
     * @return libFlag
     */
    public String getLibFlag() {
        return this.libFlag;
    }

    /**
     *
     *
     * @param libFlag
     */
    public void setLibFlag(String libFlag) {
        this.libFlag = libFlag;
    }

    /**
     *
     *
     * @return fldName
     */
    public String getFldName() {
        return this.fldName;
    }

    /**
     *
     *
     * @param fldName
     */
    public void setFldName(String fldName) {
        this.fldName = fldName;
    }

    /**
     *
     *
     * @return fldDesc
     */
    public String getFldDesc() {
        return this.fldDesc;
    }

    /**
     *
     *
     * @param fldDesc
     */
    public void setFldDesc(String fldDesc) {
        this.fldDesc = fldDesc;
    }

    /**
     *
     *
     * @return fldUniqId
     */
    public String getFldUniqId() {
        return this.fldUniqId;
    }

    /**
     *
     *
     * @param fldUniqId
     */
    public void setFldUniqId(String fldUniqId) {
        this.fldUniqId = fldUniqId;
    }

    /**
     *
     *
     * @return fldSysID
     */
    public String getFldSysID() {
        return this.fldSysID;
    }

    /**
     *
     *
     * @param fldSysID
     */
    public void setFldSysID(String fldSysID) {
        this.fldSysID = fldSysID;
    }

    /**
     *
     *
     * @return fldKeyword
     */
    public String getFldKeyword() {
        return this.fldKeyword;
    }

    /**
     *
     *
     * @param fldKeyword
     */
    public void setFldKeyword(String fldKeyword) {
        this.fldKeyword = fldKeyword;
    }

    /**
     *
     *
     * @return fldType
     */
    public String getFldType() {
        return this.fldType;
    }

    /**
     *
     *
     * @param fldType
     */
    public void setFldType(String fldType) {
        this.fldType = fldType;
    }

    /**
     *
     *
     * @return fldDataType
     */
    public String getFldDataType() {
        return this.fldDataType;
    }

    /**
     *
     *
     * @param fldDataType
     */
    public void setFldDataType(String fldDataType) {
        this.fldDataType = fldDataType;
    }

    /**
     *
     *
     * @return fldInstructions
     */
    public String getFldInstructions() {
        return this.fldInstructions;
    }

    /**
     *
     *
     * @param fldInstructions
     */
    public void setFldInstructions(String fldInstructions) {
        this.fldInstructions = fldInstructions;
    }

    /**
     *
     *
     * @return fldLength
     */
    public String getFldLength() {
        return this.fldLength;
    }

    /**
     *
     *
     * @param fldLength
     */
    public void setFldLength(String fldLength) {
        this.fldLength = fldLength;
    }

    /**
     *
     *
     * @return fldDecimal
     */
    public String getFldDecimal() {
        return this.fldDecimal;
    }

    /**
     *
     *
     * @param fldDecimal
     */
    public void setFldDecimal(String fldDecimal) {
        this.fldDecimal = fldDecimal;
    }

    /**
     *
     *
     * @return fldLinesNo
     */
    public String getFldLinesNo() {
        return this.fldLinesNo;
    }

    /**
     *
     *
     * @param fldLinesNo
     */
    public void setFldLinesNo(String fldLinesNo) {
        this.fldLinesNo = fldLinesNo;
    }

    /**
     *
     *
     * @return fldCharsNo
     */
    public String getFldCharsNo() {
        return this.fldCharsNo;
    }

    /**
     *
     *
     * @param fldCharsNo
     */
    public void setFldCharsNo(String fldCharsNo) {
        this.fldCharsNo = fldCharsNo;
    }

    /**
     *
     *
     * @return fldDefResp
     */
    public String getFldDefResp() {
        return this.fldDefResp;
    }

    /**
     *
     *
     * @param fldDefResp
     */
    public void setFldDefResp(String fldDefResp) {
        this.fldDefResp = fldDefResp;
    }

    /**
     *
     *
     * @return lookup
     */
    public String getLookup() {
        return this.lookup;
    }

    /**
     *
     *
     * @param lookup
     */
    public void setLookup(String lookup) {
        this.lookup = lookup;
    }

    /**
     *
     *
     * @return fldIsUniq
     */
    public String getFldIsUniq() {
        return this.fldIsUniq;
    }

    /**
     *
     *
     * @param fldIsUniq
     */
    public void setFldIsUniq(String fldIsUniq) {
        this.fldIsUniq = fldIsUniq;
    }

    /**
     *
     *
     * @return fldIsRO
     */
    public String getFldIsRO() {
        return this.fldIsRO;
    }

    /**
     *
     *
     * @param fldIsRO
     */
    public void setFldIsRO(String fldIsRO) {
        this.fldIsRO = fldIsRO;
        Rlog.debug("fieldlib", "** this.fldIsRO " + this.fldIsRO);
    }

    /**
     *
     *
     * @return fldIsVisible
     */
    public String getFldIsVisible() {
        return this.fldIsVisible;
    }

    /**
     *
     *
     * @param fldIsVisible
     */
    public void setFldIsVisible(String fldIsVisible) {

        this.fldIsVisible = fldIsVisible;
        Rlog.debug("fieldlib", "** this.fldIsVisible " + this.fldIsVisible);
    }

    /**
     *
     *
     * @return fldColCount
     */
    public String getFldColCount() {
        return this.fldColCount;
    }

    /**
     *
     *
     * @param fldColCount
     */
    public void setFldColCount(String fldColCount) {
        this.fldColCount = fldColCount;
    }

    /**
     *
     *
     * @return fldFormat
     */
    public String getFldFormat() {
        return this.fldFormat;
    }

    /**
     *
     *
     * @param fldFormat
     */
    public void setFldFormat(String fldFormat) {
        this.fldFormat = fldFormat;
    }

    /**
     *
     *
     * @return fldRepeatFlag
     */
    public String getFldRepeatFlag() {
        return this.fldRepeatFlag;
    }

    /**
     *
     *
     * @param fldRepeatFlag
     */
    public void setFldRepeatFlag(String fldRepeatFlag) {
        this.fldRepeatFlag = fldRepeatFlag;
    }

    public String getBrowserFlag() {
        return this.browserFlag;
    }

    public void setBrowserFlag(String browserFlag) {
        this.browserFlag = browserFlag;
    }

    /**
     *
     *
     * @return recordType
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     *
     *
     * @param recordType
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     *
     *
     * @return creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     *
     *
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     *
     *
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     *
     *
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     *
     *
     * @return ipAdd
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     *
     *
     * @param ipAdd
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     *
     *
     * @return aStyle
     */
    public Style getAStyle() {
        return this.aStyle;
    }

    /**
     *
     *
     * @param aStyle
     */
    public void setAStyle(Style aStyle) {
        this.aStyle = aStyle;
    }

    /**
     *
     *
     * @return lkpDisplayVal
     */
    public String getLkpDisplayVal() {
        return this.lkpDisplayVal;
    }

    /**
     *
     *
     * @param lkpDisplayVal
     */
    public void setLkpDisplayVal(String lkpDisplayVal) {
        this.lkpDisplayVal = lkpDisplayVal;
    }

    /**
     *
     *
     * @return lkpDataVal
     */
    public String getLkpDataVal() {
        return this.lkpDataVal;
    }

    /**
     *
     *
     * @param lkpDataVal
     */
    public void setLkpDataVal(String lkpDataVal) {
        this.lkpDataVal = lkpDataVal;
    }

    /**
     *
     *
     * @return overRideMandatory
     */
    public String getOverRideMandatory() {
        return this.overRideMandatory;
    }

    /**
     *
     *
     * @param overRideMandatory
     */
    public void setOverRideMandatory(String overRideMandatory) {
        this.overRideMandatory = overRideMandatory;
    }

    /**
     *
     *
     * @return overRideRange
     */
    public String getOverRideRange() {
        return this.overRideRange;
    }

    /**
     *
     *
     * @param overRideRange
     */
    public void setOverRideRange(String overRideRange) {
        this.overRideRange = overRideRange;
    }

    /**
     *
     *
     * @return overRideFormat
     */
    public String getOverRideFormat() {
        return this.overRideFormat;
    }

    /**
     *
     *
     * @param overRideFormat
     */
    public void setOverRideFormat(String overRideFormat) {
        this.overRideFormat = overRideFormat;
    }

    /**
     *
     *
     * @return overRideDate
     */
    public String getOverRideDate() {
        return this.overRideDate;
    }

    /**
     *
     *
     * @param overRideDate
     */
    public void setOverRideDate(String overRideDate) {
        this.overRideDate = overRideDate;
    }

    /**
     *
     *
     * @return todayCheck
     */
    public String getTodayCheck() {
        Rlog.debug("fieldlib", "getTodayCheck:this.todayCheck "
                + this.todayCheck);
        return this.todayCheck;

    }

    /**
     *
     *
     * @param todayCheck
     */
    public void setTodayCheck(String todayCheck) {
        Rlog.debug("fieldlib", "setTodayCheck:todayCheck " + todayCheck);
        this.todayCheck = todayCheck;
    }

    /**
     *
     *
     * @return fldLkpType
     */
    public String getFldLkpType() {
        return this.fldLkpType;
    }

    /**
     *
     *
     * @param fldLkpType
     */
    public void setFldLkpType(String fldLkpType) {
        this.fldLkpType = fldLkpType;
    }

    /**
     *
     *
     * @return expLabel
     */
    public String getExpLabel() {
        return this.expLabel;
    }

    /**
     *
     *
     * @param expLabel
     */
    public void setExpLabel(String expLabel) {
        this.expLabel = expLabel;
    }

    public String getFldXSL() {
        return this.fldXSL;
    }

    public void setFldXSL(String fldXSL) {
        this.fldXSL = fldXSL;
    }

    public String getFldJavaScript() {
        return this.fldJavaScript;
    }

    public void setFldJavaScript(String fldJavaScript) {
        this.fldJavaScript = fldJavaScript;
    }

    public FldValidateBean getFldValidateSk() {
        return this.fldValStatekeeper;
    }

    public void setFldValidateSk(FldValidateBean fldValStatekeeper) {

        Rlog.debug("fieldlib", "in jb fldValStatekeeper val1 "
                + fldValStatekeeper.getFldValidateVal1());
        this.fldValStatekeeper = fldValStatekeeper;
    }

    /**
     * Returns the value of fldHideLabel.
     */
    public String getFldHideLabel() {
        return fldHideLabel;
    }

    /**
     * Sets the value of fldHideLabel.
     *
     * @param fldHideLabel
     *            The value to assign fldHideLabel.
     */
    public void setFldHideLabel(String fldHideLabel) {
        this.fldHideLabel = fldHideLabel;
    }

    /**
     * Returns the value of fldHideResponseLabel.
     */
    public String getFldHideResponseLabel() {
        return fldHideResponseLabel;
    }

    /**
     * Sets the value of fldHideResponseLabel.
     *
     * @param fldHideResponseLabel
     *            The value to assign fldHideResponseLabel.
     */
    public void setFldHideResponseLabel(String fldHideResponseLabel) {
        this.fldHideResponseLabel = fldHideResponseLabel;
    }

    /**
     * Returns the value of fldDisplayWidth.
     */
    public String getFldDisplayWidth() {
        return fldDisplayWidth;
    }

    /**
     * Sets the value of fldDisplayWidth.
     *
     * @param fldDisplayWidth
     *            The value to assign fldDisplayWidth.
     */
    public void setFldDisplayWidth(String fldDisplayWidth) {
        this.fldDisplayWidth = fldDisplayWidth;
    }

    /**
     * Returns the value of fldLinkedForm.
     */
    public String getFldLinkedForm() {
        return fldLinkedForm;
    }

    /**
     * Sets the value of fldLinkedForm.
     *
     * @param fldLinkedForm
     *            The value to assign fldLinkedForm.
     */
    public void setFldLinkedForm(String fldLinkedForm) {
        this.fldLinkedForm = fldLinkedForm;
    }

    /**
     * Returns the value of fldResponseAlign.
     */
    public String getFldResponseAlign() {
        return fldResponseAlign;
    }

    /**
     * Sets the value of fldResponseAlign.
     *
     * @param fldResponseAlign
     *            The value to assign fldResponseAlign.
     */
    public void setFldResponseAlign(String fldResponseAlign) {
        this.fldResponseAlign = fldResponseAlign;
    }

    /**
     * Returns the value of fldNameFormat.
     */
    public String getFldNameFormat() {
        return fldNameFormat;
    }

    /**
     * Sets the value of fldNameFormat.
     *
     * @param fldNameFormat
     *            The value to assign fldNameFormat.
     */
    public void setFldNameFormat(String fldNameFormat) {
        this.fldNameFormat = StringUtil.escapeSpecialCharHTML(fldNameFormat);
    }

    /**
     * Returns the value of fldSortOrder.
     */
    public String getFldSortOrder() {
        return fldSortOrder;
    }

    /**
     * Sets the value of fldSortOrder.
     *
     * @param fldSortOrder
     *            The value to assign fldSortOrder.
     */
    public void setFldSortOrder(String fldSortOrder) {
        this.fldSortOrder = fldSortOrder;
    }

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     *
     * @param fieldLId:
     *            Unique Id constructor
     *
     */

    public FieldLibJB(int fieldLId) {
        setFieldLibId(fieldLId);
    }

    /**
     * Default Constructor
     */

    public FieldLibJB() {
        Rlog.debug("fieldlib", "FieldLibJB.FieldLibJB() ");
    }

    /**
     * Full arguments constructor.
     *
     * @param fieldLibId
     * @param catLibId
     * @param accountId
     * @param libFlag
     * @param fldName
     * @param fldDesc
     * @param fldUniqId
     * @param fldSysID
     * @param fldKeyword
     * @param fldType
     * @param fldDataType
     * @param fldInstructions
     * @param fldLength
     * @param fldDecimal
     * @param fldLinesNo
     * @param fldCharsNo
     * @param fldDefResp
     * @param lookup
     * @param fldIsUniq
     * @param fldIsRO
     * @param fldIsVisible
     * @param fldColCount
     * @param fldFormat
     * @param fldRepeatFlag
     * @param recordType
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     * @param aStyle
     * @param lkpDisplayVal
     * @param lkpDataVal
     * @param overRideMandatory
     * @param overRideFormat
     * @param overRideRange
     * @param overRideDate
     * @param fldLkpType
     * @param expLabel
     * @param fldHideLabel
     * @param fldHideResponseLabel
     * @param fldDisplayWidth
     * @param fldLinkedForm
     * @param fldResponseAlign
     * @param fldNameFormat
     * @param fldSortOrder
     */

    public FieldLibJB(int fieldLibId, String catLibId, String accountId,
            String libFlag, String fldName, String fldDesc, String fldUniqId,
            String fldSysID, String fldKeyword, String fldType,
            String fldDataType, String fldInstructions, String fldLength,
            String fldDecimal, String fldLinesNo, String fldCharsNo,
            String fldDefResp, String lookup, String fldIsUniq, String fldIsRO,
            String fldIsVisible, String fldColCount, String fldFormat,
            String fldRepeatFlag, String recordType, String creator,
            String modifiedBy, String ipAdd, Style aStyle,
            String lkpDisplayVal, String lkpDataVal, String todayCheck,
            String overRideMandatory, String overRideFormat,
            String overRideRange, String overRideDate, String fldLkpType,
            String expLabel, String fldHideLabel, String fldHideResponseLabel,
            String fldDisplayWidth, String fldLinkedForm,
            String fldResponseAlign, String fldNameFormat, String fldSortOrder) {
        setFieldLibId(fieldLibId);
        setCatLibId(catLibId);
        setAccountId(accountId);
        setLibFlag(libFlag);
        setFldName(fldName);
        setFldDesc(fldDesc);
        setFldUniqId(fldUniqId);
        setFldSysID(fldSysID);
        setFldKeyword(fldKeyword);
        setFldDataType(fldDataType);
        setFldType(fldType);
        setFldInstructions(fldInstructions);
        setFldLength(fldLength);
        setFldDecimal(fldDecimal);
        setFldLinesNo(fldLinesNo);
        setFldCharsNo(fldCharsNo);
        setFldDefResp(fldDefResp);
        setLookup(lookup);
        setFldIsUniq(fldIsUniq);
        setFldIsRO(fldIsRO);
        setFldIsVisible(fldIsVisible);
        setFldColCount(fldColCount);
        setFldFormat(fldFormat);
        setFldRepeatFlag(fldRepeatFlag);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAStyle(aStyle);
        setLkpDisplayVal(lkpDisplayVal);
        setLkpDataVal(lkpDataVal);
        setTodayCheck(todayCheck);
        setOverRideMandatory(overRideMandatory);
        setOverRideFormat(overRideFormat);
        setOverRideRange(overRideRange);
        setOverRideDate(overRideDate);
        setFldLkpType(fldLkpType);
        setExpLabel(expLabel);
        setFldHideLabel(fldHideLabel);
        setFldHideResponseLabel(fldHideResponseLabel);
        setFldDisplayWidth(fldDisplayWidth);
        setFldLinkedForm(fldLinkedForm);
        setFldResponseAlign(fldResponseAlign);
        setFldNameFormat(fldNameFormat);
        setFldSortOrder(fldSortOrder);

    }

    /**
     * Calls getFieldLibDetails of FieldLib Session Bean: FieldLibAgentBean
     *
     * @return
     * @see FieldLibAgentBean
     */

    public FieldLibBean getFieldLibDetails() {
        FieldLibBean fieldLsk = null;

        FldValidateBean fldValsk = null;
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLsk = fieldLibAgentRObj.getFieldLibDetails(this.fieldLibId);
        } catch (Exception e) {
            Rlog.fatal("fieldlib", "Error in FieldLib getFieldLibDetails" + e);
            e.printStackTrace();
        }

        if (fieldLsk != null) {

            setFieldLibId(fieldLsk.getFieldLibId());
            setCatLibId(fieldLsk.getCatLibId());
            setAccountId(fieldLsk.getAccountId());
            setLibFlag(fieldLsk.getLibFlag());
            setFldName(fieldLsk.getFldName());
            setFldDesc(fieldLsk.getFldDesc());
            setFldUniqId(fieldLsk.getFldUniqId());
            setFldSysID(fieldLsk.getFldSysID());
            setFldKeyword(fieldLsk.getFldKeyword());
            setFldType(fieldLsk.getFldType());
            setFldDataType(fieldLsk.getFldDataType());
            setFldInstructions(fieldLsk.getFldInstructions());
            setFldLength(fieldLsk.getFldLength());
            setFldDecimal(fieldLsk.getFldDecimal());
            setFldLinesNo(fieldLsk.getFldLinesNo());
            setFldCharsNo(fieldLsk.getFldCharsNo());
            setFldDefResp(fieldLsk.getFldDefResp());
            setLookup(fieldLsk.getLookup());
            setFldIsUniq(fieldLsk.getFldIsUniq());
            setFldIsRO(fieldLsk.getFldIsRO());
            setFldIsVisible(fieldLsk.getFldIsVisible());
            setFldColCount(fieldLsk.getFldColCount());
            setFldFormat(fieldLsk.getFldFormat());
            setFldRepeatFlag(fieldLsk.getFldRepeatFlag());
            setRecordType(fieldLsk.getRecordType());
            setCreator(fieldLsk.getCreator());
            setModifiedBy(fieldLsk.getModifiedBy());
            setIpAdd(fieldLsk.getIpAdd());
            setAStyle(fieldLsk.getAStyle());
            setLkpDisplayVal(fieldLsk.getLkpDisplayVal());
            setLkpDataVal(fieldLsk.getLkpDataVal());
            setTodayCheck(fieldLsk.getTodayCheck());
            setOverRideMandatory(fieldLsk.getOverRideMandatory());
            setOverRideFormat(fieldLsk.getOverRideFormat());
            setOverRideRange(fieldLsk.getOverRideRange());
            setOverRideDate(fieldLsk.getOverRideDate());
            setFldLkpType(fieldLsk.getFldLkpType());
            setExpLabel(fieldLsk.getExpLabel());
            setFldHideLabel(fieldLsk.getFldHideLabel());
            setFldHideResponseLabel(fieldLsk.getFldHideResponseLabel());
            setFldDisplayWidth(fieldLsk.getFldDisplayWidth());
            setFldLinkedForm(fieldLsk.getFldLinkedForm());
            setFldResponseAlign(fieldLsk.getFldResponseAlign());
            setFldNameFormat(fieldLsk.getFldNameFormat());
            setFldSortOrder(fieldLsk.getFldSortOrder());

            Rlog.debug("fieldlib", "field lib id " + fieldLsk.getFieldLibId());
            try {
                FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                        .getFldValidateAgentHome();
                fldValsk = fldValidateAgentRObj.findByFieldId(fieldLsk
                        .getFieldLibId());
                Rlog.debug("fieldlib",
                        "FieldLibJB.getFieldLibDetails() fldValsk" + fldValsk);
                if (fldValsk != null) {
                    setFldValidateSk(fldValsk);
                    fldValStatekeeper.generateJavaScript(this.fldSysID, this.overRideRange, this.fldName);
                    jsNumberRange = fldValStatekeeper.fldValidateJavaScr;
                    // fieldLsk.setFldValidateSk(fldValsk);
                    Rlog.debug("fieldlib",
                            "FieldLibJB.getFieldLibDetails() getFldValidateOp1"
                                    + fldValsk.getFldValidateOp1());
                    Rlog.debug("fieldlib",
                            "FieldLibJB.getFieldLibDetails() getFldValidateVal1"
                                    + fldValsk.getFldValidateVal1());
                    Rlog.debug("fieldlib",
                            "FieldLibJB.getFieldLibDetails() getFldValidateOP2"
                                    + fldValsk.getFldValidateOp2());
                    Rlog.debug("fieldlib",
                            "FieldLibJB.getFieldLibDetails() getFldValidateVal2"
                                    + fldValsk.getFldValidateVal2());
                    Rlog.debug("fieldlib",
                            "FieldLibJB.getFieldLibDetails() getFldValidateLogOp1"
                                    + fldValsk.getFldValidateLogOp1());
                }
            } catch (Exception e) {
                Rlog.fatal("fieldlib",
                        "Error in FieldLib getFieldLibDetails try catch" + e);
            }
        }
        return fieldLsk;

    }

    /**
     * This method is an overloaded method. Would be called in case of multiple
     * choice fields will first insert the field and then its responses
     * responses are stored in DAO Calls setFieldLibDetails() of Session Bean:
     * FieldLibAgentBean when the field is inserted, it calls the DAO for
     * inserting multiple responses
     *
     * @param FldRespDao -
     *            dao with multiple responses
     * @returns fieldId
     */

    public int setFieldLibDetails(FldRespDao fldRespDao) {
        int toReturn;
        try {
            Rlog.debug("fieldlib", "FieldLibJB.setFieldLibDetails()");
            FieldLibAgentRObj FieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            FieldLibBean tempStateKeeper = new FieldLibBean();
            tempStateKeeper = this.createFieldLibStateKeeper();

            toReturn = FieldLibAgentRObj.setFieldLibDetails(tempStateKeeper,
                    fldRespDao);
            this.setFieldLibId(toReturn);

            if (toReturn > 0) {
                fldRespDao.setField(EJBUtil.integerToString(new Integer(
                        toReturn)));
                // if the field is stored, store the multiple responses
                FldRespDao respDao = new FldRespDao();
                respDao.insertMultipleResponses(fldRespDao);
            }
            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in setFieldLibDetails() in FieldLibJB " + e);
            return -2;
        }
    }

    /**
     * Calls setFieldLibDetails() of FieldLib Session Bean: FieldLibAgentBean
     *
     */

    public int setFieldLibDetails() {
        int toReturn;
        int pk_fldVal = 0;
        try {
            Rlog.debug("fieldlib", "inside setdetails ");
            FieldLibAgentRObj FieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            FieldLibBean tempStateKeeper = new FieldLibBean();
            tempStateKeeper = this.createFieldLibStateKeeper();

            toReturn = FieldLibAgentRObj.setFieldLibDetails(tempStateKeeper);

            this.setFieldLibId(toReturn);
            Rlog.debug("fieldlib", "inside setFieldLibDetails()" + toReturn);
            setFieldLibId(toReturn);

            if (toReturn > 0) {
                Rlog.debug("fieldlib", "Inside when pk_fld is greater than 0");

                FldValidateBean fvsk = getFldValidateSk();
                if (fvsk != null) {
                    fvsk.setFldLibId(Integer.toString(toReturn));
                    tempStateKeeper.setFldValidateBean(fvsk);
                    fvsk.setFldLibId(Integer.toString(toReturn));

                    FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                            .getFldValidateAgentHome();

                    pk_fldVal = fldValidateAgentRObj
                            .setFldValidateDetails(fvsk);
                    Rlog.debug("fieldlib",
                            "insertToFormField:Value of pk_fldval" + pk_fldVal);
                }
            }

            Rlog.debug("fieldlib", "FieldLibJB.setFieldLibDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in setFieldLibDetails() in FieldLibJB " + e);
            return -2;
        }
    }

    /**
     *
     * @return a statekeeper object for the FieldLib Record with the current
     *         values of the bean
     */
    public FieldLibBean createFieldLibStateKeeper() {
        Rlog.debug("fieldlib", "FieldLibJB.createFieldLibStateKeeper ");

        return new FieldLibBean(fieldLibId, catLibId, accountId, libFlag,
                fldName, fldDesc, fldUniqId, fldSysID, fldKeyword, fldType,
                fldDataType, fldInstructions, fldLength, fldDecimal,
                fldLinesNo, fldCharsNo, fldDefResp, lookup, fldIsUniq, fldIsRO,
                fldIsVisible, fldColCount, fldFormat, fldRepeatFlag,
                recordType, creator, modifiedBy, ipAdd, aStyle, lkpDisplayVal,
                lkpDataVal, todayCheck, overRideMandatory, overRideRange,
                overRideFormat, overRideDate, fldLkpType, expLabel,
                fldHideLabel, fldHideResponseLabel, fldDisplayWidth,
                fldLinkedForm, fldResponseAlign, fldNameFormat, fldSortOrder);

    }

    /**
     * Calls updateFieldLib() of FieldLib Session Bean: FieldLibAgentBean
     *
     * @return the Primary Key of the new field
     */
    public int updateFieldLib() {
        int output;
        int retFldVal = 0;
        int fldValId = 0;
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            output = fieldLibAgentRObj.updateFieldLib(this
                    .createFieldLibStateKeeper());

            FldValidateBean fvsk = getFldValidateSk();

            // fvsk.setFldLibId(Integer.toString(fldLibId));

            FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            Rlog.debug("fieldlib", "updateFieldLib:After statekeeper");

            if (fvsk != null) {
                retFldVal = fldValidateAgentRObj.updateFldValidate(fvsk);
                Rlog.debug("fieldlib", "updateFieldLib:Value of retFldVal"
                        + retFldVal);
            }

        } catch (Exception e) {
            Rlog.debug("fieldlib",
                    "EXCEPTION IN SETTING FIELD  LIB DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * This method is an overloaded method. Would be called in case of multiple
     * choice fields will first update the field and then its responses
     * responses are stored in DAO Calls updateFieldLib() of Session Bean:
     * FieldLibAgentBean when the field is updated, it calls the DAO for
     * updating multiple responses
     *
     * @param FldRespDao -
     *            dao with multiple responses
     * @returns
     */

    public int updateFieldLib(FldRespDao fldRespDao) {
        int output;
        try {
            Rlog.debug("fieldlib",
                    "In updateFieldLib(FldRespDao fldRespDao) in FieldLibJB");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            output = fieldLibAgentRObj.updateFieldLib(this
                    .createFieldLibStateKeeper());

            if (output == 0) {
                // if the field is updated, update the multiple responses
                FldRespDao respDao = new FldRespDao();
                respDao.updateMultipleResponses(fldRespDao);
            }

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION IN UPDATING FIELD  LIB DETAILS in FieldLibJB"
                            + e);
            return -2;
        }
        return output;
    }

    /*
     * Would be called to insert multiple choice fields responses are stored in
     * DAO it calls FieldRespDAO for inserting multiple responses @param
     * FldRespDao - dao with multiple responses @returns -1 incase of error
     */

    public int insertFieldLibResponses(FldRespDao fldRespDao) {
        int toReturn;
        try {
            Rlog.debug("fieldlib",
                    "insertFieldLibResponses(FldRespDao fldRespDao)");
            // if the field is stored, store the multiple responses
            FldRespDao respDao = new FldRespDao();
            toReturn = respDao.insertMultipleResponses(fldRespDao);
            return toReturn;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in insertFieldLibResponses(FldRespDao fldRespDao)"
                            + e);
            return -1;
        }

    }

    /**
     * The method to remove a field given a Primary Key
     *
     * @return
     */
    public int removeFieldLib() {

        int output;

        try {

            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            output = fieldLibAgentRObj.removeFieldLib(this.fieldLibId);
            return output;

        } catch (Exception e) {
            Rlog.debug("fieldlib", "in FieldLibJB -removeFieldLib() method");
            return -1;
        }

    }

    /**
     *
     *
     * @param pkField
     * @param libFlag
     * @param user
     * @param ipAdd
     * @return The sucess status of the update
     */
    public int updateFieldLibRecord(int pkField, String libFlag, String user,
            String ipAdd) {
        int ret;
        try {
            Rlog.debug("fieldlib", "FieldLibJB.updateFieldLibRecord starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            ret = fieldLibAgentRObj.updateFieldLibRecord(pkField, libFlag,
                    user, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in updateFieldLibRecord in FieldLibJB " + e);
            return -2;
        }
        return ret;
    }

    /**
     * Gets the field attributes from the table depending on the search criteria
     * taken <br>
     * as parameters in this method
     *
     * @param fkCatlib
     * @param fkAccount
     * @param fkFormId
     * @param name
     * @param keyword
     * @return FieldLibDao
     */
    public FieldLibDao getFieldsFromSearch(int fkCatlib, int fkAccount,
            int fkFormId, String name, String keyword) {
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            Rlog.debug("fieldlib", "FieldLibJB.getFieldsFromSearch starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFieldsFromSearch(fkCatlib,
                    fkAccount, fkFormId, name, keyword);

            return fieldLibDao;
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "fieldlib",
                            "Error in getFieldsFromSearch(int fkCatlib,int fkAccount,int fkFormId, String name, String keyword) in FieldLibJB "
                                    + e);
        }
        return fieldLibDao;
    }

    /**
     * Returns an XSL representation of the field
     *
     * @return String
     */

    public void toXSL(boolean required, boolean softCheckStar) {

        /* generate field label */

        /* a label will be required for all field types, except <HR> */
        String dispFldName = "";
        String dispFldInst = "";
        if (EJBUtil.isEmpty(fldSysID))
            return;

        String fldRO = "";

        String hideLabel = "";
        String hideResponseLabel = "";
        String alignResponseLabel = "";
        String labelDisplayWidth = "";
        int fldColCountInt = 0;

        fldColCountInt = EJBUtil.stringToNum(getFldColCount());
        if (fldColCountInt == 0) {
            fldColCountInt = 1;
        }

        Rlog.debug("fieldlib", " in toXSL fldIsRO " + fldIsRO);
        if (EJBUtil.isEmpty(fldIsRO))
            fldRO = "0";
        else if (fldIsRO.equals("1"))
            fldRO = "1";
        Rlog.debug("fieldlib", "** fldRO " + fldRO);

        String fldVisible = "";
        Rlog.debug("fieldlib", " in toXSL fldIsVisible " + fldIsVisible);

        if (EJBUtil.isEmpty(fldIsVisible))
            fldVisible = "0";
        else
            fldVisible = fldIsVisible;

        Rlog.debug("fieldlib", "** fldVisible " + fldVisible);

        if (EJBUtil.isEmpty(fldInstructions))
            fldInstructions = "";
        else
            Rlog.debug("fieldlib", " fldInstructions 11 $$ " + fldInstructions);

        if (EJBUtil.isEmpty(fldDataType))
            fldDataType = "";
        else
            Rlog.debug("fieldlib", " fldDataType 22 $$ " + fldDataType);

        if (EJBUtil.isEmpty(fldName))
            fldName = "";
        else
            Rlog.debug("fieldlib", " fldName 33 $$ " + fldName);

        if (EJBUtil.isEmpty(fldNameFormat))
            fldNameFormat = "";

        if ((fldNameFormat.length() > 0)
                && (fldNameFormat.indexOf("[VELDISABLE]") < 0))
            dispFldName = fldNameFormat;
        else
            dispFldName = fldName;

        if (dispFldName.indexOf("<") >= 0) {

            dispFldName = StringUtil.replace(dispFldName, "<", "&lt;");
        }
        if (dispFldName.indexOf(">") >= 0) {
            dispFldName = StringUtil.replace(dispFldName, ">", "&gt;");
        }

        /*
         * if (fldName.indexOf("<")>=0) {
         *
         * fldName=StringUtil.replace(fldName,"<","&lt;"); } if
         * (fldName.indexOf(">")>=0) {
         * fldName=StringUtil.replace(fldName,">","&gt;"); }
         */
        fldName = StringUtil.htmlEncode(StringUtil.escapeSpecialChar(fldName));
        dispFldName = StringUtil.htmlEncode(StringUtil
                .escapeSpecialChar(dispFldName));

        if (EJBUtil.isEmpty(fldCharsNo))
            fldCharsNo = "";
        else
            Rlog.debug("fieldlib", " fldCharsNo 44 $$ " + fldCharsNo);

        if (EJBUtil.isEmpty(fldLength)) {
            fldLength = "";
        } else {
            Rlog.debug("fieldlib", " fldLength 55 $$ " + fldLength);
        }

        // to hide field label
        hideLabel = getFldHideLabel();
        if (StringUtil.isEmpty(hideLabel)) {
            hideLabel = "0";
        }

        // to hide response label
        hideResponseLabel = getFldHideResponseLabel();
        if (StringUtil.isEmpty(hideResponseLabel)) {
            hideResponseLabel = "0";
        }

        // to align alignResponseLabel

        alignResponseLabel = getFldResponseAlign();

        if (StringUtil.isEmpty(alignResponseLabel)) {
            alignResponseLabel = "";
        }

        labelDisplayWidth = getFldDisplayWidth();

        if (StringUtil.isEmpty(labelDisplayWidth)) {
            labelDisplayWidth = "";
        }

        Rlog.debug("fieldlib", "fldSysID *" + fldSysID);

        Rlog.debug("fieldlib", "fldType * " + fldType);
        Rlog.debug("fieldlib", "fldDataType * " + fldDataType);

        String fldXSL = "";

        String fldJS = "";

        try {
            Rlog.debug("fieldlib", "before label * " + fldType.length());

            VLabel fldLabel = new VLabel();

            fldLabel.setDisplayStyle(aStyle);
            fldLabel.title = fldInstructions;
            fldLabel.setLabelFor(fldSysID);
            fldLabel.setLabelDisplayWidth(labelDisplayWidth);
            String tempStr = "";

            if (fldType.equals("C")) {

                // VA, This is done to leave html code &#34;, in XSL after XSL
                // is created
                // If [VELDQUOTE] is not used here, then XSL convert &#34 to
                // &amp#34,
                // which causes problem on the browser with the formatting when
                // used
                // with FCKeditor.
                if ((fldNameFormat.length() > 0)
                        && (fldNameFormat.indexOf("[VELDISABLE]") < 0))
                    dispFldInst = fldNameFormat;
                else
                    dispFldInst = fldInstructions;

                dispFldInst = StringUtil.htmlEncode(StringUtil
                        .escapeSpecialChar(dispFldInst));

                if (dispFldInst.indexOf("&#34;") >= 0)
                    tempStr = StringUtil.replace(dispFldInst, "&#34;",
                            "[VELDQUOTE]");
                tempStr = VTag.escapeSpecial(tempStr);
                if (tempStr.indexOf("[VELDQUOTE]") >= 0)
                    tempStr = StringUtil.replace(tempStr, "[VELDQUOTE]",
                            "&#34;");
                tempStr = (tempStr == null) ? "" : tempStr;
                if (tempStr.length() > 0)
                    fldLabel.setText(tempStr);
                else
                    fldLabel.setText(dispFldInst);
                // fldLabel.setText(VTag.escapeSpecial(fldInstructions));
                fldLabel.title = "";
            } else if (fldType.equals("S")) {
                fldLabel.setText(fldInstructions);
                fldLabel.title = "";
            } else {
                fldLabel.setText(dispFldName);
            }

            if (fldType.equals("E") || fldType.equals("M")) {
                fldLabel.setIsRequired(required);
                // Green star with the soft check field
                fldLabel.setSoftCheckStar(softCheckStar);
            }

            if (fldType.equals("E")) {
                // check for number of lines, if fldLinesNo > 0 then it will be
                // TEXTAREA FLD

                int taRows = 0;
                if ((EJBUtil.isEmpty(fldLinesNo))) {
                    taRows = 0;
                } else {
                    taRows = EJBUtil.stringToNum(fldLinesNo);
                }

                if (!(EJBUtil.isEmpty(fldLinesNo)) && taRows > 1) {

                    Rlog.debug("fieldlib", "text area ");

                    // make a text area field
                    VTextArea ta = new VTextArea();
                    ta.setTaLabel(fldLabel);
                    ta.setName(fldSysID);
                    ta.id = fldSysID;
                    ta.title = fldInstructions;
                    ta.setRows(taRows);
                    ta.setWrap("physical");
                    ta.setMaxLength(EJBUtil.stringToNum(fldCharsNo));
                    ta.setColumns(EJBUtil.stringToNum(fldLength));
                    ta.setHideLabel(hideLabel);
                    ta.setFldNameForMessages(fldName);

                    if (aStyle != null)
                        ta.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1"))
                        ta.setReadOnly(true);

                    if (fldVisible.equals("2"))
                        ta.setDisabled(true);

                    ta.setMandatoryOverRide(overRideMandatory);

                    fldXSL = ta.drawXSL();

                    // fldJS = ta.getJavaScript(required);
                    fldJS = ta.getJavaScript(required, overRideMandatory);

                } else {

                    /* check the field data type */

                    VEditBox ve = new VEditBox();

                    Rlog.debug("fieldlib", "edit box ");

                    ve.setInputLabel(fldLabel);
                    ve.setType("text");
                    ve.setName(fldSysID);
                    ve.id = fldSysID;
                    ve.title = fldInstructions;
                    ve.setMaxLength(EJBUtil.stringToNum(fldCharsNo));
                    ve.setSize(EJBUtil.stringToNum(fldLength));

                    ve.setHideLabel(hideLabel);
                    ve.setFldNameForMessages(fldName);

                    if (aStyle != null)
                        ve.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1")) {
                        ve.setReadOnly(true);

                    }
                    if (fldVisible.equals("2")) {
                        ve.setDisabled(true);
                    }

                    // anu
                    Rlog.debug("fieldlib", "**** in  overRideMandatory::"
                            + overRideMandatory);
                    ve.setMandatoryOverRide(overRideMandatory);

                    if (fldDataType.equals("ET")) {
                        // to handle JS for text - Edit Box Text
                        ve.setDataType("T");

                    } else if (fldDataType.equals("EN")) {

                        // to handle JS for Number- Edit Box Number
                        ve.setDataType("N");
                        ve.setFormat(fldFormat);
                        // anu
                        Rlog.debug("fieldlib", "****EN in overRideFormat::"
                                + overRideFormat);
                        ve.setFormatOverRide(overRideFormat);
                        ve.setRangeOverRide(overRideRange);

                    } else if (fldDataType.equals("ED")) {
                        // to handle JS for Date - Edit Box Date
                        ve.setDataType("D");
                        ve.setDateCheck(todayCheck);

                        // set the size to 10 for date fields
                        ve.setSize(10);
                        // anu
                        Rlog.debug("fieldlib", "**** in ED overRideDate::"
                                + overRideDate);
                        ve.setDateOverRide(overRideDate);

                    } else if (fldDataType.equals("EI")) {
                        // to handle JS for Time- Edit Box Time
                        ve.setDataType("TI");

                    }

                    fldXSL = ve.drawXSL();
                    Rlog.debug("fieldlib", "edit box made fldXSL " + fldXSL);
                    fldJS = ve.getJavaScript(required);
                    if (jsNumberRange != null && jsNumberRange.length() > 0) {
                        if (fldJS == null) {
                            fldJS = jsNumberRange;
                        } else {
                            fldJS += lineSeparator+jsNumberRange;
                        }
                    }

                    Rlog.debug("fieldlib", "edit box made ");

                }

            } else if (fldType.equals("M")) {

                Rlog.debug("fieldlib", "i am multiple choice");
                if (fldDataType.equals("ML")) {
                    // MD - Multiple Choice, LOOKUP

                    VLookup vl = new VLookup(fldName);
                    vl.title = fldInstructions;
                    vl.setDisplayStyle(aStyle);

                    if (EJBUtil.isEmpty(lkpDisplayVal))
                        lkpDisplayVal = "";

                    if (EJBUtil.isEmpty(lkpDataVal))
                        lkpDataVal = "";
                    if (EJBUtil.isEmpty(fldLkpType))
                        fldLkpType = "";

                    if (EJBUtil.isEmpty(lookup))
                        lookup = "";

                    vl.setLkpView(lookup);
                    vl.setLkpFilter(lkpDataVal);
                    vl.setLkpKeyword(lkpDisplayVal);
                    vl.setLkpType(fldLkpType);

                    fldXSL = vl.drawXSL();

                } else if (fldDataType.equals("MD")) {
                    // MD - Multiple Choice, DROPDOWN

                    VSelect vs = new VSelect();
                    Rlog
                            .debug("fieldlib", "*****************Vishal"
                                    + fldLabel);
                    vs.setInputLabel(fldLabel);
                    vs.setName(fldSysID);
                    vs.id = fldSysID;
                    vs.title = fldInstructions;
                    if (fldRO.equals("1"))
                        vs.setReadOnly(true);

                    if (fldVisible.equals("2"))
                        vs.setDisabled(true);

                    if (aStyle != null) {
                        vs.setAlign(aStyle.getAlign());
                    } else {
                        Rlog.debug("fieldlib", "aSTyle * i am null");
                    }

                    vs.setHideLabel(hideLabel);
                    vs.setMandatoryOverRide(overRideMandatory);
                    vs.setFldNameForMessages(fldName);

                    fldXSL = vs.drawXSL();
                    fldJS = vs.getJavaScript(required, overRideMandatory);

                } else if (fldDataType.equals("MC") || fldDataType.equals("MR")) {
                    // MC - Multiple Choice CHECKBOX, MR - Multiple Choice RADIO

                    VMultiInput vmi = new VMultiInput();

                    Rlog.debug("fieldlib", "i am multiple choice MC MR");

                    // help icon required at the end of label
                    fldLabel.setHelpIconRequired(true);
                    vmi.setInputLabel(fldLabel);
                    vmi.setName(fldSysID);
                    vmi.id = fldSysID;
                    vmi.title = fldInstructions;
                    vmi.setHideLabel(hideLabel);
                    vmi.setHideResponseLabel(hideResponseLabel);
                    vmi.setAlignResponseLabel(alignResponseLabel);
                    vmi.setFldColCount(fldColCountInt);
                    vmi.setFldNameForMessages(fldName);

                    if (fldDataType.equals("MC"))
                        vmi.setType("checkbox");
                    else
                        vmi.setType("radio");

                    if (aStyle != null)
                        vmi.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1"))
                        vmi.setReadOnly(true);

                    if (fldVisible.equals("2"))
                        vmi.setDisabled(true);
                    vmi.setMandatoryOverRide(overRideMandatory);

                    fldXSL = vmi.drawXSL();
                    fldJS = vmi.getJavaScript(required, overRideMandatory);

                }
            } else if (fldType.equals("H")) {
                VHr vh = new VHr();
                vh.title = fldInstructions;
                fldXSL = vh.drawXSL();

            } else if (fldType.equals("C")) {
                // use fldLabel
                // set userFormat to true to display comment with blanks and
                // spaces as given by user
                fldLabel.setUserFormat(true);

                fldXSL = fldLabel.drawXSL(true);

            } else if (fldType.equals("S")) {
                fldXSL = fldLabel.drawXSL(true);
            } else if (fldType.equals("F")) {
                // F - Linked Form

                String lnkdFormId = "";
                LinkedFormsJB lJB = new LinkedFormsJB();
                String formDisplayType = "";
                String formEntryType = "";

                lnkdFormId = getFldLinkedForm();

                // get Linked Form Attributes for this form

                lJB.findByFormId(EJBUtil.stringToNum(lnkdFormId));

                formDisplayType = lJB.getLFDisplayType();
                formEntryType = lJB.getLFEntry();

                VLinkForm vLF = new VLinkForm(fldName, aStyle,
                        getFldLinkedForm());
                vLF.title = fldInstructions;
                vLF.setFormDisplayType(formDisplayType);
                vLF.setFormEntryType(formEntryType);

                fldXSL = vLF.drawXSL();

            }
        } catch (Exception ex) {
            Rlog.fatal("fieldlib", "Error in  " + ex);

        }

        Rlog.debug("fieldlib", "fldXSL *" + fldXSL);

        setFldXSL(fldXSL);
        setFldJavaScript(fldJS);

    }

    // //////
    /**
     * Returns an XSL representation of the field
     *
     * @return String
     */

    public void toXSL(boolean required) {

        /* generate field label */

        /* a label will be required for all field types, except <HR> */
        String dispFldName = "";
        String dispFldInst = "";
        if (EJBUtil.isEmpty(fldSysID))
            return;

        String fldRO = "";

        String hideLabel = "";
        String hideResponseLabel = "";
        String alignResponseLabel = "";
        String labelDisplayWidth = "";

        int fldColCountInt = 0;

        fldColCountInt = EJBUtil.stringToNum(getFldColCount());
        if (fldColCountInt == 0) {
            fldColCountInt = 1;
        }

        Rlog.debug("fieldlib", " in toXSL fldIsRO " + fldIsRO);
        if (EJBUtil.isEmpty(fldIsRO))
            fldRO = "0";
        else if (fldIsRO.equals("1"))
            fldRO = "1";
        Rlog.debug("fieldlib", "** fldRO " + fldRO);

        String fldVisible = "";
        Rlog.debug("fieldlib", " in toXSL fldIsVisible " + fldIsVisible);

        if (EJBUtil.isEmpty(fldIsVisible))
            fldVisible = "0";
        else
            fldVisible = fldIsVisible;

        Rlog.debug("fieldlib", "** fldVisible " + fldVisible);

        if (EJBUtil.isEmpty(fldInstructions))
            fldInstructions = "";
        else
            Rlog.debug("fieldlib", " fldInstructions 11 $$ " + fldInstructions);

        if (EJBUtil.isEmpty(fldDataType))
            fldDataType = "";
        else
            Rlog.debug("fieldlib", " fldDataType 22 $$ " + fldDataType);

        if (EJBUtil.isEmpty(fldName))
            fldName = "";
        else
            Rlog.debug("fieldlib", " fldName 33 $$ " + fldName);

        if (EJBUtil.isEmpty(fldNameFormat))
            fldNameFormat = "";

        if ((fldNameFormat.length() > 0)
                && (fldNameFormat.indexOf("[VELDISABLE]") < 0))
            dispFldName = fldNameFormat;
        else
            dispFldName = fldName;

        /*
         * if (dispFldName.indexOf("<")>=0) {
         *
         * dispFldName=StringUtil.replace(dispFldName,"<","&lt;"); } if
         * (dispFldName.indexOf(">")>=0) {
         * dispFldName=StringUtil.replace(dispFldName,">","&gt;"); }
         */

        /*
         * if (fldName.indexOf("<")>=0) {
         *
         * fldName=StringUtil.replace(fldName,"<","&lt;"); } if
         * (dispFldName.indexOf(">")>=0) {
         * fldName=StringUtil.replace(fldName,">","&gt;"); }
         */

        fldName = StringUtil.htmlEncode(StringUtil.escapeSpecialChar(fldName));
        dispFldName = StringUtil.htmlEncode(StringUtil
                .escapeSpecialChar(dispFldName));

        if (EJBUtil.isEmpty(fldCharsNo))
            fldCharsNo = "";
        else
            Rlog.debug("fieldlib", " fldCharsNo 44 $$ " + fldCharsNo);

        if (EJBUtil.isEmpty(fldLength)) {
            fldLength = "";
        } else {
            Rlog.debug("fieldlib", " fldLength 55 $$ " + fldLength);
        }

        // to hide field label
        hideLabel = getFldHideLabel();
        if (StringUtil.isEmpty(hideLabel)) {
            hideLabel = "0";
        }

        // to hide response label
        hideResponseLabel = getFldHideResponseLabel();
        if (StringUtil.isEmpty(hideResponseLabel)) {
            hideResponseLabel = "0";
        }

        // to align alignResponseLabel

        alignResponseLabel = getFldResponseAlign();

        if (StringUtil.isEmpty(alignResponseLabel)) {
            alignResponseLabel = "";
        }

        labelDisplayWidth = getFldDisplayWidth();

        if (StringUtil.isEmpty(labelDisplayWidth)) {
            labelDisplayWidth = "";
        }

        Rlog.debug("fieldlib", "fldSysID *" + fldSysID);

        Rlog.debug("fieldlib", "fldType * " + fldType);
        Rlog.debug("fieldlib", "fldDataType * " + fldDataType);

        String fldXSL = "";

        String fldJS = "";

        try {
            Rlog.debug("fieldlib", "before label * ");

            VLabel fldLabel = new VLabel();

            fldLabel.setDisplayStyle(aStyle);
            fldLabel.title = fldInstructions;
            fldLabel.setLabelFor(fldSysID);
            fldLabel.setLabelDisplayWidth(labelDisplayWidth);

            // for spacer type field
            String tempStr = "";
            if (fldType.equals("C")) {
                // VA, This is done to leave html code &#34;, in XSL after XSL
                // is created
                // If [VELDQUOTE] is not used here, then XSL convert &#34 to
                // &amp#34,
                // which causes problem on the browser with the formatting when
                // used
                // with FCKeditor.
                if ((fldNameFormat.length() > 0)
                        && (fldNameFormat.indexOf("[VELDISABLE]") < 0))
                    dispFldInst = fldNameFormat;
                else
                    dispFldInst = fldInstructions;

                dispFldInst = StringUtil.htmlEncode(StringUtil
                        .escapeSpecialChar(dispFldInst));

                if (dispFldInst.indexOf("&#34;") >= 0)
                    tempStr = StringUtil.replace(dispFldInst, "&#34;",
                            "[VELDQUOTE]");
                tempStr = VTag.escapeSpecial(tempStr);
                if (tempStr.indexOf("[VELDQUOTE]") >= 0)
                    tempStr = StringUtil.replace(tempStr, "[VELDQUOTE]",
                            "&#34;");
                tempStr = (tempStr == null) ? "" : tempStr;
                if (tempStr.length() > 0)
                    fldLabel.setText(tempStr);
                else
                    fldLabel.setText(dispFldInst);

                fldLabel.title = "";
            } else if (fldType.equals("S")) {
                fldLabel.setText(fldInstructions);
                fldLabel.title = "";
            } else {
                fldLabel.setText(dispFldName);
            }

            if (fldType.equals("E") || fldType.equals("M")) {
                fldLabel.setIsRequired(required);
            }

            if (fldType.equals("E")) {
                // check for number of lines, if fldLinesNo > 0 then it will be
                // TEXTAREA FLD

                int taRows = 0;
                if ((EJBUtil.isEmpty(fldLinesNo))) {
                    taRows = 0;
                } else {
                    taRows = EJBUtil.stringToNum(fldLinesNo);
                }

                if (!(EJBUtil.isEmpty(fldLinesNo)) && taRows > 1) {

                    Rlog.debug("fieldlib", "text area ");

                    // make a text area field
                    VTextArea ta = new VTextArea();
                    ta.setTaLabel(fldLabel);
                    ta.setName(fldSysID);
                    ta.id = fldSysID;
                    ta.title = fldInstructions;
                    ta.setRows(taRows);
                    ta.setWrap("physical");
                    ta.setMaxLength(EJBUtil.stringToNum(fldCharsNo));
                    ta.setColumns(EJBUtil.stringToNum(fldLength));
                    ta.setHideLabel(hideLabel);
                    ta.setFldNameForMessages(fldName);

                    if (aStyle != null)
                        ta.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1"))
                        ta.setReadOnly(true);

                    if (fldVisible.equals("2"))
                        ta.setDisabled(true);

                    fldXSL = ta.drawXSL();
                    fldJS = ta.getJavaScript(required);

                } else {

                    /* check the field data type */

                    VEditBox ve = new VEditBox();

                    Rlog.debug("fieldlib", "edit box ");

                    ve.setInputLabel(fldLabel);
                    ve.setType("text");
                    ve.setName(fldSysID);
                    ve.id = fldSysID;
                    ve.title = fldInstructions;
                    ve.setMaxLength(EJBUtil.stringToNum(fldCharsNo));
                    ve.setSize(EJBUtil.stringToNum(fldLength));
                    ve.setHideLabel(hideLabel);
                    ve.setFldNameForMessages(fldName);

                    if (aStyle != null)
                        ve.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1")) {
                        ve.setReadOnly(true);

                    }
                    if (fldVisible.equals("2")) {
                        ve.setDisabled(true);
                    }

                    // anu
                    Rlog.debug("fieldlib", "**** in  overRideMandatory::"
                            + overRideMandatory);
                    ve.setMandatoryOverRide(overRideMandatory);

                    if (fldDataType.equals("ET")) {
                        // to handle JS for text - Edit Box Text
                        ve.setDataType("T");

                    } else if (fldDataType.equals("EN")) {

                        // to handle JS for Number- Edit Box Number
                        ve.setDataType("N");
                        ve.setFormat(fldFormat);
                        // anu
                        Rlog.debug("fieldlib", "****EN in overRideFormat::"
                                + overRideFormat);
                        ve.setFormatOverRide(overRideFormat);
                        ve.setRangeOverRide(overRideRange);

                    } else if (fldDataType.equals("ED")) {
                        // to handle JS for Date - Edit Box Date
                        ve.setDataType("D");
                        ve.setDateCheck(todayCheck);

                        // set the size to 10 for date fields
                        ve.setSize(10);
                        // anu
                        Rlog.debug("fieldlib", "**** in ED overRideDate::"
                                + overRideDate);
                        ve.setDateOverRide(overRideDate);
                        Rlog.debug("fieldlib",
                                "**** in ED overRideDate after setDateOverRide::"
                                        + overRideDate);

                    } else if (fldDataType.equals("EI")) {
                        // to handle JS for Time- Edit Box Time
                        ve.setDataType("TI");

                    }

                    Rlog.debug("fieldlib", "**** in ED before drawXSL ::"
                            + overRideDate);
                    fldXSL = ve.drawXSL();
                    Rlog.debug("fieldlib", "edit box made fldXSL " + fldXSL);
                    fldJS = ve.getJavaScript(required);
                    if (jsNumberRange != null && jsNumberRange.length() > 0) {
                        if (fldJS == null) {
                            fldJS = jsNumberRange;
                        } else {
                            fldJS += lineSeparator+jsNumberRange;
                        }
                    }

                    Rlog.debug("fieldlib", "edit box made ");

                }

            } else if (fldType.equals("M")) {

                Rlog.debug("fieldlib", "i am multiple choice");
                if (fldDataType.equals("ML")) {
                    // MD - Multiple Choice, LOOKUP

                    VLookup vl = new VLookup(fldName);
                    vl.title = fldInstructions;
                    vl.setDisplayStyle(aStyle);

                    if (EJBUtil.isEmpty(lkpDisplayVal))
                        lkpDisplayVal = "";

                    if (EJBUtil.isEmpty(lkpDataVal))
                        lkpDataVal = "";
                    if (EJBUtil.isEmpty(fldLkpType))
                        fldLkpType = "";

                    if (EJBUtil.isEmpty(lookup))
                        lookup = "";

                    vl.setLkpView(lookup);
                    vl.setLkpFilter(lkpDataVal);
                    vl.setLkpKeyword(lkpDisplayVal);
                    vl.setLkpType(fldLkpType);

                    fldXSL = vl.drawXSL();

                } else if (fldDataType.equals("MD")) {
                    // MD - Multiple Choice, DROPDOWN

                    VSelect vs = new VSelect();
                    vs.setInputLabel(fldLabel);
                    vs.setName(fldSysID);
                    vs.id = fldSysID;
                    vs.title = fldInstructions;
                    if (fldRO.equals("1"))
                        vs.setReadOnly(true);

                    if (fldVisible.equals("2"))
                        vs.setDisabled(true);

                    if (aStyle != null) {
                        vs.setAlign(aStyle.getAlign());
                    } else {
                        Rlog.debug("fieldlib", "aSTyle * i am null");
                    }
                    vs.setHideLabel(hideLabel);
                    vs.setFldNameForMessages(fldName);

                    fldXSL = vs.drawXSL();
                    fldJS = vs.getJavaScript(required);

                } else if (fldDataType.equals("MC") || fldDataType.equals("MR")) {
                    // MC - Multiple Choice CHECKBOX, MR - Multiple Choice RADIO

                    VMultiInput vmi = new VMultiInput();

                    Rlog.debug("fieldlib", "i am multiple choice MC MR");

                    // help icon required at the end of label
                    fldLabel.setHelpIconRequired(true);
                    vmi.setInputLabel(fldLabel);
                    vmi.setName(fldSysID);
                    vmi.id = fldSysID;
                    vmi.title = fldInstructions;
                    vmi.setFldColCount(fldColCountInt);
                    vmi.setFldNameForMessages(fldName);

                    if (fldDataType.equals("MC"))
                        vmi.setType("checkbox");
                    else
                        vmi.setType("radio");

                    if (aStyle != null)
                        vmi.setAlign(aStyle.getAlign());
                    else
                        Rlog.debug("fieldlib", "aSTyle * i am null");

                    if (fldRO.equals("1")) {
                        vmi.setReadOnly(true);
                    }

                    if (fldVisible.equals("2")) {
                        vmi.setDisabled(true);
                    }

                    vmi.setHideLabel(hideLabel);
                    vmi.setHideResponseLabel(hideResponseLabel);
                    vmi.setAlignResponseLabel(alignResponseLabel);

                    fldXSL = vmi.drawXSL();
                    fldJS = vmi.getJavaScript(required);

                }
            } else if (fldType.equals("H")) {
                VHr vh = new VHr();
                vh.title = fldInstructions;
                fldXSL = vh.drawXSL();

            } else if (fldType.equals("C")) {
                // use fldLabel
                // set userFormat to true to display comment with blanks and
                // spaces as given by user
                fldLabel.setUserFormat(true);

                fldXSL = fldLabel.drawXSL(true);

            } else if (fldType.equals("S")) {
                fldXSL = fldLabel.drawXSL(true);
            } else if (fldType.equals("F")) {
                // F - Linked Form

                String lnkdFormId = "";
                LinkedFormsJB lJB = new LinkedFormsJB();
                String formDisplayType = "";
                String formEntryType = "";

                lnkdFormId = getFldLinkedForm();

                // get Linked Form Attributes for this form

                lJB.findByFormId(EJBUtil.stringToNum(lnkdFormId));

                formDisplayType = lJB.getLFDisplayType();
                formEntryType = lJB.getLFEntry();

                VLinkForm vLF = new VLinkForm(fldName, aStyle,
                        getFldLinkedForm());

                vLF.title = fldInstructions;
                vLF.setFormDisplayType(formDisplayType);
                vLF.setFormEntryType(formEntryType);

                fldXSL = vLF.drawXSL();

            }
        } catch (Exception ex) {
            Rlog.fatal("fieldlib", "Error in  " + ex);

        }

        Rlog.debug("fieldlib", "fldXSL *" + fldXSL);

        setFldXSL(fldXSL);
        setFldJavaScript(fldJS);

    }

    /**
     * Method to htmlEncode the field attributes to support special characters
     *
     * @param fieldLsk
     *            FieldLibStateKeeper object to manipulate
     * @return FieldLibStateKeeper object with attributes htmlEncoded
     */
    public FieldLibBean escapeStateKeeper(FieldLibBean fieldLsk) {
        /*
         * if (fieldLsk !=null) {
         *
         * fieldLsk.setFldName(StringUtil.htmlEncode(fieldLsk.getFldName()));
         * fieldLsk.setFldDesc(StringUtil.htmlEncode(fieldLsk.getFldDesc()));
         * fieldLsk.setFldKeyword(StringUtil.htmlEncode(fieldLsk.getFldKeyword()));
         * fieldLsk.setFldInstructions(StringUtil.htmlEncode(fieldLsk.getFldInstructions()));
         * fieldLsk.setFldDefResp(StringUtil.htmlEncode(
         * fieldLsk.getFldDefResp())); }
         */
        return fieldLsk;
    }

    public int insertToFormField(FieldLibBean fieldLsk) {

        int pk_fld = 0, pk_formfld = 0, pk_fldVal = 0;
        boolean required = true;
        boolean softCheckStar = true;
        String mandatory;
        int retRepeat = 0;
        String fldDataType = "";
        String sysId = "";
        String sysIdsk = "";
        int formSecId = 0;
        int retvalue = 0;
        try {
            Rlog
                    .debug("fieldlib",
                            "FieldLibJB.insertToFormField(FieldLibDao fieldLibDao) starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLsk = escapeStateKeeper(fieldLsk);
            pk_fld = fieldLibAgentRObj.insertToFormField(fieldLsk);

            if (pk_fld <= 0) {
                return -3;
            }

            setFieldLibId(pk_fld);
            getFieldLibDetails();
            String fldName = getFldName();

            if (fldName.indexOf("<") >= 0) {

                fldName = StringUtil.replace(fldName, "<", "&lt;");
            }
            if (fldName.indexOf(">") >= 0) {
                fldName = StringUtil.replace(fldName, ">", "&gt;");
            }

            String rangeOverRide = getOverRideRange();
            rangeOverRide = (rangeOverRide == null) ? "" : rangeOverRide;

            String overRideFormat = getOverRideFormat();
            overRideFormat = (overRideFormat == null) ? "" : overRideFormat;

            String overRideDate = getOverRideDate();
            overRideDate = (overRideDate == null) ? "" : overRideDate;

            String overRideMand = getOverRideMandatory();
            overRideMand = (overRideMand == null) ? "" : overRideMand;

            if (rangeOverRide.equals("1") || overRideFormat.equals("1")
                    || overRideDate.equals("1") || overRideMand.equals("1")) {
                softCheckStar = true;
            } else
                softCheckStar = false;

            Rlog.debug("fieldlib", "GET FORMSECID IN FIELDLIBJB FIELDSK $$."
                    + fieldLsk.getFormSecId());
            sysId = getFldSysID();
            Rlog.debug("fieldlib", "In JB system id" + sysId);

            if (pk_fld > 0) {
                Rlog
                        .debug("fieldlib",
                                "insertToFormField:Inside when pk_fld is greater than 0");
                // FldValidateAgentBean fldval = new FldValidateAgentBean();

                FldValidateBean fvsk = fieldLsk.getFldValidateBean();
                Rlog.debug("fieldlib", "fvsk" + fvsk);
                Rlog.debug("fieldlib", "before id");
                if (fvsk != null) {
                    Rlog.debug("fieldlib", "inside fvsk");
                    fvsk.setFldLibId(Integer.toString(pk_fld));

                    FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                            .getFldValidateAgentHome();

                    if (!(fvsk.getFldValidateVal1().equals("") && fvsk
                            .getFldValidateVal2().equals(""))) {
                        Rlog.debug("fieldlib", "inside not equals");
                        // anu
                        // if(rangeOverRide.equals("0"){
                        fvsk.generateJavaScript(sysId, rangeOverRide, fldName);
                        // }
                        // else
                        // fvsk.generateJavaScript(sysId);
                    }
                    pk_fldVal = fldValidateAgentRObj
                            .setFldValidateDetails(fvsk);
                    // }
                    Rlog.debug("fieldlib",
                            "insertToFormField:Value of pk_fldval" + pk_fldVal);
                }

            }

            mandatory = ((fieldLsk.getFormFldMandatory()) == null) ? ""
                    : (fieldLsk.getFormFldMandatory());

            if (mandatory.equals("1"))
                required = true;
            else
                required = false;

            toXSL(required, softCheckStar);

            try {

                FormFieldJB formFieldJB = new FormFieldJB();

                Integer pk_fldTemp = new Integer(pk_fld);
                formSecId = EJBUtil.stringToNum((String) fieldLsk
                        .getFormSecId());

                formFieldJB.setFormSecId(fieldLsk.getFormSecId());
                formFieldJB.setFieldId(EJBUtil.integerToString(pk_fldTemp));
                formFieldJB.setFormFldSeq(fieldLsk.getFormFldSeq());
                formFieldJB.setFormFldMandatory(fieldLsk.getFormFldMandatory());
                formFieldJB.setFormFldBrowserFlg(fieldLsk
                        .getFormFldBrowserFlg());

                formFieldJB.setRecordType(fieldLsk.getRecordType());
                formFieldJB.setCreator(fieldLsk.getCreator());
                formFieldJB.setIpAdd(fieldLsk.getIpAdd());
                formFieldJB.setOfflineFlag("0");
                Rlog.debug("fieldlib", "**** setOfflineFlag"
                        + formFieldJB.getOfflineFlag());
                formFieldJB.setFormFldXsl(getFldXSL());
                formFieldJB.setFormFldJavaScr(getFldJavaScript());
                pk_formfld = formFieldJB.setFormFieldDetails();

                Rlog.debug("fieldlib", "first pk_formfld" + pk_formfld);
                // only for lookup type field. call repeatLookupField on
                // FormField to implement repeat sections for lookup

                fldDataType = fieldLsk.getFldDataType();

                if (fldDataType == null)
                    fldDataType = "";

                if (fldDataType.equals("ML")) {
                    retRepeat = formFieldJB.repeatLookupField(pk_fldTemp
                            .intValue(), pk_formfld, EJBUtil
                            .stringToNum(fieldLsk.getFormSecId()));
                    if (retRepeat < 0) {
                        Rlog.fatal("fieldlib",
                                "Could not repeat lookup field properly");
                    }

                }

                FormLibJB formJB = new FormLibJB();

                formJB.setFormLibId(Integer.parseInt(fieldLsk.getFormId()));
                formJB.getFormLibDetails();
                formJB.setFormLibXslRefresh("1");
                formJB.setRecordType(fieldLsk.getRecordType());
                formJB.setCreator(fieldLsk.getCreator());
                formJB.setIpAdd(fieldLsk.getIpAdd());

                formJB.updateFormLib();

                if (pk_formfld > 0) {

                    FieldLibDao fldLibDao = new FieldLibDao();
                    Rlog.debug("fieldlib",
                            "FiledLibJB. in update before updateFieldSeq");

                    retvalue = fldLibDao.updateFieldSeq(formSecId, pk_formfld,
                            Integer.parseInt(fieldLsk.getFormFldSeq()),Integer.parseInt(fieldLsk.getCreator()));

                }
                return pk_formfld;

            }

            catch (Exception e) {

                Rlog.fatal("fieldlib",
                        "Exception insertToFormField(FieldLibStateKeeper fieldLsk		) in FieldLibJB"
                                + e);
                return -2;
            }

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in insertToFormField( FieldLibStateKeeper fieldLsk	) in FieldLibJB "
                            + e);
            return -2;
        }
    }

    // ///////////////////////////////////////////////////////////////////

    public int updateToFormField(FieldLibBean fieldLsk) {

        int pk_fld = 0, pk_formfld = 0, ret = 0;
        boolean required = true;
        boolean softCheckStar = true;
        String mandatory;

        int retRepeat = 0;
        String fldDataType = "";
        String sysId = "";
        int pk_fldVal = 0;
        int fldValId = 0;
        int sectionId = 0;
        int retvalue = 0;

        try {
            Rlog.debug("fieldlib", "FieldLibJB.updateToFormField starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLsk = escapeStateKeeper(fieldLsk);
            ret = fieldLibAgentRObj.updateToFormField(fieldLsk);
            Rlog.debug("fieldlib", "FieldLibJB.updateToFormField ret" + ret);
            if (ret < 0) {
                return ret;
            }

            // get the validation data that was set in the jsp page
            // always call this before getFieldLibDetails, otherwise the value
            // in SK would be overridden

            FldValidateBean fvsk = fieldLsk.getFldValidateBean();

            Rlog.debug("fieldlib",
                    "in FieldLibJB.updateToFormField fvsk before getfieldlibdetails "
                            + fvsk);

            setFieldLibId(fieldLsk.getFieldLibId());
            getFieldLibDetails();
            if (fvsk != null) {
                fvsk.generateJavaScript(this.fldSysID, this.overRideRange, this.fldName);
                jsNumberRange = fvsk.fldValidateJavaScr;
            }

            sysId = getFldSysID();
            String fldName = getFldName();

            if (fldName.indexOf("<") >= 0) {

                fldName = StringUtil.replace(fldName, "<", "&lt;");
            }
            if (fldName.indexOf(">") >= 0) {
                fldName = StringUtil.replace(fldName, ">", "&gt;");
            }

            String rangeOverRide = getOverRideRange();
            rangeOverRide = (rangeOverRide == null) ? "" : rangeOverRide;

            String overRideFormat = getOverRideFormat();
            overRideFormat = (overRideFormat == null) ? "" : overRideFormat;

            String overRideDate = getOverRideDate();
            overRideDate = (overRideDate == null) ? "" : overRideDate;

            String overRideMand = getOverRideMandatory();
            overRideMand = (overRideMand == null) ? "" : overRideMand;

            if (rangeOverRide.equals("1") || overRideFormat.equals("1")
                    || overRideDate.equals("1") || overRideMand.equals("1"))
            // if(rangeOverRide.equals("1") || getOverRideFormat().equals("1")
            // || getOverRideDate().equals("1") ||
            // getOverRideMandatory().equals("1"))
            {
                softCheckStar = true;
            } else
                softCheckStar = false;

            Rlog.debug("fieldlib", "In JB system id in update" + sysId);

            Rlog.debug("fieldlib", "in update fvsk" + fvsk);
            if (fvsk != null) {
                Rlog.debug("fieldlib", "Inside fvsk");

                // fvsk.setFldLibId(Integer.toString(fieldLsk.getFieldLibId()));

                /*
                 * Rlog.debug("fieldlib","update
                 * getfldlibId:"+fvsk.getFldLibId());
                 * Rlog.debug("fieldlib","update
                 * getFldValidateOp1:"+fvsk.getFldValidateOp1());
                 * Rlog.debug("fieldlib","update
                 * getFldValidateVal1:"+fvsk.getFldValidateVal1());
                 * Rlog.debug("fieldlib","update
                 * getFldValidateLogOp1:"+fvsk.getFldValidateLogOp1());
                 * Rlog.debug("fieldlib","update
                 * getFldValidateOp2:"+fvsk.getFldValidateOp2());
                 * Rlog.debug("fieldlib","update
                 * getFldValidateVal2:"+fvsk.getFldValidateVal2());
                 *
                 */
                FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                        .getFldValidateAgentHome();

                Rlog.debug("fieldlib", "before fvsk generate");
                fvsk.generateJavaScript(sysId, rangeOverRide, fldName);

                Rlog.debug("fieldlib",
                        "before updateFldValidate in FieldLibJB validate val1"
                                + fvsk.getFldValidateVal1());
                Rlog.debug("fieldlib",
                        "before updateFldValidate in FieldLibJB validate val2"
                                + fvsk.getFldValidateVal2());

                pk_fldVal = fldValidateAgentRObj.updateFldValidate(fvsk);

            }

            mandatory = ((fieldLsk.getFormFldMandatory()) == null) ? "0"
                    : (fieldLsk.getFormFldMandatory());

            if (mandatory.equals("1"))
                required = true;
            else
                required = false;

            toXSL(required, softCheckStar);

            try {

                FormFieldJB formFieldJB = new FormFieldJB();

                formFieldJB.setFormFieldId(Integer.parseInt(fieldLsk
                        .getFormFldId()));

                formFieldJB.getFormFieldDetails();

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldSeq())) {
                    formFieldJB.setFormFldSeq(fieldLsk.getFormFldSeq());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldMandatory())) {
                    formFieldJB.setFormFldMandatory(fieldLsk
                            .getFormFldMandatory());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldBrowserFlg())) {
                    formFieldJB.setFormFldBrowserFlg(fieldLsk
                            .getFormFldBrowserFlg());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getRecordType())) {
                    formFieldJB.setRecordType(fieldLsk.getRecordType());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getCreator())) {
                    formFieldJB.setModifiedBy(fieldLsk.getCreator());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getIpAdd())) {
                    formFieldJB.setIpAdd(fieldLsk.getIpAdd());
                }

                formFieldJB.setFormFldXsl(getFldXSL());

                Rlog.debug("fieldlib", "AM IN EDIT BOX CASE --5$$%   "
                        + getFldXSL());
                Rlog.debug("fieldlib", "AM IN EDIT BOX CASE --5$$%   "
                        + getFldJavaScript());
                formFieldJB.setFormFldJavaScr(getFldJavaScript());
                pk_formfld = formFieldJB.updateFormField();
                Rlog.debug("fieldlib",
                        "FieldLibJB. updatetoformfld 	pk_formfld" + pk_formfld);

                // / We check the formId from fieldLibStateKeeper if we dont get
                // it we
                // / get the fk_formlib from FormSecBean

                sectionId = EJBUtil.stringToNum((String) formFieldJB
                        .getFormSecId());

                Rlog.debug("fieldlib", "FieldLibJB. updatetoformfld sectionId"
                        + sectionId);

                if (fieldLsk.getFormId() == null) {

                    String secId = formFieldJB.getFormSecId();
                    int formSecId = Integer.parseInt(secId);
                    Rlog.debug("fieldlib",
                            "FiledLibJB. updatetoformfld formSecId in null"
                                    + formSecId);

                    FormSecJB formSecJB = new FormSecJB();
                    formSecJB.setFormSecId(formSecId);
                    formSecJB.getFormSecDetails();
                    fieldLsk.setFormId(formSecJB.getFormLibId());

                }

                // only for lookup type field. call repeatLookupField on
                // FormField to implement repeat sections for lookup

                fldDataType = fieldLsk.getFldDataType();

                if (fldDataType == null)
                    fldDataType = "";

                if (fldDataType.equals("ML")) {
                    retRepeat = formFieldJB.repeatLookupField(fieldLsk
                            .getFieldLibId(), Integer.parseInt(fieldLsk
                            .getFormFldId()), EJBUtil.stringToNum(formFieldJB
                            .getFormSecId()));
                    if (retRepeat < 0) {
                        Rlog.fatal("fieldlib",
                                "Could not repeat lookup field properly");
                    }

                }

                FormLibJB formJB = new FormLibJB();

                formJB.setFormLibId(Integer.parseInt(fieldLsk.getFormId()));

                formJB.getFormLibDetails();

                formJB.setFormLibXslRefresh("1");
                formJB.setRecordType(fieldLsk.getRecordType());
                formJB.setLastModifiedBy(fieldLsk.getModifiedBy());
                formJB.setIpAdd(fieldLsk.getIpAdd());
                formJB.updateFormLib();

                if (pk_fld >= 0) {

                    FieldLibDao fldLibDao = new FieldLibDao();

                    retvalue = fldLibDao.updateFieldSeq(sectionId, Integer
                            .parseInt(fieldLsk.getFormFldId()), Integer
                            .parseInt(fieldLsk.getFormFldSeq()),Integer.parseInt(fieldLsk.getModifiedBy()));
                    Rlog.debug("formsection",
                            "FormSecJB. in update retVal in updatetoformfld"
                                    + retvalue);

                }
                return pk_fld;

            }

            catch (Exception e) {

                Rlog
                        .fatal(
                                "fieldlib",
                                "Exception updateToFormField(FieldLibStateKeeper fieldLsk		) in FieldLibAgentBean:INSERT INTO ER_FORMFLD "
                                        + e);
                return -2;
            }

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in updateFormField( FieldLibStateKeeper fieldLsk	) in FieldLibJB "
                            + e);
            return -2;
        }

    }

    /**
     *
     *
     * @param formId
     * @param secId
     * @param idArray
     * @return int
     */
    public int copyMultipleFields(String secId, String[] idArray,
            String creator, String ipAdd) {
        int ret;

        try {
            Rlog.debug("fieldlib", "FieldLibJB.copyMultipleFields starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            ret = fieldLibAgentRObj.copyMultipleFields(secId, idArray, creator,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("fieldlib", "Error in copyMultipleFields in FieldLibJB "
                    + e);
            return -2;
        }
        return ret;
    }

    /**
     *
     *
     * @param formId
     * @return FieldLibDao
     */
    public FieldLibDao getFieldNames(int formId)

    {
        int ret;
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFieldNames(formId);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib", "Error in getFieldNames in FieldLibJB " + e);

        }
        return fieldLibDao;
    }

    /**
     *
     *
     * @param formId
     * @return FieldLibDao
     */
    public FieldLibDao getFltrdFieldNames(int formId)

    {
        int ret;
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFltrdFieldNames(formId);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib", "Error in getFltrdFieldNames in FieldLibJB "
                    + e);

        }
        return fieldLibDao;
    }

    // ///////////////////////////////////////////////////////////////////////////

    /**
     * This is the overloaded method which inserts a record in the FormField
     * correspnding to the current record being entered. Also this method is for
     * the Multiple Choice type of the field. <br>
     * This handles the insertion of the default responses corresponding to to a
     * Multiple Choice field
     *
     * @param fieldLsk
     * @param fldRespDao
     * @return pk_formfield of the new field added to form
     */
    public int insertToFormField(FieldLibBean fieldLsk, FldRespDao fldRespDao) {

        int pk_fld = 0, pk_formfld = 0, ret = 0;
        boolean required = true;
        boolean softCheckStar = true;
        String mandatory;
        String sysId;
        int retvalue = 0;
        int sectionId = 0;
        try {
            Rlog
                    .debug("fieldlib",
                            "FieldLibJB.insertToFormField(FieldLibDao fieldLibDao	) starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLsk = escapeStateKeeper(fieldLsk);
            pk_fld = fieldLibAgentRObj.setFieldLibDetails(fieldLsk, fldRespDao);

            if (pk_fld > 0) {
                fldRespDao.setField(EJBUtil
                        .integerToString(new Integer(pk_fld)));
                // if the field is stored, store the multiple responses
                FldRespDao respDao = new FldRespDao();
                ret = respDao.insertMultipleResponses(fldRespDao);
                Rlog.debug("fieldlib",
                        "AFTER RETURNING FROM DEF-RESP $$.RETTT=====  " + ret);
            }
            if (pk_fld <= 0) {
                return -3;
            }

            setFieldLibId(pk_fld); // set the JB with the retrieved PK of the
            // new record

            getFieldLibDetails();

            mandatory = ((fieldLsk.getFormFldMandatory()) == null) ? ""
                    : (fieldLsk.getFormFldMandatory());
            String overRideMand = getOverRideMandatory();
            overRideMand = (overRideMand == null) ? "" : overRideMand;
            if (overRideMand.equals("1")) {
                softCheckStar = true;
            } else
                softCheckStar = false;

            if (mandatory.equals("1"))
                required = true;
            else
                required = false;
            toXSL(required, softCheckStar);

            try {

                FormFieldJB formFieldJB = new FormFieldJB();

                Integer pk_fldTemp = new Integer(pk_fld);

                sectionId = EJBUtil.stringToNum((String) fieldLsk
                        .getFormSecId());
                formFieldJB.setFormSecId(fieldLsk.getFormSecId());
                // Rlog.debug("fieldlib","setFormSecId --1
                // "+formFieldJB.getFormSecId());

                formFieldJB.setFieldId(EJBUtil.integerToString(pk_fldTemp));
                // Rlog.debug("fieldlib","setFormSecId --2
                // "+formFieldJB.getFieldId());

                formFieldJB.setFormFldSeq(fieldLsk.getFormFldSeq());
                // Rlog.debug("fieldlib","setFormSecId --3
                // "+formFieldJB.getFormFldSeq());

                formFieldJB.setFormFldMandatory(fieldLsk.getFormFldMandatory());
                // Rlog.debug("fieldlib","setFormSecId --4
                // "+formFieldJB.getFormFldMandatory());

                formFieldJB.setFormFldBrowserFlg(fieldLsk
                        .getFormFldBrowserFlg());
                // Rlog.debug("fieldlib","setFormSecId --5
                // "+formFieldJB.getFormFldBrowserFlg());

                formFieldJB.setRecordType(fieldLsk.getRecordType());
                formFieldJB.setCreator(fieldLsk.getCreator());
                formFieldJB.setIpAdd(fieldLsk.getIpAdd());

                formFieldJB.setOfflineFlag("0");
                Rlog.debug("fieldlib", "**** setOfflineFlag"
                        + formFieldJB.getOfflineFlag());

                formFieldJB.setFormFldXsl(getFldXSL());
                // Rlog.debug("fieldlib","setFormSecId --5 "+getFldXSL());
                formFieldJB.setFormFldJavaScr(getFldJavaScript());
                pk_formfld = formFieldJB.setFormFieldDetails();
                Rlog.debug("fieldlib", "**** pk_formfld" + pk_formfld);

                FormLibJB formJB = new FormLibJB();

                formJB.setFormLibId(Integer.parseInt(fieldLsk.getFormId()));
                // Rlog.debug("fieldlib","setFormSecId --6
                // "+formJB.getFormLibId());
                formJB.getFormLibDetails();

                formJB.setFormLibXslRefresh("1");
                // Rlog.debug("fieldlib","setFormSecId --8
                // "+formJB.getFormLibXslRefresh());
                formJB.setRecordType(fieldLsk.getRecordType());
                // Rlog.debug("fieldlib","setFormSecId --9
                // "+formJB.getRecordType());
                formJB.setCreator(fieldLsk.getCreator());
                formJB.setIpAdd(fieldLsk.getIpAdd());
                formJB.updateFormLib();

                if (pk_fld > 0) {

                    FieldLibDao fldLibDao = new FieldLibDao();

                    retvalue = fldLibDao.updateFieldSeq(sectionId, pk_formfld,
                            Integer.parseInt(fieldLsk.getFormFldSeq()),Integer.parseInt(fieldLsk.getCreator()));
                    Rlog.debug("formsection",
                            "FormSecJB. in update retVal in insertoformfld 2"
                                    + retvalue);

                }

                return pk_formfld;

            }

            catch (Exception e) {

                Rlog
                        .fatal(
                                "fieldlib",
                                "Exception insertToFormField(FieldLibStateKeeper fieldLsk,FldDefResp 		) in FieldLibAgentBean:INSERT INTO ER_FORMFLD "
                                        + e);
                return -2;
            }

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "fieldlib",
                            "Error in insertToFormField( FieldLibStateKeeper fieldLsk, FieldDefResp	) in FieldLibJB "
                                    + e);
            return -2;
        }
    }

    // ////////////////////////////////////////////////////////////////////////

    /**
     *
     * This is the overloaded method which updates a record in the FormField
     * correspnding to the current record being entered. Also this method is for
     * the Multiple Choice type of the field. <br>
     * This handles the updation of the default responses corresponding to to a
     * Multiple Choice field
     *
     * @param fieldLsk
     * @param fldRespDao
     * @return the return status of the update
     */
    public int updateToFormField(FieldLibBean fieldLsk, FldRespDao fldRespDao) {

        int pk_fld = 0, pk_formfld = 0, ret = 0;
        boolean required = true;
        boolean softCheckStar = true;
        String mandatory;
        int sectionId = 0;
        int retvalue = 0;
        try {
            Rlog.debug("fieldlib", "FieldLibJB.updateToFormField starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLsk = escapeStateKeeper(fieldLsk);
            ret = fieldLibAgentRObj.updateToFormField(fieldLsk);

            if (ret < 0) {
                return ret;
            }
            if (ret == 0) {
                // if the field is updated, update the multiple responses
                FldRespDao respDao = new FldRespDao();
                respDao.updateMultipleResponses(fldRespDao);
            }

            Rlog.debug("fieldlib", "FieldLibJB.updateToFormField$$$ ID"
                    + getFldSysID());

            setFieldLibId(fieldLsk.getFieldLibId());
            getFieldLibDetails();
            FldValidateBean fvsk = fieldLsk.getFldValidateBean();
            if (fvsk != null) {
                fvsk.generateJavaScript(this.fldSysID, this.overRideRange, this.fldName);
                jsNumberRange = fvsk.fldValidateJavaScr;
            }

            String overRideMand = getOverRideMandatory();
            overRideMand = (overRideMand == null) ? "" : overRideMand;

            if (overRideMand.equals("1"))

            {
                softCheckStar = true;
            } else
                softCheckStar = false;

            mandatory = ((fieldLsk.getFormFldMandatory()) == null) ? "0"
                    : (fieldLsk.getFormFldMandatory());

            if (mandatory.equals("1"))
                required = true;
            else
                required = false;
            toXSL(required, softCheckStar);

            try {

                FormFieldJB formFieldJB = new FormFieldJB();

                formFieldJB.setFormFieldId(Integer.parseInt(fieldLsk
                        .getFormFldId()));

                formFieldJB.getFormFieldDetails();

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldSeq())) {
                    formFieldJB.setFormFldSeq(fieldLsk.getFormFldSeq());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldMandatory())) {
                    formFieldJB.setFormFldMandatory(fieldLsk
                            .getFormFldMandatory());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getFormFldBrowserFlg())) {
                    formFieldJB.setFormFldBrowserFlg(fieldLsk
                            .getFormFldBrowserFlg());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getRecordType())) {
                    formFieldJB.setRecordType(fieldLsk.getRecordType());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getCreator())) {
                    formFieldJB.setModifiedBy(fieldLsk.getCreator());
                }

                if (!EJBUtil.isEmpty(fieldLsk.getIpAdd())) {
                    formFieldJB.setIpAdd(fieldLsk.getIpAdd());
                }

                formFieldJB.setFormFldXsl(getFldXSL());
                formFieldJB.setFormFldJavaScr(getFldJavaScript());
                pk_formfld = formFieldJB.updateFormField();

                // / We check the formId from fieldLibStateKeeper if we dont get
                // it we
                // / get the fk_formlib from FormSecBean

                sectionId = EJBUtil.stringToNum((String) formFieldJB
                        .getFormSecId());

                if (fieldLsk.getFormId() == null) {

                    String secId = formFieldJB.getFormSecId();
                    int formSecId = Integer.parseInt(secId);

                    FormSecJB formSecJB = new FormSecJB();
                    formSecJB.setFormSecId(formSecId);
                    formSecJB.getFormSecDetails();
                    fieldLsk.setFormId(formSecJB.getFormLibId());

                }

                FormLibJB formJB = new FormLibJB();
                formJB.setFormLibId(Integer.parseInt(fieldLsk.getFormId()));

                formJB.getFormLibDetails();

                formJB.setFormLibXslRefresh("1");
                formJB.setRecordType(fieldLsk.getRecordType());
                formJB.setLastModifiedBy(fieldLsk.getModifiedBy());
                formJB.setIpAdd(fieldLsk.getIpAdd());
                formJB.updateFormLib();

                if (pk_formfld >= 0) {

                    FieldLibDao fldLibDao = new FieldLibDao();
                    retvalue = fldLibDao.updateFieldSeq(sectionId, Integer
                            .parseInt(fieldLsk.getFormFldId()), Integer
                            .parseInt(fieldLsk.getFormFldSeq()),Integer.parseInt(fieldLsk.getModifiedBy()));
                    Rlog.debug("formsection",
                            "FormSecJB. in update retVal in updatetoformfld 2"
                                    + retvalue);

                }
                return pk_formfld;

            }

            catch (Exception e) {

                Rlog
                        .fatal(
                                "fieldlib",
                                "Exception updateToFormField(FieldLibStateKeeper fieldLsk		) in FieldLibAgentBean:INSERT INTO ER_FORMFLD "
                                        + e);
                return -2;
            }

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in insertToFormField( FieldLibStateKeeper fieldLsk	) in FieldLibJB "
                            + e);
            return -2;
        }

    }

    public FieldLibDao getFieldsInformation(int formId) {
        int ret;
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFieldsInformation(formId);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in getFieldsInformation in FieldLibJB " + e);

        }
        return fieldLibDao;
    }

    /**
     *
     * This method sets the value of the browser flag in er_formfld to 1 if it
     * is selected to be displayed in browser else sets the value to 0 it also
     * sets the form_xslrefresh of er_formlib to 1 for the corresponding formId
     *
     * @param browserOne
     * @param browserZero
     * @param lastModBy
     * @param recordType,
     * @param fldRespDao
     * @return int
     */

    public int updateBrowserFlag(String browserOne, String browserZero,
            int lastModBy, String recordType, String ipAdd) {
        int ret = 0;

        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            ret = fieldLibAgentRObj.updateBrowserFlag(browserOne, browserZero,
                    lastModBy, recordType, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fieldlib", "Error in updateBrowserFlag in FieldLibJB "
                    + e);

        }
        return ret;
    }

    // ////////////////////////////////////////////////////

    /**
     * Gets the field attributes from the table depending on the search criteria
     * taken <br>
     * as parameters in this method
     *
     * @param fkCatlib
     * @param fkAccount
     * @param name
     * @param keyword
     * @return FieldLibDao
     */
    public FieldLibDao getFieldsForCopy(int fkCatlib, int fkAccount,
            String name, String keyword) {
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            Rlog.debug("fieldlib", "FieldLibJB.getFieldsForCopy starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFieldsForCopy(fkCatlib,
                    fkAccount, name, keyword);

            return fieldLibDao;
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "fieldlib",
                            "Error in getFieldsForCopy(int fkCatlib,int fkAccount, String name, String keyword) in FieldLibJB "
                                    + e);
        }
        return fieldLibDao;
    }

    /**
     * Gets section fields for form lookup
     *
     * @param formId
     * @param sectionId
     * @param fldType
     */

    public FieldLibDao getFieldNamesForSection(int formId, int sectionId,
            String fldType) {
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            Rlog.debug("fieldlib",
                    "FieldLibJB.getFieldNamesForSection starting");
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getFieldNamesForSection(formId,
                    sectionId, fldType);

            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in getFieldNamesForSection in FieldLibJB " + e);
        }
        return fieldLibDao;
    }

    /**
     * Gets All views available for lookup fields
     *
     * @param account
     */

    public LookupDao getAllLookpUpViews(String account) {
        LookupDao lkpDao = new LookupDao();
        try {

            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            lkpDao = fieldLibAgentRObj.getAllLookpUpViews(account);

            return lkpDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception in getAllLookpUpViews in FieldLibJB " + e);
        }
        return lkpDao;
    }

    /**
     * Gets All columns available for lookup view
     *
     * @param viewId
     */

    public LookupDao getLookpUpViewColumns(String viewId) {
        LookupDao lkpDao = new LookupDao();
        try {

            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            lkpDao = fieldLibAgentRObj.getLookpUpViewColumns(viewId);

            return lkpDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception in getLookpUpViewColumns in FieldLibJB " + e);
        }
        return lkpDao;
    }

    public FieldLibDao getEditMultipleFields(int formId) {
        int ret;
        FieldLibDao fieldLibDao = new FieldLibDao();
        try {
            FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                    .getFieldLibAgentHome();

            fieldLibDao = fieldLibAgentRObj.getEditMultipleFields(formId);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in getEditMultipleFields in FieldLibJB " + e);

        }
        return fieldLibDao;
    }

    /**
     * returns true if field as any inter field action associated with the field
     *
     * @param fieldId
     *
     *
     */

    public boolean hasInterFieldActions(String fieldId)
    {
   	   try {
               FieldLibAgentRObj fieldLibAgentRObj = EJBUtil
                       .getFieldLibAgentHome();

               return fieldLibAgentRObj.hasInterFieldActions(fieldId);

           } catch (Exception e) {
               Rlog.fatal("fieldlib",
                       "Error in hasInterFieldActions in FieldLibJB " + e);
               return true; // so that field is not deleted, if called from delete field

           }


    }




}
