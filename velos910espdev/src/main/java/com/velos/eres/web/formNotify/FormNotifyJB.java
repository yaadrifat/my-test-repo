/*
 * Classname : FormNotifyJB
 * 
 * Version information: 1.0
 *
 * Date: 24/06/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.web.formNotify;

/* IMPORT STATEMENTS */


import java.util.Hashtable;
import com.velos.eres.business.common.FormNotifyDao;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.service.formNotifyAgent.FormNotifyAgentRObj;
import com.velos.eres.service.formNotifyAgent.impl.FormNotifyAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Form Notification
 * 
 * @author Sonia Kaura
 * @version 1.0 24/06/2003
 */

public class FormNotifyJB {
    /**
     * The form primary key:pk_fn
     */

    private int formNotifyId;

    /**
     * The foreign key reference from the Form Library: FK_FORMLIB
     */
    private String formLibId;

    /**
     * The Msg Type, Pop or Notification : FN_MSGTYPE
     */

    private String msgType;

    /**
     * The actual message text to be sent in notification: FN_MSGTEXT
     */
    private String msgText;

    /**
     * The frequency type of the message to be sent; First Time,
     * Everytime:FN_SENDTYPE
     */

    private String msgSendType;

    /**
     * The list of the users to whom the message is sent: FN_USERS
     * 
     */
    private String userList;

    /**
     * The creator making the notifcation : CREATOR
     * 
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    /**
     * The Record Type : Modified, New or Delete
     */

    private String recordType;

    /**
     * The Name List : Name List retrieved from the user id list
     */

    private String nameList;

    // GETTER AND SETTER METHODS

    /**
     * @return the unique ID of the Form Notification
     */

    public int getFormNotifyId() {
        return this.formNotifyId;
    }

    /**
     * @param set
     *            the unique ID of the Form Notification
     */

    public void setFormNotifyId(int formNotifyId) {
        this.formNotifyId = formNotifyId;
    }

    /**
     * @return the ID of the Form as in Form Library
     */

    public String getFormLibId() {
        return this.formLibId;
    }

    /**
     * @param the
     *            unique ID of the Form Notification
     */

    public void setFormLibId(String formLibId) {
        this.formLibId = formLibId;
    }

    /**
     * @return the Message Type of the Form Notification
     */

    public String getMsgType() {
        return this.msgType;
    }

    /**
     * @param set
     *            the Message Type of the Form Notification
     */

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     * @return the Message Text of the Form Notification
     */

    public String getMsgText() {
        return this.msgText;
    }

    /**
     * @param set
     *            the Message Text of the Form Notification
     */

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    /**
     * @return the Message send type of the Form Notification
     */

    public String getMsgSendType() {
        return this.msgSendType;
    }

    /**
     * @param set
     *            the Message send type ID of the Form Notification
     */

    public void setMsgSendType(String msgSendType) {
        this.msgSendType = msgSendType;
    }

    /**
     * @return the User list of the Form Notification
     */

    public String getUserList() {
        return this.userList;
    }

    /**
     * @param set
     *            the User list of the Form Notification
     */

    public void setUserList(String userList) {
        this.userList = userList;
    }

    /**
     * @return the unique ID of the User
     */

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return The Creator of the Form Notification
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Notification
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * 
     * @return get the Names List from the Database
     */

    public String getNameList() {
        return this.nameList;
    }

    /**
     * @param set
     *            the Name List to be displayed
     */

    public void setNameList(String nameList) {
        this.nameList = nameList;
    }

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param frmNotifyId:
     *            Unique Id constructor
     * 
     */

    public FormNotifyJB(int frmNotifyId) {
        setFormNotifyId(frmNotifyId);
    }

    /**
     * Default Constructor
     */

    public FormNotifyJB() {
        Rlog.debug("formnotify", "FormNotifyJB.FormNotifyJB() ");
    }

    /**
     * Full arguments constructor.
     * 
     * @param formNotifyId
     * @param formLibId
     * @param msgType
     * @param msgText
     * @param msgSendType
     * @param userList
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     * @param recordType
     */

    public FormNotifyJB(int formNotifyId, String formLibId, String msgType,
            String msgText, String msgSendType, String userList,
            String creator, String modifiedBy, String ipAdd, String recordType) {
        setFormNotifyId(formNotifyId);
        setFormLibId(formLibId);
        setMsgType(msgType);
        setMsgText(msgText);
        setMsgSendType(msgSendType);
        setUserList(userList);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRecordType(recordType);
        Rlog.debug("formnotify", "FormNotifyJB.FormNotify(all parameters)");
    }

    /**
     * Calls getFormNotifyDetails of Form Notify Session Bean:
     * FormNotifyAgentBean
     * 
     * @return FormNotifyStatKeeper
     * @see FormNotifyAgentBean
     */

    public FormNotifyBean getFormNotifyDetails() {
        FormNotifyBean formsk = null;
        try {
            FormNotifyAgentRObj formNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();

            formsk = formNotifyAgentRObj
                    .getFormNotifyDetails(this.formNotifyId);
            Rlog.debug("formnotify",
                    "FormNotifyJB.getFormNotifyDetails() FormNotifyStateKeeper "
                            + formsk);
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in Form Notify getFormNotifyDetails" + e);
        }

        if (formsk != null) {
            this.formNotifyId = formsk.getFormNotifyId();
            this.formLibId = formsk.getFormLibId();
            this.msgType = formsk.getMsgType();
            this.msgText = formsk.getMsgText();
            this.msgSendType = formsk.getMsgSendType();
            this.userList = formsk.getUserList();
            this.creator = formsk.getCreator();
            this.modifiedBy = formsk.getModifiedBy();
            this.ipAdd = formsk.getIpAdd();
            this.recordType = formsk.getRecordType();
            this.nameList = formsk.getNameList();
        }

        return formsk;

    }

    /**
     * Calls setFormNotifyDetails() of Notify Form Session Bean:
     * FormNotifyAgentBean
     * 
     */

    public int setFormNotifyDetails() {
        int toReturn;
        try {

            FormNotifyAgentRObj FormNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();

            FormNotifyBean tempStateKeeper = new FormNotifyBean();
            tempStateKeeper = this.createFormNotifyStateKeeper();
            toReturn = FormNotifyAgentRObj
                    .setFormNotifyDetails(tempStateKeeper);
            this.setFormNotifyId(toReturn);

            Rlog.debug("formnotify", "FormNotifyJB.setFormNotifyDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in setFormNotifyDetails() in FormNotifyJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the Form Notify Record with the current
     *         values of the bean
     */
    public FormNotifyBean createFormNotifyStateKeeper() {
        Rlog.debug("formnotify", "FormNotifyJB.createFormNotifyStateKeeper ");
        return new FormNotifyBean(formNotifyId, formLibId, msgType, msgText,
                msgSendType, userList, creator, modifiedBy, ipAdd, recordType);

    }

    /**
     * Calls updateFormNotify() of FormNotify Session Bean: FormNotifyAgentBean
     * 
     * @return
     */
    public int updateFormNotify() {
        int output;
        try {
            FormNotifyAgentRObj formNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();

            output = formNotifyAgentRObj.updateFormNotify(this
                    .createFormNotifyStateKeeper());
        } catch (Exception e) {
            Rlog.debug("formnotify",
                    "EXCEPTION IN SETTING FORM NOTIFY DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeFormNotify() {

        int output;

        try {

            FormNotifyAgentRObj formNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();

            output = formNotifyAgentRObj.removeFormNotify(this.formNotifyId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "in FormNotifyJB -removeFormNotify() method");
            return -1;
        }

    }
    
    
    
 // Overloaded for INF-18183 ::: AGodara
    public int removeFormNotify(Hashtable<String, String> auditInfo) {

        int output;
        try {
            FormNotifyAgentRObj formNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();
            output = formNotifyAgentRObj.removeFormNotify(this.formNotifyId,auditInfo);
            return output;
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "in FormNotifyJB -removeFormNotify(Hashtable<String, String> auditInfo) method");
            return -1;
        }
    }

    /**
     * 
     * The method to get all the form notifications for display.
     * 
     * @param The
     *            Form Library ID is taken as the parameter
     * @return The FormNotify DAO
     * 
     * 
     * 
     */

    public FormNotifyDao getAllFormNotifications(int formLibId) {
        FormNotifyDao formNotifyDao = new FormNotifyDao();
        try {
            Rlog.debug("formnotify",
                    "FormNotifyJB.getAllFormNotifications starting");
            FormNotifyAgentRObj formNotifyAgentRObj = EJBUtil
                    .getFormNotifyAgentHome();

            formNotifyDao = formNotifyAgentRObj
                    .getAllFormNotifications(formLibId);

            return formNotifyDao;
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in getAllFormNotifications(int formLibId) in FormNotifyJB "
                            + e);
        }
        return formNotifyDao;
    }

}
