/*
 * Classname : UserSessionJB
 * 
 * Version information: 1.0
 *
 * Date: 03/04/2003
 * 
 * Copyright notice: Velos, Inc
 */

package com.velos.eres.web.userSession;

/* Import Statements */
import com.velos.eres.business.userSession.impl.UserSessionBean;
import com.velos.eres.service.userSessionAgent.UserSessionAgentRObj;
import com.velos.eres.service.userSessionAgent.impl.UserSessionAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for milepayment
 * 
 * @author Sonika Talwar
 * @version 1.0 05/30/2002
 */

public class UserSessionJB {
    /**
     * the UserSession Id
     */
    private int id;

    /**
     * the user id
     */
    private String userId;

    /**
     * the login date time of user session
     */
    private String loginTime;

    /**
     * the logout date time of user session
     */
    private String logoutTime;

    /*
     * Login sucess flag
     */
    private String successFlag;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getLoginTime() {
        return this.loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getLogoutTime() {
        return this.logoutTime;
    }

    public void setLogoutTime(String logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getSuccessFlag() {
        return this.successFlag;
    }

    public void setSuccessFlag(String successFlag) {
        this.successFlag = successFlag;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    // end of getter and setter methods

    /**
     * Constructor
     * 
     * @param userSessionId
     */
    public UserSessionJB(int userSessionId) {
        setId(userSessionId);
    }

    /**
     * Default Constructor
     * 
     */
    public UserSessionJB() {
    }

    /**
     * Full Argument Constructor
     */
    public UserSessionJB(int id, String userId, String loginTime,
            String logoutTime, String successFlag, String ipAdd) {
        setId(id);
        setLoginTime(loginTime);
        setLogoutTime(logoutTime);
        setSuccessFlag(successFlag);
        setUserId(userId);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getUserSessionDetails of UserSession Session Bean:
     * UserSessionAgentBean
     * 
     * @returns UserSessionStateKeeper object
     * @see UserSessionAgentBean
     */
    public UserSessionBean getUserSessionDetails() {
        UserSessionBean usk = null;

        try {
            UserSessionAgentRObj userSessionAgent = EJBUtil
                    .getUserSessionAgentHome();
            usk = userSessionAgent.getUserSessionDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "Error in getUserSessionDetails() in UserSessionJB " + e);
        }
        if (usk != null) {
            this.id = usk.getId();
            setIpAdd(usk.getIpAdd());
            setLoginTime(usk.getLoginTime());
            setLogoutTime(usk.getLogoutTime());
            setSuccessFlag(usk.getSuccessFlag());
            setUserId(usk.getUserId());
        }
        return usk;
    }

    /**
     * Calls setUserSessionDetails of UserSession Session Bean:
     * userSessionAgentBean
     * 
     * @see userSessionAgentBean
     */
    public void setUserSessionDetails() {
        try {
            Rlog.debug("usersession",
                    "UserSessionJB.setUserSessionDetails() line 1");
            UserSessionAgentRObj userSessionAgent = EJBUtil
                    .getUserSessionAgentHome();
            this.setId(userSessionAgent.setUserSessionDetails(this
                    .createUserSessionStateKeeper()));
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "Error in setUserSessionDetails() in userSessionJB " + e);
            e.printStackTrace();
        }
    }

    /**
     * Calls updateUserSession of UserSession Session Bean: userSessionAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see UserSessionAgentBean
     */
    public int updateUserSession() {
        int output;
        try {
            UserSessionAgentRObj userSessionAgentRObj = EJBUtil
                    .getUserSessionAgentHome();
            output = userSessionAgentRObj.updateUserSession(this
                    .createUserSessionStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "EXCEPTION IN SETTING UserSession DETAILS TO  SESSION BEAN"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Creates a UserSessionStateKeeper onject with the attribute vales of the
     * bean
     * 
     * @return UserSessionStateKeeper
     */
    public UserSessionBean createUserSessionStateKeeper() {

        return new UserSessionBean(id, userId, loginTime, logoutTime,
                successFlag, ipAdd);
    }

} // end of class
