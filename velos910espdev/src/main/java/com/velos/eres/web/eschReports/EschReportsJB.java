/*
 * Classname : 				EschReportsJB
 * 
 * Version information : 	1.0
 *
 * Date 					07/14/2001
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					dinesh
 */

package com.velos.eres.web.eschReports;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.EschCostReportDao;
import com.velos.eres.business.common.EschReportsDao;
import com.velos.eres.service.eschReportsAgent.EschReportsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for EschReports module. This class is used for fetching data
 * for any report
 * 
 * @author Dinesh
 * @version 1.0, 05/12/2001
 */

public class EschReportsJB {

    /**
     * Defines an EschReportsJB object
     */
    public EschReportsJB() {
        Rlog.debug("eschreports", "EschReportsJB.EschReportsJB() ");
    };

    public EschReportsDao fetchEschReportsData(int reportNumber,
            String filterType, String year, String month, String dateFrom,
            String dateTo, String id, String accId) {
        EschReportsDao eschreportsDao = new EschReportsDao();
        try {
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData starting");

            EschReportsAgentRObj eschReportsAgentRObj = EJBUtil
                    .getEschReportsAgentHome();
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData after remote");
            eschreportsDao = eschReportsAgentRObj.fetchEschReportsData(
                    reportNumber, filterType, year, month, dateFrom, dateTo,
                    id, accId);
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData after Dao");
            return eschreportsDao;
        } catch (Exception e) {
            Rlog.fatal("eschreports",
                    "Error in fetchEschReportsData in EschReportsJB " + e);
        }
        return eschreportsDao;
    }

    public EschCostReportDao getCostReport(int reportNumber, int protocol_id,
            int orderBy) {
        EschCostReportDao eschcostreportDao = new EschCostReportDao();
        try {
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData starting");
            EschReportsAgentRObj eschReportsAgentRObj = EJBUtil
                    .getEschReportsAgentHome();
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData after remote");
            eschcostreportDao = eschReportsAgentRObj.getCostReport(
                    reportNumber, protocol_id, orderBy);
            Rlog.debug("eschreports",
                    "EschReportsJB.fetchEschReportsData after Dao");
            return eschcostreportDao;
        } catch (Exception e) {
            Rlog.fatal("eschreports",
                    "Error in fetchEschReportsData in EschReportsJB " + e);
        }
        return eschcostreportDao;
    }

}// end of class

