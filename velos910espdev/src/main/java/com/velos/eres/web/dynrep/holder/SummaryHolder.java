package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

/** @author Sonia Abrol, 12/29/06
 * Data Holder class for Summary fields*/
public class SummaryHolder implements Serializable {
    /** */
	
	private ArrayList arData;
	
	private float minValue;
	
	private float maxValue;
	
	private  float medianValue;
	
	 
	
	private  int totalCount;
	
	private Hashtable htValueCounts;
	
	/** Hashtable for summary holders */
	private Hashtable htHolders; 
	
	/** flag to identify the summary information type. Possible values: 'sum' - for summary , 'per'- for percentage counts and 'both' for both */
	private String infoType;
	
	private float columnTotal;
	
	 /** This filed is named totalMultipleChResponseCount because in Percent/count calculation, each unique multiple choice response option will be treated 
	 as a separate unit. Example, if a checkbox field is selected and it has 2 response options selected then the response count for 
	 the single form response will be 2. But if a radio/dropdown/edit field is selected, the count will be 1.
	 The complication is only for multiple choice fields*/
	private long totalMultipleChResponseCount;

	

	public long getTotalMultipleChResponseCount() {
		return totalMultipleChResponseCount;
	}

	public void setTotalMultipleChResponseCount(long totalMultipleChResponseCount) {
		this.totalMultipleChResponseCount = totalMultipleChResponseCount;
	}

	public float getColumnTotal() {
		return columnTotal;
	}

	public void setColumnTotal(float  columnTotal) {
		this.columnTotal = columnTotal;
	}

	public SummaryHolder()
	{
		arData = new ArrayList();
		htValueCounts = new Hashtable();
		htHolders = new Hashtable();
	}
	
	public ArrayList getArData() {
		return arData;
	}

	public void setArData(ArrayList arData) {
		this.arData = arData;
	}

	public float getAverageValue() {
		float avg =0; 
		if (totalCount > 0) {		
			avg = columnTotal/totalCount;
			
			//double r = avg;
			BigDecimal bd = new BigDecimal(avg);
//			 setScale is immutable
			bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
			avg = (float) bd.doubleValue();
			return avg;
			
		}
		else
		{
			return 0;
		}	
	}

	public void setHtValueCounts(String value) {
		
		
		String keyValue = "";
		int indexMulti = -1 ;
		int repeatIndex = -1;
		
		
		
		//System.out.println("value:" + value);
		
		
		repeatIndex = value.indexOf("[VELSEP]");
		
		String[] strRepValueArray = StringUtil.strSplit(value,"[VELSEP]",false);
		
		if (strRepValueArray != null && strRepValueArray.length > 0)
		{
			for (int j=0;j<strRepValueArray.length;j++)
			{
				keyValue = strRepValueArray[j];
				indexMulti = keyValue.indexOf("[VELSEP1]");
				
				
					
					if (indexMulti > 0) //multiple choice field
					{
						keyValue = keyValue.substring(0,indexMulti);
					}
		
					keyValue = "]," + keyValue + ",[";
					 
					
					keyValue = StringUtil.replace(keyValue,"],[" , "[*VSEP*]");
					 
					keyValue = StringUtil.replace(keyValue,"]," , "[*VSEP*]"); 
					 
					keyValue = StringUtil.replace(keyValue,",[" , "[*VSEP*]");
					 
					
					String[] strValueArray = StringUtil.strSplit(keyValue,"[*VSEP*]",true); 
		
						if (strValueArray!= null && strValueArray.length > 0)
						{
							for (int k=0;k<strValueArray.length;k++)
							{
								if ((repeatIndex > 0 && !((strValueArray[k]).equals("-"))) || repeatIndex < 0) 
								{
									Integer count = new Integer(0);
									
									if (htValueCounts.containsKey(strValueArray[k]))
									{
										count = (Integer) htValueCounts.get(strValueArray[k]);
											
									}
									
									 htValueCounts.put(strValueArray[k],new Integer((count.intValue() + 1 ) ) );
									setTotalMultipleChResponseCount(getTotalMultipleChResponseCount() + 1);
							  }	
							}	
						} //strValueArray
						
		
		} //for loop - strRepValueArr
	}	//if loop
		//end of method		
	}	
	public Hashtable getHtHolders() {
		return htHolders;
	}

	public void setHtHolders(Hashtable htHolders) {
		this.htHolders = htHolders;
	}

	public Hashtable getHtValueCounts() {
		return htValueCounts;
	}
	
	public int getHtValueCounts(String key) 
	{
	  Integer val = new Integer(0);
	   if ( htValueCounts.containsKey(key))
		{
			val = (Integer)	htValueCounts.get(key);

		}
		
		return val.intValue();
	 }
	
	public float getHtValueCountPercentage(String key) 
	{
	  
		float count = 0;
	  float perNumber = 0;
	  
		
		count = getHtValueCounts(key);
		//System.out.println("key" + key + "count" + count + "getTotalMultipleChResponseCount()" + getTotalMultipleChResponseCount());
		if (getTotalMultipleChResponseCount() > 0)
		{
			perNumber = (count/getTotalMultipleChResponseCount() ) * 100;
		}
		
		DecimalFormat df = new DecimalFormat("0.##");
		
		perNumber = Float.parseFloat(df.format((double)perNumber)) ;		     
		//System.out.println("perNumber" + perNumber);
		
		return perNumber;
	 }
	
	public void setHtValueCounts(Hashtable htValueCounts) {
		this.htValueCounts = htValueCounts;
	}

	public String getInfoType() {
		return infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getMedianValue() {

		if (medianValue > 0)
		{
			return medianValue;
		}
		else //calculate median, possibly its not calculated
		{
			medianValue =  EJBUtil.calculateMedian(arData);
			return medianValue;
		}
	}

	public void setMedianValue(float medianValue) {
		this.medianValue = medianValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public void setHtHolders(int index, String value, String valueType) {
		  String strKey = "";
		  SummaryHolder sh = new SummaryHolder();
		  int repeatIndex = -1;
	
		  strKey = String.valueOf(index) ;
		  

			if ( htHolders.containsKey(strKey))
			 {
				sh = (SummaryHolder) htHolders.get(strKey);
			}
				
				
				
				if (valueType.equals("sum") || valueType.equals("both"))
				{
					repeatIndex = value.indexOf("[VELSEP]");
				
					if (repeatIndex > 0)	
					{
						String[] strRepValueArray = StringUtil.strSplit(value,"[VELSEP]",false);
						
						if (strRepValueArray != null && strRepValueArray.length > 0)
						{
							for (int j=0;j<strRepValueArray.length;j++)
							{
								if(! strRepValueArray[j].equals("-"))	
								{
									sh.setArData(new Float(EJBUtil.stringToFloat(strRepValueArray[j])));
									sh.calculate(strRepValueArray[j]);		
								}
							}
						}		
					
					} //if repeat
					else
					{
					
						sh.setArData(new Float(EJBUtil.stringToFloat(value)));
						sh.calculate(value);
					}	
				}
				

				if (valueType.equals("per") || valueType.equals("both")) //
					{
							
						sh.setHtValueCounts(value);
					}
			 
			if ( ! htHolders.containsKey(strKey))
			{
				sh.setInfoType(valueType);
				htHolders.put(strKey,sh);
			}
			
		}
	

	 
	private void calculate(String val)
	{
		 float floatValue = 0;
		 this.totalCount =    this.totalCount + 1;
		 
		 floatValue = EJBUtil.stringToFloat(val);
		 
		 if ((this.totalCount == 1) || floatValue < this.minValue)
		 {
		 	this.minValue = floatValue;
		 }
		 	 
		 if (floatValue > this.maxValue)
		 {
		 	this.maxValue = floatValue;
		 }
		 
		 
		 this.columnTotal =  this.columnTotal + floatValue;
		 
		
	}
	
	
		public void setArData(String data) {
			arData.add(data);
		}

		public void setArData(Float data) {
			arData.add(data);
		}


		public void printHolders()
		{

		  for (Enumeration e = htHolders.keys() ; e.hasMoreElements() ;) 
		    {
		         ((SummaryHolder) htHolders.get( (String)e.nextElement())).printMe(); 
		     }


		}

		void printMe()
		{
			/* 
			 * System.out.println("ArrayList data:" + arData);
			System.out.println("infoType:" + infoType);
			System.out.println("minValue:" + minValue);
			System.out.println("maxValue:" + maxValue);
			System.out.println("columnTotal:" + columnTotal);
			System.out.println("totalCount:" + totalCount);
			System.out.println("average:" + getAverageValue());
			System.out.println("median:" + getMedianValue());
			*/
			
			Enumeration e = htValueCounts.keys() ;
			
			 if (e!= null)
			 {
				 while ( e.hasMoreElements() ) 
				{
					 	String elemKey = (String)  e.nextElement();
					 	
					     //System.out.println("Per Key:" + elemKey+ " , count: " +  ((Integer) htValueCounts.get(  elemKey)).intValue());    
				 }
			   
			 }
			 
		   //System.out.println("Total Multiple Choice Responses:"  + getTotalMultipleChResponseCount() );    
			
		}

	
}	