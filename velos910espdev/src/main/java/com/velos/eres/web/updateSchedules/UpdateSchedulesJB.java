/*
 * Classname : UpdateSchedulesJB.java
 * 
 * Version information :1.0
 *
 * Date 03/05/2008
 *  
 *  Author : Manimaran
 */

package com.velos.eres.web.updateSchedules;

import java.io.Serializable;
import com.velos.eres.service.updateSchedulesAgent.UpdateSchedulesAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;



public class UpdateSchedulesJB implements Serializable {

   
    public UpdateSchedulesJB() {
    }

    
    public int discShedulesAllPatient(int discSchCal, String discDate, String discReason, int newSchCal, String newSchStartDate, int usr, String ipAdd) {
    	try{
    		int output = 0;
    		
    		UpdateSchedulesAgentRObj updateSchAgent = EJBUtil.getUpdateSchedulesAgentHome();
    		output = updateSchAgent.discShedulesAllPatient(discSchCal, discDate, discReason, newSchCal, newSchStartDate, usr, ipAdd);
    		
    		return output;
    		
    	}catch(Exception e){
    		Rlog.fatal("updateSch","discShedulesAllPatient" +e);
    		e.printStackTrace();
    		return -1;
    	}
    }

    
    public int updateEventStatusCurrAndDisc(int eveStatDiscCal, String scheduleFlag, String dateOccurDisc, String allEveDate, int dstatusDisc, String allEveStatDate, int usr, String ipAdd ) {
    	try{
    		int output = 0;
    		
    		UpdateSchedulesAgentRObj updateSchAgent = EJBUtil.getUpdateSchedulesAgentHome();
    		output = updateSchAgent.updateEventStatusCurrAndDisc(eveStatDiscCal, scheduleFlag, dateOccurDisc, allEveDate, dstatusDisc, allEveStatDate, usr, ipAdd);
    		
    		return output;
    		
    	}catch(Exception e){
    		Rlog.fatal("updateSch","updateEventStatusCurrAndDisc" +e);
    		e.printStackTrace();
    		return -1;
    	}
    }
    
    
    public int generateNewSchedules(int newSchAllCal, String patSchStartDate, int patEnrollDtCheck, int usr, String ipAdd ) {
    	try{
    		int output = 0;
    		
    		UpdateSchedulesAgentRObj updateSchAgent = EJBUtil.getUpdateSchedulesAgentHome();
    		output = updateSchAgent.generateNewSchedules(newSchAllCal, patSchStartDate, patEnrollDtCheck, usr, ipAdd);
    		
    		return output;
    		
    	}catch(Exception e){
    		Rlog.fatal("updateSch","generateNewSchedules" +e);
    		e.printStackTrace();
    		return -1;
    	}
    }
    
    
       
}// end of class

