/*
 * Classname : CTRPDraftDocAgent
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.service;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.ctrp.business.CTRPDraftDocBean;
import com.velos.eres.ctrp.business.CTRPDraftDocDao;

@Remote
public abstract interface CTRPDraftDocAgent
{
  public CTRPDraftDocDao getCTRPDraftDocDetails(int fkCtrpDraft);
  public Integer setCtrpDraftDocDetails(CTRPDraftDocBean ctrpDraftDocBean);
  public Integer updateCtrpDraftDocDetails(CTRPDraftDocBean ctrpDraftDocBean);
  public int deleteCTRPDraftDocRecord(int ctrpDraftDocId);
  
}