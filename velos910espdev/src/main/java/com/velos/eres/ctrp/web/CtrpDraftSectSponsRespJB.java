package com.velos.eres.ctrp.web;

import java.util.ArrayList;
import java.util.List;

import com.velos.eres.service.util.LC;

public class CtrpDraftSectSponsRespJB {
	
	private String sponsorId = null;
	private Integer sponsorFkSite = null;
	private Integer sponsorFkAdd = null;
	private String sponsorName = null;
	private String sponsorStreet = null;
	private String sponsorCity = null;
	private String sponsorState = null;
	private String sponsorZip = null;
	private String sponsorCountry = null;
	private String sponsorEmail = null;
	private String sponsorPhone = null;
	private String sponsorTty = null;
	private String sponsorFax = null;
	private String sponsorUrl = null;
	private Integer responsibleParty = null;
	private List<RadioOption> responsiblePartyList = null;
	private String sponsorContactFromStudy = null;
	private Integer sponsorContactType = null;
	private List<RadioOption> sponsorContactTypeList = null;
	private String personalContactId = null;
	private Integer personalContactFkUser = null;
	private Integer personalContactFkAdd = null;
	private String personalContactFirstName = null;
	private String personalContactMiddleName = null;
	private String personalContactLastName = null;
	private String personalContactStreetAddress = null;
	private String personalContactCity = null;
	private String personalContactState = null;
	private String personalContactZip = null;
	private String personalContactCountry = null;
	private String personalContactEmail = null;
	private String personalContactPhone = null;
	private String personalContactTty = null;
	private String personalContactFax = null;
	private String personalContactUrl = null;
	private String genericContactId = null;
	private String genericContactTitle = null;
	private String responsiblePartyEmail = null;
	private String responsiblePartyPhone = null;
	private String responsiblePartyPhoneExt = null;
	
	public static final String FLD_SPONS_ID = "ctrpDraftSectSponsRespJB.sponsorId";
	public static final String FLD_SPONS_FK_SITE = "ctrpDraftSectSponsRespJB.sponsorFkSite";
	public static final String FLD_SPONS_FK_ADD = "ctrpDraftSectSponsRespJB.sponsorFkAdd";
	public static final String FLD_SPONS_STREET = "ctrpDraftSectSponsRespJB.sponsorStreet";
	public static final String FLD_SPONS_CITY = "ctrpDraftSectSponsRespJB.sponsorCity";
	public static final String FLD_SPONS_STATE = "ctrpDraftSectSponsRespJB.sponsorState";
	public static final String FLD_SPONS_ZIP = "ctrpDraftSectSponsRespJB.sponsorZip";
	public static final String FLD_SPONS_COUNTRY = "ctrpDraftSectSponsRespJB.sponsorCountry";
	public static final String FLD_SPONS_EMAIL = "ctrpDraftSectSponsRespJB.sponsorEmail";
	public static final String FLD_SPONS_CONTACT_TYPE = "ctrpDraftSectSponsRespJB.sponsorContactType";
	public static final String FLD_RESP_PARTY = "ctrpDraftSectSponsRespJB.responsibleParty";
	public static final String FLD_RESP_PARTY_EMAIL = "ctrpDraftSectSponsRespJB.responsiblePartyEmail";
	public static final String FLD_RESP_PARTY_PHONE = "ctrpDraftSectSponsRespJB.responsiblePartyPhone";
	public static final String FLD_RESP_PARTY_EXT = "ctrpDraftSectSponsRespJB.responsiblePartyPhoneExt";
	public static final String FLD_PER_CONTACT_ID = "ctrpDraftSectSponsRespJB.personalContactId";
	public static final String FLD_PER_CONTACT_FK_USER = "ctrpDraftSectSponsRespJB.personalContactFkUser";
	public static final String FLD_PER_CONTACT_FK_ADD = "ctrpDraftSectSponsRespJB.personalContactFkAdd";
	public static final String FLD_PER_CONTACT_STREET = "ctrpDraftSectSponsRespJB.personalContactStreetAddress";
	public static final String FLD_PER_CONTACT_CITY = "ctrpDraftSectSponsRespJB.personalContactCity";
	public static final String FLD_PER_CONTACT_STATE = "ctrpDraftSectSponsRespJB.personalContactState";
	public static final String FLD_PER_CONTACT_ZIP = "ctrpDraftSectSponsRespJB.personalContactZip";
	public static final String FLD_PER_CONTACT_COUNTRY = "ctrpDraftSectSponsRespJB.personalContactCountry";
	public static final String FLD_PER_CONTACT_EMAIL = "ctrpDraftSectSponsRespJB.personalContactEmail";
	public static final String FLD_PER_CONTACT_PHONE = "ctrpDraftSectSponsRespJB.personalContactPhone";
	public static final String FLD_GEN_CONTACT_ID = "ctrpDraftSectSponsRespJB.genericContactId"; 
		
	public CtrpDraftSectSponsRespJB() {
		responsiblePartyList = new ArrayList<RadioOption>();
		responsiblePartyList.add(new RadioOption(LC.L_PI, 1));
		responsiblePartyList.add(new RadioOption(LC.L_Sponsor, 0));
		sponsorContactTypeList = new ArrayList<RadioOption>();
		sponsorContactTypeList.add(new RadioOption(LC.L_Personal, 1));
		sponsorContactTypeList.add(new RadioOption(LC.L_Generic, 0));
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getSponsorId() {
		return sponsorId;
	}

	public void setSponsorFkSite(Integer sponsorFkSite) {
		this.sponsorFkSite = sponsorFkSite;
	}

	public Integer getSponsorFkSite() {
		return sponsorFkSite;
	}

	public void setSponsorFkAdd(Integer sponsorFkAdd) {
		this.sponsorFkAdd = sponsorFkAdd;
	}

	public Integer getSponsorFkAdd() {
		return sponsorFkAdd;
	}

	public void setResponsibleParty(Integer responsibleParty) {
		this.responsibleParty = responsibleParty;
	}

	public Integer getResponsibleParty() {
		return responsibleParty;
	}

	public void setResponsiblePartyList(List<RadioOption> responsiblePartyList) {
		this.responsiblePartyList = responsiblePartyList;
	}

	public List<RadioOption> getResponsiblePartyList() {
		return responsiblePartyList;
	}

	public void setSponsorContactType(Integer sponsorContactType) {
		this.sponsorContactType = sponsorContactType;
	}

	public Integer getSponsorContactType() {
		return sponsorContactType;
	}

	public void setSponsorContactTypeList(List<RadioOption> sponsorContactTypeList) {
		this.sponsorContactTypeList = sponsorContactTypeList;
	}

	public List<RadioOption> getSponsorContactTypeList() {
		return sponsorContactTypeList;
	}

	public void setPersonalContactId(String personalContactId) {
		this.personalContactId = personalContactId;
	}

	public String getPersonalContactId() {
		return personalContactId;
	}

	public void setPersonalContactFkUser(Integer personalContactFkUser) {
		this.personalContactFkUser = personalContactFkUser;
	}

	public Integer getPersonalContactFkUser() {
		return personalContactFkUser;
	}

	public void setPersonalContactFkAdd(Integer personalContactFkAdd) {
		this.personalContactFkAdd = personalContactFkAdd;
	}

	public Integer getPersonalContactFkAdd() {
		return personalContactFkAdd;
	}

	public void setGenericContactId(String genericContactId) {
		this.genericContactId = genericContactId;
	}

	public String getGenericContactId() {
		return genericContactId;
	}

	public void setGenericContactTitle(String genericContactTitle) {
		this.genericContactTitle = genericContactTitle;
	}

	public String getGenericContactTitle() {
		return genericContactTitle;
	}

	public void setResponsiblePartyEmail(String responsiblePartyEmail) {
		this.responsiblePartyEmail = responsiblePartyEmail;
	}

	public String getResponsiblePartyEmail() {
		return responsiblePartyEmail;
	}

	public void setResponsiblePartyPhone(String responsiblePartyPhone) {
		this.responsiblePartyPhone = responsiblePartyPhone;
	}

	public String getResponsiblePartyPhone() {
		return responsiblePartyPhone;
	}

	public void setResponsiblePartyPhoneExt(String responsiblePartyPhoneExt) {
		this.responsiblePartyPhoneExt = responsiblePartyPhoneExt;
	}

	public String getResponsiblePartyPhoneExt() {
		return responsiblePartyPhoneExt;
	}

	public void setSponsorTty(String sponsorTty) {
		this.sponsorTty = sponsorTty;
	}

	public String getSponsorTty() {
		return sponsorTty;
	}

	public void setSponsorFax(String sponsorFax) {
		this.sponsorFax = sponsorFax;
	}

	public String getSponsorFax() {
		return sponsorFax;
	}

	public void setSponsorUrl(String sponsorUrl) {
		this.sponsorUrl = sponsorUrl;
	}

	public String getSponsorUrl() {
		return sponsorUrl;
	}

	public void setSponsorContactFromStudy(String sponsorContactFromStudy) {
		this.sponsorContactFromStudy = sponsorContactFromStudy;
	}

	public String getSponsorContactFromStudy() {
		return sponsorContactFromStudy;
	}

	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}

	public String getSponsorName() {
		return sponsorName;
	}

	public void setSponsorStreet(String sponsorStreet) {
		this.sponsorStreet = sponsorStreet;
	}

	public String getSponsorStreet() {
		return sponsorStreet;
	}

	public void setSponsorCity(String sponsorCity) {
		this.sponsorCity = sponsorCity;
	}

	public String getSponsorCity() {
		return sponsorCity;
	}

	public void setSponsorState(String sponsorState) {
		this.sponsorState = sponsorState;
	}

	public String getSponsorState() {
		return sponsorState;
	}

	public void setSponsorZip(String sponsorZip) {
		this.sponsorZip = sponsorZip;
	}

	public String getSponsorZip() {
		return sponsorZip;
	}

	public void setSponsorCountry(String sponsorCountry) {
		this.sponsorCountry = sponsorCountry;
	}

	public String getSponsorCountry() {
		return sponsorCountry;
	}

	public void setSponsorEmail(String sponsorEmail) {
		this.sponsorEmail = sponsorEmail;
	}

	public String getSponsorEmail() {
		return sponsorEmail;
	}

	public void setSponsorPhone(String sponsorPhone) {
		this.sponsorPhone = sponsorPhone;
	}

	public String getSponsorPhone() {
		return sponsorPhone;
	}

	public void setPersonalContactFirstName(String personalContactFirstName) {
		this.personalContactFirstName = personalContactFirstName;
	}

	public String getPersonalContactFirstName() {
		return personalContactFirstName;
	}

	public void setPersonalContactMiddleName(String personalContactMiddleName) {
		this.personalContactMiddleName = personalContactMiddleName;
	}

	public String getPersonalContactMiddleName() {
		return personalContactMiddleName;
	}

	public void setPersonalContactLastName(String personalContactLastName) {
		this.personalContactLastName = personalContactLastName;
	}

	public String getPersonalContactLastName() {
		return personalContactLastName;
	}

	public void setPersonalContactStreetAddress(String personalContactStreetAddress) {
		this.personalContactStreetAddress = personalContactStreetAddress;
	}

	public String getPersonalContactStreetAddress() {
		return personalContactStreetAddress;
	}

	public void setPersonalContactCity(String personalContactCity) {
		this.personalContactCity = personalContactCity;
	}

	public String getPersonalContactCity() {
		return personalContactCity;
	}

	public void setPersonalContactState(String personalContactState) {
		this.personalContactState = personalContactState;
	}

	public String getPersonalContactState() {
		return personalContactState;
	}

	public void setPersonalContactZip(String personalContactZip) {
		this.personalContactZip = personalContactZip;
	}

	public String getPersonalContactZip() {
		return personalContactZip;
	}

	public void setPersonalContactEmail(String personalContactEmail) {
		this.personalContactEmail = personalContactEmail;
	}

	public String getPersonalContactEmail() {
		return personalContactEmail;
	}

	public void setPersonalContactPhone(String personalContactPhone) {
		this.personalContactPhone = personalContactPhone;
	}

	public String getPersonalContactPhone() {
		return personalContactPhone;
	}

	public void setPersonalContactTty(String personalContactTty) {
		this.personalContactTty = personalContactTty;
	}

	public String getPersonalContactTty() {
		return personalContactTty;
	}

	public void setPersonalContactFax(String personalContactFax) {
		this.personalContactFax = personalContactFax;
	}

	public String getPersonalContactFax() {
		return personalContactFax;
	}

	public void setPersonalContactUrl(String personalContactUrl) {
		this.personalContactUrl = personalContactUrl;
	}

	public String getPersonalContactUrl() {
		return personalContactUrl;
	}

	public void setPersonalContactCountry(String personalContactCountry) {
		this.personalContactCountry = personalContactCountry;
	}

	public String getPersonalContactCountry() {
		return personalContactCountry;
	}

}
