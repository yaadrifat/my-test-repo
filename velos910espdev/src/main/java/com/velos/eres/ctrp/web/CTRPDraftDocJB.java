/*
 * Classname : CTRPDraftDocJB
 * 
 * Version information: 1.0
 *
 * Date: 12/14/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.web;

import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.eres.ctrp.business.CTRPDraftDocBean;
import com.velos.eres.ctrp.business.CTRPDraftDocDao;
import com.velos.eres.ctrp.service.CTRPDraftDocAgent;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class CTRPDraftDocJB {

	private Integer id=0;
	private Integer  fkCtrpDraft=0;
	private Integer  fkStudyApndx=0;
	private Integer  fkCtrpDocType=0;
	
    public CTRPDraftDocJB(){
      	
    }
	private static final String USER_ID = "userId";
	private static final String IP_ADD = "ipAdd";

   /**
    * 
    * @param id
    * @param fkCtrpDraft
    * @param fkStudyApndx
    * @param fkCtrpDocType
    */
    public CTRPDraftDocJB(Integer id,Integer fkCtrpDraft,Integer fkStudyApndx, Integer fkCtrpDocType){
    	super();
    	setId(id);
    	setFkCtrpDraft(fkCtrpDraft);
    	setFkStudyApndx(fkStudyApndx);
    	setFkCtrpDocType(fkCtrpDocType);
    }
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 
	public Integer getFkCtrpDraft() {
		return fkCtrpDraft;
	}
	public void setFkCtrpDraft(Integer fkCtrpDraft) {
		this.fkCtrpDraft = fkCtrpDraft;
	}
	public Integer getFkStudyApndx() {
		return fkStudyApndx;
	}
	public void setFkStudyApndx(Integer fkStudyApndx) {
		this.fkStudyApndx = fkStudyApndx;
	}
	public Integer getFkCtrpDocType() {
		return fkCtrpDocType;
	}
	public void setFkCtrpDocType(Integer fkCtrpDocType) {
		this.fkCtrpDocType = fkCtrpDocType;
	}
	 
		
	private CTRPDraftDocAgent getCtrpDraftDocAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (CTRPDraftDocAgent)ic.lookup(JNDINames.CtrpDraftDocAgentHome);
	}
	/**
	 * 
	 * @param argMap
	 * @return output
	 */
	
	public Integer setCtrpDraftDocInfo(HashMap<String, String> argMap) {
		Integer output = 0;
		CTRPDraftDocBean ctrpDraftDocBean = new CTRPDraftDocBean();
		fillCtrpDraftDocBean(ctrpDraftDocBean);
		if (argMap.get(IP_ADD) != null) {
			ctrpDraftDocBean.setIpAdd(argMap.get(IP_ADD));
		}
		try {
			CTRPDraftDocAgent ctrpDraftDocAgent = this.getCtrpDraftDocAgent();
			if (id > 0) {
				fillCtrpDraftBeanDocLastModifiedBy(ctrpDraftDocBean, argMap);
				output = ctrpDraftDocAgent.updateCtrpDraftDocDetails(ctrpDraftDocBean);
			} else {
				fillCtrpDraftDocBeanCreator(ctrpDraftDocBean, argMap);
				
				output = ctrpDraftDocAgent.setCtrpDraftDocDetails(ctrpDraftDocBean);
				id = output; // newly created ID
			}
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateCtrpDraftNonIndustrial: "+e);
			output = -1;
		}
		return output;
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 */
	private void fillCtrpDraftDocBean(CTRPDraftDocBean ctrpDraftDocBean) {
		if (id > 0) { ctrpDraftDocBean.setPkCtrpDoc(this.id); }
		ctrpDraftDocBean.setFkCtrpDocType(this.fkCtrpDocType);
		ctrpDraftDocBean.setFkCtrpDraft(this.fkCtrpDraft);
		ctrpDraftDocBean.setFkStudyApndx(this.fkStudyApndx);
		
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 * @param argMap
	 */
	
	private void fillCtrpDraftBeanDocLastModifiedBy(CTRPDraftDocBean ctrpDraftDocBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftDocBean.setLastModifiedBy(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 * @param argMap
	 */
	
	private void fillCtrpDraftDocBeanCreator(CTRPDraftDocBean ctrpDraftDocBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftDocBean.setCreator(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	
	/**
	 * 
	 * @return int
	 */
	 public int setCtrpDraftDocDetails() {
	        int apndxId = 0;
	        try {
	        	CTRPDraftDocAgent CTRPDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	            apndxId = CTRPDraftDocAgentRObj.setCtrpDraftDocDetails(this.createCtrpDraftDocStateKeeper());
	        }
	        catch (Exception e) {
	            Rlog.debug("CTRPDraftDoc", "in session bean - set CTRPDraftDoc details");
	            return -1;
	        }
	        return apndxId;
	    }
	 
	/**
	 * 
	 * @param fkCTRPDraft
	 * @return CTRPDraftDocDao
	 */
	
	public CTRPDraftDocDao getCTRPDraftDocDetails(int fkCTRPDraft) {
		CTRPDraftDocDao ctrpDraftDocDao = null;
	     try {
	    	 
	    	 CTRPDraftDocAgent CTRPDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	    	 ctrpDraftDocDao=CTRPDraftDocAgentRObj.getCTRPDraftDocDetails(fkCTRPDraft);
	     } catch (Exception e) {
	         Rlog.fatal("CTRPDraftDoc", "Error in Study  getCTRPDraftDocDetails"
	                 + e);
	     }
	      return ctrpDraftDocDao;

	 }
	 
	
	
	/**
	 * 
	 * @return CTRPDraftDocBean
	 */
	public CTRPDraftDocBean createCtrpDraftDocStateKeeper() {
	     Rlog.debug("CTRPDraftDoc", "CTRPDraftDocJB.createCtrpDraftDocStateKeeper ");
	     return new CTRPDraftDocBean(
	     		this.id,
	     		this.fkCtrpDocType,
	     		this.fkCtrpDraft,
	     		this.fkStudyApndx);
	 }
	
	 public int deleteCTRPDraftDocRecord(int CTRPDraftDocAgentRObj)
	 {
	 try {
		 
		 CTRPDraftDocAgent CtrpDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	  	 return CtrpDraftDocAgentRObj.deleteCTRPDraftDocRecord(CTRPDraftDocAgentRObj);
	  	 
	  } catch (Exception e) {
	      Rlog.debug("CTRPDraftDoc",
	              "Exception in deleteCTRPDraftDocRecord "
	                      + e.getMessage());
	      return -2;
	  }
	}

	

}
