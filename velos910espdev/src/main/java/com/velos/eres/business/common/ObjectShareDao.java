/*
 * Classname : 			FormLibDao
 * 
 * Version information: 1.0 
 *
 * Date: 				07/18/2003
 * 
 * Copyright notice: 	Velos, Inc
 *
 * Author: 				Anu Khanna
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The object share dao.<br>
 * <br>
 * This class has various methods to get resultsets of user data for different
 * parameters.
 * 
 * @author Anu Khanna
 * @vesion 1.0
 */

public class ObjectShareDao extends CommonDAO implements java.io.Serializable {

    private ArrayList sharedWithNames;

    private ArrayList sharedWithIds;

    private int cRows;
    
    private static String getRespAuthorsSQL = "SELECT DISTINCT creator FROM er_formslinear l "+
    	" WHERE L.FK_FORM IN (SELECT fk_formlib FROM er_linkedforms lf WHERE lf.pk_lf = ? "+
    	" AND lf_displaytype = RPAD (?, 2, ' ') ) AND creator <> ? ";

    public ObjectShareDao() {
        sharedWithNames = new ArrayList();

        sharedWithIds = new ArrayList();

        int cRows;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getSharedWithIds() {
        return this.sharedWithIds;
    }

    public void setSharedWithIds(ArrayList sharedWithIds) {
        this.sharedWithIds = sharedWithIds;
    }

    public ArrayList getSharedWithNames() {
        return this.sharedWithNames;
    }

    public void setSharedWithNames(ArrayList sharedWithNames) {
        this.sharedWithNames = sharedWithNames;
    }

    public void setSharedWithIds(Integer sharedWithId) {
        sharedWithIds.add(sharedWithId);
    }

    public void setSharedWithNames(String sharedWithName) {
        sharedWithNames.add(sharedWithName);
    }

    /**
     * Insert a record into the objectshare table by type.
     * 
     * @param fkObj - Object FK (e.g. Linked Form Id)
     * @param objNumber - 6=private, 5=user, 4=group, etc.
     * @param objSharedId - User Id who has the object share privilege
     * @param objSharedType - P=private, U=user, G=group, etc.
     * @param creator - Creator of the record
     * @param ipAdd - IP Address
     * @return - Code returned by the insert statement
     */
    public int insertIntoObjectShareByType(String fkObj, String objNumber,
            String objSharedId, String objSharedType, String creator, String ipAdd) {
        int ret = -1;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append(" insert into ER_OBJECTSHARE(PK_OBJECTSHARE, OBJECT_NUMBER, FK_OBJECT, ");
            sb.append("  FK_OBJECTSHARE_ID, OBJECTSHARE_TYPE, CREATOR, IP_ADD, CREATED_ON, RECORD_TYPE) ");
            sb.append(" values(SEQ_ER_OBJECTSHARE.NEXTVAL, ?, ?, ?, ?, ?, ?, ");
            sb.append("  sysdate, 'N') ");
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.setInt(1, StringUtil.stringToNum(objNumber));
            pstmt.setInt(2, StringUtil.stringToNum(fkObj));
            pstmt.setInt(3, StringUtil.stringToNum(objSharedId));
            pstmt.setString(4, objSharedType);
            pstmt.setInt(5, StringUtil.stringToNum(creator));
            pstmt.setString(6, ipAdd);
            ret = pstmt.executeUpdate();
        } catch(Exception e) {
            Rlog.fatal("common", "error in ObjectShareDao.insertIntoObjectShareByType(): " + e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        return ret;
    }
    
    
    /**
     * Insert a record into the objectshare table 
     * 
     * @param fkObj - Object FK (e.g. Linked Form Id)
     * @param formType - A=Account, PS=Patient Study, S=Study, etc.
     * @param creator - Creator of the record
     * @param ipAdd - IP Address
     * @return - Code returned by the insert statement
     */
    public int insertIntoObjectShareForRespAuthors(String fkObj, 
            String formType, String creator, String ipAdd) {
        int ret = -1;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(getRespAuthorsSQL);
            pstmt.setInt(1, StringUtil.stringToNum(fkObj));
            pstmt.setString(2, formType);
            pstmt.setInt(3, StringUtil.stringToNum(creator));
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	int authorId = rs.getInt("CREATOR");
            	System.out.println("authorId="+authorId);
            	int retInner = insertIntoObjectShareByType(fkObj, "6",
            			String.valueOf(authorId), "P", creator, ipAdd);
            	if (retInner < 0) {
            		Rlog.fatal("common", 
            				"error in ObjectShareDao.error - could not insert author into objectshare for "
            				+authorId);
            	}
            }
        } catch(Exception e) {
            Rlog.fatal("common", "error in ObjectShareDao.insertIntoObjectShareByType(): " + e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        return ret;
    }
    
    
    public int insertIntoObjectShare(int fkObj, String objName,
            String objSharedId, String objSharedType, String creator,
            String ipAdd) {
        Rlog.debug("common", "inside  insertIntoObjectShare");
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("common", "in  insertIntoObjectShare before SP");
            cstmt = conn
                    .prepareCall("{call SP_OBJECTSHAREWITH(?,?,?,?,?,?,?)}");

            cstmt.setInt(1, fkObj);
            cstmt.setString(2, objName);
            cstmt.setString(3, objSharedId);
            cstmt.setString(4, objSharedType);
            cstmt.setString(5, creator);
            cstmt.setString(6, ipAdd);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(7);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("common", "error in SP" + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Retrieves the Object sharing information.
     * 
     * @param fkObj
     *            eresearch internal id for the object.
     * @param objShareType
     *            level of object shared at e.g
     *            U-User,G=Group,O-Organization,P-Private
     * @param module
     *            module for which sharing is implemeted.
     * 
     */

    public void getObjSharedWithInfo(int fkObj, String objShareType,
            String module) {

        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        int moduleNum = StringUtil.stringToNum(module);
        try {

            conn = getConnection();
            if (objShareType.equals("G")) {
                sql = " select grp_name , pk_grp from er_grps, er_objectshare "
                        + " where er_grps.pk_grp=er_objectshare.fk_objectshare_id "
                        + " and er_objectshare.fk_object= ? and "
                        + " er_objectshare.objectshare_type= ?  and object_number=? ";

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, fkObj);
                pstmt.setString(2, objShareType);
                pstmt.setInt(3, moduleNum);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("GRP_NAME"));
                    setSharedWithIds(new Integer(rs.getInt("PK_GRP")));
                    rows++;
                }
                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }
            if (objShareType.equals("U")) {
                sql = " select (usr_firstname||' '||usr_lastname) as usr_name , pk_user from er_user, er_objectshare "
                        + " where er_user.pk_user=er_objectshare.fk_objectshare_id "
                        + " and er_objectshare.fk_object= ? and "
                        + " er_objectshare.objectshare_type= ?  and object_number=? ";

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, fkObj);
                pstmt.setString(2, objShareType);
                pstmt.setInt(3, moduleNum);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("usr_name"));
                    setSharedWithIds(new Integer(rs.getInt("pk_user")));
                    rows++;
                }
                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }

            else if (objShareType.equals("S")) {
                sql = " select study_number , pk_study from er_study, er_objectshare "
                        + " where er_study.pk_study=er_objectshare.fk_objectshare_id "
                        + " and er_objectshare.fk_object= ? and "
                        + " er_objectshare.objectshare_type= ? and object_number=?";

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, fkObj);
                pstmt.setString(2, objShareType);
                pstmt.setInt(3, moduleNum);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("STUDY_NUMBER"));
                    setSharedWithIds(new Integer(rs.getInt("PK_STUDY")));
                    rows++;
                }

                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }

            else if (objShareType.equals("O")) {
                sql = " select site_name , pk_site from er_site, er_objectshare "
                        + " where er_site.pk_site=er_objectshare.fk_objectshare_id "
                        + " and er_objectshare.fk_object= ? and "
                        + " er_objectshare.objectshare_type= ? and object_number=?";

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, fkObj);
                pstmt.setString(2, objShareType);
                pstmt.setInt(3, moduleNum);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("SITE_NAME"));
                    setSharedWithIds(new Integer(rs.getInt("PK_SITE")));
                    rows++;
                }
                setCRows(rows);

            } else if (objShareType.equals("P")) {
                sql = " select (usr_firstname||' '||usr_lastname) as usr_name , pk_user from er_user, er_objectshare "
                        + " where er_user.pk_user=er_objectshare.fk_objectshare_id "
                        + " and er_objectshare.fk_object= ? and "
                        + " er_objectshare.objectshare_type= ?  and object_number=? ";

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, fkObj);
                pstmt.setString(2, objShareType);
                pstmt.setInt(3, moduleNum);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("usr_name"));
                    setSharedWithIds(new Integer(rs.getInt("pk_user")));
                    rows++;
                }
                setCRows(rows);

            }

        } catch (SQLException ex) {
            Rlog.fatal("common", "error in getObjSharedWithInfo" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
