/*
 * Classname : UsrSiteGrpBean
 * 
 * Version information: 1.1
 *
 * Date: 25/01/2012
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Yogesh
 */

package com.velos.eres.business.usrSiteGrp.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The User-Org-Group  entity bean.<br>
 * <br>
 * 
 * @author Yogesh
 * @version 1.1 25/01/2012
 */
@Entity
@Table(name = "ER_USRSITE_GRP")
public class UsrSiteGrpBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7326761328521153158L;
	/**
     * 
     */


    /**
     * the UserSiteGroup Id
     */
    public int userSiteGrp;
    
    /**
     * the fkuser Id
     */
    public Integer fkuserId;
    /**
     * the fkgroupIds Id
     */
    public Integer fkGrpIds;
    
    /**
     * the fkUserSite Id
     */
    public Integer fkUserSite;
    
    /**
     * the creater 
     */
    public Integer creator;
    
    /**
     * the lastmodifiedby Id
     */
    public Integer modifiedBy;
    
    /**
     * the ip address Id
     */
    public String ipAdd;
  
    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return unique Id for one User-Site-Group combination
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USRSITE_GRP", allocationSize=1)
    @Column(name = "PK_USR_SITE_GRP")
    public int getUserSiteGrp() {
        return this.userSiteGrp;
    }

    public void setUserSiteGrp(int userSiteGrp) {
        this.userSiteGrp = userSiteGrp;
    }

    /**
     * 
     * 
     * @return User Id
     */
    @Column(name = "FK_GRP_ID")
    public String getFkGrpIds() {
        return StringUtil.integerToString(this.fkGrpIds);
    }

    /**
     * 
     * 
     * @param fkGrpIds
     *            Ids for groups for one site
     */
    public void setFkGrpIds(String fkGrpIds) {
        if (fkGrpIds != null) {
            this.fkGrpIds = Integer.valueOf(fkGrpIds);
        }
    }

    /**
     * 
     * 
     * @return User Id
     */
    @Column(name = "FK_USER_SITE")
    public String getFkUserSite() {
        return StringUtil.integerToString(this.fkUserSite);
    }

    /**
     * 
     * 
     * @param fkUserSite
     *            unique Id for User site
     */
    public void setFkUserSite(String fkUserSite) {
        if (fkUserSite != null) {
            this.fkUserSite = Integer.valueOf(fkUserSite);
        }
    }
    
    /**
     * 
     * 
     * @return User Id
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    /**
     * 
     * 
     * @return User Id
     */
    @Column(name = "FK_USER")
    public String getFkuserId() {
        return StringUtil.integerToString(this.fkuserId);
    }

    /**
     * 
     * 
     * @param fkuserId
     *            unique Id for one User
     */
    public void setFkuserId(String fkuserId) {
        if (fkuserId != null) {
            this.fkuserId = Integer.valueOf(fkuserId);
        }
    }
  

    // END OF GETTER SETTER METHODS

    /**
     * updates the User Site Group Record
     * 
     * @param ugsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateUsrSiteGrp(UsrSiteGrpBean ugsk) {
        try {
            setUserSiteGrp(ugsk.userSiteGrp);
            setFkuserId(ugsk.fkuserId.toString());
            setFkGrpIds(ugsk.fkGrpIds.toString());
            setFkUserSite(ugsk.fkUserSite.toString());
            Rlog.debug("usrgrp", "UsrGrpBean.updateUsrGrp");
            setCreator(ugsk.getCreator());
            setModifiedBy(ugsk.getModifiedBy());
            setIpAdd(ugsk.getIpAdd());
            setCreator(ugsk.getCreator());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("usrgrp", " error in UsrSiteGrpBean.updateUsrSiteGrp");
            return -2;
        }
    }
    public UsrSiteGrpBean() {

    }

    public UsrSiteGrpBean(int userSiteGrp, String fkuserId, String fkGrpIds,String fkUserSite,
            String creator, String modifiedBy, String ipAdd) {
        super();
        setUserSiteGrp(userSiteGrp);
        setFkuserId(fkuserId);
        setFkGrpIds(fkGrpIds);
        setFkUserSite(fkUserSite);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
