/*
 *  Classname : SasExportDao
 *
 *  Version information: 1.0
 *
 *  Date 04/13/2004
 *
 *  Copyright notice: Velos Inc.
 */
package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.dynrep.DynRepJB;
import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.study.StudyJB;

/**
 * Description of the Class
 * 
 * @author vabrol
 * @created July 2, 2004
 */
/*
 * Modified by Sonia Abrol, to show multiple choice field's display or data
 * value according to user's choice
 */
public class SasExportDao extends CommonDAO implements java.io.Serializable {

	public static String identifierPrefix = "VE";
	
	private int Id;

    private String expType;

    private ArrayList colList;

    private ArrayList colTypes;

    private ArrayList varTypes;

    private ArrayList formatList;

    private ArrayList fieldPkList;

    private StringBuffer procFormat;

    private StringBuffer procInput;

    private StringBuffer procData;

    private StringBuffer formatCommand;

    private StringBuffer labelCommand;

    private HashMap TypeAttr;

    private HashMap FldAttr;

    private String formId;

    private String formName;

    private DynRepDao dynDao;

    private DynRepJB dynrepB;

    private PersonJB person;

    private StudyJB studyB;

    private ArrayList reportCol;

    private ArrayList reportColName;

    private ArrayList reportColType;

    private ArrayList reportColDisp;

    private String reportType;

    /**
     * Flag to Display Data value of multiple choice fields. possible values:
     * 0:true,1:false (default, display value will be shown)
     */

    private ArrayList colUseDataValues;

    /**
     * Constructor for the SasExportDao object
     */
    public SasExportDao() {
    }

    /**
     * Constructor for the SasExportDao object
     * 
     * @param Type
     *            Description of the Parameter
     * @param Id
     *            Description of the Parameter
     */
    public SasExportDao(String Type, int Id) {
        this.Id = Id;
        this.expType = Type;
        colList = new ArrayList();
        colTypes = new ArrayList();
        varTypes = new ArrayList();
        formatList = new ArrayList();
        fieldPkList = new ArrayList();
        procFormat = new StringBuffer();
        procInput = new StringBuffer();
        procData = new StringBuffer();
        formatCommand = new StringBuffer();
        labelCommand = new StringBuffer();
        TypeAttr = new HashMap();
        FldAttr = new HashMap();
        formId = "";
        formName = "";
        dynDao = new DynRepDao();
        dynrepB = new DynRepJB();
        person = new PersonJB();
        studyB = new StudyJB();
        reportCol = new ArrayList();
        reportColName = new ArrayList();
        reportColType = new ArrayList();
        reportColDisp = new ArrayList();
        colUseDataValues = new ArrayList();
    }

    /**
     * Returns the value of colUseDataValues.
     */
    public ArrayList getColUseDataValues() {
        return colUseDataValues;
    }

    /**
     * Sets the value of colUseDataValues.
     * 
     * @param colUseDataValues
     *            The value to assign colUseDataValues.
     */
    public void setColUseDataValues(ArrayList colUseDataValues) {
        this.colUseDataValues = colUseDataValues;
    }

    /**
     * Returns the value of procFormat.
     * 
     * @return The procFormat value
     */
    public StringBuffer getProcFormat() {
        return procFormat;
    }

    /**
     * Sets the value of procFormat.
     * 
     * @param procFormat
     *            The value to assign procFormat.
     */
    public void setProcFormat(StringBuffer procFormat) {
        this.procFormat = procFormat;
    }

    /**
     * Returns the value of procInput.
     * 
     * @return The procInput value
     */
    public StringBuffer getProcInput() {
        return procInput;
    }

    /**
     * Sets the value of procInput.
     * 
     * @param procInput
     *            The value to assign procInput.
     */
    public void setProcInput(StringBuffer procInput) {
        this.procInput = procInput;
    }

    /**
     * Returns the value of VarTypes.
     * 
     * @return The varTypes value
     */
    public ArrayList getVarTypes() {
        return varTypes;
    }

    /**
     * Sets the value of VarTypes
     * 
     * @param varTypes
     *            The new varTypes value
     */
    public void setVarTypes(ArrayList varTypes) {
        this.varTypes = varTypes;
    }

    /**
     * Sets the varTypes attribute of the SasExportDao object
     * 
     * @param VarType
     *            The new varTypes value
     */
    public void setVarTypes(String VarType) {
        this.varTypes.add(VarType);
    }

    /**
     * Returns the value of procData.
     * 
     * @return The procData value
     */
    public StringBuffer getProcData() {
        return procData;
    }

    /**
     * Sets the value of procData.
     * 
     * @param procData
     *            The value to assign procData.
     */
    public void setProcData(StringBuffer procData) {
        this.procData = procData;
    }

    /**
     * Returns the value of fieldPkList.
     * 
     * @return The fieldPkList value
     */
    public ArrayList getFieldPkList() {
        return fieldPkList;
    }

    /**
     * Sets the value of fieldPkList.
     * 
     * @param fieldPkList
     *            The value to assign fieldPkList.
     */
    public void setFieldPkList(ArrayList fieldPkList) {
        this.fieldPkList = fieldPkList;
    }

    /**
     * Sets the fieldPkList attribute of the SasExportDao object
     * 
     * @param fieldPk
     *            The new fieldPkList value
     */
    public void setFieldPkList(String fieldPk) {
        this.fieldPkList.add(fieldPk);
    }

    /**
     * Returns the value of labelCommand.
     * 
     * @return The labelCommand value
     */
    public StringBuffer getLabelCommand() {
        return labelCommand;
    }

    /**
     * Sets the value of labelCommand.
     * 
     * @param labelCommand
     *            The value to assign labelCommand.
     */
    public void setLabelCommand(StringBuffer labelCommand) {
        this.labelCommand = labelCommand;
    }

    /**
     * Returns the value of formatCommand.
     * 
     * @return The formatCommand value
     */
    public StringBuffer getFormatCommand() {
        return formatCommand;
    }

    /**
     * Sets the value of formatCommand.
     * 
     * @param formatCommand
     *            The value to assign formatCommand.
     */
    public void setFormatCommand(StringBuffer formatCommand) {
        this.formatCommand = formatCommand;
    }

    /**
     * Gets the format attribute of the SasExportDao object
     * 
     * @param seq
     *            Description of the Parameter
     * @return The format value
     */
    public String getFormat(int seq) {
        String temp = "";
        if (seq < fieldPkList.size()) {
            temp = (String) fieldPkList.get(seq);
        }

        int index = colList.indexOf(temp);
        if (index >= 0) {
            return ((String) formatList.get(index));
        } else {
            return "INVALID";
        }
    }

    /**
     * Returns the value of formId.
     */
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of formId.
     * 
     * @param formId
     *            The value to assign formId.
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Returns the value of formName.
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of formName.
     * 
     * @param formName
     *            The value to assign formName.
     */
    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     * Returns the value of colList.
     */
    public ArrayList getColList() {
        return colList;
    }

    /**
     * Sets the value of colList.
     * 
     * @param colList
     *            The value to assign colList.
     */
    public void setColList(ArrayList colList) {
        this.colList = colList;
    }

    /**
     * Returns the value of colTypes.
     */
    public ArrayList getColTypes() {
        return colTypes;
    }

    /**
     * Sets the value of colTypes.
     * 
     * @param colTypes
     *            The value to assign colTypes.
     */
    public void setColTypes(ArrayList colTypes) {
        this.colTypes = colTypes;
    }

    /**
     * Returns the value of reportCol.
     */
    public ArrayList getReportCol() {
        return reportCol;
    }

    /**
     * Sets the value of reportCol.
     * 
     * @param reportCol
     *            The value to assign reportCol.
     */
    public void setReportCol(ArrayList reportCol) {
        this.reportCol = reportCol;
    }

    /**
     * Returns the value of reportColName.
     */
    public ArrayList getReportColName() {
        return reportColName;
    }

    /**
     * Sets the value of reportColName.
     * 
     * @param reportColName
     *            The value to assign reportColName.
     */
    public void setReportColName(ArrayList reportColName) {
        this.reportColName = reportColName;
    }

    /**
     * Returns the value of reportColType.
     */
    public ArrayList getReportColType() {
        return reportColType;
    }

    /**
     * Sets the value of reportColType.
     * 
     * @param reportColType
     *            The value to assign reportColType.
     */
    public void setReportColType(ArrayList reportColType) {
        this.reportColType = reportColType;
    }

    /**
     * Returns the value of reportColDisp.
     */
    public ArrayList getReportColDisp() {
        return reportColDisp;
    }

    /**
     * Sets the value of reportColDisp.
     * 
     * @param reportColDisp
     *            The value to assign reportColDisp.
     */
    public void setReportColDisp(ArrayList reportColDisp) {
        this.reportColDisp = reportColDisp;
    }

    /**
     * Returns the value of reportType.
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * Sets the value of reportType.
     * 
     * @param reportType
     *            The value to assign reportType.
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    // end of get/set metods
    /**
     * Method to generate SAS Structure.
     */
    public void GenerateStructure() {
        Rlog.debug("common",
                "In SasExport:generateStructure:  starting *************");
        if (this.expType.equals("adhoc")) {

            Rlog.debug("common", "In SasExport:generateStructure:value of Id"
                    + Id);
            dynDao = dynrepB.populateHash(EJBUtil.integerToString(new Integer(
                    Id)));
            TypeAttr = dynDao.getTypeAttr();
            Rlog.debug("common",
                    "In SasExport:generateStructure:value of TYPEATTR"
                            + TypeAttr);
            if (TypeAttr.containsKey("formId")) {
                formId = (String) TypeAttr.get("formId");
            }
            Rlog.debug("common",
                    "In SasExport:generateStructure:value of FormId" + formId
                            + "COLLIST" + dynDao.getMapColIds());
            Rlog.debug("common",
                    "In SasExport:generateStructure:value of Attributes"
                            + dynDao.getAttributes());
            // dynDao.getMapData(formId);
            colList = dynDao.getMapColIds();
            colTypes = dynDao.getMapColTypes();
            // Generate Proc Format
            getFieldPk();
            GenerateProcFormat();
            GenerateInput();
            // GenerateFormatLabel();
            // GenerateData();
        } else if (this.expType.equals("madhoc")) {
            Rlog.debug("common",
                    "In SasExport:generateStructure:value of export Type"
                            + expType);
            Rlog.debug("common", "In SasExport:generateStructure:value of Id"
                    + Id);
            // dynDao = dynrepB.fillReportContainer(EJBUtil.integerToString(new
            // Integer(Id)));
            // TypeAttr = dynDao.getTypeAttr();
            // Rlog.debug("common", "In SasExport:generateStructure:value of
            // TYPEATTR" + TypeAttr);
            // if (TypeAttr.containsKey("formId")) {
            // formId = (String) TypeAttr.get("formId");
            // }

            // Rlog.debug("common", "In SasExport:generateStructure:value of
            // Attributes" + dynDao.getAttributes());
            // dynDao.getMapData(formId);
            colList = this.getColList();
            colTypes = this.getColTypes();
            Rlog.debug("common",
                    "In SasExport:generateStructure:value of FormId" + formId
                            + "COLLIST" + colList + "COLtypes" + colTypes);

            // Generate Proc Format
            getFieldPk();
            GenerateProcFormat();
            GenerateInput();
            // GenerateFormatLabel();
            // GenerateData();

        }

    }

    /**
     * Method to generate SAS 'Proc Format' statements
     */
    public void GenerateProcFormat() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String varType = "";
        String varNum = "N";
        String varChar = "N";
        String dataVal = "";
        StringBuffer procFormatTemp = new StringBuffer();
        try {
            conn = getConnection();
            String sql = "select fld_name,fld_uniqueid,pk_field,fld_name,fldresp_dispval,fldresp_dataval,fldresp_seq from er_fldlib,er_fldresp"
                    + " where pk_field=? and pk_field=fk_field order by fldresp_seq";
            Rlog.debug("common", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            Rlog.debug("common", "SASExport:GenerateProcFromat:colList"
                    + colList + "\n colTypes" + colTypes);
            for (int i = 0; i < colList.size(); i++) {
                if ((((String) colTypes.get(i)).equals("MC"))
                        || (((String) colTypes.get(i)).equals("MR"))
                        || (((String) colTypes.get(i)).equals("MD"))) {

                    Rlog.debug("common", "pstmt" + pstmt);
                    varChar = "N";
                    pstmt.clearParameters();
                    pstmt.setInt(1, EJBUtil
                            .stringToNum((String) colList.get(i)));
                    Rlog.debug("common",
                            "id to process in GenereateProcFormat "
                                    + (String) colList.get(i));
                    rs = pstmt.executeQuery();
                    Rlog.debug("common", "rs-Using Classes in libext" + rs);
                    if (procFormat.length() == 0) {
                        procFormat.append("PROC FORMAT;\n");
                    }
                    if (procFormatTemp.length() > 0) {
                        procFormatTemp.delete(0, procFormatTemp.length());
                    }
                    while (rs.next()) {
                        dataVal = rs.getString("fldresp_dataval");
                        dataVal = (dataVal == null) ? "" : dataVal;

                        if (rs.getRow() == 1) {

                            procFormatTemp
                                    .append((getIdentifierPrefix())+ i + "FT  [VELSIZE]");
                        }
                        // to replace single quotes with [ ` ]
                        dataVal = StringUtil.replace(dataVal, "'", "`");
                        
                        procFormatTemp.append("\n[VELQUOTE]" + dataVal
                                + "[VELQUOTE]='"
                                + StringUtil.replace(rs.getString("fldresp_dispval"),"'","`")+ "'");
                        try {
                            if (varChar.equals("N")) {

                                Double.parseDouble(dataVal);
                            }
                        } catch (NumberFormatException e) {
                            varChar = "Y";
                            Rlog.debug("common",
                                    "SASEXPORT:GeneateProcFormat:*************not a number"
                                            + dataVal);
                        }
                    }
                    if (varChar.equals("Y")) {
                        varTypes.add("CHAR");
                        String tempStr = procFormatTemp.toString();
                        tempStr = StringUtil
                                .replace(tempStr, "[VELQUOTE]", "'");
                                                 
                        tempStr = StringUtil.replace(tempStr, "[VELSIZE]",
                                "(default=4000 max=4000)");
                        procFormatTemp = new StringBuffer(tempStr);
                        procFormat.append("Value $" + procFormatTemp.toString()
                                + ";\n");
                        // formatList.add("$VEL" + i + "FT");
                        formatList.add("$"+  getIdentifierPrefix() + i + "FT");
                    } else {
                        varTypes.add("NUM");
                        String tempStr = procFormatTemp.toString();
                        
                        tempStr = StringUtil.replace(tempStr, "[VELQUOTE]", "");
                        
                        tempStr = StringUtil.replace(tempStr, "[VELSIZE]", "");
                        procFormatTemp = new StringBuffer(tempStr);
                        procFormat.append("\nValue  _"
                                + procFormatTemp.toString() + ";" + "\n");
                        formatList.add("_"+ (getIdentifierPrefix()) + i + "FT");
                    }
                }
                // end if coltypes
                else {
                    formatList.add("_VELFT");
                }
                // this is added to keep the arraylist size consistent with
                // collist to avoid the indexexception
            }
            // end for loop
            Rlog.debug("common", "ProcFormat generated\n"
                    + procFormat.toString());
        } catch (SQLException e) {
            Rlog.fatal("common",
                    "{SQLEXCEPTION IN SasExport:generateProcFormat(id)}" + e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    // end GenereateProcFormat

    /**
     * Compiles the dataset and generate a SAS 'PROC INPUT' to use the data.
     * 
     * 
     */
    public void GenerateInput() {
        ArrayList repCol = new ArrayList();
        ArrayList repColName = new ArrayList();
        ArrayList repColType = new ArrayList();
        ArrayList repColDisp = new ArrayList();
        String colType = "";
        String colName = "";
        String colDisp = "";
        String dynType = "";

        if (expType.equals("adhoc")) {
            FldAttr = dynDao.getFldAttr();
            if (TypeAttr.containsKey("dynType")) {
                dynType = (String) TypeAttr.get("dynType");
            }
            Rlog
                    .debug("common",
                            "In SasExport:generateStructure:value of FldATTR"
                                    + FldAttr);

            if (FldAttr.containsKey("sessCol")) {
                repCol = (ArrayList) FldAttr.get("sessCol");
            }
            if (FldAttr.containsKey("sessColName")) {
                repColName = (ArrayList) FldAttr.get("sessColName");
            }
            if (FldAttr.containsKey("sessColType")) {
                repColType = (ArrayList) FldAttr.get("sessColType");
            }
            if (FldAttr.containsKey("sessColDisp")) {
                repColDisp = (ArrayList) FldAttr.get("sessColDisp");
            }
        } else if (expType.equals("madhoc")) {
            dynType = getReportType();
            repCol = this.getReportCol();
            repColName = this.getReportColName();
            repColType = this.getReportColType();
            repColDisp = this.getReportColDisp();

        }
        Rlog.debug("common", "***repCol" + repCol + "********colType"
                + repColType + "\n***************ColNAME" + repColName
                + "\n*********************format" + formatList
                + "\n***********************************" + fieldPkList
                + "\n*********************************colList" + colList);

        for (int i = 0; i < repCol.size(); i++) {
            colType = (String) repColType.get(i);

            colName = StringUtil.replace((String) repColDisp.get(i), " ", "");
            colName = (colName == null) ? "" : colName;
            if (colName.length() > 32)
                colName = colName.substring(0, 31);
            colDisp = ((String) repColName.get(i));

            if (procInput.length() == 100) {
                procInput.append("\n");
            }
            if (i == 0) {
                procInput.append("INPUT  ");
                /*
                 * if (dynType.equals("P")) { procInput.append("PatId");
                 * formatCommand.append("FORMAT PatId $char4000.;" + "\n");
                 * labelCommand.append("LABEL PatId='Patient Code';" + "\n"); }
                 * if (dynType.equals("S")) { procInput.append("StudyNumber");
                 * formatCommand.append("Format StudyNumber $char4000.;" +
                 * "\n"); labelCommand.append("LABEL StudyNumber='Study
                 * Number';" + "\n"); }
                 */

                if (colType.equals("MC") || colType.equals("MD")
                        || colType.equals("MR")) {
                    procInput.append(" '" + colName + "'n");
                    formatCommand.append("Format '" + colName + "'n "
                            + getFormat(i) + ".;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                } else if (colType.equals("ED")) {
                    formatCommand.append("Format '" + colName + "'n DATE9.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n:MMDDYY11.");
                } else if (colType.equals("ET")) {
                    formatCommand.append("Format '" + colName
                            + "'n $char4000.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n");
                } else if (colType.equals("EN")) {
                    formatCommand.append("Format '" + colName
                            + "'n $BEST4000.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n");
                } else {
                    procInput.append(" '" + colName + "'n");
                }
                /*
                 * public String getFormat(int seq){ integer temp
                 * =(Integer)fieldPkList.get(seq); int
                 * index=colList.indexOf(temp); if (index>=0){ return
                 * ((String)formatList.get(index)); } else { return "INVALID" } }
                 */
            } else {
                if (colType.equals("MC") || colType.equals("MD")
                        || colType.equals("MR")) {
                    procInput.append(" '" + colName + "'n");
                    formatCommand.append("Format '" + colName + "'n "
                            + getFormat(i) + ".;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                } else if (colType.equals("ED")) {
                    formatCommand.append("Format '" + colName + "'n DATE9.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n:MMDDYY11.\n");
                } else if (colType.equals("ET")) {
                    formatCommand.append("Format '" + colName
                            + "'n $char4000.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n");
                } else if (colType.equals("EN")) {
                    formatCommand.append("Format '" + colName
                            + "'n $BEST4000.;\n");
                    labelCommand.append("Label '" + colName + "'n='" + colDisp
                            + "';\n");
                    procInput.append(" '" + colName + "'n");
                } else {
                    procInput.append(" '" + colName + "'n");
                }
            }
        }
        procInput.append(";");
        Rlog.debug("common", "SASEXport:GenerateInput:procInput is"
                + procInput.toString());
    }

    public void GenerateData(String sql, String[] fldNames, String formType,
            ArrayList fldTypes, Hashtable htMore) {
        this.setReportColType(fldTypes);
        
        GenerateData(sql, fldNames, formType,htMore);
    }

    /**
     * Description of the Method
     * 
     * @param sql
     *            SQL to retrieve data
     * @param fldNames
     *            Array of fldnames to retrieve from resultset
     * @param formType
     *            Type of the form
     */
    public void GenerateData(String sql, String[] fldNames, String formType , Hashtable htMore) {
        String patientID = "";
        String value = "";
        String studyNumber = "";
        int personPk = 0;
        int studyPk = 0;
        String useDataValueSetting = "";
        ArrayList fldTypes = getColTypes();
        dynDao = dynrepB.getReportData(sql, fldNames, formType, this
                .getReportColType(), htMore);
        
        //System.out.println("GenerateData..after execution fldNames" + fldNames.toString());
        //System.out.println("GenerateData..after execution fldNames" + this.getReportColType() );
        
        
        ArrayList dataList = dynDao.getDataCollection();
        String fldType = "";
        for (int i = 0; i < dataList.size(); i++) {

            ArrayList tempList = (ArrayList) dataList.get(i);
            ArrayList tempVal = new ArrayList();

            /*
             * if (formType.equals("P")) { personPk =
             * EJBUtil.stringToNum((String) tempList.get(0)); if (!(personPk ==
             * 0)) { person.setPersonPKId(personPk); person.getPersonDetails();
             * patientID = person.getPersonPId(); patientID = (patientID ==
             * null) ? "" : patientID; } else { patientID = (String)
             * tempList.get(1); if (patientID == null) { patientID = "-"; } }
             * procData.append(patientID + "|"); } if (formType.equals("S")) {
             * studyPk = EJBUtil.stringToNum((String) tempList.get(0)); if
             * (!(studyPk == 0)) { studyB.setId(studyPk);
             * studyB.getStudyDetails(); studyNumber = studyB.getStudyNumber();
             * studyNumber = (studyNumber == null) ? "" : studyNumber; }
             * procData.append(studyNumber + "|"); }
             */

            if (tempList.size() == 0) {
                continue;
            }
            int maxTokens = 0;
            int tempCountToken = 0;
            int seq = 0;
            for (int j = 2; j < tempList.size(); j++) {
                ArrayList valList = new ArrayList();
                seq = 0;
                value = (String) tempList.get(j);
                fldType = ""+fldTypes.get(j-2);

                useDataValueSetting = (String) colUseDataValues.get(j - 2);
                Rlog.debug("common",
                        "SasExport:GenerateData:value of useDataValueSetting"
                                + useDataValueSetting);
                if (StringUtil.isEmpty(useDataValueSetting)) {
                    useDataValueSetting = "0";
                }

                if (value != null) {
                    if (value.indexOf("[VELSEP]") >= 0) {

                        value = StringUtil.replace(value, "[VELSEP]", "~");

                        StringTokenizer valSt = new StringTokenizer(value, "~");
                        tempCountToken = valSt.countTokens();
                        // valList.add(valSt);
                        tempVal.add(valSt);
                        if (tempCountToken > maxTokens) {
                            maxTokens = tempCountToken;
                        }
                        continue;
                    }
                } else {
                    value = "";
                }

                if (value.indexOf("[VELSEP1]") >= 0) {

                    if (useDataValueSetting.equals("1")) {
                        value = value
                                .substring((value.indexOf("[VELSEP1]")) + 9);
                    } else {
                        value = value.substring(0, value.indexOf("[VELSEP1]"));
                    }
                }

                if (value.indexOf("\n") >= 0) {
                    value = StringUtil.replace(value, "\n", " ");
                }

                if (value.indexOf("\r") >= 0) {
                    value = StringUtil.replace(value, "\r", " ");
                }
                if (value.indexOf("[VELCOMMA]") >= 0) {
                    value = StringUtil.replace(value, "[VELCOMMA]", ",");
                }
                
                if (value.length() > 0) {
                	value = StringUtil.replace(value, "'", "`");
                	if (!(fldType).equals("EN")){               	
                		value = "'" + value + "'";
                	}
                }
                
                procData.append(value + "|");
            }
            // start

            // end

            procData.append("\n");
        }

        procData.append(";");
        Rlog.debug("common", "SasExport:GenerateData:value of ProcData  is"
                + procData);
    }

    // end GenereateData

    /**
     * Populates the primary key set of the fields
     */
    public void getFieldPk() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String tempValue = "";
        this.fieldPkList = new ArrayList();
        try {
            conn = getConnection();
            String temp = "";
            /*
             * String sql = "select MP_MAPCOLNAME,mp_PKFLD from er_mapform
             * a,er_dynrepview b where " + "a.fk_form=? and fk_dynrep=? and
             * mp_mapcolname=dynrepview_col AND a.fk_form=b.fk_form "+ " order
             * by dynrepview_seq ";
             */
            String sql = "SELECT dynrepview_col,(SELECT mp_PKFLD FROM ER_MAPFORM WHERE fk_form = ? AND mp_mapcolname = dynrepview_col) AS mp_pkfld"
                    + " FROM ER_DYNREPVIEW b WHERE "
                    + " fk_dynrep=? ORDER BY dynrepview_seq";
            Rlog.debug("dynrep", "sql in getFieldPk" + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, EJBUtil.stringToNum(formId));
            Rlog.debug("common", "SASExport:getFiledPk:formId set is" + formId);
            pstmt.setInt(2, Id);
            Rlog.debug("common", "SASExport:getFiledPk:formId set is" + Id);
            Rlog.debug("dynrep", "pstmt in getFieldPk" + pstmt);
            rs = pstmt.executeQuery();
            Rlog.debug("dynrep", "rs in getFieldPk" + rs);
            while (rs.next()) {
                tempValue = rs.getString("mp_pkfld");
                tempValue = (tempValue == null) ? "0" : tempValue;
                this.setFieldPkList(tempValue);
            }
            Rlog.debug("common", "SASExport:FieldPKList genereated is"
                    + fieldPkList);
        } catch (SQLException e) {
            Rlog
                    .fatal("dynrep", "{SQLEXCEPTION IN SAsExport:getFiledPk()}"
                            + e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Returns the value of identifier name
     */
    private String getIdentifierPrefix() {
    		
        return SasExportDao.identifierPrefix;
    }

}
