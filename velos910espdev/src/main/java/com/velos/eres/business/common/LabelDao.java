/*
 * Classname			LabelDao.class
 * 
 * Version information 	1.0
 *
 * Date					04/23/2009
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * LabelDao for LabelDao
 * 
 * @author Sonia Abrol
 * @version : 1.0 04/23/2009
 */
 

public class LabelDao extends CommonDAO implements java.io.Serializable {
    private ArrayList templateFormats;

    private ArrayList templateTypes;

    private ArrayList templateNames;

    private ArrayList templatePks;

     private int cRows;

    public LabelDao() {
    	templateFormats = new ArrayList();
    	templateTypes = new ArrayList();
    	templateNames = new ArrayList();
    	templatePks = new ArrayList();
         
    }
    
    public void getTemplateList(String accountId, String templateType) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();
        if (templateType == null) { templateType = ""; }
        templateType = templateType.replaceAll("'", "''");
        if (templateType.length() == 0) { templateType = " ' ' "; }
        try {
            conn = getConnection();
            sqlBuffer.append(" select pk_lt, template_type, template_name from er_label_templates ")
            .append(" where template_type = '").append(templateType).append("' ")
            .append(" and ( template_subtype = 'default' or fk_account = ").append(accountId).append(" ) ")
            .append(" order by pk_lt ");
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setTemplatePk(rs.getInt("pk_lt"));
                setTemplateType(rs.getString("template_type"));
                setTemplateName(rs.getString("template_name"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("LabelDao","LabelDao.getTemplateFormat"+ex);
        } finally {
            try {
                if (pstmt != null) { pstmt.close(); }
            } catch (Exception e) {}
            try {
                if (conn != null) { conn.close(); }
            } catch (Exception e) {}
        }
    }
     
    public String getTemplateFormat(String templateType) {
        if (templateType == null) { return null; }

         PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();

        Clob templateCLob = null;
        String tempFormat = "";
        
        try {

            conn = getConnection();

            sqlBuffer
                    .append(" Select  template_format from er_label_templates where template_type = ?");
             
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            pstmt.setString(1, templateType);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                templateCLob = rs.getClob("template_format");

                if (!(templateCLob == null)) {
                	tempFormat = templateCLob.getSubString(1,
                            (int) templateCLob.length());
                }                 
            }
              
            return tempFormat;

        } catch (SQLException ex) {
            Rlog.fatal("LabelDao",
                    "LabelDao.getTemplateFormat"
                            + ex);
            return "";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getTemplateFormatByPk(int templatePk) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();

        Clob templateCLob = null;
        String tempFormat = "";
        
        try {

            conn = getConnection();

            sqlBuffer
                    .append(" Select  template_format from er_label_templates where pk_lt = ?");
             
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            pstmt.setInt(1, templatePk);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                templateCLob = rs.getClob("template_format");

                if (!(templateCLob == null)) {
                    tempFormat = templateCLob.getSubString(1,
                            (int) templateCLob.length());
                }                 
            }
              
            return tempFormat;

        } catch (SQLException ex) {
            Rlog.fatal("LabelDao",
                    "LabelDao.getTemplateFormat"
                            + ex);
            return "";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


	public ArrayList getTemplateFormats() {
		return templateFormats;
	}



	public void setTemplateFormats(ArrayList templateFormats) {
		this.templateFormats = templateFormats;
	}



	public ArrayList getTemplateTypes() {
		return templateTypes;
	}



	public void setTemplateTypes(ArrayList templateTypes) {
		this.templateTypes = templateTypes;
	}

    private void setTemplateType(String templateType) {
        this.templateTypes.add(templateType);
    }



	public ArrayList getTemplateNames() {
		return templateNames;
	}



	public void setTemplateNames(ArrayList templateNames) {
		this.templateNames = templateNames;
	}


	private void setTemplateName(String templateName) {
        this.templateNames.add(templateName);
    }


	public ArrayList getTemplatePks() {
	    return templatePks;
	}
	
	public void setTemplatePks(ArrayList templatePks) {
		this.templatePks = templatePks;
	}

    private void setTemplatePk(Integer templatePk) {
        this.templatePks.add(templatePk);
    }

	public int getCRows() {
		return cRows;
	}



	public void setCRows(int rows) {
		cRows = rows;
	}

        // end of class
}
