/*
 * Classname		MilepaymentBean.class
 * 
 * Version information	1.0
 *
 * Date					05/30/2002
 * 
 * Copyright notice    Velos Inc.
 */

package com.velos.eres.business.milepayment.impl;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Rights CMP entity bean.<br>
 * <br>
 * 
 * @author Sonika
 * @version 1.0
 * @ejbHome MilepaymentHome
 * @ejbRemote MilepaymentRObj
 */
@Entity
@Table(name = "er_milepayment")
public class MilepaymentBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257844381238507057L;

    /**
     * the Milepayment Id
     */
    public int id;

    /**
     * the Study Id to which payment is related
     */
    public Integer milepaymentStudyId;

    /**
     * the Milestone Id to which payment is related
     */
    public Integer milepaymentMileId;

    /**
     * the Milepayment Date
     */
    public java.util.Date milepaymentDate;

    /**
     * the Milepayment Amount
     */
    public Double milepaymentAmount;

    /**
     * the Milepayment Desc
     */
    public String milepaymentDesc;

    /**
     * the Milepayment Delflag
     */
    public String milepaymentDelFlag;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;
    
    /*
     * Comments
     */
    public String milepaymentComments ;
    
    /*
     * Payment type
     */
    public String milepaymentType ;
    
    
    
    
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_milepayment", allocationSize=1)
    @Column(name = "PK_MILEPAYMENT")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "FK_MILESTONE")
    public String getMilepaymentMileId() {
        return StringUtil.integerToString(this.milepaymentMileId);
    }

    public void setMilepaymentMileId(String milepaymentMileId) {
        if (milepaymentMileId != null) {
            this.milepaymentMileId = Integer.valueOf(milepaymentMileId);
        }
    }

    @Column(name = "FK_STUDY")
    public String getMilepaymentStudyId() {
        return StringUtil.integerToString(this.milepaymentStudyId);
    }

    public void setMilepaymentStudyId(String milepaymentStudyId) {
        if (milepaymentStudyId != null) {
            this.milepaymentStudyId = Integer.valueOf(milepaymentStudyId);
        }
    }

    @Column(name = "MILEPAYMENT_DATE")
    public Date getMilepaymentDt() {

        return this.milepaymentDate;

    }

    public void setMilepaymentDt(Date milepaymentDate) {

        this.milepaymentDate = milepaymentDate;
    }

    @Transient
    public String getMilepaymentDate() {
        return DateUtil.dateToString(getMilepaymentDt());

    }

    public void setMilepaymentDate(String milepaymentDate) {

        DateUtil pDate = null;
        setMilepaymentDt(DateUtil.stringToDate(milepaymentDate, null));
    }

    @Column(name = "MILEPAYMENT_AMT")
    public String getMilepaymentAmount() {
        double db = milepaymentAmount.doubleValue();
        DecimalFormat df = new DecimalFormat("#######0.00");
        return df.format(db);

    }

    public void setMilepaymentAmount(String milepaymentAmount) {
        if (milepaymentAmount != null) {
            Rlog.debug("milepayment", "milepayment Amount Before"
                    + this.milepaymentAmount);
            this.milepaymentAmount = new Double(milepaymentAmount);
            Rlog.debug("milepayment", "milepayment Amount After"
                    + this.milepaymentAmount);
        }

    }

    @Column(name = "MILEPAYMENT_DELFLAG")
    public String getMilepaymentDelFlag() {
        return this.milepaymentDelFlag;
    }

    public void setMilepaymentDelFlag(String milepaymentDelFlag) {
        this.milepaymentDelFlag = milepaymentDelFlag;
    }

    @Column(name = "MILEPAYMENT_DESC")
    public String getMilepaymentDesc() {
        return this.milepaymentDesc;
    }

    public void setMilepaymentDesc(String milepaymentDesc) {
        this.milepaymentDesc = milepaymentDesc;
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    // END OF GETTER SETTER METHODS


  

    /**
     * Updates the milepayment in the Database
     * 
     * @param msk
     *            State Kepper of milepayment
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    public int updateMilepayment(MilepaymentBean msk) {

        try {
            setMilepaymentStudyId(msk.getMilepaymentStudyId());
            setMilepaymentMileId(msk.getMilepaymentMileId());
            setMilepaymentDate(msk.getMilepaymentDate());
            setMilepaymentAmount(msk.getMilepaymentAmount());
            setMilepaymentDesc(msk.getMilepaymentDesc());
            setMilepaymentDelFlag(msk.getMilepaymentDelFlag());
            setCreator(msk.getCreator());
            setModifiedBy(msk.getModifiedBy());
            setIpAdd(msk.getIpAdd());
            
            setMilepaymentComments(msk.getMilepaymentComments());
            setMilepaymentType(msk.getMilepaymentType());
            
            Rlog.debug("milepayment", "UPDATED");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("milepayment",
                    "EXCEPTION IN STORING milepayment IN DATABASE" + e);
            return -2;
        }
    }

    public MilepaymentBean() {

    }

    public MilepaymentBean(int id, String milepaymentStudyId,
            String milepaymentMileId, String milepaymentDate,
            String milepaymentAmount, String milepaymentDesc,
            String milepaymentDelFlag, String creator, String modifiedBy,
            String ipAdd,String milepaymentComments,String milepaymentType) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setMilepaymentStudyId(milepaymentStudyId);
        setMilepaymentMileId(milepaymentMileId);
        setMilepaymentDate(milepaymentDate);
        setMilepaymentAmount(milepaymentAmount);
        setMilepaymentDesc(milepaymentDesc);
        setMilepaymentDelFlag(milepaymentDelFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setMilepaymentComments(milepaymentComments);
        setMilepaymentType(milepaymentType);
    }

    @Column(name = "milepayment_comments")
	public String getMilepaymentComments() {
		return milepaymentComments;
	}

	public void setMilepaymentComments(String milepaymentComments) {
		this.milepaymentComments = milepaymentComments;
	}

    @Column(name = "milepayment_type")
	public String getMilepaymentType() {
		return milepaymentType;
	}

	public void setMilepaymentType(String milepaymentType) {
		this.milepaymentType = milepaymentType;
	}

}// end of class

