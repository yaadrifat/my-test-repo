
/*
 * Classname			StorageDao.class
 *
 * Version information 	1.0
 *
 * Date					07/17/2007
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Manimaran
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


/**
 * StorageDao for getting storage records
 *
 * @author Manimaran
 * @version : 1.0 07/17/2007
 */


public class StorageDao extends CommonDAO implements java.io.Serializable {
	private ArrayList pkStorages;
    private ArrayList fkStorages;
    private ArrayList storageIds;
    private ArrayList storageNames;
    private ArrayList fkCodelstStorageTypes;
    private ArrayList storageCapNums;
    private ArrayList storageCoordinateXs;
    private ArrayList storageCoordinateYs;
    private ArrayList storageCapUnits;
    private ArrayList storageStatus;
    private ArrayList storageDim1;
    private ArrayList storageDim2;
    private ArrayList specAllocationCount;
    private ArrayList storageParent;
    private ArrayList childAllocationCount;
    private ArrayList storageLevel;

    private ArrayList storageChain; // This contains only a few; not the same dimension as all other ArrayList's
    private HashMap specimenPks; // key=PK of storage; value=PK of specimen
    private HashMap specimenIds; // key=PK of storage; value=Specimen ID
    private HashMap specimenPatientFks; // key=PK of storage; value=FK_PER
    private String topStorageNameForExplorer;

    private ArrayList specimens;        // Used by getSpecimensAndPatients only
    private ArrayList specimenPatients; // Used by getSpecimensAndPatients only

    public StorageDao() {
    	pkStorages = new ArrayList();
        fkStorages = new ArrayList();
    	storageIds = new ArrayList();
    	storageNames = new ArrayList();
    	fkCodelstStorageTypes = new ArrayList();
    	storageCapNums = new ArrayList();
    	storageCoordinateXs = new ArrayList();
    	storageCoordinateYs = new ArrayList();
    	storageCapUnits = new ArrayList();
    	storageStatus = new ArrayList();

    	storageDim1 = new ArrayList();
    	storageDim2 = new ArrayList();
    	specAllocationCount = new ArrayList();
    	storageParent = new ArrayList();
    	childAllocationCount = new ArrayList();
    	storageLevel = new ArrayList();
    	storageChain = new ArrayList();
    	specimenPks = new HashMap();
    	specimenIds = new HashMap();
    	specimenPatientFks = new HashMap();

    	specimens = new ArrayList();
    	specimenPatients = new ArrayList();
   }

//  Getter and Setter methods

    public ArrayList getPkStorages() {
        return this.pkStorages;
    }

    public void setPkStorages(ArrayList pkStorages) {
        this.pkStorages = pkStorages;
    }

    public void setPkStorages(Integer pkStorage) {
        this.pkStorages.add(pkStorage);
    }

    public ArrayList getFkStorages() {
        return this.fkStorages;
    }

    public void setFkStorages(Integer fkStorage) {
        this.fkStorages.add(fkStorage);
    }

    public ArrayList getStorageIds() {
        return this.storageIds;
    }

    public void setStorageIds(ArrayList storageIds) {
        this.storageIds = storageIds;
    }

    public void setStorageIds(String storageId) {
        this.storageIds.add(storageId);
    }

    public ArrayList getStorageNames() {
        return this.storageNames;
    }

    public void setStorageNames(ArrayList storageNames) {
        this.storageNames = storageNames;
    }

    public void setStorageNames(String storageName) {
        this.storageNames.add(storageName);
    }

    public ArrayList getFkCodelstStorageTypes() {
        return this.fkCodelstStorageTypes;
    }

    public void setFkCodelstStorageTypes(ArrayList fkCodelstStorageTypes) {
        this.fkCodelstStorageTypes = fkCodelstStorageTypes;
    }

    public void setFkCodelstStorageTypes(String fkCodelstStorageType) {
        this.fkCodelstStorageTypes.add(fkCodelstStorageType);
    }

    public ArrayList getStorageCapNums() {
        return this.storageCapNums;
    }

    public void setStorageCapNums(ArrayList storageCapNums) {
        this.storageCapNums = storageCapNums;
    }

    public void setStorageCapNums(String storageCapNum) {
        this.storageCapNums.add(storageCapNum);
    }



    public ArrayList getStorageCoordinateXs() {
        return this.storageCoordinateXs;
    }

    public void setStorageCoordinateXs(ArrayList storageCoordinateXs) {
        this.storageCoordinateXs = storageCoordinateXs;
    }

    public void setStorageCoordinateXs(String storageCoordinateX) {
        this.storageCoordinateXs.add(storageCoordinateX);
    }

    public ArrayList getStorageCoordinateYs() {
        return this.storageCoordinateYs;
    }

    public void setStorageCoordinateYs(ArrayList storageCoordinateYs) {
        this.storageCoordinateYs = storageCoordinateYs;
    }

    public void setStorageCoordinateYs(String storageCoordinateY) {
        this.storageCoordinateYs.add(storageCoordinateY);
    }

    public ArrayList getStorageCapUnits() {
        return this.storageCapUnits;
    }

    public void setStorageCapUnits(ArrayList storageCapUnits) {
        this.storageCapUnits = storageCapUnits;
    }

    public void setStorageCapUnits(String storageCapUnit) {
        this.storageCapUnits.add(storageCapUnit);
    }


    public ArrayList getStorageStatus() {
        return this.storageStatus;
    }

    public void setStorageStatus(ArrayList storageStatus) {
        this.storageStatus = storageStatus;
    }

    public void setStorageStatus(String storageStat) {
        this.storageStatus.add(storageStat);
    }




    //Query modified by Manimaran to fix the issue 3153.
    public void getStorageValue(int pkStorage) {

    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
            .prepareStatement("select PK_STORAGE,STORAGE_ID,STORAGE_NAME,FK_CODELST_STORAGE_TYPE,STORAGE_CAP_NUMBER, "
            			  			+"STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,FK_CODELST_CAPACITY_UNITS, "
            			  			+"(select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and "
            			  			+"pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and "
            			  			+"ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE))) as STORAGE_STATUS "
            			  			+"from ER_STORAGE where FK_STORAGE = ? order by STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y");//KM
            pstmt.setInt(1, pkStorage);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
            	setStorageIds(rs.getString("STORAGE_ID"));
            	setStorageNames(rs.getString("STORAGE_NAME"));
            	setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));
            	setStorageCapNums(rs.getString("STORAGE_CAP_NUMBER"));
            	setStorageCoordinateXs(rs.getString("STORAGE_COORDINATE_X"));
            	setStorageCoordinateYs(rs.getString("STORAGE_COORDINATE_Y"));
            	setStorageCapUnits(rs.getString("FK_CODELST_CAPACITY_UNITS"));//KM-09072007
            	setStorageStatus(rs.getString("STORAGE_STATUS"));

            }

       } catch (SQLException ex) {
            Rlog.fatal("storage",
                    "StorageDao.getStorageValues EXCEPTION IN FETCHING FROM storage table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


    public int deleteStorages(String[] deleteIds, int flag, int usr) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);
            //Modified by Manimaran to delete the parent, child and related records.
            cstmt = conn
                    .prepareCall("{call PKG_STORAGE.SP_DELETE_STORAGES_PARENT_CHLD(?,?,?,?)}");//KM
            Rlog.debug("storage",
                    "StorageDao:deleteStorages-after prepare call of sp_delete_storages_parent_child");
            cstmt.setArray(1, deleteIdsArray);
            Rlog.debug("storage",
                    "StorageDao:deleteStorages-after prepare call of sp_delete_storages_parent_child"
                            + deleteIdsArray);
            cstmt.setInt(2,flag);
            cstmt.setInt(3,usr);
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            Rlog.debug("storage",
                    "StorageDao:deleteStorages-after execute of sp_delete_storages_parent_child");
            ret = cstmt.getInt(4);
            Rlog.debug("storage", "StorageDao:deleteStorages-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("storage",
                    "EXCEPTION in StorageDao:deleteStorages, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }


    public int getCount(String storageName) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + "from er_storage where upper(storage_name) = upper(?)");

            pstmt.setString(1, storageName);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("storage",
                    "StorageDao.getCount EXCEPTION IN FETCHING FROM Storage table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return count;
    }

    //KM-Account Id added for new requirement change.
    public int getCountId(String storageId, String accId) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + "from er_storage where upper(storage_id) = upper(?) and fk_account = ? ");

            pstmt.setString(1, storageId);
            pstmt.setString(2, accId);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("storage",
                    "StorageDao.getCountId EXCEPTION IN FETCHING FROM Storage table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return count;
    }



    public String getStorageIdAuto() {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String storageNumber=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select seq_er_storage_id.nextval STORAGEID from dual");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	storageNumber =  rs.getString("STORAGEID");

            }
            return storageNumber;

        } catch (Exception ex) {
            Rlog.fatal("storage",
                    "StorageDao.getStorageId EXCEPTION IN FETCHING FROM seq_er_storage_id.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return storageNumber;

    }

    public String getStorageIdAuto(String fkCodelstStorageType, int fkAccount) {
        String autoStorageId = StringUtil.trueValue(getStorageIdAuto());
        SettingsDao settingsDao = new SettingsDao();
        settingsDao.retrieveSettings(fkAccount, 1, "STORAGEID_FORMAT");
        ArrayList settingArray = settingsDao.getSettingValue();
        if (settingArray == null || settingArray.size() < 1) {
            return autoStorageId;
        }
        String format = (String)settingArray.get(0);
        if (format == null || format.length() == 0) {
            return autoStorageId;
        }
        CodeDao codeDao = new CodeDao();
        String storageType = null;
        if (codeDao.getCodeValuesById(StringUtil.stringToNum(fkCodelstStorageType))) {
            ArrayList cdescArray = codeDao.getCDesc();
            if (cdescArray != null && cdescArray.size() > 0) {
                storageType = (String)cdescArray.get(0);
            }
        }
        if (storageType == null) { storageType = ""; }
        return format.replaceAll("\\[VELSTORAGEID\\]", autoStorageId)
            .replaceAll("\\[VELSTORAGETYPE\\]", storageType);
    }

    public String getParentStorage(int pkStorage) {

    	PreparedStatement pstmt = null;
    	String parentName = "";
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
            .prepareStatement("select b.STORAGE_NAME from er_storage b , er_storage a where a.pk_storage = ? and b.pk_storage = a.fk_storage ");

            pstmt.setInt(1, pkStorage);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	parentName = rs.getString("STORAGE_NAME");

            }
            return parentName;

       } catch (SQLException ex) {
            Rlog.fatal("storage",
                    "StorageDao.getParentStorage EXCEPTION IN FETCHING FROM storage table"
                            + ex);
            return parentName;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }



    //JM: 28Dec2007

   public String findAllChildStorageUnitIds(String storageId){

       String retval = "";
       CallableStatement cstmt = null;

       Connection conn = null;
       String err = "";


       try {



           conn = getConnection();
           cstmt = conn.prepareCall("{call PKG_STORAGE.sp_find_all_child_storage_ids(?,?)}");

           cstmt.setString(1, storageId);
           cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
           cstmt.execute();
           err = cstmt.getString(2);

           return err;

       } catch (SQLException ex) {
           Rlog.fatal("storage","EXCEPTION IN calling the procedure sp_find_all_child_storage_ids()" + ex);
           return retval;
       } finally {
           try {
               if (cstmt != null)
                   cstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }

       }
   }

// JM: 22Jan2008

   public String findParentStorageType(String storageId){


        PreparedStatement pstmt = null;
        Connection conn = null;
        String retString=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(" SELECT codelst_subtyp PARENT_STOR_TYPE FROM er_codelst WHERE pk_codelst = (select FK_CODELST_STORAGE_TYPE from er_storage where pk_storage = (select fk_storage from er_storage where pk_storage=?))");
            pstmt.setString(1, storageId);


            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	retString =  rs.getString("PARENT_STOR_TYPE");

            }
            return retString;

        } catch (Exception ex) {
            Rlog.fatal("storage",
                    "StorageDao.findParentStorageType EXCEPTION IN fetching data from er_storage table.." + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return retString;

	    }

   //Added by Manimaran to copy StorageUnit and Templates.
   public int copyStorageUnitsAndTemplates(String[] strgPks,String[] copyCounts,String[] strgIds,String[] strgNames,String[] templates,String usr,String ipAdd) {
       int ret = 1;
       CallableStatement cstmt = null;
       Connection conn = null;
       try {
           conn = getConnection();

           ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                   "ARRAY_STRING", conn);
           ARRAY strgPksArray = new ARRAY(inIds, conn, strgPks);
           ARRAY copyCountsArray = new ARRAY(inIds, conn, copyCounts);
           ARRAY strgIdsArray = new ARRAY(inIds, conn, strgIds);
           ARRAY strgNamesArray = new ARRAY(inIds, conn, strgNames);
           ARRAY templatesArray = new ARRAY(inIds, conn, templates);


           cstmt = conn
                   .prepareCall("{call PKG_STORAGE.SP_COPY_STORAGES(?,?,?,?,?,?,?,?)}");
           Rlog.debug("storage",
                   "StorageDao:copyStorages-after prepare call of sp_copy_storages");
           cstmt.setArray(1, strgPksArray);
           cstmt.setArray(2, copyCountsArray);
           cstmt.setArray(3, strgIdsArray);
           cstmt.setArray(4, strgNamesArray);
           cstmt.setArray(5, templatesArray);
           cstmt.setString(6, usr);
           cstmt.setString(7, ipAdd);
           cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
           cstmt.execute();
           ret = cstmt.getInt(8);
           Rlog.debug("storage", "StorageDao:copyStorages-after execute of RETURN VALUE"
                   + ret);

           return ret;

       } catch (Exception e) {
           Rlog.fatal("storage",
                   "EXCEPTION in StorageDao:copyStorages, excecuting Stored Procedure "
                           + e);
           return -1;
       } finally {
           try {
               if (cstmt != null)
                   cstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }
       }

   }


   /** Storage Search method. arguments are passed in Hastable args
    *  */
   public void searchStorage0(Hashtable args,int account) {

   	PreparedStatement pstmt = null;
       Connection conn = null;
       StringBuffer sqlbuf = new StringBuffer();

       sqlbuf.append("select PK_STORAGE,STORAGE_ID,STORAGE_NAME, FK_CODELST_STORAGE_TYPE,STORAGE_CAP_NUMBER, STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,FK_CODELST_CAPACITY_UNITS,nvl(storage_dim1_cellnum,0) storage_dim1_cellnum,nvl(storage_dim2_cellnum,0) storage_dim2_cellnum ");
       sqlbuf.append(" , nvl((select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and ");
       sqlbuf.append(" pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and ");
       sqlbuf.append(" ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE))),0) as STORAGE_STATUS, ");
       sqlbuf.append(" (select count(*) from er_specimen where fk_storage=pk_storage ) specimen_allocation_count, (select storage_name from er_storage p where p.pk_storage=o.fk_storage) parent_name, ");
       sqlbuf.append(" nvl((select lower(storage_name) from er_storage p where p.pk_storage=o.fk_storage),'-') lower_parent_name, lower(storage_name) lower_storage_name, (select count(*) from er_storage c where c.fk_storage=o.pk_storage ) child_allocation_count  from ER_STORAGE o where fk_account = ?");

       if (args.containsKey("ignoreTemplate"))
       {
    	   sqlbuf.append(" and  nvl(storage_istemplate,0) = 0 and o.fk_storage is null  " );

       }


       if (args.containsKey("name"))
       {
    	   sqlbuf.append(" and trim(lower(STORAGE_NAME)) like '%"+((String) args.get("name")).trim().toLowerCase()+"%' ");

       }

       if (args.containsKey("id"))
       {
    	   sqlbuf.append(" and trim(lower(STORAGE_ID)) like '%"+((String) args.get("id")).trim().toLowerCase()+"%' ");

       }

       if (args.containsKey("parent"))
       {
    	   sqlbuf.append(" and fk_storage = ? ");

       }

       if (args.containsKey("orderby"))
       {
    	   sqlbuf.append(" order by " + (String) args.get("orderby"));

    	   if (args.containsKey("order"))
           {
        	   sqlbuf.append( (String) args.get("order"));
           }
       }
       else{
           // IH for 3906 - Fixed col vs. row problem in Grid; swap X and Y
    	   sqlbuf.append(" order by fk_storage,STORAGE_COORDINATE_Y,STORAGE_COORDINATE_X");

       }



       try {
           conn = getConnection();
           pstmt = conn.prepareStatement(sqlbuf.toString());

           pstmt.setInt(1, account);

           if (args.containsKey("parent"))
           {
        	   pstmt.setString(2, (String)args.get("parent"));
           }

           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
        	   setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
           	setStorageIds(rs.getString("STORAGE_ID"));
           	setStorageNames(rs.getString("STORAGE_NAME"));
           	setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));
           	setStorageCapNums(rs.getString("STORAGE_CAP_NUMBER"));
           	setStorageCoordinateXs(rs.getString("STORAGE_COORDINATE_X"));
           	setStorageCoordinateYs(rs.getString("STORAGE_COORDINATE_Y"));
           	setStorageCapUnits(rs.getString("FK_CODELST_CAPACITY_UNITS"));//KM-09072007
           	setStorageStatus(rs.getString("STORAGE_STATUS"));

           	setStorageDim1(rs.getString("storage_dim1_cellnum"));
           	setStorageDim2(rs.getString("storage_dim2_cellnum"));
           	setSpecAllocationCount(rs.getString("specimen_allocation_count"));
           	setStorageParent(rs.getString("parent_name"));
           	setChildAllocationCount(rs.getString("child_allocation_count"));

           }

      } catch (SQLException ex) {
           Rlog.fatal("storage",
                   "StorageDao.searchStorage EXCEPTION IN FETCHING FROM storage table"
                           + ex + "sql: "+sqlbuf.toString());
           ex.printStackTrace();


       } finally {
           try {
               if (pstmt != null)
                   pstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }

       }

   }

   private StringBuffer prepareSQLForGridView (Hashtable args, String pkDown) {
       StringBuffer sqlbuf = new StringBuffer();
       sqlbuf.append(" select x.* from (");
       sqlbuf.append(" select v.* from (select (level+?) lvl, fk_storage, ");
       appendCorePartOfSQL(sqlbuf);
       sqlbuf.append(" start with ");
       sqlbuf.append(" fk_account = ? and fk_codelst_storage_type <> (select pk_codelst from er_Codelst where CODELST_TYPE = 'store_type' and  CODELST_SUBTYP = 'Kit')");
       if (args.containsKey("ignoreTemplate")) {
           sqlbuf.append(" and  nvl(storage_istemplate,0) = 0 " );
       }
       sqlbuf.append(" and o.fk_storage = ? ");
       sqlbuf.append(" connect by prior pk_storage = o.fk_storage ");
       sqlbuf.append(" order by o.fk_storage,storage_coordinate_y,storage_coordinate_x");
       sqlbuf.append(") v ");
       sqlbuf.append(" where fk_storage_main in ").append(pkDown);
       sqlbuf.append(") x order by x.fk_storage_main, x.storage_coordinate_y, x.storage_coordinate_x");
       return sqlbuf;
   }

   private StringBuffer prepareSQLForBrowserView (Hashtable args) {
       StringBuffer sqlbuf = new StringBuffer();
       sqlbuf.append("select");
       appendCorePartOfSQL(sqlbuf);
       sqlbuf.append(" where ");
       sqlbuf.append(" fk_account = ? and fk_codelst_storage_type <> (select pk_codelst from er_Codelst where CODELST_TYPE = 'store_type' and  CODELST_SUBTYP = 'Kit')");
       if (args.containsKey("ignoreTemplate")) {
           sqlbuf.append(" and  nvl(storage_istemplate,0) = 0 and o.fk_storage is null  " );
       }
       if (args.containsKey("name")) {
           sqlbuf.append(" and trim(lower(STORAGE_NAME)) like '%"+((String) args.get("name")).trim().toLowerCase()+"%' ");
       }
       if (args.containsKey("id")) {
           sqlbuf.append(" and trim(lower(STORAGE_ID)) like '%"+((String) args.get("id")).trim().toLowerCase()+"%' ");
       }
       if (args.containsKey("storageType")) {
           int stInt = 0;
           try { stInt = Integer.parseInt((String)args.get("storageType")); }
           catch(Exception e) { stInt = 0; }
           if ( stInt > 0) {
               sqlbuf.append(" and fk_codelst_storage_type = ").append((String)args.get("storageType")).append(" ");
           }
       }
       sqlbuf.append(" and fk_storage is null ");
       if (args.containsKey("orderby")) {
           sqlbuf.append(" order by " + (String) args.get("orderby"));
           if (args.containsKey("order")) {
               sqlbuf.append( (String) args.get("order"));
           }
       }
       return sqlbuf;
   }

   private String prepareSQLForSpecimens() {
       StringBuffer sqlbuf = new StringBuffer();
       sqlbuf.append("select PK_SPECIMEN, SPEC_ID, FK_STORAGE, FK_PER from ER_SPECIMEN where ");
       sqlbuf.append(" FK_STORAGE in (");
       for (int iX=0; iX<pkStorages.size(); iX++) {
           sqlbuf.append(pkStorages.get(iX)).append(",");
       }
       sqlbuf.append("0)");
       return sqlbuf.toString();
   }

   private void appendCorePartOfSQL(StringBuffer sqlbuf) {
       sqlbuf.append(" PK_STORAGE,STORAGE_ID,STORAGE_NAME, FK_CODELST_STORAGE_TYPE, FK_STORAGE fk_storage_main, STORAGE_CAP_NUMBER, STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,FK_CODELST_CAPACITY_UNITS,nvl(storage_dim1_cellnum,0) storage_dim1_cellnum,nvl(storage_dim2_cellnum,0) storage_dim2_cellnum ");
       sqlbuf.append(" , nvl((select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and ");
       sqlbuf.append(" pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and ");
       sqlbuf.append(" ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE))),0) as STORAGE_STATUS, ");
       sqlbuf.append(" (select count(*) from er_specimen where fk_storage=pk_storage ) specimen_allocation_count, (select storage_name from er_storage p where p.pk_storage=o.fk_storage) parent_name, ");
       sqlbuf.append(" nvl((select lower(storage_name) from er_storage p where p.pk_storage=o.fk_storage),'-') lower_parent_name, lower(storage_name) lower_storage_name, (select count(*) from er_storage c where c.fk_storage=o.pk_storage ) child_allocation_count  from ER_STORAGE o ");
   }

   private ArrayList extractChains(String pkChain) {
       if (pkChain == null) { return storageChain; }
       String[] array1 = pkChain.replaceAll("\\(", "").replaceAll("\\)", "").split(",");
       if (array1.length == 1) {
           storageChain.add(Integer.parseInt(array1[0]));
           return storageChain;
       }
       for (int iX=0; iX<array1.length-1; iX++) {
           storageChain.add(Integer.parseInt(array1[iX]));
       }
       return storageChain;
   }

   private String formChains(ArrayList ancestors) {
       if (ancestors == null) { return "(0)"; }
       StringBuffer retSB = new StringBuffer("(");
       for (int iX=0; iX<ancestors.size(); iX++) {
           if ("0".equals(ancestors.get(iX))) {
               retSB.append(",");
               break;
           }
           retSB.append(ancestors.get(iX)).append(",");
       }
       retSB.deleteCharAt( retSB.length()-1 );
       retSB.append(")");
       return retSB.toString();
   }

   public void searchStorage(Hashtable args,int account) {
       if (args.containsKey("searchUpward")) {
           searchStorageUpward(args, account);
           return;
       }
       PreparedStatement pstmt = null;
       CallableStatement cstmt = null;
       Connection conn = getConnection();
       int aLevel = 0;
       String pkDown = "('0')";
       if (args.containsKey("gridView")) {
           try {
               cstmt = conn.prepareCall(
                       "{call pkg_storage.sp_get_firstborn_descendants(?,?,?)}");
               cstmt.setInt(1,Integer.parseInt((String)args.get("parent")));
               cstmt.registerOutParameter(2,java.sql.Types.INTEGER);
               cstmt.registerOutParameter(3,java.sql.Types.VARCHAR);
               cstmt.execute();
               aLevel = cstmt.getInt(2);
               if (aLevel>0) { --aLevel; }
               pkDown = cstmt.getString(3);
               pkDown = formChains(extractChains(pkDown));
           } catch (SQLException ex) {
               Rlog.fatal("storage",
                       "StorageDao.searchStorage EXCEPTION IN FETCHING FROM storage table"
                       + ex + "sql: pkg_storage.sp_num_firstborn_descendants");
               ex.printStackTrace();
               try {
                   if (cstmt != null)
                       cstmt.close();
               } catch (Exception e) {}
               try {
                   if (conn != null)
                       conn.close();
               } catch (Exception e) {}
           } finally {
               try {
                   if (cstmt != null)
                       cstmt.close();
               } catch (Exception e) {}
           }
       }

       StringBuffer sqlbuf = null;
       if (args.containsKey("gridView")) {
           sqlbuf = prepareSQLForGridView(args, pkDown);
       } else {
           sqlbuf = prepareSQLForBrowserView (args);
       }

       try {
           if (conn == null || conn.isClosed()) {
               conn = getConnection();
           }
           pstmt = conn.prepareStatement(sqlbuf.toString());
           //System.out.println(sqlbuf.toString());
           if (args.containsKey("gridView"))
           {
               pstmt.setInt(1, aLevel);
               pstmt.setInt(2, account);
               pstmt.setString(3, (String)args.get("parent"));

           } else {
               pstmt.setInt(1, account);
           }

           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
               setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
               setStorageIds(rs.getString("STORAGE_ID"));
               setStorageNames(rs.getString("STORAGE_NAME"));
               setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));
               setFkStorages(new Integer(rs.getInt("fk_storage_main")));
               setStorageCapNums(rs.getString("STORAGE_CAP_NUMBER"));
               setStorageCoordinateXs(rs.getString("STORAGE_COORDINATE_X"));
               setStorageCoordinateYs(rs.getString("STORAGE_COORDINATE_Y"));
               setStorageCapUnits(rs.getString("FK_CODELST_CAPACITY_UNITS"));//KM-09072007
               setStorageStatus(rs.getString("STORAGE_STATUS"));
               setStorageDim1(rs.getString("storage_dim1_cellnum"));
               setStorageDim2(rs.getString("storage_dim2_cellnum"));
               setSpecAllocationCount(rs.getString("specimen_allocation_count"));
               setStorageParent(rs.getString("parent_name"));
               setChildAllocationCount(rs.getString("child_allocation_count"));
               if (args.containsKey("gridView")) {
                   setStorageLevel(rs.getString("lvl"));
               }
           }

           pstmt = conn.prepareStatement(prepareSQLForSpecimens());
           rs = pstmt.executeQuery();
           while (rs.next()) {
               setSpecimenPks(rs.getInt("FK_STORAGE"), rs.getString("PK_SPECIMEN"));
               setSpecimenIds(rs.getInt("FK_STORAGE"), rs.getString("SPEC_ID"));
               setSpecimenPatientFks(rs.getInt("FK_STORAGE"), rs.getString("FK_PER"));
           }


       } catch (SQLException ex) {
           Rlog.fatal("storage",
                   "StorageDao.searchStorage EXCEPTION IN FETCHING FROM storage table"
                   + ex + "sql: "+sqlbuf.toString());
           ex.printStackTrace();
       } finally {
           try {
               if (pstmt != null)
                   pstmt.close();
           } catch (Exception e) {}
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {}
       }

   }

   /**
    * This method searches storage chains upward.
    * @param args
    * @param account
    */
   private void searchStorageUpward(Hashtable args,int account) {
       PreparedStatement pstmt = null;
       StringBuffer sqlbuf = new StringBuffer();
       sqlbuf.append("select pk_storage, level lvl from er_storage ");
       sqlbuf.append(" start with fk_account = ? and pk_storage = ? ");
       sqlbuf.append(" connect by prior fk_storage = pk_storage ");

       Connection conn = getConnection();
       ArrayList<Integer> pkStorageList = new ArrayList<Integer>();
       ArrayList<Integer> levelList = new ArrayList<Integer>();
       try {
           pstmt = conn.prepareStatement(sqlbuf.toString());
           pstmt.setInt(1, account);
           pstmt.setString(2, (String)args.get("child"));
           ResultSet rs = pstmt.executeQuery();
           while (rs.next()) {
               pkStorageList.add(rs.getInt("PK_STORAGE"));
               levelList.add(rs.getInt("lvl"));
           }
       } catch(Exception ex) {
           Rlog.fatal("storage","StorageDao.searchStorageUpward 1 "
                   +ex+ " sql: "+sqlbuf.toString());
           ex.printStackTrace();
       } finally{
           if (pstmt != null) {
               try { pstmt.close(); } catch(Exception e) {}
           }
       }
       String pkChain = formChains(pkStorageList);
       extractChains(pkChain);

       sqlbuf = null;
       sqlbuf = new StringBuffer();
       sqlbuf.append("select storage_name from er_storage where pk_storage = ? ");

       try {
           pstmt = conn.prepareStatement(sqlbuf.toString());
           pstmt.setInt(1, pkStorageList.get(pkStorageList.size()-1));
           ResultSet rs = pstmt.executeQuery();
           while (rs.next()) {
               topStorageNameForExplorer = rs.getString("STORAGE_NAME");
           }
       } catch(Exception ex) {
           Rlog.fatal("storage","StorageDao.searchStorageUpward 2 "
                   +ex+ " sql: "+sqlbuf.toString());
           ex.printStackTrace();
       } finally{
           if (pstmt != null) {
               try { pstmt.close(); } catch(Exception e) {}
           }
       }

       sqlbuf = null;
       sqlbuf = prepareSQLForGridView(args, pkChain);

       try {
           if (conn == null || conn.isClosed()) {
               conn = getConnection();
           }
           pstmt = conn.prepareStatement(sqlbuf.toString());
           pstmt.setInt(1, 0);
           pstmt.setInt(2, account);
           pstmt.setInt(3, pkStorageList.get(pkStorageList.size()-1));
           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
               setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
               setStorageIds(rs.getString("STORAGE_ID"));
               setStorageNames(rs.getString("STORAGE_NAME"));
               setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));
               setFkStorages(new Integer(rs.getInt("fk_storage_main")));
               setStorageCapNums(rs.getString("STORAGE_CAP_NUMBER"));
               setStorageCoordinateXs(rs.getString("STORAGE_COORDINATE_X"));
               setStorageCoordinateYs(rs.getString("STORAGE_COORDINATE_Y"));
               setStorageCapUnits(rs.getString("FK_CODELST_CAPACITY_UNITS"));//KM-09072007
               setStorageStatus(rs.getString("STORAGE_STATUS"));
               setStorageDim1(rs.getString("storage_dim1_cellnum"));
               setStorageDim2(rs.getString("storage_dim2_cellnum"));
               setSpecAllocationCount(rs.getString("specimen_allocation_count"));
               setStorageParent(rs.getString("parent_name"));
               setChildAllocationCount(rs.getString("child_allocation_count"));
               if (args.containsKey("gridView")) {
                   setStorageLevel(rs.getString("lvl"));
               }
           }

           pstmt = conn.prepareStatement(prepareSQLForSpecimens());
           rs = pstmt.executeQuery();
           while (rs.next()) {
               setSpecimenPks(rs.getInt("FK_STORAGE"), rs.getString("PK_SPECIMEN"));
               setSpecimenIds(rs.getInt("FK_STORAGE"), rs.getString("SPEC_ID"));
           }

       } catch(Exception ex) {
           Rlog.fatal("storage","StorageDao.searchStorageUpward 3 "
                   +ex+ " sql: "+sqlbuf.toString());
           ex.printStackTrace();
       } finally {
           if (pstmt != null) {
               try { pstmt.close(); } catch (Exception e) {}
           }
           if (conn != null) {
               try { conn.close(); } catch (Exception e) {}
           }
       }

   }


   /** Storage Search method. arguments are passed in Hastable args
    *  */
   public void getImmediateChildren(int storagePK) {

   	PreparedStatement pstmt = null;
       Connection conn = null;
       StringBuffer sqlbuf = new StringBuffer();

       sqlbuf.append("select PK_STORAGE,STORAGE_ID,STORAGE_NAME, FK_CODELST_STORAGE_TYPE from ER_STORAGE o where fk_storage = ?");

       try {
           conn = getConnection();
           pstmt = conn.prepareStatement(sqlbuf.toString());

           pstmt.setInt(1, storagePK);

           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
        	   setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
           		setStorageIds(rs.getString("STORAGE_ID"));
           		setStorageNames(rs.getString("STORAGE_NAME"));
           		setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));
                }

      } catch (SQLException ex) {
           Rlog.fatal("storage",
                   "StorageDao.getImmediateChildren EXCEPTION IN FETCHING FROM storage table"
                           + ex + "sql: "+sqlbuf.toString());
           ex.printStackTrace();


       } finally {
           try {
               if (pstmt != null)
                   pstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }

       }

   }

   /**
    * This method gets the list of pk_specimen and fk_per from the er_specimen table
    * which make reference to the storage unit with ID pkStorage.
    *
    * @param args
    * @param account
    */
   public int getSpecimensAndPatients(int pkStorage, int account) {
       int result = 0;
       this.specimens.clear();
       this.specimenPatients.clear();
       PreparedStatement pstmt = null;
       Connection conn = null;
       StringBuffer sqlbuf = new StringBuffer();
       sqlbuf.append("select pk_specimen, fk_per from er_specimen where fk_storage = ? and fk_account = ? ");
       try {
           conn = getConnection();
           pstmt = conn.prepareStatement(sqlbuf.toString());
           pstmt.setInt(1, pkStorage);
           pstmt.setInt(2, account);
           ResultSet rs = pstmt.executeQuery();
           while (rs.next()) {
               setSpecimen(rs.getInt("pk_specimen"));
               setSpecimenPatient(rs.getInt("fk_per"));
           }
       } catch (SQLException ex) {
           Rlog.fatal("storage",
                   "StorageDao.getSpecimensAndPatients: "+ex+"=> sql: "+sqlbuf.toString());
           ex.printStackTrace();
           result = -1;
       } finally {
           try {
               if (pstmt != null) { pstmt.close(); }
           } catch (Exception e) {}
           try {
               if (conn != null) { conn.close(); }
           } catch (Exception e) {}
       }
       return result;
   }

   public ArrayList getSpecimens() {
       return specimens;
   }

   private void setSpecimen(int fkSpecimen) {
       this.specimens.add(new Integer(fkSpecimen));
   }

   public ArrayList getSpecimenPatients() {
       return specimenPatients;
   }

   private void setSpecimenPatient(int fkPer) {
       this.specimenPatients.add(new Integer(fkPer));
   }

public ArrayList getStorageDim1() {
	return storageDim1;
}

public void setStorageDim1(ArrayList storageDim1) {
	this.storageDim1 = storageDim1;
}

public void setStorageDim1(String sDim1) {
	this.storageDim1.add(sDim1);
}

public ArrayList getStorageDim2() {
	return storageDim2;
}

public void setStorageDim2(ArrayList storageDim2) {
	this.storageDim2 = storageDim2;
}

public void setStorageDim2(String sDim2) {
	this.storageDim2.add(sDim2);
}

public ArrayList getSpecAllocationCount() {
	return specAllocationCount;
}

public void setSpecAllocationCount(ArrayList specAllocationCount) {
	this.specAllocationCount = specAllocationCount;
}

public void setSpecAllocationCount(String specAllocationCt) {
	this.specAllocationCount.add(specAllocationCt);
}

public ArrayList getStorageParent() {
	return storageParent;
}

public void setStorageParent(ArrayList storageParent) {
	this.storageParent = storageParent;
}

public void setStorageParent(String stParent) {
	this.storageParent.add(stParent);
}

public ArrayList getChildAllocationCount() {
	return childAllocationCount;
}

public void setChildAllocationCount(ArrayList childAllocationCount) {
	this.childAllocationCount = childAllocationCount;
}

public void setChildAllocationCount(String childAllocCount) {
	this.childAllocationCount.add(childAllocCount);
}

public ArrayList getStorageLevel() {
    return storageLevel;
}

public void setStorageLevel(ArrayList storageLevel) {
    this.storageLevel = storageLevel;
}

public void setStorageLevel(String storageLevel) {
    this.storageLevel.add(storageLevel);
}

public ArrayList getStorageChain() {
    return storageChain;
}

public void setStorageChain(ArrayList storageChain) {
    this.storageChain = storageChain;
}

public void setSpecimenPks(int key, String value) {
    if (value == null || value.length() == 0) { return; }
    String v1 = (String)specimenPks.get(key);
    if (v1 != null && v1.length()>0) { v1 = v1 + "," + value; }
    else { v1 = value; }
    specimenPks.put(key, v1);
}

public String getSpecimenPks(int key) {
    Object o = specimenPks.get(key);
    return o == null ? null : ((String)o);
}

public void setSpecimenIds(int key, String value) {
    if (value == null || value.length() == 0) { return; }
    String v1 = (String)specimenIds.get(key);
    if (v1 != null && v1.length()>0) { v1 = v1 + "," + value.replaceAll(",", "-"); }
    else { v1 = value; }
    specimenIds.put(key, v1);
}

public String getSpecimenIds(int key) {
    Object o = specimenIds.get(key);
    return o == null ? null : ((String)o);
}

public String getSpecimenPatientFks(int key) {
    Object o = specimenPatientFks.get(key);
    return o == null ? null : ((String)o);
}

public void setSpecimenPatientFks(int key, String value) {
    if (value == null || value.length() == 0) { return; }
    String v1 = (String)specimenPatientFks.get(key);
    if (v1 != null && v1.length()>0) { v1 = v1 + "," + value; }
    else { v1 = value; }
    specimenPatientFks.put(key, v1);
}

public String getTopStorageNameForExplorer() {
    return topStorageNameForExplorer == null ? "" : topStorageNameForExplorer;
}

//JM: 21Aug2009
public void getAllChildren(int pkStorageKit) {

	PreparedStatement pstmt = null;
    Connection conn = null;
    try {
        conn = getConnection();
        pstmt = conn.prepareStatement("select pk_storage, storage_name, FK_CODELST_STORAGE_TYPE, fk_storage from er_storage  start with fk_storage =? connect by prior pk_storage = fk_storage order by pk_storage");
        pstmt.setInt(1, pkStorageKit);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
        	setPkStorages(new Integer(rs.getInt("PK_STORAGE")));
        	setStorageNames(rs.getString("STORAGE_NAME"));
        	setFkCodelstStorageTypes(rs.getString("FK_CODELST_STORAGE_TYPE"));

        	setFkStorages(new Integer(rs.getInt("fk_storage")));

        }

   } catch (SQLException ex) {
        Rlog.fatal("storage",
                "StorageDao.getAllChildren EXCEPTION IN FETCHING FROM storage table"
                        + ex);
    } finally {
        try {
            if (pstmt != null)
                pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }

    }

}



//end of class
}
