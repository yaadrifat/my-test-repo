/*
 * Classname			CtrltabBean.class
 * 
 * Version information
 *
 * Date					05/18/2006
 * 
 * Copyright notice
 */

package com.velos.eres.business.ctrltab.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Ctrltab CMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 */

@Entity
@Table(name = "er_ctrltab")
public class CtrltabBean implements Serializable {
    
    private static final long serialVersionUID = 3546356240990286140L;

    /**
     * the ctrltab Id
     */
    public int ctabId;

    /**
     * the ctrltab CtrlValue
     */
    
	public String ctabCtrlValue;

	/**
	 * the ctrltab  CtrlDesc
	 */
	
	public String ctabCtrlDesc;
	
	 /**
      * the ctrltab CtrlKey
      */	
	
	public String ctabCtrlKey;
	
	/**
	 * the ctrltab CtrlSeq
	 */
	
	public Integer ctabCtrlSeq;
	
		
	/**
     * the record creator
     */
    
	public Integer creator;

    /**
     * last modified by
     */
    
	public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;


    // GETTER SETTER METHODS
  
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_CTRLTAB", allocationSize=1)
    
    @Column(name = "PK_CTRLTAB")
    public int getCtabId() {
        return this.ctabId;
    }

    public void setCtabId(int ctabId) {
        this.ctabId = ctabId;
    }

    @Column(name = "CTRL_VALUE")
    public String getCtabCtrlValue() {
        return this.ctabCtrlValue;
    }

    public void setCtabCtrlValue(String ctabCtrlValue) {
        
            this.ctabCtrlValue = ctabCtrlValue;
    }
    

    @Column(name = "CTRL_DESC")
    public String getCtabCtrlDesc() {
        return this.ctabCtrlDesc;
    }

    
    public void setCtabCtrlDesc(String ctabCtrlDesc) {
        this.ctabCtrlDesc = ctabCtrlDesc;
    }

    @Column(name = "CTRL_KEY")
    public String getCtabCtrlKey() {
        return this.ctabCtrlKey;
    }

    public void setCtabCtrlKey(String ctabCtrlKey) {
        this.ctabCtrlKey = ctabCtrlKey;
    }

   
    @Column (name="CTRL_SEQ")
    public String getCtabCtrlSeq() {
    	return StringUtil.integerToString(this.ctabCtrlSeq);
    }
    
    
    public void setCtabCtrlSeq(String ctabCtrlSeq) {
    	 if (ctabCtrlSeq != null) {
    	this.ctabCtrlSeq =Integer.valueOf(ctabCtrlSeq);
    	}
    }
    
    /**
     * @return The Id of the creator of the record
     */
    
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
   
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    
    public CtrltabBean() {

    }

    public CtrltabBean(int ctabId, String ctabCtrlValue, String ctabCtrlDesc, String ctabCtrlKey,
    		
    		String ctabCtrlSeq, String creator , String modifiedBy , String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setCtabId(ctabId);
        setCtabCtrlValue(ctabCtrlValue);
        setCtabCtrlDesc(ctabCtrlDesc);
        setCtabCtrlKey(ctabCtrlKey);
        setCtabCtrlSeq(ctabCtrlSeq);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }
   
    public int updateCtrltab(CtrltabBean ctsk) {

        try {
            setCtabCtrlValue(ctsk.getCtabCtrlValue());
            setCtabCtrlDesc(ctsk.getCtabCtrlDesc());
            setCtabCtrlKey(ctsk.getCtabCtrlKey());
            setCtabCtrlSeq(ctsk.getCtabCtrlSeq());
            setCreator(ctsk.getCreator());
            setModifiedBy(ctsk.getModifiedBy());
            setIpAdd(ctsk.getIpAdd());
            Rlog.debug("ctrltab", "UserBean.updateUser");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("user", " error in CtrltabBean.updateCtrltab");
            return -2;
        }
    }

}// end of class
 
