/*
 * Classname			SiteBean
 * 
 * Version information	1.0
 *
 * Date					02/28/2001
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.site.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;//KM
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Site CMP entity bean.<br>
 * <br>
 * 
 * @author Dinesh
 * @version 1.0
 */
/* Modified by Sonia Abrol, added an attribute siteIdentifier , 03/17/05 */
@Entity
@Table(name = "er_site")
//Modified by Manimaran to fix the Bug2729
@NamedQueries( { 
	@NamedQuery(name = "findBySiteIdentifier", query = "SELECT OBJECT(site) FROM SiteBean site where fk_account = :fk_account  AND UPPER(trim(SITE_ID)) = UPPER(trim(:siteId))"),	
	@NamedQuery(name = "findBySiteName", query = "SELECT OBJECT(site) FROM SiteBean site where fk_account = :fk_account  AND UPPER(trim(SITE_NAME)) = UPPER(trim(:siteName))") 
})
//@NamedQuery(name = "findBySiteIdentifier", queryString = "SELECT OBJECT(site) FROM SiteBean site where fk_account = :fk_account  AND UPPER(trim(SITE_ID)) = UPPER(trim(:siteId))")

public class SiteBean implements Serializable {

    /**
     * 
     */

    /**
     * 
     */
    private static final long serialVersionUID = 3834024775256652086L;

    /**
     * the primary key of the Site bean
     */

    public Integer siteId;

    /**
     * the site Codelst
     */

    public Integer siteCodelstType;

    /**
     * the user accountId
     */

    public Integer siteAccountId;

    /**
     * the site address
     */

    public Integer sitePerAdd;

	/**
     * the site name
     */
    public String siteName;

	/**
     * the site Info
     */

    public String siteInfo;
   
    /* Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */
    
    /**
     *  the site Notes
     */
    
    public String siteNotes;

	/**
     * the site status
     */

    public String siteStatus;

    /**
     * the site parent
     */

    public Integer siteParent;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * the site AbstractIdentifier - Id to identify a site
     */

    public String siteIdentifier;

    /**
     * A number to identify the site's sequence within an account- for internal
     * use
     */
    public Integer siteSequence;
    
    public String siteHidden; //KM
    
    

    // setter getter methods over here

    public SiteBean(Integer siteId, String siteCodelstType,
            String siteAccountId, String sitePerAdd,  String siteName, 
            String siteInfo, String siteNotes, String siteStatus, String siteParent,
            String creator, String modifiedBy, String ipAdd,
            String siteIdentifier, String siteSequence, String siteHidden) {

        setSiteId(siteId);
        setSiteCodelstType(siteCodelstType);
        setSiteAccountId(siteAccountId);
        setSitePerAdd(sitePerAdd);
		setSiteName(siteName);
		setSiteInfo(siteInfo);
		
		setSiteNotes(siteNotes);
        setSiteStatus(siteStatus);
        setSiteParent(siteParent);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setSiteIdentifier(siteIdentifier);
        setSiteSequence(siteSequence);
        setSiteHidden(siteHidden);//KM
    }

    public SiteBean() {

    }

    /**
     * Returns the Site Id
     * 
     * @return Id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    // @SequenceGenerator(name="SEQ_GEN",
    // sequenceName="SEQ_ER_SITE",initialValue=1,allocationSize=1)
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SITE", allocationSize=1)
    @Column(name = "PK_SITE")
    public Integer getSiteId() {
        return this.siteId;
    }

    public void setSiteId(Integer site) {
        this.siteId = site;
    }

    /**
     * Returns the Site Code List Type Id from Site
     * 
     * @return Id
     */
    @Column(name = "fk_codelst_type")
    public String getSiteCodelstType() {
        return StringUtil.integerToString(this.siteCodelstType);
    }

    /**
     * Sets the Id of Site Code List Type in Site
     * 
     * @param siteCodelstType
     *            Id of Site Code List Type
     */
    public void setSiteCodelstType(String siteCodelstType) {
        if (siteCodelstType != null) {
            this.siteCodelstType = Integer.valueOf(siteCodelstType);
        }
    }

    /**
     * Returns the Account Id of the Site
     * 
     * @return Account Id
     */
    @Column(name = "fk_account")
    public String getSiteAccountId() {
        return StringUtil.integerToString(this.siteAccountId);
    }

    /**
     * Sets Account Id of the Site
     * 
     * @param siteAccountId
     *            Account Id of the Site
     */
    public void setSiteAccountId(String siteAccountId) {
        if (siteAccountId != null) {
            this.siteAccountId = Integer.valueOf(siteAccountId);
        }
    }

    /**
     * Returns the Address ID of the Site
     * 
     * @return Address ID
     */
    @Column(name = "fk_peradd")
    public String getSitePerAdd() {
        return StringUtil.integerToString(this.sitePerAdd);
    }

    /**
     * Sets Address ID of the Site
     * 
     * @param sitePerAdd
     *            Address Id
     */
    public void setSitePerAdd(String sitePerAdd) {
        if (sitePerAdd != null)
            this.sitePerAdd = Integer.valueOf(sitePerAdd);
    }

	
	/**
     * Returns the Site Name
     * 
     * @return Site Name
     */
    @Column(name = "site_name")
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * Sets the Site Name in Site
     * 
     * @param siteName
     *            Site Name
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

   	/**
     * Returns the information of the Site
     * 
     * @return information
     */
    @Column(name = "site_info")
    public String getSiteInfo() {
        return this.siteInfo;
    }

	/**
     * Sets the information of the Site
     * 
     * @param siteInfo
     *            Site Information
     */
    public void setSiteInfo(String siteInfo) {
        this.siteInfo = siteInfo;
    }
    
    /**
     * Returns the Notes of the site
     * 
     * @return notes
     */
    @Column(name = "site_notes")
    public String getSiteNotes(){
    	return this.siteNotes;
    }
    
    /**
     * Sets the Notes of the site
     * 
     * @param siteNotes
     *              Site Notes
     */
    public void setSiteNotes(String siteNotes){
    	
    	this.siteNotes = siteNotes;
    }

    /**
     * Returns the Status of the Site whether the site is active or not
     * 
     * @return 'A' or 'I' for the status of the site
     */
    @Column(name = "site_stat")
    public String getSiteStatus() {
        return this.siteStatus;
    }

    /**
     * Sets the Status of the Site depending on whether the site is active or
     * inactive
     * 
     * @param siteStatus
     *            'A' or 'I' is set
     */
    public void setSiteStatus(String siteStatus) {
        this.siteStatus = siteStatus;
    }

    /**
     * Returns the Parent site id in case of sub sites
     * 
     * @return Site id
     */
    @Column(name = "site_parent")
    public String getSiteParent() {
        return StringUtil.integerToString(this.siteParent);
    }

    /**
     * Sets the Parent Site Id in case of the sub sites
     * 
     * @param siteParent
     *            Parent Site Id
     */
    public void setSiteParent(String siteParent) {
        if (siteParent != null)
            this.siteParent = Integer.valueOf(siteParent);
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of siteIdentifier.
     */
    @Column(name = "site_id")
    public String getSiteIdentifier() {
        return siteIdentifier;
    }

    /**
     * Sets the value of siteIdentifier.
     * 
     * @param siteIdentifier
     *            The value to assign siteIdentifier.
     */
    public void setSiteIdentifier(String siteIdentifier) {
        this.siteIdentifier = siteIdentifier;
    }

    /**
     * Returns the value of siteSequence.
     */
    @Column(name = "site_seq")
    public String getSiteSequence() {
        return StringUtil.integerToString(siteSequence);
    }

    /**
     * Sets the value of siteSequence.
     * 
     * @param siteSequence
     *            The value to assign siteSequence.
     */
    public void setSiteSequence(String siteSequence) {
        this.siteSequence = StringUtil.stringToInteger(siteSequence);
    }
    
    
    //Added by Manimaran for Enh.#U11
    /**
     * Returns the Site Hidden
     * 
     * @return Site Hidden
     */
    @Column(name = "site_hidden")
    public String getSiteHidden() {
        return this.siteHidden;
    }

    /**
     * Sets the Site Hidden in Site
     * 
     * @param siteHidden
     *            Site Hidden
     */
    public void setSiteHidden(String siteHidden) {
    	
    	String siteH = "";
    	
    	if (StringUtil.isEmpty(siteHidden))
    	{
    		siteH = "0";
    	}
    	else
    	{
    		siteH = siteHidden;
    	}
    	
        this.siteHidden = siteH;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Update the Site State Keeper in Site and returs 0 if success else -2
     * 
     * @param ssk
     * @return 0 or -2 depending on the updation
     */
    public int updateSite(SiteBean ssk) {
        try {
            setSiteCodelstType(ssk.getSiteCodelstType());
            setSiteAccountId(ssk.getSiteAccountId());
            setSitePerAdd(ssk.getSitePerAdd());
			setSiteName(ssk.getSiteName());
			setSiteInfo(ssk.getSiteInfo());
			
			setSiteNotes(ssk.getSiteNotes());
            setSiteStatus(ssk.getSiteStatus());
            setSiteParent(ssk.getSiteParent());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setSiteIdentifier(ssk.getSiteIdentifier());
            setSiteSequence(ssk.getSiteSequence());
            setCreator(ssk.getCreator());
            setSiteHidden(ssk.getSiteHidden()); //KM
            Rlog.debug("site", "SiteBean.updateSite");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("site", " error in SiteBean.updateSite" + e);
            e.printStackTrace();
            return -2;
        }
    }

}// end of class
