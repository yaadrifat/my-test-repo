/*
 * Classname : PatCommonDao
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 06/08/2001
 *
 * Author: sonia sahni
 * Base class for all Patient Data Access  classes 
 */
package com.velos.eres.business.common;
/* Import statments */
import java.sql.Connection;
import java.sql.DriverManager;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.Rlog;
/* End of Import statments */
/**
 * 
 * 
 * The patient Common Dao.<br>
 * <br>
 * 
 * 
 * Base class for all Data Access classes
 * 
 * 
 * 
 * 
 * 
 * @author Sonia Sahni
 * 
 * 
 * @vesion 1.0 06/08/2001
 * 
 * 
 */
public class PatCommonDAO {
    /**
     * 
     * 
     * returns a connection object for patient data source
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    public Connection getPatConnection() {
        // ////////////
        try {
            Rlog.debug("common", Configuration.PATDBDriverName);
            if (Configuration.PATDBDriverName == null) {
                Configuration.readSettings();
            }
            Rlog.debug("common", "PATDB DRiver = "
                    + Configuration.PATDBDriverName);
            Rlog.debug("common", "PATDB URL = " + Configuration.PATDBUrl);
            Rlog.debug("common", "PATDB USER= " + Configuration.PATDBUser);
            Rlog.debug("common", "PATDB PWD = " + Configuration.PATDBPwd);
        } catch (Exception e) {
            Rlog.fatal("common", "IN PATCOMMON DAO" + e);
        }
        // ////////////
        Connection conn = null;
        try {
            Class.forName(Configuration.PATDBDriverName);
            conn = DriverManager.getConnection(Configuration.PATDBUrl,
                    Configuration.PATDBUser, Configuration.PATDBPwd);
            // Class.forName("oracle.jdbc.driver.OracleDriver");
            // conn
            // =DriverManager.getConnection("jdbc:oracle:thin:@192.8.8.44:1521:evelos","er","er");
            Rlog.debug("common", "Got PATConnection(): " + conn);
            return conn;
        } catch (Exception e) {
            Rlog.fatal("common", this.toString()
                    + ":getPATConnection(): exception:" + e);
            return null;
        }
    }
}