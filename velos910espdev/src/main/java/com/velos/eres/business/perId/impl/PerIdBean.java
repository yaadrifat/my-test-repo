/*
 * Classname : PerIdBean
 * 
 * Version information: 1.0
 *
 * Date: 16/02/04
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.perId.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The PerIdBean CMP entity bean.<br>
 * <br>
 * 
 * @author Anu Khana
 * @vesion 1.0 16/02/04
 * @ejbHome PerIdHome
 * @ejbRemote PerIdRObj
 */
@Entity
@Table(name = "PAT_PERID")
public class PerIdBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3906646393056409909L;

    /**
     * The primary key
     */

    public int id;

    /**
     * The per pk
     */
    public Integer perId;

    /**
     * The Id Type
     */

    public Integer perIdType;

    /**
     * The Alternate Per Id
     */
    public String alternatePerId;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    private ArrayList PerIdStateKeepers;

    private String recordType;

    // //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     * @return formSecId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "eres.SEQ_PAT_PERID", allocationSize=1)
    @Column(name = "PK_PERID")
    public int getId() {
        return id;
    }

    /**
     * 
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * 
     * @return formLibId
     */
    @Column(name = "FK_PER")
    public String getPerId() {
        return StringUtil.integerToString(perId);

    }

    /**
     * 
     * 
     * @param perId
     */
    public void setPerId(String perId) {
        if (!StringUtil.isEmpty(perId))
            this.perId = Integer.valueOf(perId);
    }

    
    public void setPerIdType(String perIdType) {
        if (!StringUtil.isEmpty(perIdType))
            this.perIdType = Integer.valueOf(perIdType);

    }

    @Column(name = "FK_CODELST_IDTYPE")
    public String getPerIdType() {
        return StringUtil.integerToString(perIdType);

    }

    
    public void setAlternatePerId(String alternatePerId) {
        this.alternatePerId = alternatePerId;
    }

    @Column(name = "PERID_ID")
    public String getAlternatePerId() {
        return alternatePerId;
    }

    /**
     * @return the Creator
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Transient
    public ArrayList getPerIdStateKeepers() {
        return PerIdStateKeepers;
    }

    public void setPerIdStateKeepers(ArrayList PerIdStateKeepers) {
        this.PerIdStateKeepers = PerIdStateKeepers;
    }

    public void setPerIdStateKeepers(PerIdBean PerIdStateKeepers) {
        this.PerIdStateKeepers.add(PerIdStateKeepers);
    }

    @Transient
    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /*
     * public PerIdStateKeeper getPerIdStateKeeper() { Rlog.debug("perId",
     * "PerIdBean.getPerIdStateKeeper"); return new PerIdStateKeeper(getId(),
     * getPerId(), getPerIdType(), getAlternatePerId(), getCreator(),
     * getModifiedBy(), getIpAdd()); }*
     * 
     * /** sets up a state keeper containing details of the per id
     */

    /*
     * public void setPerIdStateKeeper(PerIdStateKeeper ssk) {
     * 
     * GenerateId stId = null; try { Connection conn = null; conn =
     * getConnection();
     * 
     * Rlog.debug("perId", "PerIdBean.setPerIdStateKeeper() Connection :" +
     * conn);
     * 
     * this.id = stId.getId("SEQ_PAT_PERID", conn);
     * 
     * Rlog.debug("perId", "PerIdBean.setPerIdStateKeeper() perId :" +
     * this.perId); conn.close();
     * 
     * setPerId(ssk.getPerId()); setPerIdType(ssk.getPerIdType());
     * setAlternatePerId(ssk.getAlternatePerId()); setCreator(ssk.getCreator());
     * setIpAdd(ssk.getIpAdd()); } catch (Exception e) { Rlog.fatal("perId",
     * "Error in setPerIdStateKeeper() in PerIdBean " + e); } }
     */

    /**
     * updates the PerId Record
     * 
     * @param PerIdStateKeeper
     *            psk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updatePerId(PerIdBean psk) {
        try {
            setPerId(psk.getPerId());
            setPerIdType(psk.getPerIdType());
            setAlternatePerId(psk.getAlternatePerId());
            setModifiedBy(psk.getModifiedBy());
            setIpAdd(psk.getIpAdd());
            setCreator(psk.getCreator());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("perId", " error in perIdBean.updatePerId");
            return -2;
        }
    }

    public PerIdBean() {
    	PerIdStateKeepers = new ArrayList();
    }

    public PerIdBean(int id, String perId, String perIdType,
            String alternatePerId, String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setPerId(perId);
        setPerIdType(perIdType);
        setAlternatePerId(alternatePerId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        PerIdStateKeepers = new ArrayList();
    }

}
