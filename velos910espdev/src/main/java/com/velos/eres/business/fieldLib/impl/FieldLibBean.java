/*
 * Classname : FieldLib
 * 
 * Version information: 1.0
 *
 * Date: 30/06/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.business.fieldLib.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.business.common.Style;
import com.velos.eres.business.fldValidate.impl.FldValidateBean;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The FieldLib CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Kaura
 * @vesion 1.0 30/06/2003
 * @ejbHome FieldLibHome
 * @ejbRemote FieldLibRObj
 * 
 */
@Entity
@Table(name = "er_fldlib")
public class FieldLibBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257847688345956921L;

    /**
     * The primary key for the Field Library Table : pk_field
     */
    public int fieldLibId;

    /**
     * The foreign key reference to the Category Library: FK_CATLIB
     */

    public Integer catLibId;

    /**
     * The foreign key reference to the Account Table: FK_ACCOUNT
     */

    public Integer accountId;

    /**
     * The Library Flag to display if the field is within the library or belongs
     * <br>
     * to a form : FLD_LIBFLAG
     */

    public String libFlag;

    /**
     * The Name of the Field Component : FLD_NAME
     */

    public String fldName;

    /**
     * The description of the Field Component: FLD_DESC
     */

    public String fldDesc;

    /**
     * The Field Component Unique ID : FLD_UNIQUEID
     */

    public String fldUniqId;

    /**
     * The Field Component System ID: FLD_SYSTEMID
     * 
     */

    public String fldSysID;

    /**
     * 
     * The Field Component Key Word :FLD_KEYWORD
     */

    public String fldKeyword;

    /**
     * 
     * The Field Type of the Field Component
     */

    public String fldType;

    /**
     * 
     * The Datatype of the Field Component :FLD_DATATYPE
     * 
     */

    public String fldDataType;

    /**
     * 
     * The Instructions for the Field Component : FLD_INSTRUCTIONS
     * 
     */

    public String fldInstructions;

    /**
     * 
     * The length of the Field Component :FLD_LENGTH
     * 
     */

    public Integer fldLength;

    /**
     * : FLD_DECIMAL
     * 
     */

    public Integer fldDecimal;

    /**
     * 
     * The number of lines in the field : FLD_LINESNO
     * 
     */

    public Integer fldLinesNo;

    /**
     * 
     * The number of characters in the field : FLD_CHARSNO
     */

    public Integer fldCharsNo;

    /**
     * 
     * The Default Response of the Field Component: FLD_DEFRESP
     * 
     */

    public String fldDefResp;

    /**
     * The foreign key reference to the Look up table : FK_LOOKUP
     * 
     */

    public Integer lookup;

    /**
     * 
     * The attribute of the field component if its uniqe: FLD_ISUNIQUE
     * 
     * 
     */

    public Integer fldIsUniq;

    /**
     * 
     * The attribute of the field component if its read only : FLD_ISREADONLY
     * 
     */

    public Integer fldIsRO;

    /**
     * 
     * The attribute of the field component if its visible : FLD_ISVISIBLE
     * 
     */

    public Integer fldIsVisible;

    /**
     * 
     * The attribute of the field component the number of columns it has:
     * FLD_COLCOUNT
     * 
     */

    public Integer fldColCount;

    /**
     * 
     * The attribute of the field component of format of the field: FLD_FORMAT
     * 
     */

    public String fldFormat;

    /**
     * 
     * The attribute of the field component of the number of times it repeats:
     * FLD_REPEATFLAG
     * 
     */

    public Integer fldRepeatFlag;

    /**
     * 
     * The attribute of the field component if its bold : FLD_BOLD
     * 
     */

    public Integer fldBold;

    /**
     * 
     * The attribute of the field component if its italics: FLD_ITALICS
     * 
     */

    public Integer fldItalics;

    /**
     * 
     * The attribute of the field component if its on same line : FLD_SAMELINE
     * 
     */

    public Integer fldSameline;

    /**
     * 
     * The attribute of the field component if its align: FLD_ALIGN
     * 
     */

    public String fldAlign;

    /**
     * 
     * The attribute of the field component if its to be underline :
     * FLD_UNDERLINE
     * 
     */

    public Integer fldUnderline;

    /**
     * 
     * The attribute of the field component the color it has: FLD_COLOR
     * 
     */

    public String fldColor;

    /**
     * 
     * The attribute of the field component the font: FLD_FONT
     * 
     */

    public String fldFont;

    /**
     * 
     * 
     * The attribute of the field component the font size: FLD_FONTSIZE
     * 
     */

    public Integer fldFontSize;

    /**
     * 
     * 
     * The record type of the field type : RECORD_TYPE
     * 
     */

    public String recordType;

    /**
     * 
     * 
     * The creator of the field type : CREATOR
     * 
     */

    public Integer creator;

    /**
     * 
     * The id of the user who last modified the field : LAST_MODIFIED_BY
     * 
     */

    public Integer modifiedBy;

    /**
     * 
     * The IP Address of the site which made the change : IP_ADD
     * 
     */

    public String ipAdd;

    /**
     * The Style of the field
     */

    private Style aStyle;

    /**
     * The Lookup Display Value to be retrieved
     */

    public String lkpDisplayVal;

    /**
     * The Lookup Data Value to be retrieved
     */

    public String lkpDataVal;

    /**
     * Data greater than Today's date or not
     */
    public Integer todayCheck;

    /**
     * override mandatory validation on field or not
     */

    public Integer overRideMandatory;

    /**
     * override number Range validation on field or not
     */

    public Integer overRideRange;

    /**
     * override number format validation on field or not
     */

    public Integer overRideFormat;

    /**
     * override date validation on field or not
     */

    public Integer overRideDate;

    /**
     * 
     * The Field Lookup Type (L- normal llookup, A- ad-hoc reports)
     */

    public String fldLkpType;

    /**
     * expand label or not
     */

    public Integer expLabel;

    /**
     * flag to hide field label in preview, Possible values - 0:No, 1:Yes'
     */

    public String fldHideLabel;

    /**
     * flag to hide response label in form preview, Possible values - 0:No,
     * 1:Yes
     */

    public String fldHideResponseLabel;

    /**
     * This width of the column in which field label is displayed in preview
     * mode
     */

    public String fldDisplayWidth;

    /**
     * This column stores pk_formlib of the form linked with this field, for
     * fldType='F'
     */

    public String fldLinkedForm;

    /**
     * Aligment atribute for field response label
     */

    public String fldResponseAlign;

    /**
     * Formatted Field name definition
     */
    public String fldNameFormat;

    /**
     * Sort Order defined for field
     */
    public String fldSortOrder;

    private FldValidateBean fldValidateBean;

    private String formFldId;

    private String formfldMandatory;

    private String formSecId;

    private String formfldSeq;

    private String formFldBrowserFlg;

    private String formId;

    // GETTERS AND SETTERS
    private void createStyle() {
        aStyle = new Style();
    }

    public FieldLibBean() {
        createStyle();
    }

    public FieldLibBean(int fieldLibId, String catLibId, String accountId,
            String libFlag, String fldName, String fldDesc, String fldUniqId,
            String fldSysID, String fldKeyword, String fldType,
            String fldDataType, String fldInstructions, String fldLength,
            String fldDecimal, String fldLinesNo, String fldCharsNo,
            String fldDefResp, String lookup, String fldIsUniq, String fldIsRO,
            String fldIsVisible, String fldColCount, String fldFormat,
            String fldRepeatFlag, String recordType, String creator,
            String modifiedBy, String ipAdd, Style aStyle,
            String lkpDisplayVal, String lkpDataVal, String todayCheck,
            String overRideMandatory, String overRideRange,
            String overRideFormat, String overRideDate, String fldLkpType,
            String expLabel, String fldHideLabel, String fldHideResponseLabel,
            String fldDisplayWidth, String fldLinkedForm,
            String fldResponseAlign, String fldNameFormat, String fldSortOrder) {
        super();
        // TODO Auto-generated constructor stub

        setFieldLibId(fieldLibId);
        setCatLibId(catLibId);
        setAccountId(accountId);
        setLibFlag(libFlag);
        setFldName(fldName);
        setFldDesc(fldDesc);
        setFldUniqId(fldUniqId);
        setFldSysID(fldSysID);
        setFldKeyword(fldKeyword);
        setFldType(fldType);
        setFldDataType(fldDataType);
        setFldInstructions(fldInstructions);
        setFldLength(fldLength);
        setFldDecimal(fldDecimal);
        setFldLinesNo(fldLinesNo);
        setFldCharsNo(fldCharsNo);
        setFldDefResp(fldDefResp);
        setLookup(lookup);
        setFldIsUniq(fldIsUniq);
        setFldIsRO(fldIsRO);
        setFldIsVisible(fldIsVisible);
        setFldColCount(fldColCount);
        setFldFormat(fldFormat);
        setFldRepeatFlag(fldRepeatFlag);

        setFldAlign(fldAlign);

        setFldColor(fldColor);
        setFldFont(fldFont);

        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAStyle(aStyle);
        setLkpDisplayVal(lkpDisplayVal);
        setLkpDataVal(lkpDataVal);
        setTodayCheck(todayCheck);
        setOverRideMandatory(overRideMandatory);
        setOverRideRange(overRideRange);
        setOverRideFormat(overRideFormat);
        setOverRideDate(overRideDate);
        setFldLkpType(fldLkpType);
        setExpLabel(expLabel);
        setFldHideLabel(fldHideLabel);
        setFldHideResponseLabel(fldHideResponseLabel);
        setFldDisplayWidth(fldDisplayWidth);
        setFldLinkedForm(fldLinkedForm);
        setFldResponseAlign(fldResponseAlign);
        setFldNameFormat(fldNameFormat);
        setFldSortOrder(fldSortOrder);

    }

    /**
     * 
     * 
     * @return fieldLibId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FLDLIB", allocationSize=1)
    @Column(name = "PK_FIELD")
    public int getFieldLibId() {
        return this.fieldLibId;
    }

    /**
     * 
     * 
     * @param fieldLibId
     */
    public void setFieldLibId(int fieldLibId) {
        this.fieldLibId = fieldLibId;
    }

    /**
     * 
     * 
     * @return catLibId
     */
    @Column(name = "FK_CATLIB")
    public String getCatLibId() {
        return StringUtil.integerToString(this.catLibId);
    }

    /**
     * 
     * 
     * @param catLibId
     */
    public void setCatLibId(String catLibId) {

        this.catLibId = StringUtil.stringToInteger(catLibId);

    }

    /**
     * 
     * 
     * @return accountId
     */
    @Column(name = "FK_ACCOUNT")
    public String getAccountId() {

        return StringUtil.integerToString(this.accountId);
    }

    /**
     * 
     * 
     * @param accountId
     */
    public void setAccountId(String accountId) {

        this.accountId = StringUtil.stringToInteger(accountId);

    }

    /**
     * 
     * 
     * @return libFlag
     */
    @Column(name = "FLD_LIBFLAG")
    public String getLibFlag() {
        return this.libFlag;
    }

    /**
     * 
     * 
     * @param libFlag
     */
    public void setLibFlag(String libFlag) {
        this.libFlag = libFlag;
    }

    /**
     * 
     * 
     * @return fldName
     */

    @Column(name = "FLD_NAME")
    public String getFldName() {
        return this.fldName;
    }

    /**
     * 
     * 
     * @param fldName
     */
    public void setFldName(String fldName) {
        this.fldName = fldName;
    }

    /**
     * 
     * 
     * @return fldDesc
     */
    @Column(name = "FLD_DESC")
    public String getFldDesc() {
        return this.fldDesc;
    }

    /**
     * 
     * 
     * @param fldDesc
     */
    public void setFldDesc(String fldDesc) {
        this.fldDesc = fldDesc;
    }

    /**
     * 
     * 
     * @return fldUniqId
     */
    @Column(name = "FLD_UNIQUEID")
    public String getFldUniqId() {
        return this.fldUniqId;
    }

    /**
     * 
     * 
     * @param fldUniqId
     */
    public void setFldUniqId(String fldUniqId) {
        this.fldUniqId = fldUniqId;
    }

    /**
     * 
     * 
     * @return fldSysID
     */

    @Column(name = "FLD_SYSTEMID")
    public String getFldSysID() {
        return this.fldSysID;
    }

    /**
     * 
     * 
     * @param fldSysID
     */
    public void setFldSysID(String fldSysID) {
        this.fldSysID = fldSysID;
    }

    /**
     * 
     * 
     * @return fldKeyword
     */
    @Column(name = "FLD_KEYWORD")
    public String getFldKeyword() {
        return this.fldKeyword;
    }

    /**
     * 
     * 
     * @param fldKeyword
     */
    public void setFldKeyword(String fldKeyword) {
        this.fldKeyword = fldKeyword;
    }

    /**
     * 
     * 
     * @return fldType
     */
    @Column(name = "FLD_TYPE")
    public String getFldType() {
        return this.fldType;
    }

    /**
     * 
     * 
     * @param fldType
     */
    public void setFldType(String fldType) {
        this.fldType = fldType;
    }

    /**
     * 
     * 
     * @return fldDataType
     */
    @Column(name = "FLD_DATATYPE")
    public String getFldDataType() {
        return this.fldDataType;
    }

    /**
     * 
     * 
     * @param fldDataType
     */
    public void setFldDataType(String fldDataType) {
        this.fldDataType = fldDataType;
    }

    /**
     * 
     * 
     * @return fldInstructions
     */
    @Column(name = "FLD_INSTRUCTIONS")
    public String getFldInstructions() {
        return this.fldInstructions;
    }

    /**
     * 
     * 
     * @param fldInstructions
     */
    public void setFldInstructions(String fldInstructions) {
        this.fldInstructions = fldInstructions;
    }

    /**
     * 
     * 
     * @return fldLength
     */
    @Column(name = "FLD_LENGTH")
    public String getFldLength() {
        return StringUtil.integerToString(this.fldLength);

    }

    /**
     * 
     * 
     * @param fldLength
     */
    public void setFldLength(String fldLength) {
        this.fldLength = StringUtil.stringToInteger(fldLength);
    }

    /**
     * 
     * 
     * @return fldDecimal
     */
    @Column(name = "FLD_DECIMAL")
    public String getFldDecimal() {

        return StringUtil.integerToString(this.fldDecimal);
    }

    /**
     * 
     * 
     * @param fldDecimal
     */
    public void setFldDecimal(String fldDecimal) {

        // Rlog.debug("fieldlib","String fldDecimal after StringUtil
        // "+StringUtil.stringToInteger(fldDecimal));
        this.fldDecimal = StringUtil.stringToInteger(fldDecimal);

    }

    /**
     * 
     * 
     * @return fldLinesNo
     */
    @Column(name = "FLD_LINESNO")
    public String getFldLinesNo() {

        return StringUtil.integerToString(this.fldLinesNo);
    }

    /**
     * 
     * 
     * @param fldLinesNo
     */
    public void setFldLinesNo(String fldLinesNo) {

        this.fldLinesNo = StringUtil.stringToInteger(fldLinesNo);
        Rlog.debug("fieldlib", "integer line number" + this.fldLinesNo);
    }

    /**
     * 
     * 
     * @return fldCharsNo
     */
    @Column(name = "FLD_CHARSNO")
    public String getFldCharsNo() {
        return StringUtil.integerToString(this.fldCharsNo);
    }

    /**
     * 
     * 
     * @param fldCharsNo
     */
    public void setFldCharsNo(String fldCharsNo) {
        this.fldCharsNo = StringUtil.stringToInteger(fldCharsNo);

    }

    /**
     * 
     * 
     * @return fldDefResp
     */
    @Column(name = "FLD_DEFRESP")
    public String getFldDefResp() {
        return this.fldDefResp;
    }

    /**
     * 
     * 
     * @param fldDefResp
     */
    public void setFldDefResp(String fldDefResp) {
        this.fldDefResp = fldDefResp;
    }

    /**
     * 
     * 
     * @return lookup
     */
    @Column(name = "FK_LOOKUP")
    public String getLookup() {
        return StringUtil.integerToString(this.lookup);
    }

    /**
     * 
     * 
     * @param lookup
     */
    public void setLookup(String lookup) {
        this.lookup = StringUtil.stringToInteger(lookup);

    }

    /**
     * 
     * 
     * @return fldIsUniq
     */
    @Column(name = "FLD_ISUNIQUE")
    public String getFldIsUniq() {

        return StringUtil.integerToString(this.fldIsUniq);
    }

    /**
     * 
     * 
     * @param fldIsUniq
     */
    public void setFldIsUniq(String fldIsUniq) {
        this.fldIsUniq = StringUtil.stringToInteger(fldIsUniq);
    }

    /**
     * 
     * 
     * @return fldIsRO
     */
    @Column(name = "FLD_ISREADONLY")
    public String getFldIsRO() {
        return StringUtil.integerToString(this.fldIsRO);
    }

    /**
     * 
     * 
     * @param fldIsRO
     */
    public void setFldIsRO(String fldIsRO) {
        this.fldIsRO = StringUtil.stringToInteger(fldIsRO);
    }

    /**
     * 
     * 
     * @return fldIsVisible
     */
    @Column(name = "FLD_ISVISIBLE")
    public String getFldIsVisible() {
        return StringUtil.integerToString(this.fldIsVisible);
    }

    /**
     * 
     * 
     * @param fldIsVisible
     */
    public void setFldIsVisible(String fldIsVisible) {
        this.fldIsVisible = StringUtil.stringToInteger(fldIsVisible);

    }

    /**
     * 
     * 
     * @return fldColCount
     */
    @Column(name = "FLD_COLCOUNT")
    public String getFldColCount() {
        return StringUtil.integerToString(this.fldColCount);
    }

    /**
     * 
     * 
     * @param fldColCount
     */
    public void setFldColCount(String fldColCount) {
        this.fldColCount = StringUtil.stringToInteger(fldColCount);

    }

    /**
     * 
     * 
     * @return fldFormat
     */

    @Column(name = "FLD_FORMAT")
    public String getFldFormat() {
        return this.fldFormat;
    }

    /**
     * 
     * 
     * @param fldFormat
     */
    public void setFldFormat(String fldFormat) {
        this.fldFormat = fldFormat;
    }

    /**
     * 
     * 
     * @return fldRepeatFlag
     */
    @Column(name = "FLD_REPEATFLAG")
    public String getFldRepeatFlag() {
        return StringUtil.integerToString(this.fldRepeatFlag);
    }

    /**
     * 
     * 
     * @param fldRepeatFlag
     */
    public void setFldRepeatFlag(String fldRepeatFlag) {

        this.fldRepeatFlag = StringUtil.stringToInteger(fldRepeatFlag);

    }

    /**
     * 
     * 
     * @return fldBold
     */
    @Column(name = "FLD_BOLD")
    public String getFldBold() {

        return StringUtil.integerToString(this.fldBold);
    }

    /**
     * 
     * 
     * @param fldBold
     */
    public void setFldBold(String fldBold) {
        this.fldBold = StringUtil.stringToInteger(fldBold);
        if (aStyle == null)
            createStyle();
        aStyle.setBold(getFldBold()); // populate style object

    }

    /**
     * 
     * 
     * @return fldItalics
     */
    @Column(name = "FLD_ITALICS")
    public String getFldItalics() {
        return StringUtil.integerToString(this.fldItalics);
    }

    /**
     * 
     * 
     * @param fldItalics
     */
    public void setFldItalics(String fldItalics) {
        this.fldItalics = StringUtil.stringToInteger(fldItalics);
        if (aStyle == null)
            createStyle();
        aStyle.setItalics(getFldItalics()); // populate style object

    }

    /**
     * 
     * 
     * @return fldSameline
     */
    @Column(name = "FLD_SAMELINE")
    public String getFldSameline() {
        return StringUtil.integerToString(this.fldSameline);
    }

    /**
     * 
     * 
     * @param fldSameline
     */
    public void setFldSameline(String fldSameline) {

        this.fldSameline = StringUtil.stringToInteger(fldSameline);
        if (aStyle == null)
            createStyle();
        aStyle.setSameLine(getFldSameline());// populate style object
    }

    /**
     * 
     * 
     * @return fldAlign
     */
    @Column(name = "FLD_ALIGN")
    public String getFldAlign() {
        return this.fldAlign;
    }

    /**
     * 
     * 
     * @param fldAlign
     */
    public void setFldAlign(String fldAlign) {
        this.fldAlign = fldAlign;
        if (aStyle == null)
            createStyle();
        aStyle.setAlign(getFldAlign()); // populate style object
    }

    /**
     * 
     * 
     * @return fldUnderline
     */
    @Column(name = "FLD_UNDERLINE")
    public String getFldUnderline() {
        return StringUtil.integerToString(this.fldUnderline);
    }

    /**
     * 
     * 
     * @param fldUnderline
     */
    public void setFldUnderline(String fldUnderline) {
        this.fldUnderline = StringUtil.stringToInteger(fldUnderline);
        if (aStyle == null)
            createStyle();
        aStyle.setUnderline(getFldUnderline()); // populate style object

    }

    /**
     * 
     * 
     * @return fldColor
     */
    @Column(name = "FLD_COLOR")
    public String getFldColor() {
        return this.fldColor;
    }

    /**
     * 
     * 
     * @param fldColor
     */
    public void setFldColor(String fldColor) {
        this.fldColor = fldColor;
        if (aStyle == null)
            createStyle();
        aStyle.setColor(getFldColor()); // populate style object
    }

    /**
     * 
     * 
     * @return fldFont
     */
    @Column(name = "FLD_FONT")
    public String getFldFont() {
        return this.fldFont;
    }

    /**
     * 
     * 
     * @param fldFont
     */
    public void setFldFont(String fldFont) {
        this.fldFont = fldFont;
        if (aStyle == null)
            createStyle();
        aStyle.setFont(getFldFont()); // populate style object
    }

    /**
     * 
     * 
     * @return fldFontSize
     */
    @Column(name = "FLD_FONTSIZE")
    public String getFldFontSize() {
        return StringUtil.integerToString(this.fldFontSize);
    }

    /**
     * 
     * 
     * @param fldFontSize
     */
    public void setFldFontSize(String fldFontSize) {
        this.fldFontSize = StringUtil.stringToInteger(fldFontSize);
        if (aStyle == null)
            createStyle();
        aStyle.setFontSize(getFldFontSize()); // populate style object
    }

    /**
     * 
     * 
     * @return recordType
     */
    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * 
     * 
     * @param recordType
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * 
     * 
     * @return creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * 
     * 
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * 
     * 
     * @return modifiedBy
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * 
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);

    }

    /**
     * 
     * 
     * @return ipAdd
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * 
     * 
     * @param ipAdd
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * 
     * 
     * @return aStyle
     */
    @Transient
    public Style getAStyle() {
        return this.aStyle;
    }

    /**
     * 
     * 
     * @param aStyle
     */
    public void setAStyle(Style aStyle) {
        this.aStyle = aStyle;
    }

    /**
     * *
     * 
     * 
     * @return lkpDisplayVal
     */
    @Column(name = "FLD_LKPDISPVAL")
    public String getLkpDisplayVal() {
        return this.lkpDisplayVal;
    }

    /**
     * 
     * 
     * @param lkpDisplayVal
     */
    public void setLkpDisplayVal(String lkpDisplayVal) {
        this.lkpDisplayVal = lkpDisplayVal;
    }

    /**
     * 
     * 
     * @return lkpDataVal
     */
    @Column(name = "FLD_LKPDATAVAL")
    public String getLkpDataVal() {
        return this.lkpDataVal;
    }

    /**
     * 
     * 
     * @param lkpDataVal
     */
    public void setLkpDataVal(String lkpDataVal) {
        this.lkpDataVal = lkpDataVal;
    }

    /**
     * 
     * 
     * @return overRideMandatory
     */
    @Column(name = "FLD_OVERRIDE_MANDATORY")
    public String getOverRideMandatory() {
        return StringUtil.integerToString(this.overRideMandatory);

    }

    /**
     * 
     * 
     * @param overRideMandatory
     */
    public void setOverRideMandatory(String overRideMandatory) {
        this.overRideMandatory = StringUtil.stringToInteger(overRideMandatory);
    }

    /**
     * 
     * 
     * @return overRideRange
     */
    @Column(name = "FLD_OVERRIDE_RANGE")
    public String getOverRideRange() {
        return StringUtil.integerToString(this.overRideRange);

    }

    /**
     * 
     * 
     * @param overRideRange
     */
    public void setOverRideRange(String overRideRange) {
        this.overRideRange = StringUtil.stringToInteger(overRideRange);
    }

    /**
     * 
     * 
     * @return overRideFormat
     */
    @Column(name = "FLD_OVERRIDE_FORMAT")
    public String getOverRideFormat() {
        return StringUtil.integerToString(this.overRideFormat);

    }

    /**
     * 
     * 
     * @param overRideFormat
     */
    public void setOverRideFormat(String overRideFormat) {
        this.overRideFormat = StringUtil.stringToInteger(overRideFormat);
    }

    /**
     * 
     * 
     * @return overRideDate
     */
    @Column(name = "FLD_OVERRIDE_DATE")
    public String getOverRideDate() {
        return StringUtil.integerToString(this.overRideDate);

    }

    /**
     * 
     * 
     * @param overRideDate
     */
    public void setOverRideDate(String overRideDate) {
        this.overRideDate = StringUtil.stringToInteger(overRideDate);
    }

    /**
     * 
     * 
     * @return todayCheck
     */
    @Column(name = "FLD_TODAYCHECK")
    public String getTodayCheck() {
        Rlog.debug("fieldlib", " in FieldLibBean getTodayCheck:"
                + this.todayCheck);
        return StringUtil.integerToString(this.todayCheck);

    }

    /**
     * 
     * 
     * @param todayCheck
     */
    public void setTodayCheck(String todayCheck) {
        this.todayCheck = StringUtil.stringToInteger(todayCheck);
        Rlog.debug("fieldlib", " in FieldLibBean setTodayCheck:" + todayCheck);
    }

    /**
     * 
     * 
     * @return fldLkpType
     */
    @Column(name = "FLD_LKPTYPE")
    public String getFldLkpType() {
        return this.fldLkpType;
    }

    /**
     * 
     * 
     * @param fldLkpType
     */
    public void setFldLkpType(String fldLkpType) {
        this.fldLkpType = fldLkpType;
    }

    /**
     * 
     * 
     * @return expLabel
     */
    @Column(name = "FLD_EXPLABEL")
    public String getExpLabel() {
        return StringUtil.integerToString(this.expLabel);

    }

    /**
     * 
     * 
     * @param expLabel
     */
    public void setExpLabel(String expLabel) {
        this.expLabel = StringUtil.stringToInteger(expLabel);
        aStyle.setExpLabel(getExpLabel());// populate style object
    }

    /**
     * Returns the value of fldHideLabel.
     */
    @Column(name = "FLD_HIDELABEL")
    public String getFldHideLabel() {
        return fldHideLabel;
    }

    /**
     * Sets the value of fldHideLabel.
     * 
     * @param fldHideLabel
     *            The value to assign fldHideLabel.
     */
    public void setFldHideLabel(String fldHideLabel) {
        if (StringUtil.isEmpty(fldHideLabel)) {
            fldHideLabel = null;
        }
        this.fldHideLabel = fldHideLabel;
    }

    /**
     * Returns the value of fldHideResponseLabel.
     */
    @Column(name = "FLD_HIDERESPLABEL")
    public String getFldHideResponseLabel() {
        return fldHideResponseLabel;
    }

    /**
     * Sets the value of fldHideResponseLabel.
     * 
     * @param fldHideResponseLabel
     *            The value to assign fldHideResponseLabel.
     */
    public void setFldHideResponseLabel(String fldHideResponseLabel) {
        if (StringUtil.isEmpty(fldHideResponseLabel)) {
            fldHideResponseLabel = null;
        }
        this.fldHideResponseLabel = fldHideResponseLabel;
    }

    /**
     * Returns the value of fldDisplayWidth.
     */
    @Column(name = "FLD_DISPLAY_WIDTH")
    public String getFldDisplayWidth() {
        return fldDisplayWidth;
    }

    /**
     * Sets the value of fldDisplayWidth.
     * 
     * @param fldDisplayWidth
     *            The value to assign fldDisplayWidth.
     */
    public void setFldDisplayWidth(String fldDisplayWidth) {
        if (StringUtil.isEmpty(fldDisplayWidth)) {
            fldDisplayWidth = null;
        }
        this.fldDisplayWidth = fldDisplayWidth;
    }

    /**
     * Returns the value of fldLinkedForm.
     */
    @Column(name = "FLD_LINKEDFORM")
    public String getFldLinkedForm() {
        return fldLinkedForm;
    }

    /**
     * Sets the value of fldLinkedForm.
     * 
     * @param fldLinkedForm
     *            The value to assign fldLinkedForm.
     */
    public void setFldLinkedForm(String fldLinkedForm) {
        if (StringUtil.isEmpty(fldLinkedForm)) {
            fldLinkedForm = null;
        }

        this.fldLinkedForm = fldLinkedForm;
    }

    /**
     * Returns the value of fldResponseAlign.
     */
    @Column(name = "FLD_RESPALIGN")
    public String getFldResponseAlign() {
        return fldResponseAlign;
    }

    /**
     * Sets the value of fldResponseAlign.
     * 
     * @param fldResponseAlign
     *            The value to assign fldResponseAlign.
     */
    public void setFldResponseAlign(String fldResponseAlign) {
        if (StringUtil.isEmpty(fldResponseAlign)) {
            fldResponseAlign = null;
        }

        this.fldResponseAlign = fldResponseAlign;
    }

    /**
     * Returns the value of fldNameFormat.
     */
    @Column(name = "FLD_NAME_FORMATTED")
    public String getFldNameFormat() {
        return fldNameFormat;
    }

    /**
     * Sets the value of fldNameFormat.
     * 
     * @param fldNameFormat
     *            The value to assign fldNameFormat.
     */
    public void setFldNameFormat(String fldNameFormat) {
        this.fldNameFormat = fldNameFormat;
    }

    /**
     * Returns the value of fldSortOrder.
     */

    @Column(name = "FLD_SORTORDER")
    public String getFldSortOrder() {
        return fldSortOrder;
    }

    /**
     * Sets the value of fldSortOrder.
     * 
     * @param fldSortOrder
     *            The value to assign fldSortOrder.
     */
    public void setFldSortOrder(String fldSortOrder) {
        this.fldSortOrder = fldSortOrder;
    }

    @Transient
    public Style getFieldStyle() {
        Style st = new Style();
        st.setBold(getFldBold());
        st.setItalics(getFldItalics());
        st.setSameLine(getFldSameline());
        st.setAlign(getFldAlign());
        st.setUnderline(getFldUnderline());
        st.setColor(getFldColor());
        st.setFont(getFldFont());
        st.setFontSize(getFldFontSize());
        st.setExpLabel(getExpLabel());

        return st;

    }

    /**
     * Returns the value of fldValBean.
     */
    @Transient
    public FldValidateBean getFldValidateBean() {
        return fldValidateBean;
    }

    /**
     * Sets the value of fldValBean.
     * 
     * @param fldValBean
     *            The value to assign fldValBean.
     */
    public void setFldValidateBean(FldValidateBean fldValidateBean) {
        this.fldValidateBean = fldValidateBean;
    }

    /*
     * @return formFldId
     */
    @Transient
    public String getFormFldId() {

        return this.formFldId;
    }

    /**
     * 
     * 
     * @param formFldId
     */
    public void setFormFldId(String formFldId) {
        this.formFldId = formFldId;
    }

    /**
     * 
     * 
     * @return formfldMandatory
     */
    @Transient
    public String getFormFldMandatory() {
        return this.formfldMandatory;
    }

    /**
     * 
     * 
     * @param formfldMandatory
     */
    public void setFormFldMandatory(String formfldMandatory) {
        this.formfldMandatory = formfldMandatory;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public String getFormSecId() {
        return this.formSecId;
    }

    public void setFormSecId(String formSecId) {
        this.formSecId = formSecId;
    }

    /**
     * 
     * 
     * @return formfldSeq
     */

    @Transient
    public String getFormFldSeq() {
        return this.formfldSeq;
    }

    /**
     * 
     * 
     * @param formfldSeq
     */
    public void setFormFldSeq(String formfldSeq) {
        this.formfldSeq = formfldSeq;
    }

    /**
     * @return formFldBrowserFlg
     */
    @Transient
    public String getFormFldBrowserFlg() {
        Rlog.debug("fieldlib", " IN THE STATE KEEPER getFormFldBrowserFlg 45"
                + this.formFldBrowserFlg);
        return this.formFldBrowserFlg;
    }

    /**
     * 
     * 
     * @param formFldBrowserflg
     */
    public void setFormFldBrowserFlg(String formFldBrowserFlg) {

        Rlog.debug("fieldlib", " IN THE STATE KEEPER 1" + formFldBrowserFlg);
        this.formFldBrowserFlg = formFldBrowserFlg;
        Rlog.debug("fieldlib", " IN THE STATE KEEPER 1"
                + this.formFldBrowserFlg);

    }

    @Transient
    public String getFormId() {
        return this.formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    // END OF THE GETTER AND SETTER METHODS

    /*
     * public FieldLibStateKeeper getFieldLibStateKeeper() {
     * Rlog.debug("fieldlib", "FieldLibBean.getFieldLibStateKeeper");
     * 
     * Style st = new Style(); st.setBold(getFldBold());
     * st.setItalics(getFldItalics()); st.setSameLine(getFldSameline());
     * st.setAlign(getFldAlign()); st.setUnderline(getFldUnderline());
     * st.setColor(getFldColor()); st.setFont(getFldFont());
     * st.setFontSize(getFldFontSize()); st.setExpLabel(getExpLabel());
     * 
     * setAStyle(st);
     * 
     * return (new FieldLibStateKeeper(getFieldLibId(), getCatLibId(),
     * getAccountId(), getLibFlag(), getFldName(), getFldDesc(), getFldUniqId(),
     * getFldSysID(), getFldKeyword(), getFldType(), getFldDataType(),
     * getFldInstructions(), getFldLength(), getFldDecimal(), getFldLinesNo(),
     * getFldCharsNo(), getFldDefResp(), getLookup(), getFldIsUniq(),
     * getFldIsRO(), getFldIsVisible(), getFldColCount(), getFldFormat(),
     * getFldRepeatFlag(), getRecordType(), getCreator(), getModifiedBy(),
     * getIpAdd(), getAStyle(), getLkpDisplayVal(), getLkpDataVal(),
     * getTodayCheck(), getOverRideMandatory(), getOverRideFormat(),
     * getOverRideRange(), getOverRideDate(), getFldLkpType(), getExpLabel(),
     * getFldHideLabel(), getFldHideResponseLabel(), getFldDisplayWidth(),
     * getFldLinkedForm(), getFldResponseAlign(), getFldNameFormat(),
     * getFldSortOrder())); }
     */

    /**
     * sets up a state keeper containing details of the Field Library
     */

    /*
     * public void setFieldLibStateKeeper(FieldLibStateKeeper fieldLsk) {
     * GenerateId fieldLIBId = null; try { Connection conn = null; conn =
     * getConnection(); //
     * Rlog.debug("fieldlib","FieldLibBean.setFieldLibStateKeeper() //
     * Connection :"+ conn); this.fieldLibId = fieldLIBId.getId("SEQ_ER_FLDLIB",
     * conn);
     * 
     * conn.close();
     * 
     * setCatLibId(fieldLsk.getCatLibId());
     * 
     * setAccountId(fieldLsk.getAccountId());
     * 
     * setLibFlag(fieldLsk.getLibFlag());
     * 
     * setFldName(fieldLsk.getFldName());
     * 
     * setFldDesc(fieldLsk.getFldDesc());
     * 
     * setFldUniqId(fieldLsk.getFldUniqId());
     * 
     * setFldSysID(fieldLsk.getFldSysID());
     * 
     * setFldKeyword(fieldLsk.getFldKeyword());
     * 
     * setFldType(fieldLsk.getFldType());
     * 
     * setFldDataType(fieldLsk.getFldDataType());
     * 
     * setFldInstructions(fieldLsk.getFldInstructions());
     * 
     * setFldLength(fieldLsk.getFldLength());
     * 
     * setFldDecimal(fieldLsk.getFldDecimal());
     * 
     * setFldLinesNo(fieldLsk.getFldLinesNo());
     * 
     * setFldCharsNo(fieldLsk.getFldCharsNo());
     * 
     * setFldDefResp(fieldLsk.getFldDefResp());
     * 
     * setLookup(fieldLsk.getLookup());
     * 
     * setFldIsUniq(fieldLsk.getFldIsUniq());
     * 
     * setFldIsRO(fieldLsk.getFldIsRO());
     * 
     * setFldIsVisible(fieldLsk.getFldIsVisible());
     * 
     * setFldColCount(fieldLsk.getFldColCount());
     * 
     * setFldFormat(fieldLsk.getFldFormat());
     * 
     * setFldRepeatFlag(fieldLsk.getFldRepeatFlag()); //
     * ////////////////////////////////////////////////////////////////////
     * 
     * setFldBold(fieldLsk.getAStyle().getBold());
     * 
     * setFldItalics(fieldLsk.getAStyle().getItalics());
     * 
     * setFldSameline(fieldLsk.getAStyle().getSameLine());
     * 
     * setFldAlign(fieldLsk.getAStyle().getAlign());
     * 
     * setFldUnderline(fieldLsk.getAStyle().getUnderline());
     * 
     * setFldColor(fieldLsk.getAStyle().getColor());
     * 
     * setFldFont(fieldLsk.getAStyle().getFont());
     * 
     * setFldFontSize(fieldLsk.getAStyle().getFontSize());
     * 
     * setExpLabel(fieldLsk.getAStyle().getExpLabel());
     * setAStyle(fieldLsk.getAStyle()); //
     * //////////////////////////////////////////////////////////////////////
     * 
     * setRecordType(fieldLsk.getRecordType());
     * 
     * setCreator(fieldLsk.getCreator());
     * 
     * setIpAdd(fieldLsk.getIpAdd());
     * 
     * setLkpDisplayVal(fieldLsk.getLkpDisplayVal());
     * 
     * setLkpDataVal(fieldLsk.getLkpDataVal());
     * 
     * setTodayCheck(fieldLsk.getTodayCheck()); Rlog.debug("fieldlib", "in
     * fieldlibbean getTodayCheck() :" + fieldLsk.getTodayCheck());
     * 
     * setOverRideMandatory(fieldLsk.getOverRideMandatory());
     * setOverRideRange(fieldLsk.getOverRideRange());
     * setOverRideFormat(fieldLsk.getOverRideFormat());
     * setOverRideDate(fieldLsk.getOverRideDate());
     * 
     * setFldLkpType(fieldLsk.getFldLkpType());
     * 
     * Rlog.debug("fieldlib", "in fieldlibbean getFldLkpType() :" +
     * fieldLsk.getFldLkpType());
     * 
     * setExpLabel(fieldLsk.getExpLabel());
     * setFldHideLabel(fieldLsk.getFldHideLabel());
     * setFldHideResponseLabel(fieldLsk.getFldHideResponseLabel());
     * setFldDisplayWidth(fieldLsk.getFldDisplayWidth());
     * setFldLinkedForm(fieldLsk.getFldLinkedForm());
     * setFldResponseAlign(fieldLsk.getFldResponseAlign());
     * setFldNameFormat(fieldLsk.getFldNameFormat());
     * setFldSortOrder(fieldLsk.getFldSortOrder()); } catch (Exception e) {
     * Rlog.fatal("fieldlib", "Error in setFieldLibStateKeeper() in FieldLibBean " +
     * e); } }
     */

    /**
     * updates the FieldLib Record
     * 
     * @param fieldLsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateFieldLib(FieldLibBean fieldLsk) {
        try {

            setFieldLibId(fieldLsk.getFieldLibId());
            setCatLibId(fieldLsk.getCatLibId());
            setAccountId(fieldLsk.getAccountId());
            setLibFlag(fieldLsk.getLibFlag());
            setFldName(fieldLsk.getFldName());
            setFldDesc(fieldLsk.getFldDesc());
            setFldUniqId(fieldLsk.getFldUniqId());
            setFldSysID(fieldLsk.getFldSysID());
            setFldKeyword(fieldLsk.getFldKeyword());
            setFldType(fieldLsk.getFldType());
            setFldDataType(fieldLsk.getFldDataType());
            setFldInstructions(fieldLsk.getFldInstructions());
            setFldLength(fieldLsk.getFldLength());
            setFldDecimal(fieldLsk.getFldDecimal());
            setFldLinesNo(fieldLsk.getFldLinesNo());
            setFldCharsNo(fieldLsk.getFldCharsNo());
            setFldDefResp(fieldLsk.getFldDefResp());
            setLookup(fieldLsk.getLookup());
            setFldIsUniq(fieldLsk.getFldIsUniq());
            setFldIsRO(fieldLsk.getFldIsRO());
            setFldIsVisible(fieldLsk.getFldIsVisible());
            setFldColCount(fieldLsk.getFldColCount());
            setFldFormat(fieldLsk.getFldFormat());
            setFldRepeatFlag(fieldLsk.getFldRepeatFlag());
            setRecordType(fieldLsk.getRecordType());
            setModifiedBy(fieldLsk.getModifiedBy());
            setIpAdd(fieldLsk.getIpAdd());
            setTodayCheck(fieldLsk.getTodayCheck());
            Rlog.debug("fieldlib", "in fieldlibbean getTodayCheck() :"
                    + fieldLsk.getTodayCheck());
            setOverRideMandatory(fieldLsk.getOverRideMandatory());
            setOverRideFormat(fieldLsk.getOverRideFormat());
            setOverRideRange(fieldLsk.getOverRideRange());
            setOverRideDate(fieldLsk.getOverRideDate());
            setFldLkpType(fieldLsk.getFldLkpType());
            Rlog.debug("fieldlib", "in fieldlibbean getFldLkpType() :"
                    + fieldLsk.getFldLkpType());
            setExpLabel(fieldLsk.getExpLabel());
            // ////////////////////////////////////////////////////////////////////

            setFldBold(fieldLsk.getAStyle().getBold());
            setFldItalics(fieldLsk.getAStyle().getItalics());
            setFldSameline(fieldLsk.getAStyle().getSameLine());
            setFldAlign(fieldLsk.getAStyle().getAlign());
            setFldUnderline(fieldLsk.getAStyle().getUnderline());
            setFldColor(fieldLsk.getAStyle().getColor());
            setFldFont(fieldLsk.getAStyle().getFont());
            setFldFontSize(fieldLsk.getAStyle().getFontSize());
            setAStyle(fieldLsk.getAStyle());

            // //////////////////////////////////////////////////////////////////////

            setLkpDisplayVal(fieldLsk.getLkpDisplayVal());
            setLkpDataVal(fieldLsk.getLkpDataVal());
            setFldHideLabel(fieldLsk.getFldHideLabel());
            setFldHideResponseLabel(fieldLsk.getFldHideResponseLabel());
            setFldDisplayWidth(fieldLsk.getFldDisplayWidth());
            setFldLinkedForm(fieldLsk.getFldLinkedForm());
            setFldResponseAlign(fieldLsk.getFldResponseAlign());
            setFldNameFormat(fieldLsk.getFldNameFormat());
            setFldSortOrder(fieldLsk.getFldSortOrder());

            setCreator(fieldLsk.getCreator());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("fieldlib", " error in FieldLibBean.updateFieldLib");
            return -2;
        }
    }

}
