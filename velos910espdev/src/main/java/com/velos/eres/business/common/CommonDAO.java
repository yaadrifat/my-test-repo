/*
 * Classname : CommonDao
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 03/06/2001
 *
 * Author: sonia sahni
 * Base class for all Data Access  classes 
 */

package com.velos.eres.business.common;

/* Import statments */
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.CLOB;

import com.velos.base.dbutil.DBEngine;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import statments */

/**
 * The Common Dao.<br>
 * <br>
 * Base class for all Data Access classes
 * 
 * @author Sonia Sahni
 * @vesion 1.0 03/06/2001
 */

public class CommonDAO implements java.io.Serializable {

    String clobData ;
    ArrayList mapping_lMapList;
    ArrayList mapping_rMapList;
    ArrayList mapping_subTypList;
    ArrayList mapping_type;
    
    private static boolean disableConnectionPool = false;
    
    static {
        String disableEresPool = System.getenv("DISABLE_ERES_POOL");
        if (StringUtil.isEmpty(disableEresPool)) {
            try {
                disableEresPool = EnvUtil.getEnvVariable("DISABLE_ERES_POOL");
            } catch(Exception e) {}
        }
        if ("%DISABLE_ERES_POOL%".equals(disableEresPool)) {
            disableEresPool = System.getProperty("DISABLE_ERES_POOL");
        }
        if (StringUtil.trueValue(disableEresPool).startsWith("Y")) {
            disableConnectionPool = true;
        }
    }


public CommonDAO() {
    	
	 mapping_lMapList = new ArrayList();
	 mapping_rMapList = new ArrayList();
	 mapping_subTypList = new ArrayList();
	 mapping_type=new ArrayList();
	 clobData="";
        
    }

    /**
     * returns a connection object
     * 
     * 
     */
    public void setClobData(String clobdata) {
        this.clobData = clobdata;
    }

    public String getClobData() {
        return clobData;
    }
    
    public static void InitializePool(String alias) {
        String eHome="";
        DBEngine dbengine = DBEngine.getEngine();
        if (!(dbengine.IsInitialized())) {
            eHome = StringUtil.trueValue(System.getenv("ERES_HOME"));
            if (StringUtil.isEmpty(eHome)) {
                try {
                    eHome = EnvUtil.getEnvVariable("ERES_HOME");
                } catch(Exception e) {}
            }
            if (eHome.trim().equals("%ERES_HOME%")) {
                eHome = StringUtil.trueValue(System.getProperty("ERES_HOME"));
            }
            eHome = eHome.trim();
            dbengine.initializePool(eHome+"erespool.properties", alias);
        }
    }

    /**
	 * @return the mapping_lMapList
	 */
	public ArrayList getMapping_lMapList() {
		return mapping_lMapList;
	}

	/**
	 * @param mapping_lMapList the mapping_lMapList to set
	 */
	public void setMapping_lMapList(ArrayList mapping_lMapList) {
		this.mapping_lMapList = mapping_lMapList;
	}
	
	public void setMapping_lMapList(String mapping_lMap) {
		this.mapping_lMapList.add(mapping_lMap);
	}

	/**
	 * @return the mapping_rMapList
	 */
	public ArrayList getMapping_rMapList() {
		return mapping_rMapList;
	}

	/**
	 * @param mapping_rMapList the mapping_rMapList to set
	 */
	public void setMapping_rMapList(ArrayList mapping_rMapList) {
		this.mapping_rMapList = mapping_rMapList;
	}
	/**
	 * @param mapping_rMapList the mapping_rMapList to set
	 */
	public void setMapping_rMapList(String mapping_rMap) {
		this.mapping_rMapList.add(mapping_rMap);
	}

	/**
	 * @return the mapping_subTypList
	 */
	public ArrayList getMapping_subTypList() {
		return mapping_subTypList;
	}

	/**
	 * @param mapping_subTypList the mapping_subTypList to set
	 */
	public void setMapping_subTypList(ArrayList mapping_subTypList) {
		this.mapping_subTypList = mapping_subTypList;
	}
	
	public void setMapping_subTypList(String mapping_subTyp) {
		this.mapping_subTypList.add(mapping_subTyp);
	}

	/**
	 * @return the mapping_type
	 */
	public ArrayList getMapping_type() {
		return mapping_type;
	}

	/**
	 * @param mapping_type the mapping_type to set
	 */
	public void setMapping_type(ArrayList mapping_type) {
		this.mapping_type = mapping_type;
	}
	
	public void setMapping_type(String mapping_type) {
		this.mapping_type.add(mapping_type);
	}


    public static Connection getConnection() {
        if (!disableConnectionPool) {
            try {
                InitializePool("eres");
                return (DBEngine.getEngine().getConnection("eres")) ;

            } catch (Exception e) {
                Rlog.fatal("common", "getConnection(): exception:" + e);
                return null;
            }
        }
        
        // ////////////
        try {
            Rlog.debug("common",
                    com.velos.eres.service.util.Configuration.DBDriverName);

            if (com.velos.eres.service.util.Configuration.DBDriverName == null) {
                com.velos.eres.service.util.Configuration.readSettings();
            }
            Rlog.debug("common", "DB DRiver = "
                    + com.velos.eres.service.util.Configuration.DBDriverName);
            Rlog.debug("common", "DB URL = "
                    + com.velos.eres.service.util.Configuration.DBUrl);
            Rlog.debug("common", "DB USER= "
                    + com.velos.eres.service.util.Configuration.DBUser);
            Rlog.debug("common", "DB PWD = "
                    + com.velos.eres.service.util.Configuration.DBPwd);
        } catch (Exception e) {
            Rlog.fatal("common", "IN COMMON DAO" + e);
        }

        // ////////////
        Connection conn = null;
        try {
            Class
                    .forName(com.velos.eres.service.util.Configuration.DBDriverName);
            conn = DriverManager.getConnection(
                    com.velos.eres.service.util.Configuration.DBUrl,
                    com.velos.eres.service.util.Configuration.DBUser,
                    com.velos.eres.service.util.Configuration.DBPwd);
            Rlog.debug("common", "Got Connection(): " + conn);
            return conn;

        } catch (Exception e) {
            Rlog.fatal("common", "getConnection(): exception:" + e);
            return null;
        }
    }

    // for sch connection

    public static Connection getSchConnection() {
        if (!disableConnectionPool) {
            try {
                InitializePool("esch");
                return (DBEngine.getEngine().getConnection("esch")) ;
            }catch (Exception e) {
                Rlog.fatal("common", "getConnection(): exception:" + e);
                return null;
            }
        }

        // ////////////
        try {
            Rlog.debug("common",
                    com.velos.esch.service.util.Configuration.DBDriverName);

            if (com.velos.esch.service.util.Configuration.DBDriverName == null) {
                com.velos.esch.service.util.Configuration.readSettings();
            }
            Rlog.debug("common", "SCH DB DRiver = "
                    + com.velos.esch.service.util.Configuration.DBDriverName);
            Rlog.debug("common", "SCH DB URL = "
                    + com.velos.esch.service.util.Configuration.DBUrl);
            Rlog.debug("common", "SCH DB USER= "
                    + com.velos.esch.service.util.Configuration.DBUser);
            Rlog.debug("common", "SCH DB PWD = "
                    + com.velos.esch.service.util.Configuration.DBPwd);
        } catch (Exception e) {
            Rlog.fatal("common", "IN COMMON DAO" + e);
        }

        Connection conn = null;
        try {
            Class
                    .forName(com.velos.esch.service.util.Configuration.DBDriverName);
            conn = DriverManager.getConnection(
                    com.velos.esch.service.util.Configuration.DBUrl,
                    com.velos.esch.service.util.Configuration.DBUser,
                    com.velos.esch.service.util.Configuration.DBPwd);
            Rlog.debug("common", "Got SCH Connection(): " + conn);
            return conn;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static Connection getPatConnection()
    {  /*
     public static String PATDBUrl;

    public static String PATDBUser;

    public static String PATDBPwd;

    public static String PATDBDriverName;
    */

        if (!disableConnectionPool) {
            try {
                InitializePool("epat");
                return (DBEngine.getEngine().getConnection("epat")) ;
            }catch (Exception e) {
                Rlog.fatal("common", "getPatConnection(): exception:" + e);
                return null;
            }
        }

        try {
            Rlog.debug("common",
                    com.velos.eres.service.util.Configuration.PATDBDriverName);

            if (com.velos.eres.service.util.Configuration.PATDBDriverName == null) {
                com.velos.eres.service.util.Configuration.readSettings();
            }
            Rlog.debug("common", "DB DRiver = "
                    + com.velos.eres.service.util.Configuration.PATDBDriverName);
            Rlog.debug("common", "DB URL = "
                    + com.velos.eres.service.util.Configuration.PATDBUrl);
            Rlog.debug("common", "DB USER= "
                    + com.velos.eres.service.util.Configuration.PATDBUser);
          // Rlog.debug("common", "DB PWD = "+ com.velos.eres.service.util.Configuration.PATDBPwd);
        } catch (Exception e) {
            Rlog.fatal("common", "IN COMMON DAO" + e);
        }

        // ////////////
        Connection conn = null;
        try {
            Class
                    .forName(com.velos.eres.service.util.Configuration.PATDBDriverName);
            conn = DriverManager.getConnection(
                    com.velos.eres.service.util.Configuration.PATDBUrl,
                    com.velos.eres.service.util.Configuration.PATDBUser,
                    com.velos.eres.service.util.Configuration.PATDBPwd);
            Rlog.debug("common", "Got Connection(): " + conn);
            return conn;

        } catch (Exception e) {
            Rlog.fatal("common", "getConnection(): exception:" + e);
            return null;
        }
    }
    
    public static Connection getLongConnection() {
        
        // ////////////
        try {
            Rlog.debug("common",
                    com.velos.eres.service.util.Configuration.DBDriverName);

            if (com.velos.eres.service.util.Configuration.DBDriverName == null) {
                com.velos.eres.service.util.Configuration.readSettings();
            }
            Rlog.debug("common", "DB DRiver = "
                    + com.velos.eres.service.util.Configuration.DBDriverName);
            Rlog.debug("common", "DB URL = "
                    + com.velos.eres.service.util.Configuration.DBUrl);
            Rlog.debug("common", "DB USER= "
                    + com.velos.eres.service.util.Configuration.DBUser);
            Rlog.debug("common", "DB PWD = "
                    + com.velos.eres.service.util.Configuration.DBPwd);
        } catch (Exception e) {
            Rlog.fatal("common", "IN COMMON DAO" + e);
        }

        // ////////////
        Connection conn = null;
        try {
            Class
                    .forName(com.velos.eres.service.util.Configuration.DBDriverName);
            conn = DriverManager.getConnection(
                    com.velos.eres.service.util.Configuration.DBUrl,
                    com.velos.eres.service.util.Configuration.DBUser,
                    com.velos.eres.service.util.Configuration.DBPwd);
            Rlog.debug("common", "Got Connection(): " + conn);
            return conn;

        } catch (Exception e) {
            Rlog.fatal("common", "getConnection(): exception:" + e);
            return null;
        }
    }
    
    public static Connection getConnection(String db)
    {
        if (db.equals("eres")) return getConnection();
        else if (db.equals("esch") || db.equals("sch") ) return getSchConnection();
        else if (db.equals("epat")|| db.equals("per") ) return getPatConnection();
        else  return null;
        
                
        
    }
    public void returnConnection(Connection conn)
    {
        if (!disableConnectionPool) {
            DBEngine.getEngine().returnConnection(conn) ;
            return;
        }
        try {
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public int updateClob(String colValue, String tableName, String colName,
            String filterStr) {

        int resultCode = -1;
        CallableStatement cs = null;
        Connection conn = null;
        try {
            conn = this.getConnection();
            conn.setAutoCommit(false);
            Rlog.debug("common", "Clob updated to the DB" + colValue);
            Rlog.debug("common", "Clob updated to table" + tableName);
            Rlog.debug("common", "Clob updated to column" + colName);
            Rlog.debug("common", "Clob updated with filter" + filterStr);

            CLOB clob_update = getCLOB(colValue, conn);
            Rlog.debug("common", "string converted to CLOB" + clob_update);
            cs = (CallableStatement) conn
                    .prepareCall("begin sp_updateClob(?,?,?,?); end;");
            cs.setObject(1, clob_update);
            cs.setObject(2, tableName);
            cs.setObject(3, colName);
            cs.setObject(4, filterStr);
            // Execute callable statement.
            cs.execute();
            resultCode = 1; // successful

        } catch (Exception e) {
            Rlog.fatal("common",
                    "EJBUtil:Exception thrown in updateCLOB method "
                            + e.getMessage());
            resultCode = -1;
        } finally {
            if (cs != null) { try { cs.close(); } catch(Exception e) {} }
            if (conn != null) { try { conn.close(); } catch(Exception e) {} }
        }
        return resultCode;
    }

    public void populateClob(String tableName, String colName, String fltrStr) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        CLOB clob_data = null;
        try {
            conn = getConnection();

            String sql = "select " + colName + " from  " + tableName + fltrStr;
            Rlog.debug("common", "sql generated in populateClob" + sql);
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                clob_data = (CLOB) rs.getClob(1);
                if (clob_data != null)
                    setClobData(clob_data.getSubString(1, (int) clob_data
                            .length()));
            }
        } catch (SQLException e) {
            Rlog.fatal("common", "{SQLEXCEPTION IN commonDao:populateClob()}"
                    + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    /*
     * This method creates CLOB object using temporary clob and returns the
     * same.
     */
    public static CLOB getCLOB(String clobData, Connection conn)
            throws Exception {
        CLOB tempClob = null; 

        try {
            // create a new temporary CLOB
            Rlog.debug("common",
                    "In EJBUtil:getClob:Will create tempClob now with connection"
                            + conn);
            tempClob = CLOB.createTemporary(conn, true, CLOB.DURATION_SESSION);
            Rlog.debug("common", "In EJBUtil:getClob:tempClob is" + tempClob);
            // Open the temporary CLOB in readwrite mode to enable writing
            tempClob.open(CLOB.MODE_READWRITE);

            // Get the output stream to write
            Writer tempClobWriter = tempClob.getCharacterOutputStream();

            // Write the data into the temporary CLOB
            tempClobWriter.write(clobData);

            // Flush and close the stream
            tempClobWriter.flush();
            tempClobWriter.close();

            // Close the temporary CLOB
            tempClob.close();

        } catch (Exception exp) {
            exp.printStackTrace();
            tempClob.freeTemporary();
            Rlog.fatal("common",
                    "Exception thrown in getCLOB method of given status : "
                            + exp.getMessage());
        }
        Rlog.debug("common", "after getCLOB" + tempClob);
        return tempClob;
    }
    
    public boolean getMappingData(String cType,String org) {
        int rows = 0,sepList=0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String Sql="";
        if (cType.indexOf(",")>=0){ 
        	Sql="select mapping_lmapfld, mapping_rmapfld, mapping_type,mapping_subtyp from er_mapping where mapping_type in ( "+ cType +") and lower(mapping_customcol)=lower(?) order by mapping_type";
        	sepList=1;
        }
        else
        {
        	Sql="select mapping_lmapfld, mapping_rmapfld, mapping_type,mapping_subtyp from er_mapping where mapping_type =? and lower(mapping_customcol)=lower(?) order by mapping_type";
        	sepList=0;
        }
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement(Sql);
            if (sepList==0)
            	{
            	pstmt.setString(1, cType);
            	pstmt.setString(2, org);
            	}else
            	{
            		pstmt.setString(1, org);
            	}
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	this.setMapping_type(rs.getString("mapping_type"));
                this.setMapping_lMapList(rs.getString("mapping_lmapfld"));
                this.setMapping_rMapList(rs.getString("mapping_rmapfld"));
                this.setMapping_subTypList(rs.getString("mapping_subtyp"));
                rows++;
            
            }
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROM Mapping TABLE "
                    + ex);
            ex.printStackTrace();
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}// end of class

