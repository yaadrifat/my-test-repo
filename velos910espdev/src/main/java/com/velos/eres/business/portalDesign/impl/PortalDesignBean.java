/*
 * Classname			PortalDesignBean.class
 * 
 * Version information
 *
 * Date					04/19/2007 
 * 
 * Copyright notice
 */

package com.velos.eres.business.portalDesign.impl;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Portal Design BMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 * @version 1.0 04/19/2007
 */
@Entity
@Table(name = "er_portal_modules")
public class PortalDesignBean implements Serializable {
   
    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * Portal Module Id
     */
    public int portalModId;

    /**
     * fk Portal Id
     */
    public Integer fkPortalId;

    /**
     * fkId er_formlib /event_assoc
     */
    public String fkId;

    /**
     * Portal Module Type
     */
    public String portalModType;

    /**
     * Portal Module Alignment
     */
    public String portalModAlign;

    /**
     * Portal Module From
     */
    public Integer portalModFrom;

    /**
     * Portal Module To
     */
    public Integer portalModTo;
    
    /**
     * Portal Module From Unit for schedule date range
     */
    public String portalModFromUnit;

    
    /**
     * Portal Module To Unit for schedule date range
     */
    public String portalModToUnit;


    /**
     * Portal Format After Response
     */
    public String portalFormAfterResp;

    
    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_portal_modules", allocationSize=1)
    
    @Column(name = "PK_PORTAL_MODULES")
    public int getPortalModId() {
        return this.portalModId;
    }

    public void setPortalModId(int portalModId) {
        this.portalModId = portalModId;
    }

    @Column(name = "FK_PORTAL")
    public String getFkPortalId() {
        return StringUtil.integerToString(this.fkPortalId);
    }

    public void setFkPortalId(String fkPortalId) {
        this.fkPortalId = Integer.valueOf(fkPortalId);
    }
    
    @Column(name = "FK_ID")
    
    public String getFkId() {
        return this.fkId;
    }

    public void setFkId(String fkId) {
        this.fkId = fkId;
    }
    
    @Column(name = "PM_TYPE")
    public String getPortalModType() {
    	return this.portalModType;
    }

    public void setPortalModType(String portalModType) {    	
    	this.portalModType = portalModType;
    }

    @Column(name = "PM_ALIGN")
    public String getPortalModAlign() {
    	return this.portalModAlign;
    }

    
    public void setPortalModAlign(String portalModAlign) {
    	this.portalModAlign = portalModAlign;
    }

    @Column(name = "PM_FROM")
    public String getPortalModFrom() {
        return StringUtil.integerToString(this.portalModFrom);
    }

    public void setPortalModFrom(String portalModFrom) {
        //this.portalModFrom =Integer.valueOf(portalModFrom);
        this.portalModFrom = StringUtil.stringToInteger(portalModFrom);
        
    }
    
    @Column(name = "PM_TO")
    public String getPortalModTo() {
        return StringUtil.integerToString(this.portalModTo);
    }

    public void setPortalModTo(String portalModTo) {
        //this.portalModTo =Integer.valueOf(portalModTo);
        this.portalModTo = StringUtil.stringToInteger(portalModTo);
    }
    
    @Column(name = "PM_FROM_UNIT")
    public String getPortalModFromUnit() {
        return this.portalModFromUnit;
    }

    public void setPortalModFromUnit(String portalModFromUnit) {
        this.portalModFromUnit = portalModFromUnit;
    }
    
    @Column(name = "PM_TO_UNIT")
    public String getPortalModToUnit() {
        return this.portalModToUnit;
    }

    public void setPortalModToUnit(String portalModToUnit) {
        this.portalModToUnit = portalModToUnit;
    }
    
    @Column(name = "PM_FORM_AFTER_RESP")
    public String getPortalFormAfterResp() {
    	return this.portalFormAfterResp;
    }

    
    public void setPortalFormAfterResp(String portalFormAfterResp) {
    	this.portalFormAfterResp = portalFormAfterResp;
    }
    
    
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    // END OF GETTER SETTER METHODS

    public int updatePortalDesign(PortalDesignBean pdsk) {
        try {
        	setPortalModId(pdsk.getPortalModId());
        	setFkPortalId(pdsk.getFkPortalId());
        	setFkId(pdsk.getFkId());
        	setPortalModType(pdsk.getPortalModType());
        	setPortalModAlign(pdsk.getPortalModAlign());
        	setPortalModFrom(pdsk.getPortalModFrom());
        	setPortalModTo(pdsk.getPortalModTo());
        	setPortalModFromUnit(pdsk.getPortalModFromUnit());
        	setPortalModToUnit(pdsk.getPortalModToUnit());
        	setPortalFormAfterResp(pdsk.getPortalFormAfterResp());
        	setCreator(pdsk.getCreator());
            setModifiedBy(pdsk.getModifiedBy());
            setIpAdd(pdsk.getIpAdd());
            Rlog.debug("portalDesign", "PortalDesignBean.updatePortalDesign");
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("PortalDesign", " error in PortalDesignBean.updatePortalDesign");
            return -2;
        }
    }

    public PortalDesignBean() {

    }

    public PortalDesignBean(int portalModId, String fkPortalId, String fkId, String portalModType, String portalModAlign, String portalModFrom, String portalModTo, String portalModFromUnit,
            String portalModToUnit, String portalFormAfterResp,  String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        
        setPortalModId(portalModId);
    	setFkPortalId(fkPortalId);
    	setFkId(fkId);
    	setPortalModType(portalModType);
    	setPortalModAlign(portalModAlign);
    	setPortalModFrom(portalModFrom);
    	setPortalModTo(portalModTo);
    	setPortalModFromUnit(portalModFromUnit);
    	setPortalModToUnit(portalModToUnit);
    	setPortalFormAfterResp(portalFormAfterResp);
    	setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);        
      
    }

}


