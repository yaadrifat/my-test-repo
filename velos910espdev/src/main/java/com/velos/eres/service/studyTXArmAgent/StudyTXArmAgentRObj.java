/**
 * Classname : StudyTXArmAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.studyTXArmAgent;

/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.StudyTXArmDao;
import com.velos.eres.business.studyTXArm.impl.StudyTXArmBean;

/* End of Import Statements */

/**
 * Remote interface for StudyTXArmAgent session EJB
 * 
 * @author Anu Khanna
 */

@Remote
public interface StudyTXArmAgentRObj {
    /**
     * gets the StudyTXArm record
     */
    StudyTXArmBean getStudyTXArmDetails(int studyTXArmId);

    /**
     * creates a new StudyTXArm Record
     */
    public int setStudyTXArmDetails(StudyTXArmBean stk);

    /**
     * updates the StudyTXArm Record
     */
    public int updateStudyTXArm(StudyTXArmBean stk);

    /**
     * remove StudyTXArm Record
     */
    // Overloaded for INF-18183 ::: Akshi
    public int removeStudyTXArm(int StudyTXArmId,Hashtable<String, String> args);

    public int removeStudyTXArm(int StudyTXArmId);

    public StudyTXArmDao getStudyTrtmtArms(int studyId);

    public int getCntTXForPat(int studyId, int studyTXArmId);

}
