/*
 * Classname : UsrSiteGrpAgentRObj
 * 
 * Version information: 1.1
 *
 * Date: 01/27/2012
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Yogesh
 */

package com.velos.eres.service.usrSiteGrpAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.common.UserSiteGroupDao;
import com.velos.eres.business.usrSiteGrp.impl.UsrSiteGrpBean;

/* End of Import Statements */

/**
 * Remote interface for UsrGrpAgent session EJB
 * 
 * @author sajal
 * @version : 1.0 03/12/2001
 */
@Remote
public interface UsrSiteGrpAgentRObj {
    /**
     * gets the usrGrp record
     */
	UsrSiteGrpBean getUsrSiteGrpDetails(int usrGrpId);
	
	/**
	 * get User Site Group Record
	 */
	public UserSiteGroupDao getUsrSiteGrp(int userid);
    /**
     * updates Uer Group Record
     */
    public int updateUsrSiteGrp(int userid, String[] userpkSite,String[] fkGroupIds,String[] pkUserSiteGrp,String ipAdd,String creator);


}
