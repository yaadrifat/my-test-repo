package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.submission.impl.ReviewMeetingTopicBean;

@Remote
public interface ReviewMeetingTopicAgent {
    public int createMeetingTopic(ReviewMeetingTopicBean reviewMeetingTopicBean);
    public int updateMeetingTopic(ReviewMeetingTopicBean reviewMeetingTopicBean);
    public ArrayList findMeetingTopicsByFkReviewMeeting(int fkReviewMeeting);
    public ReviewMeetingTopicBean findByFkReviewMeetingAndNumber(int fkReviewMeeting, String topicNumber);
    public ReviewMeetingTopicBean getMeetingTopic(int id);
    
    
    /**
     * remove ReviewMeetingTopi Record
     * 
     */
    public int removeReviewMeetingTopic(int topicPK);
 // Overloaded for INF-18183 ::: AGodara
    public int removeReviewMeetingTopic(int topicPK,Hashtable<String, String> auditInfo);

}
