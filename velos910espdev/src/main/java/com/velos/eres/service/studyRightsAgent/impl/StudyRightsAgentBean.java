/*
 * Classname			StudyRightsAgentBean.class
 * 
 * Version information
 *
 * Date					03/15/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyRightsAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.studyRights.impl.StudyGroupRights;
import com.velos.eres.business.studyRights.impl.StudyRightsBean;
import com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.studyRights.StudyRightsJB;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */
@Stateless
public class StudyRightsAgentBean implements StudyRightsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the id parameter passed in and constructs and returns a state
     * holder. containing the details of the Study Rights<br>
     * 
     * @param id
     *            the Study Rights id
     */
    public StudyRightsBean getStudyRightsDetails(int id) {

        StudyRightsBean ssk = new StudyRightsBean();

        String studyRight = null;
        CtrlDao cntrl;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        try {
        	 
            ssk = (StudyRightsBean) em.find(StudyRightsBean.class, new Integer(
                    id));
            studyRight = ssk.getTeamRights(); // Getting the Rights
            // assigned to the study on
            // each feature
            // from Entity Bean
            // ssk = retrieved.getStudyRightsStateKeeper();// Gets Group Id and
            Rlog.debug("studyrights", "State Keeper for Study Rights" + ssk);
            sLength = studyRight.length();
            Rlog.debug("studyrights", "Length Of the Study Rights" + sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                Rlog.debug("studyrights", "Counter" + counter);
                Rlog.debug("studyrights", "Study Right at the current Counter"
                        + String.valueOf(studyRight.charAt(counter)));
                ssk.setFtrRights(String.valueOf(studyRight.charAt(counter)));
                ssk.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("study_rights");
            cVal = cntrl.getCValue();
            cDesc = cntrl.getCDesc();
            ssk.setGrValue(cVal);
            ssk.setGrDesc(cDesc);
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("studyrights", "EXCEPTION" + e);
        }
        Rlog.debug("studyrights", "RETURNING FROM SESSION BEAN GROUP RIGHTS"
                + ssk);
        return ssk;
    }

    /**
     * Sets Study Rights.
     * 
     * @param Study
     *            Rights state holder containing Rights attributes to be set.
     * @return Void
     */

    /*
     * public int setStudyRightsDetails(StudyRightsBean ssk) { int stdTeamId =
     * 0; int counter = 0; int sLength = 0; ArrayList studyRht = new
     * ArrayList(); String rights = ""; // int studyTeamId; try { // studyTeamId =
     * EJBUtil.stringToNum(ssk.getStudyTeamId()); Enumeration enum_velos =
     * studyRightsHome.findByStudyTeamId(ssk .getStudyTeamId()); if
     * (enum_velos.hasMoreElements()) return -1; // duplicate Study Team ids are
     * not allowed
     * 
     * studyRht = ssk.getFtrRights(); Rlog.debug("studyrights", "Study Rights
     * from State Keeper " + studyRht); sLength = studyRht.size();
     * Rlog.debug("studyrights", "Size of Array List Rights " + sLength); for
     * (counter = 0; counter <= (sLength - 1); counter++) { rights +=
     * studyRht.get(counter).toString(); } Rlog.debug("studyrights", "String of
     * Rights " + rights); StudyRightsRObj study = studyRightsHome.create(ssk,
     * rights); Rlog.debug("studyrights", "Creation of Group Rights Home " +
     * study); } catch (Exception e) { Rlog.fatal("studyrights", "EXCEPTION" +
     * e); e.printStackTrace(); } return 0; }
     */
    /**
     * Updates Study Rights.
     * 
     * @param Study
     *            User Rights state holder containing Rights attributes to be
     *            set.
     * @return int
     */

    public int updateStudyRights(StudyRightsBean ssk) {
        int stdTeamId = 0;
        int counter = 0;
        int sLength = 0;
        int result = 0;
        ArrayList studyRht = new ArrayList();
        StudyRightsBean studyRights = new StudyRightsBean();
        String rights = "";
        try {

            studyRights = (StudyRightsBean) em.find(StudyRightsBean.class,
                    new Integer(ssk.getId()));

            studyRht = ssk.getFtrRights();
            Rlog.debug("studyrights", "Study Rights from State Keeper "
                    + studyRht);
            sLength = studyRht.size();
            Rlog.debug("studyrights", "Size of Array List Rights " + sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += studyRht.get(counter).toString();
            }
            Rlog.debug("studyrights", "String of Rights " + rights);
            result = studyRights.updateStudyRights(ssk, rights);
            Rlog.debug("studyrights", "Updation of Study Rights" + result);
        } catch (Exception e) {
            Rlog.fatal("studyrights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2;
        }
        return 0;
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Looks up on the id parameter passed in and constructs and returns a state
     * holder. containing the details of the Study Rights<br>
     * 
     * @param id
     */
    public StudyRightsBean getGrpSupUserStudyRightsDetails(int id) {

        StudyRightsBean retrieved = new StudyRightsBean(); // Study Rights
                                                            // Entity Bean
                                                            // Remote
        StudyGroupRights group = null;
        // Object

        String studyRight = null;
        CtrlDao cntrl;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        try {
            group = (StudyGroupRights) em.find(StudyGroupRights.class,
                    new Integer(id));

            studyRight = group.getTeamRights(); // Getting the Rights
            // assigned to the study on
            // each feature
            // from Entity Bean
            sLength = studyRight.length();

            for (counter = 0; counter <= (sLength - 1); counter++) {
                retrieved.setFtrRights(String.valueOf(studyRight
                        .charAt(counter)));
                retrieved.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("study_rights");
            cVal = cntrl.getCValue();
            cDesc = cntrl.getCDesc();
            retrieved.setGrValue(cVal);
            retrieved.setGrDesc(cDesc);

            retrieved.setId(group.getId());
            retrieved.setTeamRights(group.getTeamRights());
            retrieved.setCreator(group.getCreator());
            retrieved.setModifiedBy(group.getModifiedBy());
            retrieved.setIpAdd(group.getIpAdd());

        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("studyrights", "EXCEPTION" + e);
        }

        return retrieved;
    }

    // ///////////////

    /**
     * Sets Group Super UserStudy Rights.
     * 
     * @param Study
     *            Rights state holder containing Rights attributes to be set.
     * @return Void
     */

    /*
     * public int setGrpSupUserStudyRightsDetails(StudyRightsBean ssk){ int
     * stdTeamId = 0; int counter = 0; int sLength = 0; ArrayList studyRht = new
     * ArrayList(); String rights = "";
     * 
     * try {
     * 
     * StudyRightsHome studyRightsHome = EJBUtil.getGrpSupUserHome();
     * 
     * Enumeration enum_velos = studyRightsHome.findByStudyTeamId(ssk
     * .getStudyTeamId());
     * 
     * if (enum_velos.hasMoreElements()) return -1; // duplicate Study Team ids
     * are not allowed
     * 
     * studyRht = ssk.getFtrRights();
     * 
     * sLength = studyRht.size();
     * 
     * for (counter = 0; counter <= (sLength - 1); counter++) { rights +=
     * studyRht.get(counter).toString(); }
     * 
     * Rlog.debug("studyrights", "String of Rights " + rights);
     * 
     * StudyRightsRObj study = studyRightsHome.create(ssk, rights);
     * Rlog.debug("studyrights", "Creation of Group Rights Home " + study); }
     * catch (Exception e) { Rlog.fatal("studyrights", "EXCEPTION" + e);
     * e.printStackTrace(); } return 0; }
     */
    /**
     * Updates Study Rights.
     * 
     * @param Study
     *            User Rights state holder containing Rights attributes to be
     *            set.
     * @return int
     */

    public int updateGrpSupUserStudyRights(StudyRightsBean ssk) {
        int stdTeamId = 0;
        int counter = 0;
        int sLength = 0;
        int result = 0;
        ArrayList studyRht = new ArrayList();

        GroupDao gdao = new GroupDao();

        String rights = "";
        StudyGroupRights group = new StudyGroupRights();

        try {
            StudyRightsBean studyRights = null;

            group = (StudyGroupRights) em.find(StudyGroupRights.class,
                    new Integer(ssk.getId()));

            studyRht = ssk.getFtrRights();

            sLength = studyRht.size();

            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += studyRht.get(counter).toString();
            }

            result = group.updateStudyRights(ssk, rights);

            //commented by Sonia, 10/22/07 changed super user implementation
            
            /*result = gdao.updateSupUserRights(ssk.getId(), ssk.getModifiedBy(),
                    ssk.getIpAdd(), rights);*/
            
            // update study rights for all users of the super user group

            Rlog.debug("studyrights", "Updation of Study Rights" + result);
        } catch (Exception e) {
            Rlog.fatal("studyrights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2;
        }
        return result;
    }

    /**
     * Gets Study Rights for a module.
     * 
     * @param studyId int
     * @param userId int
     * @param moduleName String
     * @return result int
     */

    public int getStudyRightsForModule(int studyId, int userId, String moduleName) {
        int result = 0;
        ArrayList teamId ;
    	teamId = new ArrayList();
    	TeamDao teamDao = new TeamDao();
    	teamDao.getTeamRights(studyId, userId);
    	teamId = teamDao.getTeamIds();

    	if (teamId.size() <= 0) { 
    		result=0 ;
    	}else {
    		StudyRightsJB stdRightsB = new StudyRightsJB();
    		stdRightsB.setId(StringUtil.stringToNum(teamId.get(0).toString()));
    		 
			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRightsB.loadStudyRights();

    		if ((stdRightsB.getFtrRights().size()) == 0){
    			result = 0;		
    		}else{
    			result = Integer.parseInt(stdRightsB.getFtrRightsByValue(moduleName));			
    		}		
    	}
        return result;
    }

}// end of class

