package com.velos.eres.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Class to hold utility methods used in the 'velos-base' classes.
 * These methods were copied from EJBUtil in order to remove dependencies
 * of velos-base on the EJB packages.
 * 
 * @author Isaac Huang
 *
 */
public class EnvUtil {
	
    public static String getEnvVariable(String Name) throws Exception {
        String envValue = "";
        int sType = 0;
        Process theProcess = null;
        BufferedReader stdInput = null;
        try {
            // Figure out what kind of system we are running on
            // 0 = windows
            // 1 = unix
            sType = getSystemType();
            if (sType == 0) {
                String envPath = "cmd /C echo %" + Name + "%";
                theProcess = Runtime.getRuntime().exec(envPath);
            } else {
                theProcess = Runtime.getRuntime().exec("printenv " + Name);
            }

            stdInput = new BufferedReader(
                new InputStreamReader(theProcess.getInputStream())
            );

            envValue = stdInput.readLine();
            stdInput.close();
            theProcess.destroy();
            return envValue;
        } catch (IOException ioe) {
            throw new Exception(ioe.getMessage());
        }
    }

    public static int getSystemType() {
        Properties p = System.getProperties();
        String os = p.getProperty("os.name");
        int osType = 0;
        if (os.indexOf("Win") != -1) {
            osType = 0;
        } else {
            osType = 1;
        }
        return osType;
    }

    /**
    *
    *
    * if the appServerParam are set in configuration class then return it,
    * otherwise read
    *
    *
    * it from xml file and store in appServerParam then return.
    *
    *
    */
   public static Hashtable getContextProp() {
      try {
          if (Configuration.getServerParam() == null)
              Configuration.readSettings();
      } catch (Exception e) {
          Rlog.fatal("common", "EnvUtil:getContextProp:general ex" + e);
      }
      return Configuration.getServerParam();
   }
   
   /** *********GET CONNECTION FROM RESOURCE********* */
   public static Connection getConnection() {
       InitialContext iCtx = null;
       Connection conn = null;
       Configuration conf = null;
       try {
           iCtx = new InitialContext(getContextProp());
           // Rlog.debug("common","INITIAL CONTECXT:" + iCtx +"conf Path"
           // +JNDINames.ERESEARCH_CONF_PATH);
           if (Configuration.dsJndiName == null) {
               Configuration.readSettings();
           }

           Rlog
                   .debug("common", "JNDI NAME DS NAME"
                           + Configuration.dsJndiName);
           DataSource ds = (DataSource) iCtx.lookup(Configuration.dsJndiName);
           Rlog.debug("common", "DATASOURCE " + ds);
           return ds.getConnection();
       } catch (NamingException ne) {
           Rlog.fatal("common", ":getConnection():Naming exception:" + ne);
           return null;
       } catch (Exception se) {
           Rlog.fatal("common", ":getConnection():Sql exception:" + se);
           return null;
       } finally {
           try {
               if (iCtx != null)
                   iCtx.close();
           } catch (NamingException ne) {
               Rlog.fatal("common",
                       ":getConnection():context close exception:" + ne);
           }
       }
   }

}
