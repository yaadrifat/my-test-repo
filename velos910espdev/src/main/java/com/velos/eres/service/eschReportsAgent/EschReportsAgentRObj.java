/*
 * Classname			EschReportsAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					07/16/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.eschReportsAgent;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;
import javax.ejb.Remote;

import com.velos.eres.business.common.EschCostReportDao;
import com.velos.eres.business.common.EschReportsDao;

/**
 * Remote interface for REportsAgentBean session EJB
 * 
 * @author sajal
 * @version 1.0, 05/12/2001
 */
@Remote
public interface EschReportsAgentRObj extends EJBObject {
    public EschReportsDao fetchEschReportsData(int reportNumber,
            String filterType, String year, String month, String dateFrom,
            String dateTo, String id, String accId) throws RemoteException;

    public EschCostReportDao getCostReport(int reportNumber, int protocol_id,
            int orderBy) throws RemoteException;

}