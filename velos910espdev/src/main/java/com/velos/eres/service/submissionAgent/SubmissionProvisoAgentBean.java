package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.patTXArm.impl.PatTXArmBean;
import com.velos.eres.business.submission.impl.SubmissionProvisoBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { SubmissionProvisoAgent.class } )
public class SubmissionProvisoAgentBean implements SubmissionProvisoAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    public SubmissionProvisoBean getSubmissionProvisoDetails(int id) {
        // TODO Auto-generated method stub
        try {
            return (SubmissionProvisoBean) em.find(SubmissionProvisoBean.class, new Integer(
                    id));

        } catch (Exception e) {
            Rlog.fatal("submission",
                    "Exception in getSubmissionProvisoDetails() in SubmissionProvisoAgentBean"
                            + e);
            return null;
        }

    }

    public Integer createSubmissionProviso(SubmissionProvisoBean submissionPBean) {
        Integer output = 0;
        try {
            SubmissionProvisoBean newBean = new SubmissionProvisoBean();
            newBean.setDetails(submissionPBean);
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionProvisoAgentBean.createSubmissionProviso "+e);
            output = -1;
        }
        return output;
    }
    
    public int updateSubmissionProviso(SubmissionProvisoBean ptk) {

    	SubmissionProvisoBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = em.find(SubmissionProvisoBean.class, ptk.getId());
            if (retrieved == null) {
                return -2;
            }
             retrieved.setDetails(ptk);

        } catch (Exception e) {
            Rlog.fatal("submission",
                    "Exception in updateSubmissionProviso in PatTXArmAgentBean" + e);
            return -2;
        }
        return 0;
    }

    public int removeSubmissionProviso(int provisoPK) {
        int output;
        SubmissionProvisoBean patPRObj = null;

        try {

            patPRObj = (SubmissionProvisoBean) em.find(SubmissionProvisoBean.class,
                    new Integer(provisoPK));
            em.remove(patPRObj);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in removeSubmissionProviso" + e);
            return -1;

        }

    }
 // Overloaded for INF-18183 ::: AGodara
    public int removeSubmissionProviso(int provisoPK,Hashtable<String, String> auditInfo) {
        int output;
        SubmissionProvisoBean patPRObj = null;

        try {
        	AuditBean audit=null;
         
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE); //Fetches the reason for deletion from the Hashtable
            String rid= AuditUtils.getRidValue("ER_SUBMISSION_PROVISO","eres","PK_SUBMISSION_PROVISO="+provisoPK); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_SUBMISSION_PROVISO",String.valueOf(provisoPK),rid,userID,currdate,app_module,ipAdd,reason);
        	patPRObj = (SubmissionProvisoBean) em.find(SubmissionProvisoBean.class,
                    new Integer(provisoPK));
            
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(patPRObj);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("submission", "removeSubmissionProviso(int provisoPK,Hashtable<String, String> auditInfo)" + e);
            return -1;
        }
    }
    
    
    /** Returns Provisos added for a submission and Submission board */
    public ArrayList getSubmissionProvisos(int fkSubmission, int fkSubmissionBoard )
    {
    	ArrayList arReturn = new ArrayList();
        try {
        	arReturn = EIRBDao.getSubmissionProvisos(fkSubmission, fkSubmissionBoard);
        		
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in getSubmissionProvisos" + e);
            

        }

    	return arReturn;
    }
    
}
