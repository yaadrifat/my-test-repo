/*


 * Classname			GenerateId


 * 


 * Version information  1.0


 *


 * Date					07/03/2000


 * 


 * Copyright notice		Velos Inc.


 * 


 * Author 				From eClinical


 */

/** * $Id: GenerateId.java,v 1.4 2006/10/09 19:46:47 vabrol Exp $


 * Copyright 2000 Velos, Inc. All rights reserved.


 */

package com.velos.eres.service.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// import com.velos.eres.service.util.Debug;

/**
 * 
 * 
 * Generates the id from database sequence
 * 
 * 
 * 
 * 
 * 
 * @author Dharani Babu
 * 
 * 
 * @version 1.0, 07/03/2000
 * 
 * 
 */

public class GenerateId {

    public static int getId(String seqName, Connection conn)
            throws SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        try {

            stmt = conn.createStatement();

            // Debug.println("select "+seqName+".nextval from dual");
            Rlog.debug("common", "select " + seqName + ".nextval from dual");

            rs = stmt.executeQuery("select " + seqName + ".nextval from dual");

            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {

            Rlog.fatal("common", "Error in GenerateId:getId:" + e.getMessage());
            e.printStackTrace();
            return -1;

        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
        }
    }

}
