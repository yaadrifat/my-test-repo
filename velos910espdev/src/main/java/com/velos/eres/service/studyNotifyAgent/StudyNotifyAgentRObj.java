/*
 * Classname			StudyNotifyAgentRObj.class
 * 
 * Version information   
 *
 * Date					03/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyNotifyAgent;

import javax.ejb.Remote;

import com.velos.eres.business.studyNotify.impl.StudyNotifyBean;

/**
 * Remote interface for StudyNotifyAgent session EJB
 * 
 * @author sajal
 */
@Remote
public interface StudyNotifyAgentRObj {
    /**
     * gets the studyNotify details
     */
    StudyNotifyBean getStudyNotifyDetails(int studyNotifyId);

    /**
     * sets the studyNotify details
     */
    public int setStudyNotifyDetails(StudyNotifyBean snsk);

    public int updateStudyNotify(StudyNotifyBean snsk);
    
    // notify subscribers
    public int notifySubscribers(int studyPK);
}
