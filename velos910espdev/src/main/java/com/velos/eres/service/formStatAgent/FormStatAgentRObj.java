/**
 * This is the Remote Object For the Session bean for FormStatAgentBean
 * @author Salil
 * Date 07/29/2003
 **/

package com.velos.eres.service.formStatAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.common.FormStatusDao;
import com.velos.eres.business.formStat.impl.FormStatBean;

/* End of Import Statements */

/**
 * Functions of the Session Bean that could be called
 * 
 */
@Remote
public interface FormStatAgentRObj {

    /**
     * This function gets the details of a form and returns a StateKeeper Object
     * 
     */
    public FormStatBean getFormStatDetails(int fromStatId);

    /**
     * This function updates the details of a form and takes an statekeeper
     * object as a parameter and returns 0 on success -2 on error
     */
    public int updateFormStatDetails(FormStatBean fssk);

    /**
     * This function sets the details of a form and takes an statekeeper object
     * as a parameter and returns the pk of the record created on success and 0
     * on error
     */
    public int setFormStatDetails(FormStatBean fssk);

    /**
     * This function gets the details of the status of a form
     * 
     * @param formId @
     *            return FormStatusDao
     */
    public FormStatusDao getFormStatusHistory(int formId);

}