/*
 * Classname : FormFieldAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formFieldAgent;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.formField.impl.FormFieldBean;

/* End of Import Statements */

/**
 * Remote interface for FormFieldAgent session EJB
 * 
 * @author SoniaKaura
 * @version : 1.0 17/07/2003
 */
@Remote
public interface FormFieldAgentRObj {
    /**
     * gets the Form Field record
     */
    FormFieldBean getFormFieldDetails(int formFieldId);

    /**
     * creates a new Form Field Record
     */
    public int setFormFieldDetails(FormFieldBean formFldsk);

    /**
     * updates the Form Field Record
     */
    public int updateFormField(FormFieldBean formFldsk);
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateFormField(FormFieldBean formFldsk, Hashtable<String, String> auditInfo);
    
    /**
     * remove Form Field Record
     */
    public int removeFormField(int formFieldId);

    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy);
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy, Hashtable<String, String> auditInfo);

    public int repeatLookupField(int pkField, int formField, int section);

}
