/*
 * Classname : UsrSiteGrpAgentBean
 * 
 * Version information: 1.1
 *
 * Date: 01/27/2012
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Yogesh
*/

package com.velos.eres.service.usrSiteGrpAgent.impl;

/* Import Statements */
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.UserSiteGroupDao;
import com.velos.eres.business.usrSiteGrp.impl.UsrSiteGrpBean;
import com.velos.eres.service.usrSiteGrpAgent.UsrSiteGrpAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP - UsrGrpBean.<br>
 * <br>
 * 
 * @author sajal
 * @see UsrGrpBean
 * @version 1.0 03/12/2001
 * @ejbHome UsrGrpAgentHome
 * @ejbRemote UsrGrpAgentRObj
 */
@Stateless
public class UsrSiteGrpAgentBean implements UsrSiteGrpAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /**
     * 
     */

    /**
     * 
     * Looks up on the UsrGrp id parameter passed in and constructs and returns
     * a UsrGrp state keeper. containing all the details of the usrGrp<br>
     * 
     * @param usrGrpId
     *            the UsrGrp id
     */
    public UsrSiteGrpBean getUsrSiteGrpDetails(int userSiteGrp) {
        UsrSiteGrpBean toreturn = null;
        try {
            return (UsrSiteGrpBean) em
                    .find(UsrSiteGrpBean.class, new Integer(userSiteGrp));

        } catch (Exception e) {
            Rlog.fatal("usrsitegrp",
                    "Error in getUsrSiteGrpDetails() in UsrSiteGrpAgentBean" + e);
        }
        Rlog.debug("usrsitegrp",
                "Returning from UsrGSiterpAgentBean.getUsrSiteGrpDetails UsrSiteGrpStateKeeper "
                        + toreturn);
        return toreturn;
    }

     /**
     * Updates a UsrGrp record.
     * 
     * @param an
     *            usrGrp state keeper containing usrGrp attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateUsrSiteGrp(int userid, String[] userpkSite,String[] fkGroupIds,String[] pkUserSiteGrp,String ipAdd,String creator) {
        int output=0;

        	UserSiteGroupDao userSiteGroupDao = new UserSiteGroupDao();
             try {
            	 output = userSiteGroupDao.updateUsrSiteGrp(userid,userpkSite, fkGroupIds,pkUserSiteGrp,ipAdd,creator);
                 return output;

        } catch (Exception e) {
            Rlog
                    .debug("usrsitegrp", "Error in updateUsrSiteGrp in UsrSiteGrpAgentBean"
                            + e);
            return -2;
        }

    }
    
    
    public UserSiteGroupDao getUsrSiteGrp(int userid) {
        	UserSiteGroupDao userSiteGroupDao = new UserSiteGroupDao();
             try {
            	 userSiteGroupDao.getUsrSiteGrp(userid);
                 return userSiteGroupDao;

        } catch (Exception e) {
            Rlog
                    .debug("usrsitegrp", "Error in getUsrSiteGrp in UsrSiteGrpAgentBean"
                            + e);
            return userSiteGroupDao;
        }

    }
}// end of class

