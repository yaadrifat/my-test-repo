/*
 * Classname			UserSiteAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					04/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSiteAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.userSite.impl.UserSiteBean;

/**
 * Remote interface for UserSiteAgent session EJB
 * 
 * @author Sajal
 */
@Remote
public interface UserSiteAgentRObj {
    /**
     * gets the site details
     */
    UserSiteBean getUserSiteDetails(int userSiteId);

    /**
     * sets the site details
     */
    public int setUserSiteDetails(UserSiteBean ussk);

    public int updateUserSite(UserSiteBean ussk);

    public int updateUserSite(String[] pkUserSites, String[] rights, int usr,
            String ipAdd);
    public int updateUserSite(String[] pkUserSites, String[] rights, int usr,int userID,
    		String ipAdd);

    public UserSiteDao getUserSiteTree(int accountId, int loginUser);

    public UserSiteDao getSitesWithViewRight(int accId, int userId);

    public UserSiteDao getAllSitesWithViewRight(int accId, int userId);

    public UserSiteDao getSitesWithRightForStudy(int studyId, int accId,
            int userId);

    public UserSiteDao getSitesWithNewRight(int accId, int userId);

    public int createUserSiteData(int user, int account, int def_group,
            int old_site, int def_site, int creator, String ip, String mode);

    public int getRightForUserSite(int userId, int siteId);

    public UserSiteDao getSitesToEnrollPat(int studyId, int accId, int userId);
    
    /** Returns the maximum right the user has on any of patient facilities. User's user site access rights are also
     * checked for all the patient facilities*/
    public int getUserPatientFacilityRight(int user , int perPk) ; 
    
    /**
     *  Function to get user's right in  patient's registering sites for a given study patient enrollments
     *  
     * @param int
     *            userId - User Id
     * @param int
     *            patprot -patprot id
     */
    
    public int getMaxRightForStudyPatient(int user, int pat, int study) ;
    
    //JM: 032406 
    public UserSiteDao getAllSitesInAccount(int accId);

}
