/*
 * Classname			FldRespAgentBean
 *
 * Version information :
 *
 * Date					07/02/2003
 *
 * Copyright notice :   Velos Inc
 */

package com.velos.eres.service.fldRespAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.fldResp.impl.FldRespBean;
import com.velos.eres.service.fldRespAgent.FldRespAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 *
 * @author Anu Khanna
 * @version 1.0, 07/02/2003
 * @ejbHome FldRespAgentHome
 * @ejbRemote FldRespAgentRObj
 */

@Stateless
public class FldRespAgentBean implements FldRespAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Looks up on the field id parameter passed in and constructs and returns a
     * FldRespStateKeeper containing all the summary details of the field
     *
     * @param fldRespId
     *            The Field id
     * @see FldRespStateKeeper
     */
    public FldRespBean getFldRespDetails(int fldRespId) {
        FldRespBean retrieved = null; // field Response Entity Bean Remote
        // Object
        try {
            retrieved = (FldRespBean) em.find(FldRespBean.class, new Integer(
                    fldRespId));
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in getFldRespDetails() in FldRespAgentBean" + e);
        }

        return retrieved;
    }

    /**
     * Add the field details in the FldRespStateKeeper Object to the database
     *
     * @param fldresp
     *            An FldRespStateKeeper containing field attributes to be set.
     * @return The field Id for the field just created <br>
     *         0 - if the method fails
     */

    public int setFldRespDetails(FldRespBean fldResp) {
        try {
            FldRespBean fb = new FldRespBean();

            fb.updateFldResp(fldResp);
            em.persist(fb);
            return (fb.getFldRespId());
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in setFldRespDetails() in FldRespAgentBean " + e);
        }
        return 0;
    }

    /**
     * Update the field details contained in the FldRespStateKeeper Object to
     * the database
     *
     * @param frsk
     *            An FldRespStateKeeper containing field attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */
    public int updateFldResp(FldRespBean frsk) {

        FldRespBean retrieved = null; // User Entity Bean Remote Object
        int output;

        try {
            retrieved = (FldRespBean) em.find(FldRespBean.class, new Integer(
                    frsk.getFldRespId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFldResp(frsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.debug("fldresp", "Error in updateFldResp in FldRespAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public FldRespDao getResponsesForField(int field) {
        try {
            Rlog.debug("fldresp",
                    "In getResponsesForField in FldRespAgentBean - 0");
            FldRespDao fldRespDao = new FldRespDao();
            fldRespDao.getResponsesForField(field);
            return fldRespDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In getResponsesForField in FldRespAgentBean "
                            + e);
        }
        return null;

    }

    public int insertMultipleResponses(FldRespDao fldRespDao) {
        try {
            Rlog.debug("fldresp",
                    "In insertMultipleResponses in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();

            fldRDao.insertMultipleResponses(fldRespDao);

        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In insertMultipleResponses in FldRespAgentBean "
                            + e);
            return -2;
        }

        return 1;
    }

    public int updateMultipleResponses(FldRespDao fldRespDao) {
        try {
            Rlog.debug("fldresp",
                    "In updateMultipleResponses in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();

            fldRDao.updateMultipleResponses(fldRespDao);

        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In updateMultipleResponses in FldRespAgentBean "
                            + e);
            return -2;
        }

        return 1;
    }

    public FldRespDao getMaxSeqNumForMulChoiceFld(int formfldId) {
        try {
            Rlog.debug("fldresp",
                    "In getMaxSeqNumForMulChoiceFld in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();
            fldRDao.getMaxSeqNumForMulChoiceFld(formfldId);
            return fldRDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In getMaxSeqNumForMulChoiceFld in FldRespAgentBean "
                            + e);

        }

        return null;

    }

    public FldRespDao getMaxRespSeqForFldInLib(int fldId) {
        try {
            Rlog.debug("fldresp",
                    "In getMaxRespSeqForFldInLib in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();
            fldRDao.getMaxRespSeqForFldInLib(fldId);
            return fldRDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In getMaxRespSeqForFldInLib in FldRespAgentBean "
                            + e);

        }

        return null;

    }


  //JM: 24Jun2008, added for the issue #3309 Need to delete record physically from ER_FLDRESP on deletion.

    public int deleteFldResp(int fldRespId){
    	int retme = 0;
        try {
            Rlog.debug("fldresp",
                    "In deleteFldResp() in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();

            retme = fldRDao.deleteFldResp(fldRespId);

        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Exception In fldRespId() in FldRespAgentBean "
                            + e);
            return -2;
        }

        return retme;
    }
    
    // deleteFldResp() Overloaded for INF-18183 ::: Raviesh
    public int deleteFldResp(int fldRespId,Hashtable<String, String> args){
    	int retme = 0;
        try {
        	AuditBean audit=null; //Audit Bean for AppDelete
            String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
            String getRidValue= AuditUtils.getRidValue("ER_FLDRESP","eres","PK_FLDRESP="+fldRespId);/*Fetches the RID/PK_VALUE*/ 
        	audit = new AuditBean("ER_FLDRESP",String.valueOf(fldRespId),getRidValue,
        			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/ 
            Rlog.debug("fldresp",
                    "In deleteFldResp() in FldRespAgentBean - 0");
            FldRespDao fldRDao = new FldRespDao();

            retme = fldRDao.deleteFldResp(fldRespId);
            if(retme!=1){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 1 else, rollback */

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("fldresp",
                    "Exception In fldRespId() in FldRespAgentBean "
                            + e);
            return -2;
        }

        return retme;
    }



}// end of class

