/*
 * Classname			ULinkRObj.class
 * 
 * Version information   
 *
 * Date					03/15/2001
 * 
 * Copyright notice
 */
package com.velos.eres.service.ulinkAgent;
import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.business.ulink.impl.ULinkBean;
/**
 * 
 * 
 * Remote interface for ULinkAgent session EJB
 * 
 * 
 * @author dinesh
 * 
 * 
 */
@Remote
public interface ULinkAgentRObj {
    /**
     * 
     * 
     * gets the user links details
     * 
     * 
     */
    ULinkBean getULinkDetails(int lnkId);
    /**
     * 
     * 
     * sets the user links details
     * 
     * 
     */
    public int setULinkDetails(ULinkBean ulink);
    public int updateULink(ULinkBean ulink);
    public UsrLinkDao getULinkValuesByAccountId(int accountId, String accType);
    public UsrLinkDao getULinkValuesByUserId(int userId);
    public int deleteULink(ULinkBean ulink);
    // Overloaded for INF-18183 -- AGodara
    public int deleteULink(ULinkBean ulink,Hashtable<String,String> agrs);
}
