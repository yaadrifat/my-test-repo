
package com.velos.eres.service.formActionAgent.impl;

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.formAction.impl.FormActionBean;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.service.formActionAgent.FormActionAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author sajal
 * @version 1.0, 02/20/2001
 * @ejbHome AccountAgentHome
 * @ejbRemote AccountAgentRObj
 */
@Stateless
public class FormActionAgentBean implements FormActionAgentRObj { 

     
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    private static final long serialVersionUID = 3258135756097925680L;

    private SessionContext ctx;

    
    public FormActionBean getFormActionDetails(Integer formActionId) {

        FormActionBean faBean = null;

        try {
            faBean = em.find(FormActionBean.class, formActionId);
            return faBean;
        } catch (Exception e) {
            Rlog.fatal("formAction",
                    "Error in getFormActionDetails() in FormActionAgentBean" + e);
            return faBean;
        }

    }

    /**
     * Add the Account details in the AccountStateKeeper Object to the database
     * 
     * @param account
     *            An AccountStateKeeper containing account attributes to be set.
     * @return The Account Id for the account just created <br>
     *         0 - if the method fails
     */

    public int setFormActionDetails(FormActionBean formAction) {
        try {
        	FormActionBean fa = new FormActionBean();

            fa.updateFormAction(formAction);
            em.persist(fa);

            //return (fa.getFormActionId());
            return 1;
        } catch (Exception e) {
            Rlog.fatal("formAction",
                    "Error in setFormActionDetails() in FormActionAgentBean " + e);
            return 0;
        }

    } 
    
    public int updateFormAction(FormActionBean fask) {

        int output;
        FormActionBean faBean = null;

        try {

            faBean = em.find(FormActionBean.class, new Integer(fask.getFormActionId()));
            output = faBean.updateFormAction(fask);
            em.merge(faBean);
        } catch (Exception e) {
            Rlog.debug("formAction", "Error in updateFormAction in FormActionAgentBean"
                    + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

// 061008ML for delete form action
    
    public int removeFormAction(int formActionId) {
        int output;
        FormActionBean retrieved = null;

        try {

            Rlog.debug("formaction",
                    "in try bloc of FormAction agent:removeFormAction");

            retrieved = (FormActionBean) em.find(FormActionBean.class,
                    new Integer(formActionId));
            em.remove(retrieved);
            Rlog.debug("formaction",
                    "Removed FormAction row with FormActionID = "
                            + formActionId);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formaction", "Exception in removeFormAction " + e);
            return -1;

        }

    }
// end of 061008ML

    // Overloaded for INF-18183 ::: AGodara
    public int removeFormAction(int formActionId,Hashtable<String, String> auditInfo) {
        
        FormActionBean retrieved = null;
        AuditBean audit=null;
        
        try {
            
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String rid= AuditUtils.getRidValue("ER_FORMACTION","eres","PK_FORMACTION="+formActionId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_FORMACTION",String.valueOf(formActionId),rid,userID,currdate,"Form Management",ipAdd,reason);
           	retrieved = (FormActionBean) em.find(FormActionBean.class,
                    new Integer(formActionId));
        	
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE 
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("formaction", "removeFormAction(int formActionId,Hashtable<String, String> auditInfo) " + e);
            return -1;
        }
    }
    
    
}// end of class

