/*
 * Classname			SavedRepAgentRObj.class
 * 
 * Version information   
 *
 * Date			05/30/2002
 * 
 * Copyright notice
 */

package com.velos.eres.service.savedRepAgent;

import java.rmi.RemoteException;

import javax.ejb.Remote;

import com.velos.eres.business.common.SavedRepDao;
import com.velos.eres.business.savedRep.impl.SavedRepBean;

/**
 * Remote interface for SavedRepAgent session EJB
 * 
 * @author Arvind
 */
@Remote
public interface SavedRepAgentRObj {
    /**
     * gets the savedRep details
     */
    SavedRepBean getSavedRepDetails(int savedRepId) throws RemoteException;

    /**
     * sets the savedRep details
     */
    public int setSavedRepDetails(SavedRepBean srsk);

    public int updateSavedRep(SavedRepBean srsk);

    public SavedRepDao getAllReps(int studyId);

    public SavedRepDao getReportContent(int savedRepId);

    int removeSavedRep(int id);

    public SavedRepDao validateReportName(int studyId, String savedRepName);

}
