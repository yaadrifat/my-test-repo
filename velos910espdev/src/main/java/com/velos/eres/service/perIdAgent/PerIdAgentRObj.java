/*
 * Classname : PerIdAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 16/02/04
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: 
 */

package com.velos.eres.service.perIdAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.common.PerIdDao;
import com.velos.eres.business.perId.impl.PerIdBean;

/* End of Import Statements */

/**
 * Remote interface for PerIdAgent session EJB
 * 
 * @author Anu Khanna
 * @version : 1.0 16/02/04
 */
@Remote
public interface PerIdAgentRObj {
    /**
     * gets the PerId record
     */
    PerIdBean getPerIdDetails(int id);

    /**
     * creates a new PerId Record
     */
    public int setPerIdDetails(PerIdBean psk);

    /**
     * updates the PerId Record
     */
    public int updatePerId(PerIdBean psk);

    /**
     * remove PerId Record
     */
    public int removePerId(int id);

    /**
     * gets per ids
     * 
     */

    public PerIdDao getPerIds(int perId,String defUserGroup);

    /**
     * create multiple per ids
     * 
     */

    public int createMultiplePerIds(PerIdBean psk);

}
