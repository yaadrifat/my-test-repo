/*
 * Classname				AccountWrapperAgentBean
 * 
 * Version information		1.0
 *
 * Date						03/14/2001
 * 
 * Copyright notice			Velos Inc
 *
 * Author					Sajal
 */

package com.velos.eres.service.accountWrapperAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.service.accountWrapperAgent.AccountWrapperAgentRObj;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a way to pass user inputs to state
 * keepers of different tables. <br>
 * <br>
 * 
 * @author sajal
 * @version 1.0, 03/14/2001
 */
@Stateless
public class AccountWrapperAgentBean implements AccountWrapperAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    private static final long serialVersionUID = 3616445717804298546L;

}// end of class

