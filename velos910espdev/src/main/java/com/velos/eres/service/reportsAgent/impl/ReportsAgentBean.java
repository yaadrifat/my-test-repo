/*
 * Classname			ReportsAgentBean
 * 
 * Version information : 1.0
 *
 * Date					05/12/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.reportsAgent.impl;

/* IMPORT STATEMENTS */
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.service.reportsAgent.ReportsAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.reports.ReportsDao;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the ReportsDao.
 * 
 * @author sajal
 * @version 1.0, 05/12/2001
 * @ejbHome ReportsAgentHome
 * @ejbRemote ReportsAgentRObj
 */

@Stateless
public class ReportsAgentBean implements ReportsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public ReportsDao fetchReportsData(int reportNumber, String filterType,
            String year, String month, String dateFrom, String dateTo,
            String id, String accId) {
        try {
            Rlog.debug("reports", "ReportsAgentBean.fetchReportsData starting");
            ReportsDao reportsDao = new ReportsDao();
            Rlog.debug("reports", "ReportsAgentBean.fetchReportsData middle");
            reportsDao.fetchReportsData(reportNumber, filterType, year, month,
                    dateFrom, dateTo, id, accId);
            Rlog.debug("reports", "ReportsAgentBean.fetchReportsData end");
            return reportsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("reports", "Error in fetchReportsData in ReportsJB "
                            + e);
        }
        return null;
    }

}// end of class

