package com.velos.eres.service.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class VelosLoginInterceptor extends AbstractInterceptor implements StrutsStatics {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		final ActionContext context = invocation.getInvocationContext().getContext();
		HttpServletRequest request = (HttpServletRequest)context.get(HTTP_REQUEST);
		HttpSession session = request.getSession(true);
		Integer userNumId = 0;
		String userId = (String) session.getAttribute("userId");
		if (userId == null) {
			return "sessionTimeOut";
		}
		
		try {
		 userNumId = Integer.parseInt(userId);
		} catch(NumberFormatException e){
			e.printStackTrace();
		}

        // if valid user return true else false
		if (userNumId > 0) {
            return invocation.invoke(); 
        } else {
            return "sessionTimeOut";
        }	
	}
}
