package com.velos.eres.tags;

import com.velos.eres.business.common.Style;

public class VLinkForm extends VTag {

    private String displayText;

    private Style displayStyle;

    private String linkedForm;

    private String formDisplayType;

    private String formEntryType;

    // /__astcwz_default_constructor
    public VLinkForm() {
        this.displayText = "";
        this.displayStyle = new Style();
        this.linkedForm = "";
        this.formDisplayType = "";
        this.formEntryType = "";

    }

    public VLinkForm(String displayText) {
        this.displayText = displayText;
        this.displayStyle = new Style();
        this.linkedForm = "";
        this.formDisplayType = "";
        this.formEntryType = "";
    }

    public VLinkForm(String displayText, Style displayStyle, String linkedForm) {
        this.displayText = displayText;
        this.displayStyle = displayStyle;
        this.linkedForm = linkedForm;
        this.formDisplayType = "";
        this.formEntryType = "";
    }

    public Style getDisplayStyle() {
        return this.displayStyle;
    }

    public void setDisplayStyle(Style displayStyle) {
        this.displayStyle = displayStyle;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getDisplayText() {
        return displayText;
    }

    /**
     * Returns the value of linkedForm.
     */
    public String getLinkedForm() {
        return linkedForm;
    }

    /**
     * Sets the value of linkedForm.
     * 
     * @param linkedForm
     *            The value to assign linkedForm.
     */
    public void setLinkedForm(String linkedForm) {
        this.linkedForm = linkedForm;
    }

    /**
     * Returns the value of formDisplayType.
     */
    public String getFormDisplayType() {
        return formDisplayType;
    }

    /**
     * Sets the value of formDisplayType.
     * 
     * @param formDisplayType
     *            The value to assign formDisplayType.
     */
    public void setFormDisplayType(String formDisplayType) {
        this.formDisplayType = formDisplayType;
    }

    /**
     * Returns the value of formEntryType.
     */
    public String getFormEntryType() {
        return formEntryType;
    }

    /**
     * Sets the value of formEntryType.
     * 
     * @param formEntryType
     *            The value to assign formEntryType.
     */
    public void setFormEntryType(String formEntryType) {
        this.formEntryType = formEntryType;
    }

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();

        xslStr.append("<td><A ");
        xslStr.append(" href= \"Javascript:void(0)\" ");
        xslStr.append(" onClick=\"return openlinkedForm(" + this.linkedForm
                + ",'" + this.formEntryType.trim() + "','"
                + this.formDisplayType.trim() + "',document.er_fillform1)\"");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        xslStr.append(getToolTip());
        xslStr.append(getId());
        xslStr.append(" >");

        xslStr.append((getDisplayStyle()).getDisplayStartTags());
        xslStr.append(getDisplayText());
        xslStr.append((getDisplayStyle()).getDisplayEndTags());

        xslStr.append("</A>");
        xslStr.append(getHelpIcon());
        xslStr.append("  &#xa0;&#xa0;&#xa0;</td>");

        return xslStr.toString();

    }

    public String drawXSL(boolean withTD) {
        if (withTD) {
            StringBuffer xslStr = new StringBuffer();
            xslStr.append("<td " + ((getDisplayStyle()).getAlignVal()) + " >");
            xslStr.append(drawXSL());
            xslStr.append("</td>");
            return xslStr.toString();
        } else {
            return drawXSL();
        }

    }

}
