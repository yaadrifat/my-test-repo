/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Wed Jul 09
 * 14:42:11 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

// /__astcwz_class_idt#(1470!n)
public class VHr extends VTag {
    // /__astcwz_attrb_idt#(1473)
    private String align;

    // /__astcwz_default_constructor
    public VHr() {
    }

    // /__astcwz_opern_idt#()
    public String getAlign() {
        return align;
    }

    // /__astcwz_opern_idt#()
    public void setAlign(String aalign) {
        align = aalign;
    }

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();
        xslStr.append("<br/>");
        xslStr.append("<hr ");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getId());
        xslStr.append(" /> ");
        xslStr.append("<br/>");

        return xslStr.toString();

    }

    public VHr(String id, String tagClass, String style, String title) {
        this.id = id;
        this.tagClass = tagClass;
        this.style = style;
        this.title = title;

    }

}
