package com.velos.services.authorization2;


import javax.ejb.Remote;
import com.velos.services.OperationException;

/**
 * 
 * @author Raman
 *
 */

@Remote
public interface PatientAuthAgentRObj {
	
	  public boolean checkPatientPermission(Integer mainPK,Privilege privilege)
		throws OperationException;
}
