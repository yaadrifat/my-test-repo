/**
 * 
 */
package com.velos.services.authorization2;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Raman
 *
 */
@XmlRootElement(name="Privilege")
public enum Privilege{
	PRIVILEGE_NEW("new"),
	PRIVILEGE_EDIT("edit"),
	PRIVILEGE_VIEW("view");
	
	private String value; 
	
	private Privilege(String value)
	{
		this.value = value; 
	}
	@Override
	public String toString()
	{
		return value; 
	}
}