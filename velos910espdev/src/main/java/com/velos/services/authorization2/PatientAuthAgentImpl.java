/**
 * Date	02 November 2012
 */
package com.velos.services.authorization2;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import org.apache.log4j.Logger;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;


/**
 * @author Raman
 *
 */
@Stateless
@Remote(PatientAuthAgentRObj.class)
public class PatientAuthAgentImpl extends AbstractService implements PatientAuthAgentRObj{
	@EJB
	private GrpRightsAgentRObj groupRightsAgent;
	@EJB
	private UserSiteAgentRObj userSiteAgent;
	@EJB
	private UserAgentRObj userAgent;
	@Resource 
	private SessionContext sessionContext;
	
	
	private static Logger logger = Logger.getLogger(PatientAuthAgentImpl.class.getName());
	
	/**
	 * Checks User's permission on patient for new or edit or view operation depending on parameter passed.
	 * @param mainPK =personPk, if checking edit or view permission, else sitePk.
	 * @param permission , Type of permission to be checked like "view","edit"or"new". 
	 * @return boolean
	 * @throws OperationException
	 */
	public boolean checkPatientPermission(Integer mainPK,Privilege permission) throws OperationException
	{
		GroupAuthModule groupAuth = new GroupAuthModule(callingUser, groupRightsAgent);
		
		//-------------Checking for Group Rights---------------//
		Integer managePatientPriv = groupAuth.getAppManagePatientsPrivileges();
		//boolean hasViewManagePatient = GroupAuthModule.hasViewPermission(managePatientPriv);
		boolean hasViewManagePatient = checkPermission(permission.toString(), managePatientPriv);
		System.out.println("user manage protocol priv: " + managePatientPriv +" "+permission+" : "+ hasViewManagePatient);		
		if (!hasViewManagePatient)
		{
			if (logger.isDebugEnabled()) logger.debug("User not authorized for '"+permission+"' action on patient data");
            response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized for '"+permission+"' action on patient data"));
			throw new AuthorizationException();
		}
		
		//------------Checking patient facility rights; this is the max rights among all facilities of this patient-----//
		Integer patFacilityRights = 0;
		if("new".equalsIgnoreCase(permission.toString()))
		{
			patFacilityRights = userSiteAgent.getRightForUserSite(callingUser.getUserId(),mainPK);
		}
		else
		{
			patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), mainPK);
		}
		//boolean hasPatFacilityRights = GroupAuthModule.hasViewPermission(patFacilityRights);
		boolean hasPatFacilityRights = checkPermission(permission.toString(), patFacilityRights);
		System.out.println("user patient facility rights: " + patFacilityRights +" "+permission+" : "+ hasPatFacilityRights);
		if (!hasPatFacilityRights) {
            if (logger.isDebugEnabled()) logger.debug("User not authorized for '"+permission+"' action on patient data in its registered sites");
            response.addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,"User not authorized for '"+permission+"' action on patient data in its registered sites"));
            throw new AuthorizationException();
            }
		return true;
	}
	
	/**
	 * Checks for Group permission(new,edit,view) on rights integer passed as a parameter.
	 * @param permission (view,edit,new)
	 * @param rights (Integer value for permission checking)
	 * @return boolean
	 */
	private boolean checkPermission(String permission,Integer rights) {
		//Depending on frequency of, type of permission,
		//being checked, following order is decided, 
		//in order to increase performance(like view permission being checked more often than others)   
		return  ("view".equalsIgnoreCase(permission))  ? (GroupAuthModule.hasViewPermission(rights))
				:(("edit".equalsIgnoreCase(permission))? (GroupAuthModule.hasEditPermission(rights))
				:(("new".equalsIgnoreCase(permission)) ? (GroupAuthModule.hasNewPermission(rights))
				:(false)));
		
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

}
