/**
 * 
 */
package com.velos.services.authorization;

/**
 * @author dylan
 *
 */
public abstract class AbstractAuthModule {

	public static final int PRIVILEGE_VIEW = 4;
	public static final int PRIVILEGE_EDIT = 2;
	public static final int PRIVILEGE_NEW = 1;
	
	
	/** Privilege lists are peppered with headings for display
	 * of privileges. Use this constant when filtering them out.
	 */
	public static final String HEADER_PREFIX = "H_";
	
	
	/**
	 * Utility method that does a check to see if PRIVILEGE_NEW is included in
	 * the privilege list.
	 * 
	 * @param permissionFlags
	 * @return
	 */
	public static boolean hasNewPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_NEW) == PRIVILEGE_NEW;
	}

	/**
	 * Utility method that does a check to see if PRIVILEGE_EDIT is included in
	 * the privilege list.
	 * 
	 * @param permissionFlags
	 * @return
	 */
	public static boolean hasEditPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_EDIT) == PRIVILEGE_EDIT;
	}

	/**
	 * Utility method that does a check to see if PRIVILEGE_VIEW is included in
	 * the privilege list.
	 * 
	 * @param permissionFlags
	 * @return
	 */
	public static boolean hasViewPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_VIEW) == PRIVILEGE_VIEW;
	}
 
	/**
	 * 
	 */
	
	public AbstractAuthModule() {
		super();
	}

}