package com.velos.services.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import org.apache.log4j.Logger;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.GroupIdentifier;
import com.velos.services.model.Groups;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.User.UserStatus;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(UserService.class)
public class UserServiceImpl extends AbstractService implements UserService
{
	private static Logger logger = Logger.getLogger(UserService.class.getName());
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private SiteAgentRObj siteAgent;
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private GroupAgentRObj groupAgentRObj;

	public ResponseHolder changeUserStatus(UserIdentifier userId, UserStatus userStat)throws OperationException 
	{
		try
		{
				//-----------checking if UserIdentifier is null or not.
				if(userId==null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Identifier is required with OID or User Login Name or First Name and Last Name is required."));
					throw new OperationException(); 
				}
				//-----------checking if UserIdentifier is blank or not.
				if((StringUtil.isEmpty(userId.getOID()))
					&& (StringUtil.isEmpty(userId.getUserLoginName()))
					&&(StringUtil.isEmpty(userId.getFirstName()) && StringUtil.isEmpty(userId.getLastName())))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Identifier is required with OID or User Login Name or First Name and Last Name is required."));
					throw new OperationException();
				}
				//------------------Checking calling user's group rights for manage Users------------------------------------------------------------------------------
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
				boolean hasEditUserPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(manageUsers));
				if (!hasEditUserPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to update user's data"));
					throw new AuthorizationException("User is not authorized to edit user's data");
				}
				//--------checking if User account for the status change can be identified or not.
				UserBean userBean=null;
				try {
					userBean=ObjectLocator.userBeanFromIdentifier(callingUser, userId,userAgent, sessionContext, objectMapService);
					
				} catch(MultipleObjectsFoundException mce)
				{	
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, mce.getMessage()));
					throw new OperationException();
				}
				//--------checking if userBean is null or not OR User account for status change is System or Non System user
				//if((userBean==null) || (!userBean.getUserType().equalsIgnoreCase("S")))
				//--------checking if userBean is null or not.
				if((userBean==null))
				{
					addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"User account for the status change not found."));
					throw new OperationException("User account for the status change not found.");
				}
				
				if((userStat==null) || (userStat.toString().length()==0))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,"User Status field is required."));
					throw new OperationException("User Status field is required.");
				}
				if((userBean.getUserType().equalsIgnoreCase("N"))&&(userStat.getLegacyString().equalsIgnoreCase("B")))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,"User Status can not be changed to BLOCKED"));
					throw new OperationException("User Status can not be changed to BLOCKED");
				}
				userBean.setUserStatus(userStat.getLegacyString());
			
				int chk=userAgent.updateUser(userBean);
				if(chk==-2)
				{
					addIssue(new Issue(IssueTypes.ERROR_UPDATING_USER_ACCOUNT_STATUS,"Error occured while updating user account status with OID : " +
							(objectMapService.getOrCreateObjectMapFromPK(TABLE_NAME, userBean.getUserId())).getOID()));
					throw new OperationException("Error occured while updating user account status ");
				}
				if(chk==0)
				{
					userId.setUserLoginName(userBean.getUserLoginName());
					userId.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userBean.getUserId()).getOID());
					
				}
				
				
				response.addAction(new CompletedAction(userId, CRUDAction.UPDATE)); 
				return response;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl update", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl update", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
	}

	public Groups getAllGroups() throws OperationException 
	{
		try
		{
				//------------------Checking calling user's group rights for manage Users------------------------------------------------------------------------------
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageGroupsPrivileges().intValue();
				boolean hasViewUserPermissions = GroupAuthModule.hasViewPermission(Integer.valueOf(manageUsers));
				if (!hasViewUserPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to view data"));
					throw new AuthorizationException("User is not authorized to view data");
				}
			
				GroupDao groupDao=groupAgentRObj.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
				List<GroupIdentifier> grpList=new ArrayList<GroupIdentifier>();
				for(int i=0;i<groupDao.getGrpNames().size();i++)
				{
					GroupIdentifier group=new GroupIdentifier();
					group.setGroupName((groupDao.getGrpNames()).get(i).toString());
					group.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_GROUPS,(Integer) groupDao.getGrpIds().get(i) )).getOID());
					grpList.add(group);
				}
				Groups groups=new Groups();
				groups.addAll(grpList);
				return groups;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
				
	}
	public Groups getUserGroups(UserIdentifier userId) throws OperationException
	{
		ArrayList<GroupIdentifier> usrgrpList = new ArrayList<GroupIdentifier>();
		try
		{  
		    //------------------Checking calling user's group rights for manage users & manage groups ------------------------------------------------------------------------------
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
			int manageGroups = authModule.getAdminManageGroupsPrivileges().intValue();
			boolean hasManageUserViewPermission = GroupAuthModule.hasViewPermission((Integer.valueOf(manageGroups)));
			boolean hasManageGroupsViewPermissions = GroupAuthModule.hasViewPermission((Integer.valueOf(manageUsers)));
					
			if (!hasManageUserViewPermission && !hasManageGroupsViewPermissions)
	 		{ 
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to view user data "));
				throw new AuthorizationException(" Calling User is not authorized to view user data ");
			}
			
			if(userId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid UserIdentifier is required ")); 
				throw new OperationException();
			}	
			if((StringUtil.isEmpty(userId.getOID())) && (StringUtil.isEmpty(userId.getUserLoginName())))
			{
		    	addIssue(new Issue(IssueTypes.USER_NOT_FOUND, " Valid UserIdentifier with OID or userLoginName is required "));
				throw new OperationException(); 
		    }	
		    	
		    UserBean usrbn = null;
		    try
		    {
		    	usrbn = ObjectLocator.userBeanFromIdentifier(callingUser, userId, userAgent, sessionContext, objectMapService);
		    }
		    catch (MultipleObjectsFoundException e) {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
		    			"Multiple Patients found")); 
		    	throw new OperationException(); 
		    }
		    if(usrbn == null ){
		        addIssue(new Issue(
		                IssueTypes.USER_NOT_FOUND, 
		                "User not found for OID: "+userId.getOID()+ " UserLoginName " +userId.getUserLoginName()));
		        throw new OperationException();
		    }
		    int userID = usrbn.getUserId();
		    int acctID = (StringUtil.stringToNum(usrbn.getUserAccountId()));
		  	    
			CodeDao cd = new CodeDao();
	        cd.getUserGroups(userID, acctID);
	        cd.getCId();
	        cd.getCDesc();
	        for(int counter=0;counter<cd.getCId().size();counter++)
	        {
	           	GroupIdentifier usrgrp = new GroupIdentifier();
	        	usrgrp.setGroupName(cd.getCDesc().get(counter).toString());	        	
	        	usrgrp.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_GROUPS, (Integer)cd.getCId().get(counter))).getOID());
	        	usrgrpList.add(usrgrp);
	        }
	        Groups userGroups=new Groups();
	        userGroups.addAll(usrgrpList);
	        return userGroups;  
		}
		catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved", t);
			throw new OperationException(t);
		}
				
	}
	
      //---------creating NonSystemUser ----------------------

	public ResponseHolder createNonSystemUser(NonSystemUser nonSystemUser)
			throws OperationException 
	{
		
	   try
	   {
		   //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
		   
		GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageUsers)));
		if (!hasNewPermissions)
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to view user data "));
			throw new AuthorizationException(" User is not authorized to view user data ");
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("sessionContext", sessionContext); 
		parameters.put("userAgent", userAgent);
		parameters.put("objectMapService", objectMapService);
		parameters.put("callingUser", callingUser);
		parameters.put("ResponseHolder", response); 
		
		UserServiceHelper userServiceHelper = new UserServiceHelper();
		int nonSystemUserPK = userServiceHelper.createNonSystemUser(nonSystemUser,parameters);
	 		
		ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(nonSystemUserPK));
		UserIdentifier userIdentifier = new UserIdentifier();
		userIdentifier.setOID(map.getOID());
		this.response.addAction(new CompletedAction(userIdentifier, CRUDAction.CREATE));
	}
	   catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
			throw new OperationException(t);
		}
	   
	   return response;
	}
	
	
		public Organizations getAllOrganizations() throws OperationException{
		
		try{
			
		        boolean isUserAUthorised=false;
		        GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		        int manageUsers=authModule.getAdminManageUsersPrivileges().intValue();
		        if((GroupAuthModule.hasNewPermission(manageUsers) || (GroupAuthModule.hasEditPermission(manageUsers)) ))
		        {
		        	
		        	isUserAUthorised=true;
		        }
		        else 
		        {
		        	
		        	int manageOrganisation= authModule.getAdminManageOrganizationsPrivileges().intValue();
		        	if((GroupAuthModule.hasViewPermission(manageOrganisation)) || (GroupAuthModule.hasEditPermission(manageOrganisation)))
		        			{
			        			
			        			isUserAUthorised=true;
		        			}
		        
		        	
		        }
		        if(!isUserAUthorised)
		        {
		        	//throw exception
		        	addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to view organization"));
					throw new AuthorizationException("User is not authorized to view organization");
		        	
		        }
		        
		  
			SiteDao siteDao= siteAgent.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
			List<OrganizationIdentifier> orgList= new ArrayList<OrganizationIdentifier>();
			for(int counter=0;counter<siteDao.getSiteNames().size();counter++)
			{
				OrganizationIdentifier organization= new OrganizationIdentifier();
				organization.setSiteName(siteDao.getSiteNames().get(counter).toString());
				organization.setOID((objectMapService.getOrCreateObjectMapFromPK("er_site",(Integer) siteDao.getSiteIds().get(counter) )).getOID());
				orgList.add(organization);
				
			}
			
			Organizations organizations=new Organizations();
			organizations.addAll(orgList);
			return organizations;
				
		}
		catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		
	}
	
	
	//Method to Kill the User Session
		public ResponseHolder killUserSession(UserIdentifier userId) throws OperationException {
			
			try{
				UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
				UserBean userBean = new UserBean();	
				int result = 0;
				
				if (callingUser.getUserId() == null || callingUser.getUserId() == 0){
					
					addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"Valid calling user required for killing the session of another user"));
					throw new OperationException();
				}
				
				if(userId == null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid UserIdentifier is required ")); 
					throw new OperationException();
				}	
				if((StringUtil.isEmpty(userId.getOID()))
						&& (StringUtil.isEmpty(userId.getUserLoginName()))
						&&(StringUtil.isEmpty(userId.getFirstName()) && StringUtil.isEmpty(userId.getLastName())))
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Identifier is required with OID or User Login Name or First Name and Last Name is required."));
						throw new OperationException();
					}
				
				try {
					userBean = ObjectLocator.userBeanFromIdentifier(callingUser, userId, userAgent, sessionContext, objectMapService);
				} catch (MultipleObjectsFoundException mce) {
					// TODO Auto-generated catch block
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, mce.getMessage()));
					throw new OperationException();
				}
				
				if((userBean==null))
				{
					addIssue(new Issue(IssueTypes.SYSTEM_USER_NOT_FOUND,"User account not found for which the session is to be killed"));
					throw new OperationException("User account not found for which the session is to be killed");
				}
				
				if(userBean.getUserType().compareTo("N") == 0){
					
					addIssue(new Issue(IssueTypes.SYSTEM_USER_NOT_FOUND,"System User account not found for which the session is to be killed"));
					throw new OperationException("System User account not found for which the session is to be killed");
				}
				
				
				GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
				
				if(groupAgentRObj.getGroupDetails(EJBUtil.stringToNum(callingUser.getUserGrpDefault())).getGroupName().compareTo("Admin") != 0){
					
					if(callingUser.getUserId() != userBean.getUserId()){
						addIssue(new Issue(IssueTypes.DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION,"User does not have the right to kill somebody else's session"));
						throw new OperationException("User does not have the right to kill somebody else's session");
					}
				}
					
				if (userBean.getUserLoginFlag().compareTo("0") == 0){
					addIssue(new Issue(IssueTypes.USER_NOT_LOGGED_IN,"User is not currently logged in so no active session to kill"));
					throw new OperationException("User is not currently logged in so no active session to kill");
				}
			    userBean.setUserCurrntSsID("");
			    userBean.setUserLoginFlag("0");
			    result = userAgentRObj.updateUser(userBean);
			    userAgentRObj.flush();
			    if (result ==0){
					userId.setUserLoginName(userBean.getUserLoginName());//Bug Fix : 12253
					userId.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userBean.getUserId()).getOID());//Bug Fix: 12253
			    	response.addAction(new CompletedAction(userId, CRUDAction.UPDATE));
			    }
			    else if (result == -2){
			    	addIssue(new Issue(IssueTypes.USER_SESSION_NOT_KILLED,"User was not logged out of the system successfully"));
					throw new OperationException("User was not logged out of the system successfully");
			    }
			    
				return response;
				}catch(OperationException e){
					sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", e);
					throw new OperationRolledBackException(response.getIssues());
			
				}
				catch(Throwable t){
					sessionContext.setRollbackOnly();
					addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
					throw new OperationRolledBackException(response.getIssues());
			
				}
				
			}
     

		//  ------- creating system User -------------
			
		public ResponseHolder createUser(User user) throws OperationException 
		{
			try
			{
				  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
							
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageUsers)));
				if (!hasNewPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to create user data "));
					throw new AuthorizationException(" User is not authorized to create new user.");
				}
				if(user == null)
				{	
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required")); 
					throw new OperationException(); 
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("sessionContext", sessionContext); 
				parameters.put("userAgent", userAgent);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("siteAgent", siteAgent);
				parameters.put("ResponseHolder", response);
				
				UserServiceHelper userServiceHelper = new UserServiceHelper();
				int userPK = userServiceHelper.createUser(user,parameters);
							
				ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(userPK));
				UserIdentifier userIdentifier = new UserIdentifier();
				userIdentifier.setOID(map.getOID());
				this.response.addAction(new CompletedAction(userIdentifier, CRUDAction.CREATE));
				
				
			}
			
			catch(OperationException e){
				e.printStackTrace();
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				throw new OperationException(t);
			}
			
			
			return response;
		}
		
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		this.response = new ResponseHolder();
		this.callingUser = getLoggedInUser(this.sessionContext, this.userAgent);
		return ctx.proceed();
	}

}
