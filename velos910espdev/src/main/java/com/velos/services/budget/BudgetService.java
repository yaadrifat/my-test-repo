package com.velos.services.budget;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;

@Remote
public interface BudgetService {
	
	public BudgetStatus getBudgetStatus(BudgetIdentifier budgetIdentifier) throws OperationException;

}
