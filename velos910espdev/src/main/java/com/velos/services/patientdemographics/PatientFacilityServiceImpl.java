package com.velos.services.patientdemographics;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote({PatientFacilityService.class })
public class PatientFacilityServiceImpl extends AbstractService implements PatientFacilityService
{
	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@EJB
	private PersonAgentRObj personAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private SiteAgentRObj siteAgent;

	@EJB
	private ObjectMapService objectMapService;

	@EJB
	private PatProtAgentRObj patProtAgent;

	@EJB
	private UserSiteAgentRObj userSiteAgent;

	@EJB
	private PatFacilityAgentRObj patFacilityAgent;
	
	@EJB
	private PerIdAgentRObj perIdAgentRObj;
	
	@EJB
	private PrefAgentRObj prefAgent; 

	private static Logger logger = Logger.getLogger(PatientFacilityServiceImpl.class.getName());
	private static Integer personPk=null;
	
	//------------------addPatientFacility method implementation-----------------------------------------------------------------------------------------------
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ResponseHolder addPatientFacility(PatientIdentifier patId,PatientOrganization patOrg) throws OperationException
	{
		try
		{
			if(patId == null)
			{	
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required")); 
				throw new OperationException(); 
			}
			if((patId.getOID() == null || patId.getOID().length() == 0) 
					&& ((patId.getPatientId() == null || patId.getPatientId().length() == 0)
							|| (patId.getOrganizationId() == null) ) )
			{	
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			
			//----------------------------------finding personPk----------------------------------------------------------------------------------------------
			personPk = null; 
		    try
		    {
		    	personPk = ObjectLocator.personPKFromPatientIdentifier(callingUser,patId,objectMapService);
		    } 
		    catch (MultipleObjectsFoundException e)
		    {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Multiple Patients found")); 
		    	throw new OperationException();
		    }
		    //-----------------------------------if person not found, add an issue------------
		    if(personPk == null || personPk ==0)
		    {
		    	if(((patId.getPatientId() == null || patId.getPatientId().length() == 0) || (patId.getOrganizationId() == null) ) 
		    			&& (patId.getOID() != null || patId.getOID().length() != 0))
		    	{
		    		addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for OID : "+patId.getOID()));
		    	}
		    	else
		    	{
		    		addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for code: "+patId.getPatientId()+ " for given OrganizationId"));
		    	}
		    	
		        throw new OperationException();
		    }
		    
		    if (patOrg == null ) 
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No Input Data provided"));
				OperationException oe = new OperationException();
				oe.setIssues(response.getIssues());
				throw oe;
			} 
			else 
			{
				validate(patOrg);
			}
		    
		    //------------------Checking calling user's group rights for manage patients------------------------------------------------------------------------------
			GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges().intValue();
			boolean hasEditPatientPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(managePatients));
			if (!hasEditPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to edit patient data"));
				throw new AuthorizationException("User is not authorized to edit patient data");
			}
		    
		    //-------------------checking organisation authorisation--------------------------------------------------------------------------------------------------
			PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilities(personPk);
			int facilityCount = (patFacilityDao.getId()).size();
			boolean hasAccessToSite=false;
			Integer userSiteRight=null;
				 
			for (int patFacilityCount = 0; patFacilityCount < facilityCount ; patFacilityCount++)
			{
		    	 String sitePK = patFacilityDao.getPatientSite().get(patFacilityCount).toString();
		    	 			    	 
		    	if(patFacilityDao.getPatAccessFlag(patFacilityCount).equals("7")) //7 means Access is granted for this organization, 0 means revoked
				{
					userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(callingUser.getUserId().intValue(),EJBUtil.stringToInteger(sitePK)));
					
					if (AbstractAuthModule.hasEditPermission(userSiteRight))
				      {
				        hasAccessToSite=true;
						break;
				      }
				 }
			 }
			
		     if(!hasAccessToSite)
		     {
		    	 if(((patId.getPatientId() == null || patId.getPatientId().length() == 0) || (patId.getOrganizationId() == null) ) 
			    			&& (patId.getOID() != null || patId.getOID().length() != 0))
		    	 {
		    		 addIssue(
					          new Issue(
					          IssueTypes.ORGANIZATION_AUTHORIZATION, 
					          "User not Authorized to edit Patient to this Organization for OID : "+patId.getOID()));
		    	}
			    else
			    {
			    	addIssue(
						          new Issue(
						          IssueTypes.ORGANIZATION_AUTHORIZATION, 
						          "User not Authorized to edit Patient to this Organization. OID: " + 
						          patId.getOrganizationId().getOID() + " SiteAltID:" + patId.getOrganizationId().getSiteAltId() + " SiteName:" + patId.getOrganizationId().getSiteName()));
			    }
		    	 
				        throw new OperationException();
		     }
		     
		     Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("personAgent", this.personAgent);
				parameters.put("sessionContext", this.sessionContext);
				parameters.put("userAgent", this.userAgent);
				parameters.put("siteAgent", this.siteAgent);
				parameters.put("objectMapService", this.objectMapService);
				parameters.put("patProtAgent", this.patProtAgent);
				parameters.put("userSiteAgent", this.userSiteAgent);
				parameters.put("patFacilityAgent", this.patFacilityAgent);
				parameters.put("callingUser", this.callingUser);
				parameters.put("ResponseHolder", this.response);
				parameters.put("personPk", personPk);
				parameters.put("groupRightsAgent", groupRightsAgent);
				parameters.put("prefAgent", prefAgent); 
				
				PatientFacilityHelper addPatientFacilityServiceHelper=new PatientFacilityHelper();
				int saved=addPatientFacilityServiceHelper.addPatientFacility(patId, patOrg, parameters);
				if(saved==0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_ERROR_ADD_ORGANIZATION));
					throw new OperationException();
				}
				
				ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FACILITY, saved);
				PatientOrganizationIdentifier patientOrganizationIdentifier=new PatientOrganizationIdentifier();
				patientOrganizationIdentifier.setOID(map.getOID());

			    this.response.addObjectCreatedAction(patientOrganizationIdentifier);
				
            
			
		    
			
			
		}
		catch (OperationException e)
		{	
			this.sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", e);
			throw new OperationRolledBackException(this.response.getIssues());
		}
		catch (Throwable t)
		{	
			this.sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", t);
			throw new OperationRolledBackException(this.response.getIssues());
		}
		
		return response;
	}
	
	
	
	

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ResponseHolder updatePatientFacility(PatientIdentifier patIdentifier,
			OrganizationIdentifier orgID,
			PatientOrganizationIdentifier paramOrganizationIdentifier,
			PatientOrganization paramPatientOrganization) throws OperationException
	{
		
		try
		{				
			//validating Patient Organization Identifier
			
			if(paramOrganizationIdentifier == null && (patIdentifier == null || orgID == null))
			{	
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientOrganizationIdentifier or PatientIdentifier and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			if((paramOrganizationIdentifier == null || paramOrganizationIdentifier.getOID() == null  || paramOrganizationIdentifier.getOID().length()==0)
					&& (!isPatientIdentifierValid(patIdentifier) || !isOrganizationIdentifierValid(orgID)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientOrganizationIdentifier with OID Or valid PatientIdentifier and OrganzationIdentifier is required"));
				throw new OperationException(); 
			}
			
			
			
			ObjectMap	map = null;
			 	int patFacilityPK = -1;

			 	if ((paramOrganizationIdentifier != null) && (paramOrganizationIdentifier.getOID() != null) && (paramOrganizationIdentifier.getOID().length() > 0))
			 	{
			 		try
			 		{
			 			map = this.objectMapService.getObjectMapFromId(paramOrganizationIdentifier.getOID());

			 		}
			 		catch (MultipleObjectsFoundException e)
			 		{
			 			addIssue(
			 					new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
			 							"Multiple Patients found"));
			 			throw new OperationException();
			 		}
			 		
			 		if(map != null)
				 	{	
				 		if(map.getTableName() != null)
				 		{	
				 			if(map.getTableName().equalsIgnoreCase("er_patfacility"))
				 			{
				 				patFacilityPK = map.getTablePK();
				 			}
				 			else
				 			{	
				 				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for -OID : " + paramOrganizationIdentifier.getOID()));
				 				throw new OperationException();
				 			}	
				 		}
				 	}
			 	}else if(isOrganizationIdentifierValid(orgID) && isPatientIdentifierValid(patIdentifier))
			 	{
			 		 PatientFacilityDAO patientFacilityServiceDAO = new PatientFacilityDAO();
			 		 Integer organizationID = ObjectLocator.sitePKFromIdentifier(callingUser, orgID, sessionContext, objectMapService); 
			 		 if(organizationID == null || organizationID == 0)
			 		 {
			 			addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Organization for OrganizationIdentifier not found"));
						throw new OperationException(); 
			 		 }
			 		 
			 		 Integer patientID = ObjectLocator.personPKFromPatientIdentifier(callingUser, patIdentifier, objectMapService); 
			 		 if(patientID == null || patientID == 0)
			 		 {
			 			addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient for PatientIdentifier not found"));
						throw new OperationException(); 
			 			 
			 		 }
			 		 patFacilityPK = patientFacilityServiceDAO.getPatientFacilityID(organizationID, patientID); 
			 	}else
			 	{
			 		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientOrganizationIdentifier with OID Or valid PatientIdentifier and OrganzationIdentifier is required"));
					throw new OperationException(); 
			 	}
			 	
			 	
			 
			 	
			    if(patFacilityPK ==0)
			    {
			        addIssue(new Issue(
			                IssueTypes.PATIENT_NOT_FOUND, 
			                "Patient not found for OID : "+paramOrganizationIdentifier.getOID()));
			        throw new OperationException();
			    }
			    
			   // --- retrieving the PatFacilityDetails ----
			    
			    PatFacilityBean patBean = patFacilityAgent.getPatFacilityDetails(patFacilityPK);
			  		    
			    if(patBean == null)
			    {
			    	 	addIssue(new Issue(
	                        IssueTypes.PATIENT_NOT_FOUND, 
	                        "Patient not found for OID : "+paramOrganizationIdentifier.getOID()));
	                throw new OperationException();
			    }
			    String personPK = patBean.getPatientPK();
			    
			    Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("personAgent", this.personAgent);
				parameters.put("sessionContext", this.sessionContext);
				parameters.put("userAgent", this.userAgent);
				parameters.put("siteAgent", this.siteAgent);
				parameters.put("objectMapService", this.objectMapService);
				parameters.put("patProtAgent", this.patProtAgent);
				parameters.put("userSiteAgent", this.userSiteAgent);
				parameters.put("patFacilityAgent", this.patFacilityAgent);
				parameters.put("callingUser", this.callingUser);
				parameters.put("ResponseHolder", this.response);
				parameters.put("perIdAgentRObj", this.perIdAgentRObj);
				parameters.put("personPK", personPK); 
				parameters.put("prefAgent", prefAgent); 
			    
				
				//checking calling users group rights for manage patients
								
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int managePatients = authModule.getAppManagePatientsPrivileges().intValue();
				boolean hasEditPatientPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(managePatients));
				if (!hasEditPatientPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to edit patient data"));
					throw new AuthorizationException("User is not authorized to edit patient data");
				}
				
					
				//--------checking organization authorization
				
				PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilities(EJBUtil.stringToNum((personPK)));
				int facilityCount = (patFacilityDao.getId()).size();
				boolean hasAccessToSite=false;
				Integer userSiteRight=null;
				for (int patFacilityCount = 0; patFacilityCount < facilityCount ; patFacilityCount++)
				{
			    	 String sitePK = patFacilityDao.getPatientSite().get(patFacilityCount).toString();
 
			    	if(patFacilityDao.getPatAccessFlag(patFacilityCount).equals("7")) //7 means Access is granted for this organization, 0 means revoked
					{
						userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(callingUser.getUserId().intValue(),EJBUtil.stringToInteger(sitePK)));
						
						if (AbstractAuthModule.hasEditPermission(userSiteRight))
					      {
					        hasAccessToSite=true;
							break;
					      }
					}
				}
				
			     if(!hasAccessToSite)
			     {
			    	 addIssue(new Issue(
					          IssueTypes.ORGANIZATION_AUTHORIZATION, 
					          "User not Authorized to edit Patient to this Organization. OID: "));
					        throw new OperationException();
			     }
			  			 
			   //-----calling updatePatientfacility method of UpdatePatientFacilityHelper
			     
			     PatientFacilityHelper helper = new PatientFacilityHelper();
			     	int result = helper.updatePatientfacility(paramPatientOrganization,parameters,patBean);
					if (result == 0)
					{
						if(paramOrganizationIdentifier == null) paramOrganizationIdentifier = new PatientOrganizationIdentifier(); 
						if(paramOrganizationIdentifier.getOID() == null || paramOrganizationIdentifier.getOID().length() == 0)
						{
							ObjectMap pfacmap =objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FACILITY, patFacilityPK); 
							paramOrganizationIdentifier.setOID(pfacmap.getOID()); 
						}
						this.response.addAction(new CompletedAction("Patient facility with OID :" + paramOrganizationIdentifier.getOID() + 
				    	        " updated successfully", CRUDAction.UPDATE));
					} 
					else
					{
						  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							        new Issue(
							        IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION));
							      throw new OperationException();

					}
			  	     
		}
		catch (OperationException e) {
			this.sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("updatePatientfacility update", e);
			throw new OperationRolledBackException(this.response.getIssues());
		} catch (Throwable t) {
			this.sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("updatePatientfacility update", t);
			throw new OperationRolledBackException(this.response.getIssues());
		}
		return response;
		
		// TODO Auto-generated method stub
		
	}
	
	private boolean isOrganizationIdentifierValid(OrganizationIdentifier orgID)
	{
		if(orgID == null) return false; 
		if((orgID.getOID() == null || orgID.getOID().length() == 0) 
				&& (orgID.getSiteAltId() == null || orgID.getSiteAltId().length() == 0) 
				&& (orgID.getSiteName() == null || orgID.getSiteName().length() == 0) ) return false; 
		return true; 
	}
	
	private boolean isPatientIdentifierValid(PatientIdentifier patID)
	{
		if(patID == null) return false; 
		if((patID.getOID() == null || patID.getOID().length() == 0)
				&& (patID.getPatientId() == null || patID.getPatientId().length() == 0 || !isOrganizationIdentifierValid(patID.getOrganizationId()))) return false; 
		return true; 
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		this.response = new ResponseHolder();
		this.callingUser = getLoggedInUser(this.sessionContext, this.userAgent);
		return ctx.proceed();
	}

}
