package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;

/**
 * A remote service supporting the PatientDemographics service messages.
 * 
 * @author virendra
 *
 */
@Remote
public interface PatientDemographicsService{ 
	/**
	 * PatientDemographics remote service
	 * @param patientId
	 * @return
	 * @throws OperationException
	 */
	
public PatientDemographics getPatientDemographics(PatientIdentifier patientId) throws OperationException;

/**
 * Creates Patient  
 * @param patient to create
 * @return A ResponseHolder instance that may hold information about database persisting
 *  actions that occurred as a result of this operation
 * @throws OperationException
 */
public ResponseHolder createPatient(Patient patient) throws OperationException; 

}