package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SitesOfService")
public class SitesOfService extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6699703769196010428L;
	protected List<OrganizationIdentifier> sitesOfService = new ArrayList<OrganizationIdentifier>();
	
	public List<OrganizationIdentifier> getSitesOfService() {
		return sitesOfService;
	}
	
	public void setSitesOfService(List<OrganizationIdentifier> sitesOfService) {
		this.sitesOfService = sitesOfService;
	}
	
	public void add(OrganizationIdentifier sitesOfService) {
		this.sitesOfService.add(sitesOfService);
	}

	public void addAll(List<OrganizationIdentifier> sitesOfService) {
		this.sitesOfService = sitesOfService;
	}

}
