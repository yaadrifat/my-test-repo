package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="VisitIdentifier")
public class VisitNameIdentifier extends VisitIdentifier{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5420816923619078767L;
	protected String visitName;
	
	
	public VisitNameIdentifier(){
		
	}
	
	public String getVisitName()
	{
		return visitName;
	}
	
	public void setVisitName(String visitName)
	{
	    this.visitName = visitName;	
	}
	
}