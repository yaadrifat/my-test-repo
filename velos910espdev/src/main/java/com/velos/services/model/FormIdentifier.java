/**
 * Created On Sep 1, 2011
 */
package com.velos.services.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.FormDesign.SharedWith;

/**
 * @author Kanwaldeep
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FormIdentifier")
public class FormIdentifier extends SimpleIdentifier {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3909551017780401214L;	


}
