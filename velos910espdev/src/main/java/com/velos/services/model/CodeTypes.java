/**
 * Created On Nov 6, 2012
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="CodeTypes")
public class CodeTypes extends ServiceObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4281717059856868967L;
	private List<String> type = new ArrayList<String>();

	/**
	 * @param type the type to set
	 */
	public void setType(List<String> type) {
		this.type = type;
	}

	public List<String> getType() {
		return type;
	} 
	
	public void add(String type)
	{
		this.type.add(type); 
	}

}
