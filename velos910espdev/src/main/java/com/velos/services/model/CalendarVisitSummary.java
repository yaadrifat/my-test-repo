/**
 * Created On Jul 26, 2011
 */
package com.velos.services.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="VisitSummary")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarVisitSummary extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7085871989130768891L;
	@XmlElement(required=true)	
	protected String visitName;
	protected VisitIdentifier visitIdentifier; 
	protected String visitDescription;
	protected Integer visitSequence;
	protected Duration absoluteInterval;
	protected Duration relativeVisitInterval;
	protected String relativeVisitName; 
	protected Duration visitWindowBefore;
	protected Duration visitWindowAfter;
	protected Boolean noIntervalDefined;
	
	
	public Integer getVisitSequence() {
		return visitSequence;
	}

	public void setVisitSequence(Integer visitSequence) {
		this.visitSequence = visitSequence;
	}

	@Valid
	public Duration getVisitWindowBefore() {
		return visitWindowBefore;
	}

	public void setVisitWindowBefore(Duration visitWindowBefore) {
		this.visitWindowBefore = visitWindowBefore;
	}

	@Valid
	public Duration getVisitWindowAfter() {
		return visitWindowAfter;
	}

	public void setVisitWindowAfter(Duration visitWindowAfter) {
		this.visitWindowAfter = visitWindowAfter;
	}

	public Boolean isNoIntervalDefined() {
		return noIntervalDefined;
	}

	public void setNoIntervalDefined(Boolean noIntervalDefined) {
		this.noIntervalDefined = noIntervalDefined;
	}
	
	@NotNull
	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitDescription() {
		return visitDescription;
	}

	public void setVisitDescription(String visitDescription) {
		this.visitDescription = visitDescription;
	}

	@Valid
	public Duration getAbsoluteInterval() {
		return absoluteInterval;
	}

	public void setAbsoluteInterval(Duration absoluteInterval) {
		this.absoluteInterval = absoluteInterval;
	}

	@Valid
	public Duration getRelativeVisitInterval() {
		return relativeVisitInterval;
	}

	public void setRelativeVisitInterval(Duration relativeVisitInterval) {
		this.relativeVisitInterval = relativeVisitInterval;
	}

	
	public String getRelativeVisitName() {
		return relativeVisitName;
	}

	public void setRelativeVisitName(String relativeVisitName) {
		this.relativeVisitName = relativeVisitName;
	}
	
	public VisitIdentifier getVisitIdentifier() {
		return visitIdentifier;
	}

	public void setVisitIdentifier(VisitIdentifier visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	}
	
	public ParentIdentifier getParentIdentifier() {
		return super.parentIdentifier; 
	}

	public void setParentIdentifier(ParentIdentifier calendarIdentifer) {

		super.parentIdentifier = calendarIdentifer;
	}


}
