/**
 * Created On Aug 27, 2012
 */
package com.velos.services.model;

import javax.validation.constraints.NotNull;

/**
 * @author Kanwaldeep
 *
 */
public class UpdatePatientDemographics extends PatientDemographics {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5623552583489260278L;
	protected Code deathCause;
	protected String deathCauseOther;
	protected Codes additionalEthnicity ;
	protected Codes additionalRace;
	protected Code bloodGroup;
	protected Code maritalStatus;
	protected String notes;
	protected Code timeZone;
	protected Code employment;
	protected Code education;
	protected String reasonForChange; 
	
	public Code getDeathCause() {
		return deathCause;
	}
	public void setDeathCause(Code deathCause) {
		this.deathCause = deathCause;
	}
	public String getDeathCauseOther() {
		return deathCauseOther;
	}
	public void setDeathCauseOther(String deathCauseOther) {
		this.deathCauseOther = deathCauseOther;
	}
	public Codes getAdditionalEthnicity() {
		return additionalEthnicity;
	}
	public void setAdditionalEthnicity(Codes additionalEthnicity) {
		this.additionalEthnicity = additionalEthnicity;
	}
	public Codes getAdditionalRace() {
		return additionalRace;
	}
	public void setAdditionalRace(Codes additionalRace) {
		this.additionalRace = additionalRace;
	}
	public Code getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(Code bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Code getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(Code maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Code getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(Code timeZone) {
		this.timeZone = timeZone;
	}
	public Code getEmployment() {
		return employment;
	}
	public void setEmployment(Code employment) {
		this.employment = employment;
	}
	public Code getEducation() {
		return education;
	}
	public void setEducation(Code education) {
		this.education = education;
	}
	
	@NotNull
	public String getReasonForChange() {
		return reasonForChange;
	}
	public void setReasonForChange(String reasonForChange) {
		this.reasonForChange = reasonForChange;
	}	

}
