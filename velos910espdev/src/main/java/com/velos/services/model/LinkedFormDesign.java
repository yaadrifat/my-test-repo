/**
 * Created On Sep 7, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Form")
public class LinkedFormDesign extends FormDesign{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 7024485472658984308L;
	private boolean isIRBForm = false; 
	private Code submissionType; 
	private DisplayType displayType; 
	private EntrySettings entrySetting;
	private Integer dataCount; 

	public enum DisplayType
	{
		STUDY("S"), 
		PATIENT_ENROLLED_TO_SPECIFIC_STUDY("SP"), 
		ACCOUNT("A"), 
		ALL_STUDIES("SA"), 
		ALL_PATIENTS("PA"),
		PATIENT_LEVEL_ALL_STUDIES("PS"), 
		PATIENT_LEVEL_ALL_STUDIES_RESTRICTED("PR"); 
		
		private String value; 
		
		private DisplayType(String value)
		{
			this.value = value; 
		}
		@Override
		public String toString()
		{
			return value; 
		}
	}
	
	public enum EntrySettings
	{
		MULTIPLE("M"), 
		ONCE("O"),
		ONCE_EDITABLE("E"); 
		
		private String value; 
		private EntrySettings(String value)
		{
			this.value = value; 
		}
		
		public String toString()
		{
			return value; 
		}
	}

	/**
	 * @param entrySetting the entrySetting to set
	 */
	public void setEntrySetting(EntrySettings entrySetting) {
		this.entrySetting = entrySetting;
	}

	/**
	 * @return the entrySetting
	 */
	public EntrySettings getEntrySetting() {
		return entrySetting;
	}
	
	public boolean isIRBForm() {
		return isIRBForm;
	}

	public void setIRBForm(boolean isIRBForm) {
		this.isIRBForm = isIRBForm;
	}

	public Code getSubmissionType() {
		return submissionType;
	}

	public void setSubmissionType(Code submissionType) {
		this.submissionType = submissionType;
	}

	public DisplayType getDisplayType() {
		return displayType;
	}

	public void setDisplayType(DisplayType displayType) {
		this.displayType = displayType;
	}

	public Integer getDataCount() {
		return dataCount;
	}

	public void setDataCount(Integer dataCount) {
		this.dataCount = dataCount;
	}

}
