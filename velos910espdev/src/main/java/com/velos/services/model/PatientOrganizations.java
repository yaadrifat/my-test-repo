/**
 * Created On Jun 17, 2011
 */
package com.velos.services.model;

import java.util.List;

import javax.validation.Valid;

/**
 * @author Kanwaldeep
 *
 */
public class PatientOrganizations extends ServiceObject{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6605608568880230326L;
	protected List<PatientOrganization> organization;

	@Valid
	public List<PatientOrganization> getOrganization() {
		return organization;
	}

	public void setOrganization(List<PatientOrganization> organization) {
		this.organization = organization;
	} 	

}
