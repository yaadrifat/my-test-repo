
package com.velos.services.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data transfer object representing a study status.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyStatus")
public class StudyStatus
    extends ServiceObject
{

	/**
     * 
     */
    private static final long serialVersionUID = 2564377396563561079L;

    protected OrganizationIdentifier organizationId;
	
	protected StudyStatusIdentifier studyStatusId; 

	protected Code statusType;

    protected Code status;

    protected UserIdentifier documentedBy;

    protected UserIdentifier assignedTo;

    protected Date validFromDate;

    protected Date validUntilDate;

    protected Date meetingDate;
    
    protected Code reviewBoard;

    protected Code outcome;

    protected String notes;
 
    protected boolean isCurrentForStudy;
    
    /**
     * Gets the value of the statusType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @NotNull
    @Valid
    public Code getStatusType() {
        return statusType;
    }

    /**
     * Sets the value of the statusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatusType(Code value) {
        this.statusType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @NotNull
    @Valid
    public Code getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setStatus(Code value) {
        this.status = value;
    }

    /**
     * Gets the value of the documentedBy property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    @NotNull
    public UserIdentifier getDocumentedBy() {
        return documentedBy;
    }

    /**
     * Sets the value of the documentedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setDocumentedBy(UserIdentifier value) {
        this.documentedBy = value;
    }

    /**
     * Gets the value of the assignedTo property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public UserIdentifier getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets the value of the assignedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setAssignedTo(UserIdentifier value) {
        this.assignedTo = value;
    }

    /**
     * Gets the value of the validFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @NotNull
    public Date getValidFromDate() {
        return validFromDate;
    }

    /**
     * Sets the value of the validFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFromDate(Date value) {
        this.validFromDate = value;
    }

    /**
     * Gets the value of the validUntilDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public Date getValidUntilDate() {
        return validUntilDate;
    }

    /**
     * Sets the value of the validUntilDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidUntilDate(Date value) {
        this.validUntilDate = value;
    }

    /**
     * Gets the value of the meetingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public Date getMeetingDate() {
        return meetingDate;
    }

    /**
     * Sets the value of the meetingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeetingDate(Date value) {
        this.meetingDate = value;
    }

    /**
     * Gets the value of the reviewBoard property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @Valid
    public Code getReviewBoard() {
        return reviewBoard;
    }

    /**
     * Sets the value of the reviewBoard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setReviewBoard(Code value) {
        this.reviewBoard = value;
    }

    /**
     * Gets the value of the outcome property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @Valid
    public Code getOutcome() {
        return outcome;
    }

    /**
     * Sets the value of the outcome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setOutcome(Code value) {
        this.outcome = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Returns whether this is the current status of the study.
     * @return
     */
    @NotNull
	public boolean isCurrentForStudy() {
		return isCurrentForStudy;
	}

    /**
     * Sets whether this will be the current status for the study.
     * @param isCurrentForStudy
     */
	public void setCurrentForStudy(boolean isCurrentForStudy) {
		this.isCurrentForStudy = isCurrentForStudy;
	}

	/**
	 * Returns the {@link OrganiztionIdentifier} for this study status.
	 * @return
	 */
	@NotNull
	public OrganizationIdentifier getOrganizationId() {
		return organizationId;
	}

	/**
	 * Sets the {@link OrganizationIdentifer} for this study status.
	 * @param organizationId
	 */
	public void setOrganizationId(OrganizationIdentifier organizationId) {
		this.organizationId = organizationId;
	}
	

    /**
     * Returns the {@link StudyStatusIdentifier} for this study status.
     * @return
     */
    public StudyStatusIdentifier getStudyStatusId() {
		return studyStatusId;
	}

	/**
	 * /**
	 * Sets the {@link StudyStatusIdentifier} for this study status.
	 * @param studyStatusId
	 */
	public void setStudyStatusId(StudyStatusIdentifier studyStatusId) {
		this.studyStatusId = studyStatusId;
	}
	
	//virendra;Fixed#6124, overloading setParentId for studyIdentifier
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }
    
}
