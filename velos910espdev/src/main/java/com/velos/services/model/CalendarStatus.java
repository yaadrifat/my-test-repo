/**
 * Created On Jul 6, 2011
 */
package com.velos.services.model;

import java.util.Date;

/**
 * @author Kanwaldeep
 *
 */
public class CalendarStatus extends ServiceObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7111702812803500276L;
	
	private Code status; 
	private Date statusDate; 
	private UserIdentifier statusChangedBy; 
	private String notes; 
	public enum FlagValues
	{
		ALL,
		CURRENT,
		STATUSBASED
	}; 
	
	private FlagValues applyTo; 
	private Code patientStatus; 
	private Date patientStatusOnOrAfterDate; 
	
	public Code getStatus() {
		return status;
	}
	public void setStatus(Code status) {
		this.status = status;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public UserIdentifier getStatusChangedBy() {
		return statusChangedBy;
	}
	public void setStatusChangedBy(UserIdentifier statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public FlagValues getApplyTo() {
		return applyTo;
	}
	public void setApplyTo(FlagValues applyTo) {
		this.applyTo = applyTo;
	}
	public Code getPatientStatus() {
		return patientStatus;
	}
	public void setPatientStatus(Code patientStatus) {
		this.patientStatus = patientStatus;
	}
	public Date getPatientStatusOnOrAfterDate() {
		return patientStatusOnOrAfterDate;
	}
	public void setPatientStatusOnOrAfterDate(Date patientStatusOnOrAfterDate) {
		this.patientStatusOnOrAfterDate = patientStatusOnOrAfterDate;
	}

}
