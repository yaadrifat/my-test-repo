/**
 * Created On Oct 20, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="TextFieldValidations")
public class TextFieldValidations extends FieldValidations{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9032457192490413722L;
	private Integer fieldLength;
	private Integer maxCharAllowed; 
	private Integer editBoxLines;
	private String defaultResponse;
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	public Integer getMaxCharAllowed() {
		return maxCharAllowed;
	}
	public void setMaxCharAllowed(Integer maxCharAllowed) {
		this.maxCharAllowed = maxCharAllowed;
	}
	public Integer getEditBoxLines() {
		return editBoxLines;
	}
	public void setEditBoxLines(Integer editBoxLines) {
		this.editBoxLines = editBoxLines;
	}
	public String getDefaultResponse() {
		return defaultResponse;
	}
	public void setDefaultResponse(String defaultResponse) {
		this.defaultResponse = defaultResponse;
	} 	
}
