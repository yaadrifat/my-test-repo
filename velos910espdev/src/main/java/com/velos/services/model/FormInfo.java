/**
 * Created On Dec 2, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class FormInfo extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8894529314531832875L;
	protected FormIdentifier formIdentifier; 
	protected String formName;
	public FormIdentifier getFormIdentifier() {
		return formIdentifier;
	}
	public void setFormIdentifier(FormIdentifier formIdentifier) {
		this.formIdentifier = formIdentifier;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}

}
