/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**model class for Study a patient belongs to.
 * @author virendra
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientStudy")
public class PatientStudy 
	extends ServiceObject
	implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected StudyIdentifier studyIdentifier;
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
}