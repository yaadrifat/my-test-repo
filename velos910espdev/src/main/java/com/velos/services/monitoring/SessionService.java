/**
 *  
 */
package com.velos.services.monitoring;

import javax.ejb.Remote;



/**
 * A remote service supporting the monitoring of service messages.
 * 
 * @author dylan
 *
 */
@Remote
public interface SessionService {
	public static final String SESSION_LOG_KEY = "com.velos.services.session";
	
	public SessionLog setSessionDetails(SessionLog session);
	public SessionLog updateSessionDetails(SessionLog session);
	public SessionLog getSessionDetails(Integer sessionPK);
	
}
