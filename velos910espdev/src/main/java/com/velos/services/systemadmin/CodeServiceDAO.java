/**
 * Created On Nov 6, 2012
 */
package com.velos.services.systemadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.model.CodeTypes;

/**
 * @author Kanwaldeep
 *
 */
public class CodeServiceDAO extends CommonDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2905895089446389972L;
	private static Logger logger = Logger.getLogger(CodeServiceDAO.class); 
	
	public CodeTypes getCodeTypes() throws SQLException
	{
		CodeTypes types = new CodeTypes(); 
		
		String sql = "select distinct codelst_type from er_codelst union"
					+" select distinct codelst_type from sch_codelst"; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null; 
		
		try{
			
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				types.add(rs.getString(1)); 
			}
		}catch(SQLException sqe)
		{
			logger.error("Failed to retrieve CodeTypes from DB"); 
			throw sqe; 
		}finally
		{
			if(rs != null)
			{
				try{
					rs.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close result set", se); 
				}
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close prepared Statement", se); 
				}
				
			}
			if(conn != null)
			{
				returnConnection(conn); 
			}
		}
		
		
		
		return types; 
	}

}
