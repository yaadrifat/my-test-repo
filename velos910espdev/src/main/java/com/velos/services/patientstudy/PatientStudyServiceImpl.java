package com.velos.services.patientstudy;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.ObjectLocator;
/**
 * Session Bean with implementations related to PatientStudies
 * @author Virendra
 *
 */
@Stateless
@Remote(PatientStudyService.class)
public class PatientStudyServiceImpl 
extends AbstractService 
implements PatientStudyService{

@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private StudyAgent studyAgent;
@EJB
private ObjectMapService objectMapService;
@EJB
private PatProtAgentRObj patProtAgent;
Integer personPk = 0;
@EJB
private UserSiteAgentRObj userSiteAgent;

	
	private static Logger logger = Logger.getLogger(PatientStudyServiceImpl.class.getName());
	/**
	 * returns list of patientStudies with param as PatientIdentifier.
	 */
	public List<PatientStudy> getPatientStudies(PatientIdentifier patientId)
			throws OperationException {
		
		try{
			
			if(patientId == null || ((patientId.getOID() == null || patientId.getOID().length() == 0) 
					&& ((patientId.getPatientId() == null || patientId.getPatientId().length() == 0)
							|| (patientId.getOrganizationId() == null) ) ))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			
			try{
			//PersonPk from ObjectLocator
			personPk = ObjectLocator.personPKFromPatientIdentifier(
					callingUser, 
					patientId, 
					objectMapService);
			
			} catch (MultipleObjectsFoundException e) {
				addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
						"Multiple Patients found")); 
				throw new OperationException(); 
			}
			
			if(personPk == null | personPk ==0){
				addIssue(new Issue(
							IssueTypes.PATIENT_NOT_FOUND, 
							"Patient not found"));
				throw new OperationException();
			}
		
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
//			Integer manageProtocolPriv = 
//				groupAuth.getAppManageProtocolsPrivileges();
//			
//			boolean hasViewManageProt = 
//				GroupAuthModule.hasViewPermission(manageProtocolPriv);
//			
//			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
//			if (!hasViewManageProt){
//				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
//				throw new AuthorizationException("User Not Authorized to view Patient data");
//			}
			
			Integer managePatientsPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(managePatientsPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage patient priv: " + managePatientsPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}
			
	        // #6242: Check patient facility rights; this is the max rights among all facilities of this patient
            Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPk);
            if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
                addIssue(
                        new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                                "User not authorized to access data of this patient"));
                throw new AuthorizationException("User not authorized to access data of this patient");
            }
            
			ArrayList<Integer> lstPatProtPk = ObjectLocator.getPatProtPkFromPersonPk(personPk, objectMapService);
			ArrayList<PatientStudy> lstPatientStudy = new ArrayList<PatientStudy>();
			for(Integer patProtPk: lstPatProtPk){
				PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtPk);
				Integer studyPk =  EJBUtil.stringToInteger(patProtBean.getPatProtStudyId());
				lstPatientStudy.add(getPatientStudy(studyPk));
			}
			if (lstPatientStudy.isEmpty()){
				Issue issue = new Issue(IssueTypes.STUDIES_FOR_PATIENT_NOT_FOUND, "Studies for patient not found");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException();
			}
			return lstPatientStudy;
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	//
	private PatientStudy getPatientStudy(Integer studyPk) throws AuthorizationException{
		
		PatientStudy patStudy = new PatientStudy();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		
		StudyBean studyBean = studyAgent.getStudyDetails(studyPk);
		String studyNumber = studyBean.getStudyNumber();
		ObjectMap studyObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPk);
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPk);
		
		int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
		
		if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
			if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view patient study");
			
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view patient study"));
			throw new AuthorizationException("User does not have view permission to view patient Study");
		}
		int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();
		//Virendra:#6156, for phi rights(patient privileges for logged user)
		if(!TeamAuthModule.hasViewPermission(patientManagePrivileges)){
			studyIdentifier.setStudyNumber("*");
			studyIdentifier.setOID("*");
		}
		else{
			studyIdentifier.setStudyNumber(studyNumber);
			studyIdentifier.setOID(studyObjectMap.getOID());
		}
		patStudy.setStudyIdentifier(studyIdentifier);
	return 	patStudy;
	}	

	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
}