package com.velos.services.patientstudy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.OperationException;
import com.velos.services.model.StudyIdentifier;
/**
 * Data Access Object dealing with Studies patient is enrolled on.
 * @author Virendra
 *
 */
public class PatientStudyDAO extends CommonDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PatientStudyDAO.class);
	//Virendra:Fixed 6158, modified the query "patprot_stat = 1"
	private static String sql = " select pk_patprot from er_patprot where fk_per=? and patprot_stat = 1 ";
	private static String sql1 = "select study_number from er_study where pk_study = ?";
	
	/**
	 * Returns studyPk from PersonCode. 
	 * @param patientId
	 * @return
	 * @throws OperationException
	 */
	public static ArrayList<Integer> getPatProtPkFromPersonPk(Integer personPk) throws OperationException {
		
		ArrayList<Integer> lstPatProtPK = new ArrayList<Integer>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		int patProtPk = 0;
		try{
			conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug(" sql:" + sql);
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, personPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				patProtPk = rs.getInt("pk_patprot");
				lstPatProtPK.add(patProtPk);
			}
			
		}
		catch(Throwable t){
	
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstPatProtPK;

	}
//public static StudyIdentifier getStudyIdentifierFromStudyPK(int studyPK) throws OperationException{
//	
//	
//	PreparedStatement pstmt = null;
//	Connection conn = null;
//	String studyNumber = null;
//	StudyIdentifier studyId = new StudyIdentifier();
//	try{
//		//patientPK=28629;
//		conn = getConnection();
//
//		if (logger.isDebugEnabled()) logger.debug(" sql:" + sql);
//		
//		
//		pstmt = conn.prepareStatement(sql1);
//		pstmt.setInt(1, studyPK);
//		ResultSet rs = pstmt.executeQuery();
//		while (rs.next()) {
//			studyNumber = rs.getString("study_number");
//			studyId.setStudyNumber(studyNumber);
//		}
//		
//	}
//	catch(Throwable t){
//
//		t.printStackTrace();
//		throw new OperationException();
//		
//	}
//	finally {
//		try {
//			if (pstmt != null)
//				pstmt.close();
//		} catch (Exception e) {
//		}
//		try {
//			if (conn != null)
//				conn.close();
//		} catch (Exception e) {
//		}
//
//	}
//	
//	
//	return studyId;
//	
//}
//	
	
}