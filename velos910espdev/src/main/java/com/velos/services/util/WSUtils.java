package com.velos.services.util;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;



public class WSUtils {

	public static XMLGregorianCalendar dateToXMLCalendar(Date date)
		{
		if (date == null) return null;
		DatatypeFactory dFactory = null;
		try {
			dFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(
					"WSUtils.dateToXMLCalendar(): Error creating datatype factory", 
					e);
		}
		GregorianCalendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(date);
		XMLGregorianCalendar xmlDate = 
			dFactory.newXMLGregorianCalendar(startCalendar);
		
		return xmlDate;
	}
}
