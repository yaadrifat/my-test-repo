/**
 * 
 */
package com.velos.services.calendar;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.catLib.impl.CatLibBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.eventcost.impl.EventcostBean;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.esch.service.util.DateUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Code;
import com.velos.services.model.Cost;
import com.velos.services.model.Duration;
import com.velos.services.model.EventCostIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventIdentifiers;
import com.velos.services.model.EventNames;
import com.velos.services.model.ParentIdentifier;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.VisitIdentifiers;
import com.velos.services.model.VisitNames;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author dylan
 *
 */
@Stateless
@Remote(StudyCalendarService.class)
public class StudyCalendarServiceImpl extends AbstractService implements StudyCalendarService {
	
    private static Logger logger = Logger.getLogger(StudyCalendarServiceImpl.class.getName()); 
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private EventAssocAgentRObj eventAssocAgent; 
	
	@EJB
	private StudyAgent studyAgent;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB 
	private CatLibAgentRObj catlib;
	@EJB 
	private EventcostAgentRObj eventCostAgent;
	@EJB 
	private EventdefAgentRObj eventDefAgent;
	@EJB
	private ProtVisitAgentRObj protVisitAgent; 
	@EJB
	private BudgetAgentRObj budgetAgent; 
	@EJB
	private ProtVisitAgentRObj visitAgent;
	@EJB
	private SiteAgentRObj siteAgent;
	
	
	@Resource
	private SessionContext sessionContext; 
	/**
	 * 
	 */
	public ResponseHolder createStudyCalendar(StudyCalendar studyCalendar) throws OperationException {
		
		
		//Virendra
		try{
		validate(studyCalendar);
		checkGroupUpdatePermissions();
		
		Integer studyPK = 0;
		StudyIdentifier studyIdentifier = studyCalendar.getStudyIdentifier(); 
		if(studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null))
		{
			Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "studyIdentifier is required"); 
			addIssue(issue);
			throw new OperationException(issue.toString());
		}
		studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyCalendar.getStudyIdentifier(),objectMapService);
		if(studyPK == null || studyPK == 0){
			
			StringBuffer errString = new StringBuffer("The Study could not be found for "); 
			if(studyIdentifier.getOID() != null) errString.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
			if(studyIdentifier.getStudyNumber() != null) errString.append(" StudyNumber : " + studyIdentifier.getStudyNumber() )	; 
			Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errString.toString());
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
			
		}
		checkStudyTeamCreatePermissions(studyPK);
		
		EventAssocBean eventAssocBean = new EventAssocBean();
		
		Integer studyCalendarPK = createStudyCalendar(studyCalendar, eventAssocBean);
				
		CalendarIdentifier calendarIdentifier = new CalendarIdentifier();
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, studyCalendarPK);
		calendarIdentifier.setOID(map.getOID()); 
		
		response.addObjectCreatedAction(calendarIdentifier);
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#updateStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.StudyCalendar)
	 */
	public ResponseHolder updateStudyCalendarSummary(
			CalendarIdentifier calendarIdentifier,StudyIdentifier studyIdentifier, String calendarName, CalendarSummary calendarSummary)
			throws OperationException {
		
		try{
			
			checkGroupUpdatePermissions();
			Map<String, Integer> pKMap = locateStudyCalendarForRemoveUpdate(calendarIdentifier, studyIdentifier, calendarName);
			Integer calendarPK = pKMap.get("calendarPK");
			Integer studyPK = pKMap.get("studyPK");
			checkCalendarActiveStatus(calendarPK);
			//Virendra:#7394
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			if(studyIdentifier == null ) studyIdentifier = new StudyIdentifier(); 
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyBean.getId()); 
			studyIdentifier.setOID(map.getOID());
			studyIdentifier.setStudyNumber(studyBean.getStudyNumber()); 
			
			if(calendarSummary == null){
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "CalendarSummary is required"); 
				addIssue(issue);
				throw new ValidationException("CalendarSummary is Null");
			}
	//		validate(calendarSummary); // This check is not required as we are updating and if tag is not sent we are touching that part
			
			Integer updateCalendarFlag = updateStudyCalendarSummary(calendarPK, calendarSummary, studyIdentifier);
			if(updateCalendarFlag != 0){
				addIssue(new Issue(
						IssueTypes.ERROR_UPDATING_STUDY_CALENDAR, 
						"Error while updating Study Calendar"));
				throw new OperationException();
				
			}
			else{
				response.addAction(new CompletedAction("Calendar with ID :"+(calendarIdentifier==null?"":calendarIdentifier.getOID())+
						" or Name :"+calendarName+"  updated successfully", CRUDAction.UPDATE));
			}
			return response;
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
	}

	private Integer updateStudyCalendarSummary(Integer calendarPK, CalendarSummary calendarSummary, StudyIdentifier studyIdentifier) throws OperationException {
		
		EventAssocBean persistentEventAssocBean = eventAssocAgent.getEventAssocDetails(calendarPK);
		
		if(persistentEventAssocBean == null)
		{
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		calendarSummaryIntoBean(calendarPK, studyIdentifier, calendarSummary, persistentEventAssocBean, false);
		Integer eventAssocPK= eventAssocAgent.updateEventAssoc(persistentEventAssocBean);
		return eventAssocPK;
	
	}


	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendar(com.velos.services.model.CalendarIdentifier)
	 */
	public StudyCalendar getStudyCalendar(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName)
	throws OperationException {

		try{

			checkGroupViewPermissions(); 	

			// getStudyId from calendarIdentifier 
			Integer studyPK = 0;
			Integer calendarID = 0;

			//Bug Fix - 6628
			if((calendarIdentifier == null || calendarIdentifier.getOID() == null) && (studyIdentifier == null || calendarName == null || calendarName.length() == 0))
			{

				addIssue(new Issue(
						IssueTypes.DATA_VALIDATION, 
				"CalendarIdentifier Or StudyIdentifier and CalendarName is required to retrieve Calendar"));
				throw new OperationException();

			}


			if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0)
			{
				calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 

			}


			if(calendarID == 0){	

				if(studyIdentifier != null && (studyIdentifier.getOID() != null || studyIdentifier.getStudyNumber() !=null))
				{
					studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
					if(studyPK == null || studyPK == 0 ){
						StringBuffer errString = new StringBuffer("The Study could not be found for "); 
						if(studyIdentifier.getOID() != null) errString.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
						if(studyIdentifier.getStudyNumber() != null) errString.append(" StudyNumber : " + studyIdentifier.getStudyNumber() )	; 

						addIssue(new Issue(
								IssueTypes.STUDY_NOT_FOUND, 
								errString.toString()));
						throw new OperationException();
					} 
					StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
					calendarID = studyCalendarDAO.locateCalendarPK(calendarName, studyPK);
				}

				if(calendarID == 0)
				{
					StringBuffer  errorMsg = new StringBuffer("Calendar not found for"); 
					if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0) errorMsg.append(" CalendarIdentifier OID : " + calendarIdentifier.getOID());
					if(studyIdentifier != null)
					{
						if(studyIdentifier.getOID() != null) errorMsg.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
						if(studyIdentifier.getStudyNumber() != null) errorMsg.append(" StudyNumber : " + studyIdentifier.getStudyNumber() )	; 
					}
					if(calendarName != null && calendarName.length() > 0 ) errorMsg.append(" CalendarName : " + calendarName); 
					Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, errorMsg.toString());
					addIssue(issue);
					logger.error(issue);
					throw new OperationException(issue.toString());
				}

			}

			if(calendarIdentifier == null ) 
				calendarIdentifier = new CalendarIdentifier(); 				

			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, calendarID);
			calendarIdentifier.setOID(map.getOID());
			//getStudyId from eventID 
			EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
			if(calendarBean == null)
			{
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}

			if(studyPK == 0)
			{
				studyPK = EJBUtil.stringToInteger(calendarBean.getChain_id()); 
			}


			//Bug Fix 6544
			checkStudyTeamViewPermissions(studyPK); 

			StudyCalendar calendar = getStudyCalendar(calendarBean); 
			calendar.setCalendarIdentifier(calendarIdentifier); 
			return calendar;

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendar", e);
			throw new OperationException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendar", t);
			throw new OperationException(response.getIssues());

		}


	}
	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarSummary(com.velos.services.model.CalendarIdentifier)
	 */
	public CalendarSummary getStudyCalendarSummary(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName)
	throws OperationException {

		try{
		
		checkGroupViewPermissions(); 	
		
		// getStudyId from calendarIdentifier 
		Integer studyPK = 0;
		Integer calendarID = 0;
		
		//Bug Fix - 6628
		if((calendarIdentifier == null || calendarIdentifier.getOID() == null) && (studyIdentifier == null || calendarName == null || calendarName.length() == 0))
		{

			addIssue(new Issue(
					IssueTypes.DATA_VALIDATION, 
					"CalendarIdentifier Or StudyIdentifier and CalendarName is required to retrieve Calendar"));
			throw new OperationException();

		}
		
		if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0)
		{
			calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
		}else
		{
			calendarIdentifier = new CalendarIdentifier(); 
		}
		
		
		if(calendarID == 0){	
			
			if(studyIdentifier != null  && (studyIdentifier.getOID() != null || studyIdentifier.getStudyNumber() !=null))
			{
				studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
				if(studyPK == null || studyPK == 0 ){
					StringBuffer errString = new StringBuffer("The Study could not be found"); 
					if(studyIdentifier.getOID() != null) errString.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
					if(studyIdentifier.getStudyNumber() != null) errString.append(" StudyNumber : " + studyIdentifier.getStudyNumber()); 
					addIssue(new Issue(
							IssueTypes.STUDY_NOT_FOUND, 
							errString.toString()));
					throw new OperationException();
				} 
				StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
				calendarID = studyCalendarDAO.locateCalendarPK(calendarName, studyPK);
			}			
			if(calendarID == 0)
			{
				StringBuffer  errorMsg = new StringBuffer("Calendar not found for"); 
				if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0) errorMsg.append(" CalendarIdentifier OID : " + calendarIdentifier.getOID());
				if(studyIdentifier != null)
				{
					if(studyIdentifier.getOID() != null) errorMsg.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
					if(studyIdentifier.getStudyNumber() != null) errorMsg.append(" StudyNumber : " + studyIdentifier.getStudyNumber() )	; 
				}
				if(calendarName != null && calendarName.length() > 0 ) errorMsg.append(" CalendarName : " + calendarName);  
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, errorMsg.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
		}
				
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, calendarID);
		if(calendarIdentifier.getOID() == null || calendarIdentifier.getOID().length() == 0) calendarIdentifier.setOID(map.getOID());
		//getStudyId from eventID 
		EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
		if(calendarBean == null)
		{
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		if(studyPK == 0)
		{
			studyPK = EJBUtil.stringToInteger(calendarBean.getChain_id()); 
		}

		checkStudyTeamViewPermissions(studyPK); 
		
		CalendarSummary calendarSummary = getStudyCalendarSummary(calendarBean); 
		
		ParentIdentifier parentIdentifier = new ParentIdentifier(); 
		if(studyIdentifier == null) studyIdentifier = new StudyIdentifier(); 
		if(studyIdentifier.getOID() == null || studyIdentifier.getOID().length() == 0)
		{
			ObjectMap studymap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			studyIdentifier.setOID(studymap.getOID()); 
		}
		
		if(studyIdentifier.getStudyNumber() == null || studyIdentifier.getStudyNumber().length() == 0)
		{
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			studyIdentifier.setStudyNumber(studyBean.getStudyNumber()); 
		}
		parentIdentifier.addIdentifier(studyIdentifier); 
		parentIdentifier.addIdentifier(calendarIdentifier); 
		
		calendarSummary.setParentIdentifier(parentIdentifier); 
		
		return calendarSummary;
		
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl get", e);
			throw new OperationException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl get", t);
			throw new OperationException(response.getIssues());
	
		}
		
		
	}
	
	/**
	 * 
	 * @param calendarBean
	 * @return
	 * @throws OperationException
	 */
	private StudyCalendar getStudyCalendar(EventAssocBean calendarBean) throws OperationException{
		
		 
		
		StudyCalendar calendar = new StudyCalendar(); 
		//StudyIdentifier
		StudyIdentifier studyIdentifier = new StudyIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, EJBUtil.stringToInteger(calendarBean.getChain_id())); 
		studyIdentifier.setOID(map.getOID()); 
		StudyBean studyBean = studyAgent.getStudyDetails(EJBUtil.stringToInteger(calendarBean.getChain_id())); 
		studyIdentifier.setStudyNumber(studyBean.getStudyNumber()); 
		
		calendar.setStudyIdentifier(studyIdentifier);
		calendar.setCalendarSummary(marshallCalendarSummary(calendarBean));		
		calendar.setVisits(marshallVisits(calendarBean.getEvent_id())); 
		
		return calendar; 
	}
	/**
	 * 
	 * @param calendarBean
	 * @return
	 * @throws OperationException
	 */
	private CalendarSummary getStudyCalendarSummary(EventAssocBean calendarBean) throws OperationException{
		
		CalendarSummary calendarSummary= marshallCalendarSummary(calendarBean);		
		return calendarSummary; 
	}
	/**
	 * 
	 * @param protocolID
	 * @return
	 * @throws OperationException
	 */
	private CalendarVisits marshallVisits(Integer protocolID) throws OperationException {
	
		StudyCalendarDAO dao = new StudyCalendarDAO(); 	
		CalendarVisits visits = null; 
		try{
			visits = dao.getStudyCalendarVisits(protocolID, EJBUtil.stringToNum(callingUser.getUserAccountId()), null, null); 	
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			throw new OperationException();
		}
		return visits; 
	}

	/**
	 * 
	 */
//	public ResponseHolder updateStudyCalendarVisit(
//			CalendarIdentifier studyCalendarIdentifier, String visitName,
//			CalendarVisitSummary studyCalendarVisit) throws OperationException {
//		
//		try
//		{
//			checkGroupViewPermissions();
//			
//			Integer visitPK = locateVisit(studyCalendarIdentifier, visitIdentifier, visitName) ; 
//
//			StudyCalendarDAO dao = new StudyCalendarDAO(); 
//			dao.getCalendarStudyPKForVisit(visitPK); 
//			checkStudyTeamViewPermissions(dao.getStudyPK()); 
//			Integer calendarPK= dao.getCalendarPK();
//			checkCalendarActiveStatus(calendarPK);
//
//			CalendarVisit visit = null;
//			try {
//				visit = dao.getStudyCalendarVisit( EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, calendarPK); 
//				//	visit = (CalendarVisit) dao.getStudyCalendarVisits(null, EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, null).getVisit().get(0);
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//			return visit;
//		}catch(OperationException e){
//			sessionContext.setRollbackOnly();
//			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", e);
//			throw new OperationException(response.getIssues());
//
//		}
//		catch(Throwable t){
//			sessionContext.setRollbackOnly();
//			addUnknownThrowableIssue(t);
//			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", t);
//			throw new OperationException(response.getIssues());
//
//		}
//	}
//
//	/* (non-Javadoc)
//	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
//	 */
//	public CalendarVisit getStudyCalendarVisit(CalendarIdentifier calendarIdentifier,
//			VisitIdentifier visitIdentifier, String visitName)
//	throws OperationException {
//		try
//		{
//			checkGroupViewPermissions();
//
//
//
//			Integer visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName) ; 
//
//			StudyCalendarDAO dao = new StudyCalendarDAO(); 
//			dao.getCalendarStudyPKForVisit(visitPK); 
//			checkStudyTeamViewPermissions(dao.getStudyPK()); 
//			Integer calendarPK= dao.getCalendarPK();
//			checkCalendarActiveStatus(calendarPK);
//
//			CalendarVisit visit = null;
//			try {
//				visit = dao.getStudyCalendarVisit( EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, calendarPK); 
//				//	visit = (CalendarVisit) dao.getStudyCalendarVisits(null, EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, null).getVisit().get(0);
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//			return visit;
//		}catch(OperationException e){
//			sessionContext.setRollbackOnly();
//			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", e);
//			throw new OperationException(response.getIssues());
//
//		}
//		catch(Throwable t){
//			sessionContext.setRollbackOnly();
//			addUnknownThrowableIssue(t);
//			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", t);
//			throw new OperationException(response.getIssues());
//
//		}
//	}
	
	public ResponseHolder updateStudyCalendarVisit(
			CalendarIdentifier studyCalendarIdentifier, VisitIdentifier visitIdentifier, String visitName,
			CalendarVisitSummary studyCalendarVisit) throws OperationException {
		
		try
		{
			checkGroupViewPermissions();
			
			Integer visitPK = locateVisit(studyCalendarIdentifier, visitIdentifier, visitName) ; 

			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForVisit(visitPK); 
			checkStudyTeamUpdatePermissions(dao.getStudyPK()); 
			Integer calendarPK= dao.getCalendarPK();
			checkCalendarActiveStatus(calendarPK);
			
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ProtVisitAgentRObj", protVisitAgent); 
			parameters.put("ResponseHolder" , response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("ObjectMapService", objectMapService); 
			//parameters for events
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("ProtVisitAgentRObj", visitAgent);
			parameters.put("SiteAgentRObj", siteAgent);
			
			
			ProtVisitBean visitBean = protVisitAgent.getProtVisitDetails(visitPK);
			CalendarVisitHelper helper = new CalendarVisitHelper();
			
			helper.visitSummaryIntoBeanForUpdate(studyCalendarVisit, visitBean, calendarPK, parameters);
			
			int responseSave = protVisitAgent.updateProtVisit(visitBean); 
			
			
			if(responseSave == 0)
			{
				response.addAction(new CompletedAction("Visit updated", CRUDAction.UPDATE));
				
			}
			else{	
				Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + studyCalendarVisit.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			return response;
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", e);
			throw new OperationException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", t);
			throw new OperationException(response.getIssues());

		}
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
	 */
	public CalendarVisit getStudyCalendarVisit(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName)
	throws OperationException {
		try
		{
			checkGroupViewPermissions();



			Integer visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName) ; 

			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForVisit(visitPK); 
			checkStudyTeamViewPermissions(dao.getStudyPK()); 
			Integer calendarPK= dao.getCalendarPK();
			checkCalendarStatus(calendarPK);

			CalendarVisit visit = null;
			try {
				visit = dao.getStudyCalendarVisit( EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, calendarPK); 
				//	visit = (CalendarVisit) dao.getStudyCalendarVisits(null, EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, null).getVisit().get(0);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return visit;
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", e);
			throw new OperationException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendarVisit", t);
			throw new OperationException(response.getIssues());

		}
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#addVisitsToStudyCalendar(com.velos.services.model.CalendarIdentifier, java.util.List)
	 */
	@TransactionAttribute(REQUIRED)
	public ResponseHolder addVisitsToStudyCalendar(
			CalendarIdentifier calendarIdentifier, CalendarVisits newVisits)
			throws OperationException {
		try
		{
			checkGroupUpdatePermissions(); 
			if(calendarIdentifier == null || calendarIdentifier.getOID() == null && calendarIdentifier.getOID().length() == 0)
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "CalendarIdentifier is required");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			int calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
			//getStudyId from eventID 
			EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
			if(calendarBean == null)
			{
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			checkCalendarActiveStatus(calendarID);
			
			validate(newVisits); 
			
				
			
			Integer studyPK = Integer.parseInt(calendarBean.getChain_id()); 
			checkStudyTeamUpdatePermissions(studyPK); 
			//pass Calendar Duration 
			int duration= EJBUtil.stringToInteger(calendarBean.getDuration()); 
//			Bug Fix 6555 - Duration column is calculated to store in 'D' only. 
//			String durationUnit = calendarBean.getDurationUnit(); 
//			if(durationUnit.equalsIgnoreCase("W"))
//			{
//				duration = duration * 7; 
//			}else if (durationUnit.equalsIgnoreCase("M"))
//			{
//				duration = duration * 30; 
//			}else if(durationUnit.equalsIgnoreCase("Y"))
//			{
//				duration = duration*365; 
//			}
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ProtVisitAgentRObj", protVisitAgent); 
			parameters.put("ResponseHolder" , response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("ObjectMapService", objectMapService); 
			//parameters for events
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("ProtVisitAgentRObj", visitAgent);
			parameters.put("SiteAgentRObj", siteAgent);
		
			CalendarVisitHelper helper = new CalendarVisitHelper(); 
			
			
			helper.addVisitsToCalendar(calendarID, newVisits, duration, parameters); 
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#updateStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String, com.velos.services.model.Event)
	 */
	public ResponseHolder updateStudyCalendarEvent(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName,
			CalendarEventSummary studyCalendarEvent) throws OperationException {
		try{
			
			
			checkGroupUpdatePermissions();
			Integer calendarPK = 0;
			Integer eventPK = 0;
			Integer visitPK = 0;
			Integer studyPK = 0;
			//Virendra: #7236,7458 Rearrangement of logic
			
			eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier, eventName, eventIdentifier);
			if(eventPK != null && eventPK != 0){
				
				EventAssocBean bean = eventAssocAgent.getEventAssocDetails(eventPK);
				String calendarPKStr = bean.getChain_id();
				if(calendarPKStr == null){
					
					addIssue(new Issue(
							IssueTypes.CALENDAR_NOT_FOUND, 
							"Calendar not found for the event"));
					throw new OperationException();
				}
				
				
				String visitPKStr = bean.getEventVisit();
				if(visitPKStr == null){
					
					addIssue(new Issue(
							IssueTypes.VISIT_NOT_FOUND, 
							"Visit not found for the event"));
					throw new OperationException();
				}
				
				calendarPK = EJBUtil.stringToInteger(calendarPKStr);
				visitPK = EJBUtil.stringToInteger(visitPKStr);
				
			}else{
				
				Issue issue = new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			if(studyCalendarEvent == null ){
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Event required to update Calendar event");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			
			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForEvent(eventPK); 
			studyPK = dao.getStudyPK();
			if (studyPK == null || studyPK ==0)
			{
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for the event");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			checkStudyTeamUpdatePermissions(studyPK);
			checkCalendarActiveStatus(calendarPK);
			
			CalendarEventHelper eventHelper = new CalendarEventHelper();
			Integer eventUpdateResult = eventHelper.updateStudyCalendarEvent(studyCalendarEvent, eventPK, calendarPK, visitPK, dao.isAddedBeforeOffline(), getParameters());
			
		
			if(eventUpdateResult == -2){
				response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_CALENDAR_EVENT, "Error occured while updating study calendar event"));
			}
			else{
				if(eventIdentifier == null) eventIdentifier = new EventIdentifier(); 
				if(eventIdentifier.getOID() == null || eventIdentifier.getOID().length() == 0)
				{
					ObjectMap eventMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, eventPK);
					eventIdentifier.setOID(eventMap.getOID());
				}
				response.addAction(new CompletedAction(eventIdentifier, "Event updated", CRUDAction.UPDATE));
		}
	
		
		}catch (OperationException e) {
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		} catch (Throwable t) {
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;	
	}
	
	
	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public CalendarEvent getStudyCalendarEvent(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
	throws OperationException {
		CalendarEvent event = new CalendarEvent(); 
		try
		{
			checkGroupViewPermissions(); 
			Integer eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier, eventName, eventIdentifier); 


			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForEvent(eventPK); 		
			checkStudyTeamViewPermissions(dao.getStudyPK());


			try {

				event = dao.getStudyCalendarEvent(eventPK,  EJBUtil.stringToInteger(callingUser.getUserAccountId())); 

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 


		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendar", e);
			throw new OperationException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl getStudyCalendar", t);
			throw new OperationException(response.getIssues());

		}
		return event;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#addEventsToStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, java.lang.String, com.velos.services.model.VisitIdentifier, java.util.List)
	 */
	public ResponseHolder addEventsToStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier, String visitName,
			VisitIdentifier visitIdentifier,CalendarEvents studyCalendarEvents)
	throws OperationException {
		try{	
			checkGroupUpdatePermissions();
			validate(studyCalendarEvents);

			int visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName); 
			if (visitPK ==0)
			{
				Issue issue = new Issue(IssueTypes.VISIT_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			StudyCalendarDAO dao = new StudyCalendarDAO();
			dao.getCalendarStudyPKForVisit(visitPK);
			Integer studyPK = dao.getStudyPK();
			Integer calendarPK = dao.getCalendarPK(); 
			checkStudyTeamUpdatePermissions(studyPK);
			checkCalendarActiveStatus(calendarPK);

			ProtVisitBean visitBean = protVisitAgent.getProtVisitDetails(visitPK); 

			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ObjectMapService", objectMapService); 
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("ResponseHolder", response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("ProtVisitAgentRObj", visitAgent);
			parameters.put("SiteAgentRObj", siteAgent);
			
			Map<String, Integer> eventNames = new HashMap<String, Integer>(); 	
			int costMax = 0; 
			CalendarEventHelper helper = new CalendarEventHelper();
			helper.populateEventNames(studyCalendarEvents, eventNames, calendarPK, parameters, costMax); 
			helper.addEventsToVisit(studyCalendarEvents, calendarPK, visitPK, visitBean.getDisplacement(), parameters, eventNames); 

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.velos.services.calendar.StudyCalendarService#removeEventFromStudyCalendar
	 * (com.velos.services.model.CalendarIdentifier,
	 * com.velos.services.model.VisitIdentifier, java.lang.String,
	 * com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public ResponseHolder removeEventsFromStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifiers eventIdentifiers, EventNames eventNames)
			throws OperationException {
			try{
			
			//Virendra:#7073, Re-arrangement of logic
			checkGroupUpdatePermissions();
			
			Integer eventPK = 0;
			ArrayList <String> lstEventPK = new ArrayList<String>();
			ArrayList<String> flag = new ArrayList<String>();
			if(eventIdentifiers != null && eventIdentifiers.getEventIdentifier() != null && !eventIdentifiers.getEventIdentifier().isEmpty() && eventIdentifiers.getEventIdentifier().size() >0){
				StudyCalendarDAO dao = new StudyCalendarDAO();
				for(int i= 0; i< eventIdentifiers.getEventIdentifier().size();i++){
					if(eventIdentifiers.getEventIdentifier().get(i).getOID() != null && eventIdentifiers.getEventIdentifier().get(i).getOID().length() > 0){
						eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier,
								null, eventIdentifiers.getEventIdentifier().get(i));
						if(eventPK != null && eventPK != 0){
								dao.getCalendarStudyPKForEvent(eventPK);
								Integer calendarPK = dao.getCalendarPK(); 
								checkCalendarActiveStatus(calendarPK);
							
								lstEventPK.add(EJBUtil.integerToString(eventPK));
								flag.add("0");
							}
						else{
							response.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found in study for event identifier"+eventIdentifiers.getEventIdentifier().get(i).getOID())); 
							}
						
					}
				}
			}
			else if(eventNames != null && eventNames.getEventName() != null && eventNames.getEventName().size() >0){
			
//				if(calendarIdentifier == null || calendarIdentifier.getOID() == null)
//				{
//					addIssue(new Issue(
//							IssueTypes.DATA_VALIDATION, 
//							"CalendarIdentifier is required to retrieve Calendar"));
//					throw new OperationException();
//				}
//				Integer calendarPK = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID());
//				if(calendarPK == null || calendarPK == 0)
//				{
//					Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
//					addIssue(issue);
//					logger.error(issue);
//					throw new OperationException(issue.toString());
//				}
//				checkCalendarActiveStatus(calendarPK);
				
				int visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName); 
				if (visitPK ==0)
				{
					Issue issue = new Issue(IssueTypes.VISIT_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID()+" and Visit Identifier :"+visitIdentifier.getOID());
					addIssue(issue);
					logger.error(issue);
					throw new OperationException(issue.toString());
				}
								
				for(int i= 0; i< eventNames.getEventName().size();i++){
					if(eventNames.getEventName().get(i) != null && eventNames.getEventName().get(i).length() > 0){
						eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier,
								eventNames.getEventName().get(i), null);
						if(eventPK != null && eventPK != 0){
							lstEventPK.add(EJBUtil.integerToString(eventPK));
							flag.add("0");
							}
						else{
							response.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found in study for event name: "+eventNames.getEventName().get(i))); 
							throw new OperationException();
						}
					}
				}
			}
		
			
			Integer studyPK= 0;
						
			for(String eventPK1:  lstEventPK){
				StudyCalendarDAO dao = new StudyCalendarDAO();
				dao.getCalendarStudyPKForEvent(EJBUtil.stringToInteger(eventPK1));
				studyPK= dao.getStudyPK();
				if(studyPK == null || studyPK == 0 ){
					response.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found in study")); 
					throw new OperationException();
				}
				//Virendra:#7104,#7106, #7172
				checkStudyTeamUpdatePermissions(studyPK);
				Integer calendarPK = dao.getCalendarPK(); 
				if(calendarPK == null || calendarPK == 0)
					{
						Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
						addIssue(issue);
						logger.error(issue);
						throw new OperationException(issue.toString());
					}
					checkCalendarActiveStatus(calendarPK);
					if(dao.isAddedBeforeOffline())
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Event cannot be removed after Calendar with this event was activated")); 
						throw new OperationException();
					}
			
			}
			//Virendra: Fixed #7074 
			if(lstEventPK.isEmpty()){
				response.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, 
						"Required calendar identifier and event name or eventidentifier to remove an event")); 
				throw new OperationException();
			
			}
			
			String[] deleteIdsArr= (String[]) lstEventPK.toArray(new String[lstEventPK.size()]);
			String[] deleteIdsFlagsArr= (String[]) flag.toArray(new String[flag.size()]);
			
			Integer deleteFlag = eventDefAgent.deleteEvtOrVisits(deleteIdsArr, "Event_Assoc", deleteIdsFlagsArr, callingUser.getUserId());
			if(deleteFlag == 0){
				response.addAction(new CompletedAction("Events removed", CRUDAction.REMOVE));
			}
			else{
				response.addIssue(new Issue(IssueTypes.ERROR_REMOVING_STUDY_CALENDAR_EVENT, "Error occured while deleting study calendar event"));
		}
	
		
		}catch (OperationException e) {
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		} catch (Throwable t) {
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;

	}
	/**
	 * 
	 * @param calendarBean
	 * @return
	 */
	private CalendarSummary marshallCalendarSummary(EventAssocBean calendarBean) 
	{
	
		CalendarSummary summary = new CalendarSummary(); 
		StudyCalendarDAO dao = new StudyCalendarDAO(); 
		summary.setCalendarDescription(calendarBean.getDescription()); 
		summary.setCalendarDuration(new Duration(Integer.parseInt(calendarBean.getDuration()),TimeUnits.DAY)); 
		summary.setCalendarName(calendarBean.getName()); 
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
				Integer.parseInt(calendarBean.getStatCode()), 
				callingUserAccountId); 
		
		summary.setCalendarStatus(status);
				
		summary.setStatusChangedBy(getUserIdFromPK(EJBUtil.stringToInteger(calendarBean.getStatusUser()), objectMapService, userAgent)); 
		summary.setStatusDate(calendarBean.getStatusDateValue());
		summary.setStatusNotes(calendarBean.getNotes()); 
		
		CatLibBean catlibbean = catlib.getCatLibDetails(calendarBean.getEventCalType()); 
		
		Code catLibCode = new Code(); 
		catLibCode.setType(catlibbean.getCatLibType()); 
		catLibCode.setCode(catlibbean.getCatLibName()); 
		catLibCode.setDescription(catlibbean.getCatLibDesc()); 
		summary.setCalendarType(catLibCode); 	
		
		
		return summary; 
	}
	
	/**
	 * 
	 * @throws OperationException
	 */
	private void checkGroupViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasViewManageProt = 
			GroupAuthModule.hasViewPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies and cant access Study Calendars"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
	}
	/**
	 * 
	 * @throws OperationException
	 */
	private void checkGroupUpdatePermissions() throws OperationException
	{
		//Get the user's permission for updating protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasUpdateManageProt = 
			GroupAuthModule.hasEditPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasUpdateManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to update studies and cant update Study Calendars"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
	}
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */
	private void checkStudyTeamViewPermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasViewStudyCalPermissions = TeamAuthModule.hasViewPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasViewStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view Study Calendar"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
		
		
	}
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */
	private void checkStudyTeamUpdatePermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasUpdateStudyCalPermissions = TeamAuthModule.hasEditPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasUpdateStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to Update Study Calendar"));
			throw new AuthorizationException("User Not Authorized to update Study Calendar");
		}
	}
	
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */

	private void checkStudyTeamCreatePermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasUpdateStudyCalPermissions = TeamAuthModule.hasNewPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasUpdateStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to Create Study Calendar"));
			throw new AuthorizationException("User Not Authorized to create Study Calendar");
		}
	}
	/**
	 * 
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	
	/**
	 * 
	 * @param studyCalendar
	 * @param eventAssocBean
	 * @return
	 * @throws OperationException
	 */
	private Integer createStudyCalendar(
			StudyCalendar studyCalendar,
			EventAssocBean eventAssocBean) 
	throws OperationException{
		//Integer intCalendarCreated= 0;
		Integer studyCalendarPK = 0;
		StudyIdentifier studyId =studyCalendar.getStudyIdentifier();
		//studyCalendar.getCalendarSummary().getCalendarName();
		
		Integer studyPK= ObjectLocator.studyPKFromIdentifier(callingUser, studyId, objectMapService);
		StudyCalendarDAO dao = new StudyCalendarDAO();
		Integer countSimilarCalendars = dao.checkDuplicateCalendarsInStudy(studyCalendar.getCalendarSummary().getCalendarName(), studyPK);
		if(countSimilarCalendars > 0){
				Issue issue = new Issue(IssueTypes.CALENDAR_NAME_ALREADY_EXISTS, "For Calendar ID : " + studyCalendar.getCalendarSummary().getCalendarName());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
		}
		
		
		calendarSummaryIntoBean(0, studyCalendar.getStudyIdentifier(), studyCalendar.getCalendarSummary(),eventAssocBean, true );
		//setEventAssocDetails
		
		//Integer intDuplicateStudyCalendar= eventAssocAgent.copyStudyProtocol(protocolId, protocolName, userId, ipAdd, accId);
		studyCalendarPK = eventAssocAgent.setEventAssocCalendarDetails(eventAssocBean);
		//eventAssocBean.setEvent_id(studyCalendarPK);
		
		if (studyCalendarPK == -2 ){
			sessionContext.setRollbackOnly();
			addIssue(new Issue(IssueTypes.ERROR_CREATING_STUDY_CALENDAR));
			throw new OperationException("Error Creating Study Calendar, Study Calendar already exists");

		}
		// Bug Fix 6537
		if (studyCalendar.getVisits()!=null)
		{
		if(studyCalendar.getVisits().getVisit().size() > 0)
		{
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ProtVisitAgentRObj", protVisitAgent); 
			parameters.put("ResponseHolder" , response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("ObjectMapService", objectMapService); 
			//parameters for events
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("ProtVisitAgentRObj", visitAgent);
			parameters.put("ProtVisitAgentRObj", visitAgent);
			parameters.put("SiteAgentRObj", siteAgent);
			
			CalendarVisits calendarVisits = studyCalendar.getVisits();
			ArrayList <CalendarVisit> lstCalendarVisit = (ArrayList<CalendarVisit>) calendarVisits.getVisit();
			parameters.put("lstCalendarVisit", lstCalendarVisit);
			
			
			CalendarVisitHelper visitHelper = new CalendarVisitHelper(); 
			visitHelper.addVisitsToCalendar(studyCalendarPK, studyCalendar.getVisits(), studyCalendar.getCalendarSummary().getCalendarDuration().getValue(), parameters);
			
		}}
		return studyCalendarPK;
	}
	/**
	 * 
	 * @param calendarPK
	 * @param calendarStudy
	 * @param calendarSummary
	 * @param persistentEventAssocBean
	 * @param isNew
	 * @throws OperationException
	 */
	private void calendarSummaryIntoBean(
			Integer calendarPK, StudyIdentifier calendarStudy,
			CalendarSummary calendarSummary, 
			EventAssocBean persistentEventAssocBean, boolean isNew)
	throws 
	OperationException{		
		persistentEventAssocBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		if (isNew){ 
			persistentEventAssocBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistentEventAssocBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		//Virendra:#7532
		if(calendarSummary.getCalendarDescription() != null){
				persistentEventAssocBean.setDescription(calendarSummary.getCalendarDescription());
			
		}
		
		
		Duration calendarDuration = calendarSummary.getCalendarDuration();
		Integer duration = null;
		TimeUnits durationUnit = null;
		
		if(calendarDuration != null)
		{
			if(calendarDuration.getUnit()  == null)
			{
				addIssue(new Issue(
						IssueTypes.DATA_VALIDATION, 
						"Calendar Duration unit should have valid value"));
				throw new OperationException();
			}
			duration = calendarDuration.getValue();
			durationUnit = calendarDuration.getUnit();
		}
		
		
		//Bug Fixed #6598 & #6637
		if (isNew && (durationUnit == null|| duration ==0||duration <0))
		{
			addIssue(new Issue(
					IssueTypes.DATA_VALIDATION, 
					"Calendar Duration value should be greater than '0'"));
			throw new OperationException();
		}
		
		if(duration != null && durationUnit != null){
			
			
			if(!durationUnit.equals(TimeUnits.DAY))
			{ 
				addIssue(new Issue(
						IssueTypes.DATA_VALIDATION, 
						"Please enter Calendar Duration unit as 'DAY'"));
				throw new OperationException();
			}
			
			String strDurationUnit = Duration.encodeUnit(durationUnit);			
			persistentEventAssocBean.setDuration(EJBUtil.integerToString(duration));
			persistentEventAssocBean.setDurationUnit(strDurationUnit);
			
			
			
		}
		
		//String strDurationUnit = durationUnit.equals(other)
		String calendarName = calendarSummary.getCalendarName();
		
		//Raman: BugFix #10883
		if ((isNew && (calendarName == null || calendarName.length()==0)) ||
				(calendarName != null && calendarName.length()==0))
		{
			addIssue(new Issue(
					IssueTypes.DATA_VALIDATION, 
					"Calendar Name field cannot be empty "));
			throw new OperationException();
		}
		
		if(calendarName != null) persistentEventAssocBean.setName(calendarSummary.getCalendarName());
		
		Integer stdCalendarStatusCodePk=0;
		Integer calTypeCodePk=0;
		
			
		
		//Virendra:#7302
		if(calendarSummary.getCalendarStatus() != null ){
			if( !isNew && calendarSummary.getCalendarStatus().getCode().equalsIgnoreCase("O")){
					String persistCalendarStatusCodePkStr =persistentEventAssocBean.getStatCode();
					if(persistCalendarStatusCodePkStr != null){
					Integer persistCalendarStatusCodePk = EJBUtil.stringToInteger(persistCalendarStatusCodePkStr);
					CodeCache codeCache = CodeCache.getInstance();
					Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
							persistCalendarStatusCodePk, 
							EJBUtil.stringToInteger(callingUser.getUserAccountId()));
					if(status.getCode().equalsIgnoreCase("W")){
						Issue issue = new Issue(IssueTypes.CALENDAR_STATUS_NOT_UPDATEABLE, "Cannot change calendar status from work in progress to offline for editing" );
						addIssue(issue);
						logger.error(issue);
						throw new OperationException(issue.toString());
					}
				}
			}
			// Kanwal Bug fix 7644
			if( !isNew && calendarSummary.getCalendarStatus().getCode().equalsIgnoreCase("W")){
				String persistCalendarStatusCodePkStr =persistentEventAssocBean.getStatCode();
				if(persistCalendarStatusCodePkStr != null){
				Integer persistCalendarStatusCodePk = EJBUtil.stringToInteger(persistCalendarStatusCodePkStr);
				CodeCache codeCache = CodeCache.getInstance();
				Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
						persistCalendarStatusCodePk, 
						EJBUtil.stringToInteger(callingUser.getUserAccountId()));
				if(!status.getCode().equalsIgnoreCase("W")){
					Issue issue = new Issue(IssueTypes.CALENDAR_STATUS_NOT_UPDATEABLE, "Cannot change calendar status to work in progress" );
					addIssue(issue);
					logger.error(issue);
					throw new OperationException(issue.toString());
				}
			}
		}
			try{
				stdCalendarStatusCodePk = dereferenceSchCode(calendarSummary.getCalendarStatus(), CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS, callingUser);
				persistentEventAssocBean.setStatCode(EJBUtil.integerToString(stdCalendarStatusCodePk));
			}catch(CodeNotFoundException e){
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Calendar Status Code not found"));
				throw new OperationException(); 
			
			}
			if(stdCalendarStatusCodePk == 0 || stdCalendarStatusCodePk == null){
				addIssue(
						new Issue(
								IssueTypes.CODE_NOT_FOUND, 
								"Study Calendar Status Code not found"));
					throw new OperationException(); 
				
			}
			
			
			//virendra
			persistentEventAssocBean.setStatusUser(EJBUtil.integerToString(callingUser.getUserId()));
			persistentEventAssocBean.setStatusDate(DateUtil.dateToString(new Date()));
			persistentEventAssocBean.setUser_id(EJBUtil.integerToString(callingUser.getUserId()));
			
		}
		
		
		StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
		if(calendarSummary.getCalendarType() !=  null){
			calTypeCodePk = studyCalendarDAO.locateCatLibPK(calendarSummary.getCalendarType(), CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS_LIB, callingUser.getUserId());
			//Virendra: Fixed#7335
			if(calTypeCodePk == 0){
				addIssue(
						new Issue(
								IssueTypes.CODE_NOT_FOUND, 
								"Study Calendar Type Code not found"));
					throw new OperationException(); 
				
			}
			persistentEventAssocBean.setEventCalType(calTypeCodePk);
		}
		Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, calendarStudy, objectMapService);
		if (studyPK == 0 || studyPK == null){
			Issue issue = new Issue(
						IssueTypes.STUDY_NOT_FOUND, 
						"The Study could not be found");
			addIssue(issue);
			throw new OperationException();
		} 
		persistentEventAssocBean.setChain_id(EJBUtil.integerToString(studyPK));
		persistentEventAssocBean.setEvent_type(CodeCache.CODE_STUDY_CALENDAR_TYPE);
		persistentEventAssocBean.setCalAssocTo(CodeCache.CODE_STUDY_PATIENT_CALENDAR_TYPE);
	}
	/**
	 * 
	 * @param calendarIdentifier
	 * @param visitName
	 * @param visitIdentifier
	 * @param eventName
	 * @param eventIdentifier
	 * @return
	 * @throws OperationException
	 */
	private Integer locateEvent(CalendarIdentifier calendarIdentifier, 
			String visitName,
			VisitIdentifier visitIdentifier,
			String eventName, 
			EventIdentifier eventIdentifier) throws OperationException{
		
		Integer eventPK = null; 
		if(eventIdentifier != null && eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
		{
			eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
			if(eventPK == null || eventPK == 0)
			{ 
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
					"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
				throw new OperationException(); 
			}
			return eventPK; 			
		}
		
		if(eventName != null && eventName.length() > 0)
		{
			Integer visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName); 
		
			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			eventPK = dao.getEventPKByEventName(eventName, visitPK); 
			if(eventPK == null || eventPK == 0)
			{
				StringBuffer errorMessage = new StringBuffer("Event not found with Event Name "+eventName +"for Visit "); 
				if(visitIdentifier != null && visitIdentifier.getOID() != null)
				{
					errorMessage.append(" OID" + visitIdentifier.getOID()); 
				}
				else
				{
					errorMessage.append(" Name " + visitName + " CalendarIdentifier OID : " + calendarIdentifier.getOID()); 
				}
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, errorMessage.toString())); 
				throw new OperationException(); 
			}
				
			return eventPK; 
		}
			
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "EventIdentifer OR EventName and VisitIdentifer" +
				" OR EventName and VisitName and CalendarIdentifier is required to find event")); 
		throw new OperationException(); 
		
	}
	/**
	 * 
	 * @param calendarIdentifier
	 * @param visitIdentifier
	 * @param visitName
	 * @return
	 * @throws OperationException
	 */
	private Integer locateVisit(CalendarIdentifier calendarIdentifier, VisitIdentifier visitIdentifier, String visitName) throws OperationException
	{
		
			Integer visitPK = null; 
			if(visitIdentifier != null && visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
			{
				visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID()); 
				if(visitPK == null || visitPK == 0)
				{
					addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found for Visit Identifier - OID " + visitIdentifier.getOID())); 	
					throw new OperationException(); 
				}
				return visitPK; 
			}
			
			if(visitName != null && visitName.length() > 0 && calendarIdentifier != null 
					&& calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0 )
			{
				Integer calendarPK = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID());
				if(calendarPK == null || calendarPK == 0)
				{
					addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found for OID "+ calendarIdentifier.getOID())); 
					throw new OperationException();
				}
				StudyCalendarDAO dao = new StudyCalendarDAO(); 
				visitPK = dao.getVisitPKByName(visitName, calendarPK); 
				if(visitPK == null || visitPK == 0)
				{
					addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found for VisitName " + visitName + 
						" and CalendarIdentifier OID " + calendarIdentifier.getOID() )); 
					throw new OperationException(); 
				}
				
				return visitPK; 
			}
			//Bug Fix 6635			
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "VisitIdentifier OR VisitName and CalendarIdentifer" +
			" is required to find visit")); 
			throw new OperationException(); 
			
	}
	/**
	 * 
	 */
//	public ResponseHolder removeEventsFromStudyCalendarVisit(
//			CalendarIdentifier calendarIdentifier,
//			VisitIdentifier visitIdentifier, String visitName,
//			EventIdentifiers eventIdentifiers, EventNames eventNames)
//			throws OperationException {
//		if(eventIdentifiers != null && eventIdentifiers.getEventIdentifier() != null && !eventIdentifiers.getEventIdentifier().isEmpty() && eventIdentifiers.getEventIdentifier().size() >0){
//			for(int i= 0; i< eventIdentifiers.getEventIdentifier().size();i++){
//				if(eventIdentifiers.getEventIdentifier().get(i).getOID() != null && !eventIdentifiers.getEventIdentifier().get(i).getOID().equals("")){
//					response = removeEventFromStudyCalendar(calendarIdentifier, visitIdentifier, visitName, eventIdentifiers.getEventIdentifier().get(i), null);
//				}
//			}
//		}
//		if(eventNames != null && eventNames.getEventName() != null && eventNames.getEventName().size() >0){
//			for(int i= 0; i< eventNames.getEventName().size();i++){
//				if(eventNames.getEventName().get(i) != null && !eventNames.getEventName().get(i).equals("")){
//					response = removeEventFromStudyCalendar(calendarIdentifier, visitIdentifier, visitName, null, eventNames.getEventName().get(i));
//				}
//			}
//		}
//		return response;
//	}
	
	/**
	 * 
	 */
	public ResponseHolder removeVisitsFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifiers visitIdentifiers, VisitNames visitNames)
			throws OperationException {
		try {
			
		
			checkGroupUpdatePermissions();

			Integer calendarPK = 0;
			Integer visitPK = 0;
			Integer calendarStudyPK = null;
			ProtVisitDao protVisitDao = new ProtVisitDao(); 
			ArrayList<String> lstVisitPK = new ArrayList<String>();
			ArrayList<String> flag = new ArrayList<String>();
			//Virendra:#7119,7125,7114,7145 Rearranging and adding logic.
			//Virendra:#7120, 7148
			if(visitIdentifiers != null && visitIdentifiers.getVisitIdentifier() != null 
					&& visitIdentifiers.getVisitIdentifier().size() >  0 
					&& visitIdentifiers.getVisitIdentifier().get(0).getOID() != null
					&& visitIdentifiers.getVisitIdentifier().get(0).getOID().length() > 0){
				
			for(VisitIdentifier visitIdentifier: visitIdentifiers.getVisitIdentifier()){
				if(visitIdentifier.getOID().length() > 0){
						visitPK = locateVisit(calendarIdentifier, visitIdentifier, null);
							if(visitPK != null && visitPK != 0){
								ProtVisitBean visitBean = protVisitAgent.getProtVisitDetails(visitPK);
								
								if(visitBean == null){
									response.addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, 
											"Visit not found in study for visit identifier :"+visitIdentifier.getOID())); 
									throw new OperationException();
								}
								//Bug Fix 7168
								if(protVisitDao.getProtocolVisitChildCount(visitPK) > 0)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, 
											" Visit(s) Cannot be deleted, referred by one or more child visits")); 
									throw new OperationException();
								}
								calendarPK= visitBean.getProtocol_id();
								if(calendarPK != null){
									checkCalendarActiveStatus(calendarPK);
									EventAssocBean eventAssocBean = eventAssocAgent.getEventAssocDetails(calendarPK);
									String calendarStudyPKStr = eventAssocBean.getChain_id();
									if(calendarStudyPKStr != null){
										calendarStudyPK = EJBUtil.stringToInteger(calendarStudyPKStr);
										checkStudyTeamUpdatePermissions(calendarStudyPK);
									}
								}
								lstVisitPK.add(EJBUtil.integerToString(visitPK));
								}
							else{
								response.addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found in study for visit identifier :"+visitIdentifier.getOID())); 
								throw new OperationException();
							}
						}
						else{
							response.addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found in study for visit identifier :"+visitIdentifier.getOID())); 
							throw new OperationException();
						}
					}
			}
			else if(calendarIdentifier != null  && calendarIdentifier.getOID() != null  && 
					calendarIdentifier.getOID().length() > 0 && visitNames != null && visitNames.getVisitName() != null 
					&& visitNames.getVisitName().size() > 0
					&& !visitNames.getVisitName().get(0).trim().equals("")){
				
					//Virendra: Fixed #7120,7138,
					
				calendarPK = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID());
				EventAssocBean eventAssocBean = eventAssocAgent.getEventAssocDetails(calendarPK);
				if(eventAssocBean == null)
				{
					Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found for calendar ID :"+calendarIdentifier.getOID());
					addIssue(issue);
					logger.error(issue);
					throw new OperationException(issue.toString());
				}
			
				calendarStudyPK = EJBUtil.stringToInteger(eventAssocBean.getChain_id());
				
				if(calendarStudyPK == null){
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND,"Study not found for calendar Id"+calendarIdentifier.getOID()));
					throw new OperationException();
				}
			
				checkStudyTeamUpdatePermissions(calendarStudyPK);
				checkCalendarActiveStatus(calendarPK);
				
				for(String visitName: visitNames.getVisitName()){
				
					visitPK = locateVisit(calendarIdentifier, null, visitName); 
					if(visitPK != null && visitPK != 0){
						//Bug Fix 7168
						if(protVisitDao.getProtocolVisitChildCount(visitPK) > 0)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, 
									" Visit(s) Cannot be deleted, referred by one or more child visits")); 
							throw new OperationException();
						}
						lstVisitPK.add(EJBUtil.integerToString(visitPK));
						}
					else{
						response.addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found in study for visit name :"+visitName)); 
						throw new OperationException();
					}
				}
				
			}
			else{
				addIssue(new Issue(
					IssueTypes.DATA_VALIDATION, 
					"VisitIdentifier or CalendarIdentifier and VisitName is required to remove Visit"));
				throw new OperationException();
			}
			StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
			ArrayList<Integer> lstEventPK = null;
			ArrayList<String> lstEventPKAll = new ArrayList<String>();
			for(String visitPK1: lstVisitPK){
				lstEventPK = new ArrayList<Integer>();
				lstEventPK = studyCalendarDAO.geEventPKsForVisit(EJBUtil.stringToInteger(visitPK1), calendarStudyPK, calendarPK);
				studyCalendarDAO.getCalendarStudyPKForVisit(visitPK); 
				if(studyCalendarDAO.isAddedBeforeOffline())
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Visit cannot be removed after Calendar with this Visit was activated")); 
					throw new OperationException();
				}
				if(!lstEventPK.isEmpty()){
				ArrayList<String> lstEventPKStr = new ArrayList<String>(lstEventPK.size()); 
					for (Integer eventPK : lstEventPK) { 
						lstEventPKStr.add(EJBUtil.integerToString(eventPK));
						flag.add("0");
					}
				lstEventPKAll.addAll(lstEventPKStr);
				}
			}
			
			for(String visitPK1: lstVisitPK){
				lstEventPKAll.add(visitPK1);
				flag.add("1");
			}
			Integer deleteFlag = 
				eventDefAgent.deleteEvtOrVisits(
						(String[])lstEventPKAll.toArray(new String[lstEventPKAll.size()]), "event_assoc", 
						(String[])flag.toArray(new String[flag.size()]), callingUser.getUserId());
			
			if(deleteFlag == 0){
				response.addAction(new CompletedAction("events and visits deleted successfully", CRUDAction.REMOVE));
			}
			else{
				response.addIssue(new Issue(IssueTypes.ERROR_REMOVING_STUDY_CALENDAR, "Error occured while deleting events and visits"));
			}
			
		} catch (OperationException e) {
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		} catch (Throwable t) {
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	
	}
	// Begin: Bug Fix # 9795: Tarandeep Singh Bali
	private String checkCalendarStatus(Integer calendarPK) throws OperationException{
		String statusStr = null; 
		EventAssocBean bean = eventAssocAgent.getEventAssocDetails(calendarPK);
		if(bean == null)
		{
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
				Integer.parseInt(bean.getStatCode()), 
				callingUserAccountId);
				//Virendra: Fixed#7146,7147 
		if(status.getCode().equalsIgnoreCase("D")){
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_ACTIVE, "Cannot Get Study Calendar Visit, Calendar status 'DEACTIVATED' or 'OFFLINE FOR EDITING'" );
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		return status.getCode(); 
	}
	//End: Bug Fix # 9795: Tarandeep Singh Bali
	private String checkCalendarActiveStatus(Integer calendarPK) throws OperationException{
		String statusStr = null; 
		EventAssocBean bean = eventAssocAgent.getEventAssocDetails(calendarPK);
		if(bean == null)
		{
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
				Integer.parseInt(bean.getStatCode()), 
				callingUserAccountId);
				//Virendra: Fixed#7146,7147 
		if(status.getCode().equalsIgnoreCase("A")|| status.getCode().equalsIgnoreCase("D") ){
			Issue issue = new Issue(IssueTypes.CALENDAR_ACTIVE_NOT_UPDATEABLE, "Cannot Add/Edit/Remove, Calendar status 'ACTIVE' or 'DEACTIVATED' or 'OFFLINE FOR EDITING'" );
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		return status.getCode(); 
	}
	
	public ResponseHolder removeStudyCalendar(
			CalendarIdentifier calendarIdentifier, 
			StudyIdentifier studyIdentifier, String calendarName)
	throws OperationException
	{
		
		try{
			checkGroupUpdatePermissions();
			
			Map<String, Integer> pKMap = locateStudyCalendarForRemoveUpdate(
					calendarIdentifier, studyIdentifier, calendarName);
			Integer calendarPK = pKMap.get("calendarPK");
			Integer studyPK = pKMap.get("studyPK");
			Integer calendarRemoveFlag = null;
			
			if(calendarPK != null && studyPK != null){
				calendarRemoveFlag=  eventAssocAgent.studyCalendarDelete(studyPK, calendarPK);
			}
			//Virendra-Fixed #7141,7060,
			if(calendarRemoveFlag != 0){
				if(calendarRemoveFlag == -2 
						||calendarRemoveFlag == -3 
						||calendarRemoveFlag == -4 ){
					
					addIssue(new Issue(
							IssueTypes.ERROR_REMOVING_STUDY_CALENDAR, 
							"Cannot remove, either patient enrolled or " +
							"budget/milestone linked with study calendar"));
					
				}
				else {
					addIssue(new Issue(
							IssueTypes.ERROR_REMOVING_STUDY_CALENDAR, 
							"Error while removing study calendar"));
					
				}
				throw new OperationRolledBackException(response.getIssues());
			}
			else{
				response.addAction(new CompletedAction(
						"Study Calendar removed successfully", CRUDAction.REMOVE));
				
			}
			
		}catch (OperationException e) {
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl removeStudyCalendar", e);
			throw new OperationRolledBackException(response.getIssues());

		} catch (Throwable t) {
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("StudyCalendarServiceImpl removeStudyCalendar", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
		
	}
	private Map<String, Integer> locateStudyCalendarForRemoveUpdate(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName) throws OperationException{
		
		Integer calendarID = 0;
		Integer studyPK = 0;
		Map<String, Integer> pKMap = new HashMap<String, Integer>();
		if(calendarIdentifier == null || calendarIdentifier.getOID() == null )
		{
			if((studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)) 
					|| calendarName == null){
				addIssue(new Issue(
						IssueTypes.DATA_VALIDATION, 
						"CalendarIdentifier Or StudyIdentifier and CalendarName is required to retrieve Calendar"));
				throw new OperationException();
			}
		}
		//Virendra:#7071
		if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0)
		{
			calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
			EventAssocBean bean = eventAssocAgent.getEventAssocDetails(calendarID);
			if(bean == null)
			{
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Study calendar not found for Calendar Id :"+calendarIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			String studyPKStr = bean.getChain_id();
			if(studyPKStr == null){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND,"Study not found for calendar Id"+calendarIdentifier.getOID()));
				throw new OperationException();
			}
			studyPK = EJBUtil.stringToInteger(studyPKStr);
			
		}
		//Virendra: #7394
		if(calendarID == 0 || calendarID == null){
				studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
				if(studyPK == null || studyPK == 0){
					//Bug Fix 10880
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND,"Study not found for calendar"));
					throw new OperationException();					
				}
				if(studyPK != null && studyPK != 0){
					StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
					calendarID = studyCalendarDAO.locateCalendarPK(calendarName, studyPK);
				}
				
					
			if(calendarID == 0)
			{
				StringBuffer  errorMsg = new StringBuffer("Calendar not found for"); 
				if(calendarIdentifier != null  && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0) errorMsg.append(" CalendarIdentifier OID : " + calendarIdentifier.getOID());
				if(studyIdentifier != null)
				{
					if(studyIdentifier.getOID() != null) errorMsg.append(" StudyIdentifier OID : " + studyIdentifier.getOID()); 
					if(studyIdentifier.getStudyNumber() != null) errorMsg.append(" StudyNumber : " + studyIdentifier.getStudyNumber() )	; 
				}
				if(calendarName != null && calendarName.length() > 0 ) errorMsg.append(" CalendarName : " + calendarName); 
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, errorMsg.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
		
		}
		checkStudyTeamUpdatePermissions(studyPK);
		pKMap.put("calendarPK", calendarID);
		pKMap.put("studyPK", studyPK);
		return pKMap;
	
	}
	
	public ResponseHolder removeEventCost(EventCostIdentifier eventCostIdentifier) throws OperationException{
		
		try{
			
			checkGroupUpdatePermissions(); 
			HashMap<String, Integer> map = getCostPKEventPKForEventCost(eventCostIdentifier);
			Integer eventCostPK = map.get("eventCostPK");
			Integer eventPK = map.get("eventPK");
			
			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForEvent(eventPK);  
			Integer studyPK = dao.getStudyPK();
			
			if (studyPK == null || studyPK ==0)
			{
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for the event");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			checkStudyTeamUpdatePermissions(studyPK); 
			
			Integer eventCostID= eventDefAgent.removeEventCost(eventPK, eventCostPK, "D");
			if (eventCostID != 1) {
				response.getIssues()
						.add(new Issue(
								IssueTypes.ERROR_REMOVING_EVENT_COST,
								"Error removing event costs"));
				throw new OperationRolledBackException(response.getIssues());
			}
			else{
				response.addAction(new CompletedAction(
						"Event cost removed successfully", CRUDAction.REMOVE));
			}
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		
		return response;
	}
	
	public ResponseHolder updateEventCost(EventCostIdentifier eventCostIdentifier, Cost cost) throws OperationException{
		
		try{
			
			checkGroupUpdatePermissions(); 
			HashMap<String, Integer> map = getCostPKEventPKForEventCost(eventCostIdentifier);			
			Integer eventCostPK = map.get("eventCostPK");
			Integer eventPK = map.get("eventPK");
			
			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			dao.getCalendarStudyPKForEvent(eventPK);  
			Integer studyPK = dao.getStudyPK();
			
			if (studyPK == null || studyPK ==0)
			{
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for the event");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			checkStudyTeamUpdatePermissions(studyPK); 
			
			CalendarEventHelper helper = new CalendarEventHelper();
			Integer eventID = helper.updateEventCost(cost, eventPK, eventCostPK, getParameters());
			
			if (eventID == 0) {
				response.addAction(new CompletedAction(
						"Event cost updated successfully", CRUDAction.UPDATE));
			}
			else{
				response.getIssues()
				.add(new Issue(
						IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
						"Error creating event costs"));
				throw new OperationRolledBackException(response.getIssues());
			}
		
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
		
	}
	
	public ResponseHolder addEventCost(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName,
			Cost cost) throws OperationException{
		
		try{
		validateCost(cost);
		
		checkGroupUpdatePermissions();
		
		Integer calendarPK = 0;
		Integer eventPK = 0;
		Integer visitPK = 0;
		Integer studyPK = 0;
		
		eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier, eventName, eventIdentifier);
		
		eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier, eventName, eventIdentifier);
		if(eventPK != null && eventPK != 0){
			
			EventAssocBean bean = eventAssocAgent.getEventAssocDetails(eventPK);
			String calendarPKStr = bean.getChain_id();
			if(calendarPKStr == null){
				
				addIssue(new Issue(
						IssueTypes.CALENDAR_NOT_FOUND, 
						"Calendar not found for the event"));
				throw new OperationException();
			}
			
			
			String visitPKStr = bean.getEventVisit();
			if(visitPKStr == null){
				
				addIssue(new Issue(
						IssueTypes.VISIT_NOT_FOUND, 
						"Visit not found for the event"));
				throw new OperationException();
			}
			
			calendarPK = EJBUtil.stringToInteger(calendarPKStr);
			visitPK = EJBUtil.stringToInteger(visitPKStr);
			
		}else{
			
			Issue issue = new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}		
		
		StudyCalendarDAO dao = new StudyCalendarDAO(); 
		dao.getCalendarStudyPKForEvent(eventPK); 
		studyPK = dao.getStudyPK();
		if (studyPK == null || studyPK ==0)
		{
			Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for the event");
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		checkStudyTeamUpdatePermissions(studyPK);
// Bug fixed 
//		checkCalendarActiveStatus(calendarPK); 
		
		if (cost != null) {
			
			CalendarEventHelper calendarEventHelper = new CalendarEventHelper();
			Integer eventCostID=  calendarEventHelper.addEventCost(cost, eventPK, getParameters());
			if (eventCostID == -2 || eventCostID == 0) {
				response.getIssues()
				.add(new Issue(
						IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
						"Error creating event costs"));
				throw new OperationRolledBackException(response.getIssues());
			}
			else{
							
				EventCostIdentifier eventCostIdentifier = new EventCostIdentifier();
				ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_COST, eventCostID); 
				eventCostIdentifier.setOID(map.getOID()); 
				response.addObjectCreatedAction(eventCostIdentifier); 
//				response.addAction(new CompletedAction(
//						"Event cost added successfully", CRUDAction.CREATE));
				
			}
		}
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl remove", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	
	public Map<String, Object> getParameters(){
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("ProtVisitAgentRObj", protVisitAgent); 
		parameters.put("ResponseHolder" , response); 
		parameters.put("StudyCalendarServiceImpl", this); 
		parameters.put("ObjectMapService", objectMapService); 
		//parameters for events
		parameters.put("EventdefAgentRObj", eventDefAgent); 
		parameters.put("EventAssocAgentRObj", eventAssocAgent); 
		parameters.put("EventcostAgentRObj", eventCostAgent); 
		parameters.put("callingUser", callingUser);
		parameters.put("sessionContext", sessionContext); 
		parameters.put("ProtVisitAgentRObj", visitAgent);
		parameters.put("SiteAgentRObj", siteAgent);
		
		
		return parameters;
	}

	public void validateCost(Cost cost) throws OperationException{
		if(cost == null 
				|| cost.getCost() == null 
				|| cost.getCost().toString().length() == 0
				|| cost.getCostType() == null
				|| cost.getCurrency() == null){
			
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Event Cost is required")); 	
			throw new OperationException();
			
		}
		
	};
	
	public HashMap<String, Integer> getCostPKEventPKForEventCost(EventCostIdentifier eventCostIdentifier) throws OperationException{
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		Integer eventCostPK = null; 
		Integer eventPK = null;
		if(eventCostIdentifier != null && eventCostIdentifier.getOID() != null && eventCostIdentifier.getOID().length() > 0)
		{
			eventCostPK = objectMapService.getObjectPkFromOID(eventCostIdentifier.getOID()); 
			if(eventCostPK == null || eventCostPK == 0)
			{
				addIssue(new Issue(IssueTypes.EVENT_COST_NOT_FOUND, "Event Cost not found for Event Cost Identifier - OID " + eventCostIdentifier.getOID())); 	
				throw new OperationException(); 
			}
			EventcostBean eventcostBean = eventCostAgent.getEventcostDetails(eventCostPK);
			if(eventcostBean == null)
			{
				addIssue(new Issue(IssueTypes.EVENT_COST_NOT_FOUND, "Event Cost not found for Event Cost Identifier - OID " + eventCostIdentifier.getOID())); 	
				throw new OperationException(); 
			}
			String eventPKStr = eventcostBean.getEventId();
			
			if(eventPKStr == null || eventPKStr.length() == 0){
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found for Event Cost Identifier - OID " + eventCostIdentifier.getOID())); 	
				throw new OperationException(); 
				
			}
			eventPK = EJBUtil.stringToInteger(eventPKStr);
			
		}
		else{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Event Cost Identifier is required to remove Event Cost")); 	
			throw new OperationException();
		}
		map.put("eventCostPK", eventCostPK);
		map.put("eventPK", eventPK);
		
		return map;
		
	};
	
}
