/**
 * 
 */
package com.velos.services.study;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * The SummaryCLOB EJB exists to persist Clob fields from
 * er_study. eResearch persists them in a separate transaction through
 * a stored procedure. This creates issues in the service layer as 
 * all persistence is done in the container-managed transaction. So,
 * instead of going through the stored procedure for these fields, 
 * we create EJBs to handle them.
 * 
 * @author dylan
 *
 */

@Stateless
public class SummaryCLOBServiceImpl implements SummaryCLOBService{

	@PersistenceContext(unitName = "eres")
    protected EntityManager em;

	/* (non-Javadoc)
	 * @see com.velos.services.study.SummaryCLOBService#updateCLOB(com.velos.services.study.SummaryCLOB)
	 */
	public void updateCLOB(SummaryCLOB summaryCLOB) {
		em.merge(summaryCLOB);
		
	}

}
