package com.velos.services.studypatient;


import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.ConfigDetailsObject;
import com.velos.eres.web.user.ConfigFacade;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Code;
import com.velos.services.model.PatientStudyStatusIdentifier;

public class StudyPatientHelper extends CommonDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8892283816684237784L;
	private static Logger logger = Logger.getLogger(StudyPatientHelper.class);
	 
	
	private boolean isMandatory(ConfigDetailsObject configDetails, String field)
	{
		boolean isMandatory = false;
		ArrayList fields = configDetails.getPcfField(); 
		ArrayList mandatory = configDetails.getPcfMandatory();
		
		if(fields == null || fields.indexOf(field) == -1)
		{
			
			if(field.equals("patstatus")) return true; 
			if(field.equals("statusdate")) return true; 
			if(field.equals("enrolledby")) return true; 
			
		}else
		{
			if(mandatory.get(fields.indexOf(field)).toString().equals("1")) return true; 
		}
	
		
		return isMandatory; 
	}


	public void updateStudyPatientStatus(
			PatStudyStatBean patStudyStatBean,
			Code reason, Date validFrom, boolean isCurrentStatus, String notes, Map<String, Object> parameters) throws OperationException
	{
		PatStudyStatAgentRObj patStudyStatAgent= (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent");
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Integer perPK=(Integer) parameters.get("perPK");
		Integer studyPK=(Integer) parameters.get("studyPK");
		ConfigDetailsObject configDetails =ConfigFacade.getConfigFacade().populateObject(StringUtil.stringToInteger(callingUser.getUserAccountId()).intValue(), "patstudystatus");
		
		if(isCurrentStatus)
		{
			PatStudyStatBean currentPatStatBean = patStudyStatAgent.getPatStudyCurrentStatus(StringUtil.integerToString(studyPK),StringUtil.integerToString(perPK));
			currentPatStatBean.setCurrentStat("0");
			currentPatStatBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			if(patStudyStatAgent.updatePatStudyStat(currentPatStatBean)<0)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add((new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS, "Study Patient status could not be updated successfully"))); 
				throw new OperationException();
			}
			
			patStudyStatBean.setCurrentStat("1");
		}
		if(reason!=null)
		{
			if((reason.getCode() == null || reason.getCode().trim().length()==0))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add((new Issue(IssueTypes.DATA_VALIDATION, "Reason code may not be null"))); 
				throw new OperationException(); 
			}
			CodeDao codeDao=new CodeDao();
			String statusReason="";
			String statusType= codeDao.getCodeSubtype(StringUtil.stringToInteger(patStudyStatBean.getPatStudyStat()));
			if(!reason.getType().equalsIgnoreCase(statusType))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add((new Issue(IssueTypes.CODE_NOT_FOUND, "Reason Code not found: " + reason.getCode()))); 
				throw new OperationException();
			}
			try
			{
				statusReason= AbstractService.dereferenceCodeStr(reason, statusType, callingUser);		
						 
			}catch(CodeNotFoundException ce)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.CODE_NOT_FOUND,"Reason Code Not Found: " + reason.getCode()));
				throw new OperationException(); 
			}
			
			patStudyStatBean.setPatStudyStatReason(statusReason);
		}
		if(!StringUtil.isEmpty(notes))
		{
			patStudyStatBean.setNotes(notes);
		}
		else if(StringUtil.isEmpty(notes) && isMandatory(configDetails, "notes"))
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION,"Field notes may not be null"));
			throw new OperationException();
		}
		else if(StringUtil.isEmpty(notes) && (!isMandatory(configDetails, "notes")))
		{
			if(notes!=null && notes.trim().length()==0)
			patStudyStatBean.setNotes("");
		}
		
		if(validFrom!=null)
		{
			patStudyStatBean.setStartDate(validFrom);
		}
		
		//-------------Updating study patient status---------//
		if(patStudyStatAgent.updatePatStudyStat(patStudyStatBean)<0)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS, "Study Patient status could not be updated successfully")); 
			throw new OperationException();
		}
		
	}
}
