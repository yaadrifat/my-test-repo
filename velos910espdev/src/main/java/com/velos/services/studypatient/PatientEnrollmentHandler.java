/**
 * Created On Jun 30, 2011
 */
package com.velos.services.studypatient;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.user.ConfigDetailsObject;
import com.velos.eres.web.user.ConfigFacade;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.patientdemographics.PatientDemographicsHelper;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
public class PatientEnrollmentHandler {
	
	private static Logger logger = Logger.getLogger(PatientEnrollmentHandler.class); 
	
	private int enrollPatientToStudy(PatientEnrollmentDetails patientEnrollmentDetails
			,Map<String,Object> parameters, int patientPK, Integer studyPK, Map<String, Object> patientData) throws OperationException
	{
		
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		PersonBean personB = personAgent.getPersonDetails(patientPK); 		
     	PatProtBean patProtBean = new PatProtBean();
		PatStudyStatBean protStudyStatbean = new PatStudyStatBean(); 	
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Date patientDOB = (Date) patientData.get("patientDOB"); 
		String patientCode = patientData.get("patientCode").toString(); 
		if(patientEnrollmentDetails.getPatientStudyId() == null || patientEnrollmentDetails.getPatientStudyId().length() == 0)
		{
			patientEnrollmentDetails.setPatientStudyId(patientCode);
		}
		validatePatientEnrollmentDetails(patientEnrollmentDetails, callingUser, parameters, patientDOB); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder"); 
		//PatProt data
		patProtBean.setPatProtPersonId(EJBUtil.integerToString(patientPK));
		patProtBean.setPatProtStudyId(EJBUtil.integerToString(studyPK)); 
		//Enrollment Details - if status = enrolled
		
		if(patientEnrollmentDetails.getStatus().getCode().equals("enrolled"))
		{
			patProtBean.setPatProtRandomNumber(patientEnrollmentDetails.getRandomizationNumber());
			
			patProtBean.setPatProtEnrolDt(patientEnrollmentDetails.getStatusDate()); 

			if(patientEnrollmentDetails.getEnrolledBy() != null)
			{
				UserBean enrollingUserBean; 			
				try{
					enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getEnrolledBy(), userAgent, sessionContext, objectMapService);
					if(enrollingUserBean != null)
					{
						patProtBean.setPatProtUserId(EJBUtil.integerToString(enrollingUserBean.getUserId())); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.USER_NOT_FOUND ,
								"EnrolledBy User Not found"));
						throw new OperationException();
					}
				} catch (MultipleObjectsFoundException e) {

					responseHolder.addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched Enrolling User"));
					throw new OperationException();
				} 

			}
		}
		
		//Informed Consent Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("infConsent"))
		{
			 protStudyStatbean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber()) ; 	
		}

		//Follow-up Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("followup"))
		{
			protStudyStatbean.setPtstNextFlwupPersistent(patientEnrollmentDetails.getNextFollowUpDate());
		}
		
		//Screening Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("screening"))
		{
			protStudyStatbean.setScreenNumber(patientEnrollmentDetails.getScreenNumber());
			
			if(patientEnrollmentDetails.getScreenedBy() != null)
			{
				UserBean screeningUserBean; 			
				try{
					screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getScreenedBy(), userAgent, sessionContext, objectMapService);
					if(screeningUserBean != null)
					{
						protStudyStatbean.setScreenedBy(EJBUtil.integerToString(screeningUserBean.getUserId())); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.USER_NOT_FOUND, 
								"ScreenBy User not found"));
						throw new OperationException();
					}
				} catch (MultipleObjectsFoundException e) {

					responseHolder.addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched ScreenedBy User"));
					throw new OperationException();
				} 

			}
			
			
			if(patientEnrollmentDetails.getScreeningOutcome() != null)
			{
				String screeningOutcome = null; 
				try{
					screeningOutcome = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getScreeningOutcome(),
						CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
				
				}catch(CodeNotFoundException e)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"ScreeningOutcome Code Not Found: " + patientEnrollmentDetails.getScreeningOutcome().getCode()));
					throw new OperationException();
				}
				
				protStudyStatbean.setScreeningOutcome(screeningOutcome);				
			}
		
			
		}


		//additional information
		String patientStudyId = patientEnrollmentDetails.getPatientStudyId(); 
		if(patientStudyId == null || patientStudyId.length() == 0) patientStudyId = patientCode; 
		patProtBean.setPatStudyId(patientStudyId); 
		
		
		
		if(patientEnrollmentDetails.getEnrollingSite() != null)
		{
			// Get Enrolling Sites for this Study
			//Bug Fix 6758,6789,6757
			
			Integer enrollingSite;
			List<Integer> siteToEnroll = (List<Integer>) patientData.get("patientOrganizations"); 
			try {
				enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getEnrollingSite(), sessionContext, objectMapService);
				
				if(enrollingSite != null && enrollingSite != 0)
				{
					if(siteToEnroll.contains(enrollingSite))
					{
						patProtBean.setEnrollingSite(EJBUtil.integerToString(enrollingSite)); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
						throw new OperationException(); 
					}
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND,
									"EnrollingSite not found")); 
					throw new OperationException(); 
					
				}
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Enrolling Site"));
				throw new OperationException();
			} 
			
		}
		
		if(patientEnrollmentDetails.getAssignedTo() != null )
		{
			try{
			UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getAssignedTo(), userAgent, sessionContext, objectMapService);
			if(assignToBean != null)
			{
				patProtBean.setAssignTo(EJBUtil.integerToString(assignToBean.getUserId())); 
			}else
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.USER_NOT_FOUND ,
						"AssignedTo User Not found"));
				throw new OperationException();
			}
			}catch(MultipleObjectsFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Assigned to User"));
				throw new OperationException();
			}
		}
		
		
		if(patientEnrollmentDetails.getPhysician() != null)
		{
			UserBean physicianBean;
			try {
				physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getPhysician(), userAgent, sessionContext, objectMapService);
				if(physicianBean != null)
				{
					patProtBean.setPatProtPhysician(EJBUtil.integerToString(physicianBean.getUserId())); 
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.USER_NOT_FOUND ,
							"Physician Not found"));
					throw new OperationException();
				}
			
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Physician"));
				throw new OperationException();
			} 
			
		}
		
		if(patientEnrollmentDetails.getTreatmentLocation() != null )
		{
			try{
				 String treatmentlocation = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
				 patProtBean.setTreatmentLoc(treatmentlocation); 
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"TreatmentLocation Code Not Found: " + patientEnrollmentDetails.getTreatmentLocation().getCode()));
					throw new OperationException();
			}
		}
		
		if(patientEnrollmentDetails.getTreatingOrganization() != null)
		{
			try {
				
				
				Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getTreatingOrganization(), sessionContext, objectMapService);
				
				if(treatingOrganization != null && treatingOrganization != 0)
				{
					List<Integer> treatingOrg = new ArrayList<Integer>(); 			
					SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
					SiteDao siteDao = siteAgentBean.getByAccountId(EJBUtil.stringToInteger(callingUser.getUserAccountId()));
					//TODO SiteAgentBean .get SiteValues(acctID); 

					for( int j = 0; j < siteDao.getSiteIds().size(); j++)
					{
						treatingOrg.add(EJBUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
					}
					
					if(treatingOrg.contains(treatingOrganization))
					{					
						patProtBean.setpatOrg(EJBUtil.integerToString(treatingOrganization));
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"TreatingOrganization is not in list of possible Treating Organizations"));
						throw new OperationException(); 
					}
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND, 
							"TreatingOrganization not found"));
					throw new OperationException(); 
				}
				 
				
				
				
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Treating Organization"));
				throw new OperationException();
			} 
		}
		
		
		//Evaluable Status
		
		if(patientEnrollmentDetails.getDateOfDeath() != null)
		{
			patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath()); 
		}

		patProtBean.setPatProtStudyId(EJBUtil.integerToString(studyPK)); 
		if(patientEnrollmentDetails.getEvaluationFlag() != null)
		{
			try{
				String evaluationFlag = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationFlag(),
						CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
				if(evaluationFlag != null)
				{
					patProtBean.setPtstEvalFlag(evaluationFlag); 
				}else
				{
					throw new OperationException(); 
				}
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"EvaluationFlag Code Not Found: " + patientEnrollmentDetails.getEvaluationFlag().getCode()));
				throw new OperationException();
			}
		}

		if(patientEnrollmentDetails.getEvaluationStatus() != null)
		{
			String evaluationStatus = null; 
			try{
			evaluationStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationStatus(),
					CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
			
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"EvaluationStatus Code Not Found: " + patientEnrollmentDetails.getEvaluationStatus().getCode()));
				throw new OperationException();
			}		
			patProtBean.setPtstEval(evaluationStatus); 	
			
			
		}
		
		if(patientEnrollmentDetails.getInevaluationStatus() != null)
		{
			try{
				String inEval = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getInevaluationStatus(),
						CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
				
				patProtBean.setPtstInEval(inEval); 
			}catch(CodeNotFoundException e){
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"InevaluationStatus Code Not Found: " + patientEnrollmentDetails.getInevaluationStatus().getCode()));
				throw new OperationException();
			}
			
		}
		
		patProtBean.setCreator(EJBUtil.integerToString(callingUser.getUserId())); 
		patProtBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
		//Add new patient get status as "1" to identify current record		
		patProtBean.setPatProtStat("1"); 
		
			
		//Patient Status
		
		if(patientEnrollmentDetails.getSurvivalStatus() != null)
		{
			try{
				String survivalStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser); 
				patProtBean.setPtstSurvival(survivalStatus);
				
				
				if(personB.getPersonStatus().equals(survivalStatus) &&  patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
				{
					personB.setPersonStatus(survivalStatus); 
					if(patientEnrollmentDetails.getDateOfDeath() != null) personB.setPersonDeathDt(patientEnrollmentDetails.getDateOfDeath());
				}				
				
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Survival Status Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
				throw new OperationException();
			}
			
		}
		
		
		if(patientEnrollmentDetails.getDateOfDeath() != null) patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath()); 
		if(patientEnrollmentDetails.getSpecifyCause() != null) personB.setDthCauseOther(patientEnrollmentDetails.getSpecifyCause()); 
		if(patientEnrollmentDetails.getCauseOfDeath() != null)
		{
			try{
				String patDeathCause = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getCauseOfDeath(), 
						CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser); 
				personB.setPatDthCause(patDeathCause); 
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"CauseOfDeath Code Not Found: " + patientEnrollmentDetails.getCauseOfDeath().getCode()));
				throw new OperationException(); 
			}
		}
		
		if(patientEnrollmentDetails.getDeathRelatedToTrial() != null)
		{
			try{
				String deathRelatedToTrial = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getDeathRelatedToTrial(), 
						CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser); 
				patProtBean.setPtstDthStdRel(deathRelatedToTrial); 
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"DeathRelatedToTrial Code Not Found: " + patientEnrollmentDetails.getDeathRelatedToTrial().getCode()));
				throw new OperationException(); 
			}
		}
		
		patProtBean.setPtstDthStdRelOther(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()); 
		
			
		protStudyStatbean.setPatientId(EJBUtil.integerToString(patientPK)); 
		protStudyStatbean.setStudyId(EJBUtil.integerToString(studyPK)); 
		protStudyStatbean.setPatProtObj(patProtBean); 
		
		if(patientEnrollmentDetails.getStatus() != null)
		{
			String status = ""; 
			try{
			 status = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatus(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser); 
			//Bug fix 6836
			
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"status Code Not Found: " + patientEnrollmentDetails.getStatus().getCode()));
				throw new OperationException(); 
			}
			protStudyStatbean.setPatStudyStat(status);
			if(patientEnrollmentDetails.getStatusReason() != null)
			{
				String  statusReason;  
				try
				{
					statusReason= AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatusReason(),
					status, callingUser); 
				}catch(CodeNotFoundException ce)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"status Code Not Found: " + patientEnrollmentDetails.getStatusReason().getCode()));
					throw new OperationException(); 
				}
								
				protStudyStatbean.setPatStudyStatReason(statusReason); 
			}
		}
		
		personB.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId())); 
		int patientSuccessfullyUpdated = personAgent.updatePerson(personB); 
		if(patientSuccessfullyUpdated != 0 )
		{
			responseHolder.addIssue(new Issue(IssueTypes.PATIENT_ERROR_UPDATE_DEMOGRAPHICS));
			throw new OperationException(); 
		}
		
		if(patientEnrollmentDetails.getStatusDate() != null) protStudyStatbean.setStartDate(patientEnrollmentDetails.getStatusDate()); 
		protStudyStatbean.setCreator(EJBUtil.integerToString(callingUser.getUserId())); 
		protStudyStatbean.setCurrentStat(patientEnrollmentDetails.isCurrentStatus()? "1": "0"); 
		protStudyStatbean.setNotes(patientEnrollmentDetails.getNotes()); 
		protStudyStatbean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
				
		PatStudyStatAgentRObj patStudyStatAgent = (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent"); 
		int enrollmentPK = patStudyStatAgent.setPatStudyStatDetails(protStudyStatbean); 
		if(enrollmentPK < 0)
		{
			responseHolder.addIssue(new Issue(IssueTypes.ERROR_CREATE_PATIENT_ENROLLMENT));
			throw new OperationException(); 
		}
		
		
	
		protStudyStatbean = patStudyStatAgent.getPatStudyStatDetails(enrollmentPK); 
		
		ObjectMap patProtMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, protStudyStatbean.getPatProtObj().getPatProtId()); 
		PatientProtocolIdentifier patprotIdentifier = new PatientProtocolIdentifier(); 
		patprotIdentifier.setOID(patProtMap.getOID()); 
		responseHolder.addObjectCreatedAction(patprotIdentifier); 
		
		
		ObjectMap patProtStatusMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, enrollmentPK); 
		PatientStudyStatusIdentifier patStudyStatusIdentifier = new PatientStudyStatusIdentifier(); 
		patStudyStatusIdentifier.setOID(patProtStatusMap.getOID()); 
		responseHolder.addObjectCreatedAction(patStudyStatusIdentifier); 
		
		return enrollmentPK; 	
		
		
	}	
	
	public int enrollPatientToStudy(PatientIdentifier patientIdentifier, Integer studyPK, PatientEnrollmentDetails patientEnrollmentDetails, Map<String,Object> parameters)
	throws OperationException{

		int enrollmentPK = 0; 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder"); 
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		StudyAgent studyAgent = (StudyAgent) parameters.get("studyAgent"); 
		Integer patientPK = null;
		
		if(patientIdentifier != null && (patientIdentifier.getOID() != null || 
				(patientIdentifier.getPatientId() != null && patientIdentifier.getOrganizationId() != null)))
		{
			try {
				patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
						"Multiple Patients found")); 
				throw new OperationException(); 
			}
		}else{
			responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Patient OID or PatientId and OrganizationIdentifier is required")); 
			throw new OperationException(); 
		}
				
		if(patientPK == null || patientPK == 0)
		{
			responseHolder.addIssue(
					new Issue(
						IssueTypes.PATIENT_NOT_FOUND, 
						"Patient not Found for PatientIdentifier OID: " + patientIdentifier.getOID() + " patientID: " + patientIdentifier.getPatientId()));
			throw new OperationException(); 
		}
		
		List<Integer> patientList = StudyPatientDAO.getAllPatients(callingUser.getUserId()); 		
		if(!patientList.contains(patientPK))
		{
			responseHolder.addIssue(	
					new Issue(
					IssueTypes.DATA_VALIDATION, 
					"User is not authorized to enroll given patient"));
			throw new OperationException(); 
		}
	
		
		if(isPatientAlreadyEnrolledToThisStudy(patientPK, studyPK, studyAgent, callingUser.getUserId()))
		{
			responseHolder.addIssue(new Issue(IssueTypes.PATIENT_ALREADY_ENROLLED, "Patient is already enrolled to this Study")); 
			throw new OperationException(); 
			
		}
		
		PersonBean personBean = personAgent.getPersonDetails(patientPK); 
		Map<String, Object> patientData = new HashMap<String,Object>(); 
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
	
		ArrayList<Integer> siteToEnroll = new ArrayList<Integer>(); 
		
		for( int j = 0; j < patFacilityDao.getPatientSite().size(); j++)
		{
			siteToEnroll.add(EJBUtil.stringToInteger(patFacilityDao.getPatientSite().get(j).toString())); 
		}
		
		patientData.put("patientOrganizations", siteToEnroll); 
		patientData.put("patientDOB",  personBean.getPersonDb()); 
		patientData.put("patientCode", personBean.getPersonPId()); 
	
		enrollmentPK = enrollPatientToStudy(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData); 
		
		return enrollmentPK; 
	}
	
	
	private boolean isPatientAlreadyEnrolledToThisStudy(Integer patientPK, Integer studyPK, StudyAgent studyAgent, Integer userID) throws OperationException
	{
		PatStudyStatDao dao = studyAgent.getPatientStudies(patientPK, userID); 
		List enrolledInStudies = dao.getStudyIds(); 
		
		for(int i = 0 ; i < enrolledInStudies.size(); i++)
		{
			Integer enrolledInStudy = EJBUtil.stringToInteger(enrolledInStudies.get(i).toString()); 
			if( enrolledInStudy.equals(studyPK))
			{
				return true; 
			}
		}		
		return false; 
	}

	public int createAndEnrollPatient(Patient patient, Integer studyPK, PatientEnrollmentDetails patientEnrollmentDetails, Map<String, Object> parameters)
	throws OperationException
	{
		int enrollmentPK = 0; 
		ResponseHolder response =(ResponseHolder) parameters.get("ResponseHolder"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		PatientDemographicsHelper patientDemographicHelper = new PatientDemographicsHelper(); 
		Integer patientPK = patientDemographicHelper.createPatient(patient, parameters);	
		if(patientPK == null || patientPK == 0)
		{
			throw new OperationException(); 
		}
		
			
		Map<String, Object> patientData = new HashMap<String,Object>(); 
		patientData.put("patientDOB",  patient.getPatientDemographics().getDateOfBirth()); 
		patientData.put("patientCode", patient.getPatientDemographics().getPatientCode());
		patientData.put("patientOrganizations", parameters.get("patientOrganizations")); 
		
	
		enrollmentPK = enrollPatientToStudy(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData);
		
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, patientPK); 
		
		PatientIdentifier patientIdentifier = new PatientIdentifier(); 
		patientIdentifier.setOID(map.getOID()); 
		patientIdentifier.setPatientId(patient.getPatientDemographics().getPatientCode()); 
		
		
		response.addObjectCreatedAction(patientIdentifier);
	
		return enrollmentPK; 
	}
	
	
	private void validatePatientEnrollmentDetails(PatientEnrollmentDetails patientEnrollmentDetails, UserBean callingUser,Map<String, Object> parameters, Date patientDOB) throws ValidationException
	{
		
		ConfigDetailsObject configDetails
		= ConfigFacade.getConfigFacade().populateObject(EJBUtil.stringToInteger(callingUser.getUserAccountId()), "patstudystatus");
		AccountJB acctJB = new AccountJB(); 
		acctJB.setAccId(EJBUtil.stringToInteger(callingUser.getUserAccountId())); 
		acctJB.getAccountDetails(); 
	
		List<Issue> validationIssues = new ArrayList<Issue>();
		
		if(configDetails != null ){
			if(isMandatory(configDetails, "patstatus") && (patientEnrollmentDetails.getStatus() == null || patientEnrollmentDetails.getStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_StatusCode_Mandatory)); 
			}
			
			
			if(isMandatory(configDetails, "statusdate") && patientEnrollmentDetails.getStatusDate() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_StatusDate_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "reason") && (patientEnrollmentDetails.getStatusReason() == null || patientEnrollmentDetails.getStatusReason().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_StatusReason_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "notes") && (patientEnrollmentDetails.getNotes() == null || patientEnrollmentDetails.getNotes().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_Notes_Mandatory)); 
			}
			
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("enrolled"))
			{

				if(isMandatory(configDetails, "randomnumber") && (patientEnrollmentDetails.getRandomizationNumber() == null || patientEnrollmentDetails.getRandomizationNumber().length() == 0))
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_RandomNumber_Mandatory)); 
				}


				if(isMandatory(configDetails, "enrolledby") && patientEnrollmentDetails.getEnrolledBy() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_EnrolledBy_Mandatory)); 
				}

			}
			
			
			//Informed Consent Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("infConsent"))
			{
				if(isMandatory(configDetails, "icversionnumber") && patientEnrollmentDetails.getInformedConsentVersionNumber() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ICVersionNumber_Mandatory));
				}
			}

			//Follow-up Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("followup"))
			{
				if(isMandatory(configDetails, "followupdate") && patientEnrollmentDetails.getNextFollowUpDate() == null)
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_NextFollowUpDate_Mandatory)); 
				}
			}
			
			//Screening Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("screening"))
			{			
			
				if(isMandatory(configDetails, "screennumber") && patientEnrollmentDetails.getScreenNumber() == null)
				{
				
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreenNumber_Mandatory)); 
					
				}
				
				if(isMandatory(configDetails, "screenedby") && patientEnrollmentDetails.getScreenedBy() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreenBy_Mandatory)); 
				}
				
				if(isMandatory(configDetails, "screeningoutcome") && patientEnrollmentDetails.getScreenedBy() == null)
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreeningOutcome_Mandatory)); 
				}
			}
			// Additional Information
			
			if(isMandatory(configDetails,"enrollingsite" )
					&& (patientEnrollmentDetails.getEnrollingSite() == null || 
							(patientEnrollmentDetails.getEnrollingSite().getOID() == null 
									&& patientEnrollmentDetails.getEnrollingSite().getSiteAltId() == null 
									&& patientEnrollmentDetails.getEnrollingSite().getSiteName() == null)))
			{

				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EnrollingSite_Mandatory)); 

			}
			
			if(isMandatory(configDetails, "assignedto") && patientEnrollmentDetails.getAssignedTo() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_AssignedTo_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "physician") && patientEnrollmentDetails.getPhysician() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_Physician_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "treatmentlocation") && (patientEnrollmentDetails.getTreatmentLocation() == null ||patientEnrollmentDetails.getTreatmentLocation().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_TreatmentLocation_Mandatory)); 
			}
			
			if(isMandatory(configDetails,"treatingorg" )
					&& (patientEnrollmentDetails.getTreatingOrganization() == null || 
							(patientEnrollmentDetails.getTreatingOrganization().getOID() == null 
									&& patientEnrollmentDetails.getTreatingOrganization().getSiteAltId() == null 
									&& patientEnrollmentDetails.getTreatingOrganization().getSiteName() == null)))
			{

				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_TreatingOrg_Mandatory)); 

			}
			
			if(isMandatory(configDetails, "evaluableflag") && (patientEnrollmentDetails.getEvaluationFlag() == null || patientEnrollmentDetails.getEvaluationFlag().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EvaluationFlag_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "evaluablestatus") && (patientEnrollmentDetails.getEvaluationStatus() == null || patientEnrollmentDetails.getEvaluationStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EvaluationStatusCode_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "inevaluablestatus") && (patientEnrollmentDetails.getInevaluationStatus() == null || patientEnrollmentDetails.getInevaluationStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_InevaluationStatusCode_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "survivalstatus") && (patientEnrollmentDetails.getSurvivalStatus() == null || patientEnrollmentDetails.getSurvivalStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_SurvivalStatus_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "dod") && patientEnrollmentDetails.getDateOfDeath() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_DateOfDeath_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "cod") && (patientEnrollmentDetails.getCauseOfDeath() == null || patientEnrollmentDetails.getCauseOfDeath().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_CauseOfDeath_Mandatory)); 
			}
			if(isMandatory(configDetails, "specifycause") && (patientEnrollmentDetails.getSpecifyCause() == null || patientEnrollmentDetails.getSpecifyCause().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_SpecifyCause_Mandatory)); 
			}
			if(isMandatory(configDetails, "deathrelated") && (patientEnrollmentDetails.getDeathRelatedToTrial() == null || patientEnrollmentDetails.getDeathRelatedToTrial().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory)); 
			}


		}
		
		
			
// more Validation
		if(patientEnrollmentDetails.getDateOfDeath() != null && patientDOB != null && (patientEnrollmentDetails.getDateOfDeath().compareTo(patientDOB) < 0))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_DeathDtBirthDt_Camparison));			
		}
		
		if(patientEnrollmentDetails.getStatusDate() != null && patientDOB != null && (patientEnrollmentDetails.getStatusDate().compareTo(patientDOB) < 0))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_BirthDtStatusDt_Camparison)); 
		}
		
// Death related validation 
		
		if(patientEnrollmentDetails.getDateOfDeath() != null)
		{
			if(patientEnrollmentDetails.getSurvivalStatus() != null && patientEnrollmentDetails.getSurvivalStatus().getCode() != null )
			{
				if(!patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_DeathDate_SurvivalStatus)); 
				}
			}else
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_DeathDate_SurvivalStatus)); 
			}
			
		}
		
		if(patientEnrollmentDetails.getSurvivalStatus() != null && patientEnrollmentDetails.getSurvivalStatus().getCode() != null && patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
		{
			if(patientEnrollmentDetails.getDateOfDeath() == null) 
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "dateOfDeath is required for dead Patient"));
			}
		}
		
		if(((patientEnrollmentDetails.getCauseOfDeath() != null  && patientEnrollmentDetails.getCauseOfDeath().getCode() != null)
				||(patientEnrollmentDetails.getSpecifyCause() != null && patientEnrollmentDetails.getSpecifyCause().length() > 0))
				&& (!patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead")))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_CauseOfDeath_SurvivalStatus)); 
		}
		if(((patientEnrollmentDetails.getReasonOfDeathRelatedToTrial() != null && patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().length() > 0 )|| (patientEnrollmentDetails.getDeathRelatedToTrial() != null && patientEnrollmentDetails.getDeathRelatedToTrial().getCode() != null )) && (!patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead")))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_DeathRelatedToTrial_SurvivalStatus)); 
		}
		

		if (validationIssues.size() > 0){
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
		//	response.getIssues().addAll(validationIssues);
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for PatientEnrollmentDetails");
			throw new ValidationException();
		}
		
		
	}
	
	private boolean isMandatory(ConfigDetailsObject configDetails, String field)
	{
		boolean isMandatory = false;
		ArrayList fields = configDetails.getPcfField(); 
		ArrayList mandatory = configDetails.getPcfMandatory();
		
		if(fields == null || fields.indexOf(field) == -1)
		{
			
			if(field.equals("patstatus")) return true; 
			if(field.equals("statusdate")) return true; 
			if(field.equals("enrolledby")) return true;
			if(field.equals("patstdid")) return true; 
			
		}else
		{
			if(mandatory.get(fields.indexOf(field)).toString().equals("1")) return true; 
		}
	
		
		return isMandatory; 
	}

	public void updateStudyPatientStatus(PatStudyStatBean patStudyStatBean,
			PatientEnrollmentDetails patientEnrollmentDetails, Map<String, Object> parameters) throws OperationException {
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		PatStudyStatAgentRObj patStudyStatAgent= (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent");
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Integer patientPK=(Integer) parameters.get("perPK");
		Integer studyPK=(Integer) parameters.get("studyPK");
		ResponseHolder response=(ResponseHolder) parameters.get("response");
		ConfigDetailsObject configDetails=ConfigFacade.getConfigFacade().populateObject(StringUtil.stringToInteger(callingUser.getUserAccountId()), "patstudystatus");
		CodeDao codeDao=new CodeDao();
		PatProtBean patProtBean= patStudyStatBean.getPatProtObj();
		UserAgentRObj userAgent=(UserAgentRObj) parameters.get("userAgent");
		PersonBean personB = personAgent.getPersonDetails(patientPK);
		String statusType= codeDao.getCodeSubtype(StringUtil.stringToInteger(patStudyStatBean.getPatStudyStat()));
		
		boolean isPersonBUpdated=false;
		//Patient Study Status
		//Reason
		if(patientEnrollmentDetails.getStatusReason()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getType()))
			{
				if(isMandatory(configDetails, "reason"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_StatusReason_Mandatory));
					throw new OperationException();
				}
				else
				{
					patStudyStatBean.setPatStudyStatReason("");
				}
			}
			else
			{
				String statusReason="";
				try
				{
					statusReason= AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatusReason(), statusType, callingUser);		
				}
				catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND,"Reason Code Not Found: " + patientEnrollmentDetails.getStatusReason().getCode()));
					throw new OperationException(); 
				}
				patStudyStatBean.setPatStudyStatReason(statusReason);
			}
		}
		//Status Date
		if(null!=patientEnrollmentDetails.getStatusDate())
		{
			patStudyStatBean.setStartDate(patientEnrollmentDetails.getStatusDate());
		}
		//Current Status
		if(patientEnrollmentDetails.isCurrentStatus())
		{
			PatStudyStatBean currentPatStatBean = patStudyStatAgent.getPatStudyCurrentStatus(StringUtil.integerToString(studyPK),StringUtil.integerToString(patientPK));
			currentPatStatBean.setCurrentStat("0");
			currentPatStatBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			if(patStudyStatAgent.updatePatStudyStat(currentPatStatBean)<0)
			{
				response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS, "Study Patient status could not be updated successfully")); 
				throw new OperationException();
			}
			patStudyStatBean.setCurrentStat("1");
		}
		//Notes
		if(!StringUtil.isEmpty(patientEnrollmentDetails.getNotes()))
		{
			patStudyStatBean.setNotes(patientEnrollmentDetails.getNotes());
		}
		else 
		{
			if(patientEnrollmentDetails.getNotes()!=null)
			{
				if(isMandatory(configDetails, "notes"))
				{		response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_Notes_Mandatory));
						throw new OperationException();
				}
				else
				{
					patStudyStatBean.setNotes("");
				}
			}
		}
		//Setting Additional Information
		//Patient Study ID
		if(patientEnrollmentDetails.getPatientStudyId()!=null)
		{
			if(patientEnrollmentDetails.getPatientStudyId().trim().length()==0)
			{
				if(isMandatory(configDetails, "patstdid"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Field patientStudyId may not be null"));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPatStudyId("")	;
				}
			}
			else
			{
				if(!patientEnrollmentDetails.getPatientStudyId().equalsIgnoreCase(patProtBean.getPatStudyId()))
				{
					//if(patStudyStatAgent.getCountPatStudyId(studyPK, patientEnrollmentDetails.getPatientStudyId())>0){}
					patProtBean.setPatStudyId(patientEnrollmentDetails.getPatientStudyId())	;
				}
			}
		}
		//Enrolling Site
		if(patientEnrollmentDetails.getEnrollingSite()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getOID())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getSiteAltId())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getSiteName()))
			{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EnrollingSite_Mandatory)); 
					throw new OperationException();
			}
			Integer enrollingSite;
			try {
					enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getEnrollingSite(), sessionContext, objectMapService);
					if(enrollingSite != null && enrollingSite != 0)
					{
						PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
						List siteToEnroll = patFacilityDao.getPatientSite(); 
						if(siteToEnroll.contains(StringUtil.integerToString(enrollingSite)))
						{
							patProtBean.setEnrollingSite(StringUtil.integerToString(enrollingSite)); 
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
							throw new OperationException(); 
						}
					}else
					{
						response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"EnrollingSite not found")); 
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) 
				{
					response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling Site"));
					throw new OperationException();
				}
		}
		//Assigned To
		if(patientEnrollmentDetails.getAssignedTo()!=null)
		{
			if( (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getFirstName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getLastName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getUserLoginName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getOID())))
			{
				if(isMandatory(configDetails, "assignedto"))
				{
					response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_AssignedTo_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setAssignTo("");
				}
			}
			else
			{
				try
				{
					UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getAssignedTo(), userAgent, sessionContext, objectMapService);
					if(assignToBean != null)
					{
						patProtBean.setAssignTo(StringUtil.integerToString(assignToBean.getUserId())); 
					}else
					{
						response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"AssignedTo User Not found"));
						throw new OperationException();
					}
				}catch(MultipleObjectsFoundException e)
				{
					response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Assigned to User"));
					throw new OperationException();
				}
			}
		}
		//Physician
		if(patientEnrollmentDetails.getPhysician()!=null)
		{
			if( (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getFirstName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getLastName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getUserLoginName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getOID())))
				{
					if(isMandatory(configDetails, "physician"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_Physician_Mandatory));
						throw new OperationException();
					}
					else
					{
						patProtBean.setPatProtPhysician("");
					}
				}	
		    else
			{
				UserBean physicianBean;
				try {
						physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getPhysician(), userAgent, sessionContext, objectMapService);
						if(physicianBean != null)
						{
							patProtBean.setPatProtPhysician(StringUtil.integerToString(physicianBean.getUserId())); 
						}else
						{
							response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"Physician Not found"));
							throw new OperationException();
						}
					} 
					catch (MultipleObjectsFoundException e) 
					{
						response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Physician"));
						throw new OperationException();
					}	
			}
		}
		//Treatment Location
		if(patientEnrollmentDetails.getTreatmentLocation()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getType()))
			{
				if(isMandatory(configDetails, "treatmentlocation"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_TreatmentLocation_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setTreatmentLoc("");
				}
			}
			else
			{
				try{
					 String treatmentlocation = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
					 patProtBean.setTreatmentLoc(treatmentlocation); 
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"TreatmentLocation Code Not Found: " + patientEnrollmentDetails.getTreatmentLocation().getCode()));
					throw new OperationException();
				}
			}
		}
		// Treating Organization
		if(patientEnrollmentDetails.getTreatingOrganization()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getOID())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteAltId())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteName()))
			{
				if(isMandatory(configDetails,"treatingorg" ))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_TreatingOrg_Mandatory)); 
				}
				else
				{
					patProtBean.setpatOrg("");
				}
			}
			else
			{
				try
				{
					Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getTreatingOrganization(), sessionContext, objectMapService);
					if(treatingOrganization != null && treatingOrganization != 0)
					{
						List<Integer> treatingOrg = new ArrayList<Integer>(); 			
						SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
						SiteDao siteDao = siteAgentBean.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
						//TODO SiteAgentBean .get SiteValues(acctID);
						for( int j = 0; j < siteDao.getSiteIds().size(); j++)
						{
							treatingOrg.add(StringUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
						}
						if(treatingOrg.contains(treatingOrganization))
						{					
							patProtBean.setpatOrg(StringUtil.integerToString(treatingOrganization));
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"TreatingOrganization is not in list of possible Treating Organizations"));
							throw new OperationException(); 
						}
					}else
					{
						response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"TreatingOrganization not found"));
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) {
					response.addIssue(
							new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Treating Organization"));
					throw new OperationException();
				} 
			}
		}
		//Evaluable Status
		//Evaluable Flag
		if(patientEnrollmentDetails.getEvaluationFlag()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getType()))
			{
				if(isMandatory(configDetails, "evaluableflag"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EvaluationFlag_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstEvalFlag("");
				}
			}
			else
			{
				try{
					String evaluationFlag = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationFlag(),CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
					if(evaluationFlag != null)
					{
						patProtBean.setPtstEvalFlag(evaluationFlag); 
					}else
					{
						throw new OperationException(); 
					}
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationFlag Code Not Found: " + patientEnrollmentDetails.getEvaluationFlag().getCode()));
					throw new OperationException();
				}
			}
		}
		//Evaluable Status
		if(patientEnrollmentDetails.getEvaluationStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getType()))
			{
				if(isMandatory(configDetails, "evaluablestatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_EvaluationStatusCode_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstEval("");
				}
			}
			else
			{
				String evaluationStatus = null; 
				try{
				evaluationStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationStatus(),CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
				}catch(CodeNotFoundException e)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationStatus Code Not Found: " + patientEnrollmentDetails.getEvaluationStatus().getCode()));
					throw new OperationException();
				}		
				patProtBean.setPtstEval(evaluationStatus); 
			}
		}
		//Unevaluable Status
		if(patientEnrollmentDetails.getInevaluationStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getType()))
			{
				if(isMandatory(configDetails, "inevaluablestatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_InevaluationStatusCode_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstInEval("");
				}
			}
			else
			{
				try{
					String inEval = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getInevaluationStatus(),CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
					patProtBean.setPtstInEval(inEval); 
				}catch(CodeNotFoundException e){
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"InevaluationStatus Code Not Found: " + patientEnrollmentDetails.getInevaluationStatus().getCode()));
					throw new OperationException();
				}
			}
		}
		//Status dependent extra details
		//Enrollment Details - if status = enrolled
		if(statusType.equalsIgnoreCase("enrolled"))
		{
			if(patientEnrollmentDetails.getRandomizationNumber()!=null)
			{
				if(patientEnrollmentDetails.getRandomizationNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "randomnumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_RandomNumber_Mandatory)); 
					}
					else
					{
						patProtBean.setPatProtRandomNumber("");						
					}
				}
				else
				{
					patProtBean.setPatProtRandomNumber(patientEnrollmentDetails.getRandomizationNumber());
				}
			}
			if(patientEnrollmentDetails.getEnrolledBy() != null)
			{
				if( (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getFirstName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getLastName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getUserLoginName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getOID())))
						{
							if(isMandatory(configDetails, "enrolledby"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_EnrolledBy_Mandatory));
								throw new OperationException();
							}
							else
							{
								patProtBean.setPatProtUserId("");
							}
						}	
				    else
					{
				    	UserBean enrollingUserBean;
				    	try{
				    		enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getEnrolledBy(), userAgent, sessionContext, objectMapService);
				    		if(enrollingUserBean != null)
				    		{
				    			patProtBean.setPatProtUserId(StringUtil.integerToString(enrollingUserBean.getUserId())); 
				    		}else
				    		{
				    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"EnrolledBy User Not found"));
				    			throw new OperationException();
				    		}
				    	} catch (MultipleObjectsFoundException e)
				    	{
				    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling User"));
				    		throw new OperationException();
				    	} 
					}
			}
		}
		//Follow-up Details - if status = followup
		if(statusType.equalsIgnoreCase("followup"))
		{
			if(patientEnrollmentDetails.getNextFollowUpDate()!=null&&patientEnrollmentDetails.isEmptyNextFollowUpDate())
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyNextFollowUpDate field is set to true along with nextFollowUpDate"));
	    		throw new OperationException();
			}
			if(patientEnrollmentDetails.getNextFollowUpDate()!=null)
			{
				patStudyStatBean.setPtstNextFlwupPersistent(patientEnrollmentDetails.getNextFollowUpDate());
			}
			if(patientEnrollmentDetails.isEmptyNextFollowUpDate())
			{
				patStudyStatBean.setPtstNextFlwupPersistent(null);
			}
		}
		//Informed Consent Details - if status = infConsent
		if(statusType.equalsIgnoreCase("infConsent"))
		{
			if(patientEnrollmentDetails.getInformedConsentVersionNumber()!=null)
			{
				if(patientEnrollmentDetails.getInformedConsentVersionNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "icversionnumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ICVersionNumber_Mandatory)); 
					}
					else
					{
						patProtBean.setPtstConsentVer("");						
					}
				}
				else
				{
					patProtBean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber());
				}
			}	
		}
		//Screening Details - if status = screening
		if(statusType.equalsIgnoreCase("screening"))
		{
			if(patientEnrollmentDetails.getScreenNumber()!=null)
			{
				if(patientEnrollmentDetails.getScreenNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "screennumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreenNumber_Mandatory)); 
						throw new OperationException();
					}
					else
					{
						patStudyStatBean.setScreenNumber("");						
					}
				}
				else
				{
					patStudyStatBean.setScreenNumber(patientEnrollmentDetails.getScreenNumber());
				}
			}
			if(patientEnrollmentDetails.getScreenedBy() != null)
			{
				if( (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getFirstName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getLastName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getUserLoginName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getOID())))
						{
							if(isMandatory(configDetails, "screenedby"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreenBy_Mandatory));
								throw new OperationException();
							}
							else
							{
								patStudyStatBean.setScreenedBy("");
							}
					}	
				else
				{
				    UserBean screeningUserBean; 			
			    	try
			    	{
			    		screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getScreenedBy(), userAgent, sessionContext, objectMapService);
			    		if(screeningUserBean != null)
			    		{
			    			patStudyStatBean.setScreenedBy(StringUtil.integerToString(screeningUserBean.getUserId())); 
			    		}
			    		else
			    		{
			    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"ScreenBy User not found"));
			    			throw new OperationException();
			    		}
			    	}
			    	catch (MultipleObjectsFoundException e)
			    	{
			    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched ScreenedBy User"));
			    		throw new OperationException();
			    	}	 
				}
			}
			if(patientEnrollmentDetails.getScreeningOutcome() != null)
			{
				if(StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getCode())&&
						StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getDescription())&&
						StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getType()))
				{
					if(isMandatory(configDetails, "screeningoutcome"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, MC.Svc_PatientEnrollment_ScreeningOutcome_Mandatory));
						throw new OperationException();
					}
					else
					{
						patStudyStatBean.setScreeningOutcome("");
					}
				}
				else
				{
					String screeningOutcome = null; 
					try{
						screeningOutcome = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getScreeningOutcome(),CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
					}catch(CodeNotFoundException e)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"ScreeningOutcome Code Not Found: " + patientEnrollmentDetails.getScreeningOutcome().getCode()));
						throw new OperationException();
					}
					patStudyStatBean.setScreeningOutcome(screeningOutcome);				
				}
			}
		}
		//Patient Status
		//Survival Status
		int  deadStatPk = codeDao.getCodeId("ptst_survival","dead");
		if(patientEnrollmentDetails.getSurvivalStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getType()))
			{
				if(isMandatory(configDetails, "survivalstatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_SurvivalStatus_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstSurvival("");
				}
			}
			else
			{
				try{
					String survivalStatus=null; 
					try{
						survivalStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser);
					}
					catch (CodeNotFoundException e) {
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"survivalStatus Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
						throw new OperationException();
					}
					String prevSurvivalStatus=patProtBean.getPtstSurvival();
					if(StringUtil.stringToNum(prevSurvivalStatus)!= StringUtil.stringToNum(survivalStatus))
					{
						patProtBean.setPtstSurvival(survivalStatus);
						if(patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
						{
							if(patientEnrollmentDetails.getDateOfDeath()==null)
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field may not be null when survival status provided as dead"));
								throw new OperationException();
							}
							if(patientEnrollmentDetails.isEmptyDeathDate())
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"emptyDeathDate field may not be set to True when survival status provided as dead"));
								throw new OperationException();
							}
							if((StringUtil.stringToInteger(survivalStatus)==deadStatPk))
							{
								personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","dead")));
								isPersonBUpdated=true;
							}
						}
						else
						{
							if(StringUtil.stringToNum(prevSurvivalStatus)==deadStatPk)
							{
								//checking death fields for !null
								//deathDate
								if(patientEnrollmentDetails.getDateOfDeath()!=null)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(patProtBean.getPtstDeathDatePersistent()!=null&&!patientEnrollmentDetails.isEmptyDeathDate())
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because Death Date is already provided"));
									throw new OperationException();
								}
								//CauseOfDeath
								if(patientEnrollmentDetails.getCauseOfDeath()!=null && (
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
										))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"causeOfDeath field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(personB.getPatDthCause())
										&&(patientEnrollmentDetails.getCauseOfDeath()==null ||(
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
												)
											))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because cause of death is already provided"));
									throw new OperationException();
								}
								//Specify Cause
								if(patientEnrollmentDetails.getSpecifyCause()!=null && patientEnrollmentDetails.getSpecifyCause().trim().length()!=0)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"specifyCause field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(personB.getDthCauseOther())&& (patientEnrollmentDetails.getSpecifyCause()==null||patientEnrollmentDetails.getSpecifyCause().trim().length()!=0))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because specifyCause is already provided"));
									throw new OperationException();
								}
								//Death Related to Study
								if(patientEnrollmentDetails.getDeathRelatedToTrial()!=null && (
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
										))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathRelatedToTrial field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRel())
										&&(patientEnrollmentDetails.getDeathRelatedToTrial()==null ||(
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
												)
											))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because deathRelatedToTrial is already provided"));
									throw new OperationException();
								}
								//Reason of Death Related to Study
								if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()!=null)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"raesonOfDeathRelatedToTrial field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRelOther())&& (patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()==null||patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()!=0))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because raesonOfDeathRelatedToTrial is already provided"));
									throw new OperationException();
								}
								personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","A")));
								isPersonBUpdated=true;
							}
						}
					}
					else
					{
						//status provided is already person's current survival status.Do nothing.
					}
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Survival Status Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
					throw new OperationException();
				}
			}
		}
		//Death Date
		if(patientEnrollmentDetails.getDateOfDeath()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"dateOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if (patientEnrollmentDetails.getDateOfDeath().before(personB.getPersonDb()))
		    {
		        response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
		        throw new OperationException();
		    }
	    	Date date = new Date();
	    	if (patientEnrollmentDetails.getDateOfDeath().after(date))
	    	{
	    	      response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Date of death should not be after present day"));
	    	      throw new OperationException();
	    	}
			patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath());
			personB.setPersonDeathDate(DateUtil.dateToString(patientEnrollmentDetails.getDateOfDeath()));
			isPersonBUpdated=true;
		}
		//emptyDeathDate
		if(patientEnrollmentDetails.isEmptyDeathDate())
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())==deadStatPk)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(patientEnrollmentDetails.getDateOfDeath()!=null)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person dateOfDeath provided" ));
				throw new OperationException();
			}
			patProtBean.setPtstDeathDatePersistent(null);
			personB.setPersonDeathDate(null);
			isPersonBUpdated=true;
		}
		//Cause of death
		if(patientEnrollmentDetails.getCauseOfDeath()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
					)
				)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType()))
			{
				if(isMandatory(configDetails, "cod"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_CauseOfDeath_Mandatory));
					throw new OperationException();
				}
				else
				{
					personB.setPatDthCause("");
					isPersonBUpdated=true;
				}
			}
			else
			{
				try{
					personB.setPatDthCause(AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getCauseOfDeath(), CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser));
				}
				catch (CodeNotFoundException e) {
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath Code Not Found: " + patientEnrollmentDetails.getCauseOfDeath().getCode()));
					throw new OperationException();
				}
				isPersonBUpdated=true;
			}
		}
		//specify cause
		if(patientEnrollmentDetails.getSpecifyCause()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk && patientEnrollmentDetails.getSpecifyCause().trim().length()!=0)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"specifyCause field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(patientEnrollmentDetails.getSpecifyCause().trim().length()==0)
			{
				if(isMandatory(configDetails, "specifycause"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_SpecifyCause_Mandatory)); 
				}
				else
				{
					personB.setDthCauseOther("");	
					isPersonBUpdated=true;
				}
			}
			else
			{
				personB.setDthCauseOther(patientEnrollmentDetails.getSpecifyCause());
				isPersonBUpdated=true;
			}
		}
		//Death related to study
		if(patientEnrollmentDetails.getDeathRelatedToTrial()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
												))
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			
			if(StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType()))
			{
				if(isMandatory(configDetails, "deathrelated"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstDthStdRel("");
				}
			}
			else
			{
				try{
					patProtBean.setPtstDthStdRel(AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getDeathRelatedToTrial(), CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser));
				}
				catch (CodeNotFoundException e) {
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial Code Not Found: " + patientEnrollmentDetails.getDeathRelatedToTrial().getCode()));
					throw new OperationException();
				}
			}
		}
		//Reason of Death Related to Study
		if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()!=0)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonOfDeathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			
			
			if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()==0)
			{
				if(isMandatory(configDetails, "reasonofdeathrel"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	MC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory)); 
				}
				else
				{
					patProtBean.setPtstDthStdRelOther("");					
				}
			}
			else
			{
				patProtBean.setPtstDthStdRelOther(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial());
			}
		}
		
		if(isPersonBUpdated)
		{
			personB.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			if(personAgent.updatePerson(personB)!=0)
			{
				response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS,"Error in updating Study Patient Status"));
				throw new OperationException();
			}
		}
    	patStudyStatBean.setPatProtObj(patProtBean);
		patStudyStatAgent.updatePatStudyStat(patStudyStatBean);
	}

}
