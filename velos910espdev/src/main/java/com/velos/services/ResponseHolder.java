/**
 * 
 */
package com.velos.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.velos.services.model.SimpleIdentifier;

/**
 * @author dylan
 *
 */
public class ResponseHolder implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -889273989561751040L;

    public Results results = new Results();
    
    public Issues issues = new Issues();
	
	public ResponseHolder(){
		
	}
	
	public Results getActions(){
		return this.results;
	}
	
	public void addAction(CompletedAction action){
		this.results.addAction(action);
	}
	
	public Issues getIssues(){
		return this.issues;
	}
	
	public void addIssue(Issue issue){
		this.issues.add(issue);
	}
	
	public void addObjectCreatedAction(SimpleIdentifier objectId){
	    this.results.addAction(new CompletedAction(objectId, objectId.getClass().getSimpleName(), CRUDAction.CREATE));
	}
}
