/**
 * 
 */
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationException")
public class OperationException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1220801809776763727L;
    
    protected Issues issues = new Issues();
	

	public OperationException() {
		super();
	}

	public OperationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public OperationException(String arg0) {
		super(arg0);
	}

	public OperationException(Throwable arg0) {
		super(arg0);
	}
	
	/**
	 * @param issues2
	 */
	public OperationException(Issues issues) {
		this.issues = issues;
	}

	public Issues getIssues(){
		return this.issues;
	}
	
	public void setIssues(Issues issues){
		this.issues = issues;
	}
	

}
