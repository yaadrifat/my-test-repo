/**
 * 
 */
package com.velos.services;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.SessionContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger; 

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.ServiceObject;
import com.velos.services.model.User;
import com.velos.services.model.UserIdentifier;
import com.velos.services.user.UserService;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author dylan
 *
 */
public abstract class AbstractService {
	public static final String IP_ADDRESS_FIELD_VALUE = "SvcsFrmwrk"; // cannot exceed 15 bytes
	public static final String RESPONSE_ISSUES_PROP_KEY = "velos.response.issues";
	public static final String RESPONSE_SYSTEM_IDS_PROP_KEY = "velos.response.system.ids";
	public static final String NUMBER_ZERO = "0";

	protected static String SYSTEM_LINE_SEP = System.getProperty("line.separator");	
	protected SessionContext sessionContext;
	
	protected UserBean callingUser;
	
	protected ResponseHolder response = new ResponseHolder();
	
	private static Logger logger = Logger.getLogger(AbstractService.class);
	/**
	 * Retreives the user identity set by the client and available through
	 * sessionContext. Calls sessionContext.getCallerPrincipal() and 
	 * uses the username of the principal to produce fetch the UserBean of that user.
	 * 
	 * @param sessionContext
	 * @param userAgent
	 * @return The UserBean instance for the logged in user, or null if not found.
	 * @throws AuthenticationException Thrown if the sessionContext is in a bad state, or for a number
	 * of other Throwables caught. Check the cause for detailed information.
	 */
	protected UserBean getLoggedInUser(
			SessionContext sessionContext, 
			UserAgentRObj userAgent) 
		throws 
			AuthenticationException{

		Principal principal = null;
		try{
			principal = sessionContext.getCallerPrincipal();
		}
		catch (IllegalStateException e){
			throw new AuthenticationException(
							"IllegalStateException. This " +
							"may indicate a problem  setting up the client LoginContext");
		}
		catch(Throwable t){
			throw new AuthenticationException(
						"Exception finding requesting user, could not" +
						"get caller principal.");
		}
		
		UserBean userBean = null;
		try{
			userBean = userAgent.findUser(principal.getName());
		}
		catch(Throwable t){
			throw new AuthenticationException(
					"Exception finding requesting user.");
		}
		if (userBean == null){
			throw new AuthenticationException("user not found: " + principal.getName());
		}
		return userBean;
		
	}

	protected static List<Issue> getDataValidationIssues(ServiceObject  study){
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<ServiceObject>> constraintViolations = validator.validate(study);
		List<Issue> issues = new ArrayList<Issue>();
		
		for (ConstraintViolation<ServiceObject> violation : constraintViolations){
			StringBuilder violationMsg = new StringBuilder();
			violationMsg.append("Field: " + violation.getPropertyPath().toString());
			violationMsg.append(" issue: " + violation.getMessage());
			Issue issue = 
				new Issue(
						IssueTypes.DATA_VALIDATION,
						violationMsg.toString());
			issues.add(issue);
		}
		return issues;
	}

	public static String dereferenceCodeStr(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
		Integer codePK = dereferenceCode(code, codeType, requestingUser);
		if (codePK != null){
			return codePK.toString();
		}
		return null;
	}
	
	protected static Integer dereferenceCode(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
		if (code == null) return null;
		CodeCache codes = CodeCache.getInstance();
		return 
			codes.dereferenceCode(code, 
					codeType, new Integer(requestingUser.getUserAccountId()));
	}
	//Virendra
	public static Integer dereferenceSchCode(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
		if (code == null) return null;
		CodeCache codes = CodeCache.getInstance();
		return 
			codes.dereferenceSchCode(code, 
					codeType, new Integer(requestingUser.getUserAccountId()));
	}
	
	//Kanwal 
	public static String dereferenceSchCodeStr(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
		
		Integer codePK = dereferenceSchCode(code, codeType, requestingUser);
		if (codePK != null){
			return codePK.toString();
		}
		return null;
	}
	
	//Raman
	public static Integer dereferenceTzCode(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
		
		if (code == null) return null;
		CodeCache codes = CodeCache.getInstance();
		return 
			codes.dereferenceTzCode(code, 
					codeType, new Integer(requestingUser.getUserAccountId()));
		}
		
	//Raman 
	public static String dereferenceTzCodeStr(Code code, String codeType, UserBean requestingUser)
	throws CodeNotFoundException{
			
			Integer codePK = dereferenceTzCode(code, codeType, requestingUser);
			if (codePK != null){
				return codePK.toString();
			}
			return null;
		}
	

	
	/**
	 * Creates a non-system user using the UserIdentifier object passed into the 
	 * method. It sets the new user's account and ogranization to the same account
	 * and organization as the calling user.
	 * @param userAgent
	 * @param objectMapService
	 * @param userIdent
	 * @param callingUser
	 * @return
	 * @throws OperationException
	 */
	protected UserBean createNonSystemUser(
			UserAgentRObj userAgent,
			ObjectMapService objectMapService,
			UserIdentifier userIdent,
			UserBean callingUser)
	throws OperationException{
		//@TODO in a future version, drop the two service input parameters (ObjectMapService and
		//UserAgenRObj and just use the EJBUitil.getFooHome() methods.
		
		//DRM 5401 - Added more required fields for non-system user creation
		//5731 and 5730 DRM 1/17/11 - need to add a minimal
		//row to er_add in order for the user to show up in lookups
		
		AddressBean address = new AddressBean();
		address.setCreator(getCreatorString());
		address.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		AddressAgentRObj addressAgent = EJBUtil.getAddressAgentHome();
		int addressPK = addressAgent.setAddressDetails(address);
		if (addressPK < 1){
			addIssue( 
				new Issue(
						IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, "Error creating new non-system user address entry"));
			throw new OperationException();
		}
		
		UserBean newNonSystemUser = new UserBean();
		newNonSystemUser.setUserFirstName(userIdent.getFirstName());
		newNonSystemUser.setUserLastName(userIdent.getLastName());
		newNonSystemUser.setUserType(ObjectLocator.USER_TYPE_NON_SYSTEM);
		newNonSystemUser.setUserSiteId(callingUser.getUserSiteId());
		newNonSystemUser.setUserAccountId(callingUser.getUserAccountId());
		//added following line for 5728 DRM 1/14/11
		newNonSystemUser.setUserStatus(User.UserStatus.ACTIVE.getLegacyString());
		newNonSystemUser.setCreator(getCreatorString());
		newNonSystemUser.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		newNonSystemUser.setUserHidden(NUMBER_ZERO);
		newNonSystemUser.setUserPerAddressId(EJBUtil.integerToString(addressPK));
		int returnId = userAgent.setUserDetails(newNonSystemUser);
		if (returnId < 1){
			addIssue( 
				new Issue(
						IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, "Error creating new non-system user"));
			throw new OperationException();
		}
		newNonSystemUser.setUserId(returnId);
		
		userAgent.flush(); 
		
		
		//DRM Removed references to system ID and object map service for 2.0 build 100
//		//we just created a new user...let's create a map id for it
//		String newUserSysId = objectMapService.createObjectMap(
//				UserService.TABLE_NAME, 
//				newNonSystemUser.getUserId());
//		userIdent.setSystemId(newUserSysId);
		//add new id to the response to tell the caller that we created the user
		response.addAction(
				new CompletedAction(
						userIdent, 
						CRUDAction.CREATE));
		return newNonSystemUser;
	}
	
	/**
	 * When storing booleans in eResearch, the flags are often stored in a bean
	 * as a string that holds a 1 or a 0. This method translates from Boolean to that.
	 * 
	 * @param bool
	 * @return true=1, false=0, null=0
	 */
	protected static String booleanToOneZeroFlag(Boolean bool){
		if (bool != null){
			return bool ? "1" : "0";
		}
		return "0";
	}
	
	protected UserIdentifier getUserIdFromPKStr(
			String userPKStr,
			ObjectMapService objectMapService,
			UserAgentRObj userAgent){
		if (userPKStr == null) return null;
		Integer userPK = new Integer(userPKStr);
		return getUserIdFromPK(userPK, objectMapService, userAgent);
	}
	
	protected UserIdentifier getUserIdFromPK(
			Integer userPK,
			ObjectMapService objectMapService,
			UserAgentRObj userAgent){
		
		if (userPK == null) return null;
		ObjectMap map = null;
		try{
			map = objectMapService.
					getOrCreateObjectMapFromPK(
							UserService.TABLE_NAME, 
							userPK);
		}
		catch(Throwable t){
			logger.debug("Map does not exist for " + new Integer(userPK));
		}
				
		UserBean userBean = userAgent.getUserDetails(userPK);
		UserIdentifier userId = 
		new UserIdentifier(
				userBean.getUserLoginName(), 
				userBean.getUserFirstName(), 
				userBean.getUserLastName());
		if (map != null){
			//TODO fill in rest of UserIdentifier fields by building a UserBean??
//			UserIdentifier userId = 
//				new UserIdentifier(map.getSystemId());		
			
			userId.setOID(map.getOID());
		}  
			
			return userId;
		
		
	}
	
	
	//This method could be used to merge a list of similar objects
	//with a list of persisted objects, in a generic fashion. Not needed
	//now, but saving in this file for later use.
//	/**
//	 * Utility class that tkaes a list of incoming Service objects,
//	 * and calculates the necessary actions to update that list with the 
//	 * existing list. For each incoming object, the ServiceObjectCRUDCallback is
//	 * called to find that individual object.
//	 * 
//	 * If the list of incomingObjects is null or empty, this method will return a
//	 * list of REMOVES, one for each item in the existingPKs list.
//	 * 
//	 * 
//	 * 
//	 * @param incomingObjects ServiceObject list of thet incoming objects.
//	 * @param existingPKs List of primary keys of the currently-persisted objects.
//	 * @param callback
//	 * @return List of PersistActions. Each item will tell you whether you needs
//	 * to update, remove, create.
//	 * @throws OperationException
//	 * @throws MultipleObjectsFoundException
//	 */
//	protected void syncServiceObjectCollection(
//			List<? extends ServiceObject> incomingObjects,
//			List<Integer> existingPKs,
//			ServiceObjectCRUDCallback callback)
//			throws OperationException, MultipleObjectsFoundException{
//		
//		if (existingPKs == null) existingPKs = new ArrayList<Integer>();
//		
//		//Return table telling the caller what actions to take
//		List<PersistAction> actionsList = 
//			new ArrayList<PersistAction>();
//		
//	
//		if (incomingObjects == null || 
//				incomingObjects.size() == 0){
//			for (Integer existingPK : existingPKs){
//				actionsList.add(new PersistAction(CRUDAction.REMOVE, existingPK));
//			}
//		}
//		else{
//			//Calculate the list of PKs for each of the incoming objects.
//			//This list will be used to 
//			List<Integer> incomingPKs = new ArrayList<Integer>();
//			
//			for (ServiceObject incomingObject : incomingObjects){
//				Integer incomingPK = null;
//				try {
//					incomingPK = callback.locate(incomingObject);
//				} catch (MultipleObjectsFoundException e) {
//					logger.error("AbstractService.calculateListUpdateActions: Multiple object found when locating " + incomingObject.toString(), e);
//					continue;
//				}
//				if (incomingPK == null || incomingPK < 1){
//					//this object wasn't found, must be new.
//					actionsList.add(new PersistAction(CRUDAction.CREATE, incomingObject));
//				}
//				else{
//					//object was found, must be an update
//					actionsList.add(new PersistAction(CRUDAction.UPDATE, incomingObject, incomingPK));
//					//we remove the pk, since we're later going to have to
//					//loop through the existingPKs list for items not found here...better
//					//to make that loop smaller.
//					existingPKs.remove(incomingPK);
//				}
//				incomingPKs.add(incomingPK);
//			}
//		
//		
//			//Calculate Removes by going through the list of existing pks
//			//Recall that this list was an input parameter to this method,
//			//but we pruned it down above as we found pks that required 
//			//an update.
//			for (Integer existingPK : existingPKs){
//				//if a pk is in the existing list, but not the update list, we've got a remove.
//				if (!incomingPKs.contains(existingPK)){
//					actionsList.add(new PersistAction(CRUDAction.REMOVE, existingPK));
//				}
//			}
//		}
//		
//		for (PersistAction action : actionsList){
//			if (action.getActionType() == CRUDAction.CREATE){
//				callback.create(action);
//			}
//			
//			if (action.getActionType() == CRUDAction.UPDATE){
//				callback.update(action);
//			}
//			
//			if (action.getActionType() == CRUDAction.REMOVE){
//				callback.remove(action);
//			}
//		}
//	}
	
	protected void addIssue(Issue issue){
		response.getIssues().add(issue);
	}
	
	protected void addUnknownThrowableIssue(Throwable t){
		StringBuilder message = new StringBuilder();
		message.append(t.getMessage() + SYSTEM_LINE_SEP);
		for (StackTraceElement elem : t.getStackTrace()){
			message.append(elem.toString() + SYSTEM_LINE_SEP);
		}
		response.getIssues().add(
				new Issue(
						IssueTypes.UNKNOWN_THROWABLE, 
						message.toString()));
	}
	
	protected void validate(ServiceObject object)
	throws ValidationException{
		//check for any validation issues on the incoming study object
		List<Issue> validationIssues = getDataValidationIssues(object);
		if (validationIssues.size() > 0){
			response.getIssues().addAll(validationIssues);
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for object " + object.toString());
			throw new ValidationException();
		}
	}
	
	protected String getCreatorString(){
		return new Integer(callingUser.getUserId()).toString();
	}
	
}
