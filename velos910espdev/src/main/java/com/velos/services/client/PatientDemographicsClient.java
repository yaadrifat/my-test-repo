package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.model.UpdatePatientDemographics;
import com.velos.services.patientdemographics.PatientDemographicsService;
import com.velos.services.patientdemographics.PatientFacilityService;
import com.velos.services.patientdemographics.UpdatePatientDemographicsService;
import com.velos.services.searchpatient.SearchPatientService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for PatientDemographics Services 
 * @author Virendra
 *
 */
public class PatientDemographicsClient{
	/**
	 * Invokes the remote object.
	 * @return remote PatientDemographics service
	 * @throws OperationException
	 */
	
	private static PatientDemographicsService getPatientDemoRemote()
	throws OperationException{
		
		PatientDemographicsService PatientDemoRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatientDemoRemote =
			(PatientDemographicsService) ic.lookup(JNDINames.PatientDemographicsServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatientDemoRemote;
	}
	
	private static SearchPatientService getSearchPatientRemote()
	throws OperationException{
		
		SearchPatientService searchPatientService = null; 
		InitialContext ic; 
		try{
			ic = new InitialContext(); 
			searchPatientService = (SearchPatientService) ic.lookup(JNDINames.SearchPatientServiceImpl); 
			
		}catch(NamingException e)
		{
			throw new OperationException(e);
		}
		return searchPatientService;	
	}
	
	private static UpdatePatientDemographicsService updatePatientRemote()
	throws OperationException {
		System.out.println("reached client file");
		UpdatePatientDemographicsService updatePatientRemote = null;
		try {
			InitialContext ic = new InitialContext();
			updatePatientRemote = (UpdatePatientDemographicsService) ic
			.lookup(JNDINames.UpdatePatientDemographicsServiceImpl);
		} catch (NamingException e) {
			throw new OperationException(e);
		}
		return updatePatientRemote;
	}
	
	private static PatientFacilityService patientFacilityRemote() throws OperationException
	{
		PatientFacilityService patientFacilityService = null;
		try {
			InitialContext ic = new InitialContext();
			patientFacilityService = (PatientFacilityService) ic.lookup(JNDINames.PatientFacilityServiceImpl);
		} catch (NamingException e) {
			throw new OperationException(e);
		}
		if (patientFacilityService == null)
	    {
		  ResponseHolder response = new ResponseHolder();
	      Issue issue = new Issue(IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION, 
	        "Exception getting remote update patient facility service");
	      response.addIssue(issue);
	      throw new OperationException();
	    }
		return patientFacilityService;
	}
	
	/**
	 * Client method calls PatientDemographics Service 
	 * to get Patient Demographics object
	 * @param patientId
	 * @return
	 * @throws OperationException
	 */
	public static PatientDemographics getPatientDemographics(PatientIdentifier patientId) 
	throws OperationException{
		ResponseHolder response = new ResponseHolder();
		PatientDemographicsService patientDemoService = getPatientDemoRemote();
		if(patientDemoService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES, 
					"Exception getting remote patient demographics service:"+patientDemoService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return patientDemoService.getPatientDemographics(patientId);
	}
	/**
	 * @param patient
	 * @return
	 */
	public static ResponseHolder createPatient(Patient patient)
	throws OperationException{
		ResponseHolder response = new ResponseHolder(); 
		PatientDemographicsService patientDemoService = getPatientDemoRemote(); 
		if(patientDemoService == null )
		{
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES, 
					"Exception getting remote patient demographics service:"+patientDemoService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return patientDemoService.createPatient(patient);
	}

	/**
	 * @param paramPatientIdentifier
	 * @param paramPatientUpdateDemographics
	 * @return
	 * @throws OperationException 
	 */
	public static ResponseHolder updatePatient(
			PatientIdentifier paramPatientIdentifier,
			UpdatePatientDemographics paramPatientUpdateDemographics) throws OperationException {
		UpdatePatientDemographicsService updatePatientService = updatePatientRemote(); 
		return updatePatientService.UpdatePatientDemographics(paramPatientIdentifier, paramPatientUpdateDemographics);
	}
	
	public static ResponseHolder addPatientFacility(PatientIdentifier patId, PatientOrganization patOrg) throws OperationException
	{
		PatientFacilityService patientFacilityService  = patientFacilityRemote();
		return patientFacilityService.addPatientFacility(patId, patOrg);
	}
	
	public static ResponseHolder updatePatientFacility(PatientIdentifier patIdentifier,
			OrganizationIdentifier orgID,PatientOrganizationIdentifier paramOrganizationIdentifier, PatientOrganization paramPatientOrganization) throws OperationException
	{
		PatientFacilityService patientFacilityService  = patientFacilityRemote();
		return patientFacilityService.updatePatientFacility(patIdentifier, orgID, paramOrganizationIdentifier, paramPatientOrganization);
	}

	/**
	 * @param paramPatientIdentifier
	 * @return
	 * @throws OperationException 
	 */
	public static PatientSearchResponse searchPatient(
			PatientSearch paramPatientIdentifier) throws OperationException {
		SearchPatientService searchPatientService = getSearchPatientRemote();
		return searchPatientService.searchPatient(paramPatientIdentifier);
	}


}