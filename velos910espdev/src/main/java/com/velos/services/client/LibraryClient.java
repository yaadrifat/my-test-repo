package com.velos.services.client;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.library.LibraryService;
import com.velos.services.model.EventSearch;
import com.velos.services.model.Events;
import com.velos.services.model.LibraryEvents;
import com.velos.services.util.JNDINames;

public class LibraryClient {
	
	private static LibraryService getLibraryRemote()
	throws OperationException{
		
	LibraryService LibraryRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		LibraryRemote =
			(LibraryService) ic.lookup(JNDINames.LibraryServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return LibraryRemote;
	}
	
	public static LibraryEvents searchEvent(EventSearch eventSearch) 
	throws OperationException{
		LibraryService libraryService = getLibraryRemote();
		return libraryService.searchEvent(eventSearch);
	}

}
