package com.velos.services.client;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.patientstudy.PatientStudyService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for Patient Study Services 
 * @author Virendra
 *
 */
public class PatientStudyClient{
	/**
	 * Invokes the remote object.
	 * @return remote PatientStudy service
	 * @throws OperationException
	 */
	private static PatientStudyService getPatientStudyRemote()
	throws OperationException{
		
	PatientStudyService PatientStudyRemote = null;
	InitialContext ic;
		try{
			ic = new InitialContext();
			PatientStudyRemote =
				(PatientStudyService) ic.lookup(JNDINames.PatientStudyServiceImpl);
			}
			catch(NamingException e){
				throw new OperationException(e);
			}
		return PatientStudyRemote;
	}
	/**
	 * get PatientStudyRemote object and calls getPatientStudies 
	 * to return list of Patient Studies. 
	 * @param patientId
	 * @return List<PatientStudy>
	 * @throws OperationException
	 */
	public static List<PatientStudy> getPatientStudy(PatientIdentifier patientId) 
	throws OperationException{
		PatientStudyService patientStudyService = getPatientStudyRemote();
		return patientStudyService.getPatientStudies(patientId);
	}
}