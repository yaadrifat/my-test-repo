package com.velos.services.client;

import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Code;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.studypatient.StudyPatientService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for StudyPatient Services 
 * @author Virendra
 *
 */
public class StudyPatientClient{
	/**
	 * Invokes the remote object.
	 * @return remote StudyPatient service
	 * @throws OperationException
	 */
	
	private static StudyPatientService getStudyPatientRemote()
	throws OperationException{
		
	StudyPatientService StudyPatientRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		StudyPatientRemote =
			(StudyPatientService) ic.lookup(JNDINames.StudyPatientServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return StudyPatientRemote;
	}
	/**
	 * Calls getStudyPatient on getStudyPatient Remote object
	 * to return List of StudyPatients 
	 * @param studyId
	 * @return List<StudyPatient>
	 * @throws OperationException
	 */
	public static List<StudyPatient> getStudyPatients(StudyIdentifier studyId) 
	throws OperationException{
		StudyPatientService studyPatientService = getStudyPatientRemote();
		return studyPatientService.getStudyPatients(studyId);
	}
	
	public static ResponseHolder enrollPatientToStudy(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEntrollmentDetails)
	throws OperationException
	{
		StudyPatientService studyPatientRemote = getStudyPatientRemote(); 
		ResponseHolder response = studyPatientRemote.enrollPatientToStudy(patientIdentifier, studyIdentifier, patientEntrollmentDetails); 
		return response;
	}
	
	public static ResponseHolder createAndEnrollPatient(Patient patient, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException, OperationRolledBackException
	{
		StudyPatientService studyPatientRemote  = getStudyPatientRemote(); 
		ResponseHolder response = studyPatientRemote.createAndEnrollPatient(patient, studyIdentifier, patientEnrollmentDetails); 
		return response; 
	}
	public static ResponseHolder deleteStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier) throws OperationException {
		StudyPatientService studyPatientRemote  = getStudyPatientRemote(); 
		ResponseHolder response = studyPatientRemote.deleteStudyPatientStatus(patientStudyStatusIdentifier);
		return response; 
	}
	public static ResponseHolder updateStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier,PatientEnrollmentDetails patientEnrollmentDetails) throws OperationException {
		StudyPatientService studyPatientRemote  = getStudyPatientRemote(); 
		ResponseHolder response = studyPatientRemote.updateStudyPatientStatus(patientStudyStatusIdentifier,patientEnrollmentDetails);
		return response;
	}
}