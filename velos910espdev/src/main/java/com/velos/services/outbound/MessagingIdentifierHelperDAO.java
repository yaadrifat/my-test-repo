/**
 * Created On Mar 29, 2011
 */
package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.CommonDAO;

/**
 * @author Kanwaldeep
 *
 */
public class MessagingIdentifierHelperDAO extends CommonDAO{
	private static Logger logger = Logger.getLogger(MessagingIdentifierHelperDAO.class); 
	
	private String GET_STUDY_SQL = "select study_number from er_study where pk_study = ?"; 
	
	private String GET_USER_SQL = "select usr_lastname, usr_firstname, usr_logname from er_user where pk_user = ?"; 
	
	public String getStudyNumber(int pkStudy)
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String studyNumber = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_STUDY_SQL); 
			stmt.setInt(1, pkStudy); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				studyNumber = rs.getString("study_number"); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Study Number for study "+sqe); 
		}finally
		{
			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			returnConnection(conn); 
		}
		
		return studyNumber; 
	}
	
	public Map<String, String> getUserInformation(int pkUser)
	{
		Map<String, String> userinfo = new HashMap<String, String>(); 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String studyNumber = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_USER_SQL); 
			stmt.setInt(1, pkUser); 
			
			rs = stmt.executeQuery(); 
			if(rs.next())
			{
				userinfo.put("firstname", rs.getString(2)); 
				userinfo.put("lastname", rs.getString(1)); 
				userinfo.put("logname", rs.getString(3)); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Study Number for study "+sqe); 
		}finally
		{
			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			returnConnection(conn); 
		}
		
		return userinfo; 	
	}

}
