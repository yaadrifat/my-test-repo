package com.velos.services.library;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.EventSearch;
import com.velos.services.model.LibraryEvent;
import com.velos.services.model.LibraryEvents;
import com.velos.services.patientdemographics.PatientDemographicsDAO;
import com.velos.services.util.ServicesUtil;

public class LibraryServiceDAO extends CommonDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4728938754447175606L;
	private static Logger logger = Logger.getLogger(LibraryServiceDAO.class);
	
	private static String searchEvents = "SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' " +
                                         "else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name , " +
                                         "(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, " + 
                                         "(select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) " + 
                                         "category_desc,event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' " +  
                                         "else notes end as notes , lst_cost(event_id) as eventcost,  lst_additionalcode(event_id,'evtaddlcode') as additionalcode, " +
                                         "(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b " +
                                         "WHERE CHAIN_ID= 2703 AND TRIM(UPPER(EVENT_TYPE)) IN ('E','L') ";
	
	
	public static LibraryEvents searchEvent(EventSearch eventSearch, int eventLibraryType, Map<String, Object> parameters) throws OperationException{
		
		Connection conn = getConnection();		
		PreparedStatement pstmt = null;
		String searchClause = "";
		String fromClause = "";
		StringBuffer eventDefSql = new StringBuffer();
		StringBuffer countSql = new StringBuffer();
		
		UserBean usrBean = (UserBean)parameters.get("callingUser"); 
		int acctId = usrBean.userAccountId;
//		PreparedStatement pstmt = null;
//		Connection conn = null;
		LibraryEvents libraryEvents = new LibraryEvents();
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		
		try{
            
	        if (!StringUtil.isEmpty(eventSearch.getEventNameDescNotes())){
	              searchClause = " AND ( LOWER(DESCRIPTION) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') " +
	              		         "OR LOWER(NAME) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') " +
	              		         "OR LOWER(NOTES) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') )";
	        }
	        
			if (!StringUtil.isEmpty(eventSearch.getCptCode().getDescription())) {
				searchClause = searchClause + " AND (LOWER(EVENT_CPTCODE) LIKE LOWER('%"+eventSearch.getCptCode().getCode().trim()+"%')) ";
			}
			
			if (!StringUtil.isEmpty(eventSearch.getFacility().getSiteAltId())) {
				searchClause = searchClause + " AND (LOWER(FACILITY_ID) LIKE LOWER('%"+eventSearch.getFacility().getSiteAltId()+"%')) ";
			}
	        
			if (!StringUtil.isEmpty(eventSearch.getCostType().getDescription())) {
				searchClause = searchClause + " AND (LOWER(fk_cost_desc) LIKE LOWER('%"+eventSearch.getCostType().getDescription()+"%')) and fk_event = event_id ";
				fromClause = fromClause + " , sch_eventcost ";

			}
			
			searchClause = searchClause + " AND EVENT_LIBRARY_TYPE = "+eventLibraryType +" ";
			
			if (eventSearch.getEventCategory().equals("") || eventSearch.getEventCategory().equals(" ")){
				
				eventDefSql.append("SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' " +
						    "else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ," +
						    "(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, " +
						    "(select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc,event_cptcode, " +
						    "case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, " +
						    "lst_cost(event_id) as eventcost, lst_additionalcode(event_id,'evtaddlcode') as additionalcode," +
						    "(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ");
				
				eventDefSql.append(fromClause);
				
				eventDefSql.append(" WHERE TRIM(USER_ID)= "+acctId+" AND TRIM(UPPER(EVENT_TYPE)) = 'E'") ;
				
				countSql.append("select count(*) from (SELECT distinct event_id FROM EVENT_DEF ");
				countSql.append(fromClause);
				countSql.append(" WHERE  TRIM(USER_ID)= "+acctId+" AND TRIM(UPPER(EVENT_TYPE)) IN('L','E')");
			}
			else{
				eventDefSql.append("SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' " +
						    "else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ," +
						    "(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, " +
						    "(select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc," +
						    "event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes , " +
						    "lst_cost(event_id) as eventcost,  lst_additionalcode(event_id,'evtaddlcode') as additionalcode," +
						    "(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ");


				eventDefSql.append(fromClause);
				eventDefSql.append(" WHERE CHAIN_ID= "+eventSearch.getEventCategory()+" AND TRIM(UPPER(EVENT_TYPE)) = 'E'" );
				
				//ORDER BY CHAIN_ID";
				countSql.append("select count(*) from (SELECT distinct event_id FROM EVENT_DEF ");
				countSql.append(fromClause);
				countSql.append(" WHERE  CHAIN_ID= "+eventSearch.getEventCategory()+" and TRIM(UPPER(EVENT_TYPE)) IN('L','E') ");

			}
			
		    eventDefSql.append(searchClause);
		    countSql.append(searchClause); 
		    countSql.append(" )");
		    
		    String mainSql = eventDefSql.toString();
		    
			try{
				
			    List<LibraryEvent> libraryEventList = new ArrayList<LibraryEvent>();  
				pstmt = conn.prepareStatement(mainSql);
			      
			      ResultSet rs = pstmt.executeQuery();
			      
			      if(rs != null){
			    	  
				      while (rs.next())
				      {
				    	LibraryEvent libraryEvent = new LibraryEvent();
				    	libraryEvent.setEventName(rs.getString("EVENT_NAME"));
				    	libraryEvent.setDescription(rs.getString("DESCRIPTION"));
				    	libraryEvent.setEventCategory(rs.getString("CATEGORY"));
				    	libraryEventList.add(libraryEvent);
				      }
			      }
			      
			      libraryEvents.setLibraryEvent(libraryEventList);
			      return libraryEvents;
				
			}catch(Throwable t){
	    		t.printStackTrace();
	    		throw new OperationException();
	    	}
	    	
	    	finally{
	    		try{
	    			if(pstmt!=null){
	    				pstmt.close();
	    			}
	    		}catch(Exception e){
	    			
	    		}
	    		try{
	    			if(conn!=null){
	    				conn.close();
	    			}
	    		}catch(Exception e){
	    			
	    		}
	    	}
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
		
	}

}
