package com.velos.services.patientschedule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.AccountDao;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.groupAgent.impl.GroupAgentBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.personAgent.impl.PersonAgentBean;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.userSiteAgent.impl.UserSiteAgentBean;
import com.velos.esch.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.studySite.StudySiteJB;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.web.eventdef.EventdefJB;
import com.velos.esch.web.eventresource.EventResourceJB;
import com.velos.esch.web.eventstat.EventStatJB;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.studypatient.StudyPatientDAO;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
/**
 * @author Tarandeep Singh Bali
 *
 */
@Stateless
@Remote(PatientScheduleService.class)
public class PatientScheduleServiceImpl 
extends AbstractService 
implements PatientScheduleService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@EJB
private StudySiteAgentRObj studySiteAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;
@EJB
private PersonAgentRObj personAgentBean;
@EJB
private AccountAgentRObj accountAgent;
@EJB
private PatProtAgentRObj patProtAgent;
@Resource 
private SessionContext sessionContext;
	
	private static Logger logger = Logger.getLogger(PatientScheduleServiceImpl.class.getName());
	/**
	 * 
	 */
				
	public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		
		PatientSchedules patientSchedules = new PatientSchedules();
		
		try{
			Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
				
			if (studyPK == null || studyPK==0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
				throw new OperationException("Patient study not found");
			}
			
			if(patientId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required")); 
				throw new OperationException(); 
			}
			if((patientId.getOID() == null || patientId.getOID().length() == 0) 
					&& ((patientId.getPatientId() == null || patientId.getPatientId().length() == 0)
							|| (patientId.getOrganizationId() == null) ) )
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			
			
			
			Integer personPK = ObjectLocator.personPKFromPatientIdentifier(
					callingUser, 
					patientId, 
					objectMapService);
			
			if (personPK == null || personPK == 0){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
				throw new OperationException("Patient not found");
			}
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}

			Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyID, personPK); 
			
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
			if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Study Team"));
				throw new AuthorizationException("User does not have view permission to view Study Team");
			}
			
			
			int studyStatusPrivileges = teamAuthModule.getStudyStatusPrivileges();
			//Add Study Patient Access Validation:Tarandeep Singh Bali 
			int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();
			if ((!TeamAuthModule.hasViewPermission(studyStatusPrivileges) || (!TeamAuthModule.hasViewPermission(patientManagePrivileges)))){
				if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Manage Patient details"));
				throw new AuthorizationException("User does not have view permission to view the Manage Patient details");
			}
			
			//PatientScheduleDAO call for PatientSchedule with primary info
		   patientSchedules = PatientScheduleDAO.getPatientScheduleList(personPK, studyPK);
			
			
			PatientScheduleDAO patScheduleDAO = new PatientScheduleDAO();
			return patientSchedules;
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

	}
	
	public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleIdentifier)
		throws OperationException{
			try{
				int patProtPK = 0;
				int studyPK = 0;
				Integer personPK = 0;
				PatientSchedule patientSchedule = new PatientSchedule();
				
				GroupAuthModule groupAuth = 
						new GroupAuthModule(callingUser, groupRightsAgent);
					Integer manageProtocolPriv = 
						groupAuth.getAppManagePatientsPrivileges();
					
					boolean hasViewManageProt = 
						GroupAuthModule.hasViewPermission(manageProtocolPriv);
					
					if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
					if (!hasViewManageProt){
						addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
						throw new AuthorizationException("User Not Authorized to view Patient data");
					}
					
					
				if(scheduleIdentifier != null && scheduleIdentifier.getOID() != null && scheduleIdentifier.getOID().length() > 0)
					{
						ObjectMap objectMap = objectMapService.getObjectMapFromId(scheduleIdentifier.getOID()); 
						
						if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE)))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid ScheduleIdentifier"));
							throw new OperationException();
						}
				        
						patProtPK = objectMap.getTablePK();
						studyPK = PatientScheduleDAO.getStudyPK(patProtPK);
						TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
						int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
						if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
							addIssue(
									new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
											"User does not have view permission to view Study Team"));
							throw new AuthorizationException("User does not have view permission to view Study Team");
						}
						//Bug Fix : 12463 && 12467
						int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
						if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
							addIssue(
									new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
											"User does not have view permission to view the Patient data"));
							throw new AuthorizationException("User does not have the view permission to view the Patient data");
						}
						
			           personPK = PatientScheduleDAO.getPersonPK(patProtPK);
			           
						if (personPK == null || personPK == 0){
							addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
							throw new OperationException("Patient not found");
						}
						
						PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
						
						if(patProtBean.getPatProtId() == 0)
						{
							addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
							throw new OperationException(); 
						}
						
					    patientSchedule = PatientScheduleDAO.getPatientSchedule(patProtPK);
						
				
				return patientSchedule;
				
				}else
					{
						addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND, "Valid ScheduleIdentifier is required to get Patient Schedule")); 
						throw new OperationException(); 
					}
				
			}		catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
				throw new OperationException(t);
			}
		}
	// Navneet
	public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
			throws OperationException{
				try{
					
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("response", this.response);
				
					Integer studyPK = 0;
					Integer personPK = 0;
					PatientSchedule currentPatientSchedule = new PatientSchedule();
					
					GroupAuthModule groupAuth = 
							new GroupAuthModule(callingUser, groupRightsAgent);
						Integer manageProtocolPriv = 
							groupAuth.getAppManagePatientsPrivileges();
						
						boolean hasViewManageProt = 
							GroupAuthModule.hasViewPermission(manageProtocolPriv);
						
						if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
						if (!hasViewManageProt){
							addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
							throw new AuthorizationException("User Not Authorized to view Patient data");
						}
						
						if((StringUtil.isEmpty(studyIdentifier.getOID()))
								&& (StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier is required to get Patient Schedule"));
								throw new OperationException();
							}
						
						if((StringUtil.isEmpty(patientIdentifier.getOID()))
								&& (StringUtil.isEmpty(patientIdentifier.getPatientId()))
								&&(StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID()) && StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())))
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
								throw new OperationException();
							}
						   
						    if((patientIdentifier.getOID() == null || patientIdentifier.getOID().length() == 0) 
								&& ((patientIdentifier.getPatientId() == null || patientIdentifier.getPatientId().length() == 0)
										|| (patientIdentifier.getOrganizationId() == null) ) )
						   {
							  addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
							  throw new OperationException(); 
						   }

						    
							studyPK =
									ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
									
								if (studyPK == null || studyPK==0){
									addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
									throw new OperationException("Patient study not found");
								}
							 personPK = ObjectLocator.personPKFromPatientIdentifier(
									callingUser, 
									patientIdentifier, 
									objectMapService);

							TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
							int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
							if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
								addIssue(
										new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
												"User does not have view permission to view Study Team"));
								throw new AuthorizationException("User does not have view permission to view Study Team");
							}//Bug Fix : 12439
							
							int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
							if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
								addIssue(
										new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
												"User does not have view permission for the Patient data"));
								throw new AuthorizationException("User does not have the view permission for the Patient data");
							}//Bug Fix : 12440
									           
							if (personPK == null || personPK == 0){
								addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
								throw new OperationException("Patient not found");
							}
							
							PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
							
							if(patProtBean.getPatProtId() == 0)
							{
								addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
								throw new OperationException(); 
							}
							
							String parseStartdate = DateUtil.dateToString(startDate);
							String parseEndDate = DateUtil.dateToString(endDate);
													
						    currentPatientSchedule = PatientScheduleDAO.getCurrentPatientSchedule(personPK, studyPK,parseStartdate, parseEndDate,parameters);
						    currentPatientSchedule.setEndDate(endDate);
						    currentPatientSchedule.setStartDate(startDate);
						    return currentPatientSchedule;
					
					
				}		catch(OperationException e){
					sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
					e.setIssues(response.getIssues());
					throw e;
				}
				catch(Throwable t){
					this.addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
					throw new OperationException(t);
				}
			}
		
	
	public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus) throws OperationException{
		EventStatJB eventStatB = new EventStatJB();
		EventResourceJB eventResB = new EventResourceJB();
        EventdefJB eventdefB = new EventdefJB();
		EventStatusIdentifier eventStatusIdentifier = new EventStatusIdentifier();
		Integer eventPK = 0;
		Integer patProtPK = 0;
		Integer studyPK = 0;
		Integer personPK = 0;
		int execBy = 0;
		Integer eventSOSId = 0;
		Integer statusCodelstPK = 0;
		int eventStatusPK = 0;
		int oldStatus = 0;
		int eventCoverageType = 0;
		String notes = "";
		String reasonForCoverageChange = "";
		String mode = "N";
		Date execOn;
		
		try{
		
		GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}
		
		if(eventIdentifier != null && eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
		{
			eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
			if(eventPK == null || eventPK == 0)
			{ 
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
					"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
				throw new OperationException("Event not found for Event Identifier : OID " + eventIdentifier.getOID()); 
			}
			 			
		}
		else{
			addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid EventIdentifier is required to update the event status")); 
			throw new OperationException("Valid EventIdentifier is required to update the event status"); 
		}
		
		patProtPK = PatientScheduleDAO.getPatProtPK(eventPK);
		
		//Bug Fix : 12477
		if(patProtPK == null || patProtPK == 0)
		{ 
			addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_DOES_NOT_EXIST,				
				"Patient Schedule Does Not Exist")); 
			throw new OperationException("Patient Schedule Does Not Exist "); 
		}
		
		studyPK = PatientScheduleDAO.getStudyPK(patProtPK);
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
		int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
		if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view Study Team"));
			throw new AuthorizationException("User does not have view permission to view Study Team");
		}
		
		int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
		if (!TeamAuthModule.hasEditPermission(patientManageViewPrivileges)){
			addIssue(
					new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
							"User does not have view permission to edit the Patient data"));
			throw new AuthorizationException("User does not have the view permission to edit the Patient data");
		}
		
		int patientManageEditPrivileges = teamAuthModule.getPatientManagePrivileges();
		if (!TeamAuthModule.hasEditPermission(patientManageEditPrivileges)){
			addIssue(
					new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
							"User does not have edit permission to edit the Patient data"));
			throw new AuthorizationException("User does not have the edit permission to edit the Patient data");
		}
        personPK = PatientScheduleDAO.getPersonPK(patProtPK);
        
			if (personPK == null || personPK == 0){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
				throw new OperationException("Patient not found");
			}
			
			PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
			
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}
		//Bug Fix : 12416
		if(eventStatus.getStatusValidFrom() == null || StringUtil.isEmpty(eventStatus.getStatusValidFrom().toString())){
            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter the value for the Status Valid From field"));
            throw new AuthorizationException("Please enter the value for the Status Valid From field");
		}
		//Bug Fix : 12414
		if(eventStatus.getEventStatusCode() == null || StringUtil.isEmpty(eventStatus.getEventStatusCode().getCode())){
            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter a valid Event Status Code"));
            throw new AuthorizationException("Please enter a valid Event Status Code");
		}
				
	    execOn = eventStatus.getStatusValidFrom();
	    java.sql.Date protEndDate = DateUtil.dateToSqlDate(execOn);
		execBy = callingUser.getUserId();
		oldStatus = PatientScheduleDAO.getOldEventStatus(eventPK);
		try{//Bug Fix : 12414
			statusCodelstPK = dereferenceSchCode(eventStatus.getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser);
		}catch(OperationException e){
            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter a valid Event Status Code"));
            throw new AuthorizationException("Please enter a valid Event Status Code");
			
		}
		
		//Bug Fix : 12377	
		if((eventStatus.getCoverageType() == null) || (eventStatus.getSiteOfService() == null) || (eventStatus.getNotes() ==  null)){
			    EventdefBean eventDefBean = new EventdefBean();
				eventDefBean = PatientScheduleDAO.getOldEventDetails(eventPK);
				if(eventStatus.getCoverageType() == null){
					eventCoverageType = EJBUtil.stringToNum(eventDefBean.getEventCoverageType());
					if(eventStatus.getReasonForChangeCoverType() != null || !StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType())){
			             if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
			             addIssue(
			                     new Issue(IssueTypes.DATA_VALIDATION, 
			                             "Cannot enter new Reason For Coverage Change without changing the Coverage Type"));
			             throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
					}
					reasonForCoverageChange = eventDefBean.getReasonForCoverageChange();
				}
				//Bug Fix : 12415
				if(eventStatus.getSiteOfService() == null){
					eventSOSId = EJBUtil.stringToNum(eventDefBean.getEventSOSId());
				}
				if(eventStatus.getNotes() == null){
					notes = eventDefBean.getNotes();
				}
			}
		//Bug Fix : 12377	
		if((eventStatus.getCoverageType() != null) && !StringUtil.isEmpty(eventStatus.getCoverageType().getCode())){
			eventCoverageType = dereferenceSchCode(eventStatus.getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser);
			if(eventStatus.getReasonForChangeCoverType() == null || StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType())){
	             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
	             addIssue(
	                     new Issue(IssueTypes.DATA_VALIDATION, 
	                             "Please enter the Reason For The Coverage Change"));
	             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
			}
			reasonForCoverageChange = eventStatus.getReasonForChangeCoverType();
		}
		
		if((eventStatus.getSiteOfService() !=null) && (!StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteName())
				                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getOID())
				                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteAltId()))){	
				try {//BUg Fix : 12615
					eventSOSId = ObjectLocator.sitePKFromIdentifier(callingUser, eventStatus.getSiteOfService(), sessionContext, objectMapService);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}
			}
		//Bug Fix : 12614
		if (eventSOSId == null || eventSOSId == 0){
			addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
			throw new OperationException("Invalid site of service");
		}
		
		if(eventStatus.getNotes() != null && !StringUtil.isEmpty(eventStatus.getNotes())){
			notes = eventStatus.getNotes();
		}
		
		eventdefB.MarkDone(eventPK.intValue(), notes, protEndDate, execBy, statusCodelstPK, oldStatus, callingUser.getIpAdd(), mode, eventSOSId, eventCoverageType, reasonForCoverageChange);
        
		String eventPKString = eventPK.toString();
		int len_evt_id = eventPKString.length();
		int len_pad = 10 - len_evt_id ;
		String len_pad_str = "";

		for (int k=0; k < len_pad; k++ ){
			len_pad_str = len_pad_str + "0";
		}
		eventPKString = len_pad_str + eventPKString;
		
		eventStatB.setEventStatDate(DateUtil.dateToString(eventStatus.getStatusValidFrom()));
		eventStatB.setEvtStatNotes(eventStatus.getNotes());
		eventStatB.setFkEvtStat(eventPKString);
		eventStatB.setEvtStatus(statusCodelstPK.toString());
		eventStatB.setCreator(callingUser.getCreator());
		eventStatB.setIpAdd(callingUser.getIpAdd());
		eventStatB.setEventStatDetails();
		eventStatusPK  = eventResB.getStatusIdOfTheEvent(eventPK);
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_STATUS_TABLE, eventStatusPK);
		eventStatusIdentifier.setOID(map.getOID());
		response.addObjectCreatedAction(eventStatusIdentifier);
		}	catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
		
		return response;
	}
	//Tarandeep
	public SitesOfService getSitesOfService() throws OperationException{
		SitesOfService sitesOfService = new SitesOfService();
		int accountPK = 0;
		try{
			
			if (callingUser.getUserId() == null || callingUser.getUserId() == 0){
				
				addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"Valid calling user required for getting the Sites of Service"));
				throw new OperationException();
			}
			
			accountPK = EJBUtil.stringToNum(callingUser.getUserAccountId());
			sitesOfService = PatientScheduleDAO.getSitesOfService(accountPK);
			return sitesOfService;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
		
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
		
}