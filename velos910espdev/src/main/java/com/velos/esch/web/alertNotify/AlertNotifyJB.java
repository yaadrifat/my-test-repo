/*
 * Classname : AlertNotifyJB
 * 
 * Version information: 1.0 
 *
 * Date: 12/10/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.alertNotify;

/* IMPORT STATEMENTS */

import com.velos.esch.business.alertNotify.impl.AlertNotifyBean;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.service.alertNotifyAgent.AlertNotifyAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for Alert NOTIFY
 * 
 * @author Arvind
 * @version 1.0 12/10/2001
 * 
 */

public class AlertNotifyJB {

    private int alNotId;

    private String alertNotifyPatProtId;

    private String alertNotifyCodelstAnId;

    private String alertNotifyAlNotType;

    private String alertNotifyAlNotUsers;

    private String alertNotifyAlNotMobEve;

    private String alertNotifyAlNotFlag;

    private String creator;

    private String modifiedBy;

    private String ipAdd;

    private String alertNotifyStudy;

    private String alertNotifyProtocol;

    private String alertNotifyGlobalFlag;

    // GETTER SETTER METHODS

    public int getAlNotId() {
        return this.alNotId;
    }

    public void setAlNotId(int alNotId) {
        this.alNotId = alNotId;
    }

    public String getAlertNotifyPatProtId() {
        return this.alertNotifyPatProtId;
    }

    public void setAlertNotifyPatProtId(String alertNotifyPatProtId) {
        this.alertNotifyPatProtId = alertNotifyPatProtId;
    }

    public String getAlertNotifyCodelstAnId() {
        return this.alertNotifyCodelstAnId;
    }

    public void setAlertNotifyCodelstAnId(String alertNotifyCodelstAnId) {
        this.alertNotifyCodelstAnId = alertNotifyCodelstAnId;
    }

    public String getAlertNotifyAlNotType() {
        return this.alertNotifyAlNotType;
    }

    public void setAlertNotifyAlNotType(String alertNotifyAlNotType) {
        this.alertNotifyAlNotType = alertNotifyAlNotType;
    }

    public String getAlertNotifyAlNotUsers() {
        return this.alertNotifyAlNotUsers;
    }

    public void setAlertNotifyAlNotUsers(String alertNotifyAlNotUsers) {
        this.alertNotifyAlNotUsers = alertNotifyAlNotUsers;
    }

    public String getAlertNotifyAlNotMobEve() {
        return this.alertNotifyAlNotMobEve;
    }

    public void setAlertNotifyAlNotMobEve(String alertNotifyAlNotMobEve) {
        this.alertNotifyAlNotMobEve = alertNotifyAlNotMobEve;
    }

    public String getAlertNotifyAlNotFlag() {
        return this.alertNotifyAlNotFlag;
    }

    public void setAlertNotifyAlNotFlag(String alertNotifyAlNotFlag) {
        this.alertNotifyAlNotFlag = alertNotifyAlNotFlag;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getAlertNotifyProtocol() {
        return this.alertNotifyProtocol;
    }

    public void setAlertNotifyProtocol(String alertNotifyProtocol) {
        this.alertNotifyProtocol = alertNotifyProtocol;
    }

    public String getAlertNotifyStudy() {
        return this.alertNotifyStudy;
    }

    public void setAlertNotifyStudy(String alertNotifyStudy) {
        this.alertNotifyStudy = alertNotifyStudy;
    }

    public String getAlertNotifyGlobalFlag() {
        return this.alertNotifyGlobalFlag;
    }

    public void setAlertNotifyGlobalFlag(String alertNotifyGlobalFlag) {
        this.alertNotifyGlobalFlag = alertNotifyGlobalFlag;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Alert Notify Id
     * 
     * @param alNotId
     *            of the Alert
     */
    public AlertNotifyJB(int alNotId) {
        setAlNotId(alNotId);
    }

    /**
     * Default Constructor
     * 
     */
    public AlertNotifyJB() {
        Rlog.debug("alertNotify", "AlertNotifyJB.AlertNotifyJB() ");
    }

    public AlertNotifyJB(int alNotId, String alertNotifyPatProtId,
            String alertNotifyCodelstAnId, String alertNotifyAlNotType,
            String alertNotifyAlNotUsers, String alertNotifyAlNotMobEve,
            String alertNotifyAlNotFlag, String creator, String modifiedBy,
            String ipAdd, String alertNotifyStudy, String alertNotifyProtocol,
            String alertNotifyGlobalFlag) {

        setAlNotId(alNotId);
        setAlertNotifyPatProtId(alertNotifyPatProtId);
        setAlertNotifyCodelstAnId(alertNotifyCodelstAnId);
        setAlertNotifyAlNotType(alertNotifyAlNotType);
        setAlertNotifyAlNotUsers(alertNotifyAlNotUsers);
        setAlertNotifyAlNotMobEve(alertNotifyAlNotMobEve);
        setAlertNotifyAlNotFlag(alertNotifyAlNotFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAlertNotifyProtocol(alertNotifyProtocol);
        setAlertNotifyStudy(alertNotifyStudy);
        setAlertNotifyGlobalFlag(alertNotifyGlobalFlag);
        Rlog
                .debug("alertNotify",
                        "AlertNotifyJB.AlertNotifyJB(all parameters)");
    }

    /**
     * Calls getAlertDetails() of AlertNotify Session Bean: AlertNotifyAgentBean
     * 
     * 
     * @return The Details of the alert in the AlertNotify StateKeeper Object
     * @See AlertNotifyAgentBean
     */

    public AlertNotifyBean getAlertNotifyDetails() {
        AlertNotifyBean edsk = null;

        try {

            AlertNotifyAgentRObj alertNotifyAgent = EJBUtil
                    .getAlertNotifyAgentHome();
            edsk = alertNotifyAgent.getAlertNotifyDetails(this.alNotId);
            Rlog.debug("alertNotify",
                    "AlertNotifyJB.getAlertNotifyDetails() AlertNotifyStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("alertNotify",
                    "Error in getAlertNotifyDetails() in AlertNotifyJB " + e);
        }
        if (edsk != null) {
            this.alNotId = edsk.getAlNotId();
            this.alertNotifyPatProtId = edsk.getAlertNotifyPatProtId();
            this.alertNotifyCodelstAnId = edsk.getAlertNotifyCodelstAnId();
            this.alertNotifyAlNotType = edsk.getAlertNotifyAlNotType();
            this.alertNotifyAlNotUsers = edsk.getAlertNotifyAlNotUsers();
            this.alertNotifyAlNotMobEve = edsk.getAlertNotifyAlNotMobEve();
            this.alertNotifyAlNotFlag = edsk.getAlertNotifyAlNotFlag();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();

            this.alertNotifyStudy = edsk.getAlertNotifyProtocol();
            this.alertNotifyProtocol = edsk.getAlertNotifyStudy();
            this.alertNotifyGlobalFlag = edsk.getAlertNotifyGlobalFlag();

        }
        return edsk;
    }

    /**
     * Calls setAlertNotifyDetails() of AlertNotify Session Bean:
     * AlertNotifyAgentBean
     * 
     */

    public void setAlertNotifyDetails() {

        try {
            AlertNotifyAgentRObj alertNotifyAgent = EJBUtil
                    .getAlertNotifyAgentHome();
            this.setAlNotId(alertNotifyAgent.setAlertNotifyDetails(this
                    .createAlertNotifyStateKeeper()));
            Rlog.debug("alertNotify", "AlertNotifyJB.setAlertNotifyDetails()");
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "Error in setAlertNotifyDetails() in AlertNotifyJB " + e);
        }
    }

    /**
     * Calls updateAlertNotify() of AlertNotify Session Bean:
     * AlertNotifyAgentBean
     * 
     * @return
     */
    public int updateAlertNotify() {
        int output;
        try {
            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            output = alertNotifyAgentRObj.updateAlertNotify(this
                    .createAlertNotifyStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("alertNotify",
                            "EXCEPTION IN SETTING ALERT NOTIFY DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int updateAlertNotifyList(String[] anIds, String[] flags,
            String[] userIds, String[] mobIds, String lastModifiedBy,
            String ipAdd, String studyId, String protocolId) {
        int output;
        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            Rlog.fatal("alertNotify", "This is debug line #2");
            output = alertNotifyAgentRObj
                    .updateAlertNotifyList(anIds, flags, userIds, mobIds,
                            lastModifiedBy, ipAdd, studyId, protocolId);
            Rlog.fatal("alertNotify", "This is debug line #3");
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN SETTING ALERT NOTIFY DETAILS TO  SESSION BEAN"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public EventInfoDao getStudyAlertNotify(int studyId, int protocolId) {
        // String output = "N";
        EventInfoDao al = new EventInfoDao();

        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            al = alertNotifyAgentRObj.getStudyAlertNotify(studyId, protocolId);
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN getting study ALERT NOTIFY DETAILS ");
            e.printStackTrace();
            return al;
        }
        return al;
    }

    public String getANGlobalFlag(int studyId, int protocolId) {
        // String output = "N";
        String al = "";

        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            al = alertNotifyAgentRObj.getANGlobalFlag(studyId, protocolId);
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN getting study ALERT NOTIFY DETAILS ");
            e.printStackTrace();
            return al;
        }
        return al;
    }

    public int updateStudyAlertNotify(int studyId, int protocolId,
            int modifiedBy, String ipAdd, String globFlag, int lockValue) {
        int ret = 0;

        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            ret = alertNotifyAgentRObj.updateStudyAlertNotify(studyId,
                    protocolId, modifiedBy, ipAdd, globFlag, lockValue);
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN updateStudyAlertNotify AlertNotifyJB ");
            e.printStackTrace();
            return -1;
        }
        return ret;
    }

    public int notifyAdmin(int userId, String ipAdd, int velosUser,
            String failDate, String failTime)

    {
        int ret = 0;

        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            ret = alertNotifyAgentRObj.notifyAdmin(userId, ipAdd, velosUser,
                    failDate, failTime);
        } catch (Exception e) {
            Rlog
                    .fatal("alertNotify",
                            "EXCEPTION IN notifyAdmin AlertNotifyJB ");
            e.printStackTrace();
            return -1;
        }
        return ret;
    }

    public int setDefStudyAlnot(String studyid, String protocolid,
            String creator, String ipAdd) {
        int ret = 0;

        try {

            AlertNotifyAgentRObj alertNotifyAgentRObj = EJBUtil
                    .getAlertNotifyAgentHome();
            ret = alertNotifyAgentRObj.setDefStudyAlnot(studyid, protocolid,
                    creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN setDefStudyAlnot AlertNotifyJB ");
            e.printStackTrace();
            return -1;
        }
        return ret;
    }

    /**
     * 
     * 
     * @return the Alert Notify StateKeeper Object with the current values of
     *         the Bean
     */

    public AlertNotifyBean createAlertNotifyStateKeeper() {
        Rlog
                .debug("alertNotify",
                        "AlertNotifyJB.createAlertNotifyStateKeeper ");
        return new AlertNotifyBean(new Integer(alNotId), alertNotifyPatProtId,
                alertNotifyCodelstAnId, alertNotifyAlNotType,
                alertNotifyAlNotUsers, alertNotifyAlNotMobEve,
                alertNotifyAlNotFlag, creator, modifiedBy, ipAdd,
                alertNotifyGlobalFlag, alertNotifyStudy, alertNotifyProtocol);
    }

}// end of class
