/*
 * Classname : BudgetJB
 * 
 * Version information: 1.0 
 *
 * Date: 03/20/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.web.budget;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.persistence.Column;

import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetDao;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for Budget
 * 
 * @author Dinesh
 * @version 1.0 03/20/2002
 * 
 */

public class BudgetJB {

    /**
     * budgetId
     */
    private int budgetId;

    /**
     * budgetName
     */
    private String budgetName;

    /**
     * budgetVersion
     */
    private String budgetVersion;

    /**
     * budgetdesc
     */
    private String budgetdesc;

    /**
     * budgetCreator
     */
    private String budgetCreator;

    /**
     * budgetTemplate
     */
    private String budgetTemplate;

    /**
     * budgetStatus
     */
    private String budgetStatus;

    /**
     * budgetCurrency
     */
    private String budgetCurrency;

    /**
     * budgetSFlag
     */
    private String budgetSFlag;

    /**
     * budgetCFlag
     */
    private String budgetCFlag;

    /**
     * budgetStudyId
     */
    private String budgetStudyId;

    /**
     * budgetAccountId
     */
    private String budgetAccountId;

    /**
     * budgetSiteId
     */
    private String budgetSiteId;

    /**
     * budgetRScope
     */
    private String budgetRScope;

    /**
     * budgetRights
     */
    private String budgetRights;

    /**
     * creator
     */
    private String creator;

    /**
     * modifiedBy
     */
    private String modifiedBy;

    /**
     * ipAdd
     */
    private String ipAdd;

    /**
     * budgetDelFlag
     */
    private String budgetDelFlag;

    /**
     * budgetType
     */
    private String budgetType;

    /**
     * lastModifiedDate
     */
    private String lastModifiedDate;
    
    /**
     * Budget Codelist Status
     */
    private String budgetCodeListStatus;

    /**
     * Budget default calendar
     */
    
    private String budgetDefaultCalendar;

    /**
     * Budget default calendar
     */
    
    private String budgetCombinedFlag;

    // GETTER SETTER METHODS

    public int getBudgetId() {
        return this.budgetId;
    }

    public void setBudgetId(int budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return this.budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetVersion() {
        return this.budgetVersion;
    }

    public void setBudgetVersion(String budgetVersion) {
        this.budgetVersion = budgetVersion;
    }

    public String getBudgetdesc() {
        return this.budgetdesc;
    }

    public void setBudgetdesc(String budgetdesc) {
        this.budgetdesc = budgetdesc;
    }

    public String getBudgetCreator() {
        return this.budgetCreator;
    }

    public void setBudgetCreator(String budgetCreator) {
        this.budgetCreator = budgetCreator;
    }

    public String getBudgetTemplate() {
        return this.budgetTemplate;
    }

    public void setBudgetTemplate(String budgetTemplate) {
        this.budgetTemplate = budgetTemplate;
    }

    public String getBudgetStatus() {
        return this.budgetStatus;
    }

    public void setBudgetStatus(String budgetStatus) {
        this.budgetStatus = budgetStatus;
    }

    public String getBudgetCurrency() {
        return this.budgetCurrency;
    }

    public void setBudgetCurrency(String budgetCurrency) {
        this.budgetCurrency = budgetCurrency;
    }

    public String getBudgetSFlag() {
        return this.budgetSFlag;
    }

    public void setBudgetSFlag(String budgetSFlag) {
        this.budgetSFlag = budgetSFlag;
    }

    public String getBudgetCFlag() {
        return this.budgetCFlag;
    }

    public void setBudgetCFlag(String budgetCFlag) {
        this.budgetCFlag = budgetCFlag;
    }

    public String getBudgetStudyId() {
        return this.budgetStudyId;
    }

    public void setBudgetStudyId(String budgetStudyId) {
        this.budgetStudyId = budgetStudyId;
    }

    public String getBudgetAccountId() {
        return this.budgetAccountId;
    }

    public void setBudgetAccountId(String budgetAccountId) {
        this.budgetAccountId = budgetAccountId;
    }

    public String getBudgetSiteId() {
        return this.budgetSiteId;
    }

    public void setBudgetSiteId(String budgetSiteId) {
        this.budgetSiteId = budgetSiteId;
    }

    public String getBudgetRScope() {
        return this.budgetRScope;
    }

    public void setBudgetRScope(String budgetRScope) {
        this.budgetRScope = budgetRScope;
    }

    public String getBudgetRights() {
        return this.budgetRights;
    }

    public void setBudgetRights(String budgetRights) {
        this.budgetRights = budgetRights;
    }

    public String getBudgetDelFlag() {
        return this.budgetDelFlag;
    }

    public void setBudgetDelFlag(String budgetDelFlag) {
        this.budgetDelFlag = budgetDelFlag;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getBudgetType() {
        return this.budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Budget Id
     * 
     * @param budgetId
     *            Id of the Budget
     */
    public BudgetJB(int budgetId) {
        setBudgetId(budgetId);
    }

    /**
     * Default Constructor
     * 
     */
    public BudgetJB() {
        Rlog.debug("budget", "BudgetJB.BudgetJB() ");
    }

    public BudgetJB(int budgetId, String budgetName, String budgetVersion,
            String budgetdesc, String budgetCreator, String budgetTemplate,
            String budgetStatus, String budgetCurrency, String budgetSFlag,
            String budgetCFlag, String budgetStudyId, String budgetAccountId,
            String budgetSiteId, String budgetRScope, String budgetRights,
            String budgetType, String creator, String modifiedBy, String ipAdd,
            String budgetDelFlag, String lastModifiedDate,String cstat, String combBgtFlag) {

        setBudgetId(budgetId);
        setBudgetName(budgetName);
        setBudgetVersion(budgetVersion);
        setBudgetdesc(budgetdesc);
        setBudgetCreator(budgetCreator);
        setBudgetTemplate(budgetTemplate);
        setBudgetStatus(budgetStatus);
        setBudgetCurrency(budgetCurrency);
        setBudgetSFlag(budgetSFlag);
        setBudgetCFlag(budgetCFlag);
        setBudgetStudyId(budgetStudyId);
        setBudgetAccountId(budgetAccountId);
        setBudgetSiteId(budgetSiteId);
        setBudgetRScope(budgetRScope);
        setBudgetRights(budgetRights);
        setBudgetType(budgetType);
      //  setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBudgetDelFlag(budgetDelFlag);
      // Modified by Amarnadh for Bugzilla issue #3010 
      // setLastModifiedDate(lastModifiedDate);

        setBudgetCodeListStatus(cstat);
        setBudgetCombinedFlag(combBgtFlag);
        Rlog.debug("budget", "BudgetJB.BudgetJB(all parameters)");
    }

    /**
     * Calls getBudgetDetails() of Budget Session Bean: BudgetAgentBean
     * 
     * 
     * @return The Details of the budget in the Budget StateKeeper Object
     * @See BudgetAgentBean
     */

    public BudgetBean getBudgetDetails() {
        BudgetBean bdsk = null;

        try {
            BudgetAgentRObj budgetAgent = EJBUtil.getBudgetAgentHome();
            bdsk = budgetAgent.getBudgetDetails(this.budgetId);
            Rlog.debug("budget",
                    "BudgetJB.getBudgetDetails() BudgetStateKeeper " + bdsk);
        } catch (Exception e) {
            Rlog
                    .fatal("budget", "Error in getBudgetDetails() in BudgetJB "
                            + e);
        }
        if (bdsk != null) {
            this.budgetId = bdsk.getBudgetId();
            this.budgetName = bdsk.getBudgetName();
            this.budgetVersion = bdsk.getBudgetVersion();
            this.budgetdesc = bdsk.getBudgetdesc();
            this.budgetCreator = bdsk.getBudgetCreator();
            this.budgetTemplate = bdsk.getBudgetTemplate();
            this.budgetStatus = bdsk.getBudgetStatus();
            this.budgetCurrency = bdsk.getBudgetCurrency();
            this.budgetSFlag = bdsk.getBudgetSFlag();
            this.budgetCFlag = bdsk.getBudgetCFlag();
            this.budgetStudyId = bdsk.getBudgetStudyId();
            this.budgetAccountId = bdsk.getBudgetAccountId();
            this.budgetSiteId = bdsk.getBudgetSiteId();
            this.budgetRScope = bdsk.getBudgetRScope();
            this.budgetRights = bdsk.getBudgetRights();
            this.budgetType = bdsk.getBudgetType();
            this.creator = bdsk.getCreator();
            this.modifiedBy = bdsk.getModifiedBy();
            this.ipAdd = bdsk.getIpAdd();
            this.budgetDelFlag = bdsk.getBudgetDelFlag();
            setBudgetCodeListStatus(bdsk.getBudgetCodeListStatus());
            setBudgetDefaultCalendar(bdsk.getBudgetDefaultCalendar());
            setBudgetCombinedFlag(bdsk.getBudgetCombinedFlag());
            
           // this.lastModifiedDate = bdsk.getLastModifiedDate();
        }
        return bdsk;
    }

    /**
     * Calls setBudgetDetails() of Budget Session Bean: BudgetAgentBean
     * 
     */

    public void setBudgetDetails() {

        try {
            BudgetAgentRObj budgetAgent = EJBUtil.getBudgetAgentHome();
            this.setBudgetId(budgetAgent.setBudgetDetails(this
                    .createBudgetStateKeeper()));
            Rlog.debug("budget", "BudgetJB.setBudgetDetails()");
        } catch (Exception e) {
            Rlog
                    .fatal("budget", "Error in setBudgetDetails() in BudgetJB "
                            + e);
        }
    }

    /**
     * Calls updateBudget() of Budget Session Bean: BudgetAgentBean
     * 
     * @return
     */
    public int updateBudget() {
        int output;
        try {
            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            output = budgetAgentRObj.updateBudget(this
                    .createBudgetStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    // Overloaded for INF-18183 ::: Akshi
    public int updateBudget(Hashtable<String, String> args) {
        int output;
        try {
            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            output = budgetAgentRObj.updateBudget(this
                    .createBudgetStateKeeper(),args);
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    /**
     * 
     * 
     * @return the Budget StateKeeper Object with the current values of the Bean
     */

    public BudgetBean createBudgetStateKeeper() {

        return new BudgetBean(budgetId, budgetName, budgetVersion, budgetdesc,
                budgetCreator, budgetTemplate, budgetStatus, budgetCurrency,
                budgetSFlag, budgetCFlag, budgetStudyId, budgetAccountId,
                budgetSiteId, budgetRScope, budgetRights, creator, modifiedBy,
                ipAdd, budgetDelFlag, lastModifiedDate, budgetType,budgetCodeListStatus,budgetCombinedFlag);
    }

    public BudgetDao getBudgetInfo(int budgetId) {
        BudgetDao budgetDao = new BudgetDao();
        try {
            Rlog.debug("budget", "BudgetJB.getBudgetInfo starting");
            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();

            budgetDao = budgetAgentRObj.getBudgetInfo(budgetId);

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getBudgetInfo() in BudgetJB " + e);
        }
        return budgetDao;
    }

    public BudgetDao getBudgetsForUser(int userId) {
        BudgetDao budgetDao = new BudgetDao();
        try {

            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            budgetDao = budgetAgentRObj.getBudgetsForUser(userId);
        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getBudgetsForUser() in BudgetJB "
                    + e);
        }
        return budgetDao;
    }

    public BudgetDao getCombBudgetForStudy(int userId, int studyId) {
    	BudgetDao budgetDao = new BudgetDao();
        try {

            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            budgetDao = budgetAgentRObj.getCombBudgetForStudy(userId, studyId);
        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getCombBudgetForStudy() in BudgetJB "
                    + e);
        }
        return budgetDao;
    }
    
    public int copyBudget(int oldBudget, String newBudName, String newBudVer,
            int creator, String ipAdd) {
        int retVal = -1;
        try {

            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            retVal = budgetAgentRObj.copyBudget(oldBudget, newBudName,
                    newBudVer, creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("budget", "Error in copyBudget() in BudgetJB " + e);
            return -1;
        }
        return retVal;
    }

    /*
     * generates a String of XML for the budget details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("budget", "BudgetJB.toXML()");
        return "test";
        // to be implemented later
   

    }// end of method

    /**
     * Calls copyBudgetForTemplate() of Budget Session Bean: budgetAgentRObj
     * 
     * 
     * @return 0 if successful
     * @See budgetAgentRObj
     */

    public int copyBudgetForTemplate(int oldBudget, int budgetId, int creator,
            String ipAdd) {
        Rlog.debug("budget", "inside JB copyBudgetForTemplate");

        int retVal = -1;
        try {
            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            retVal = budgetAgentRObj.copyBudgetForTemplate(oldBudget, budgetId,
                    creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in copyBudgetForTemplate() in BudgetJB " + e);
            return -1;
        }
        return retVal;
    }

    public BudgetDao getBgtTemplateSubType(int bgtTemplate, int budgetId) {
        BudgetDao budgetDao = new BudgetDao();
        try {

            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            budgetDao = budgetAgentRObj.getBgtTemplateSubType(bgtTemplate,
                    budgetId);
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in getBgtTemplateSubType() in BudgetJB " + e);
        }
        return budgetDao;
    }
    
    

    /**
     * gets budget's status. refers to sch_codelst pk 
     * */
	public String getBudgetCodeListStatus() {
		return budgetCodeListStatus;
	}

	public void setBudgetCodeListStatus(String budgetCodeListStatus) {
		this.budgetCodeListStatus = budgetCodeListStatus;
	}

	
	 /**
     * Get Default Budget PK For a Calendar
     * 
     * @param PKProtocol Protocol ID
     */

    public int getDefaultBudgetForCalendar(int PKProtocol) {
    	
    	int BudgetPk = 0;
        try {

            BudgetAgentRObj budgetAgentRObj = EJBUtil.getBudgetAgentHome();
            BudgetPk = budgetAgentRObj.getDefaultBudgetForCalendar(PKProtocol);
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Exception in getDefaultBudgetForCalendar(int PKProtocol) in BudgetJB " + e);
        }
        return BudgetPk;
    }
    
	public String getBudgetDefaultCalendar() {
		return budgetDefaultCalendar;
	}

	public void setBudgetDefaultCalendar(String cal) {
		budgetDefaultCalendar = cal;
	}

	 /**
     * gets budget's combined budget flag
     * */
	public String getBudgetCombinedFlag() {
		return budgetCombinedFlag;
	}

	public void setBudgetCombinedFlag(String flag) {
		budgetCombinedFlag = flag;
	}
}// end of class
