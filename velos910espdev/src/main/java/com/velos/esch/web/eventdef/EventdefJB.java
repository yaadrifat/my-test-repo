/*
 * Classname : EventdefJB
 *
 * Version information: 1.0
 *
 * Date: 05/14/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana /Vikas Chawla
 */

package com.velos.esch.web.eventdef;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for Eventdef
 *
 * @author Dinesh
 * @version 1.0 05/14/2001
 *
 */

public class EventdefJB {

    /**
     * event id
     */
    private int event_id;

    private int org_id;

    /**
     * chain id
     */
    private String chain_id;

    /**
     * event type
     */
    private String event_type;

    /**
     * event name
     */

    private String name;

    /**
     * event notes
     */
    private String notes;

    /**
     * cost of the event
     */
    private String cost;

    /**
     * cost description
     */
    private String cost_description;

    /**
     * event duration
     */
    private String duration;

    /**
     * user id
     */

    private String user_id;

    /**
     * linked url
     */

    private String linked_uri;

    /**
     * fuzzy_period
     */
    private String fuzzy_period;

    /**
     * msg_to
     */
    private String msg_to;

    /**
     * description
     */

    private String description;

    /**
     * displacement
     */
    private String displacement;

    /**
     * event_msg
     */

    private String event_msg;

    /**
     * patDaysBefore
     */

    private String patDaysBefore;

    /**
     * patDaysAfter
     */

    private String patDaysAfter;

    /**
     * usrDaysBefore
     */

    private String usrDaysBefore;

    /**
     * usrDaysAfter
     */

    private String usrDaysAfter;

    /**
     * patMsgBefore
     */

    private String patMsgBefore;

    /**
     * patMsgAfter
     */

    private String patMsgAfter;

    /**
     * usrMsgBefore
     */

    private String usrMsgBefore;

    /**
     * usrMsgAfter
     */

    private String usrMsgAfter;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Calendar SharedWith
     */
    private String sharedWith = "";

    /*
     * Duration Unit
     */
    private String durationUnit = "D"; // duration type day/week/month

    /*
     * Event Visit
     */
    private String eventVisit; // event's foreign key to visit

    private String cptCode;

    private String durationUnitAfter;

    private String fuzzyAfter;

    private String durationUnitBefore;

    private String eventCalType;

    private String eventCategory;//KM

    private String sequence;

    private String eventLibType;

    private String eventLineCategory;

    /* Event Site of Service */
    private String eventSOSId;

    /* Event Facility */
    private String eventFacilityId;

    /* Event Coverage Type */
    private String eventCoverageType;


    /* Reason for change in coverage type */

    private String reasonForCoverageChange;

    /*Coverage Notes*/
    private String coverageNotes;
    
    //JM: 
    private String statCode;

    // GETTER SETTER METHODS

    public int getEvent_id() {
        return this.event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getOrg_id() {
        return this.org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public String getChain_id() {
        return this.chain_id;
    }

    public void setChain_id(String chain_id) {
        this.chain_id = chain_id;
    }

    public String getEvent_type() {
        return this.event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCost() {
        return this.cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCost_description() {
        return this.cost_description;
    }

    public void setCost_description(String cost_description) {
        this.cost_description = cost_description;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUser_id() {
        return this.user_id;
    }

    // SV, 9/7/04, cal-enh-01
    public String getCalSharedWith() {
        return this.sharedWith;
    }

    public void setCalSharedWith(String calSharedWith) {
        this.sharedWith = calSharedWith;
    }

    // SV, 9/7/04, end cal-enh-01

    // SV, 9/14/04, cal-enh-02
    public String getDurationUnit() {
        return this.durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    // SV, 9/14/04, cal-enh-02

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLinked_uri() {
        return this.linked_uri;
    }

    public void setLinked_uri(String linked_uri) {
        this.linked_uri = linked_uri;
    }

    public String getFuzzy_period() {
        return this.fuzzy_period;
    }

    public void setFuzzy_period(String fuzzy_period) {
        this.fuzzy_period = fuzzy_period;
    }

    public String getMsg_to() {
        return this.msg_to;
    }

    public void setMsg_to(String msg_to) {
        this.msg_to = msg_to;
    }    

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplacement() {
        return this.displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public String getEvent_msg() {
        return this.event_msg;
    }

    public void setEvent_msg(String event_msg) {
        this.event_msg = event_msg;
    }

    public String getMsg_details() {
        return getLinked_uri();
    }

    public void setMsg_details(String msg_details) {
        setLinked_uri(msg_details);
    }

    public String getPatDaysBefore() {
        return this.patDaysBefore;
    }

    public void setPatDaysBefore(String patDaysBefore) {
        this.patDaysBefore = patDaysBefore;
    }

    public String getPatDaysAfter() {
        return this.patDaysAfter;
    }

    public void setPatDaysAfter(String patDaysAfter) {
        this.patDaysAfter = patDaysAfter;
    }

    public String getUsrDaysBefore() {
        return this.usrDaysBefore;
    }

    public void setUsrDaysBefore(String usrDaysBefore) {
        this.usrDaysBefore = usrDaysBefore;
    }

    public String getUsrDaysAfter() {
        return this.usrDaysAfter;
    }

    public void setUsrDaysAfter(String usrDaysAfter) {
        this.usrDaysAfter = usrDaysAfter;
    }

    public String getPatMsgBefore() {
        return this.patMsgBefore;
    }

    public void setPatMsgBefore(String patMsgBefore) {
        this.patMsgBefore = patMsgBefore;
    }

    public String getPatMsgAfter() {
        return this.patMsgAfter;
    }

    public void setPatMsgAfter(String patMsgAfter) {
        this.patMsgAfter = patMsgAfter;
    }

    public String getUsrMsgBefore() {
        return this.usrMsgBefore;
    }

    public void setUsrMsgBefore(String usrMsgBefore) {
        this.usrMsgBefore = usrMsgBefore;
    }

    public String getUsrMsgAfter() {
        return this.usrMsgAfter;
    }

    public void setUsrMsgAfter(String usrMsgAfter) {
        this.usrMsgAfter = usrMsgAfter;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // SV, 9/29/04, cal-enh-03
    public String getEventVisit() {
        return this.eventVisit;
    }

    public void setEventVisit(String eventVisit) {
        this.eventVisit = eventVisit;
    }

    // END, SV, 9/29/04, cal-enh-03

    public String getCptCode() {

        return cptCode;
    }

    public void setCptCode(String cptCode) {
        this.cptCode = cptCode;
    }

    public String getDurationUnitAfter() {
        return durationUnitAfter;
    }

    public void setDurationUnitAfter(String durationUnitAfter) {
        this.durationUnitAfter = durationUnitAfter;
    }

    public String getFuzzyAfter() {
        return fuzzyAfter;
    }

    public void setFuzzyAfter(String fuzzyAfter) {
        this.fuzzyAfter = fuzzyAfter;
    }

        public String getDurationUnitBefore() {
        return durationUnitBefore;
    }

    public void setDurationUnitBefore(String durationUnit) {
        this.durationUnitBefore = durationUnit;
    }

    public String getEventCalType() {
        return eventCalType;
    }

    public void setEventCalType(String eventCalType) {
        this.eventCalType = eventCalType;
    }

    //KM
    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getEventSequence() {
        return sequence;
    }

    public void setEventSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getEventLibType() {
        return eventLibType;
    }

    public void setEventLibType(String eventLibType) {
        this.eventLibType = eventLibType;
    }

    public String getEventLineCategory() {
        return eventLineCategory;
    }

    public void setEventLineCategory(String eventLineCategory) {
        this.eventLineCategory = eventLineCategory;
    }

    public String getEventSOSId() {
        return eventSOSId;
    }

    public void setEventSOSId(String eventSOSId) {
        this.eventSOSId = eventSOSId;
    }

    public String getEventFacilityId() {
        return eventFacilityId;
    }

    public void setEventFacilityId(String eventFacilityId) {
        this.eventFacilityId = eventFacilityId;
    }

    public String getEventCoverageType() {
        return eventCoverageType;
    }

    public void setEventCoverageType(String eventCoverageType) {
        this.eventCoverageType = eventCoverageType;
    }


    public String getReasonForCoverageChange() {
        return reasonForCoverageChange;
    }

    public void setReasonForCoverageChange(String reasonForCoverageChange) {
        this.reasonForCoverageChange = reasonForCoverageChange;
    }

  //BK 23rd July 2010
    public String getCoverageNotes() {
		return this.coverageNotes;
	}

	public void setCoverageNotes(String coverageNotes) {
		this.coverageNotes = coverageNotes;
	}
	

	//JM: 25JAN2011: #Enh-D-FIN9
	
	public String getStatCode() {
        return statCode;
    }
    
    public void setStatCode(String statCode) {
        this.statCode = statCode;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Eventdef Id
     *
     * @param eventdefId
     *            Id of the Eventdef
     */
    public EventdefJB(int event_id) {
        setEvent_id(event_id);
    }

    /**
     * Default Constructor
     *
     */
    public EventdefJB() {
        Rlog.debug("eventdef", "EventdefJB.EventdefJB() ");
    }

    public EventdefJB(int event_id, String chain_id, String event_type,
        String name, String notes, String cost, String cost_description,
        String duration, String user_id, String linked_uri,
        String fuzzy_period, String msg_to, 
        String description, String displacement, int org_id,
        String event_msg, String patDaysBefore, String patDaysAfter,
        String usrDaysBefore, String usrDaysAfter, String patMsgBefore,
        String patMsgAfter, String usrMsgBefore, String usrMsgAfter,
        String creator, String modifiedBy, String ipAdd,String cptCode,
        String durationUnitAfter,String fuzzyAfter,String eventCalType,String eventCategory, String sequence,
        String eventLibType, String eventLineCategory,
        String eventSOSId, String eventFacilityId, String eventCoverageType, String reasonForCoverageChange,
        String CoverageNotes,String statCode) {

        setEvent_id(event_id);
        setChain_id(chain_id);
        setEvent_type(event_type);
        setName(name);
        setNotes(notes);
        setCost(cost);
        setCost_description(cost_description);
        setDuration(duration);
        setUser_id(user_id);
        setLinked_uri(linked_uri);
        setFuzzy_period(fuzzy_period);
        setMsg_to(msg_to);        
        setDescription(description);
        setDisplacement(displacement);
        setOrg_id(org_id);
        setEvent_msg(event_msg);
        setPatDaysBefore(patDaysBefore);
        setPatDaysAfter(patDaysAfter);
        setUsrDaysBefore(usrDaysBefore);
        setUsrDaysAfter(usrDaysAfter);
        setPatMsgBefore(patMsgBefore);
        setPatMsgAfter(patMsgAfter);
        setUsrMsgBefore(usrMsgBefore);
        setUsrMsgAfter(usrMsgAfter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setCptCode(cptCode);
        setDurationUnitAfter(durationUnitAfter);
        setDurationUnitBefore(durationUnitBefore);
        setFuzzyAfter(fuzzyAfter);
        setEventCalType(eventCalType);
        setEventCategory(eventCategory);//KM

        setEventSequence(sequence);
        setEventLibType(eventLibType);
        setEventLineCategory(eventLineCategory);

        setEventSOSId(eventSOSId);
        setEventFacilityId(eventFacilityId);
        setEventCoverageType(eventCoverageType);
        setReasonForCoverageChange(reasonForCoverageChange);
        this.setCoverageNotes(coverageNotes);
        setStatCode(statCode);


        Rlog.debug("eventdef", "EventdefJB.EventdefJB(all parameters)");
    }

    /**
     * Calls getEventdefDetails() of Eventdef Session Bean: EventdefAgentBean
     *
     *
     * @return The Details of the eventdef in the Eventdef StateKeeper Object
     * @See EventdefAgentBean
     */

    public EventdefBean getEventdefDetails() {
        EventdefBean edsk = null;

        try {

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            edsk = eventdefAgent.getEventdefDetails(this.event_id);
            Rlog.debug("eventdef",
                    "EventdefJB.getEventdefDetails() EventdefStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "Error in getEventdefDetails() in EventdefJB " + e);
        }
        if (edsk != null) {
            this.event_id = edsk.getEvent_id();
            this.chain_id = edsk.getChain_id();
            this.event_type = edsk.getEvent_type();
            this.name = edsk.getName();
            this.notes = edsk.getNotes();
            this.cost = edsk.getCost();
            this.cost_description = edsk.getCost_description();
            this.duration = edsk.getDuration();
            this.user_id = edsk.getUser_id();
            this.linked_uri = edsk.getLinked_uri();
            this.fuzzy_period = edsk.getFuzzy_period();
            this.msg_to = edsk.getMsg_to();            
            this.description = edsk.getDescription();
            this.displacement = edsk.getDisplacement();
            this.org_id = edsk.getOrg_id();
            this.event_msg = edsk.getEvent_msg();
            this.patDaysBefore = edsk.getPatDaysBefore();
            this.patDaysAfter = edsk.getPatDaysAfter();
            this.usrDaysBefore = edsk.getUsrDaysBefore();
            this.usrDaysAfter = edsk.getUsrDaysAfter();
            this.patMsgBefore = edsk.getPatMsgBefore();
            this.patMsgAfter = edsk.getPatMsgAfter();
            this.usrMsgBefore = edsk.getUsrMsgBefore();
            this.usrMsgAfter = edsk.getUsrMsgAfter();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
            this.sharedWith = edsk.getCalSharedWith();
            this.durationUnit = edsk.getDurationUnit(); // SV, 9/14/04, handling
            // duration type.
            this.eventVisit = edsk.getEventVisit(); // SV, 9/29/04, handling
            // event' visit.
            this.cptCode=edsk.getCptCode();
            this.durationUnitAfter=edsk.getDurationUnitAfter();
            this.durationUnitBefore=edsk.getDurationUnitBefore();
            this.fuzzyAfter=edsk.getFuzzyAfter();
            this.eventCalType=EJBUtil.integerToString(edsk.getEventCalType());
            this.eventCategory = edsk.getEventCategory();//KM
            this.sequence = edsk.getEventSequence();
            this.eventLibType = edsk.getEventLibType();
            this.eventLineCategory = edsk.getEventLineCategory();

            this.eventSOSId = edsk.getEventSOSId();
            this.eventFacilityId = edsk.getEventFacilityId();
            this.eventCoverageType = edsk.getEventCoverageType();
            this.reasonForCoverageChange = edsk.getReasonForCoverageChange();
            this.coverageNotes = edsk.getCoverageNotes();
            this.statCode= edsk.getStatCode();
        }
        return edsk;
    }

    /**
     * Calls setEventdefDetails() of Eventdef Session Bean: EventdefAgentBean
     *
     */

    public int setEventdefDetails() {
        int output;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            // SV, 10/27/04
            // this.setEvent_id(eventdefAgent.setEventAssocDetails(this.createEventdefStateKeeper()));
            output = eventdefAgent.setEventdefDetails(this
                    .createEventdefStateKeeper());
            this.setEvent_id(output); // event_id was always set with output.
            // just broke the above statement in two
            // to return output.
            Rlog.debug("eventdef", "EventdefJB.setEventdefDetails()");
            return output;

        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "Error in setEventdefDetails() in EventdefJB " + e);
            return -2;
        }
    }

    /**
     * Calls updateEventdef() of Eventdef Session Bean: EventdefAgentBean
     *
     * @return
     */
    public int updateEventdef() {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.updateEventdef(this
                    .createEventdefStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls propagateEventdef() of eventdefDao.
     *
     * @return
     */
    public int propagateEventdef(String propagateInVisitFlag,
            String propagateInEventFlag) {
        int output = 0;
        EventdefBean edsk;

        try {
            EventdefDao eventdefdao = new EventdefDao();

            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            edsk = this.createEventdefStateKeeper();
            // SV, 10/20/04, mode = 'U' as we are updating event record with
            // messages, 'L' for library calendar.
            output = eventdefdao.propagateEventUpdates(EJBUtil.stringToNum(edsk
                    .getChain_id()), edsk.getEvent_id(), "EVENT_DEF", 0,
                    propagateInVisitFlag, propagateInEventFlag, "M", "P");
        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls propagateEventdef() of eventdefDao.
     *
     * @return
     */
    public int propagateEventmessage(String propagateInVisitFlag,
            String propagateInEventFlag) {
        int output = 0;
        EventdefBean edsk;

        try {
            EventdefDao eventdefdao = new EventdefDao();

            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            edsk = this.createEventdefStateKeeper();
            // SV, 10/20/04, mode = 'U' as we are updating event record with
            // messages, 'L' for library calendar.
            output = eventdefdao.propagateEventUpdates(EJBUtil.stringToNum(edsk
                    .getChain_id()), edsk.getEvent_id(), "EVENT_MESSAGE", 0,
                    propagateInVisitFlag, propagateInEventFlag, "M", "P");
        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls Delete Eventdef() of Eventdef Session Bean: EventdefJB
     *
     * @return
     */
    public int removeEventdefChild(int event_id) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefChild(event_id);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeEventdefChilds(int event_ids[]) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefChilds(event_ids);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Ankitk
    public int removeEventdefChild(int event_id, Hashtable<String, String> usrDetail) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefChild(event_id, usrDetail);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls Delete Eventdef() of Eventdef Session Bean: EventdefJB
     *
     * @return
     */

    public int removeEventdefHeader(int chain_id) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefHeader(chain_id);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Ankitk
    public int removeEventdefHeader(int chain_id, Hashtable<String, String> usrDetail) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefHeader(chain_id, usrDetail);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeEventdefHeaders(int chain_ids[]) {
        int output;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            output = eventdefAgentRObj.removeEventdefHeaders(chain_ids);
        } catch (Exception e) {
            Rlog.debug("eventdef", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public EventInfoDao getEventDefInfo(int eventId) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getByUserId starting");
            Rlog.debug("eventdef", "EventdefJB.getByUserId home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getByUserId after remote");
            eventinfoDao = eventdefAgentRObj.getEventDefInfo(eventId);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getEventDefInfo(int eventId) in EventdefJB " + e);
        }
        return eventinfoDao;
    }

    public EventdefDao getUserProtAndEvents(int user_id) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getByUserId starting");
            Rlog.debug("eventdef", "EventdefJB.getByUserId home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getByUserId after remote");
            eventdefDao = eventdefAgentRObj.getUserProtAndEvents(user_id);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getByUserId(int userId) in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getAllLibAndEvents(int user_id) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getByUserId starting");

            Rlog.debug("eventdef", "EventdefJB.getByUserId home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getByUserId after remote");
            eventdefDao = eventdefAgentRObj.getAllLibAndEvents(user_id);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getByUserId(int userId) in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getEventStatusHistory(String eventId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getEventStatusHistory starting");
            Rlog.debug("eventdef", "EventdefJB.getEventStatusHistory home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getEventStatusHistory after remote");
            eventdefDao = eventdefAgentRObj.getEventStatusHistory(eventId);
            Rlog
                    .debug("eventdef",
                            "EventdefJB.getEventStatusHistory after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getEventStatusHistory(String eventId) in EventdefJB "
                            + e);
        }
        return eventdefDao;
    }

    public EventdefDao getFilteredEvents(int user_id, int protocol_id) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getByUserId starting");
            Rlog.debug("eventdef", "EventdefJB.getByUserId home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getByUserId after remote");
            eventdefDao = eventdefAgentRObj.getFilteredEvents(user_id,
                    protocol_id);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getByUserId(int userId) in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog
                    .debug("eventdef",
                            "EventdefJB.getFilteredRangeEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getFilteredRangeEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getFilteredRangeEvents after remote");
            eventdefDao = eventdefAgentRObj.getFilteredRangeEvents(protocolId,
                    beginDisp, endDisp);
            Rlog.debug("eventdef",
                    "EventdefJB.getFilteredRangeEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getFilteredRangeEvents in EventdefJB " + e);
        }
        return eventdefDao;
    }

    //KM
    public EventdefDao getHeaderList(int user_id, String type, int evtLibType) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getHeaderList starting");
            Rlog.debug("eventdef", "EventdefJB.getHeaderList home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after remote");
            eventdefDao = eventdefAgentRObj.getHeaderList(user_id, type, evtLibType);
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getHeaderList in EventdefJB " + e);
        }
        return eventdefDao;
    }
    //Added by IA multiple organizations bug 2613
    public EventdefDao getHeaderList(int account_id, int user_id, String type, String orgId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getHeaderList starting");
            Rlog.debug("eventdef", "EventdefJB.getHeaderList home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after remote");
            eventdefDao = eventdefAgentRObj.getHeaderList(account_id, user_id,
                    type, orgId);
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getHeaderList in EventdefJB " + e);
        }
        return eventdefDao;
    }

    //end added

    public EventdefDao getHeaderList(int account_id, int user_id, String type) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getHeaderList starting");
            Rlog.debug("eventdef", "EventdefJB.getHeaderList home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after remote");
            eventdefDao = eventdefAgentRObj.getHeaderList(account_id, user_id,
                    type);
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getHeaderList in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getHeaderList(int account_id, int user_id, String type,
            int budgetId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getHeaderList starting");

            Rlog.debug("eventdef", "EventdefJB.getHeaderList home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after remote");
            eventdefDao = eventdefAgentRObj.getHeaderList(account_id, user_id,
                    type, budgetId);
            Rlog.debug("eventdef", "EventdefJB.getHeaderList after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getHeaderList in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getAllProtAndEvents(int event_id) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getAllProtAndEvents after remote");
            eventdefDao = eventdefAgentRObj.getAllProtAndEvents(event_id);
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAllProtAndEvents in EventdefJB " + e);
        }
        return eventdefDao;
    }

    //Modified by Manimaran to fix the issue #3394
    public EventdefDao getProtSelectedEvents(int protocolId, String orderType, String orderBy) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getAllProtAndEvents after remote");
            eventdefDao = eventdefAgentRObj.getProtSelectedEvents(protocolId,orderType,orderBy);
            Rlog.debug("eventdef", "EventdefJB.getAllProtAndEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAllProtAndEvents in EventdefJB " + e);
        }
        return eventdefDao;
    }


    public EventdefDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight) {
        EventdefDao eventdefDao = null;
        try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgentRObj
                    .getProtSelectedAndGroupedEvents(protocolId, isExport, finDetRight);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "In EventdefJB.getProtSelectedAndGroupedEvents: "+e);
            eventdefDao = new EventdefDao();
        }
        return eventdefDao;
    }

    public EventdefDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getProtRowEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getProtRowEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getProtRowEvents after remote");
            eventdefDao = eventdefAgentRObj.getProtRowEvents(protocolId,
                    rownum, beginDisp, endDisp);
            Rlog.debug("eventdef", "EventdefJB.getProtRowEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getProtRowEvents in EventdefJB "
                    + e);
        }
        return eventdefDao;
    }

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd) {
        try {
            EventdefDao eventdefdao = new EventdefDao();

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefAgent.AddEventsToProtocol(chain_id, events_disps, fromDisp,
                    toDisp, userId, ipAdd);
            eventdefdao.UpdateEventVisitIds(chain_id, userId, ipAdd);

        } catch (Exception e) {
            System.out.println("inside EventdefJB::AddEventsToProtocol");
        }

    }

    public int CopyProtocol(String protocol_id, String name, String calType, String userId,
            String ipAdd) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.CopyProtocol(protocol_id, name, calType,  userId, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("event_def", "inside EventdefJB::CopyProtocol");
            return -1;
        }
        return output;

    }

    //KM-02Sep09
    public int CopyEventToCat(int event_id, int whichTable, int category_id, int evtLibType,
            String name, String userId, String ipAdd) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.CopyEventToCat(event_id, whichTable,
                    category_id, evtLibType, name, userId, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in CopyEventToCat(event_id, whichTable)  in EventdefJB "
                            + e);
            return -1;
        }
        return output;

    }

    public int createEventsDef(String[] eIdList, String[] vIdList, String[] seqList, 
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd) {
    	int output = -1;
    	try {
            EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
            output = eventdefAgentRObj.createEventsDef(eIdList, vIdList, seqList,
            		dispList, hideList,
            		userId, ipAdd);
    	} catch (Exception e) {
            Rlog.fatal("eventdef", "inside EventdefJB::createEventsDef");
        }
    	return output;
    }

    //Modified by Manimaran for Enh#c7
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable) {
        int output = 0;
        try {

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.RemoveProtEventInstance(deleteIds, whichTable);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in RemoveProtEventInstance(event_id, whichTable)  in EventdefJB "
                            + e);
            return -1;
        }
        return output;

    }
    
    // Overloaded for INF-18183 ::: Ankit
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable, Hashtable<String, String> args) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.RemoveProtEventInstance(deleteIds, whichTable, args);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in RemoveProtEventInstance(event_id, whichTable)  in EventdefJB "
                            + e);
            return -1;
        }
        return output;

    }

    public int fetchEventIdByVisitAndDisplacement(String anEventId, String whichTable,
            String fkVisit, String displacement) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.fetchEventIdByVisitAndDisplacement(anEventId, whichTable,
                    fkVisit, displacement);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in fetchEventIdByVisitAndDisplacement in EventdefJB " + e);
            output = -1;
        }
        return output;
    }

    public int fetchEventIdByVisit(String anEventId, String whichTable, String fkVisit) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.fetchEventIdByVisit(anEventId, whichTable, fkVisit);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in fetchEventIdByVisit in EventdefJB " + e);
            output = -1;
        }
        return output;
    }

    //JM: 31Mar08: #C8.5
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user) {

    	int output = 0;
        try {

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            //KM-#5949
            output = eventdefAgent.deleteEvtOrVisits(deleteIds, whichTable, flags, user );
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in deleteEvtOrVisits(event_id, whichTable)  in EventdefJB "
                            + e);
            return -1;
        }
        return output;


    }
    
    // Overloaded for INF-18183 ::: Ankit
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user,Hashtable<String, String> auditInfo) {

    	int output = 0;
        try {

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            //KM-#5949
            output = eventdefAgent.deleteEvtOrVisits(deleteIds, whichTable, flags, user, auditInfo);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in deleteEvtOrVisits(event_id, whichTable)  in EventdefJB "
                            + e);
            return -1;
        }
        return output;


    }
/////////////
    public int GenerateSchedule(int protocol_id, int patient_id,
            java.sql.Date st_date, int patProtId, Integer days, String modifiedBy,
            String ipAdd) {
        int output = 0;
        try {

            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.GenerateSchedule(protocol_id, patient_id,
                    st_date, patProtId, days, modifiedBy, ipAdd);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::DeactivateEvents");
            return -1;
        }
        return output;

    }

    public int DeactivateEvents(int protocol_id, int patient_id,
            java.sql.Date st_date,String type) {
        int output;
        try {
            Rlog.debug("eventdef", "EventdefJB.DeactivateEvents line1");
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.DeactivateEvents line3");
            output = eventdefAgent.DeactivateEvents(protocol_id, patient_id,
                    st_date,type);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::DeactivateEvents");
            return -1;
        }
        return output;

    }

    public int MarkDone(int event_id, String notes, java.sql.Date exe_date,
            int exe_by, int status, int oldStatus, String ipAdd, String mode,
            int eventSOSId, int eventCoverType, String reasonForCoverageChange) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.MarkDone(event_id, notes, exe_date, exe_by,
                    status, oldStatus, ipAdd, mode, eventSOSId, eventCoverType, reasonForCoverageChange);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::MarkDone");
            return -1;
        }
        return output;

    }

    public int DeleteProtocolEvents(String chain_id) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.DeleteProtocolEvents(chain_id);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::DeleteProtocolEvents");
            return -1;
        }
        return output;

    }

    // SV, 8/19/04,
    public int DeleteProtocolEventsPastDuration(String chain_id, int duration) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.DeleteProtocolEventsPastDuration(chain_id,
                    duration);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::DeleteProtocolEventsPastDuration");
            return -1;
        }
        return output;

    }

    public EventdefDao FetchSchedule(int protocol_id, int patient_id,
            String pro_start_date) {
        EventdefDao eventdefDao = new EventdefDao();

        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.FetchSchedule(protocol_id, patient_id,
                    pro_start_date);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::FetchSchedule");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao FetchSchedule(int eventId) {
        EventdefDao eventdefDao = new EventdefDao();

        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.FetchSchedule(eventId);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::FetchSchedule");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao getTotalVisit(int protocol_id, int patient_id,
            String pro_start_date) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getTotalVisit(protocol_id, patient_id,
                    pro_start_date);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::getTotalVisit");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao getSchEvents(int protocol_id, int patient_id,
            String pro_start_date) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getSchEvents(protocol_id, patient_id,
                    pro_start_date);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::getSchEvents");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao FetchFilteredSchedule(int protocol_id, int patient_id,
            String pro_start_date, String visit, String month, String year,
            String event, String status) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.FetchFilteredSchedule(protocol_id,
                    patient_id, pro_start_date, visit, month, year, event,
                    status);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::FetchFilteredSchedule");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao FetchPreviousSchedule(int studyId, int patientId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.FetchPreviousSchedule(studyId,
                    patientId);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::FetchPreviousSchedule");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao getVisitEvents(int patProtId, int visit) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getVisitEvents(patProtId, visit);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::getVisitEvents");
            return eventdefDao;
        }
        return eventdefDao;

    }
    public EventdefDao getVisitEvents(int patProtId, int visit,String type,int protocolId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getVisitEvents(patProtId, visit,type,protocolId);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::getVisitEvents");
            return eventdefDao;
        }
        return eventdefDao;

    }

    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getVisitEvents(patProtId, status,
                    visit, exeon);
        } catch (Exception e) {
            System.out
                    .println("inside EventdefJB::getVisitEvents(int patProtId)");
            return eventdefDao;
        }
        return eventdefDao;

    }
    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon,String type,int protocolId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventdefDao = eventdefAgent.getVisitEvents(patProtId, status,
                    visit, exeon,type,protocolId);
        } catch (Exception e) {
            System.out
                    .println("inside EventdefJB::getVisitEvents(int patProtId)");
            return eventdefDao;
        }
        return eventdefDao;

    }
    
    public ArrayList findAllMovingEvents(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag,String calAssoc,int synchSugDate) {
    	ArrayList eventIds = new ArrayList();
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            eventIds = eventdefAgent.findAllMovingEvents(prevDate, actualDate,
                    eventId, patprotId, statusFlag, calAssoc, synchSugDate);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::findAllMovingEvents");
            return null;
        }
        return eventIds;
    }

    public int ChangeActualDate(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String ipAdd,
            String modifiedBy,String calAssoc,int synchSugDate) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.ChangeActualDate(prevDate, actualDate,
                    eventId, patprotId, statusFlag, ipAdd, modifiedBy,calAssoc, synchSugDate);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::ChangeActualDate");
            return -1;
        }
        return output;

    }

    public EventInfoDao getAdverseEvents(int fkStudy, int fkPer) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getByUserId starting");

            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getByUserId after remote");

            Rlog.debug("eventdef",
                    "VALUE OF fkStudy in EventdefJB::getAdverseEvents"
                            + fkStudy);
            Rlog.debug("eventdef",
                    "VALUE OF fkPer in EventdefJB::getAdverseEvents" + fkPer);

            eventinfoDao = eventdefAgentRObj.getAdverseEvents(fkStudy, fkPer);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAdverseEvents in EventdefJB " + e);
        }
        return eventinfoDao;
    }

    public EventInfoDao getAdvInfo(int fkAdverseEvent, String flag) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getAdvInfo starting");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getAdvInfo after remote");

            Rlog.debug("eventdef", "VALUE OF fkStudy in EventdefJB::getAdvInfo"
                    + fkAdverseEvent);

            eventinfoDao = eventdefAgentRObj.getAdvInfo(fkAdverseEvent, flag);
            Rlog.debug("eventdef", "EventdefJB.getAdvInfo after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAdvInfo(int fkAdverseEvent, String flag) in EventdefJB "
                            + e);
        }
        return eventinfoDao;
    }

    public EventInfoDao getAdvNotify(int fkAdverseEvent) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getAdvNotify starting");
            Rlog.debug("eventdef", "EventdefJB.getAdvNotify home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef", "EventdefJB.getAdvNotify after remote");

            Rlog.debug("eventdef",
                    "VALUE OF fkStudy in EventdefJB::getAdvNotify"
                            + fkAdverseEvent);

            eventinfoDao = eventdefAgentRObj.getAdvNotify(fkAdverseEvent);
            Rlog.debug("eventdef", "EventdefJB.getAdvNotify after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAdvNotify(int fkAdverseEvent) in EventdefJB "
                            + e);
        }
        return eventinfoDao;
    }

    /**
     *
     *
     * @return the Eventdef StateKeeper Object with the current values of the
     *         Bean
     */

    public EventdefBean createEventdefStateKeeper() {
        // SV, 9/14/04, added duration unit (type) support.

        return new EventdefBean(event_id, chain_id, org_id, event_type, name,
                notes, cost, cost_description, duration, user_id, linked_uri,
                fuzzy_period, msg_to, description, displacement,
                event_msg, patDaysBefore, patDaysAfter, usrDaysBefore,
                usrDaysAfter, patMsgBefore, patMsgAfter, usrMsgBefore,
                usrMsgAfter, creator, modifiedBy, ipAdd, sharedWith,
                durationUnit, eventVisit, cptCode, durationUnitAfter,
                fuzzyAfter,durationUnitBefore,eventCalType,eventCategory,sequence,
                eventLibType,eventLineCategory,
                eventSOSId, eventFacilityId, eventCoverageType, reasonForCoverageChange ,this.coverageNotes,statCode ); // SV, 9/29

    }

    /*
     * generates a String of XML for the eventdef details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("eventdef", "EventdefJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"eventdefId\" value=\"" + this.getEventdefId()+
         * "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventdefCodelstJobtype\" value=\"" +
         * this.getEventdefCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdefAccountId\" value=\"" +
         * this.getEventdefAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdefSiteId\" value=\"" +
         * this.getEventdefSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdefPerAddressId\" value=\"" +
         * this.getEventdefPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"eventdefLastName\" value=\"" +
         * this.getEventdefLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdefFirstName\" value=\"" +
         * this.getEventdefFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdefMidName \" value=\"" +
         * this.getEventdefMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdefWrkExp\" value=\"" +
         * this.getEventdefWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventdefPhaseInv\" value=\"" +
         * this.getEventdefPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventdefSessionTime\" value=\"" +
         * this.getEventdefSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"eventdefLoginName\" value=\"" +
         * this.getEventdefLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"eventdefPwd\" value=\"" + this.getEventdefPwd() +
         * "\" size=\"20\"/>" + "<input type=\"text\" name=\"eventdefSecQues\"
         * value=\"" + this.getEventdefSecQues() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventdefAnswer\" value=\"" +
         * this.getEventdefAnswer() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventdefStatus\" value=\"" +
         * this.getEventdefStatus() + "\" size=\"1\"/>" + "<input type=\"text\"
         * name=\"eventdefCodelstSpl\" value=\"" + this.getEventdefCodelstSpl() +
         * "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventdefGrpDefault\" value=\"" + this.getEventdefGrpDefault() +
         * "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

    public int copyNotifySetting(int p_old_fkpatprot, int p_new_fkpatprot,
            int p_copyflag, int p_creator, String p_ip) {
        int output = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            output = eventdefAgent.copyNotifySetting(p_old_fkpatprot,
                    p_new_fkpatprot, p_copyflag, p_creator, p_ip);
        } catch (Exception e) {
            System.out.println("inside EventdefJB::copyNotifySetting" + e);
            return -1;
        }
        return output;

    }

    public int getMaxCost(String chainId) {
        int cost = 0;
        try {
            EventdefAgentRObj eventdefAgent = EJBUtil.getEventdefAgentHome();
            cost = eventdefAgent.getMaxCost(chainId);
        } catch (Exception e) {
            System.out.println("EventdefJB.GetMaxCost" + e);
            return -2;
        }
        return cost;
    }

    // //////

    public EventdefDao getAllProtSelectedEvents(int protocolId,String visitId, String evtName ) {
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef",
                    "EventdefJB.getAllProtSelectedEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getAllProtSelectedEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getAllProtSelectedEvents after remote");
            eventdefDao = eventdefAgentRObj
                    .getAllProtSelectedEvents(protocolId, visitId, evtName);
            Rlog.debug("eventdef",
                    "EventdefJB.getAllProtSelectedEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAllProtSelectedEvents in EventdefJB " + e);
        }
        return eventdefDao;
    }
        //YK: FOR PCAL-20461
    public EventdefDao getAllProtSelectedVisitsEvents(int protocolId,String visitId, String evtName ) {
    	EventdefDao eventdefDao = new EventdefDao();
    	try {
    		Rlog.debug("eventdef",
    		"EventdefJB.getAllProtSelectedVisitsEvents starting");
    		Rlog.debug("eventdef", "EventdefJB.getAllProtSelectedVisitsEvents home");
    		EventdefAgentRObj eventdefAgentRObj = EJBUtil
    		.getEventdefAgentHome();
    		Rlog.debug("eventdef",
    		"EventdefJB.getAllProtSelectedVisitsEvents after remote");
    		eventdefDao = eventdefAgentRObj
    		.getAllProtSelectedVisitsEvents(protocolId, visitId, evtName);
    		Rlog.debug("eventdef",
    		"EventdefJB.getAllProtSelectedVisitsEvents after Dao");
    		return eventdefDao;
    	} catch (Exception e) {
    		Rlog.fatal("eventdef",
    				"Error in getAllProtSelectedVisitsEvents in EventdefJB " + e);
    	}
    	return eventdefDao;
    }

    public EventdefDao getScheduleEventsVisits(int patprot) {
        EventdefDao eventdefDao = new EventdefDao();
        try {

            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            eventdefDao = eventdefAgentRObj.getScheduleEventsVisits(patprot);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getScheduleEventsVisits in EventdefJB " + e);
        }
        return eventdefDao;
    }

    public EventdefDao getStudyScheduleEventsVisits(int studyId,int protocolId) {
        EventdefDao eventdefDao = new EventdefDao();
        try {

            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            eventdefDao = eventdefAgentRObj.getStudyScheduleEventsVisits(studyId,protocolId);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getStudyScheduleEventsVisits in EventdefJB " + e);
        }
        return eventdefDao;
    }


    /**
     * Delete calendar from library
     *
     * @return 0 for successful delete, -2 in case of exception
     */

    public int calendarDelete(int calId) {
        int ret = 0;
        try {
            Rlog.debug("eventdef", "In eventdefBean calendarDelete()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.calendarDelete(calId);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean -" + e);
            return -2;
        }

    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int calendarDelete(int calId, Hashtable<String, String> auditInfo) {
        int ret = 0;
        try {
            Rlog.debug("eventdef", "In eventdefBean calendarDelete()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.calendarDelete(calId,auditInfo);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean -" + e);
            return -2;
        }

    }

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In eventdefBean saveMultipleEventsVisits()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,
                    arrNote, arrSos, arrCoverage, reasonForCoverageChng, ipAdd);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean saveMultipleEventsVisits-"
                    + e);
            return -2;
        }

    }
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In eventdefBean saveMultipleEventsVisits()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon, ipAdd);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean saveMultipleEventsVisits-"
                    + e);
            return -2;
        }

    }
    

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd,String type) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In eventdefBean saveMultipleEventsVisits()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,
                    arrNote, arrSos, arrCoverage, reasonForCoverageChng, ipAdd,type);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean saveMultipleEventsVisits-..."
                    + e);
            return -2;
        }

    }
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd,String type) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In eventdefBean saveMultipleEventsVisits()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,ipAdd,type);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean saveMultipleEventsVisits-..."
                    + e);
            return -2;
        }

    }


    public int copyProtToLib(String protocol_id, String study_id,String name,String desc,String type,String shareWith,
            String userId, String ipAdd)
    {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In eventdefJB:CopyProtToLib()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.copyProtToLib(protocol_id, study_id,name,desc,type,shareWith,
                    userId, ipAdd);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean copyProtToLib"
                    + e);
            return -2;
        }


    }

    /*Added by Sonia Abrol, 08/20/06. */
    public int propagateEventUpdates(int protocolId, int eventId,
            String tableName, int primary_key, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType)
    {
        int ret = 0;
        try {
            Rlog.debug("eventdef",
                            "In eventdefJB:propagateEventUpdates()");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();

            ret = eventdefAgentRObj.propagateEventUpdates(protocolId,  eventId,
                    tableName,  primary_key,  propagateInVisitFlag,
                    propagateInEventFlag,  mode,  calType);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "In eventdefBean propagateEventUpdates-"
                    + e);
            return -2;
        }


    }

    /**
     *
     * @param protocolId
     * @return
     */

    public EventdefDao getProtVisitAndEvents(int protocolId, String search,String resetSort) {  /*YK 04Jan- SUT_1_Requirement_18489*/
        EventdefDao eventdefDao = new EventdefDao();
        try {
            Rlog.debug("eventdef", "EventdefJB.getProtVisitAndEvents starting");
            Rlog.debug("eventdef", "EventdefJB.getProtVisitAndEvents home");
            EventdefAgentRObj eventdefAgentRObj = EJBUtil
                    .getEventdefAgentHome();
            Rlog.debug("eventdef",
                    "EventdefJB.getProtVisitAndEvents after remote");
            eventdefDao = eventdefAgentRObj.getProtVisitAndEvents(protocolId, search, resetSort); /*YK 04Jan- SUT_1_Requirement_18489*/
            Rlog.debug("eventdef", "EventdefJB.getProtVisitAndEvents after Dao");
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getProtVisitAndEvents in EventdefJB " + e);
        }
        return eventdefDao;
    }


    //JM: 10April2008: #C8
    public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName) {
        int ret=0;
        try {
        	EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
            ret = eventdefAgentRObj.getMaxSeqForVisitEvents(visitId, eventName, tableName);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getMaxSeqForVisitEvents() in EventDefJB" + e);
            return -2;
        }
        return ret;
    }


  //JM: 17April2008: #C8: copy event details
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd) {
        int ret=0;
        try {
        	EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
            eventdefAgentRObj.CopyEventDetails( old_event_id,  new_event_id,  crfInfo,  userId,  ipAdd);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in CopyEventDetails() in EventDefJB" + e);
            return -2;
        }
        return ret;
    }


  /**
   * This method shall update the fk_patprot column of ER_PATTXARM table with the current protocol id/the current patient schedule
   * @param StudyId
   */
    public int updatePatTxtArmsForTheNewPatProt(int patProtIdOld, int patProtId) {
        try {
        	EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
            eventdefAgentRObj.updatePatTxtArmsForTheNewPatProt(patProtIdOld, patProtId);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Error in updatePatTxtArmsForTheNewPatProt() in EventDefJB" + e);
            return -1;
        }
        return 1;
    }
 //Yk: Added for Enhancement :PCAL-19743
   public EventdefDao getProtSelectedAndVisitEvents(int protocolId) {
    	EventdefDao eventdefDao = null;
    	try {
    		EventdefAgentRObj eventdefAgentRObj = EJBUtil.getEventdefAgentHome();
    		eventdefDao = eventdefAgentRObj
    		.getProtSelectedAndVisitEvents(protocolId);
    	} catch (Exception e) {
    		Rlog.fatal("eventassoc",
    				"In EventdefJB.getProtSelectedAndVisitEvents: "+e);
    		eventdefDao = new EventdefDao();
    	}
    	return eventdefDao;
    }







}// end of class
