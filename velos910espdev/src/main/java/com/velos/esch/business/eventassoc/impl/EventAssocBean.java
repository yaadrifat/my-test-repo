/*
 * Classname : EventAssocBean
 *
 * Version information: 1.0
 *
 * Date: 06/12/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.business.eventassoc.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

//import com.velos.esch.service.util.DateUtil;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "event_assoc")
@NamedQuery(name = "findEventIdsAssoc", query = "SELECT OBJECT(event) FROM EventAssocBean event where trim(chain_id)=:chainId")
public class EventAssocBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3617009767202174007L;

    /**
     * event id
     */
    public Integer event_id;

    /**
     * chain id
     */
    public Integer chain_id;

    public int org_id;

    /**
     * event type
     */
    public String event_type;

    /**
     * event name
     */

    public String name;

    /**
     * event notes
     */
    public String notes;

    /**
     * cost of the event
     */
    public Integer cost;

    /**
     * cost description
     */
    public String cost_description;

    /**
     * event duration
     */
    public Integer duration;

    /**
     * user id
     */

    public Integer user_id;

    /**
     * linked uri
     */

    public String linked_uri;

    /**
     * fuzzy_period
     */
    public String fuzzy_period;

    /**
     * msg_to
     */
    public String msg_to;
    
    /**
     * description
     */

    public String description;

    /**
     * displacement
     */
    public Integer displacement;

    /**
     * event_msg
     */
    public String event_msg;

    /**
     * patDaysBefore
     */

    public Integer patDaysBefore;

    /**
     * patDaysAfter
     */

    public Integer patDaysAfter;

    /**
     * usrDaysBefore
     */

    public Integer usrDaysBefore;

    /**
     * usrDaysAfter
     */

    public Integer usrDaysAfter;

    /**
     * patMsgBefore
     */

    public String patMsgBefore;

    /**
     * patMsgAfter
     */

    public String patMsgAfter;

    /**
     * usrMsgBefore
     */

    public String usrMsgBefore;

    /**
     * usrMsgAfter
     */

    public String usrMsgAfter;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /*
     * the protocol start date
     */
    public Date statusDate = null;

    /*
     * status of user
     */
    public Integer statusUser;

    /*
     * flag now used for keeping a flag for updating patient's alert
     * notification setings in case settings associated with the protocol is
     * changed. Will be used for protocol type records. Possible values: 1:apply
     * setings 0:no settings to apply
     */
    public Integer origCal;

    /*
     * Duration Type
     */

    public String durationUnit = ""; // SV, 9/14, cal enh-02

    /*
     * event's visit key
     */

    public Integer eventVisit; // SV, 9/29/04, cal-enh-03-b

    public String durationUnitAfter;

    public String cptCode;

    public String fuzzyAfter;

    private String durationUnitBefore;

    private Integer eventCalType;

    private String calAssoc;

    /*
     * the protocol Calendar schedule start date
     */
    public Date calSchDate ;

    private String eventCategory;//KM



  //JM: 06May2008, added for, Enh. #PS16
    public Integer eventSequence;



    public Integer eventFlag;
    
    //KM-16Dec09
    private String eventLibType;
    
    private String eventLineCategory;

    /* Event Site of Service */
    private Integer eventSOSId;
    
    /* Event Facility */
    private Integer eventFacilityId;
    
    /* Event Coverage Type */
    private Integer eventCoverageType;
    
    /* Calendar Default Template */
    private Integer budgetTemplate;
    
    /* Coverage Notes*/    
    private String coverageNotes; 
    
    
    /* calendar status */
    
    private Integer statCode;
    private Integer eventHideFlag;//YK: Fix for Bug#7264
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "EVENT_DEFINITION_SEQ", allocationSize=1)
    @Column(name = "event_id")
    public Integer getEvent_id() {
        return this.event_id;
    }

    public void setEvent_id(Integer event_id) {
        this.event_id = event_id;
    }

    @Column(name = "org_id")
    public int getOrg_id() {
        return this.org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    @Column(name = "chain_id")
    public String getChain_id() {
        return StringUtil.integerToString(this.chain_id);
    }

    public void setChain_id(String chain_id) {
        if (chain_id != null) {
            this.chain_id = Integer.valueOf(chain_id);
        }
    }

    @Column(name = "event_type")
    public String getEvent_type() {
        return this.event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    @Column(name = "name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "notes")
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "cost")
    public String getCost() {
        return StringUtil.integerToString(this.cost);
    }

    public void setCost(String cost) {
        if (cost != null) {
            this.cost = Integer.valueOf(cost);
        }

    }

    @Column(name = "cost_description")
    public String getCost_description() {
        return this.cost_description;
    }

    public void setCost_description(String cost_description) {
        this.cost_description = cost_description;
    }

    @Column(name = "duration")
    public String getDuration() {
        return StringUtil.integerToString(this.duration);
    }

    public void setDuration(String duration) {
        if (duration != null) {
            this.duration = Integer.valueOf(duration);
        }
    }

    @Column(name = "user_id")
    public String getUser_id() {
        return StringUtil.integerToString(this.user_id);
    }

    public void setUser_id(String user_id) {
        if (user_id != null) {
            this.user_id = Integer.valueOf(user_id);
        }
    }

    @Column(name = "linked_uri")
    public String getLinked_uri() {
        return this.linked_uri;
    }

    public void setLinked_uri(String linked_uri) {
        this.linked_uri = linked_uri;
    }

    @Column(name = "fuzzy_period")
    public String getFuzzy_period() {
        return this.fuzzy_period;
    }

    public void setFuzzy_period(String fuzzy_period) {
        this.fuzzy_period = fuzzy_period;
    }

    @Column(name = "msg_to")
    public String getMsg_to() {
        return this.msg_to;
    }

    public void setMsg_to(String msg_to) {
        this.msg_to = msg_to;
    }

       
  //JM: 20JAN2011: enh #D-FIN-9
  	@Column(name = "FK_CODELST_CALSTAT")
    public String getStatCode() {
        return StringUtil.integerToString(this.statCode);
    }

    public void setStatCode(String statCode) {
    	if (statCode != null){
    		this.statCode = Integer.valueOf(statCode);
    	}
    }

    @Column(name = "description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "displacement")
    public String getDisplacement() {
        return StringUtil.integerToString(this.displacement);
    }

    public void setDisplacement(String displacement) {
        if (displacement != null) {
            this.displacement = Integer.valueOf(displacement);
        }
    }

    @Column(name = "event_msg")
    public String getEvent_msg() {
        return this.event_msg;
    }

    public void setEvent_msg(String event_msg) {
        this.event_msg = event_msg;
    }

    @Column(name = "PAT_DAYSBEFORE")
    public String getPatDaysBefore() {
        return StringUtil.integerToString(this.patDaysBefore);
    }

    public void setPatDaysBefore(String patDaysBefore) {
        if (patDaysBefore != null) {
            this.patDaysBefore = Integer.valueOf(patDaysBefore);
        }
    }

    @Column(name = "PAT_DAYSAFTER")
    public String getPatDaysAfter() {
        return StringUtil.integerToString(this.patDaysAfter);
    }

    public void setPatDaysAfter(String patDaysAfter) {
        if (patDaysAfter != null) {
            this.patDaysAfter = Integer.valueOf(patDaysAfter);
        }
    }

    @Column(name = "USR_DAYSBEFORE")
    public String getUsrDaysBefore() {
        return StringUtil.integerToString(this.usrDaysBefore);
    }

    public void setUsrDaysBefore(String usrDaysBefore) {
        if (usrDaysBefore != null) {
            this.usrDaysBefore = Integer.valueOf(usrDaysBefore);
        }
    }

    @Column(name = "USR_DAYSAFTER")
    public String getUsrDaysAfter() {
        return StringUtil.integerToString(this.usrDaysAfter);
    }

    public void setUsrDaysAfter(String usrDaysAfter) {
        if (usrDaysAfter != null) {
            this.usrDaysAfter = Integer.valueOf(usrDaysAfter);
        }
    }

    @Column(name = "PAT_MSGBEFORE")
    public String getPatMsgBefore() {
        return this.patMsgBefore;
    }

    public void setPatMsgBefore(String patMsgBefore) {
        this.patMsgBefore = patMsgBefore;
    }

    @Column(name = "PAT_MSGAFTER")
    public String getPatMsgAfter() {
        return this.patMsgAfter;
    }

    public void setPatMsgAfter(String patMsgAfter) {
        this.patMsgAfter = patMsgAfter;
    }

    @Column(name = "USR_MSGBEFORE")
    public String getUsrMsgBefore() {
        return this.usrMsgBefore;
    }

    public void setUsrMsgBefore(String usrMsgBefore) {
        this.usrMsgBefore = usrMsgBefore;
    }

    @Column(name = "USR_MSGAFTER")
    public String getUsrMsgAfter() {
        return this.usrMsgAfter;
    }

    public void setUsrMsgAfter(String usrMsgAfter) {
        this.usrMsgAfter = usrMsgAfter;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "STATUS_DT")
    public Date getStatusDateValue() {
        // return this.statusDate;
        return this.statusDate;
    }

    public void setStatusDateValue(Date statusDate) {
        DateUtil sDate = null;
        this.statusDate = statusDate;

    }

    @Transient
    public String getStatusDate() {
        // return this.statusDate;
        return DateUtil.dateToString(getStatusDateValue());
    }

    public void setStatusDate(String statusDate) {
        DateUtil sDate = null;
        //setStatusDateValue(DateUtil.stringToDate(statusDate, "MM/dd/yyyy"));
        setStatusDateValue(DateUtil.stringToDate(statusDate, ""));

    }

    @Column(name = "STATUS_CHANGEBY")
    public String getStatusUser() {
        return StringUtil.integerToString(this.statusUser);
    }

    public void setStatusUser(String statusUser) {
        if (statusUser != null) {
            this.statusUser = Integer.valueOf(statusUser);
        }
    }

    @Column(name = "orig_cal")
    public String getOrigCal() {
        return StringUtil.integerToString(this.origCal);
    }

    public void setOrigCal(String origCal) {
        if (origCal != null) {
            this.origCal = Integer.valueOf(origCal);
        }
    }

    // SV, 9/29/04, cal enh-02
    @Column(name = "duration_unit")
    public String getDurationUnit() {
        return this.durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        if (durationUnit != null) {
            this.durationUnit = durationUnit;
        }
    }

    // SV, 9/29/04, end cal enh-02

    // SV, 9/29/04, cal enh-02
    @Column(name = "fk_visit")
    public String getEventVisit() {
        return StringUtil.integerToString(this.eventVisit);

    }

    public void setEventVisit(String eventVisit) {
        if (eventVisit != null) {
            this.eventVisit = Integer.valueOf(eventVisit);

        }
    }

    // SV, 9/29/04, end cal enh-01

    // VA 09/21/2005
    @Column(name = "EVENT_CPTCODE")
    public String getCptCode() {
        return cptCode;
    }

    public void setCptCode(String cptCode) {
        this.cptCode = cptCode;
    }

    @Column(name = "event_durationafter")
    public String getDurationUnitAfter() {
        return durationUnitAfter;
    }

    public void setDurationUnitAfter(String durationUnitAfter) {
        this.durationUnitAfter = durationUnitAfter;
    }

    @Column(name = "event_durationbefore")
    public String getDurationUnitBefore() {
        return durationUnitBefore;
    }

    public void setDurationUnitBefore(String durationUnit) {
        this.durationUnitBefore = durationUnit;
    }

    @Column(name = "event_fuzzyafter")
    public String getFuzzyAfter() {
        return fuzzyAfter;
    }

    public void setFuzzyAfter(String fuzzyAfter) {
        this.fuzzyAfter = fuzzyAfter;
    }

    @Column(name = "FK_CATLIB")
    public Integer getEventCalType() {
        return eventCalType;
    }

    public void setEventCalType(Integer eventCalType) {
        this.eventCalType = eventCalType;
    }

    @Column(name = "event_calassocto")
    public String getCalAssocTo() {
        return calAssoc;
    }

    public void setCalAssocTo(String calAssoc) {
        this.calAssoc = calAssoc;
    }

    @Column(name = "EVENT_CALSCHDATE")
    public Date getCalSchDt() {

        return this.calSchDate;
    }

    public void setCalSchDt(Date schDate) {
        DateUtil sDate = null;
        this.calSchDate = schDate;

    }

    @Transient
    public String getCalSchDate() {

        return DateUtil.dateToString(getCalSchDt());
    }

    public void setCalSchDate(String schDateStr) {
        DateUtil sDate = null;
        //setCalSchDt(DateUtil.stringToDate(schDateStr, "MM/dd/yyyy"));
        setCalSchDt(DateUtil.stringToDate(schDateStr, ""));

    }

    //KM
    @Column(name = "EVENT_CATEGORY")
    public String getEventCategory() {
    	return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
    	this.eventCategory = eventCategory;
    }


//JM: 27Mar2008

    @Column(name = "EVENT_SEQUENCE")
    public String getEventSequence() {
        return StringUtil.integerToString(this.eventSequence);
    }

    public void setEventSequence(String eventSequence) {
        if (eventSequence != null) {
            this.eventSequence = Integer.valueOf(eventSequence);
        }
    }


  //JM: 06May2008, added for, Enh. #PS16

    @Column(name = "EVENT_FLAG")
    public String getEventFlag() {
        return StringUtil.integerToString(this.eventFlag);
    }

    public void setEventFlag(String eventFlag) {
        if (eventFlag != null) {
            this.eventFlag = Integer.valueOf(eventFlag);
        }
    }

    @Column(name = "EVENT_LIBRARY_TYPE")
    public String getEventLibType() {
        return eventLibType;
    }

    public void setEventLibType(String eventLibType) {
        this.eventLibType = eventLibType;
    }
    
    @Column(name = "EVENT_LINE_CATEGORY")
    public String getEventLineCategory() {
        return eventLineCategory;
    }

    public void setEventLineCategory(String eventLineCategory) {
        this.eventLineCategory = eventLineCategory;
    }
    
    @Column(name = "SERVICE_SITE_ID")
    public String getEventSOSId() {
        return StringUtil.integerToString(this.eventSOSId);
    }

    public void setEventSOSId(String eventSOSId) {
        if (eventSOSId != null) {
            this.eventSOSId = Integer.valueOf(eventSOSId);
        }
    }
    
    @Column(name = "FACILITY_ID")
    public String getEventFacilityId() {
        return StringUtil.integerToString(this.eventFacilityId);
    }

    public void setEventFacilityId(String eventFacilityId) {
    	if (eventFacilityId != null) {
            this.eventFacilityId = Integer.valueOf(eventFacilityId);
        }
    }
    
    @Column(name = "FK_CODELST_COVERTYPE")
    public String getEventCoverageType() {
        return StringUtil.integerToString(this.eventCoverageType);
    }

    public void setEventCoverageType(String eventCoverageType) {
    	if (eventCoverageType != null) {
            this.eventCoverageType = Integer.valueOf(eventCoverageType);
        }
    }

    @Column(name = "BUDGET_TEMPLATE")
    public String getBudgetTemplate() {
        return StringUtil.integerToString(this.budgetTemplate);
    }

    public void setBudgetTemplate(String budgetTemplate) {
    	if (budgetTemplate != null) {
            this.budgetTemplate = Integer.valueOf(budgetTemplate);
        }
    }
    
    //BK 26th july 2010
    @Column(name = "COVERAGE_NOTES")  
      public String getCoverageNotes() {
  		return coverageNotes;
  	}

  	public void setCoverageNotes(String coverageNotes) {
  		this.coverageNotes = coverageNotes;
  	}		
    //YK: Fix for Bug#7264
  	 @Column(name = "HIDE_FLAG")
     public String getEventHideFlag() {
         return StringUtil.integerToString(this.eventHideFlag);
     }

     public void setEventHideFlag(String eventHideFlag) {
         if (eventHideFlag != null) {
             this.eventHideFlag = Integer.valueOf(eventHideFlag);
         }
     }
  	// / END OF GETTER SETTER METHODS

    /**
     * /** updates the eventassoc details
     *
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventAssoc(EventAssocBean edsk) {
        try {

            setChain_id(edsk.getChain_id());
            setEvent_type(edsk.getEvent_type());
            setName(edsk.getName());
            setNotes(edsk.getNotes());
            setCost(edsk.getCost());
            setCost_description(edsk.getCost_description());
            setDuration(edsk.getDuration());
            setUser_id(edsk.getUser_id());
            setLinked_uri(edsk.getLinked_uri());
            setFuzzy_period(edsk.getFuzzy_period());
            setMsg_to(edsk.getMsg_to());            
            setDescription(edsk.getDescription());
            setDisplacement(edsk.getDisplacement());
            setOrg_id(edsk.getOrg_id());
            setEvent_msg(edsk.getEvent_msg());
            setPatDaysBefore(edsk.getPatDaysBefore());
            setPatDaysAfter(edsk.getPatDaysAfter());
            setUsrDaysBefore(edsk.getUsrDaysBefore());
            setUsrDaysAfter(edsk.getUsrDaysAfter());
            setPatMsgBefore(edsk.getPatMsgBefore());
            setPatMsgAfter(edsk.getPatMsgAfter());
            setUsrMsgBefore(edsk.getUsrMsgBefore());
            setUsrMsgAfter(edsk.getUsrMsgAfter());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setStatusDate(edsk.getStatusDate());
            setStatusUser(edsk.getStatusUser());
            setOrigCal(edsk.getOrigCal());
            setDurationUnit(edsk.getDurationUnit()); // SV, 9/29/04
            setEventVisit(edsk.getEventVisit()); // SV, 9/29/04
            this.setCptCode(edsk.getCptCode());
            this.setDurationUnitAfter(edsk.getDurationUnitAfter());
            this.setDurationUnitBefore(edsk.getDurationUnitBefore());
            this.setFuzzyAfter(edsk.getFuzzyAfter());
            this.setEventCalType(edsk.getEventCalType());
            this.setCalAssocTo(edsk.getCalAssocTo());
            this.setCalSchDate(edsk.getCalSchDate());
            setEventCategory(edsk.getEventCategory()); //KM
            setEventSequence(edsk.getEventSequence());	//JM: 27Mar08
            setEventFlag(edsk.getEventFlag());//JM: 06May2008, added for, Enh. #PS16
            //KM-16Dec09
            this.setEventLibType(edsk.getEventLibType());
            this.setEventLineCategory(edsk.getEventLineCategory());
            
            this.setEventSOSId(edsk.getEventSOSId());
            this.setEventFacilityId(edsk.getEventFacilityId());
            this.setEventCoverageType(edsk.getEventCoverageType());
            this.setBudgetTemplate(edsk.getBudgetTemplate());
            this.setCoverageNotes(edsk.getCoverageNotes());
            this.setStatCode(edsk.getStatCode());
            this.setEventHideFlag(edsk.getEventHideFlag());//YK: Fix for Bug#7264
            
            return 0;

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    " error in EventAssocBean.updateEventAssoc : " + e);
            return -2;
        }
    }

    public EventAssocBean() {

    }

    public EventAssocBean(Integer event_id, String chain_id, int org_id,
            String event_type, String name, String notes, String cost,
            String cost_description, String duration, String user_id,
            String linked_uri, String fuzzy_period, String msg_to,             
            String description, String displacement,
            String event_msg, String patDaysBefore, String patDaysAfter,
            String usrDaysBefore, String usrDaysAfter, String patMsgBefore,
            String patMsgAfter, String usrMsgBefore, String usrMsgAfter,
            String creator, String modifiedBy, String ipAdd, String statusDate,
            String statusUser, String origCal, String durationUnit,
            String eventVisit, String cptCode, String durationUnitAfter,
            String fuzzyAfter, String durationUnitBefore,String eventCalType,
            String calAssoc,String schDate, String eventCategory, String sequence, String evtFlg,String eventLibType, String eventLineCategory, 
            String eventSOSId, String eventFacilityId, String eventCoverageType,
            String budgetTemplate,String coverageNotes, String statCode, String eventHideFlag) {
        super();
        // TODO Auto-generated constructor stub
        setEvent_id(event_id);
        setChain_id(chain_id);
        setOrg_id(org_id);
        //setEvent_type(event_type);
        this.setEvent_type(event_type);
        setName(name);
        setNotes(notes);
        setCost(cost);
        setCost_description(cost_description);
        setDuration(duration);
        setUser_id(user_id);
        setLinked_uri(linked_uri);
        setFuzzy_period(fuzzy_period);
        setMsg_to(msg_to);        
        setDescription(description);
        setDisplacement(displacement);
        setEvent_msg(event_msg);
        setPatDaysBefore(patDaysBefore);
        setPatDaysAfter(patDaysAfter);
        setUsrDaysBefore(usrDaysBefore);
        setUsrDaysAfter(usrDaysAfter);
        setPatMsgBefore(patMsgBefore);
        setPatMsgAfter(patMsgAfter);
        setUsrMsgBefore(usrMsgBefore);
        setUsrMsgAfter(usrMsgAfter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setStatusDate(statusDate);
        setStatusUser(statusUser);
        setOrigCal(origCal);
        setDurationUnit(durationUnit);
        setEventVisit(eventVisit);
        this.setCptCode(cptCode);
        this.setDurationUnitAfter(durationUnitAfter);
        this.setFuzzyAfter(fuzzyAfter);
        this.setDurationUnitBefore(durationUnitBefore);
        this.setEventCalType(StringUtil.stringToNum(eventCalType));
        this.setCalAssocTo(calAssoc);
        this.setCalSchDate(schDate);
        setEventCategory(eventCategory);//KM
        this.setEventSequence(sequence);//JM: 27Mar08
        setEventFlag(evtFlg);//JM: 06May2008, added for, Enh. #PS16
        //KM-16Dec09
        this.setEventLibType(eventLibType);
        this.setEventLineCategory(eventLineCategory);

        this.setEventSOSId(eventSOSId);
        this.setEventFacilityId(eventFacilityId);
        this.setEventCoverageType(eventCoverageType);
        this.setBudgetTemplate(budgetTemplate);
        this.setCoverageNotes(coverageNotes);
        this.setStatCode(statCode);
        this.setEventHideFlag(eventHideFlag);//YK: Fix for Bug#7264
    }

}// end of class
