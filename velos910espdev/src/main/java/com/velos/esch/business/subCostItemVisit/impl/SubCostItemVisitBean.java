 /*
 * Classname			SubCostItemVisitBean
 * 
 * Version information  1.0
 *
 * Date					01/21/2011 
 * 
 * Copyright notice		Velos Inc
 */
package com.velos.esch.business.subCostItemVisit.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The Subject Cost Item Visit BMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 * @version 1.0 01/21/2011
 */
@Entity
@Table(name = "sch_subcost_item_visit")
@NamedQuery(name = "findSubCostItemByFkItemAndFkVisit",
        query = "SELECT OBJECT(i) FROM SubCostItemVisitBean i where "+
        " i.fkSubCostItem = :fkItem and i.fkProtocolVisit = :fkVisit ")
public class SubCostItemVisitBean implements Serializable {
	
	private static final long serialVersionUID = 3977581398557275957L;
	
	/**
	 * subCostItemVisitId
	 */
	public int subCostItemVisitId;
	
	/**
	 * fkSubCostItem
	 */
	public Integer fkSubCostItem;
	
	/**
	 * fkProtocolVisit
	 */
	public Integer fkProtocolVisit;
	
	/**
     * creator
     */
    public Integer creator;
    
    /**
     * last modifiedBy
     */
    public Integer modifiedBy;
    
    /**
     * IP Address
     */
    public String ipAdd;

	
    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_SUBCOST_ITEM_VISIT", allocationSize=1)
    @Column(name = "PK_SUBCOST_ITEM_VISIT")
    public int getSubCostItemVisitId() {
		return this.subCostItemVisitId;
	}

	public void setSubCostItemVisitId(int subCostItemVisitId) {
		this.subCostItemVisitId = subCostItemVisitId;
	}

	@Column(name = "FK_SUBCOST_ITEM")
	public String getFkSubCostItem() {
		return StringUtil.integerToString(fkSubCostItem);
	}

	public void setFkSubCostItem(String fkSubCostItem) {
		if(fkSubCostItem != null) {
			this.fkSubCostItem = Integer.valueOf(fkSubCostItem);
		}
	}

	@Column(name = "FK_PROTOCOL_VISIT")
	public String getFkProtocolVisit() {
		return StringUtil.integerToString(fkProtocolVisit);
	}

	public void setFkProtocolVisit(String fkProtocolVisit) {
		if(fkProtocolVisit != null){
			this.fkProtocolVisit = Integer.valueOf(fkProtocolVisit);
		}
	}

	@Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    public int updateSubCostItemVisit(SubCostItemVisitBean scsk) {
        try {
        	setSubCostItemVisitId(scsk.getSubCostItemVisitId());
        	setFkSubCostItem(scsk.getFkSubCostItem());
        	setFkProtocolVisit(scsk.getFkProtocolVisit());
        	setCreator(scsk.getCreator());
        	setModifiedBy(scsk.getModifiedBy());
        	setIpAdd(scsk.getIpAdd());
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("subcost", " error in SubCostItemVisitBean.updateSubCostItemVisit");
            return -2;
        }
    }
 
    public SubCostItemVisitBean(){
    }
    
    public SubCostItemVisitBean(int subCostItemVisitId, String fkSubCostItem, String fkProtocolVisit,
    		String creator, String modifiedBy, String ipAdd) {
        super();
        
        setSubCostItemVisitId(subCostItemVisitId);
    	setFkSubCostItem(fkSubCostItem);
    	setFkProtocolVisit(fkProtocolVisit);
    	setCreator(creator);
    	setModifiedBy(modifiedBy);
    	setIpAdd(ipAdd);
    
    }
}
