/*
 * Classname : ProtVisitBean
 * 
 * Version information: 1.0
 *
 * Date: 09/22/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.business.protvisit.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import com.velos.esch.service.util.StringUtil;
import javax.persistence.Table;
import org.json.JSONObject;

import com.velos.esch.service.util.Rlog;

/* End of Import Statements */
//BK,May/13/2011,Fix #6227
//BK,May/20/2011,Fixed #6014
@Entity
@Table(name = "sch_protocol_visit")
@NamedQueries({@NamedQuery(name = "findInsertAfterVisits", query = "SELECT COUNT(visit.visit_id) FROM ProtVisitBean visit where to_number(visit.insertAfter) =?1"),
	@NamedQuery(name = "findDayZeroVisit", query = "SELECT MIN(visit.visit_id) FROM ProtVisitBean visit where to_number(visit.protocol_id) =?1 and to_number(visit.days) = 0"),
	@NamedQuery(name = "findDayZeroVisitCount", query = "SELECT COUNT(visit.visit_id) FROM ProtVisitBean visit where to_number(visit.protocol_id) =?1 and to_number(visit.days) = ?2 and to_number(visit.visit_id) <> ?3"),
	@NamedQuery(name = "findLastDayVisitCount", query = "SELECT COUNT(visit.visit_id) FROM ProtVisitBean visit where to_number(visit.protocol_id) =?1 and to_number(visit.displacement) =?2 and to_number(visit.visit_id) <> ?3")})
public class ProtVisitBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3256722879411335481L;

    /**
     * visit id
     */
    public int visit_id;

    /**
     * protocol id
     */
    public int protocol_id;

    /**
     * visit name
     */
    public String name;

    /**
     * visit description
     */
    public String description;

    /**
     * visit no.
     */

    public int visit_no;

    /**
     * displacement
     */
    public Integer displacement;

    /*
     * interval months
     */
    public int months;

    /*
     * interval weeks
     */
    public int weeks;

    /*
     * interval days
     */
     ////D-FIN-25-DAY0 BK JAN-23-2011
    public Integer days;

    /*
     * insert after
     */
    public Integer insertAfter;

    /*
     * insert after Interval
     */
    public Integer insertAfterInterval;

    /*
     * insert after IntervalUnit
     */
    public String insertAfterIntervalUnit;

    /*
     * creator
     */
    public Integer creator; // Amar

    /*
     * last modified by
     */
    public Integer modifiedBy;  // Amar

    /*
     * IP Address
     */
    public String ip_add;

    public String visitType; // SV, 10/29/04, to reset the system generated
    
    public String intervalFlag;
    
    public String durationBefore;
    
    public String durationUnitBefore;
    
    public String durationAfter;
    
    public String durationUnitAfter;
    
    public Integer offlineFlag;
    public Integer hideFlag;
    

    // status of the visit.

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SCH_PROT_VISIT_SEQ", allocationSize=1)
    @Column(name = "pk_protocol_visit")
    public int getVisit_id() {
        return visit_id;
    }

    public void setVisit_id(int visit_id) {
        this.visit_id = visit_id;
    }

    @Column(name = "fk_protocol")
    public int getProtocol_id() {
        return this.protocol_id;
    }

    public void setProtocol_id(int protocol_id) {
        this.protocol_id = protocol_id;
    }

    @Column(name = "visit_name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "visit_no")
    public int getVisit_no() {
        return this.visit_no;
    }

    public void setVisit_no(int visit_no) {
        this.visit_no = visit_no;
    }

    
    @Column(name = "displacement")
    public String getDisplacement() {
        return StringUtil.integerToString(this.displacement);
    }

    public void setDisplacement(String displacement) {
        if (displacement != null) {
            this.displacement = Integer.valueOf(displacement);
        } else this.displacement = null;
    }
    

    @Column(name = "num_months")
    public int getMonths() {
        return this.months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    @Column(name = "num_weeks")
    public int getWeeks() {
        return this.weeks;
    }

    public void setWeeks(int weeks) {
        this.weeks = weeks;
    }
//D-FIN-25-DAY0 BK JAN-23-2011
    @Column(name = "num_days")
    public Integer getDays() {
        return this.days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    @Column(name = "insert_after")
    public Integer getInsertAfter() {
        return this.insertAfter;
    }

    public void setInsertAfter(Integer insertAfter) {
        this.insertAfter = insertAfter;
    }

    @Column(name = "insert_after_interval")
    public Integer getInsertAfterInterval() {
        return this.insertAfterInterval;
    }

    public void setInsertAfterInterval(Integer insertAfterInterval) {
        this.insertAfterInterval = insertAfterInterval;
    }

    @Column(name = "insert_after_interval_unit")
    public String getInsertAfterIntervalUnit() {
        return this.insertAfterIntervalUnit;
    }

    public void setInsertAfterIntervalUnit(String insertAfterIntervalUnit) {
        this.insertAfterIntervalUnit = insertAfterIntervalUnit;
    }
   // Modified By Amarnadh for Bugzilla issue #3131
    @Column(name = "creator")
    public String getCreator() {
    	return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
    	if (creator != null) {
        this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
    	return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
    	if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getip_add() {
        return this.ip_add;
    }

    public void setip_add(String ip_add) {
        this.ip_add = ip_add;
    }

    @Column(name = "visit_type")
    public String getVisitType() {
        return this.visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }
    
    
    @Column(name = "no_interval_flag")
    public String getIntervalFlag() {
        return this.intervalFlag;
    }

    public void setIntervalFlag(String intervalFlag) {
        this.intervalFlag = intervalFlag;
    }

    //KM-DFIN4
    @Column(name ="win_before_number")
    public String getDurationBefore() {
    	return this.durationBefore;
    }
    
    public void setDurationBefore(String durationBefore) {
    	this.durationBefore = durationBefore;
	}
    
    @Column(name ="win_before_unit")
    public String getDurationUnitBefore() {
    	return this.durationUnitBefore;
    }
    
    public void setDurationUnitBefore(String durationUnitBefore) {
    	this.durationUnitBefore = durationUnitBefore;
    }
    
    @Column(name ="win_after_number")
    public String getDurationAfter() {
    	return this.durationAfter;
    }
    
    public void setDurationAfter(String durationAfter) {
    	this.durationAfter = durationAfter;
	}
    
    @Column(name ="win_after_unit")
    public String getDurationUnitAfter() {
    	return this.durationUnitAfter;
    }
    
    public void setDurationUnitAfter(String durationUnitAfter) {
    	this.durationUnitAfter = durationUnitAfter;
    }
    
    //Ak:Added for enh PCAL-20353 and 20354
    @Column(name ="offline_flag")
     public Integer getOfflineFlag() {
    	return this.offlineFlag;
    }
    
    public void setOfflineFlag(Integer offlineFlag) {
    	this.offlineFlag = offlineFlag;
    }
    
    @Column(name ="hide_flag")
    public Integer getHideFlag() {
   	return this.hideFlag;
   }
   
   public void setHideFlag(Integer hideFlag) {
   	this.hideFlag = hideFlag;
   }
    
    
    // SETTER METHODS

    // //////////////////

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this ProtVisit
     */

    /*
     * public ProtVisitStateKeeper getProtVisitStateKeeper() {
     * Rlog.debug("protvisit", "ProtVisitBean.getProtVisitStateKeeper"); return
     * new ProtVisitStateKeeper(this.visit_id, this.protocol_id, this.name,
     * this.description, this.visit_no, this.displacement, this.months,
     * this.weeks, this.days, this.insertAfter, this.insertAfterInterval,
     * this.insertAfterIntervalUnit, this.creator, this.modifiedBy, this.ip_add,
     * this.visitType); }
     */

    /**
     * sets up a state keeper containing details of the ProtVisit
     * 
     * @param vdsk
     */

    /*
     * public void setProtVisitStateKeeper(ProtVisitStateKeeper vdsk) {
     * GenerateId visitId = null; int status = 0;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); Rlog.debug("protvisit",
     * "ProtVisitBean.setProtVisitStateKeeper() Connection :" + conn); visit_id =
     * visitId.getId("SCH_PROT_VISIT_SEQ", conn); conn.close();
     * 
     * Rlog.debug("protvisit", "after setting the visit id"); // SV, ??
     * setVisit_id(visit_id); setProtocol_id(vdsk.getProtocol_id());
     * setName(vdsk.getName()); setDescription(vdsk.getDescription());
     * setVisit_no(vdsk.getVisit_no()); setDisplacement(vdsk.getDisplacement());
     * setMonths(vdsk.getMonths()); setWeeks(vdsk.getWeeks());
     * setDays(vdsk.getDays()); setInsertAfter(vdsk.getInsertAfter());
     * setInsertAfterInterval(vdsk.getInsertAfterInterval());
     * setInsertAfterIntervalUnit(vdsk.getInsertAfterIntervalUnit());
     * setModifiedBy(vdsk.getModifiedBy()); setip_add(vdsk.getip_add());
     * setVisitType(vdsk.getVisitType());// SV, 10/29 Rlog.debug("protvisit",
     * "ProtVisitBean.setProtVisitStateKeeper() ProtVisitId :" + visit_id); }
     * catch (Exception e) { System.out .println("Error in
     * setProtVisitStateKeeper() in ProtVisitBean " + e); } }
     */

    /**
     * updates the ProtVisit details
     * 
     * @param vdsk
     * @return 0 if successful; -2 for exception
     */
    public int updateProtVisit(ProtVisitBean vdsk) {
        try {

            this.setVisit_id(vdsk.getVisit_id());
            this.setName(vdsk.getName());
            this.setDescription(vdsk.getDescription());
            this.setVisit_no(vdsk.getVisit_no());
            this.setDisplacement(vdsk.getDisplacement());
            this.setMonths(vdsk.getMonths());
            this.setWeeks(vdsk.getWeeks());
            this.setDays(vdsk.getDays());
            this.setInsertAfter(vdsk.getInsertAfter());
            this.setInsertAfterInterval(vdsk.getInsertAfterInterval());
            this.setInsertAfterIntervalUnit(vdsk.getInsertAfterIntervalUnit());
            this.setCreator(vdsk.getCreator());
            this.setModifiedBy(vdsk.getModifiedBy());
            this.setip_add(vdsk.getip_add());
            this.setVisitType(vdsk.getVisitType());// SV, 10/29
            this.setIntervalFlag(vdsk.getIntervalFlag());
            this.setDurationBefore(vdsk.getDurationBefore());
            this.setDurationUnitBefore(vdsk.getDurationUnitBefore());
            this.setDurationAfter(vdsk.getDurationAfter());
            this.setDurationUnitAfter(vdsk.getDurationUnitAfter());
            

            return 0;
        } catch (Exception e) {
            Rlog.fatal("protvisit",
                    " error in ProtVisitBean.updateProtVisit : " + e);
            return -2;
        }
    }

    public ProtVisitBean() {

    }

    public ProtVisitBean(int visit_id, int protocol_id, String name,
            String description, int visit_no, String displacement, int months,
            int weeks, Integer days, Integer insertAfter, Integer insertAfterInterval,
            String insertAfterIntervalUnit, String creator, String modifiedBy,
            String ip_add, String visitType, String intervalFlag, String durationBefore,
            String durationUnitBefore, String durationAfter, String durationUnitAfter) {
        super();
        // TODO Auto-generated constructor stub
        setVisit_id(visit_id);
        setProtocol_id(protocol_id);
        setName(name);
        setDescription(description);
        setVisit_no(visit_no);
        setDisplacement(displacement);
        setMonths(months);
        setWeeks(weeks);
        setDays(days);
        setInsertAfter(insertAfter);
        setInsertAfterInterval(insertAfterInterval);
        setInsertAfterIntervalUnit(insertAfterIntervalUnit);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setip_add(ip_add);
        setVisitType(visitType);
        setIntervalFlag(intervalFlag);
        setDurationBefore(durationBefore);
        setDurationUnitBefore(durationUnitBefore);
        setDurationAfter(durationAfter);
        setDurationUnitAfter(durationUnitAfter);
    }
    
    /**
     * 
     * @param dataRecord
     * @param op
     * @return
     */
    public int setProtVisitBeanJSON(JSONObject dataRecord,char op,ProtVisitBean bVisitBean){
    	String Unit,tempUnit;
    	try{
    		
    		this.setName(dataRecord.getString("visitName"));
            this.setDescription(dataRecord.getString("description"));           
            int ntpd = dataRecord.getInt("noTimePointId");
            int noIntervalId = dataRecord.getInt("noIntervalId");
    	if (noIntervalId!=ntpd){    		
            
            this.setDisplacement(bVisitBean.getDisplacement());
            this.setMonths(bVisitBean.getMonths());
            this.setWeeks(bVisitBean.getWeeks());
           
            this.setInsertAfter(bVisitBean.getInsertAfter());
            this.setInsertAfterInterval(StringUtil.stringToNum(dataRecord.getString("insertAfterInterval")));
            tempUnit = dataRecord.getString("intervalUnit");
            Unit = tempUnit.equals("Days")?"D":tempUnit.equals("Months")?"M":tempUnit.equals("Years")?"Y":tempUnit.equals("Weeks")?"W":"";
            this.setInsertAfterIntervalUnit(Unit);  
            		
                      
        	    this.setDays(bVisitBean.getDays());  
        	    this.setIntervalFlag(String.valueOf(noIntervalId));
    	}
    	else{
    		this.setIntervalFlag(String.valueOf(noIntervalId));
    		this.setDisplacement("0");
    		this.setDays(null);
    		this.setMonths(0);
    		this.setWeeks(0);
    		this.setInsertAfter(0);
    		this.setInsertAfterInterval(0);
    	}
    	//this.setVisitType(dataRecord.getString("visitType"));
        this.setDurationBefore(dataRecord.getString("beforenum"));
        tempUnit = dataRecord.getString("durUnitA");
        Unit = tempUnit.equals("Days")?"D":tempUnit.equals("Months")?"M":tempUnit.equals("Years")?"Y":tempUnit.equals("Weeks")?"W":"";
        this.setDurationUnitBefore(Unit);
        this.setDurationAfter(dataRecord.getString("afternum"));
        tempUnit = dataRecord.getString("durUnitB");
        Unit = tempUnit.equals("Days")?"D":tempUnit.equals("Months")?"M":tempUnit.equals("Years")?"Y":tempUnit.equals("Weeks")?"W":"";
        this.setDurationUnitAfter(Unit);        
    	}
    	
        catch(Exception e){
			   Rlog.fatal("visit",
	                    "EXCEPTION IN ProtVisitBean.setProtVisitBean(JSONObject,char) " + e);
	            return -2;
			   
		   }
        return 1;
        
    }   
    

}// end of class
