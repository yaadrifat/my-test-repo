/*
 * Classname	CrfStatDao.class
 *
 * Version information 	1.0
 *
 * Date	11/29/2001
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/**
 * CrfStatDao for getting CrfStat records
 *
 * @author Arvind
 * @version : 1.0 11/29/2001
 */

public class CrfStatDao extends CommonDAO implements java.io.Serializable {
    private ArrayList crfNumbers;

    private ArrayList crfNames;

    private ArrayList crfEnterBys;

    private ArrayList crfEnterOns;

    private ArrayList crfStats;

    private ArrayList crfReviewBys;

    private ArrayList crfReviewOns;

    private ArrayList crfSentTos;

    private ArrayList crfSentBys;

    private ArrayList crfSentOns;

    private ArrayList crfIds;

    private ArrayList crfStatIds;

    private int cRows;

    public CrfStatDao() {
        crfNumbers = new ArrayList();
        crfNames = new ArrayList();
        crfEnterBys = new ArrayList();
        crfEnterOns = new ArrayList();
        crfStats = new ArrayList();
        crfReviewBys = new ArrayList();
        crfReviewOns = new ArrayList();
        crfSentTos = new ArrayList();
        crfSentBys = new ArrayList();
        crfSentOns = new ArrayList();
        crfStatIds = new ArrayList();
        crfIds = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getCrfNumbers() {
        return this.crfNumbers;
    }

    public void setCrfNumbers(ArrayList crfNumbers) {
        this.crfNumbers = crfNumbers;
    }

    public ArrayList getCrfNames() {
        return this.crfNames;
    }

    public void setCrfNames(ArrayList crfNames) {
        this.crfNames = crfNames;
    }

    public ArrayList getCrfEnterBys() {
        return this.crfEnterBys;
    }

    public void setCrfEnterBys(ArrayList crfEnterBys) {
        this.crfEnterBys = crfEnterBys;
    }

    public ArrayList getCrfEnterOns() {
        return this.crfEnterOns;
    }

    public void setCrfEnterOns(ArrayList crfEnterOns) {
        this.crfEnterOns = crfEnterOns;
    }

    public ArrayList getCrfStats() {
        return this.crfStats;
    }

    public void setCrfStats(ArrayList crfStats) {
        this.crfStats = crfStats;
    }

    public ArrayList getCrfReviewBys() {
        return this.crfReviewBys;
    }

    public void setCrfReviewBys(ArrayList crfReviewBys) {
        this.crfReviewBys = crfReviewBys;
    }

    public ArrayList getCrfReviewOns() {
        return this.crfReviewOns;
    }

    public void setCrfReviewOns(ArrayList crfReviewOns) {
        this.crfReviewOns = crfReviewOns;
    }

    public ArrayList getCrfSentTos() {
        return this.crfSentTos;
    }

    public void setCrfSentTos(ArrayList crfSentTos) {
        this.crfSentTos = crfSentTos;
    }

    public ArrayList getCrfSentBys() {
        return this.crfSentBys;
    }

    public void setCrfSentBys(ArrayList crfSentBys) {
        this.crfSentBys = crfSentBys;
    }

    public ArrayList getCrfSentOns() {
        return this.crfSentOns;
    }

    public void setCrfSentOns(ArrayList crfSentOns) {
        this.crfSentOns = crfSentOns;
    }

    public ArrayList getCrfIds() {
        return this.crfIds;
    }

    public void setCrfIds(ArrayList crfIds) {
        this.crfIds = crfIds;
    }

    public ArrayList getCrfStatIds() {
        return this.crfStatIds;
    }

    public void setCrfStatIds(ArrayList crfStatIds) {
        this.crfStatIds = crfStatIds;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setCrfNumbers(String crfNum) {
        crfNumbers.add(crfNum);
    }

    public void setCrfNames(String crfName) {
        crfNames.add(crfName);
    }

    public void setCrfEnterBys(String crfEnterBy) {
        crfEnterBys.add(crfEnterBy);
    }

    public void setCrfEnterOns(String crfEnterOn) {
        crfEnterOns.add(crfEnterOn);
    }

    public void setCrfStats(String crfStat) {
        crfStats.add(crfStat);
    }

    public void setCrfReviewBys(String crfReviewBy) {
        crfReviewBys.add(crfReviewBy);
    }

    public void setCrfReviewOns(String crfReviewOn) {
        crfReviewOns.add(crfReviewOn);
    }

    public void setCrfSentTos(String crfSentTo) {
        crfSentTos.add(crfSentTo);
    }

    public void setCrfSentBys(String crfSentBy) {
        crfSentBys.add(crfSentBy);
    }

    public void setCrfSentOns(String crfSentOn) {
        crfSentOns.add(crfSentOn);
    }

    public void setCrfIds(Integer crfId) {
        crfIds.add(crfId);
    }

    public void setCrfStatIds(Integer crfStatId) {
        crfStatIds.add(crfStatId);
    }

    // end of getter and setter methods

    /**
     * Gets all Sites for an Account. Sets the values in the class attributes
     *
     * @param eventId
     *            Id of the Event
     */

    public void getCrfValues(int patProtId, int visit) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            if (visit == 0) {
                sqlStr = "select a.pk_crf, "
                        + "a.crf_number, "
                        + "a.crf_name, "
                        + "b.pk_crfstat, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_enterby) as crfstat_enterby, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_reviewby) as crfstat_reviewby, "
                        + "to_char(b.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_reviewon, "
                        + "(select usr_lst (b.crfstat_sentto) from dual) crfstat_sentto, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_sentby) as crfstat_sentby, "
                        + "to_char(b.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_senton, "
                        + "to_char(b.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_enteron, "
                        + "c.codelst_desc "
                        + "from sch_crf a, sch_crfstat b, sch_codelst c "
                        + "where a.pk_crf = b.fk_crf and c.pk_codelst = b.FK_CODELST_CRFSTAT and a.fk_events1 = '0000000000' "
                        + "and fk_patprot = ? " + " order by pk_crf desc , "
                        + "pk_crfstat desc";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, patProtId);
            } else {
                sqlStr = "select a.pk_crf, "
                        + "a.crf_number, "
                        + "a.crf_name, "
                        + "b.pk_crfstat, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_enterby) as crfstat_enterby, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_reviewby) as crfstat_reviewby, "
                        + "to_char(b.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_reviewon, "
                        + "(select usr_lst (b.crfstat_sentto) from dual) crfstat_sentto, "
                        + "(select usr_lastname || ', ' || usr_firstname from er_user  where pk_user = b.crfstat_sentby) as crfstat_sentby, "
                        + "to_char(b.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_senton, "
                        + "to_char(b.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT) as crfstat_enteron, "
                        + "c.codelst_desc "
                        + "from sch_crf a, sch_crfstat b, sch_codelst c "
                        + "where a.pk_crf = b.fk_crf and c.pk_codelst = b.FK_CODELST_CRFSTAT and a.fk_events1 in (select event_id from sch_events1 where fk_patprot = ? and visit = ?) "
                        + "order by pk_crf desc , " + "pk_crfstat desc";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, patProtId);
                pstmt.setInt(2, visit);
            }

            Rlog.debug("dummy", "SQL**** " + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setCrfNumbers(rs.getString("crf_number"));
                setCrfNames(rs.getString("crf_name"));
                setCrfEnterBys(rs.getString("crfstat_enterby"));
                setCrfEnterOns(rs.getString("crfstat_enteron"));
                setCrfStats(rs.getString("codelst_desc"));
                setCrfReviewBys(rs.getString("crfstat_reviewby"));
                setCrfReviewOns(rs.getString("crfstat_reviewon"));
                setCrfSentTos(rs.getString("crfstat_sentto"));
                setCrfSentBys(rs.getString("crfstat_sentby"));
                setCrfSentOns(rs.getString("crfstat_senton"));
                setCrfIds(new Integer(rs.getInt("pk_crf")));
                setCrfStatIds(new Integer(rs.getInt("pk_crfstat")));

                rows++;
                Rlog.debug("site", "SiteDao.getSiteValues rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("CRF",
                    "CrfStatDao.getCrfStatValues EXCEPTION IN FETCHING FROM CrfStat table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
