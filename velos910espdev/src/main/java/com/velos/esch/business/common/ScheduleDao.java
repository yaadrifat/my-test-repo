/*
 * Classname : ScheduleDao
 * 
 * Version information : 1.0 
 *
 * Date: 11/15/2006
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Abrol
 */

package com.velos.esch.business.common;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.DateUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */

/*
 * ScheduleDao implements dataHolder pattern
 * Starting with some basic fields but will extend the class as and when needed
 */

public class ScheduleDao extends CommonDAO implements java.io.Serializable {
	
	private ArrayList protocolNames;
	private  ArrayList protocolIds;
	private String patientId;
	private String studyId;
	private ArrayList protocolStartDates;
	private ArrayList patProtIds;
	
	//fields added 5/6/09 for calculating visit window on events
	/** start date from SCH.EVENTS1 **/
	private ArrayList<java.util.Date> eventStartDates;
	
	/** beginning of window for this event **/
	private ArrayList<java.util.Date> eventWindowStart;
	
	/** unit for beginning of window for this event **/
	private ArrayList<java.util.Date> eventWindowEnd;
	
	/** CPT code related to event (from esch.event_assoc) **/
	private ArrayList eventCPTs;
	//end add fields
	
	private ArrayList<String> visitMonths;
	
	private ArrayList<ArrayList<Integer>> listOfVisitFKsByMonth;
	
    private ArrayList<ArrayList<String>> listOfVisitNamesByMonth;
    
    private ArrayList<ArrayList<String>> listOfStartDatesByMonth;
    
    private ArrayList<ArrayList<String>> listOfActualDatesByMonth;

    private ArrayList<ArrayList<Integer>> listOfWinBeforeNumbersByMonth;
    
    private ArrayList<ArrayList<String>> listOfWinBeforeUnitsByMonth;
    
    private ArrayList<ArrayList<Integer>> listOfWinAfterNumbersByMonth;
    
    private ArrayList<ArrayList<String>> listOfWinAfterUnitsByMonth;
    
    private ArrayList<ArrayList<Integer>> listOfEvtCountsByMonth;
    
    private ArrayList<ArrayList<Integer>> listOfDoneCountsByMonth;
    
    private ArrayList<ArrayList<Integer>> listOfPastsByMonth;
    
	public ScheduleDao() {
		super();
		// TODO Auto-generated constructor stub
		protocolNames = new ArrayList();
		protocolIds = new ArrayList();
		patientId = "";
		studyId = "" ;
		protocolStartDates = new ArrayList();
		patProtIds = new ArrayList();
		visitMonths = new ArrayList<String>();
		listOfVisitFKsByMonth = new ArrayList<ArrayList<Integer>>();
		listOfVisitNamesByMonth = new ArrayList<ArrayList<String>>();
		listOfStartDatesByMonth = new ArrayList<ArrayList<String>>();
		listOfActualDatesByMonth = new ArrayList<ArrayList<String>>();
		listOfWinBeforeNumbersByMonth = new ArrayList<ArrayList<Integer>>();
		listOfWinBeforeUnitsByMonth = new ArrayList<ArrayList<String>>();
		listOfWinAfterNumbersByMonth = new ArrayList<ArrayList<Integer>>();
		listOfWinAfterUnitsByMonth = new ArrayList<ArrayList<String>>();
		listOfEvtCountsByMonth = new ArrayList<ArrayList<Integer>>();
		listOfDoneCountsByMonth = new ArrayList<ArrayList<Integer>>();
		listOfPastsByMonth = new ArrayList<ArrayList<Integer>>();
	}
	

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	

	public ArrayList getPatProtIds() {
		return patProtIds;
	}
	
	
	public String getPatProtIds(int index) {
		if (index < patProtIds.size() )
		{
			return (String)patProtIds.get(index);
		}
		else
		{
			return "";
		}
	}

	public void setPatProtIds(ArrayList patProtIds) {
		this.patProtIds = patProtIds;
	}
	
	public void setPatProtIds(String patProtId) {
		this.patProtIds.add(patProtId);
	}

	public ArrayList getProtocolIds() {
		return protocolIds;
	}
	
	
	public String getProtocolIds(int index) {
		
		if (index < protocolIds.size() )
		{
			return (String)protocolIds.get(index);
		}
		else
		{
			return "";
		}
	}
	
	public void setEventCPT(ArrayList eventCPTs){
		this.eventCPTs = eventCPTs;
	}

	public void setProtocolIds(ArrayList protocolIds) {
		this.protocolIds = protocolIds;
	}
	
	public void setProtocolIds(String protocolId) {
		this.protocolIds.add(protocolId);
	}

	public ArrayList getProtocolNames() {
		return protocolNames;
	}
	
	public String getProtocolNames(int index) {
		
		if (index < protocolNames.size() )
		{
			return (String)protocolNames.get(index);
		}
		else
		{
			return "";
		}
		
	}

	public void setProtocolNames(ArrayList protocolNames) {
		this.protocolNames = protocolNames;
	}
	
	public void setProtocolNames(String protocolName) {
		this.protocolNames.add(protocolName);
	}

	public ArrayList getProtocolStartDates() {
		return protocolStartDates;
	}
	
	public String getProtocolStartDates(int index) {
		
		if (index < protocolStartDates.size() )
		{
			return (String)protocolStartDates.get(index);
		}
		else
		{
			return "";
		}
		
		
	}

	public void setProtocolStartDates(ArrayList protocolStartDates) {
		this.protocolStartDates = protocolStartDates;
	}
	
	public void setProtocolStartDates(String protocolStartDate) {
		this.protocolStartDates.add(protocolStartDate);
	}

	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	
	
	
	  /** Returns main Schedule information for a patient on a study
	   */
	  public void getAvailableSchedules(String patientId, String studyId){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
      
	      try {
	          conn = getConnection();

	          sql= "select p.pk_patprot,p.fk_protocol,ev.name as protocol_name,to_char(p.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) patprot_start from er_patprot p, event_assoc ev where p.fk_study = ?  and  p.fk_per = ? and ev.event_id = p.fk_protocol and ev.event_type = 'P' order by pk_patprot desc";	
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setString(1, studyId);                      
		          pstmt.setString(2, patientId);
	                                
	          ResultSet rs = pstmt.executeQuery();
	          
	            while (rs.next()) {
	            	setProtocolIds(rs.getString("fk_protocol"));
	            	setProtocolNames(rs.getString("protocol_name"));
	            	setPatProtIds(rs.getString("pk_patprot"));
	            	setProtocolStartDates(rs.getString("patprot_start"));
	            }
	           
	      } catch (SQLException ex) {
	          Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getAvailableSchedules(String patientId, String studyId)"+ ex);
	          
	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();
	    	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getAvailableSchedules(String patientId, String studyId)"+ ex);
	              
	              
	          } 
	 
	    	  try {
		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getAvailableSchedules(String patientId, String studyId)"+ ex);
		              
		          } 
	      } 
	      
	   }
	  
	  public int deleteSchedules(String[] deleteIds,int userId) {
	        int ret = 1;
	        CallableStatement cstmt = null;
	        Connection conn = null;
	        try {
	            conn = getConnection();
	             ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);
	              	            
	            
	            cstmt = conn.prepareCall("{call SP_DELETE_SCHEDULES(?,?,?)}");//KV:BugNo:5111
	            cstmt.setArray(1, deleteIdsArray);
	            cstmt.setInt(2, userId);
	            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
	            cstmt.execute();
	            Rlog.debug("eventinfo","ScheduleDao:deleteSchedules after execute of deleteSchedules");
	            ret = cstmt.getInt(3);
	            Rlog.debug("eventinfo", "ScheduleDao:deleteSchedules-after execute of RETURN VALUE"
	                    + ret);

	            return ret;

	        } catch (Exception e) {
	            Rlog.fatal("eventinfo",
	                    "EXCEPTION in ScheduleDao:deleteSchedules, excecuting Stored Procedure "
	                            + e);
	            e.printStackTrace();
	            return -1;
	        } finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            	
	            	Rlog.fatal("eventinfo",
		                    "EXCEPTION in ScheduleDao:deleteSchedules"
		                            + e);
	            	e.printStackTrace();
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            	
	            	Rlog.fatal("eventinfo",
		                    "EXCEPTION in ScheduleDao:deleteSchedules"
		                            + e);	
	            	e.printStackTrace();
	            }
	         }

	     }


	  public Hashtable getVisitEventForScheduledEvent(String schevents1_pk){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
	      Hashtable htRet = new Hashtable();
	      String evname = "";
	      String visname = "";
      
	      try {
	          conn = getConnection();

	          sql= "select (select visit_name from sch_protocol_visit where pk_protocol_visit = a.fk_visit) visit_name," +
	          		" (select name from event_assoc o where o.event_id = a.fk_assoc) event_name from sch_events1 a where a.event_id = ?";	
		      
	          pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, StringUtil.stringToNum(schevents1_pk));                      
		           
	                                
	          ResultSet rs = pstmt.executeQuery();
	          
	            while (rs.next()) {
	            	evname = rs.getString("event_name");
	            	visname = rs.getString("visit_name");
	            	
	            	
	            }
	         
	            
	            htRet.put("eventName",evname);
	            htRet.put("visitName",visname);
	            
	           //return htRet;
	      } catch (SQLException ex) {
	          Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitEventForScheduledEvent"+ ex);
	          
	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();
	    	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitEventForScheduledEvent"+ ex);
	          } 
	    	  
	    	  
	    	  try {

		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitEventForScheduledEvent"+ ex);
		          } 
	      } 
	      return htRet;
	   }

	  /**
	   * Populates this DAO with fields for calculating the visit window for each event for a visit. 
	   * eventStartDates, eventAssocFuzzyPeriods, eventAssocFuzzyDurations, 
	   * eventAssocAfterFuzzyPeriods,eventAssocAfterFuzzyDurations
	   * 
	   * @param visitId visitId for the visit to grab event info for
	   * @param patProtId patprotId used to search for the events in the visit, as patients
	   * can be in multiple studies that might share the same visit ids
	   * @author dylan
	   * 
	   */
	  public void getVisitWindowDateRange(int visitId, String patProtId){

		  eventStartDates = new ArrayList<java.util.Date>();
		  eventWindowStart = new ArrayList<java.util.Date>();
		  eventWindowEnd = new ArrayList<java.util.Date>();
		  eventCPTs = new ArrayList<String>();
		  PreparedStatement pstmt = null;
		  Connection conn = null;
		  StringBuilder sql = new StringBuilder();

		  try {
			  conn = getConnection();

			  sql.append("select event.start_date_time, assoc.fuzzy_period,");
			  sql.append("assoc.event_durationbefore, assoc.event_fuzzyafter,");
			  sql.append("assoc.event_durationafter, assoc.event_cptcode ");
			  sql.append(" from event_assoc assoc, sch_events1 event");
			  sql.append(" where event.fk_assoc = assoc.event_id and assoc.fk_visit = ? and event.fk_patprot = ?");
			  Rlog.debug("eventinfo", sql.toString());
			  pstmt = conn.prepareStatement(sql.toString()); 
			  pstmt.setInt(1, visitId);                      
			  pstmt.setString(2, patProtId);

			  ResultSet rs = pstmt.executeQuery();

			  while(rs.next()){
				  Date eventStartDate = rs.getDate("start_date_time");  
				  eventStartDates.add(eventStartDate);
				  String fuzzyPeriodBegin = rs.getString("fuzzy_period");
				  String beginUnit = rs.getString("event_durationbefore");
				  String fuzzyPeriodEnd = rs.getString("event_fuzzyafter");
				  String endUnit = rs.getString("event_durationafter");
				  String cptCode = rs.getString("event_cptcode");
				  eventCPTs.add(cptCode);
				  if (fuzzyPeriodBegin != null && fuzzyPeriodBegin.length() > 0 &&
						  beginUnit != null && beginUnit.length() > 0){
					  Calendar windowBegin = new GregorianCalendar();
					  windowBegin.setTime(eventStartDate);
					  //add the period to the start date
					  windowBegin.add(
							  intervalToCalendarField(beginUnit),
							  -(new Integer(fuzzyPeriodBegin)));
					  Rlog.debug("eventinfo", "Begin: " + windowBegin.toString());
					  eventWindowStart.add(windowBegin.getTime());
				  }
				  else{
					  //padd the arraylist to keep index event
					  eventWindowStart.add(null);
				  }
				  if (fuzzyPeriodEnd != null && fuzzyPeriodEnd.length() > 0 &&
						  endUnit != null && endUnit.length() > 0){
					  Calendar windowEnd = new GregorianCalendar();
					  windowEnd.setTime(eventStartDate);
					  //subtract the period from the start date
					  windowEnd.add(
							  intervalToCalendarField(beginUnit),
							  new Integer(fuzzyPeriodBegin));
					  Rlog.debug("eventinfo", "End: " + windowEnd.toString());
					  eventWindowEnd.add(windowEnd.getTime());
				  }
				  else{
					  //pad the arraylist to keep index even
					  eventWindowEnd.add(null);
				  }
			  } 
		
		  } 
		  catch (SQLException ex) {
			  Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitWindowDateRange"+ ex);

		  } 
		  catch (Throwable t) {
			  Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitWindowDateRange"+ t);

		  }
		  finally{
			  try {
				  if (pstmt != null)
					  pstmt.close();
			  }
			  catch (SQLException ex) {
				  Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitWindowDateRange"+ ex);
			  } 
			  
			  try {
				  if (conn != null)
					  conn.close();
			  }
			  catch (SQLException ex) {
				  Rlog.fatal("eventinfo","ScheduleDao EXCEPTION in getVisitWindowDateRange"+ ex);
			  } 
		  } 
	  }

	  public ArrayList<String> getEventCPTs(){
		  return eventCPTs;
	  }
	  public ArrayList<java.util.Date> getEventStartDates() {
		  return eventStartDates;
	  }


	  public ArrayList<java.util.Date> getEventWindowStart() {
		  return eventWindowStart;
	  }


	  public ArrayList<java.util.Date> getEventWindowEnd() {
		  return eventWindowEnd;
	  }

	  private int intervalToCalendarField(String interval){
		  if ("H".equalsIgnoreCase(interval)){
			  return Calendar.HOUR_OF_DAY;
		  }
		  if ("D".equalsIgnoreCase(interval)){
			  return Calendar.DAY_OF_MONTH;
		  }
		  if("W".equalsIgnoreCase(interval)){
			  return Calendar.WEEK_OF_MONTH;
		  }
		  return -1;
	  }
	  
	  public void getVisitsByMonth(int patProdId, HashMap paramMap) {
          Connection conn = null;
	      PreparedStatement pstmt = null;
	      StringBuilder sql = new StringBuilder();
	      SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy.MM");
	      ArrayList<String> allMonthsOfVisits = new ArrayList<String>();
          ArrayList<Integer> allFKsOfVisits = new ArrayList<Integer>();
          ArrayList<String> allNamesOfVisits = new ArrayList<String>();
          ArrayList<String> allStartDatesOfVisits = new ArrayList<String>();
          ArrayList<String> allActualDatesOfVisits = new ArrayList<String>();
          ArrayList<Integer> allWinBeforeNumbersOfVisits = new ArrayList<Integer>();
          ArrayList<String> allWinBeforeUnitsOfVisits = new ArrayList<String>();
          ArrayList<Integer> allWinAfterNumbersOfVisits = new ArrayList<Integer>();
          ArrayList<String> allWinAfterUnitsOfVisits = new ArrayList<String>();
          ArrayList<Integer> allEvtCountsOfVisits = new ArrayList<Integer>();
          ArrayList<Integer> allDoneCountsOfVisits = new ArrayList<Integer>();
          ArrayList<Integer> allPastsOfVisits = new ArrayList<Integer>();
          
	      try {
	          conn = getConnection();
	          sql.append(" select ev.FK_VISIT, v.VISIT_NAME, v.VISIT_NO, ");
	          sql.append(" CASE (MIN (NVL(ev.ACTUAL_SCHDATE, to_date('01/01/3000', 'mm/dd/yyyy')))) ");
	          sql.append(" WHEN to_date('01/01/3000', 'mm/dd/yyyy') THEN NULL ");
	          sql.append(" ELSE MIN (NVL(ev.ACTUAL_SCHDATE, to_date('01/01/3000', 'mm/dd/yyyy'))) ");
	          sql.append(" END ACTUAL_SCHDATE,  ");
	          // Check for NULL ACTUAL_SCHDATE for START_DATE_TIME; this is intentional
	          sql.append(" CASE (MIN (NVL2(ev.ACTUAL_SCHDATE, ev.START_DATE_TIME, to_date('01/01/3000', 'mm/dd/yyyy'))))  ");
	          sql.append(" WHEN to_date('01/01/3000', 'mm/dd/yyyy') THEN NULL ");
	          sql.append(" ELSE MIN (NVL2(ev.ACTUAL_SCHDATE, ev.START_DATE_TIME, to_date('01/01/3000', 'mm/dd/yyyy')))  ");
	          sql.append(" END START_DATE_TIME, ");
	          sql.append(" v.WIN_BEFORE_NUMBER, v.WIN_BEFORE_UNIT, ");
              sql.append(" v.WIN_AFTER_NUMBER, v.WIN_AFTER_UNIT, ");
              sql.append(" count(ev.EVENT_ID) EVT_COUNT, ");
              sql.append(" (select count(*) from sch_events1 ev1 where ev1.FK_PATPROT = ? ");
              sql.append("    and ev1.FK_VISIT = ev.FK_VISIT ");
              sql.append("    and ev1.ISCONFIRMED = (select PK_CODELST from SCH_CODELST where ");
              sql.append("    CODELST_TYPE = 'eventstatus' and CODELST_SUBTYP = 'ev_done')) DONE_COUNT, ");
              sql.append(" CASE (  ");
              sql.append("    select count(*) from sch_events1 ev1 where ");
              sql.append("    ev1.FK_PATPROT = ? and ev1.FK_VISIT = ev.FK_VISIT ");
              sql.append("    and ev1.ISCONFIRMED = (select PK_CODELST from SCH_CODELST where ");
              sql.append("        CODELST_TYPE = 'eventstatus' and CODELST_SUBTYP = 'ev_notdone') ");
              sql.append("        and trunc(ev1.ACTUAL_SCHDATE - sysdate) < 0 ");
              sql.append(" ) WHEN 0 THEN 0 ELSE 1 ");
              sql.append(" END IS_PAST ");
	          sql.append(" from SCH_EVENTS1 ev, SCH_PROTOCOL_VISIT v ");
	          sql.append(" where ev.FK_VISIT = v.PK_PROTOCOL_VISIT and ev.FK_PATPROT = ? ");
	          if (paramMap.containsKey("visit")) {
	              sql.append(" and ev.VISIT = ? ");
	          }
              if (paramMap.containsKey("month")) {
                  sql.append(" and TO_NUMBER(TO_CHAR (ACTUAL_SCHDATE, 'mm')) = ? ");
              }
              if (paramMap.containsKey("year")) {
                  sql.append(" and TO_NUMBER(TO_CHAR (ACTUAL_SCHDATE, 'yyyy')) = ? ");
              }
              if (paramMap.containsKey("event")) {
                  sql.append(" and UPPER (TRIM (ev.DESCRIPTION)) = UPPER (TRIM (?)) ");
              }
              if (paramMap.containsKey("eventStatus")) {
                  sql.append(" and ev.ISCONFIRMED = ? ");
              }
              if (paramMap.containsKey("dateRange")) {
                  String dateRange = (String)paramMap.get("dateRange");
                  sql.append(" and ( ").append(com.velos.eres.service.util.StringUtil.getRelativeTimeLong(
                          "(select ACTUAL_SCHDATE from dual)", Calendar.getInstance(), dateRange));
                  sql.append(" ) ");
              }
              
              sql.append(" group by ev.FK_VISIT, v.VISIT_NO, v.VISIT_NAME,  ");
              sql.append(" v.WIN_BEFORE_NUMBER, v.WIN_BEFORE_UNIT, v.WIN_AFTER_NUMBER, v.WIN_AFTER_UNIT ");
	          sql.append(" order by ACTUAL_SCHDATE, v.VISIT_NO, ev.FK_VISIT nulls last ");

	          pstmt = conn.prepareStatement(sql.toString()); 
              pstmt.setInt(1, patProdId);
              pstmt.setInt(2, patProdId);
              pstmt.setInt(3, patProdId);

              int paramIndex = 4;
              // Make sure the parameters below are listed in the same order as above
              if (paramMap.containsKey("visit")) {
                  pstmt.setInt(paramIndex++, (Integer)paramMap.get("visit"));
              }
              if (paramMap.containsKey("month")) {
                  pstmt.setInt(paramIndex++, (Integer)paramMap.get("month"));
              }
              if (paramMap.containsKey("year")) {
                  pstmt.setInt(paramIndex++, (Integer)paramMap.get("year"));
              }
              if (paramMap.containsKey("event")) {
                  pstmt.setString(paramIndex++, (String)paramMap.get("event"));
              }
              if (paramMap.containsKey("eventStatus")) {
                  pstmt.setInt(paramIndex++, (Integer)paramMap.get("eventStatus"));
              }
              
              ResultSet rs = pstmt.executeQuery();
              while(rs.next()) {
                  String formattedMonth = rs.getDate("ACTUAL_SCHDATE") == null ? "" : 
                      monthFormat.format(rs.getDate("ACTUAL_SCHDATE"));
                  setVisitMonths(formattedMonth);
                  allMonthsOfVisits.add(formattedMonth);
                  allFKsOfVisits.add(rs.getInt("FK_VISIT"));
                  allNamesOfVisits.add(rs.getString("VISIT_NAME"));
                  allStartDatesOfVisits.add(DateUtil.dateToString(rs.getDate("START_DATE_TIME")));
                  if (rs.getDate("ACTUAL_SCHDATE") == null) {
                      allActualDatesOfVisits.add("");
                  } else {
                      allActualDatesOfVisits.add(DateUtil.dateToString(rs.getDate("ACTUAL_SCHDATE")));
                  }
                  allWinBeforeNumbersOfVisits.add(rs.getInt("WIN_BEFORE_NUMBER"));
                  if (rs.getString("WIN_BEFORE_UNIT") == null) {
                      allWinBeforeUnitsOfVisits.add("");
                  } else {
                      allWinBeforeUnitsOfVisits.add(rs.getString("WIN_BEFORE_UNIT"));
                  }
                  allWinAfterNumbersOfVisits.add(rs.getInt("WIN_AFTER_NUMBER"));
                  if (rs.getString("WIN_AFTER_UNIT") == null) {
                      allWinAfterUnitsOfVisits.add("");
                  } else {
                      allWinAfterUnitsOfVisits.add(rs.getString("WIN_AFTER_UNIT"));
                  }
                  allEvtCountsOfVisits.add(rs.getInt("EVT_COUNT"));
                  allDoneCountsOfVisits.add(rs.getInt("DONE_COUNT"));
                  allPastsOfVisits.add(rs.getInt("IS_PAST"));
              }
              
              // Organize visits by month
              if (allFKsOfVisits.size() > 0) {
                  String monthOfLastVisit = allMonthsOfVisits.get(0);
                  ArrayList<Integer> visitListOfOneMonth = new ArrayList<Integer>();
                  ArrayList<String> visitNameListOfOneMonth = new ArrayList<String>();
                  ArrayList<String> startDateListOfOneMonth = new ArrayList<String>();
                  ArrayList<String> actualDateListOfOneMonth = new ArrayList<String>();
                  ArrayList<Integer> winBeforeNumberListOfOneMonth = new ArrayList<Integer>();
                  ArrayList<String> winBeforeUnitListOfOneMonth = new ArrayList<String>();
                  ArrayList<Integer> winAfterNumberListOfOneMonth = new ArrayList<Integer>();
                  ArrayList<String> winAfterUnitListOfOneMonth = new ArrayList<String>();
                  ArrayList<Integer> evtCountListOfOneMonth = new ArrayList<Integer>();
                  ArrayList<Integer> doneCountListOfOneMonth = new ArrayList<Integer>();
                  ArrayList<Integer> pastListOfOneMonth = new ArrayList<Integer>();
                  for (int iX=0; iX<allFKsOfVisits.size(); iX++) {
                      if (allMonthsOfVisits.get(iX).equals(monthOfLastVisit)) {
                          if (!visitListOfOneMonth.contains(allFKsOfVisits.get(iX))) {
                              visitListOfOneMonth.add(allFKsOfVisits.get(iX));
                              visitNameListOfOneMonth.add(allNamesOfVisits.get(iX));
                              startDateListOfOneMonth.add(allStartDatesOfVisits.get(iX));
                              actualDateListOfOneMonth.add(allActualDatesOfVisits.get(iX));
                              winBeforeNumberListOfOneMonth.add(allWinBeforeNumbersOfVisits.get(iX));
                              winBeforeUnitListOfOneMonth.add(allWinBeforeUnitsOfVisits.get(iX));
                              winAfterNumberListOfOneMonth.add(allWinAfterNumbersOfVisits.get(iX));
                              winAfterUnitListOfOneMonth.add(allWinAfterUnitsOfVisits.get(iX));
                              evtCountListOfOneMonth.add(allEvtCountsOfVisits.get(iX));
                              doneCountListOfOneMonth.add(allDoneCountsOfVisits.get(iX));
                              pastListOfOneMonth.add(allPastsOfVisits.get(iX));
                          }
                      } else {
                          listOfVisitFKsByMonth.add(visitListOfOneMonth);
                          listOfVisitNamesByMonth.add(visitNameListOfOneMonth);
                          listOfStartDatesByMonth.add(startDateListOfOneMonth);
                          listOfActualDatesByMonth.add(actualDateListOfOneMonth);
                          listOfWinBeforeNumbersByMonth.add(winBeforeNumberListOfOneMonth);
                          listOfWinBeforeUnitsByMonth.add(winBeforeUnitListOfOneMonth);
                          listOfWinAfterNumbersByMonth.add(winAfterNumberListOfOneMonth);
                          listOfWinAfterUnitsByMonth.add(winAfterUnitListOfOneMonth);
                          listOfEvtCountsByMonth.add(evtCountListOfOneMonth);
                          listOfDoneCountsByMonth.add(doneCountListOfOneMonth);
                          listOfPastsByMonth.add(pastListOfOneMonth);
                          visitListOfOneMonth = new ArrayList<Integer>();
                          visitNameListOfOneMonth = new ArrayList<String>();
                          startDateListOfOneMonth = new ArrayList<String>();
                          actualDateListOfOneMonth = new ArrayList<String>();
                          winBeforeNumberListOfOneMonth = new ArrayList<Integer>();
                          winBeforeUnitListOfOneMonth = new ArrayList<String>();
                          winAfterNumberListOfOneMonth = new ArrayList<Integer>();
                          winAfterUnitListOfOneMonth = new ArrayList<String>();
                          evtCountListOfOneMonth = new ArrayList<Integer>();
                          doneCountListOfOneMonth = new ArrayList<Integer>();
                          pastListOfOneMonth = new ArrayList<Integer>();
                          if (!visitListOfOneMonth.contains(allFKsOfVisits.get(iX))) {
                              visitListOfOneMonth.add(allFKsOfVisits.get(iX));
                              visitNameListOfOneMonth.add(allNamesOfVisits.get(iX));
                              startDateListOfOneMonth.add(allStartDatesOfVisits.get(iX));
                              actualDateListOfOneMonth.add(allActualDatesOfVisits.get(iX));
                              winBeforeNumberListOfOneMonth.add(allWinBeforeNumbersOfVisits.get(iX));
                              winBeforeUnitListOfOneMonth.add(allWinBeforeUnitsOfVisits.get(iX));
                              winAfterNumberListOfOneMonth.add(allWinAfterNumbersOfVisits.get(iX));
                              winAfterUnitListOfOneMonth.add(allWinAfterUnitsOfVisits.get(iX));
                              evtCountListOfOneMonth.add(allEvtCountsOfVisits.get(iX));
                              doneCountListOfOneMonth.add(allDoneCountsOfVisits.get(iX));
                              pastListOfOneMonth.add(allPastsOfVisits.get(iX));
                          }
                      }
                      monthOfLastVisit = allMonthsOfVisits.get(iX);
                  }
                  listOfVisitFKsByMonth.add(visitListOfOneMonth);
                  listOfVisitNamesByMonth.add(visitNameListOfOneMonth);
                  listOfStartDatesByMonth.add(startDateListOfOneMonth);
                  listOfActualDatesByMonth.add(actualDateListOfOneMonth);
                  listOfWinBeforeNumbersByMonth.add(winBeforeNumberListOfOneMonth);
                  listOfWinBeforeUnitsByMonth.add(winBeforeUnitListOfOneMonth);
                  listOfWinAfterNumbersByMonth.add(winAfterNumberListOfOneMonth);
                  listOfWinAfterUnitsByMonth.add(winAfterUnitListOfOneMonth);
                  listOfEvtCountsByMonth.add(evtCountListOfOneMonth);
                  listOfDoneCountsByMonth.add(doneCountListOfOneMonth);
                  listOfPastsByMonth.add(pastListOfOneMonth);
              }
	      } catch (Exception e) {
              Rlog.fatal("eventinfo","ScheduleDao.getVisitsByMonth: "+ e);
	      } finally {
              try { if (pstmt != null) pstmt.close(); } catch(Exception e) {}
              try { if (conn != null) conn.close(); } catch(Exception e) {}
	      }
	      return;
	  }
	  
	  private void setVisitMonths(String month) {
	      if (!visitMonths.contains(month)) {
	          visitMonths.add(month);
	      }
	  }
	  
	  public ArrayList<String> getVisitMonths() {
	      return visitMonths;
	  }
	  
	  public ArrayList<ArrayList<Integer>> getListOfVisitFKsByMonth() {
	      return listOfVisitFKsByMonth;
	  }
	  
      public ArrayList<ArrayList<String>> getListOfVisitNamesByMonth() {
          return listOfVisitNamesByMonth;
      }
      
      public ArrayList<ArrayList<String>> getListOfStartDatesByMonth() {
          return listOfStartDatesByMonth;
      }
      
      public ArrayList<ArrayList<String>> getListOfActualDatesByMonth() {
          return listOfActualDatesByMonth;
      }
      
      public ArrayList<ArrayList<Integer>> getListOfWinBeforeNumbersByMonth() {
          return listOfWinBeforeNumbersByMonth;
      }
      
      public ArrayList<ArrayList<String>> getListOfWinBeforeUnitsByMonth() {
          return listOfWinBeforeUnitsByMonth;
      }
      
      public ArrayList<ArrayList<Integer>> getListOfWinAfterNumbersByMonth() {
          return listOfWinAfterNumbersByMonth;
      }
      
      public ArrayList<ArrayList<String>> getListOfWinAfterUnitsByMonth() {
          return listOfWinAfterUnitsByMonth;
      }
      
      public ArrayList<ArrayList<Integer>> getListOfEvtCountsByMonth() {
          return listOfEvtCountsByMonth;
      }
      
      public ArrayList<ArrayList<Integer>> getListOfDoneCountsByMonth() {
          return listOfDoneCountsByMonth;
      }
      
      public ArrayList<ArrayList<Integer>> getListOfPastsByMonth() {
          return listOfPastsByMonth;
      }

}