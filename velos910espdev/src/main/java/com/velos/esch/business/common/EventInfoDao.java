/*
 * Classname : EventInfoDao
 *
 * Version information : 1.0
 *
 * Date: 06/24/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/* End of Import Statements */

/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1.Added new attribute advFormStatus
 * and respective getter/setters 2. Modified method getAdverseEvents() (see
 * method's History)
 * *************************END****************************************
 */

public class EventInfoDao extends CommonDAO implements java.io.Serializable {

    ArrayList costIds;

    ArrayList costDescriptions;

    ArrayList costValues;

    ArrayList currencyIds;

    ArrayList eventUserIds;

    ArrayList userTypes;

    ArrayList userIds;

    ArrayList docIds;

    ArrayList docNames;

    ArrayList docDescs;

    ArrayList docTypes;

    ArrayList advIds;

    ArrayList advTypes;

    ArrayList advDescs;

    ArrayList advGrades;

    ArrayList advNames;
// Added by Gopu for Nov.2005 Enhancement #6
	ArrayList attributions;

    ArrayList advStartDates;

    ArrayList advEndDates;

    ArrayList advUsers;

    ArrayList advStatusIds;

    ArrayList advEventNames;

    /**
     * ArrayList of Adverse Events Form Status
     */
    ArrayList advFormStatus;

    ArrayList anIds;

    ArrayList anCodelstDescs;

    ArrayList anCodelstSubTypes;

    ArrayList anTypes;

    ArrayList anuserLists;

    ArrayList anuserIds;

    ArrayList anMobIds;

    ArrayList anFlags;

    ArrayList alnot_globalflags;

    ArrayList crflibNames;

    ArrayList crflibNumbers;

    ArrayList crflibIds;

    ArrayList crflibFormFlags;

    // ** Adverse Event Info fields
    ArrayList advInfoIds;

    ArrayList advInfoAdverseIds;

    ArrayList advInfoCodelstInfoIds;

    ArrayList advInfoValues;

    ArrayList advInfoTypes;

    // ** Adverse Event Notify fields
    ArrayList advNotifyIds;

    ArrayList advNotifyAdverseIds;

    ArrayList advNotifyCodelstNotTypeIds;

    ArrayList advNotifyDates;

    ArrayList advNotifyNotes;

    ArrayList advNotifyValues;

    public EventInfoDao() {
        costIds = new ArrayList();
        costDescriptions = new ArrayList();
        costValues = new ArrayList();
        eventUserIds = new ArrayList();
        userTypes = new ArrayList();
        userIds = new ArrayList();
        docIds = new ArrayList();
        docNames = new ArrayList();
        docDescs = new ArrayList();
        docTypes = new ArrayList();
        currencyIds = new ArrayList();

        advIds = new ArrayList();
        advStatusIds = new ArrayList();
        advTypes = new ArrayList();
        advDescs = new ArrayList();
        advGrades = new ArrayList();
        advNames = new ArrayList();
		// Added by Gopu for Nov.2005 Enhancement #6
		attributions = new ArrayList();
        advStartDates = new ArrayList();


        advEndDates = new ArrayList();
        advUsers = new ArrayList();
        advEventNames = new ArrayList();
        advFormStatus = new ArrayList();

        anIds = new ArrayList();
        anCodelstDescs = new ArrayList();
        anCodelstSubTypes = new ArrayList();
        anTypes = new ArrayList();
        anuserLists = new ArrayList();
        anuserIds = new ArrayList();
        anMobIds = new ArrayList();
        anFlags = new ArrayList();
        alnot_globalflags = new ArrayList();

        crflibNames = new ArrayList();
        crflibNumbers = new ArrayList();
        crflibIds = new ArrayList();
        crflibFormFlags = new ArrayList();

        // ** Adverse Event Info fields
        advInfoIds = new ArrayList();
        advInfoAdverseIds = new ArrayList();
        advInfoCodelstInfoIds = new ArrayList();
        advInfoValues = new ArrayList();
        advInfoTypes = new ArrayList();

        // ** Adverse Event Notify fields
        advNotifyIds = new ArrayList();
        advNotifyAdverseIds = new ArrayList();
        advNotifyCodelstNotTypeIds = new ArrayList();
        advNotifyDates = new ArrayList();
        advNotifyNotes = new ArrayList();
        advNotifyValues = new ArrayList();

    }

    public ArrayList getadvEventNames() {
        return this.advEventNames;
    }

    public void setadvEventNames(ArrayList advEventNames) {
        this.advEventNames = advEventNames;
    }

    public ArrayList getCostIds() {
        return this.costIds;
    }

    public void setCostIds(ArrayList costIds) {
        this.costIds = costIds;
    }

    public ArrayList getCostDescriptions() {
        return this.costDescriptions;
    }

    public void setCostDescriptions(ArrayList costDescriptions) {
        this.costDescriptions = costDescriptions;
    }

    public ArrayList getCostValues() {
        return this.costValues;
    }

    public void setCostValues(ArrayList costValues) {
        this.costValues = costValues;
    }

    public ArrayList getEventUserIds() {
        return this.eventUserIds;
    }

    public void setEventUserIds(ArrayList eventUserIds) {
        this.eventUserIds = eventUserIds;
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    public ArrayList getUserTypes() {
        return this.userTypes;
    }

    public void setUserTypes(ArrayList userTypes) {
        this.userTypes = userTypes;
    }

    public ArrayList getDocIds() {
        return this.docIds;
    }

    public void setDocIds(ArrayList docIds) {
        this.docIds = docIds;
    }

    public ArrayList getDocNames() {
        return this.docNames;
    }

    public void setDocNames(ArrayList docNames) {
        this.docNames = docNames;
    }

    public ArrayList getDocDescs() {
        return this.docDescs;
    }

    public void setDocDescs(ArrayList docDescs) {
        this.docDescs = docDescs;
    }

    public ArrayList getDocTypes() {
        return this.docTypes;
    }

    public void setDocTypes(ArrayList docTypes) {
        this.docTypes = docTypes;
    }

    public ArrayList getCurrencyIds() {
        return this.currencyIds;
    }

    public void setCurrencyIds(ArrayList currencyIds) {
        this.currencyIds = currencyIds;
    }

    // /////////////////////adverse event

    public ArrayList getAdvIds() {
        return this.advIds;
    }

    public void setAdvIds(ArrayList advIds) {
        this.advIds = advIds;
    }

    public ArrayList getAdvTypes() {
        return this.advTypes;
    }

    public void setAdvTypes(ArrayList advTypes) {
        this.advTypes = advTypes;
    }

    public ArrayList getAdvDescs() {
        return this.advDescs;
    }

    public void setAdvDescs(ArrayList advDescs) {
        this.advDescs = advDescs;
    }

    public ArrayList getAdvGrades() {
        return this.advGrades;
    }

    public void setAdvGrades(ArrayList advGrades) {
        this.advGrades = advGrades;
    }

    public ArrayList getAdvNames() {
        return this.advNames;
    }

    public void setAdvNames(ArrayList advNames) {
        this.advNames = advNames;
    }

// Added by Gopu for Nov.2005 Enhancement #6
 public ArrayList getAttributions() {
        return this.attributions;
    }

    public void setAttributions(ArrayList attributions) {
        this.attributions = attributions;
    }

// Added by Gopu for Nov.2005 Enhancement #6
    public ArrayList getAdvStartDates() {
        return this.advStartDates;
    }

    public void setAdvStartDates(ArrayList advStartDates) {
        this.advStartDates = advStartDates;
    }

    public ArrayList getAdvEndDates() {
        return this.advEndDates;
    }

    public void setAdvEndDates(ArrayList advEndDates) {
        this.advEndDates = advEndDates;
    }

    public ArrayList getAdvUsers() {
        return this.advUsers;
    }

    public void setAdvUsers(ArrayList advUsers) {
        this.advUsers = advUsers;
    }

    public ArrayList getAdvStatusIds() {
        return this.advStatusIds;
    }

    public void setAdvStatusIds(ArrayList advStatusIds) {
        this.advStatusIds = advStatusIds;
    }

    // ////////////ALERT NOTIFY

    public ArrayList getAnIds() {
        return this.anIds;
    }

    public void setAnIds(ArrayList anIds) {
        this.anIds = anIds;
    }

    public ArrayList getAnCodelstDescs() {
        return this.anCodelstDescs;
    }

    public void setAnCodelstDescs(ArrayList anCodelstDescs) {
        this.anCodelstDescs = anCodelstDescs;
    }

    public ArrayList getAnCodelstSubTypes() {
        return this.anCodelstSubTypes;
    }

    public void setAnCodelstSubTypes(ArrayList anCodelstSubTypes) {
        this.anCodelstSubTypes = anCodelstSubTypes;
    }

    public ArrayList getAnTypes() {
        return this.anTypes;
    }

    public void setAnTypes(ArrayList anTypes) {
        this.anTypes = anTypes;
    }

    public ArrayList getAnuserLists() {
        return this.anuserLists;
    }

    public void setAnuserLists(ArrayList anuserLists) {
        this.anuserLists = anuserLists;
    }

    public ArrayList getAnuserIds() {
        return this.anuserIds;
    }

    public void setAnuserIds(ArrayList anuserIds) {
        this.anuserIds = anuserIds;
    }

    public ArrayList getAnMobIds() {
        return this.anMobIds;
    }

    public void setAnMobIds(ArrayList anMobIds) {
        this.anMobIds = anMobIds;
    }

    public ArrayList getAnFlags() {
        return this.anFlags;
    }

    public void setAnFlags(ArrayList anFlags) {
        this.anFlags = anFlags;
    }

    public ArrayList getAlnot_globalflags() {
        return this.alnot_globalflags;
    }

    public void setAlnot_globalflags(ArrayList alnot_globalflags) {
        this.alnot_globalflags = alnot_globalflags;
    }

    // ///////////CRFLIB

    public ArrayList getCrflibIds() {
        return this.crflibIds;
    }

    public void setCrflibIds(ArrayList crflibIds) {
        this.crflibIds = crflibIds;
    }

    public ArrayList getCrflibNames() {
        return this.crflibNames;
    }

    public void setCrflibNames(ArrayList crflibNames) {
        this.crflibNames = crflibNames;
    }

    public ArrayList getCrflibNumbers() {
        return this.crflibNumbers;
    }

    public void setCrflibNumbers(ArrayList crflibNumbers) {
        this.crflibNumbers = crflibNumbers;
    }

    public ArrayList getCrflibFormFlags() {
        return this.crflibFormFlags;
    }

    public void setCrflibFormFlags(ArrayList crflibFormFlags) {
        this.crflibFormFlags = crflibFormFlags;
    }

    public ArrayList getAdvInfoIds() {
        return this.advInfoIds;
    }

    public void setAdvInfoIds(ArrayList advInfoIds) {
        this.advInfoIds = advInfoIds;
    }

    public ArrayList getAdvInfoAdverseIds() {
        return this.advInfoAdverseIds;
    }

    public void setAdvInfoAdverseIds(ArrayList advInfoAdverseIds) {
        this.advInfoAdverseIds = advInfoAdverseIds;
    }

    public ArrayList getAdvInfoCodelstInfoIds() {
        return this.advInfoCodelstInfoIds;
    }

    public void setAdvInfoCodelstInfoIds(ArrayList advInfoCodelstInfoIds) {
        this.advInfoCodelstInfoIds = advInfoCodelstInfoIds;
    }

    public ArrayList getAdvInfoValues() {
        return this.advInfoValues;
    }

    public void setAdvInfoValues(ArrayList advInfoValues) {
        this.advInfoValues = advInfoValues;
    }

    public ArrayList getAdvInfoTypes() {
        return this.advInfoTypes;
    }

    public void setAdvInfoTypes(ArrayList advInfoTypes) {
        this.advInfoTypes = advInfoTypes;
    }

    public ArrayList getAdvNotifyIds() {
        return this.advNotifyIds;
    }

    public void setAdvNotifyIds(ArrayList advNotifyIds) {
        this.advNotifyIds = advNotifyIds;
    }

    public ArrayList getAdvNotifyAdverseIds() {
        return this.advNotifyAdverseIds;
    }

    public void setAdvNotifyAdverseIds(ArrayList advNotifyAdverseIds) {
        this.advNotifyAdverseIds = advNotifyAdverseIds;
    }

    public ArrayList getAdvNotifyCodelstNotTypeIds() {
        return this.advNotifyCodelstNotTypeIds;
    }

    public void setAdvNotifyCodelstNotTypeIds(
            ArrayList advNotifyCodelstNotTypeIds) {
        this.advNotifyCodelstNotTypeIds = advNotifyCodelstNotTypeIds;
    }

    public ArrayList getAdvNotifyDates() {
        return this.advNotifyDates;
    }

    public void setAdvNotifyDates(ArrayList advNotifyDates) {
        this.advNotifyDates = advNotifyDates;
    }

    public ArrayList getAdvNotifyNotes() {
        return this.advNotifyNotes;
    }

    public void setAdvNotifyNotes(ArrayList advNotifyNotes) {
        this.advNotifyNotes = advNotifyNotes;
    }

    public ArrayList getAdvNotifyValues() {
        return this.advNotifyValues;
    }

    public void setAdvNotifyValues(ArrayList advNotifyValues) {
        this.advNotifyValues = advNotifyValues;
    }

    // / over loaded functions here

    public void setAdvEventNames(String advEventName) {
        advEventNames.add(advEventName);
    }

    public void setCostIds(String costId) {
        costIds.add(costId);
    }

    public void setCostDescriptions(String costDescription) {
        costDescriptions.add(costDescription);
    }

    public void setCostValues(String costValue) {
        costValues.add(costValue);
    }

    public void setEventUserIds(String eventUserId) {
        eventUserIds.add(eventUserId);
    }

    public void setUserIds(String userId) {
        userIds.add(userId);
    }

    public void setUserTypes(String userType) {
        userTypes.add(userType);
    }

    public void setDocIds(String docId) {
        docIds.add(docId);
    }

    public void setDocNames(String docName) {
        docNames.add(docName);
    }

    public void setDocDescs(String docDesc) {
        docDescs.add(docDesc);
    }

    public void setDocTypes(String docType) {
        docTypes.add(docType);
    }

    public void setCurrencyIds(String currencyId) {
        currencyIds.add(currencyId);
    }

    public void setAdvIds(String advId) {
        advIds.add(advId);
    }

    public void setAdvTypes(String advType) {
        advTypes.add(advType);
    }

    public void setAdvDescs(String advDesc) {
        advDescs.add(advDesc);
    }

    public void setAdvGrades(String advGrade) {
        advGrades.add(advGrade);
    }

    public void setAdvNames(String advName) {
        advNames.add(advName);
    }
// Added by Gopu for Nov.2005 Enhancement #6
    public void setAttributions(String attribution) {
        attributions.add(attribution);
    }

//////tkg
    public void setAdvStartDates(String advStartDate) {
        advStartDates.add(advStartDate);
    }

    public void setAdvEndDates(String advEndDate) {
        advEndDates.add(advEndDate);
    }

    public void setAdvUsers(String advUser) {
        advUsers.add(advUser);
    }

    public void setAdvStatusIds(String advStatusId) {
        advStatusIds.add(advStatusId);
    }

    public void setAnIds(String anId) {
        anIds.add(anId);
    }

    public void setAnCodelstDescs(String anCodelstDesc) {
        anCodelstDescs.add(anCodelstDesc);
    }

    public void setAnCodelstSubTypes(String anCodelstSubType) {
        anCodelstSubTypes.add(anCodelstSubType);
    }

    public void setAnTypes(String anType) {
        anTypes.add(anType);
    }

    public void setAnuserLists(String anuserList) {
        anuserLists.add(anuserList);
    }

    public void setAnuserIds(String anuserId) {
        anuserIds.add(anuserId);
    }

    public void setAnMobIds(String anMobId) {
        anMobIds.add(anMobId);
    }

    public void setAnFlags(String anFlag) {
        anFlags.add(anFlag);
    }

    public void setCrflibIds(String crflibId) {
        crflibIds.add(crflibId);
    }

    public void setCrflibNames(String crflibName) {
        crflibNames.add(crflibName);
    }

    public void setCrflibNumbers(String crflibNumber) {
        crflibNumbers.add(crflibNumber);
    }

    public void setCrflibFormFlags(String crflibFormFlag) {
        crflibFormFlags.add(crflibFormFlag);
    }

    public void setAlnot_globalflags(String alnot_globalflag) {
        alnot_globalflags.add(alnot_globalflag);
    }

    public String getAlnot_globalflags(int index) {
        return (String) this.alnot_globalflags.get(index);
    }

    public void setAdvInfoIds(String advInfoId) {
        advInfoIds.add(advInfoId);
    }

    public void setAdvInfoAdverseIds(String advInfoAdverseId) {
        advInfoAdverseIds.add(advInfoAdverseId);
    }

    public void setAdvInfoCodelstInfoIds(String advInfoCodelstInfoId) {
        advInfoCodelstInfoIds.add(advInfoCodelstInfoId);
    }

    public void setAdvInfoValues(String advInfoValue) {
        advInfoValues.add(advInfoValue);
    }

    public void setAdvInfoTypes(String advInfoType) {
        advInfoTypes.add(advInfoType);
    }

    public void setAdvNotifyIds(String advNotifyId) {
        advNotifyIds.add(advNotifyId);
    }

    public void setAdvNotifyAdverseIds(String advNotifyAdverseId) {
        advNotifyAdverseIds.add(advNotifyAdverseId);
    }

    public void setAdvNotifyCodelstNotTypeIds(String advNotifyCodelstNotTypeId) {
        advNotifyCodelstNotTypeIds.add(advNotifyCodelstNotTypeId);
    }

    public void setAdvNotifyDates(String advNotifyDate) {
        advNotifyDates.add(advNotifyDate);
    }

    public void setAdvNotifyNotes(String advNotifyNote) {
        advNotifyNotes.add(advNotifyNote);
    }

    public void setAdvNotifyValues(String advNotifyValue) {
        advNotifyValues.add(advNotifyValue);
    }

    /**
     * Returns ArrayList of advFormStatus.
     */
    public ArrayList getAdvFormStatus() {
        return advFormStatus;
    }

    /**
     * Sets the ArrayList of advFormStatus.
     *
     * @param advFormStatus
     *            ArrayList of advFormStatus.
     */
    public void setAdvFormStatus(ArrayList advFormStatus) {
        this.advFormStatus = advFormStatus;
    }

    /**
     * appends an advFormStatus id to the arrayList advFormStatus
     *
     * @param advFormStatusid
     *            advFormStatus id
     */
    public void setAdvFormStatus(String advFormStatusid) {
        this.advFormStatus.add(advFormStatusid);
    }

    // Getter and Setter methods

    public void getEventDefInfo(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select a.PK_EVENTCOST as eventcost, "
                    + "b.CODELST_DESC as description, "
                    + "to_char(a.EVENTCOST_VALUE,'9999999999.99') as costvalue, "
                    + "c.CODELST_DESC as currency"
                    + " FROM SCH_EVENTCOST a, "
                    + "sch_codelst b ,"
                    + "sch_codelst c "
                    + "WHERE a.FK_EVENT= ? and a.FK_COST_DESC=b.pk_codelst and a.FK_CURRENCY = c.pk_codelst "
                    + "order by description, currency, costvalue";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCostIds(rs.getString("eventcost"));

                setCostDescriptions(rs.getString("description"));
                setCostValues(rs.getString("costvalue"));

                setCurrencyIds(rs.getString("currency"));

            }

            mysql = "select PK_EVENTUSR, " + "EVENTUSR, " + "EVENTUSR_TYPE "
                    + "FROM SCH_EVENTUSR "
                    + "WHERE FK_EVENT= ? ORDER BY EVENTUSR_TYPE";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println("eventinfo line 1");

                setEventUserIds(rs.getString("PK_EVENTUSR"));

                setUserIds(rs.getString("EVENTUSR"));

                setUserTypes(rs.getString("EVENTUSR_TYPE"));
            }

            mysql = "select pk_docs," + "doc_name, " + "doc_desc, "
                    + "doc_type  " + "from sch_docs " + "where pk_docs "
                    + "in(select pk_docs from sch_eventdoc where fk_event=?) ";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                setDocIds(rs.getString("pk_docs"));

                setDocNames(rs.getString("doc_name"));

                setDocDescs(rs.getString("doc_desc"));

                setDocTypes(rs.getString("doc_type"));

            }
        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventInfoValues EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getEventCrfs(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select PK_CRFLIB, crflib_number,crflib_name, crflib_formflag from "
                    + "sch_crflib where fk_events= ? "
                    + "order by lower(crflib_name) ";
            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCrflibIds(rs.getString("PK_CRFLIB"));

                setCrflibNames(rs.getString("crflib_name"));

                setCrflibNumbers(rs.getString("crflib_number"));

                setCrflibFormFlags(rs.getString("crflib_formflag"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventCrfs EXCEPTION IN FETCHING FROM sch_crflib table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the object with patient adverse events
     *
     * @param fkStudy
     *            study on which the patient is enrolled
     * @param fkPer
     *            patient id
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: 1.populating
     * form_status for adverse events
     * *************************END****************************************
     */
    public void getAdverseEvents(int fkStudy, int fkPer) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
//query modified by gopu for Nov.2005 Enhancement #6
            String mysql = "select a.pk_adveve as advId,"
                    + "b.CODELST_DESC as advType,"
                    + "a.FK_CODLST_AETYPE as advTypeId,"
                    + "	a.AE_DESC as advDesc,"
                    + "    a.AE_GRADE as advGrade,"
                    + "    a.AE_Name  as advName,"
					+ "a.AE_RELATIONSHIP ,"
					+ "(select distinct codelst_desc from sch_codelst where sch_codelst.codelst_type='adve_relation' and "
					+ "sch_codelst.PK_CODELST=a.AE_RELATIONSHIP) as attribution,"
                    + "	a.ae_stdate as advSdate,"
                    + "	a.ae_enddate as advEdate,"
                    + "	c.usr_firstname ||' '|| c.usr_lastname  as advUser, FORM_STATUS  "
                    + "	from sch_adverseve a,"
                    + "	sch_codelst b,"
                    + "	er_user c"
                    + "	where a.FK_CODLST_AETYPE=b.pk_codelst"
                    + " and a.AE_ENTERBY= c.pk_user and fk_study= ? and fk_per= ? order by a.ae_stdate desc,a.pk_adveve desc";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, fkStudy);
            pstmt.setInt(2, fkPer);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setAdvIds(rs.getString("advId"));
                setAdvTypes(rs.getString("advType"));
                setAdvStatusIds(rs.getString("advTypeId"));
                setAdvDescs(rs.getString("advDesc"));
                setAdvGrades(rs.getString("advGrade"));
                setAdvNames(rs.getString("advName"));
				// Added by Gopu for Nov.2005 Enhancement #6
				setAttributions(rs.getString("attribution"));
                setAdvStartDates(rs.getString("advSdate"));
                setAdvEndDates(rs.getString("advEdate"));
                setAdvUsers(rs.getString("advUser"));
                setAdvFormStatus(rs.getString("FORM_STATUS"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventInfoValues EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAlertNotifyValues(int patprotId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select a.pk_alnot as anID ,b.codelst_desc as codelstDesc ,b.codelst_subtyp as codelstsubType ,a.alnot_type as anType, "
                    + "usr_lst(a.alnot_users) as userList,"
                    + "a.alnot_users as userIds, "
                    + "a.alnot_mobeve as  mobileId"
                    + ",a.alnot_flag as anFlag "
                    + "from sch_alertnotify a,"
                    + "sch_codelst b "
                    + "where a.fk_codelst_an=b.pk_codelst "
                    + "and a.fk_patprot= ? order by b.codelst_seq ASC ";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patprotId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setAnIds(rs.getString("anID"));
                setAnCodelstDescs(rs.getString("codelstDesc"));
                setAnCodelstSubTypes(rs.getString("codelstsubType"));
                setAnTypes(rs.getString("anType"));
                setAnuserLists(rs.getString("userList"));
                setAnuserIds(rs.getString("userIds"));
                setAnMobIds(rs.getString("mobileId"));
                setAnFlags(rs.getString("anFlag"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventInfoValues EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAlertNotifyValues(int FK_STUDY, int FK_PROTOCOL, String flag) {

        PreparedStatement pstmt = null;
        String mysql = null;
        Connection conn = null;
        try {
            conn = getConnection();

            if (flag.equals("N")) {
                mysql = "select a.pk_alnot as anID ,b.codelst_desc as codelstDesc ,b.codelst_subtyp as codelstsubType ,a.alnot_type as anType, "
                        + "usr_lst(a.alnot_users) as userList,"
                        + "a.alnot_users as userIds, "
                        + "a.alnot_mobeve as  mobileId"
                        + ",a.alnot_flag as anFlag "
                        + "from sch_alertnotify a,"
                        + "sch_codelst b "
                        + "where a.fk_codelst_an=b.pk_codelst "
                        + "and a.alnot_type='N'"
                        + "and a.FK_STUDY= ? and a.FK_PROTOCOL= ? and FK_PATPROT is null";
            }

            if (flag.equals("A")) {
                mysql = "select a.pk_alnot as anID ,b.codelst_desc as codelstDesc ,b.codelst_subtyp as codelstsubType ,a.alnot_type as anType, "
                        + "usr_lst(a.alnot_users) as userList,"
                        + "a.alnot_users as userIds, "
                        + "a.alnot_mobeve as  mobileId"
                        + ",a.alnot_flag as anFlag "
                        + "from sch_alertnotify a,"
                        + "sch_codelst b "
                        + "where a.fk_codelst_an=b.pk_codelst "
                        + "and a.alnot_type='A'"
                        + "and a.FK_STUDY= ? and a.FK_PROTOCOL= ? and FK_PATPROT is null";
            }

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, FK_STUDY);
            pstmt.setInt(2, FK_PROTOCOL);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setAnIds(rs.getString("anID"));
                setAnCodelstDescs(rs.getString("codelstDesc"));
                setAnCodelstSubTypes(rs.getString("codelstsubType"));
                setAnTypes(rs.getString("anType"));
                setAnuserLists(rs.getString("userList"));
                setAnuserIds(rs.getString("userIds"));
                setAnMobIds(rs.getString("mobileId"));
                setAnFlags(rs.getString("anFlag"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventInfoValues EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAdverseEventNames(int patProtId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select distinct(DESCRIPTION) as evedesc  from sch_events1 where FK_PATPROT= ?";
            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patProtId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setAdvEventNames(rs.getString("evedesc"));

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventinfo",
                            "EventInfoDao.getAdverseEventNames EXCEPTION IN FETCHING FROM Sch_Events1 table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    } // end of method

    public void getStudyAlertNotifyValues(int studyid, int protocolid) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select a.pk_alnot as anID ,a.alnot_type as anType, "
                    + "usr_lst(a.alnot_users) as userList,"
                    + "a.alnot_users as userIds, "
                    + "a.alnot_mobeve as  mobileId"
                    + ",a.alnot_flag as anFlag, a.ALNOT_GLOBALFLAG "
                    + "from sch_alertnotify a "
                    + "where a.fk_study= "
                    + studyid
                    + " and a.fk_protocol = "
                    + protocolid
                    + " and fk_patprot is null and ALNOT_TYPE = 'G'";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setAnIds(rs.getString("anID"));
                setAnTypes(rs.getString("anType"));
                setAnuserLists(rs.getString("userList"));
                setAnuserIds(rs.getString("userIds"));
                setAnMobIds(rs.getString("mobileId"));
                setAnFlags(rs.getString("anFlag"));
                setAlnot_globalflags(rs.getString("ALNOT_GLOBALFLAG"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getEventInfoValues EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getANGlobalFlag(int studyid, int protocolid) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String anGlobalFlag = "";
        try {
            conn = getConnection();

            String mysql = "Select ALNOT_GLOBALFLAG from SCH_ALERTNOTIFY "

            + "where fk_study= " + studyid + " and fk_protocol = " + protocolid
                    + " and fk_patprot is null and ALNOT_TYPE = 'G'";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            // pstmt.setInt(1,studyid);
            // pstmt.setInt(2,protocolid);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                anGlobalFlag = rs.getString("ALNOT_GLOBALFLAG");

            }

        }

        catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getANGlobalFlag EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
            return anGlobalFlag;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        return anGlobalFlag;

    }

    public void getAdvInfo(int fkAdverseEvent, String flag) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select a.PK_ADVINFO as advInfoId,"
                    + "a.FK_ADVERSE as advId,"
                    + "b.CODELST_DESC as codeLstDesc,"
                    + "b.CODELST_SEQ as codeLstSeq,"
                    + "	a.FK_CODELST_INFO as advInfoCodeLst,"
                    + "	a.ADVINFO_VALUE as advInfoValue,"
                    + "	a.ADVINFO_TYPE as advInfoType"

                    + "	from sch_advinfo a," + "	sch_codelst b"

                    + "	where a.FK_CODELST_INFO=b.pk_codelst"
                    + " and a.FK_ADVERSE= ? and a.ADVINFO_TYPE = ? "
                    + "order by codelst_seq";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, fkAdverseEvent);
            pstmt.setString(2, flag);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setAdvInfoIds(rs.getString("advInfoId"));

                setAdvInfoAdverseIds(rs.getString("advId"));

                setAdvInfoCodelstInfoIds(rs.getString("advInfoCodeLst"));

                setAdvInfoValues(rs.getString("advInfoValue"));

                setAdvInfoTypes(rs.getString("advInfoType"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getAdvInfo EXCEPTION IN FETCHING FROM EventInfo table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAdvNotify(int fkAdverseEvent) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select a.PK_ADVNOTIFY as advNotifyId,"
                    + "a.FK_ADVERSEVE as advId,"
                    + "b.CODELST_DESC as codeLstDesc,"
                    + "	a.FK_CODELST_NOT_TYPE as advNotifyCodeLst,"
                    + "TO_CHAR(a.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as advNotifyDate,"
                    + "	a.ADVNOTIFY_NOTES as advNotifyNotes,"
                    + "	a.ADVNOTIFY_VALUE as advNotifyValue"

                    + "	from sch_advnotify a," + "	sch_codelst b"

                    + "	where a.FK_CODELST_NOT_TYPE=b.pk_codelst"
                    + " and a.FK_ADVERSEVE= ?  order by advNotifyCodeLst";

            Rlog.debug("eventinfo", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, fkAdverseEvent);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setAdvNotifyIds(rs.getString("advNotifyId"));

                setAdvNotifyAdverseIds(rs.getString("advId"));

                setAdvNotifyCodelstNotTypeIds(rs.getString("advNotifyCodeLst"));

                setAdvNotifyDates(rs.getString("advNotifyDate"));

                setAdvNotifyNotes(rs.getString("advNotifyNotes"));

                setAdvNotifyValues(rs.getString("advNotifyValue"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventinfo",
                    "EventInfoDao.getAdvNotify EXCEPTION IN FETCHING FROM sch_advnotify table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of class

}