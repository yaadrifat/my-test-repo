/*
 * Classname : EventResourceDao
 *
 * Version information : 1.0
 *
 * Date: 02/18/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/* End of Import Statements */

public class EventResourceDao extends CommonDAO implements java.io.Serializable {

    ArrayList evtResTrackIds;
	ArrayList evtStats;
    ArrayList evtRoles; //imp note: same as the pk_codelst retrieved...
    ArrayList evtTrackDurations;
    ArrayList resourceNames;
    ArrayList selectedIds;

    public EventResourceDao() {

    	evtResTrackIds = new ArrayList();
    	evtStats = new ArrayList();
    	evtRoles = new ArrayList();
    	evtTrackDurations = new ArrayList();
    	resourceNames = new ArrayList();
    	selectedIds = new ArrayList();
    }



    public ArrayList getEvtResTrackIds() {
        return this.evtResTrackIds;
    }

    public void setEvtResTrackIds(ArrayList evtResTrackIds) {
        this.evtResTrackIds = evtResTrackIds;
    }

    public void setEvtResTrackIds(String evtResTrackId) {
    	evtResTrackIds.add(evtResTrackId);
    }

    public ArrayList getEvtStats() {
        return this.evtStats;
    }

    public void setEvtStats(ArrayList evtStats) {
        this.evtStats = evtStats;
    }

    public void setEvtStats(String evtStat) {
    	evtStats.add(evtStat);
    }


    public ArrayList getEvtRoles() {
        return this.evtRoles;
    }

    public void setEvtRoles(ArrayList evtRoles) {
        this.evtRoles = evtRoles;
    }

    public void setEvtRoles(String evtRole) {
    	evtRoles.add(evtRole);
    }

    public ArrayList getEvtTrackDurations() {
        return this.evtTrackDurations;
    }

    public void setEvtTrackDurations(ArrayList evtTrackDurations) {
        this.evtTrackDurations = evtTrackDurations;
    }

    public void setEvtTrackDurations(String evtTrackDuration) {
    	evtTrackDurations.add(evtTrackDuration);
    }


    public ArrayList getResourceNames() {
        return this.resourceNames;
    }

    public void setResourceNames(ArrayList resourceNames) {
        this.resourceNames = resourceNames;
    }

    public void setResourceNames(String resourceName) {
        this.resourceNames.add(resourceName);
    }

    // end of Getter and Setter methods


        public void getAllRolesForEvent(int eventId) {
        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();

            sb.append(" select pk_codelst, codelst_desc, PK_EVRES_TRACK, FK_EVENTSTAT,EVRES_TRACK_DURATION ");
            sb.append(" from  er_codelst , SCH_EVRES_TRACK ");
            sb.append(" where 	FK_EVENTSTAT= (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where FK_EVENT = ? )");
            sb.append(" and pk_codelst = FK_CODELST_ROLE and codelst_type = 'role' ");
            sb.append(" union ");
            sb.append(" select pk_codelst, codelst_desc, 0, 0, eventusr_duration  ");
            sb.append(" from  er_codelst , sch_eventusr ");
            sb.append(" where 	fk_event = (select fk_assoc from sch_events1 where EVENT_ID = ? ) and eventusr_type = 'R'");
            sb.append(" and pk_codelst = eventusr and codelst_type = 'role' ");
            sb.append(" and pk_codelst not in (select pk_codelst from  er_codelst , SCH_EVRES_TRACK ");
            sb.append(" where 	FK_EVENTSTAT= (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where FK_EVENT = ? )");
            sb.append(" and pk_codelst = FK_CODELST_ROLE and codelst_type = 'role') ");

            pstmt = conn.prepareStatement(sb.toString());

            pstmt.setInt(1, eventId);
            pstmt.setInt(2, eventId);
            pstmt.setInt(3, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	setEvtRoles(rs.getString("pk_codelst"));
            	setResourceNames(rs.getString("codelst_desc"));
            	setEvtResTrackIds(rs.getString("PK_EVRES_TRACK"));
            	setEvtStats(rs.getString("FK_EVENTSTAT"));
            	setEvtTrackDurations(rs.getString("EVRES_TRACK_DURATION"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventresource",
                    "getAllRolesForEvent - EXCEPTION IN FETCHING FROM event resources "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }




//Get the pk of the event_status

    public int getStatusIdOfTheEvent(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int eventStatusId= 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select max(PK_EVENTSTAT) event_status_id from SCH_EVENTSTAT where FK_EVENT = ? ");
            pstmt.setInt(1, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	eventStatusId =  rs.getInt("event_status_id");

            }
            return eventStatusId;

        } catch (Exception ex) {
            Rlog.fatal("eventuser",
                    "EventUserDao.getStatusIdOfTheEvent EXCEPTION IN FETCHING FROM SCH_EVENTSTAT table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return eventStatusId;

    }

}