/*
 * Classname : AdvNotifyAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 12/11/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advNotifyAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.advNotify.impl.AdvNotifyBean;
import com.velos.esch.service.advNotifyAgent.AdvNotifyAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Stateless session EJB acting as a facade for the CMP entity bean
 * AdvNotifyAgentBean and other adverse event notification services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Service layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
@Stateless
public class AdvNotifyAgentBean implements AdvNotifyAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /*
     * * returns an AdvNotifyStateKeeper object with Adverse Event Notification
     * details. <p> Calls getAdvNotifyDetails() on Entity Bean AdvNotifyBean and
     * creates and returns an AdvNotifyStateKeeper. containing all the details
     * of the AdvNotifyBean<br> </p>
     * 
     * @param advNotifyId the adverse event notification id
     * 
     * @see AdvNotifyStateKeeper
     */
    public AdvNotifyBean getAdvNotifyDetails(int advNotifyId) {

        try {

            return (AdvNotifyBean) em.find(AdvNotifyBean.class, new Integer(
                    advNotifyId));

        } catch (Exception e) {
            System.out
                    .print("Error in getAdvNotifyDetails() in AdvNotifyAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setAdvNotifyDetails() on Entity Bean AdvNotifyBean and creates new
     * Adverse Event Notification.
     * 
     * @param edsk
     *            AdvNotifyStateKeeper object with Adverse Event Notification
     *            details
     * @return success flag
     *         <ul>
     *         <li> id of new adverse event notification</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see AdvNotifyStateKeeper
     */

    public int setAdvNotifyDetails(AdvNotifyBean edsk) {
        try {
            em.merge(edsk);
            return (edsk.getAdvNotifyId());
        } catch (Exception e) {
            Rlog.fatal("advNotify",
                    "Exception in AdvNotifyAgentBean.setAdvNotifyDetails " + e);
        }
        return 0;
    }

    /**
     * Calls updateAdvNotify() on Entity Bean AdvNotifyBean and updates an
     * Adverse Notify Info.
     * 
     * @param edsk
     *            AdvNotifyStateKeeper object with Adverse Event Notify details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see AdvNotifyStateKeeper
     * 
     */
    public int updateAdvNotify(AdvNotifyBean edsk) {

        AdvNotifyBean retrieved = null; // AdvNotify Entity Bean Remote Object
        int output;

        try {

            retrieved = (AdvNotifyBean) em.find(AdvNotifyBean.class,
                    new Integer(edsk.getAdvNotifyId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateAdvNotify(edsk);

        } catch (Exception e) {
            Rlog.debug("advNotify",
                    "Error in updateAdvNotify in AdvNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

}// end of class
