/* 
 * Classname			BudgetUsersAgentBean.class
 * 
 * Version information
 *
 * Date					03/21/2002
 *
 * Author 				Sonika Talwar
 * Copyright notice
 */

package com.velos.esch.service.budgetUsersAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.budgetUsers.impl.BudgetUsersBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.esch.service.budgetUsersAgent.BudgetUsersAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * BudgetUserBean.
 * 
 */
@Stateless
public class BudgetUsersAgentBean implements BudgetUsersAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /**
     * Calls getBudgetUsersDetails() on BudgetUser Entity Bean. Looks up on the
     * BudgetUser Id parameter passed in and constructs and returns a Budget
     * state holder. containing all the summary details of the Budget<br>
     * 
     * @param budgetUserId
     *            the Budget id
     * @ee BudgetBean
     */
    public BudgetUsersBean getBudgetUsersDetails(int bgtUsersId) {

        try {
            return (BudgetUsersBean) em.find(BudgetUsersBean.class,
                    new Integer(bgtUsersId));
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in getBudgetUsersDetails() in BudgetUsersAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setBudgetUsersDetails() on Budget Users Entity Bean
     * BudgetBean.Creates a new Budget User with the values in the StateKeeper
     * object.
     * 
     * @param budgetUsers
     *            state keeper containing budget users attributes for the new
     *            Budget Users.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setBudgetUsersDetails(BudgetUsersBean busk) {
        try {
            ArrayList userIds = busk.getUserIds();

            for (int i = 0; i < userIds.size(); i++) {
                String userId = (String) userIds.get(i);
                busk.setBgtUsers(userId);
                em.merge(busk);
            }

            return (busk.getBgtUsersId());
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in setBudgetUsersDetails() in BudgetUsersAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateBudgetUsers(BudgetUsersBean busk) {

        BudgetUsersBean retrieved = null; // Budget Entity Bean Remote Object
        int output;

        try {

            retrieved = (BudgetUsersBean) em.find(BudgetUsersBean.class,
                    new Integer(busk.getBgtUsersId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateBudgetUsers(busk);

        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in updateBudgetUsers in BudgetUsersAgentBean " + e);
            return -2;
        }
        return output;
    }

    public BudgetUsersDao getUsersNotInBudget(int accountId, int budgetId,
            String lname, String fname, String role, String organization,
            String userType) {
        try {
            BudgetUsersDao bgtUsersDao = new BudgetUsersDao();
            bgtUsersDao.getUsrNotInBudget(accountId, budgetId, lname, fname,
                    role, organization, userType);
            return bgtUsersDao;
        }

        catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in getUsersNotInBudget in BudgetUsersAgentBean" + e);
        }
        return null;
    }

    public BudgetUsersDao getUsersInBudget(int budgetId) {
        try {
            BudgetUsersDao bgtUsersDao = new BudgetUsersDao();
            bgtUsersDao.getUsrInBudget(budgetId);
            return bgtUsersDao;
        }

        catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in getUsersInBudget in BudgetUsersAgentBean" + e);
        }
        return null;
    }

    public String getBudgetUserRight(int budgetId, int userId) {
        String userRight;
        try {
            BudgetUsersDao bgtUsersDao = new BudgetUsersDao();
            userRight = bgtUsersDao.getBudgetUserRight(budgetId, userId);
            return userRight;
        }

        catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in getBudgetUserRight in BudgetUsersAgentBean" + e);
        }
        return null;

    }

    /**
     * Delete user from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */
    public int removeBudgetUser(int bgtUserId) {
        BudgetUsersBean budgetUsersR = null; // Budget Users Entity Bean
        // Remote Object
        int ret = 0;
        try {

            budgetUsersR = (BudgetUsersBean) em.find(BudgetUsersBean.class,
                    new Integer(bgtUserId));
            em.remove(budgetUsersR);
            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("BudgetUser", "EXCEPTION IN REMOVING BUDGET USER" + e);
            return -2;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeBudgetUser(int bgtUserId,Hashtable<String, String> auditInfo) {
        
    	BudgetUsersBean budgetUsersR = null; // Budget Users Entity Bean
    	AuditBean audit=null;
        
    	try {
    		
    		String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("SCH_BGTUSERS","esch","PK_BGTUSERS="+bgtUserId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("SCH_BGTUSERS",String.valueOf(bgtUserId),rid,userID,currdate,app_module,ipAdd,reason);
        	budgetUsersR = (BudgetUsersBean) em.find(BudgetUsersBean.class,
                    new Integer(bgtUserId));
            
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(budgetUsersR);
            return 0;
        }

        catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            Rlog.fatal("BudgetUser", "EXCEPTION IN REMOVING BUDGET USER removeBudgetUser(int bgtUserId,Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }

}// end of class
