/*
 * Classname : CrflibAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 01/31/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.service.crflibAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.common.CrflibDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.crflib.impl.CrflibBean;
import com.velos.esch.service.crflibAgent.CrflibAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP CrflibBean.<br>
 * <br>
 * 
 */
@Stateless
public class CrflibAgentBean implements CrflibAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getCrflibDetails() on Crflib Entity Bean CrflibBean. Looks up on
     * the Crflib id parameter passed in and constructs and returns a Crflib
     * state holder. containing all the summary details of the Crflib<br>
     * 
     * @param crflibId
     *            the Crflib id
     * @ee CrflibBean
     */
    public CrflibBean getCrflibDetails(int crflibId) {

        try {
            return (CrflibBean) em
                    .find(CrflibBean.class, new Integer(crflibId));
        } catch (Exception e) {
            System.out.print("Error in getCrflibDetails() in CrflibAgentBean"
                    + e);
            return null;
        }

    }

    /**
     * Calls setCrflibDetails() on Crflib Entity Bean crflibBean.Creates a new
     * Crflib with the values in the StateKeeper object.
     * 
     * @param crflib
     *            a crflib state keeper containing crflib attributes for the new
     *            Crflib.
     * @return int - 0 if successful; -2 for Exception;
     */
    /* Modified by Sonia Abrol, 05/30/06, EJB migration issue*/
    public int setCrflibDetails(CrflibBean edsk) {
        try {
 	
        	CrflibBean  cbean = new CrflibBean ();
        	cbean.updateCrflib(edsk);

            em.persist(cbean);
            return (cbean.getCrflibId());

            
        } catch (Exception e) {
            System.out.print("Error in setCrflibDetails() in CrflibAgentBean "
                    + e);
        }
        return 0;
    }

    public int updateCrflib(CrflibBean edsk) {

        CrflibBean retrieved = null; // Crflib Entity Bean Remote Object
        int output;

        try {

            retrieved = (CrflibBean) em.find(CrflibBean.class, edsk
                    .getCrflibId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCrflib(edsk);

        } catch (Exception e) {
            Rlog
                    .debug("crflib", "Error in updateCrflib in CrflibAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

 // Overloaded for INF-18183 ::: Akshi
    public int removeCrflib(int crflibId,Hashtable<String, String> args) {

        CrflibBean retrieved = null; // Crflib Entity Bean Remote Object
        int output = 0;

        try {
        	
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("SCH_CRFLIB","esch","PK_CRFLIB="+crflibId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_CRFLIB",String.valueOf(crflibId),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	
            retrieved = (CrflibBean) em.find(CrflibBean.class, new Integer(
                    crflibId));
            em.remove(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("crflib",
                    "Error in removeCrflibChild  in CrflibAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int removeCrflib(int crflibId) {

        CrflibBean retrieved = null; // Crflib Entity Bean Remote Object
        int output = 0;

        try {
        	
        	        	
            retrieved = (CrflibBean) em.find(CrflibBean.class, new Integer(
                    crflibId));
            em.remove(retrieved);
        } catch (Exception e) {
        	Rlog.debug("crflib",
                    "Error in removeCrflibChild  in CrflibAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    
    
    public EventInfoDao getEventCrfs(int eventId) {
        try {
            Rlog.debug("crflib",
                    "In getEventCrfs in CrflibAgentBean line number 0");
            EventInfoDao eventinfoDao = new EventInfoDao();
            Rlog.debug("crflib",
                    "In getEventCrfs in CrflibAgentBean line number 1");
            eventinfoDao.getEventCrfs(eventId);
            Rlog.debug("crflib",
                    "In getEventCrfs in CrflibAgentBean line number 2");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("crflib",
                    "Exception In getEventCrfs in CrflibAgentBean " + e);
        }
        return null;

    }

    /**
     * Unsert multiple crf names and numbers to the sch_crflib
     * 
     * @param CrfLibDao
     * 
     */
    // SV, 10/21/04, changed the return value to crfIdsdao, essentially a new
    // crflibdao filled with crflibIds created in this step.
    // This will be returned to client, which will then try to propagate the
    // changes to crflibIds to other events. cal-enh-04, 05
    public CrflibDao insertCrfNamesNums(CrflibDao crflibDao) {

        // SV, 10/21/04, changes made for propagating the event crflib changes.
        // This is yet again different from how other event
        // related records are stored. Here the dao is filled in with the
        // records from screen and the method here in agent actually
        // loops on the records in dao and creates bean for each.
        // The change is to return the list of id's in a returned dao object.
        // Once this list is returned we can propagate for each record in list.

        try {
            Rlog.debug("crflib", "crflibAgentBean.insertCrfNamesNums starting");
            int rows = crflibDao.getRows();
            Rlog.debug("crflib", "getrows in agentbean" + crflibDao.getRows());
            int crflibId = 0; // SV, 10/21/04, changed ret to crflibId
            CrflibDao crfIdsDao = new CrflibDao();
            for (int i = 0; i < rows; i++) {
                CrflibBean crflibsk = new CrflibBean();

                crflibsk.setEventsId((String) crflibDao.getEventsId(i));
                Rlog.debug("crflib", "eventsid in agentbean"
                        + crflibDao.getEventsId(i));

                crflibsk.setCrflibName((String) crflibDao.getCrflibName(i));
                Rlog.debug("crflib", "crflibName in agentbean"
                        + crflibDao.getCrflibName(i));

                crflibsk.setCrflibNumber((String) crflibDao.getCrflibNumber(i));
                Rlog.debug("crflib", "crflibNumber in agentbean"
                        + crflibDao.getCrflibNumber(i));

                crflibsk.setCrflibFlag((String) crflibDao.getCrflibFlag(i));
                Rlog.debug("crflib", "crflibFlag in agentbean"
                        + crflibDao.getCrflibFlag(i));

                crflibsk.setCreator((String) crflibDao.getCreator(i));
                Rlog.debug("crflib", "creator in agentbean"
                        + crflibDao.getCreator(i));

                crflibsk.setIpAdd((String) crflibDao.getIpAdd(i));
                Rlog.debug("crflib", "ipAdd in agentbean"
                        + crflibDao.getIpAdd(i));
                crflibId = setCrflibDetails(crflibsk); // SV, 10/21
                crfIdsDao.setCrflibId(crflibId); // SV, 10/21/04
                Rlog.debug("crflib",
                        "CrflibAgentBean.INSIDE ROWS AFTER SETTING###");
            }

            return crfIdsDao;
        } catch (Exception e) {
            Rlog.fatal("crflib",
                    "Exception In insertCrfNameNums in CrflibAgentBean " + e);
            return new CrflibDao();
        }

    }

}// end of class

