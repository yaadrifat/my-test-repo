/*
 * Classname			SubCostItemVisitAgentBean
 * 
 * Version information : 1.0
 *
 * Date				: 01/21/2011
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.esch.service.subCostItemVisitAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.esch.business.subCostItemVisit.impl.SubCostItemVisitBean;
import com.velos.esch.service.subCostItemVisitAgent.SubCostItemVisitAgentRObj;
import com.velos.esch.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Manimaran
 * @version 1.0, 01/21/2011
 * @ejbRemote SubCostItemVisitAgentRObj
 */

@Stateless
public class SubCostItemVisitAgentBean implements SubCostItemVisitAgentRObj {
	@PersistenceContext(unitName = "esch")
    protected EntityManager em;
	
	private static final long serialVersionUID = 3257007661496480563L;
	
	public SubCostItemVisitBean getSubCostItemVisitDetails(int subCostItemVisitId) {
        try {

            return (SubCostItemVisitBean) em.find(SubCostItemVisitBean.class, new Integer(
            		subCostItemVisitId));

        } catch (Exception e) {
            System.out
                    .print("Error in getSubCostItemVisitDetails() in SubCostItemVisitAgentBean"
                            + e);
            return null;
        }
	}
	
	
	public int setSubCostItemVisitDetails(SubCostItemVisitBean scsk) {
        try {

            SubCostItemVisitBean subcost = new SubCostItemVisitBean();

            subcost.updateSubCostItemVisit(scsk);

            em.persist(subcost);
            return (subcost.getSubCostItemVisitId());

        }

        catch (Exception e) {
            System.out
                    .print("Error in setSubCostItemVisitDetails() in SubCostItemVisitAgentBean "
                            + e);
        }
        return 0;
	}
	
	public int updateSubCostItemVisit(SubCostItemVisitBean scsk) {

		 SubCostItemVisitBean retrieved = null; 
	     int output;

	        try {

	            retrieved = (SubCostItemVisitBean) em.find(SubCostItemVisitBean.class,
	                    new Integer(scsk.getSubCostItemVisitId()));

	            if (retrieved == null) {
	                return -2;
	            }
	            output = retrieved.updateSubCostItemVisit(scsk);
	            em.merge(retrieved);

	        } catch (Exception e) {
	            Rlog.debug("subjectcost",
	                    "Error in updateSubCostItemVisit in SubCostItemVisitAgentBean" + e);
	            return -2;
	        }
	        return output;
	}
	
	public Integer getExistingItemsByFkItemAndFkVisit(Integer fkItem, Integer fkVisit) {
        Integer output = 0;
        Query query = em.createNamedQuery("findSubCostItemByFkItemAndFkVisit");
        query.setParameter("fkItem", fkItem);
        query.setParameter("fkVisit", fkVisit);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubCostItemVisitBean bean = (SubCostItemVisitBean)list.get(0);
        output = bean.getSubCostItemVisitId();
        return output;
    }

}
