/*
 * Classname : ProtVisitAgentBean
 *
 * Version information: 1.0
 *
 * Date: 09/22/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.service.protvisitAgent.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONObject;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.web.protvisit.ProtVisitResponse;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * ProtVisitBean.<br>
 * <br>
 *
 */
@Stateless
public class ProtVisitAgentBean implements ProtVisitAgentRObj {
	@Resource private SessionContext context;
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     *
     */
    private static final long serialVersionUID = 3257852060673454384L;

    /**
     * Calls getProtocolVisits() on ProtVisitDao;
     *
     * @param protocol
     *            Id
     * @return ProtVisitDao object with details of all Visits for a protcol
     *         calendar.
     * @ee EventdefDao
     */

    public ProtVisitDao getProtocolVisits(int protocol_id) {
        try {
            Rlog
                    .debug("protvisit",
                            "In getProtocolVisits in ProtocolVisitAgentBean line number 0");
            ProtVisitDao protVisitDao = new ProtVisitDao();

            protVisitDao.getProtocolVisits(protocol_id);

            return protVisitDao;
        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getProtocolVisits in ProtVisitAgentBean "
                                    + e);
        }
        return null;

    }

    /**JM: 16Apr2008
    *
    * @param protocol_id, search
    */
    public ProtVisitDao getProtocolVisits(int protocol_id, String search) {
        try {
            Rlog
                    .debug("protvisit",
                            "In getProtocolVisits in ProtocolVisitAgentBean line number 0");
            ProtVisitDao protVisitDao = new ProtVisitDao();

            protVisitDao.getProtocolVisits(protocol_id, search);

            return protVisitDao;
        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getProtocolVisits in ProtVisitAgentBean "
                                    + e);
        }
        return null;

    }




    /**
     * Calls getProtVisitDetails() on ProtVisit Entity Bean ProtVisitBean. Looks
     * up on the ProtVisit id parameter passed in and constructs and returns a
     * ProtVisit state holder. containing all the summary details of the
     * ProtVisit<br>
     *
     * @param ProtVisitId
     *            the ProtVisit id
     * @ee ProtVisitBean
     */
    public ProtVisitBean getProtVisitDetails(int visit_id) {

        try {
            return (ProtVisitBean) em.find(ProtVisitBean.class, new Integer(
                    visit_id));
        }

        catch (Exception e) {
            System.out
                    .print("Error in getProtVisitDetails() in ProtVisitAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setProtVisitDetails() on ProtVisit Entity Bean
     * ProtVisitBean.Creates a new ProtVisit with the values in the StateKeeper
     * object.
     *
     * @param ProtVisit
     *            a ProtVisit state keeper containing ProtVisit attributes for
     *            the new ProtVisit.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setProtVisitDetails(ProtVisitBean vdsk) {
        int status = 0;
        int dispVal =0;
        ProtVisitBean pv = new ProtVisitBean();

        try {
            Rlog.debug("protvisit", "entering ProtVisitAgentBean.");
            ProtVisitDao ProtVisitdao = new ProtVisitDao();

            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id=" + vdsk.getProtocol_id()
                    + "displacement=" + vdsk.getDisplacement());
            
            //KM
            if(vdsk.getDisplacement() == null)
            	dispVal = 0;
            else
            	dispVal = StringUtil.stringToNum(vdsk.getDisplacement());
            //D-FIN-25-DAY0 BK JAN-23-2011
            int result = ProtVisitdao.ValidateVisit(vdsk.getProtocol_id(), -1,
                    vdsk.getName(), dispVal,vdsk.getDays(),vdsk.getWeeks(),vdsk.getMonths()); // SV, 10/24/04,
            // turned
            // duplicate
            // name check.
            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id=" + vdsk.getProtocol_id()
                    + "displacement=" + vdsk.getDisplacement()
                    + "protvisitAgentBean:count=" + result);
            if (result < 0) {
                return result;
            }

            //em.merge(vdsk);
            em.persist(vdsk);
            
            
            return (vdsk.getVisit_id());
        } catch (Exception e) {
            Rlog.fatal("protvisit", "In the remote exception :" + e);
            e.printStackTrace();
            return -1;

        }
    }

    public int updateProtVisit(ProtVisitBean vdsk) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output;
        int dispVal =0;

        try {
        //BK,May/13/2011,Fix #6227
        	Query query = em.createNamedQuery("findInsertAfterVisits");        	
        	query.setParameter(1, vdsk.visit_id);
        	Number setAfterDependendent = (Number) query.getSingleResult(); 
        	if( setAfterDependendent.intValue() > 0 && "1".equals(vdsk.intervalFlag)){
        		return -5;
        	}
            ProtVisitDao ProtVisitdao = new ProtVisitDao();
            //KM
            if(vdsk.getDisplacement() == null)
            	dispVal = 0;
            else
            	dispVal = StringUtil.stringToNum(vdsk.getDisplacement());
            //D-FIN-25-DAY0 BK JAN-23-2011
            int pId = vdsk.getProtocol_id();
            int vId = vdsk.getVisit_id();
            String Name = vdsk.getName();
            Integer day = vdsk.getDays();
            int weeks = vdsk.getWeeks();
            int months =vdsk.getMonths();
            
            int result = ProtVisitdao.ValidateVisit(pId, vId,Name,dispVal,day,weeks,months); // SV,
            // 10/24/04,
            // turned
            // duplicate
            // name
            // check.

            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id" + vdsk.getProtocol_id()
                    + "protvisitAgentBean:count=" + result);
            if (result < 0) {
                return result;
            }

            retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(vdsk.getVisit_id()));

            if (retrieved == null) {
                return -4;
            }
            output = retrieved.updateProtVisit(vdsk);
        } catch (Exception e) {
            Rlog.fatal("protvisit",
                    "Error in updateProtVisit in ProtVisitAgentBean" + e);
            return -4;
        }
        return output;
    }


    public int removeProtVisit(int visit_id) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output = 0;

        try {
        	
        	retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(visit_id));
            em.remove(retrieved);
        } catch (Exception e) {
    
            Rlog.fatal("protvisit",
                    "Error in removeProtVisit  in ProtVisitAgentBean" + e);
            return -2;
        }
        return output;
    }

    // removeProtVisit() Overloaded for INF-18183 ::: Raviesh
    public int removeProtVisit(int visit_id,Hashtable<String, String> args) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output = 0;

        try {
        	
        	AuditBean audit=null; //Audit Bean for AppDelete
            String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
            String getRidValue= AuditUtils.getRidValue("SCH_PROTOCOL_VISIT","esch","pk_protocol_visit="+visit_id);/*Fetches the RID/PK_VALUE*/ 
        	audit = new AuditBean("SCH_PROTOCOL_VISIT",String.valueOf(visit_id),getRidValue,
        			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
           
        	
        	retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(visit_id));
            em.remove(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("protvisit",
                    "Error in removeProtVisit  in ProtVisitAgentBean" + e);
            return -2;
        }
        return output;
    }

    /** This method is not used anymore. Bug #6631
     * This method updates the displacement for all no interval visits out
     * @param protocol_visit_id
     * @return  -1 if unsuccessful, new protocol duration if successful
     */
    public int pushNoIntervalVisitsOut(int protocol_visit_id, String ip, int userId)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.pushNoIntervalVisitsOut(protocol_visit_id, ip, userId);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In pushNoIntervalVisitsOut in ProtVisitAgentBean "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    
    /** This method updates the displacement for all child records based on parent
     * @param protocol_visit_id
     * @return  -1 if unsuccessful, new protocol duration if successful
     */
    public int generateRipple(int protocol_visit_id,String calledFrom)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.generateRipple(protocol_visit_id,calledFrom);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In generateRipple in ProtVisitAgentBean "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    public int getMaxVisitNo(int protocol_id)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.getMaxVisitNo(protocol_id);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getmaxvisitNo "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    

    // Added by Manimaran for the Enhancement #C8.3
    public int copyVisit(int protocolId,int frmVisitId, int visitId,String calledFrom, String usr, String ipAdd) {
        ProtVisitDao protDao = new ProtVisitDao();
        int ret = 0;
        try {
            Rlog.debug("protvisit", "ProtVisitAgentBean:copyVisit- 0");
            ret = protDao.copyVisit(protocolId,frmVisitId,visitId,calledFrom,usr,ipAdd);//KM-3447

        } catch (Exception e) {
            Rlog.fatal("protvisit", "ProtVisitAgentBean:Exception In copyVisit-"
                    + e);
            return -1;
        }
 
        return ret;

    }

    public ScheduleDao getVisitsByMonth(int patProdId, HashMap paramMap) {
        ScheduleDao schDao = new ScheduleDao();
        schDao.getVisitsByMonth(patProdId, paramMap);
        return schDao;
    }
    
  //BK,May/20/2011,Fixed #6014
    /**
     * This method validates the duration against day zero visits in the calendar.
     * @param duration The Calendar duration
     * @param protocolId Calendar Identifier
     * @param checkDayZero flag for day zero check.
     *        1-Check for day zero visit.
     *        2-Check for last day visit.
     * @param visitId visit Identifier
     * @return integer 
     */
    public int validateDayZeroVisit(int duration,int protocolId,int checkDayZero,int visitId) {
    	ProtVisitBean aProtVisitBean = new ProtVisitBean();
    	//aProtVisitBean.setVisit_id(protocolId);
        int output = 0;
        int dispVal = 0;

        try {
        	if(checkDayZero == 1 ){
        		if (0 == visitId){
        			Query query = em.createNamedQuery("findDayZeroVisit");        	
		        	query.setParameter(1, protocolId);
		        	Number DayZeroVisit = (Number) query.getSingleResult(); 
		        	return DayZeroVisit.intValue();
        		} else {
		        	//ProtVisitBean  	bProtVisitBean = em.find(ProtVisitBean.class,new Integer(visitId));        
		        	Query query = em.createNamedQuery("findDayZeroVisitCount");        	
		        	query.setParameter(1, protocolId);
		        	query.setParameter(2, 0);
		        	query.setParameter(3, visitId);
		        	Number DayZeroCount = (Number) query.getSingleResult(); 
		        	return DayZeroCount.intValue();
        		}
        	}
        	else{
        		Query query = em.createNamedQuery("findLastDayVisitCount");        	
            	query.setParameter(1, protocolId);
            	query.setParameter(2, duration);
            	query.setParameter(3, visitId);
            	Number DayZeroCount = (Number) query.getSingleResult();             	
            	return DayZeroCount.intValue();
        	}
    }
        catch(Exception e){
        	Rlog.fatal("protvisit", "ProtVisitAgentBean:Exception In validateDayZeroVisit-"
                    + e);
            return -6;
        }
    }   

    private int findGridContents(JSONArray dataGridArray,String key, String lookFor, int startLikeEnd){
    	JSONObject gridRecord = null;
    	String value = null;
    	try{
	    	for (int i = 0; i < dataGridArray.length(); ++i) { 			
				gridRecord = dataGridArray.getJSONObject(i);
				value = gridRecord.getString(key);
				value=(value == null)? null:value.trim();
				if (null == value || value.equals(""))
					return 0;
				switch(startLikeEnd){
				case 1:
					if (lookFor.equals(value)){
						return 1;
					}
					break;
				case 0:
					if (lookFor.startsWith(value)){
						return 1;
					}
					break;
				case -1:
					if (lookFor.contains(value)){
						return 1;
					}
					break;
				case 2:
					if (lookFor.endsWith(value)){
						return 1;
					}
					break;
				}
	    	}
    	} catch (Exception e){
    		
    	}
	    return 0;
    }
    /**
     * The method to add,modify and delete visits in a calendar.
     **/    
    
    //Modified for INF-18183 and BUG#7224 : Raviesh
    public ProtVisitResponse manageVisit(String dataGridString,String updateString,ProtVisitBean vsk,int duration, String calStatus
    	    ,String calType,String deleteString,Hashtable<String, String> args){
    	    	
    	int displacement,checkVisit;
    	int insertAfterInterval;
    	ProtVisitResponse visitResponse = new ProtVisitResponse();
    	JSONArray dataGridArray;
    	JSONArray updateArray;
    	JSONArray deleteArray;
    	JSONObject gridRecord = null;
    	JSONObject updateRecord = null;
    	JSONObject deleteRecord = null;
    	ProtVisitBean visitBean=null,aVisitBean = null,bVisitBean = null,cVisitBean = null;
    	List<ProtVisitBean> visitBeanList = null;
    	int hideFlag = 0,offLineFlag = 0,beanHideFlag;
    	ProtVisitDao protVisitDao = new ProtVisitDao();
    	SchCodeDao schcodedao = new SchCodeDao();
    	int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    	int ftp = schcodedao.getCodeId("timepointtype", "FTP");
    	int dtp = schcodedao.getCodeId("timepointtype", "DTP");
    	String tableName = null;
    	int fromCopyVisitId;
    	HashMap<Integer, Integer> copyVisitMap= new HashMap<Integer, Integer>();//Ak:Added for enhancement PCAL-20801
     try{    	
    	 //fix #7109,BK,09/21/11
    	 int visitNum = this.getMaxVisitNo(vsk.getProtocol_id()); 
    	     dataGridArray = new JSONArray(dataGridString);   
    	     visitBeanList =  this.calculateDisplacement(dataGridArray);    	     
    	     
    	    if((!"[]".equals(deleteString) && !StringUtil.isEmpty(deleteString))){
    	    	deleteArray = new JSONArray(deleteString);
    	    	int visitId = 0;
    			boolean isUpdate = false;
    			//deletion
    			if(calType.equals("L") || calType.equals("P"))
    				tableName = "event_def";
    			else
    				tableName = "event_assoc";

    			ArrayList<String> deleteIdArr = new ArrayList();
    			ArrayList<String> flagArr = new ArrayList();
    			for (int j = 0; j < deleteArray.length(); ++j) {
    				deleteRecord = deleteArray.getJSONObject(j);
    				visitId = deleteRecord.getInt("i");
    				deleteIdArr.add(deleteRecord.getString("i"));
    				flagArr.add("1");
    			}
    			String[] deleteIds = new String[deleteIdArr.size()];
    			deleteIds = deleteIdArr.toArray(deleteIds); 
    			String[] flags = new String[flagArr.size()];
    			flags = flagArr.toArray(flags); 
    			
    			EventdefDao aEventDefDao = new EventdefDao();
				//Modified for INF-18183 and BUG#7224 : Raviesh
				aEventDefDao.deleteEvtOrVisits(deleteIds, tableName, flags, Integer.parseInt(vsk.getCreator()),args);
    	    }

    			for (int i = 0; i < dataGridArray.length(); ++i) { 			
    				gridRecord = dataGridArray.getJSONObject(i); 
    				int intervalType =gridRecord.getInt("noIntervalId");
    				bVisitBean = visitBeanList.get(i);
    				displacement = StringUtil.stringToNum(bVisitBean.getDisplacement());    		            
    				insertAfterInterval = StringUtil.stringToNum(gridRecord.getString("insertAfterInterval"));
    				visitResponse.setVisitName(gridRecord.getString("visitName"));
    				int offlineFlag=StringUtil.stringToNum(gridRecord.getString("offlineFlag")); //AK:Fixed Bug#7633
    				fromCopyVisitId= StringUtil.stringToNum(gridRecord.getString("fromCopyVisitId"));//Ak:Added for enhancement PCAL-20801
    				visitResponse.setRowNumber(StringUtil.stringToNum(gridRecord.getString("visitSeq")));
    				//*************Validate visit interval against duration of the calendar.   				  
    				if(displacement >= duration){
    					cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 1, visitBeanList);
    				} 
    				    				
    				//**************    				
    				if(gridRecord.getInt("visitId")==0 ){ 
    					int insertAfter = (bVisitBean.getInsertAfter()).intValue();
    					if(displacement == 0 && calStatus.equals("O") && insertAfter != -1 && intervalType!=ntpd){ //YK:Modified for Bug#10009
        					visitResponse.setVisitId(-12);				
        					break;
        				}
    					//fix #7056,BK,09/21/11 ,Fixed 7107 ,10/12/2011
        				if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
        					visitResponse.setVisitId(-7);
        					break;
        				}    				
        				else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
        					visitResponse.setVisitId(-9);				
        					break;
        				}    			
        				//fix #7105,7107,BK,09/21/11
        				if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
        						|| displacement == 0){
        					//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
        					cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 0, visitBeanList);
        					}
        				if (cVisitBean != null){
        					visitResponse.setVisitId(-8);
        					break;
        				}
    					visitBean = new ProtVisitBean();									//visit creation i.e if visitId is 0 and delete flag					 
    					visitBean.setip_add(vsk.getip_add());								//is false then record has to be added in database.
    					visitBean.setCreator(vsk.getCreator());
    					visitBean.setProtocol_id(vsk.getProtocol_id());    					
    					visitBean.setVisit_no(++visitNum);      //setting visitId 
    					//SETTING THE HIDE FLAG
                  	    hideFlag = gridRecord.getInt("hideFlag"); //AK:fixed BUG#7486
                  	    if(hideFlag>0){
                  	    	visitBean.setHideFlag(hideFlag);
                  	    }
    					visitBean.setProtVisitBeanJSON(gridRecord, 'C',bVisitBean); 
    					//validating visit in request data
    					cVisitBean = this.validateVisit(i, visitBeanList);
    					if(cVisitBean != null){
    						visitResponse.setVisitId(-2);
    						break;
    					}
    					else{
    						em.persist(visitBean);
    						visitResponse.setVisitId(visitBean.getVisit_id());
    						if(fromCopyVisitId>0 && visitBean.getVisit_id()>0){ //Ak:Added for enhancement PCAL-20801
    							copyVisitMap.put(visitBean.getVisit_id(),fromCopyVisitId);
    						}
    					}    							

    				}				
    				
    				// visit updates
    				else if((!"[]".equals(updateString) || !StringUtil.isEmpty(updateString))) {
    					updateArray = new JSONArray(updateString);
    					boolean isUpdate =false;
    					for (int j = 0; j < updateArray.length(); ++j) {
    						updateRecord = updateArray.getJSONObject(j);
    						if(updateRecord.getInt("i") == gridRecord.getInt("visitSeq")){
    							isUpdate = true;
    							break;
    						}
    					}
                           if(isUpdate){
                        	   
                        		Query query = em.createNamedQuery("findInsertAfterVisits");        	
                               	query.setParameter(1, gridRecord.getInt("visitId"));
                               	Number setAfterDependendent = (Number) query.getSingleResult(); 
                               	if( setAfterDependendent.intValue() > 0 && intervalType==ntpd){ //Ak:Fixed BUG#7085
                               		int parentInGrid = this.findGridContents(dataGridArray, "insertAfterId", gridRecord.getString("visitId")+"/",0);
                               		if (parentInGrid > 0){
                               			visitResponse.setVisitId(-5);
                               			break;
                               		}
                               	}
                               	int insertAfter = (bVisitBean.getInsertAfter()).intValue();
                        	   if(displacement == 0 && calStatus.equals("O") && offlineFlag==0 && insertAfter != -1 && intervalType!=ntpd){ //AK:Fixed Bug#7633,//YK:Modified for Bug#10009
                        		   visitResponse.setVisitId(-12);				
                        		   break;
                        	   }
                        	 if(displacement == 1 &&  bVisitBean.getDays() == 0 && calStatus.equals("O") && offlineFlag=='0'){  //AK:Fixed Bug#7633
               					visitResponse.setVisitId(-13);				
               					break;
               					}
                        	 //fix #7056,BK,09/21/11
               				if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
               					visitResponse.setVisitId(-7);				
               					break;
               				}    				
               				else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
               					visitResponse.setVisitId(-9);				
               					break;
               				}    			
               				//fix #7105,7107,BK,09/21/11
               				if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
               						|| displacement == 0){
               					//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
               					cVisitBean = this.validateDayZeroVisit(vsk,duration, i, 0, visitBeanList);
               				}
               				if (cVisitBean != null){
               					visitResponse.setVisitId(-8);
               					break;
               				} 
                        
                               //D-FIN-25-DAY0 BK JAN-23-2011                           				 
                           		//validating visit in request data                   
                           		cVisitBean = this.validateVisit(i, visitBeanList);
                           		if(cVisitBean != null){
                           			visitResponse.setVisitId(-2);
                           			break;
                           		}
                               aVisitBean = (ProtVisitBean) em.find(ProtVisitBean.class,new Integer(gridRecord.getInt("visitId")));
                               if (aVisitBean == null) {
                            	   visitResponse.setVisitId(-4);
                             		break;
                               }
                          	   aVisitBean.setModifiedBy(vsk.getCreator());
                          	   //SETTING THE HIDE FLAG
                          	    hideFlag = gridRecord.getInt("hideFlag");
                          	    offLineFlag = aVisitBean.getOfflineFlag() == null? 0: aVisitBean.getOfflineFlag();
                          	    beanHideFlag = aVisitBean.getHideFlag() == null? 0: aVisitBean.getHideFlag();
                          	    if(beanHideFlag != hideFlag){
                          	    	aVisitBean.setHideFlag(hideFlag);
                          	    }
                               aVisitBean.setProtVisitBeanJSON(gridRecord, 'U', bVisitBean);
                               visitResponse.setVisitId(aVisitBean.getVisit_id());
                               em.merge(aVisitBean);
                           }

    				}   			
    			}
     		}
    	     catch(Exception e){
    	    	 Rlog.fatal("protvisit","Exception In manageVisit in ProtVisitAgentBean "
    	                 + e);
    	    	 visitResponse.setVisitId(-11);
    	    	 context.setRollbackOnly();
    				 return visitResponse ;
    	     }     	
    	        
    			if(visitResponse.getVisitId() < 0){
    				context.setRollbackOnly();
    			}
    			visitResponse.setCopyVisitMap(copyVisitMap); //Ak:Added for enhancement PCAL-20801
    			 return visitResponse ;	
    	   }
    /**
     * Calculating Displacement based on parent visit or interval.
     * @param gridRecord
     * @return
     * @throws Exception
     */
    private List<ProtVisitBean> calculateDisplacement(JSONArray dataGridArray) throws Exception{
    	JSONObject gridRecord = null;
    	List<ProtVisitBean> aVisitBeanList = new ArrayList<ProtVisitBean>();
    	int displacement = 0;
    	ProtVisitBean aTempBean = null;     	
    	for (int i = 0; i < dataGridArray.length(); ++i) { 			
			gridRecord = dataGridArray.getJSONObject(i); 
			aTempBean = new ProtVisitBean();
			int intervalType =gridRecord.getInt("noIntervalId");
	    	int insertAfterInterval = StringUtil.stringToNum(gridRecord.getString("insertAfterInterval"));
			String insertAfterDispStr = gridRecord.getString("insertAfterId");
			String insertAfterIntervalUnit = gridRecord.getString("intervalUnit");
			String monthStr = gridRecord.getString("months");
			String weekStr = gridRecord.getString("weeks");
			String dayStr = gridRecord.getString("days");
			String dispStr = "";
			int insertAfter = 0,prevdisp,interval,months,weeks,days;
			prevdisp = 0;
			int noOfDays = 0;
			String strIntervalType = "";
			if (intervalType == gridRecord.getInt("noTimePointId")){
				strIntervalType = "1";
			} else {
				if ((insertAfterInterval != 0)) {
					int pos = insertAfterDispStr.indexOf('/');
					if (pos > 0) {
						String insertAfterStr =  insertAfterDispStr.substring(0, pos);
						dispStr = insertAfterDispStr.substring(pos+1);
						insertAfter = StringUtil.stringToNum(insertAfterStr);
						prevdisp = StringUtil.stringToNum(dispStr);
					}
					 months = 0;weeks = 0; days = 0;
					 //FIX#7010		 
					if (insertAfterIntervalUnit.equals("Months"))
						interval = insertAfterInterval *30;
					else if (insertAfterIntervalUnit.equals("Weeks"))
						interval = insertAfterInterval *7;
					else //days
						interval = insertAfterInterval;
					displacement = prevdisp + interval;
				}
				else {
					months = StringUtil.stringToNum(monthStr);
					weeks = StringUtil.stringToNum(weekStr);
					days = StringUtil.stringToNum(dayStr);
		
					if (months == 0) months++;
					if (weeks == 0) weeks++;
					if (days == 0) days++;
					displacement = (months - 1)*30 + (weeks - 1)*7 + days;
		
		
					displacement = 0;
					if (months > 0) displacement+= (months -1)*30;
					if (weeks > 0) displacement+= (weeks -1)*7;
					displacement += days;  		// Imp: in case of calculated day zero visits, displacement = 0
					insertAfter = 0;
				}
				//DFIN-25
		        if(!StringUtil.isEmpty(gridRecord.getString("days")) && insertAfter == 0){               
		    		noOfDays = gridRecord.getInt("days");
		        	 }
		        else if(insertAfter != 0){
		        	noOfDays = displacement;
		    		}
		        else {
		    	    noOfDays = 1; 
		        }
		     	strIntervalType = "0";
			}
			months = StringUtil.stringToNum(monthStr);
			weeks = StringUtil.stringToNum(weekStr);
			aTempBean.setName(gridRecord.getString("visitName"));
			aTempBean.setVisit_id(gridRecord.getInt("visitId"));
			aTempBean.setMonths(months);
			aTempBean.setWeeks(weeks);
			aTempBean.setDays(noOfDays);
			aTempBean.setInsertAfter(insertAfter);
		    aTempBean.setIntervalFlag(strIntervalType);
			aTempBean.setDisplacement(""+displacement);
	    	aVisitBeanList.add(aTempBean);
    	
    	}
    	return aVisitBeanList;
    	
    }
 /**
  *   Fixed #6994
  * @param index
  * @param pVisitBeanList
  * @return
  */
    private ProtVisitBean validateVisit(int index,List<ProtVisitBean> pVisitBeanList){
    	ProtVisitBean aVisitBean = pVisitBeanList.get(index);
    	int disp,days,weeks,months;
    	ProtVisitBean bVisitBean = null;
    	for(int i=0;i<pVisitBeanList.size();i++){
    		bVisitBean = pVisitBeanList.get(i);
    		disp = Integer.parseInt(bVisitBean.getDisplacement());
    		months = bVisitBean.getMonths();
    		days = bVisitBean.getDays();
    		weeks = bVisitBean.getWeeks();    
    		
    		if(i!=index){
    			int bWeekMonth = bVisitBean.getWeeks()|bVisitBean.getMonths();
    			int aWeekMonth = aVisitBean.getWeeks()|aVisitBean.getMonths();
    			if( (Integer.parseInt(aVisitBean.getDisplacement()) == 1 ||Integer.parseInt(aVisitBean.getDisplacement()) == 0) 
    					&& aVisitBean.getDays() == 0 &&(Integer.parseInt(bVisitBean.getDisplacement()) == 7 && bWeekMonth == 1)) {
    				// day 7 condition
    				return aVisitBean;

    			}
    			else if((Integer.parseInt(aVisitBean.getDisplacement()) == 7 && aWeekMonth == 1) &&( (Integer.parseInt(bVisitBean.getDisplacement()) == 1 
    					||Integer.parseInt(bVisitBean.getDisplacement()) == 0) 
    					&& bVisitBean.getDays() == 0 )){
    				//day 0 condition
    				return aVisitBean;
    			}

    		}
    	}

    	
    	
    	return  null;
    }
    /**
     * 
     * @param index
     * @param pVisitBeanList
     * @return
     */
    private ProtVisitBean validateDayZeroVisit(ProtVisitBean vsk, int duration,int index,int checkDayZero,List<ProtVisitBean> pVisitBeanList){
    	ProtVisitBean aVisitBean = pVisitBeanList.get(index);
    	String aIntervalFlag = aVisitBean.getIntervalFlag();
    	int aInsertAfter = (aVisitBean.getInsertAfter()).intValue();
    	
    	if ("0".equals(aIntervalFlag) && (-1 != aInsertAfter)){
	    	int days,weeks,months;
			int visitDisp = Integer.parseInt(aVisitBean.getDisplacement());

	    	for(int i=0;i<pVisitBeanList.size();i++){
		    	ProtVisitBean bVisitBean = null;
	    		bVisitBean = pVisitBeanList.get(i);
    			int disp = Integer.parseInt(bVisitBean.getDisplacement());
	    		String bIntervalFlag = bVisitBean.getIntervalFlag();
	    		int bInsertAfter = (bVisitBean.getInsertAfter()).intValue();

	    		if(i!=index){
		    		if( checkDayZero == 0 && disp == duration ) {
	    				if((disp == 1 ||disp == 0) 
	        				&& bVisitBean.getDays() == 0 ){
	        				//day 0 condition
	        				if (duration != 1)
	        					return aVisitBean;
	    				}else{
		    				// last day condition
		    				return aVisitBean;
	    				}
	    			}
	    			else if(checkDayZero == 1){
			    		if ("0".equals(bIntervalFlag)){
				    		if (disp != 0){
					    		months = bVisitBean.getMonths();
					    		days = bVisitBean.getDays();
					    		weeks = bVisitBean.getWeeks();    
	
				    			int bWeekMonth = bVisitBean.getWeeks()|bVisitBean.getMonths();
				    			int aWeekMonth = aVisitBean.getWeeks()|aVisitBean.getMonths();

			    				if (visitDisp == 1 || visitDisp == 0){
			    					if (aVisitBean.getDays() == 0 ){
			    						//day 0 condition
			    						if (disp == 1 && duration != 1)
			    							return aVisitBean;
			    					}else{
				    					int dayZeroExists = 0;
				    					dayZeroExists = validateDayZeroVisit(duration, vsk.getProtocol_id(),1,aVisitBean.getVisit_id());
				    					// last day condition
				    					if (dayZeroExists > 0 && visitDisp == duration){
				    						return aVisitBean;
				    					}
			    					}
			    				}else{
			    					int dayZeroExists = 0;
			    					dayZeroExists = validateDayZeroVisit(duration, vsk.getProtocol_id(),1,aVisitBean.getVisit_id());
			    					
			    					if (visitDisp > duration || (dayZeroExists > 0 && visitDisp == duration)){
			    						return aVisitBean;
			    					}    					
			    				}
				    		}else{
				    			if (-1 != bInsertAfter){ //fix #9924
					    			// bVisitBean is a calculated day zero visit
									int dayZeroExists = 1;
									if (visitDisp == 1 || visitDisp == 0){
										//day 0 condition
										if (aVisitBean.getDays() == 0 ){
					    					if (dayZeroExists > 0 && visitDisp == duration){
					    						return aVisitBean;
					    					}
										}
									}else{				
										if (visitDisp > duration || (dayZeroExists > 0 && visitDisp == duration)){
											return aVisitBean;
										}
									}
				    			}
				    		}
			    		}
	    			}
	    		} else{
	    			// Processing self fix #9940
	    			if (visitDisp > duration){
						return aVisitBean;
					}
	    		}
	    	}
    	}    	
    	
    	return  null;
    }

}// end of class
