/* 
 * Classname			LineitemAgentRObj.class
 * 
 * Version information
 *
 * Date					03/28/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.service.lineitemAgent;

/* Import Statements */
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.common.LineitemDao;
import com.velos.esch.business.lineitem.impl.LineitemBean;

@Remote
public interface LineitemAgentRObj {

    LineitemBean getLineitemDetails(int lineitemId);

    public int setLineitemDetails(LineitemBean lsk);

    public int updateLineitem(LineitemBean lsk);
    
    // Overloaded for INF-18183 ::: Akshi
    public int updateLineitem(LineitemBean lsk,Hashtable<String, String> args);

    public LineitemDao getLineitemsOfBgtcal(int bgtcalId);

    public int updateAllLineitems(ArrayList lineitemIds, ArrayList discounts,
            ArrayList unitCosts, ArrayList noUnits, ArrayList researchCosts,
            ArrayList stdCareCosts, ArrayList categories,
            ArrayList applyIndirects, String ipAdd, String usr, ArrayList lineItemTotalCosts, ArrayList lineItemSponsorAmount, ArrayList lineItemVariance);

    public int setRepeatLineitems(String[] lineitemNames,
            String[] lineitemDescs, String[] lineitemCpts,
            String[] lineitemRepeatOpts, String[] categories, String[] tmids,
            String[] cdms, String bgtSectionId, String budgetCalId,
            String ipAdd, String usr);

    public int lineitemDelete(int lineitemId);

    public LineitemDao insertPersonnelCost(int budgetCalId, String[] perTypes,
            String[] rates, String[] inclusions, String[] notes,
            String[] categories, String[] tmids, String[] cdms, int creator,
            String ipAdd);

    public LineitemDao getLineitemsOfDefaultSection(int budgetCalId);

    public LineitemDao updatePersonnelCost(int budgetCalId, String[] parentIds,
            String[] personnelTypes, String[] rates, String[] inclusions,
            String[] notes, String[] applyTypes, String[] categories,
            String[] tmids, String[] cdms, int p_creator, String ipAdd);

    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd);
    
    //Overloaded for INF-18183 ::: Ankit
    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo);

    public int updateRepeatLineitems(int budgetCalId, String[] parentIds,
            String[] lineitemNames, String[] lineitemDescs,
            String[] lineitemCpts, String[] lineitemRepeat,
            String[] applyTypes, String[] categories, String[] tmids,
            String[] cdms, int creator, String ipAdd);

    public LineitemDao getLineitemsOfRepeatDefaultSec(int budgetCalId);

    public int insertAllLineItems(ArrayList lineitemNames,
            ArrayList lineitemDescs, ArrayList lineitemCpts,
            ArrayList category, ArrayList TMIDs, ArrayList CDMs,
            String bgtSectionId, String ipAdd, String usr,Hashtable htMoreData);

    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd);
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo);

    public LineitemDao getSectionLineitems(int sectionPK); 
    
}