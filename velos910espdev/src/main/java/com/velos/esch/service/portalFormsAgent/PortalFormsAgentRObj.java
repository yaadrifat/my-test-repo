/*
 * Classname : PortalFormsAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 03DEC2010
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majuamdar
 */

package com.velos.esch.service.portalFormsAgent;


/* Import Statements */
import java.util.ArrayList;
import java.util.Hashtable;
import javax.ejb.Remote;

//import com.velos.esch.business.common.CrflibDao;
//import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.PortalFormsDao;
import com.velos.esch.business.portalForms.impl.PortalFormsBean;

@Remote
public interface PortalFormsAgentRObj {

	public int setPortalForms(PortalFormsBean pfsk);
	public PortalFormsBean getPortalFormDetails(int pfId);
	public int updatePortalForms(PortalFormsBean pfsk);
	public PortalFormsDao getCheckedFormsForPortalCalendar(int calId, int portalId);
	public int removePortalFormId(int pfId);	
	// removePortalFormId() Overloaded for INF-18183 ::: Raviesh
	public int removePortalFormId(int pfId,Hashtable<String, String> args);
    public int removeCalFromPortal(int potalId, int calId);
    // Overloaded for INF-18183 ::: AGodara
    public int removeCalFromPortal(int potalId, int calId,Hashtable<String, String> auditInfo);
    // PP-18306 AGodara
    public int removeMulCalForms(int potalId, String calIds,Hashtable<String, String> auditInfo);
    public PortalFormsDao getPortalFormsDAO(int eventId1,ArrayList<String> selId);
    public int setPortalFormsDAO(PortalFormsDao pfDao);

}