/*
 * Classname : AdvInfoAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 12/10/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advInfoAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.esch.business.advInfo.impl.AdvInfoBean;

// import com.velos.esch.business.common.EventInfoDao;
@Remote
public interface AdvInfoAgentRObj {

    AdvInfoBean getAdvInfoDetails(int advInfoId);

    public int setAdvInfoDetails(AdvInfoBean account);

    public int updateAdvInfo(AdvInfoBean usk);
    // public EventInfoDao getAdverseEventNames(int patProtID)throws
    // RemoteException;

}