/*
 * Classname : BudgetcalAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 03/19/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.budgetcalAgent;

/* Import Statements */
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.common.BudgetcalDao;

/* End of Import Statements */
@Remote
public interface BudgetcalAgentRObj {

    BudgetcalBean getBudgetcalDetails(int budgetcalId);

    public int setBudgetcalDetails(BudgetcalBean account);

    public int updateBudgetcal(BudgetcalBean usk);
    
    // Overloaded for INF-18183 ::: Akshi
    public int updateBudgetcal(BudgetcalBean usk,Hashtable<String, String> args);

    public int removeBudgetcal(int budgetcalId);

    public BudgetcalDao getAllBgtCals(int budgetId);

    public BudgetcalDao getBgtcalDetail(int bgtcalId);

    // ///////////////////////
    /*
     * Update lineitem_othercost when 'exclude SOC' is updated
     */

    public int updateOtherCostToExcludeSOC(String bgtCalId, int excludeFlag,String modifiedBy,String ipAdd) ;
}