/*
 * Classname : EventStatAgentBean
 *
 * Version information: 1.0
 *
 * Date: 08/27/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.service.eventstatAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.esch.business.eventstat.impl.EventStatBean;
import com.velos.esch.service.eventstatAgent.EventStatAgentRObj;
import com.velos.esch.service.util.Rlog;

//import com.velos.esch.business.common.EventStatDao;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * EventStatBean.<br>
 * <br>
 *
 */
@Stateless
public class EventStatAgentBean implements EventStatAgentRObj {
	@PersistenceContext(unitName = "esch")
	protected EntityManager em;

	/**
	 *
	 */
	private static final long serialVersionUID = 3257565122447683633L;

	public EventStatBean getEventStatDetails(int evtStatId) {

		try {

			return (EventStatBean) em.find(EventStatBean.class,
					new Integer(evtStatId));

		} catch (Exception e) {
			System.out
					.print("Error in getEventStatDetails() in EventStatAgentBean"
							+ e);
			return null;
		}

	}

	/**
	 * Calls setEventStatDetails() on Event Status Entity Bean
	 * EventStatBean. Creates a new EventStatBean with the values in the
	 * StateKeeper object.
	 *
	 * @param ersk
	 *            a EventStat state keeper containing eventresopurce
	 *            attributes for the new EventStatBean.
	 * @return int - 0 if successful; -2 for Exception;
	 */

	public int setEventStatDetails(EventStatBean estsk) {
		try {

			EventStatBean esb = new EventStatBean();
			esb.updateEventStat(estsk);
			em.persist(esb);

			return (esb.getEvtStatId());

		} catch (Exception e) {
			System.out.print("Error in setEventStatDetails() in EventStatAgentBean "+ e);
		}
		return 0;
	}

	public int updateEventStat(EventStatBean estsk) {

		EventStatBean retrieved = null;
		int output =0;
		try {

			retrieved = (EventStatBean) em.find(EventStatBean.class, new Integer(estsk.getEvtStatId()));

            if (retrieved == null) {
                return -2;
            }

            output = retrieved.updateEventStat(estsk);
            em.merge(retrieved);



		} catch (Exception e) {
						Rlog.fatal("eventstat", "Error in updateEventStat() in EventStatAgentBean "    + e);
		}

		return output;
	}


//-----------------------------------------------------------------------

}// end of class
