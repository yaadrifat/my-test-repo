/**
 * Created On Nov 5, 2012
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;

/**
 * @author Kanwaldeep
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface SystemAdministrationSEI {
	
	@WebResult(name="Codes")
	public Codes getCodeList(
			@WebParam(name="type")
			String type) throws OperationException; 
	
	
	@WebResult(name="CodeTypes")
	public CodeTypes getCodeTypes() throws OperationException; 


}
