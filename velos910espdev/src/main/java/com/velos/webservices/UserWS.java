package com.velos.webservices;

import javax.jws.WebService;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.UserClient;
import com.velos.services.model.Groups;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.User.UserStatus;

/**
 * WebServices class dealing with all User Services operations.
 */
@WebService(
		serviceName="UserService",
		endpointInterface="com.velos.webservices.UserSEI",
		targetNamespace="http://velos.com/services/"
		)
public class UserWS implements UserSEI{
	

	public ResponseHolder changeUserStatus(UserIdentifier uId, UserStatus uStat)
			throws OperationException {
		// TODO Auto-generated method stub
		return UserClient.changeUserStatus(uId, uStat);
	}
	

	public Groups getAllGroups() throws OperationException {
		// TODO Auto-generated method stub
		return UserClient.getAllGroups();
	}
	
	
		
	public Groups getUserGroups(UserIdentifier userId)throws OperationException 
	{
		return UserClient.getUserGroups(userId);
		
	}
	
	
	public  Organizations getAllOrganizations() throws OperationException{
		// TODO Auto-generated method stub
		return UserClient.getAllOrganizations();
	}
	

	public ResponseHolder createNonSystemUser(NonSystemUser nonsystemuser)
			throws OperationException
	{
		return UserClient.createNonSystemUser(nonsystemuser);
	}
	
	public ResponseHolder killUserSession(UserIdentifier userId)
			throws OperationException
	{
		return UserClient.killUserSession(userId);
	}

	public ResponseHolder createUser(User user) throws OperationException {
		// TODO Auto-generated method stub
		return UserClient.createUser(user);
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.UserSEI#createNonSystemUser(com.velos.services.model.NonSystemUser)
	 */

//	public ResponseHolder createNonSystemUser(NonSystemUser nonSystemUser) {
		// TODO Auto-generated method stub
//		return null;
//	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.UserSEI#createUser(com.velos.services.model.User)
	 */

//	public ResponseHolder createUser(User user) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.UserSEI#changeUserStatus(com.velos.services.model.UserIdentifier, com.velos.services.model.User.UserStatus)
	 */

	
	
}