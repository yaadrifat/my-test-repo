package com.velos.webservices;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.model.StudyIdentifier;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of PatientStudies webservices
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")

	public interface PatientStudySEI {
		/**
		 * Service Endpoint Interface public method which
		 * calls getPatientStudies of webservices and returns
		 * List of PatientStudy
		 * @param patientId
		 * @return
		 * @throws OperationException
		 */
		@WebResult(name = "PatientStudy" )
		public List<PatientStudy> getPatientStudies(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId)
				throws OperationException;   

}