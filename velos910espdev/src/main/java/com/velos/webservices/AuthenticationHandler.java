/**
 * 
 */
package com.velos.webservices;


import java.sql.Connection;


import javax.ejb.EJB;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.ws.rs.core.Response;


import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.jaxrs.ext.RequestHandler;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.message.Message;



import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.web.user.UserJB;
import com.velos.services.model.User.UserStatus;

/**
 * @author dylan
 *
 */
public class AuthenticationHandler implements RequestHandler {

	@EJB
	private UserAgentRObj userAgent;

	public Response handleRequest(Message m, ClassResourceInfo resourceClass) {
		AuthorizationPolicy policy = (AuthorizationPolicy)m.get(AuthorizationPolicy.class);
		policy.getUserName();
		policy.getPassword(); 
		// policy.setAuthorization(value)
		UserJB userJB = new UserJB();
		UserBean user = null;
		Connection conn = null;
		try{
			user = userJB.validateUser(policy.getUserName(), policy.getPassword());
			//	 resourceClass.
		}
		catch (AccessDeniedException ex) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		finally {
			try { if (conn != null) { conn.close(); } } catch (Exception e){}
		}

		if (user == null){
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		//DRM - fix for 5448 and 5453 - block authentication if the user
		//is not Active
		if (!user.userStatus.equalsIgnoreCase(UserStatus.ACTIVE.getLegacyString())){
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		// alternatively :
		// HttpHeaders headers = new HttpHeadersImpl(m);
		// access the headers as needed  





		LoginContext lctx;

		try {

			// invokes the standard jboss client login module...required
			// for all EJB client communication in JBOss
			lctx = new LoginContext("client-login",
					new SimpleAuthCallbackHandler(
							policy.getUserName()));
			lctx.login();

		} catch (LoginException e) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}


		return null; 

	}
}

