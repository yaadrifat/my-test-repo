/**
 * 
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Cost;
import com.velos.services.model.EventCostIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventIdentifiers;
import com.velos.services.model.EventNameIdentifier;
import com.velos.services.model.EventNames;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.VisitIdentifiers;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.model.VisitNames;

/**
 * @author dylan 
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({EventNameIdentifier.class,
	VisitNameIdentifier.class
	})
public interface StudyCalendarSEI {

	@WebResult(name = "Response")
	public abstract ResponseHolder createStudyCalendar(
			@WebParam(name = "StudyCalendar") 
			StudyCalendar study)
			throws OperationException,OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarSummary(
			@WebParam(name="CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName, 
			@WebParam(name = "CalendarSummary") 
			CalendarSummary study)
			throws OperationException, OperationRolledBackException;

	
	@WebResult(name = "StudyCalendar")
	public abstract StudyCalendar getStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier, 
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName) 
		throws OperationException;
	
	
	//Visit Methods

	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarVisitDetails(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "Visit") 
			CalendarVisitSummary studyCalendarVisit) 
	throws OperationException,OperationRolledBackException;
	

	@WebResult(name="Visit")
	public abstract CalendarVisit getStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "VisitName")
			String visitName) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addVisitsToStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "Visits") 
			CalendarVisits newVisit) 
	throws OperationException,OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeVisitsFromStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifiers")
			VisitIdentifiers visitIdentifiers,
			@WebParam(name = "VisitNames")
			VisitNames visitNames) 
	throws OperationException,OperationRolledBackException;
	
	
	//Event Methods

	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarEventDetails(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName,
			@WebParam(name = "Event") 
			CalendarEventSummary studyCalendarEvent) 
	throws OperationException,OperationRolledBackException;
	
	@WebResult(name="Event")
	public abstract CalendarEvent getStudyCalendarEvent(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addEventsToStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "VisitIdentifer")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "Events") 
			CalendarEvents studyCalendarEvent) 
	throws OperationException,OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeEventsFromStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifier")
			VisitIdentifier visitIdentifiers,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifiers")
			EventIdentifiers eventIdentifiers,
			@WebParam(name = "EventName")
			EventNames eventName) 
	throws OperationException,OperationRolledBackException;

	@WebResult(name = "CalendarSummary")
	public abstract CalendarSummary getStudyCalendarSummary(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier, 
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName) 
		throws OperationException;
	
	@WebResult(name = "Response")
    public abstract ResponseHolder removeStudyCalendar(
    		@WebParam(name = "CalendarIdentifier")    		
    		CalendarIdentifier calendarIdentifier,
    		@WebParam(name = "StudyIdentifier")
    		StudyIdentifier studyIdentifier,
    		@WebParam(name = "CalendarName")
    		String calendarName)
	throws OperationException; 
	
	@WebResult(name = "Response")
    public abstract ResponseHolder addEventCost(
    		@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName,
    		@WebParam(name = "Cost")
    		Cost cost)
	throws OperationException; 
	
	@WebResult(name = "Response")
    public abstract ResponseHolder updateEventCost(
    		@WebParam(name = "EventCostIdentifier")    		
    		EventCostIdentifier eventCostIdentifier,
    		@WebParam(name = "Cost")
    		Cost cost)
	throws OperationException; 
	
	@WebResult(name = "Response")
    public abstract ResponseHolder removeEventCost(
    		@WebParam(name = "EventCostIdentifier")    		
    		EventCostIdentifier eventCostIdentifier)
	throws OperationException; 
}

