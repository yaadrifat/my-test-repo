/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Mon Feb 02
 * 14:13:44 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2067)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.Serializable;

// /__astcwz_class_idt#(2066!n)
public class DataColumn implements Serializable {
    // /__astcwz_attrb_idt#(2046)
    private String columnName;

    // /__astcwz_attrb_idt#(2047)
    private String columnType;

    // /__astcwz_attrb_idt#(2064)
    private int columnDisplaySize;

    // /__astcwz_attrb_idt#(2048)
    private int columnScale;

    // /__astcwz_attrb_idt#(2049)
    private int columnPrecision;

    // /__astcwz_default_constructor

    public DataColumn() {
    }

    // /__astcwz_opern_idt#()
    public String getColumnName() {
        return columnName;
    }

    // /__astcwz_opern_idt#()
    public String getColumnType() {
        return columnType;
    }

    // /__astcwz_opern_idt#()
    public int getColumnDisplaySize() {
        return columnDisplaySize;
    }

    // /__astcwz_opern_idt#()
    public int getColumnScale() {
        return columnScale;
    }

    // /__astcwz_opern_idt#()
    public int getColumnPrecision() {
        return columnPrecision;
    }

    // /__astcwz_opern_idt#()
    public void setColumnName(String acolumnName) {
        columnName = acolumnName;
    }

    // /__astcwz_opern_idt#()
    public void setColumnType(String acolumnType) {
        columnType = acolumnType;
    }

    // /__astcwz_opern_idt#()
    public void setColumnDisplaySize(int acolumnDisplaySize) {
        columnDisplaySize = acolumnDisplaySize;
    }

    // /__astcwz_opern_idt#()
    public void setColumnScale(int acolumnScale) {
        columnScale = acolumnScale;
    }

    // /__astcwz_opern_idt#()
    public void setColumnPrecision(int acolumnPrecision) {
        columnPrecision = acolumnPrecision;
    }

    public String getColumnDDL() {
        /* Returns a DDL string for the column */

        StringBuffer sbCol = new StringBuffer();

        sbCol.append(columnName + "   " + columnType);

        if (columnType.equalsIgnoreCase("CHAR")
                || columnType.equalsIgnoreCase("VARCHAR2")) {
            sbCol.append(" (" + columnDisplaySize + ")");
        }

        if (columnType.equalsIgnoreCase("NUMBER")) {
            if (columnPrecision > 0 && columnScale >= 0) {
                sbCol.append(" (" + columnPrecision + ", " + columnScale + ")");
            }
        }

        // for blob, clob,xmltype, date, long we dont have to give column size
        return sbCol.toString();

    }

}
