package com.velos.remoteservice;

public class VelosServiceException extends RuntimeException {

	public VelosServiceException() {
		// TODO Auto-generated constructor stub
	}

	public VelosServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public VelosServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public VelosServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
