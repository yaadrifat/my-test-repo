package com.velos.remoteservice.lab;


import com.velos.remoteservice.AbstractServiceFactory;

/**
 * Factory class for creating ILabService implementations. 
 * @author dylan
 *
 */
public class LabServiceFactory extends AbstractServiceFactory {

	public static ILabService getService() throws ClassNotFoundException{
		//This sweet code will only work with Java 1.6. For 1.5, we need the
		//Class.newInstance()
		//		final  ServiceLoader<ILabService> labServiceLoader =
		//			ServiceLoader.load(ILabService.class);
		//		
		//		for (ILabService labService : labServiceLoader){
		//			return labService;
		//		}
		//		return null;


		Class<ILabService>  labServiceClass = ILabService.class;
		for (ILabService service : load(labServiceClass)){
			return service;
		}
		
		return null;


	}
	
	

}
