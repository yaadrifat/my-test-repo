package com.velos.remoteservice.demographics;

import java.util.List;
import java.util.Map;

import com.velos.remoteservice.IRemoteService;
import com.velos.remoteservice.VelosServiceException;

/**
 * This interface defines methods for an entry-point into the Velos
 * Consumption Framework plugins. Implementations can be created to integrate
 * to remote services that provide patient searc services. Methods included querying
 * a service for a list of patients by MRN, and by name and gender.
 * 
 * Note that a subsequent version of this class will support returning lists of IPatientDemographics 
 * objects.
 * 
 * The framework is agnostic about the protocols used to query and retreive
 * content.
 * 
 * @author dylan
 *
 */
public interface IDemographicsService extends IRemoteService {

	/**
	 * Retrieve patient demographic information for a single patient based on the submitted MRN.
	 * @param MRN
	 * @return
	 * @throws VelosServiceException
	 */
	public IPatientDemographics getPatientDemographics(String MRN) throws PatientNotFoundException;
	
	/**
	 * Retreives a list of patient demographic information from the remote system
	 * based on first name, last name and gender
	 * @param lastName
	 * @param firstName
	 * @param gender
	 * @return
	 * @throws VelosServiceException
	 */
	public List<IPatientDemographics> getPatientDemographics(String lastName, String firstName, String gender) throws PatientNotFoundException;

	/**
	 * Retrieves a list of patient demographic infomration form the remote system
	 * based on a configurable set of parameters. That set of parameters will be passed
	 * to the remote system based on the business logic of the implementation. 
	 * @param searchParameters
	 * @return
	 */
	public List<IPatientDemographics> getPatientDemographics(Map<String, Object> searchParameters) throws PatientNotFoundException;
	
}
