package com.velos.base.daemon;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * An Observer/Observable Implementation of a Deamon Thread that looks at any
 * new File created in a gived Folder. New files are checked by their last modif
 * date.
 * 
 * Build a list of files and pass it to any Registered Observer.<br>
 * 
 * <pre>
 *  
 *   Works in two modes : mode = ALL           -&gt; returns all files in the folder.
 *                        mode = CHECK_DATE    -&gt; returns only new files.
 *   
 * </pre>
 * 
 * @author Vishal Abrol
 * @version 1.0
 */
public class FileWatcher extends Observable {

    // private static org.apache.log4j. =
    // org.apache.log4j..get(FileWatcher.class);

    /** The Full Real Path to the Folder to Watch * */
    private String m_FolderPath = "";

    /** The Abstract File object of the Folder to watch * */
    private File m_Folder;

    /** Define at what interval the folder must be checked, in millis * */
    private long m_Interval;

    /** The internal Thread Timer * */
    private Timer m_Timer;

    /** Both file and directory or only file * */
    private boolean m_FileOnly = true;

    /** Define if the Thread is User or Deamon Thread * */
    private boolean m_IsDeamon = true;

    /** Check files by their last modif date status or not * */
    public boolean mCheckDate = false;

    /** the Last Time the Folder was checked * */
    private long m_LastCheckTime;

    /** The Vector of new files * */
    Vector m_NewFiles = new Vector();

    /** If the directories should be returned.m_ProcessDir * */
    private boolean mProcessDir = true;

    /**
     * Inner TimerTask Class used to schedule Thread Task
     * 
     */
    public class FileWatcherTask extends TimerTask {

        public void run() {

            // Check for new files
            synchronized (m_NewFiles) {

                m_NewFiles.clear();

                checkFiles();
                // //Rlog.debug("common","count of observers
                // are"+countObservers());
                // Notify Observers if number of files > 0
                if (m_NewFiles.size() > 0) {
                    // //Rlog.debug("common","count of observers after files
                    // changed"+countObservers());
                    externalSetChanged(); // Alert the Observable Object That
                    // there are change in the folder
                    notifyObservers(m_NewFiles);
                }
            }
        }
    }

    /**
     * Constructor
     * 
     * @param (String)
     *            fullDirPath , the real Path to the folder to watch
     * @param (boolean)
     *            checkDate, check by last modif date or not
     * 
     * @param (long)
     *            interval , the interval to do the repeat Task
     * @exception IOException
     */
    public FileWatcher(String fullFolderPath, boolean checkDate, long interval,
            boolean fileOnly) throws IOException {

        setFolderPath(fullFolderPath);
        setCheckDate(checkDate);
        setInterval(interval);
        setFileOnly(fileOnly);
    }

    /**
     * Constructor Precise if the Thread to Create is a Deamon or not
     * 
     * @param (String)
     *            fullFolderPath , the real Path to the folder to watch
     * @param (boolean)
     *            checkDate, check new files by date changes ?,
     * 
     * @param (long)
     *            interval , the interval to do the repeat Task
     * @param (boolean)
     *            isDeamon , create a User or Deamon Thread
     * @exception IOException
     */
    public FileWatcher(String fullFolderPath, boolean checkDate, long interval,
            boolean fileOnly, boolean isDeamon) throws IOException {

        setFolderPath(fullFolderPath);
        setCheckDate(checkDate);
        setInterval(interval);
        setFileOnly(fileOnly);
        setDeamon(isDeamon);

    }

    /**
     * Creates the Timer Thread and starts it
     * 
     * @Exception IOException
     */
    public void start() throws IOException {

        initialize();
        m_Timer = new Timer(isDeamon());

        // Rlog.debug("common","Time created, Check Interval=" + getInterval() +
        // " (millis) " );

        m_Timer.schedule(new FileWatcherTask(), 0, getInterval());

        // Rlog.debug("common","Timer started() ");

    }

    /**
     * Returns The Interval Values
     * 
     * @return (long) the interval
     */
    public long getInterval() {
        return m_Interval;
    }

    /**
     * Set The Interval Values
     * 
     * @param (long)
     *            interval , the interval used to do repeatitive task in millis
     */
    protected void setInterval(long interval) {
        m_Interval = interval;
    }

    /**
     * Small trick to export the setChanged method that has a protected. This is
     * necessary for compilation with Ant...
     */
    public void externalSetChanged() {
        setChanged();
    }

    /**
     * Returns The Path of the Folder to watch
     * 
     * @return (String) the path to the folder to watch
     */
    public String getFolderPath() {
        return m_FolderPath;
    }

    /**
     * Set The FolderPath
     * 
     * @param (String)
     *            fullFolderPath, the path to the folder to watch
     */
    protected void setFolderPath(String fullFolderPath) {
        m_FolderPath = fullFolderPath;
    }

    /**
     * set file only mode
     * 
     * @param (boolean)
     *            file only or not
     */
    public void setFileOnly(boolean fileOnly) {
        m_FileOnly = fileOnly;
    }

    /**
     * Returns The Check File Mode
     * 
     * @return (boolean) if check new file by controlling the last modified date
     */
    public boolean getCheckDate() {
        return mCheckDate;
    }

    /**
     * Set The Check File Mode ( by last modif date or returns all files found
     * 
     * @param (boolean)
     *            checkDate, check by last modif date or not
     */
    public void setCheckDate(boolean checkDate) {
        mCheckDate = checkDate;
    }

    /**
     * Thread is a Deamon or not
     * 
     * @return (boolean) true if is Deamon Thread
     */
    public boolean isDeamon() {
        return m_IsDeamon;
    }

    /**
     * Create a Deamon or User Thread
     * 
     * @param (boolean)
     *            isDeamon
     */
    protected void setDeamon(boolean isDeamon) {
        m_IsDeamon = isDeamon;
    }

    /**
     * Verify if the Folder to watch exists. Create the Archive Folder if not
     * exist.
     * 
     * @exception IOException
     */
    protected void initialize() throws IOException {

        // Rlog.debug("common","Initializing file watcher" );

        /*
         * For Test Purpose ToChange : restore the last check time from ext.
         * file !
         */
        m_LastCheckTime = System.currentTimeMillis();

        // Rlog.debug("common","Watching directory=" + getFolderPath() );

        File tmpFile = new File(getFolderPath());

        if (tmpFile != null && tmpFile.isDirectory() && !tmpFile.canWrite()) {

            // Rlog.debug("common","No write access to directory " +
            // getFolderPath() + " tmpFile=" + tmpFile.toString());

        } else if (!tmpFile.exists()) {

            // Rlog.debug("common","Directory " + tmpFile.toString() + " does
            // not exist, creating...");

            tmpFile.mkdirs();

            // Rlog.debug("common","Directory " + tmpFile.toString() + " created
            // successfully.");

            if (tmpFile == null) {
                throw new IOException(
                        "FileWatcher::initialize(), cannot create directory "
                                + tmpFile.toString());
            }
        }
        m_Folder = tmpFile;

    }

    /**
     * Checks new files and builds the Vector of files to pass to Observers
     * 
     */
    protected void checkFiles() {

        // Vector newFiles = new Vector();
        if (m_Folder.isDirectory()) {

            if (!getCheckDate()) {

                File[] files = m_Folder.listFiles();
                int size = files.length;

                for (int i = 0; i < size; i++) {
                    // //Rlog.debug("common","FileWatcher found new file " +
                    // files[i].getName() );
                    if (files[i].canWrite()) {
                        if (!m_FileOnly) {
                            m_NewFiles.add(files[i]);
                        } else if (files[i].isFile()) {
                            m_NewFiles.add(files[i]);
                        }
                    }
                }

            } else {

                File[] files = m_Folder.listFiles();
                int size = files.length;

                for (int i = 0; i < size; i++) {

                    if (files[i].lastModified() > m_LastCheckTime) {
                        m_NewFiles.add(files[i]);
                    }
                }

            }

        }
        // return newFiles;

    }

} // end Class FileWatcher
