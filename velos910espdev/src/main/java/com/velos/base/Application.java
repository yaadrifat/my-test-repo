package com.velos.base;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * <p>
 * An Application is a consumer of a messages. Once a parser parses an incoming
 * message, the message would normally be forwarded to an Application of some
 * sort (e.g. a lab system) which would process the message in some way
 * meaningful for it, and then return a response.
 * </p>
 * <p>
 * If you are wondering how to process the message, this is probably the place.
 * Create a class that implements Application,
 * </p>
 * For reference,Please see DefaultApplication Implementation
 * 
 * @see com.velos.base.DefaultApplication
 * @author Vishal Abrol
 */
public interface Application {

    /**
     * this method will provide the implemeting classes to write their
     * processing.
     * 
     * @author Vishal Abrol
     * @return status of processed message as int. returns -1 if processing
     *         failed.
     */
    public int processMessage(ArrayList in, InterfaceConfig cfg, Connection conn)
            throws ApplicationException;
}
