<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
<BODY>
   
  <%@ page language = "java" import = " oracle.sql.CLOB,java.sql.*,java.util.*,com.velos.eres.service.util.*, com.velos.eres.business.common.*"%>
  <%

	 
	String pkformaudit[] = request.getParameterValues("pk_formaudit");
	String reason[] = request.getParameterValues("FA_REASON");
		String eSign = request.getParameter("eSign");	
 

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

%>

<%
	int length = 0;
	
	if (pkformaudit != null)
	{
		length = pkformaudit.length;
	}
	
		Connection conn = CommonDAO.getConnection();
		 
	    conn.setAutoCommit(false);
	              
                   
        CallableStatement cs = null;    	
         cs = (CallableStatement) conn.prepareCall("begin sp_updateClob(?,?,?,?); end;");
            
	for (int ctr = 0; ctr <	length ; ctr++)
	{
		        

        try {
        	
        	CLOB clob_update = CommonDAO.getCLOB(reason[ctr], conn);
            
            cs.setObject(1, clob_update);
            cs.setObject(2, "er_formauditcol");
            cs.setObject(3, "fa_reason");
	        cs.setObject(4, " where pk_formauditcol = " + pkformaudit[ctr] );
    		cs.execute();
        
        } catch (Exception e) {
            Rlog.fatal("common",
                    "EJBUtil:Exception thrown in updating form auditcol reason "
                            + e.getMessage());
            System.out.println("message:"+e);
             
        }

		
	}
	
	try
	{
			conn.commit();
            // Close the Statement object
            cs.close();
            conn.close();
	} catch(Exception ex)
				{
					Rlog.fatal("common",
                    "EJBUtil:Exception thrown in closing connections"
                            + ex.getMessage());
            	System.out.println("message:"+ex);
            	
				}
	 
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	
 <p align="center"><button onClick="window.self.close();"><%=LC.L_Close%></button></p>

	  
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





