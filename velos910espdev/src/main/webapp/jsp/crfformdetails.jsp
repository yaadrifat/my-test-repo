<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Crf_FormData%><%--CRF Form Data*****--%></title>

<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<SCRIPT>
function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}

function openPrintWin(formId,filledFormId) {
   windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&linkFrom=C","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
}


</SCRIPT>
</head>

<%
String src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<%@page language = "java" import="java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.service.util.*,com.velos.eres.business.group.* " %>
 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<br>
<div class="formDefault" id="div1">

<%
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
 {
 
 	
 	int pageRight = 0;	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
	
    String studyId=request.getParameter("studyId");
    String statDesc=request.getParameter("statDesc");
    String statid=request.getParameter("statid");	
    String patProtId=request.getParameter("patProtId");
    String patientCode=request.getParameter("patientCode");
	String patientId = request.getParameter("pkey");
	String formFillDt = request.getParameter("formFillDt");
	if (formFillDt == null) formFillDt = "ALL";
    int patId=EJBUtil.stringToNum(patientId);	
	String formName="";
    
	String modRight = (String) tSession.getValue("modRight");
	int patProfileSeq = 0, formLibSeq = 0;

		 	// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);
	
	int filledFormUserAccessInt = lnkformsB.getFilledFormUserAccess(
		    StringUtil.stringToNum(request.getParameter("filledFormId")),
		    StringUtil.stringToNum(request.getParameter("formId")),
		    StringUtil.stringToNum((String)tSession.getAttribute("userId")) );
			

		 if ( ((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) 
				 && (pageRight >=4) && filledFormUserAccessInt > 0)
	{%>
	
	<P class="sectionHeadings"> <%=MC.M_MngPatsSch_LkdFrm%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> Linked Forms*****--%></P>
	
     <jsp:include page="patienttabs.jsp" flush="true"> 
     <jsp:param name="pkey" value="<%=patId%>"/>
     <jsp:param name="statDesc" value="<%=statDesc%>"/>
     <jsp:param name="statid" value="<%=statid%>"/>
     <jsp:param name="patProtId" value="<%=patProtId%>"/>
     <jsp:param name="patientCode" value="<%=patientCode%>"/>
     <jsp:param name="fromPage" value=""/>
     <jsp:param name="page" value="patientEnroll"/>
     </jsp:include>   
<br>
	 <%	
  		 String linkedFormId = request.getParameter("linkedFormId");
   		 String formId = request.getParameter("formId");
		 
		 if (formId==null) {
		 	 lnkformsB.setLinkedFormId(EJBUtil.stringToNum(linkedFormId));
		  	 lnkformsB.getLinkedFormDetails();
		 	 formId = lnkformsB.getFormLibId();
		 }
		 
	 	 formlibB.setFormLibId(EJBUtil.stringToNum(formId));
		 formlibB.getFormLibDetails();
		 formName=formlibB.getFormLibName();

	   	 String mode = request.getParameter("mode");
   		 String filledFormId = request.getParameter("filledFormId");
		 String entryChar = request.getParameter("entryChar");
		 String fkcrf = request.getParameter("fkcrf");
    	 String schevent = request.getParameter("schevent");
		 
		 String findStr = "";
		 StringBuffer sbuffer = null; 
		 int len = 0;
		 int pos = 0;

         String formHtml = "";
		 %>
		
		 <table width="100%">
		 <tr>
			 <td>
			 	 <P class="defComments"><b> <%=LC.L_Frm_Name%><%--Form Name*****--%>: </b><%=formName%></P>
			 </td>
			 <td>
		 		<A onclick="openPrintWin(<%=formId%>,<%=filledFormId%>)" href=#><img src="./images/printer.gif" border="0" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>"></img></A>
			 </td>
		</tr>
		</table>		
		 
		 <%
         if (mode.equals("N"))
         {
    	   formHtml =  lnkformsB.getFormHtmlforPreview(EJBUtil.stringToNum(formId));
         }
        else
        {
    	   formHtml =  lnkformsB.getFilledFormHtml(EJBUtil.stringToNum(filledFormId),"C");
        }

		
		//in case of view right, remove the submit button
		//in case of new/edit mode add a hidden element of mode.
		//according to this mode the pop-up messages of First time and Every time would be displayed
		if ((pageRight == 4) || (entryChar.equals("O")&& mode.equals("M")) ) {		
		   findStr = "<input type=\"image\" src=\"../images/jpg/Submit.gif\" align=\"absmiddle\" border=\"0\"/>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   if (pos > 0) {
		      sbuffer = new StringBuffer(formHtml);
		      sbuffer.replace(pos,pos+len,"");
		      formHtml = sbuffer.toString();
		   }
		   
    	   // Disable the Form tag for issue 4632
		   formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
	          .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
	          .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");
		   
		} else {
		   findStr = "</Form>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
	   //  by salil on 13 oct 2003 to see if the check box type multiple choice exists 
	  //  or not and to handle the case
	  // when no option is selected in a check box type multiple choice the element name does not 
	  	// be  carried forward to the next page hence the "chkexists"  variable is used to see 
		// if the page has a multiple choice field or not .     
	
		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }
		   
		   sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/> </Form>");
		   
		   formHtml = sbuffer.toString();
		}
 
 			   
       // set session attributes
	 	tSession.setAttribute("formDispLocation" ,"C");
		tSession.setAttribute("formFillMode" , mode);
		tSession.setAttribute("formId" , formId);
		tSession.setAttribute("srcmenu" , src);	

		tSession.setAttribute("entryChar" , entryChar);	
		tSession.setAttribute("fkcrf" , fkcrf);
		tSession.setAttribute("schevent" , schevent);
		tSession.setAttribute("formStudy" , studyId);		
		tSession.setAttribute("patientId" , patientId);	
		tSession.setAttribute("statDesc" , statDesc);
		tSession.setAttribute("statid" , statid);
		tSession.setAttribute("patProtId" , patProtId);
		tSession.setAttribute("patientCode" , patientCode);
		tSession.setAttribute("formFillDt" , formFillDt);		
		
		
		if (mode.equals("N"))
	    {
		}
		else
		{
			tSession.setAttribute("formFilledFormId" , filledFormId);
			tSession.setAttribute("formLibVer" , "0");				
		}	
		
		
 // end of session attributes		
		
 	
%>

<%=formHtml%>
<%

	}// end of if for page right 
	else
	{

	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	  <%

	} //end of else body for page right	
		
  } //end of if for session
 
 else 
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>



