<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.web.user.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*" %>

<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	

		<table class="dynFormTable" width="90%">

	   	<jsp:include page="include.jsp" flush="true"/> 
	
		<%
		
			HttpSession sessObject = request.getSession(true);
			
			if (sessionmaint.isValidSession(sessObject))
			{
				
					int accId= EJBUtil.stringToNum((String)sessObject.getAttribute("accountId"));
					 
			 	    int userId = EJBUtil.stringToNum((String) sessObject.getValue("userId"));
				    
				    UserJB userB = (UserJB) sessObject.getValue("currentUser");
			   	    int siteId = EJBUtil.stringToNum(userB.getUserSiteId());
			   	    
		 			String selType = "";
					int studyId = 0;
					String formSubmissionTypes = "";
		   			String appSubmissionType = "";
					
					
					appSubmissionType = request.getParameter("appSubmissionType");
					
					if (StringUtil.isEmpty(appSubmissionType))
					{
						appSubmissionType = "";
						
					}
					
					
					studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
					
					
					int study_acc_form_right = 0;
					int study_team_form_access_right = 0;
					
					study_acc_form_right = EJBUtil.stringToNum(request.getParameter("study_acc_form_right"));
					study_team_form_access_right = EJBUtil.stringToNum(request.getParameter("study_team_form_access_right"));
					
					
					int formPK = 0;
					String formlibver = "";
					String pkstudyforms = "";
					String formName = "";
					int respCount = 0;
					
					LinkedFormsDao linkFormDao = new LinkedFormsDao();
					int totalforms = 0;
					
					ArrayList arForms = new ArrayList();
					ArrayList arResponses = new ArrayList();
					ArrayList arFormNames = new ArrayList();
					
			 		
					formSubmissionTypes = EIRBDao.getIRBFormSubmissionTypes(appSubmissionType);	 						

		  			linkFormDao = lnkformB.getStudyFormsWithResponseData(accId, studyId, userId,siteId, study_acc_form_right, 		study_team_form_access_right, 
		            true, formSubmissionTypes, "");

					if (linkFormDao != null)
					{
					
						arForms = linkFormDao.getFormId();
				    	arResponses = linkFormDao.getArFormResponseDao();
				    	arFormNames = linkFormDao.getFormName();
				    	
				    	totalforms = arForms.size();
				    	
				    	if (totalforms == 0)
				    	{ %>
				    		<P class="sectionHeadings"><%=MC.M_NoFormsConfigured%><%--No forms configured*****--%></P>
				    		<%	
				    	}
				    	
				     
						
				    	for (int ctr= 0;ctr < totalforms; ctr++)
				    	{
				    		respCount = 0;
				    		FormResponseDao respD = new FormResponseDao();
				    			    	
				    		respD = (FormResponseDao) arResponses.get(ctr);
				    		
				    		formPK = ((Integer)arForms.get(ctr)).intValue();
				    		formName = (String) arFormNames.get(ctr);
					
							%>
								<tr><td colspan = 3><b><%=LC.L_Frm_Name%><%--Form Name*****--%>:</b> <%= formName%></td></tr>
								<tr><td colspan = 3>&nbsp;</td></tr> 
								<tr><td ><P class="sectionHeadings"><%=LC.L_Data_EntryDate%><%--Data Entry Date*****--%></P></td>
								<td ><P class="sectionHeadings"><%=LC.L_Form_Status%><%--Form Status*****--%></P></td>
								<td ><P class="sectionHeadings"><%=LC.L_View%><%--View*****--%></P></td>
								</tr>
								
								<%    		
				    		if (respD != null)
				    		{
				    				String fillDate = "";
				    				String pkResponseId = "";
				    				String respVersionNumber= "";
				    				String formStatusDesc = "";
				    				
				    				ArrayList arFillDates = new ArrayList();
				    				ArrayList arPkResponseIds = new ArrayList();
				    				ArrayList arVersionNumbers = new ArrayList();
				    				ArrayList arFormStatusDescs = new ArrayList();
				    				
					            	arFillDates =  respD.getArFilledFormDate();
					            	arPkResponseIds = respD.getArPKResponseID();
					            	arVersionNumbers = respD.getArVersionNumber();
									arFormStatusDescs = respD.getArFormStatusDesc();
									
									for (int respctr= 0;respctr < arFillDates.size(); respctr++)
							    	{
							    		respCount++;
							    		
							    		fillDate = 	(String)arFillDates.get(respctr)	;
							    		pkResponseId = 	(String)arPkResponseIds.get(respctr)	;
							    		respVersionNumber = 	(String)arVersionNumbers.get(respctr)	;
							    		formStatusDesc = (String) arFormStatusDescs.get(respctr);
							    		
							    		String	url = "formprint.jsp?formId="+formPK+"&filledFormId="+pkResponseId+"&studyId="+studyId+"&formLibVerNumber="+respVersionNumber+"&linkFrom=S";
		  			
		  								%>
		  									<tr> 
		  										<td><%= fillDate %> </td>	
		  										<td><%= formStatusDesc %> </td>	
		  									<td> <A href="#" onclick="openPrintWinCommon(<%=formPK%>,<%=pkResponseId%>,'SA',<%=studyId%>,'','','','',<%=respVersionNumber%>)">[<%=LC.L_View_Response%><%--View Response*****--%>]</A> </td>
		  									</tr>
		  									<%
									}

				    		}
				    		// check if respCount = 0
				    		if (respCount == 0)
				    		{
				    			%>
				    				<tr>	<td>&nbsp; </td> <td colspan = 2><P class="defComments"><%=MC.M_NoResponseEntered%><%--No Response Entered*****--%></P></td></tr>
				    				<%
				    		}
				    		%>
				    		<tr><td colspan = 3>&nbsp;</td></tr> 
				    		<tr><td colspan = 3>&nbsp;</td></tr> 	
				    			<%
				    	} 
					}
					
							
		  				
		  				
		  	} // end of session	check		
			%>
		
			</table>
		
			<div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>