<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- <style type="text/css">

TH {background-color:"#9A99FF";font-family: "Verdana, Arial, Helvetica,  sans-serif";color:white;font-size:13;height:"25"}
P.plwait { font-family: "arial";color:red;font-size:15; font-weight:bold;text-align:left; }
DIV.plwt {visibility:visible;margin-left:0px;position:relative;margin-top:0;width:350 px;}
</style>
-->



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<script>
	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit();
}
 function setFilterText(form){
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "")
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "")
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;

 }
 function openwindow()
{
	    windowName=window.open("viesettings.jsp?selectedTab=1","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=400,height=500 top=100,left=100 0,");
}
	</script>
<jsp:include page="viepanel.jsp" flush="true"/>

<title><%=LC.L_Search_Pats%><%--Search <%=LC.Pat_Patients%>*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
System.out.println("finally in VIemsgbrowse");
%>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>


<DIV>
<%
  HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{


   	     String selectedTab = "" ;
   	     String filter="";
   	     String dateselect="", datespec="", fromdate="",todate="";

String uName = (String) tSession.getValue("userName");
String mode=request.getParameter("mode");
if (mode.equals("initial")){

//filter="where TO_DATE(msg_date,'mm/dd/yyyy')>=(SYSDATE-7)" ;
filter="where (msg_date)>=(SYSDATE-7)" ;
dateselect="spec";
datespec="-7";

}else {
 dateselect=request.getParameter("dateselect");

	if (dateselect.equals("spec")){
		 datespec=request.getParameter("datespec");
//		filter="where TO_DATE(msg_date,'mm/dd/yyyy')>=(SYSDATE"+datespec+")" ;
		filter="where (msg_date)>=(SYSDATE"+datespec+")" ;
		}   else {
			 fromdate=request.getParameter("from");
			 todate=request.getParameter("to");
//filter= "where (trunc(TO_DATE(msg_date,'mm/dd/yyyy'))>=TO_DATE('"+fromdate+ "','mm/dd/yyyy')) and (trunc(TO_DATE(msg_date,'mm/dd/yyyy'))<=TO_DATE('"+todate+ "','mm/dd/yyyy'))";
filter= "where (trunc((msg_date))>=TO_DATE('"+fromdate+ "','mm/dd/yyyy')) and (trunc((msg_date))<=TO_DATE('"+todate+ "','mm/dd/yyyy'))";
System.out.println(filter);

	}
}


/*String sql = " select sum(Success) as success_count,sum(Failure) as failure_count,sum(warning) as warning_count,sum(unprocess) as Unprocessed,sum(unsupported) as Unsupported,"+
		 "(sum(Success)+sum(failure)+sum(unsupported)) Total_processed,(sum(Success)+sum(failure)+sum(unprocess)+sum(unsupported)) as Total_messages,msgDate,sending_app from "+
		"(select  DECODE(status,'1',1,0) as Success, DECODE(status,'0',1,0) as Unprocess,DECODE(status,'-9',1,0) as Unsupported,"+
		"DECODE(status,'-2',1,0) as warning,DECODE(status,'-1',1,0) as Failure,"+
		"TO_CHAR(msg_date,'mm/dd/yyyy') as msgDate,sending_app	from vie_message " + filter + ")	group by msgDate,sending_app order by msgDate desc" ;*/
String sql="select TO_CHAR(msg_date,'mm/dd/yyyy') as msg_date,sending_app,success_count,Unprocessed,Unsupported,warning_count,failure_count"+
",groupby from (select  trunc(msg_date) as msg_date,sending_app,count(DECODE(status,'1',1)) as success_count,"+
"count(DECODE(status,'0',1)) as Unprocessed,count(DECODE(status,'-9',1)) as Unsupported,"+
"count(DECODE(status,'-2',1)) as warning_count,count(DECODE(status,'-1',1)) as failure_count, grouping(sending_app) groupby"+
  " from vie_message group by rollup(trunc(msg_date),sending_app)) "+ filter + " and groupby = 0 order by 1 desc" ;
System.out.println(sql);

String countsql=" select count(*) from (select TO_CHAR(msg_date,'mm/dd/yyyy') as msg_date,sending_app,success_count,Unprocessed,Unsupported,warning_count,failure_count"+
",groupby from (select  trunc(msg_date) as msg_date,sending_app,count(DECODE(status,'1',1)) as success_count,"+
"count(DECODE(status,'0',1)) as Unprocessed,count(DECODE(status,'-9',1)) as Unsupported,"+
"count(DECODE(status,'-2',1)) as warning_count,count(DECODE(status,'-1',1)) as failure_count, grouping(sending_app) groupby"+
  " from vie_message group by rollup(trunc(msg_date),sending_app)) "+ filter + " and groupby = 0 order by 1 desc)"  ;
/*    String countsql= "select count(*) from( "+
    	 " select sum(Success) as success_count,sum(Failure) as failure_count,sum(warning) as warning_count,sum(unprocess) as Unprocessed,sum(unsupported) as Unsupported,"+
		 "(sum(Success)+sum(failure)+sum(unsupported)) Total_processed,(sum(Success)+sum(failure)+sum(unprocess)+sum(unsupported)) as Total_messages,msgDate,sending_app from "+
		"(select  DECODE(status,'1',1,0) as Success, DECODE(status,'0',1,0) as Unprocess,DECODE(status,'-9',1,0) as Unsupported,"+
		"DECODE(status,'-2',1,0) as warning,DECODE(status,'-1',1,0) as Failure,"+
		"TO_CHAR(msg_date,'mm/dd/yyyy') as msgDate,sending_app	from vie_message " + filter + ")	group by msgDate,sending_app order by msgDate desc)";*/


			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;

			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;

			Configuration conf = new Configuration();
			rowsPerPage = conf.getRowsPerBrowserPage(conf.ERES_HOME +"eresearch.xml");
			totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");

			BrowserRows br = new BrowserRows();


            br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();

		     startPage = br.getStartPage();

			hasMore = br.getHasMore();

			hasPrevious = br.getHasPrevious();


			totalRows = br.getTotalRows();


			firstRec = br.getFirstRec();

			lastRec = br.getLastRec();


%>











  <Form name="msgbrowse" method="post" action="viemsgbrowse.jsp?page=1&mode=M">

    <br>

 <!--<Form  name="search" method="post" action="allPatient.jsp">-->
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">

<span class="pagebanner">
 <table class="displaytag" style="border:0px;solid">

<tr>
<td width="5%"><input type="radio" value="spec" checked name="dateselect"></td>
<td width="20%" class="tdDefault"><%=LC.L_Select_AnOption%><%--Select an Option*****--%>&nbsp;&nbsp;</td>
<td width="20%">
<select size="1" name="datespec">
<option selected value="-30"><%=LC.L_All%><%--All*****--%></option>
<option value="-7"><%=LC.L_LastOne_Week%><%--Last One Week *****--%></option>
<option value="-14"><%=LC.L_Last2_Weeks%><%--Last 2 Weeks *****--%></option>
<option value="-21"><%=LC.L_Last3_Weeks%><%--Last 3 Weeks*****--%></option>
<option value="-30"><%=LC.L_Last_OneMonth%><%--Last One month*****--%></option>
</select>
</td></tr>
<tr>
<td width="2%"><input type="radio" value="range" name="dateselect"></td>
<td width="15%" class="tdDefault"><%=LC.L_Specify_Range%><%--Specify range*****--%></td>
<td class="tdDefault"><%=LC.L_From%><%--From*****--%>: <input type="text" name="from" size="10">(<%=LC.L_MmDdYyyy_Lower%><%--mm/dd/yyyy*****--%>)</td>
<td class="tdDefault"><%=LC.L_To%><%--To*****--%>:<input type="text" name="to" size="10">(<%=LC.L_MmDdYyyy_Lower%><%--mm/dd/yyyy*****--%>)</td>
<td class="tdDefault"><button type="submit"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button> </td>
</tr>
  </table>
</span>
<!--</Form>-->

<input type="hidden" name="selectedTab" Value="<%=selectedTab%>">
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="page" value="<%=curPage%>">
<Input type="hidden" name="orderType" value="<%=orderType%>">

<%
//Retrieve study count for this user
/*	PatStudyStatDao pstudydao = new PatStudyStatDao();
	pstudydao = studyB.getStudyPatientResults(EJBUtil.stringToNum(""), "", EJBUtil.stringToNum("") ,EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId) );
     ArrayList patientCodes=   pstudydao.getPatientCodes();
     int patcodes= patientCodes.size() ;



	//end retrieve study count*/


/*	PatientDao pdao = new PatientDao();

	pdao = person.getPatients(EJBUtil.stringToNum(siteId),patCode,418);*/

	ArrayList studyList=new ArrayList();


//	patientResults = pdao.getPatient();

	if (totalRows <= 0 ) {

%>

	<P class="defComments"><%=MC.M_NoMsg_SrchCrit%><%--No messages found for your search criteria. Please modify the criteria and click on ''Go''*****--%></P>

<%

	} else {

	   int i = 0;

	   //int lenPatient = rowsReturned ;

	   //lenPatient = patientResults.size();

	   if(rowsReturned == 0) {

%>

	<P class="defComments"><%=MC.M_NoMsg_SrchCrit%><%--No messages found for your search criteria. Please modify the criteria and click on ''Go''*****--%></P>

<%

	} else {

//	   PersonStateKeeper psk = null;








	%>





    <table class="displaytag" >
 <tr>
	<th width=10% ><%=LC.L_Client_Name%><%--Client Name*****--%></th>

   	<th width=10%><%=LC.L_Msg_Date%><%--Message Date*****--%></th>

    <th width=10%><%=LC.L_Total_Msgs%><%--Total Messages*****--%></th>

	<th width=10%><%=LC.L_Processed%><%--Processed*****--%></th>

  	<th width=10%><%=LC.L_Success%><%--Success*****--%></th>

  	<th width=10%><%=LC.L_Failure%><%--Failure*****--%></th>
  	<th width=10%><%=LC.L_Warning%><%--Warning*****--%></th>

  	<th  width=10%><%=LC.L_Unprocessed%><%--Unprocessed*****--%></th>

  	<th  width=10%><%=LC.L_Unsupported%><%--Unsupported*****--%></th>
    <th></th>





   </tr>

<%


        String tempPercode="" ;
        String creator="" ;
            int study_counter = 0 ,processcount=0,total=0;
            String msgDate="";
            String successcount = "" ,failurecount="",unprocesscount="",unsupportcount="",sending_app="",warningcount="";

		for(i = 1 ; i <=rowsReturned ; i++)

	  	{
	  	    sending_app= br.getBValues(i,"sending_app")	 		   ;
	  	    successcount = br.getBValues(i,"success_count")	 		   ;
	  	    failurecount = br.getBValues(i,"failure_count")	 		   ;
            unprocesscount=  br.getBValues(i,"unprocessed")	 		   ;
            unsupportcount=    br.getBValues(i,"unsupported")	 		   ;
            warningcount= br.getBValues(i,"warning_count")	 		   ;
            msgDate=br.getBValues(i,"msg_date")	 		   ;
            processcount=EJBUtil.stringToNum(successcount)+EJBUtil.stringToNum(failurecount)+EJBUtil.stringToNum(unsupportcount);
            total=EJBUtil.stringToNum(successcount)+EJBUtil.stringToNum(failurecount)+EJBUtil.stringToNum(unsupportcount)+EJBUtil.stringToNum(unprocesscount);

//            processcount=    br.getBValues(i,"total_processed")	 		   ;


//            total=  br.getBValues(i,"total_messages")	 		   ;



//			if (psk!= null)

//			{








	 		   if ((i%2)==0) {

%>

      <tr class="even">

<%

	 		   }else{

%>

      <tr class="odd">

<%

	 		   }
if (sending_app!=null){
	if (sending_app.length()==0) sending_app="-";
} else {
	sending_app="-";
}
%>
      <td width =10%><%=sending_app%></td>
        <td width =10%><%=msgDate%></td>
        <td width =10%><%=total%></td>
        <td width =10%><%=processcount%></td>
        <td width =10%><%=successcount%></td>
        <td width =10%><%=failurecount%></td>
        <td width =10%><%=warningcount%></td>
         <td width =10%><%=unprocesscount%></td>
        <td width =10%><%=unsupportcount%></td>
        <td width=10%><A href="viemsglist.jsp?clientname=<%=sending_app%>&filterdate=<%=msgDate%>&status=All"><%=LC.L_View%><%--View*****--%></A></td>




      </tr>

	  <%

//	  		} //end for temppatient

	  }// end for for loop

	  } // end of if for length == 0

  } //end of if for the check of patientResults == null

	  %>

    </table>
    	<table class="displaytag" style="border:0px;">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>

	<table align="center">
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="viemsgbrowse.jsp?selectedTab=1&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&dateselect=<%=dateselect%>&datespec=<%=datespec%>&from=<%=fromdate%>&to=<%=todate%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="viemsgbrowse.jsp?selectedTab=2&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&dateselect=<%=dateselect%>&datespec=<%=datespec%>&from=<%=fromdate%>&to=<%=todate%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="viemsgbrowse.jsp?selectedTab=2&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&dateselect=<%=dateselect%>&datespec=<%=datespec%>&from=<%=fromdate%>&to=<%=todate%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>





  </Form>

 </div>
 	<%}else{%>
<br>
<br>
<br>
<br>
<br>

<p class = "sectionHeadings" align = center> <%=MC.M_YourSessExpired_Relogin%><%--Your session has Expired.Please Relogin.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=viehome.jsp">


<%}%>

 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>
</body>

</html>

