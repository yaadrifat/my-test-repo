<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Std_BcastNotify%><%--<%=LC.Std_Study%> >> Broadcast/ Notify*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT Language="javascript">
	function  validate(formobj, pubFlag)
	{
		if (!(validate_col('e-Signature',formobj.eSign))) return false

		if(isNaN(formobj.eSign.value) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}

	}
</SCRIPT>
<% String src;
src= request.getParameter("srcmenu");
String from = "notify";
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>

<%@ page import="com.velos.eres.service.util.*" %>

<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>
<DIV class="BrowserTopn" id="divtab">
  <jsp:include page="studytabs.jsp" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
  </jsp:include>
</DIV>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
   <DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
	<%} else {%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"><%}


   	HttpSession tSession = request.getSession(true);
   	if (sessionmaint.isValidSession(tSession))
	{
		String study = null;
		String uName = (String) tSession.getValue("userName");
		study = (String) tSession.getValue("studyId");
    	int pageRight = 0;
		if(study == "" || study == null) {
		%>
			<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		<%
		} else {
	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
				pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYNOTIFY"));
		   	}
			if (pageRight > 4 )
			{
				int studyId = EJBUtil.stringToNum(study);
			%>
			<%
				studyB.setId(EJBUtil.stringToNum(study));
				studyB.getStudyDetails();
				if (studyB.getStudyPubFlag().equals("N"))
				{
			%>
					<Form name="mpublic" id="mpublicfrm" method="post" action="makeStudyPublic.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" onSubmit="if (validate(document.mpublic,'<%=studyB.getStudyPubFlag()%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
						<table width="100%" cellspacing="0" cellpadding="0" border=0 >
						    	<tr height="30"><td>&nbsp;</td></tr>
							<tr align="center">
								<td>
						       <b><%=MC.M_StdNotPublic_Cur%><%--Your <%=LC.Std_Study_Lower%> is not public currently.*****--%> <BR><%=MC.M_SendNotfic_RelStdNetwork%><%--In order to send the notification you need to first release your <%=LC.Std_Study_Lower%> to your network by clicking on the button below.*****--%><BR>
            						</b><BR><BR>
            					</td>
							</tr>
						</table>
							<jsp:include page="submitBar.jsp" flush="true">
									<jsp:param name="displayESign" value="Y"/>
									<jsp:param name="formID" value="mpublicfrm"/>
									<jsp:param name="showDiscard" value="N"/>
							</jsp:include>
					</Form>
			<%
				} else {   //else of if for study public flag
			%>
					<Form name="notify" id="notifyfrmid" method="post" action="sendNotification.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" onSubmit = "if (validate(document.notify,'<%=studyB.getStudyPubFlag()%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
						<table width="100%" cellspacing="0" cellpadding="0" border=0 >
							<tr height="30"><td>&nbsp;</td></tr>
							<tr align="center">
								<td>
						       <b>
									<%=MC.M_EmailNotfic_ThrpArea%><%--You can send e-mail notification about your <%=LC.Std_Study_Lower%> to all subscribers of your <%=LC.Std_Study_Lower%>'s Therapeutic Area.*****--%>
								</b>	<BR>
						            <BR>
								</td>
							</tr>
						</table>

					<jsp:include page="submitBar.jsp" flush="true">
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="notifyfrmid"/>
							<jsp:param name="showDiscard" value="N"/>
					</jsp:include>
					</Form>
				<%
				} //end of if for study public flag
				%>
  <!--for saving study in blob -->
				<!-- <Form name="studysave" method="post" action="savetoblob.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" >
					<table width="100%" cellspacing="0" cellpadding="0" border=0 >
						<tr >
							<td> <br>
								<p class = "sectionHeadings">Take <%=LC.Std_Study%> Snapshot</p>
								<P class = "defComments">
									<br>To provide View access to an external user, you must take a snapshot of your <%=LC.Std_Study%>.   When you give View access to an external user, he would be able to see last saved snapshot.
									<BR>
						            <BR>
						        </P>
							</td>
						</tr>
						<tr>
					        <td height="10">
								<button type="submit"><%=LC.L_Search%></button>
							  	<BR>
					            <BR>
								<BR>
					            <BR>
							</td>
						</tr>
					</table>
				</Form>	 -->
  <!-- end of change-->
  <%
			} //end of if body for page right
			else
			{
			%>
				<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
			} //end of else body for page right
		} //end of if body for check on studyId
	}//end of if body for session
	else
	{
	%>
  		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

