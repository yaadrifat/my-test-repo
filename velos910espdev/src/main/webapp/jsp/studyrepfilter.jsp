<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<title><%=LC.L_Std_Rpts%><%--<%=LC.Std_Study%> Reports*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>

function fMakeVisible(type){

	typ = document.all("year");

	typ.style.visibility = "visible";

}



function fValidate(formobj){

	reportChecked=false;

	for(i=0;i<formobj.reportNumber.length;i++){

		sel = formobj.reportNumber[i].checked;

		if (formobj.reportNumber[i].checked){

	   		reportChecked=true;

		}

	}

	if (reportChecked==false) {

		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/

		return false;

	}

	reportNo = formobj.repId.value;


	if ((formobj.studyPk.value == "") && (reportNo != "97")) {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

		return false;

	}






	switch (reportNo) {

		case "44": //Protocol Calendar Template

			if (formobj.study1.value == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}



			if (formobj.protId.value == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

		break;



		case "21": //Estimated Study Budget

			if (formobj.study1.value == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}



			if (formobj.protId.value == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

		break;

		case "97": //Study Dump
			return true;

	 default:

 			if (formobj.study1.value == "") {

 				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}

			break;

	}

}



function fSetId(ddtype,formobj){

	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them

		for(i=0;i<formobj.reportNumber.length;i++)	{

			if (formobj.reportNumber[i].checked){

				lsReport = formobj.reportNumber[i].value;

				ind = lsReport.indexOf("%");

				formobj.repId.value = lsReport.substring(0,ind);

				formobj.repName.value = lsReport.substring(ind+1,lsReport.length);

				break;

			}

		}

	}



	if (ddtype == "study1") {
		i=formobj.study1.options.selectedIndex;
		formobj.val.value = formobj.study1.options[i].text;
		formobj.studyPk.value = formobj.study1.options[i].value;
		formobj.protId.value = "";
		formobj.selProtocol.value="None";

	}



//	if (ddtype == "study2") {
//		formobj.selProtocol.value = "None";
//		formobj.protId.value = "";
//		i=formobj.study2.options.selectedIndex;
//		formobj.val.value = formobj.study2.options[i].text;
//		formobj.studyPk.value = formobj.study2.options[i].value;
//	}



}



</SCRIPT>



<SCRIPT language="JavaScript1.1">

function openProtWindow(formobj) {

	lstudyPk = formobj.studyPk.value;

	if ((lstudyPk == "") || (lstudyPk==null) ) {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

			return false;

	}



	if ((lstudyPk == "  ") || (lstudyPk==null) ) {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

			return false;

	}





	rpType='all';

	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+rpType,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=200");

	windowName.focus();

}



</SCRIPT>





</head>







<% String src="";



src= request.getParameter("srcmenu");



%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>



<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>





<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>

<br>



<!-- <DIV class="formDefault" id="div1"> -->
<DIV class="browserDefault" id="div1">



<%



   HttpSession tSession = request.getSession(true);



   if (sessionmaint.isValidSession(tSession))



   {


    String report_id="",report_name="",report_type="",repid_name="";

	String uName =(String) tSession.getValue("userName");

	String userId = (String) tSession.getValue("userId");

 	String acc = (String) tSession.getValue("accountId");

	String tab = request.getParameter("selectedTab");

	    if ((request.getParameter("study_rep").length())>0)  {
	        	repid_name=request.getParameter("study_rep");
	      }

	     report_id =repid_name.substring(0,(repid_name.indexOf("%")));
	     report_name=repid_name.substring((repid_name.indexOf("%")+1),repid_name.lastIndexOf("%"));
	     report_name=report_name.replace('~',' ');
	     report_type=repid_name.substring((repid_name.lastIndexOf("%") + 1));

	int counter=0;

	int studyId=0;

	StringBuffer study=new StringBuffer();

	StudyDao studyDao = new StudyDao();

	studyDao.getReportStudyValuesForUsers(userId);



	study.append("<SELECT NAME=study1 onChange=fSetId('study1',document.reports)>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){

		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");

	}

	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study.append("</SELECT>");



	%>

	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>


<Form Name="reports" method="post" action="repRetrieve.jsp" target="_blank">



<Input type="hidden" name="val" value="">
<input type="hidden" name="type" >
<Input type="hidden"  name="protId" value="">
<Input type=hidden name="repId" value="<%=report_id%>" >
<Input type=hidden name="repName" value="<%=report_name%>">
<Input type=hidden name="id">
<Input type=hidden name="studyPk">





<P class = "userName">

	<%= uName %>

</P>



<!-- <jsp:include page="reporttabs.jsp" flush="true"/>   -->
<input type="hidden" name="srcmenu" value='<%=src%>'>
<input type="hidden" name="selectedTab" value='<%=tab%>'>


<!-- <HR class=blue></hr> -->


         <Table width=100%>
         <tr>
         	 <td width="10%">
         	<p class = "reportHeadings"><A href="repmain.jsp?selectedTab=1&srcmenu=tdmenubaritem8" type="submit"><%=LC.L_Back%></A></p>
         	</td>
         	 <td>
         	<p class = "reportHeadings"><%=LC.L_Rep_StdRpt%><%--Report >> Study Reports*****--%></p>
         	</td>
         	</tr>
		</table>


 <table width="100%">
<tr>
	<th width="35%"><Font class="reportText"><%=LC.L_Report%><%--Report*****--%></font></th>
	<th width="25%"><Font class="reportText"><%=LC.L_Required_Filters%><%--Required Filters*****--%></font></th>
	<th width="40%"><Font class="reportText"><%=LC.L_Available_Filters%><%--Available Filters*****--%></font></th>
 </tr>
</table>


<!-- <table width="100%" cellspacing="0" cellpadding="0" border=0 >

	<tr>

		<td colspan=3>

			<p class = "reportHeadings"><%=LC.Std_Study%> Protocol Reports</p>

		</td>

	</tr>

	<tr>

		<td width=40%> -->

			<%

			int accId = EJBUtil.stringToNum(acc);

			CtrlDao ctrl = new CtrlDao();

			ctrl.getControlValues("studyspecific");

			String catId = (String) ctrl.getCValue().get(0);

			repdao.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIds= repdao.getPkReport();

			ArrayList names= repdao.getRepName();

		   	ArrayList desc= repdao.getRepDesc();

			ArrayList repFilterStudy = repdao.getRepFilters();

			int len = repIds.size() ;

			CtrlDao ctrl1 = new CtrlDao();

			ctrl1.getControlValues("protcalspecific");

			catId = (String) ctrl1.getCValue().get(0);

			repdao1.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIdsProtCal= repdao1.getPkReport();

			ArrayList namesProtCal= repdao1.getRepName();

		   	ArrayList descProtCal= repdao1.getRepDesc();

			ArrayList repFilterProt = repdao1.getRepFilters();

			int lenProtCal = repIdsProtCal.size() ;

			int repId=0;

			String name="";
%>

<table width="100%">
	<tr>
	<td width="60%">
		<table>
         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_Std_PcolRpts%><%--<%=LC.Std_Study%> Protocol Reports*****--%></p>
         	</td>
         	</tr>
    <%
			for(counter = 0;counter<len;counter++){
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%
		}
         	%><td width="35%"><%
			repId = ((Integer)repIds.get(counter)).intValue();
				name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
                if (report_id.equals((repIds.get(counter)).toString())) {
				%>
				<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick="fSetId('report',document.reports)" checked> <%=name%>
				<% } else { %>
				<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick="fSetId('report',document.reports)"> <%=name%>
				<%}%>
			</td>
			<td width="25%"><%=repFilterStudy.get(counter)%></td>
		</tr>

			<%}%>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=MC.M_Pcol_CalRpts%><%--Protocol Calendar Specific Reports*****--%></p>
         	</td>
         	</tr>

         	<% for(counter = 0;counter < lenProtCal;counter++)
         	{
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%
		}
         	%><td width="35%"><%
	repId = ((Integer)repIdsProtCal.get(counter)).intValue();
	name=((namesProtCal.get(counter)) == null)?"-":(namesProtCal.get(counter)).toString();
            if (report_id.equals((repIdsProtCal.get(counter)).toString())) {
				%>
				<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick=fSetId('report',document.reports) checked> <%=name%>
			<%} else { %>
				<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick=fSetId('report',document.reports)> <%=name%>
			<%}%>
			</td>
			<td width="25%"><%=repFilterProt.get(counter)%></td>
		</tr>
         	<%}%>
         </table>
</td>

 <td width="40%" valign="top">
	<table><tr>
         	<td><br><br><Font class=reportText><%=LC.L_Filter%><%--Filter*****--%>:</Font></td></tr>
		<tr>
	       	<td><Font class=comments><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>:</Font></td>
	       	<td> <%=study%></td>
		</tr>

		<tr>
	       	<td><A HREF=# onClick="openProtWindow(document.reports)"><%=LC.L_Select_PcolCal%><%--Select Protocol Calendar*****--%>:</A></td>
	       	<td><Input type=text name=selProtocol size=20 READONLY value=None></td>
		</tr>


		<tr>
			<td colspan="2" align="center">
			<br><br>
		<!--	<A onClick = "return fValidate(document.reports)" href="#"><input type="image"   src="../images/jpg/displayreport.jpg" align="absmiddle" border=0></A>-->
			<input type="image"  onClick = "return fValidate(document.reports)" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0>
			</td>
		</tr>


         </table>
</td></table>

</form>

<%} //end of if session times out



else



{



%>



<jsp:include page="timeout.html" flush="true"/>



<%



}



%>





<div>



<jsp:include page="bottompanel.jsp" flush="true"/>



</div>





</div>







<DIV class="mainMenu" id = "emenu">



  <jsp:include page="getmenu.jsp" flush="true"/>



</DIV>







</body>



</html>

