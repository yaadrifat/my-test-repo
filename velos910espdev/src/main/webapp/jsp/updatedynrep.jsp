<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>



<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>







<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>







<Link Rel=STYLESHEET HREF="common.css" type=text/css>     







<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>

<jsp:include page="skinChoser.jsp" flush="true"/>





<BODY>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>
<jsp:useBean id="dynrepdt" scope="request" class="com.velos.eres.web.dynrepdt.DynRepDtJB"/>
<jsp:useBean id="dynrepfltr" scope="request" class="com.velos.eres.web.dynrepfltr.DynRepFltrJB"/>
<!--sonika -->
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<!--end sonika -->
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.*,com.velos.eres.business.person.*"%>

<%
String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {

%>

  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

	} else {

 	//String studyId = (String) tSession.getValue("studyId");
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String accId=(String) tSession.getValue("accountId");
// dSAVE DATA
	//declare variables
	int dynPK= 0,arraySize=0,dynfltrPK=0;	
	String mode = null;
	//Declare varibale
	int id=0,pers=0;
	String repId="";
	String formId="";
	String repName="";
	String repDesc="";
	String repFilter="";
	String repOrder="";
	String headAlign="";
	String footAlign="";
	String pkStr="";
	String repHdr="";
	String repFtr="";
	String repType="";
	String creator="";
	String modifiedBy="";
	HashMap attributes=new HashMap();
	HashMap TypeAttr=new HashMap();
	HashMap FldAttr=new HashMap();
	HashMap FltrAttr=new HashMap();
	HashMap RepAttr=new HashMap();
	DynRepDao dynDao=new DynRepDao();
	CommonDAO cDao=new CommonDAO();
	attributes=(HashMap)tSession.getAttribute("attributes");
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap) attributes.get("TypeAttr");
	if (attributes.containsKey("FldAttr")) FldAttr=(HashMap) attributes.get("FldAttr");
	if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap) attributes.get("FltrAttr");
	if (attributes.containsKey("RepAttr")) RepAttr=(HashMap) attributes.get("RepAttr");
	String repCol="",repSeq="",repWidth="",repFormat="",repDisp="",repColName="",repColDataType="",perId="",studyId="",fltrName="";
	String src = request.getParameter("srcmenu");
	int fltrId=-1;
	if (TypeAttr.containsKey("repMode")) mode = (String)TypeAttr.get("repMode");
	if (TypeAttr.containsKey("formId")) formId = (String)TypeAttr.get("formId");
	if (TypeAttr.containsKey("dynType")) repType = (String)TypeAttr.get("dynType");
	System.out.println("*************************repMode***************"+mode);
	//report information-available in previous screen
	repId=request.getParameter("repId");
	repName=request.getParameter("repName");
	repDesc=request.getParameter("repDesc");
	repFilter=request.getParameter("repFilter");
	if (repFilter==null) repFilter="";
	if (repFilter.length()==0) {
	if (FltrAttr.containsKey("filter")) repFilter=(String)FltrAttr.get("filter");
	}
	if (repFilter==null) repFilter="";
	
	/*VA -filter is set to undefined if none specified in filter page*/
	repFilter=(repFilter.equals("undefined")?"":repFilter);
	
	
	repOrder=request.getParameter("dataOrder");
	repHdr=request.getParameter("repHeader");
	repHdr=(repHdr==null)?"":repHdr;
	repFtr=request.getParameter("repFooter");
	repFtr=(repFtr==null)?"":repFtr;
	headAlign=request.getParameter("headerAlign");
	headAlign=(headAlign==null)?"[VELCENTER]":headAlign;
	footAlign=request.getParameter("footerAlign");
	footAlign=(footAlign==null)?"[VELCENTER]":footAlign;
	if (repHdr.length() >0) repHdr=headAlign+":"+repHdr;
	if (repFtr.length() >0) repFtr=footAlign+":"+repFtr;
	//end
	repType=(repType==null)?"":repType;
	
	repType=((repType.trim()).equals("study"))?"S":repType;
	repType=((repType.trim()).equals("pat"))?"P":repType;
	if (repType==null) repType="";
	
	if (repType.equals("P")){
	if (TypeAttr.containsKey("perId")) perId = (String)TypeAttr.get("perId");
	perId=(perId==null)?"":perId;
	if (TypeAttr.containsKey("studyId")) studyId = (String)TypeAttr.get("studyId");
	studyId=(studyId==null)?"":studyId;
	}
	if (repType.equals("S")){
	if (TypeAttr.containsKey("studyId")) studyId = (String)TypeAttr.get("studyId");
	studyId=(studyId==null)?"":studyId;
	}
	//int rows=EJBUtil.stringToNum(request.getParameter("rows"));
	ArrayList fldColIdList=new ArrayList();
	ArrayList fldColList=new ArrayList();
	ArrayList fldNameList=new ArrayList();
	ArrayList fldDispList=new ArrayList();
	ArrayList fldFormatList=new ArrayList();
	ArrayList fldOrderList=new ArrayList();
	ArrayList fldWidthList=new ArrayList();
	ArrayList fldSeqList=new ArrayList();
	ArrayList fldTypeList=new ArrayList();
	
	//ArrayLists for filter screens
	ArrayList afiltColId=new ArrayList();
	ArrayList afiltCol=new ArrayList();
	ArrayList afiltQf=new ArrayList();
	ArrayList afiltData=new ArrayList();
	ArrayList afiltExclude=new ArrayList();
	ArrayList afiltBbrac=new ArrayList();
	ArrayList afiltEbrac=new ArrayList();
	ArrayList afiltOper=new ArrayList();
	ArrayList afiltSeq=new ArrayList();
	String afiltName="";
	String afiltId="";
	
	
    //get the values from session attribute FLTRATTR
        
	if (FltrAttr.containsKey("fltrId")) afiltId=(String)FltrAttr.get("fltrId");
	if (FltrAttr.containsKey("fldNameSelList")) afiltCol=(ArrayList)FltrAttr.get("fldNameSelList");
	if (FltrAttr.containsKey("fltrDtIdList")) afiltColId=(ArrayList)FltrAttr.get("fltrDtIdList");
	if (FltrAttr.containsKey("scriteriaList")) afiltQf=(ArrayList)FltrAttr.get("scriteriaList");
	if (FltrAttr.containsKey("fldDataList")) afiltData=(ArrayList)FltrAttr.get("fldDataList");
	if (FltrAttr.containsKey("excludeList")) afiltExclude=(ArrayList)FltrAttr.get("excludeList");
	if (FltrAttr.containsKey("startBracList")) afiltBbrac=(ArrayList)FltrAttr.get("startBracList");
	if (FltrAttr.containsKey("endBracList")) afiltEbrac=(ArrayList)FltrAttr.get("endBracList");
	if (FltrAttr.containsKey("extendList")) afiltOper=(ArrayList)FltrAttr.get("extendList");
	if (FltrAttr.containsKey("fltrName")) afiltName=(String)FltrAttr.get("fltrName");
	
	
	//end ArrayLists
	
	if (FldAttr.containsKey("sessColId")) fldColIdList = (ArrayList)FldAttr.get("sessColId");
	if (FldAttr.containsKey("sessCol")) fldColList = (ArrayList)FldAttr.get("sessCol");
	if (FldAttr.containsKey("sessColName")) fldNameList = (ArrayList)FldAttr.get("sessColName");
	if (FldAttr.containsKey("sessColDisp")) fldDispList = (ArrayList)FldAttr.get("sessColDisp");
	if (FldAttr.containsKey("sessColWidth")) fldWidthList = (ArrayList)FldAttr.get("sessColWidth");
	if (FldAttr.containsKey("sessColSeq")) fldSeqList = (ArrayList)FldAttr.get("sessColSeq");
	if (FldAttr.containsKey("sessColType")) fldTypeList = (ArrayList)FldAttr.get("sessColType");
	if (FldAttr.containsKey("sessColFormat")) fldFormatList = (ArrayList)FldAttr.get("sessColFormat");
	
	//fldFormatList=EJBUtil.strArrToArrayList(fldFormat);
	
	String sharedWith="";
	String sharedWithIds="";
	String objNumber =request.getParameter("objNumber");
	String fkObj = request.getParameter("fkObj");
	sharedWith=request.getParameter("rbSharedWith");
	sharedWithIds="";

	if(sharedWith.equals("P"))
	{
		sharedWithIds=request.getParameter("selUsrId");
	}
	else if(sharedWith.equals("A"))
	{
		sharedWithIds=request.getParameter("selAccId");
	}
	else if(sharedWith.equals("G"))
	{
		sharedWithIds=request.getParameter("selGrpIds");
	}
	else if(sharedWith.equals("S"))
	{
		sharedWithIds=request.getParameter("selStudyIds");
	}
	else if(sharedWith.equals("O"))
	{
	 sharedWithIds=request.getParameter("selOrgIds");
	}	
	
	
// got values
System.out.println("repMode"+ mode);
	int saved = 0;
	if (mode.equals("M"))
	{
		dynrep.setId(EJBUtil.stringToNum(repId));
		dynrep.getDynRepDetails();
		dynrep.setFormId(formId);
		dynrep.setFormIdStr(formId);
		dynrep.setRepName(repName);
		dynrep.setRepDesc(repDesc);
		dynrep.setRepFilter("");
		dynrep.setRepOrder(repOrder);
		dynrep.setRepHdr(repHdr);
		dynrep.setRepFtr(repFtr);
		dynrep.setUserId(usr);
		dynrep.setAccId(accId);
		dynrep.setModifiedBy(usr);
		dynrep.setIpAdd(ipAdd);
		dynrep.setRepType(repType);
		dynrep.setStudyId(studyId);
		dynrep.setPerId(perId);
		dynrep.setShareWith(sharedWith);
		saved=dynrep.updateDynRep();
		
	    if (saved>=0){
	    	System.out.println("repId"+repId+"\nfldColIdList"+fldColIdList+"\nfldColList"+fldColList+"\nfldNameList" + fldNameList+"\nfldTypeList"+fldTypeList+"\nfldSeqList"+fldSeqList+"\fldWidthList"+fldWidthList+"\nfldFormatList"+fldFormatList+"\nfldDispList"+fldDispList);
	       saved=dynrepdt.updateAllCols(fldColIdList,repId,fldColList,fldSeqList,fldWidthList,fldFormatList,fldDispList,usr,ipAdd,fldNameList,fldTypeList,formId);
		   
		   //sonika
    		   
		   
		   fkObj=repId;
		objShareB.setIpAdd(ipAdd);
         	objShareB.setCreator(usr);
         	objShareB.setObjNumber(objNumber);//number of module
         	objShareB.setFkObj(fkObj);// formId, CalId, reportId
         	objShareB.setObjSharedId(sharedWithIds);// shared ids
         	objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id 
         	objShareB.setRecordType(mode);
         	objShareB.setModifiedBy(usr);	
         	int ret = objShareB.setObjectShareDetails();
		    System.out.println("sonika ret " + ret);
		   //end sonika
	       }
	       
	}	
	else
	{
		dynrep.setFormId(formId);
		dynrep.setFormIdStr(formId);
		dynrep.setRepName(repName);
		dynrep.setRepDesc(repDesc);
		dynrep.setRepFilter("");
		dynrep.setRepOrder(repOrder);
		dynrep.setRepHdr(repHdr);
		dynrep.setRepFtr(repFtr);
		dynrep.setUserId(usr);
		dynrep.setAccId(accId);
		dynrep.setCreator(usr);
		dynrep.setIpAdd(ipAdd);
		dynrep.setRepType(repType);
		dynrep.setStudyId(studyId);
		dynrep.setPerId(perId);
		dynrep.setShareWith(sharedWith);
		saved=dynrep.setDynRepDetails();
	if (saved>=0){
		dynPK=dynrep.getId();
		pkStr=(new Integer(dynrep.getId())).toString();
		repId=pkStr;
		
		
		
	   //sonika	
		   //String shareWithObjNum = (String) tSession.getAttribute("shareWithObjNum");		   			
		   //String sharedWith = (String) tSession.getAttribute("shareWithSelected");
		  // String sharedWithIds = (String) tSession.getAttribute("shareWithIds");			   
		   
		   //in new mode fk_obj will come after the parent record is saved 		   
		fkObj=repId;   		
		   
		    objShareB.setIpAdd(ipAdd);
         	objShareB.setCreator(usr);
         	objShareB.setObjNumber(objNumber);//number of module
         	objShareB.setFkObj(fkObj);// formId, CalId, reportId
         	objShareB.setObjSharedId(sharedWithIds);// shared ids
         	objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id 
         	objShareB.setRecordType(mode);
         	objShareB.setModifiedBy(usr);	
         	int ret = objShareB.setObjectShareDetails();
		    System.out.println("sonika ret " + ret);

 		   //end sonika
		
		
		
		System.out.println("**********dynID returned" + dynPK);
		saved=dynrepdt.setAllCols(pkStr,fldColList,fldSeqList,fldWidthList,fldFormatList,fldDispList,usr,ipAdd,fldNameList,fldTypeList,formId);

	}
	

	
	}
	
	//update the filter
	if (saved>=0){
	fltrId=EJBUtil.stringToNum(afiltId);
	System.out.println("********************Starting with FltrId***" +fltrId);
	if (fltrId>0){
	dynrepfltr.setId(fltrId);
	dynrepfltr.getDynRepFltrDetails();
	}
	dynrepfltr.setRepId(repId);
	if (fltrId>0){
	dynrepfltr.setModifiedBy(usr);
	}else{
	dynrepfltr.setCreator(usr);
	}
	dynrepfltr.setIpAdd(ipAdd);
	if (fltrId>0){ 
	if (afiltName.length()>0) dynrepfltr.setFltrName(StringUtil.decodeString(afiltName));
	}else {
	dynrepfltr.setFltrName(afiltName);
	}
	
	System.out.println("********************RepFilter before Update***" +repFilter);
	//dynrepfltr.setFltrStr(repFilter);
	
	if (fltrId>0){
	System.out.println("********************Definitely an update***" +fltrId);
	saved=dynrepfltr.updateDynRepFltr();
	System.out.println("will update CLOB to ID in update-saved" + saved);
	if (saved>=0){
	System.out.println("will update CLOB to ID in update" + fltrId + "repfilter" + repFilter );
	repFilter=StringUtil.encodeString(repFilter);
	System.out.println("return in update for updateClob"+cDao.updateClob(repFilter,"er_dynrepfilter","dynrepfilter_filter","where pk_dynrepfilter= " +fltrId));
	}
	} else if (fltrId==0) {
	System.out.println("********************Definitely an insert***" +fltrId + "repFltr" + repFilter + "filtname" + afiltName);
	if (((repFilter.trim()).length()>0) || ((afiltName.trim()).length()>0)){ //this is done in case nothing was selected in filter screen
	saved=dynrepfltr.setDynRepFltrDetails();
	if (saved>=0){
	System.out.println("will update CLOB to ID in update" + dynrepfltr.getId()+ "repfilter" + repFilter);
	repFilter=StringUtil.encodeString(repFilter);
	System.out.println("return from previous one" + cDao.updateClob(repFilter,"er_dynrepfilter","dynrepfilter_filter","where pk_dynrepfilter= " +dynrepfltr.getId()));
	}
	}
	}
	dynfltrPK=dynrepfltr.getId();	
	}
	System.out.println("********************saved returned from previous one***" +saved);
	if (saved>=0){
	
	System.out.println("I WILL TRY TO UPDATE filtrDetails now" +saved);
	dynDao.setFiltCol(afiltCol);
	dynDao.setFiltQf(afiltQf);
	dynDao.setFiltData(afiltData);
	dynDao.setFiltExclude(afiltExclude);
	dynDao.setFiltBbrac(afiltBbrac);
	dynDao.setFiltEbrac(afiltEbrac);
	dynDao.setFiltOper(afiltOper);
	dynDao.setFiltSeq(afiltSeq);
	//dynDao.setFiltName(afiltName);
	dynDao.setFiltId((new Integer(dynfltrPK)).toString());
	//String afiltId=dynDao.getFiltId();
	dynDao.setIpAdd(ipAdd);
	dynDao.setUsr(usr);
	dynDao.setRepId(repId);
	System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@afiltColId\n" + afiltColId);
	dynDao.setFltrColIds(afiltColId);
	dynDao.setFilterDetails(dynDao,"N");
	}
	
	//end ypdate filter
  if (saved >= 0)
 {
tSession.removeAttribute("attributes"); 
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">

<%

}
else
{
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%></p>
<%
}
}//end of if for eSign check
}//end of if body for session
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>


