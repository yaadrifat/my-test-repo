var January = L_January;
var February = L_February;
var March = L_March;
var April = L_April;
var May = L_May;
var June = L_June;
var July = L_July;
var August = L_August;
var September = L_September;
var October = L_October;
var November = L_November;
var December = L_December;
var Su = L_Su;
var Mo = L_Mo;
var Tu = L_Tu;
var We = L_We;
var Th = L_Th;
var Fr = L_Fr;
var Sa = L_Sa;

/*monthString = Array("","January","February","March","April","May","June","July","August","September","October","November","December");*****/
monthString = Array("",January, February, March, April, May, June, July, August, September, October, November, December);
 
var beginYr = 1850;
var finalYr = 2100;
var temp1;
var temp2;
var temp3;
var temp4;
var temp5;
 
function processTemps(temp1,temp2) {
       if ((temp2==1)||(temp2==3)||(temp2==5)||(temp2==7)||(temp2==8)||(temp2==10)||(temp2==12)) temp3=31
  else if ((temp2==4)||(temp2==6)||(temp2==9)||(temp2==11)) temp3=30
  else if ((((temp1 % 100)==0) && ((temp1 % 400)==0)) || (((temp1 % 100)!=0) && ((temp1 % 4)==0))) temp3 = 29
  else temp3 = 28;
  return temp3;
};
 
function selectMonth(monthActual,day) {
  var selectedMonth = "";
  selectedMonth = "<select name='temp2' size='1' onChange='javascript:opener.monthTemp2(self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value,self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value,"+ day+");'>\r\n";
  for (var i=1; i<=12; i++) {
    selectedMonth = selectedMonth + "  <option value='" + i + "'";
    if (i == monthActual) selectedMonth = selectedMonth + " selected";
    selectedMonth = selectedMonth + ">" + monthString[i] + "</option>\r\n";
  }
  selectedMonth = selectedMonth + "</select>\r\n";
  return selectedMonth;
}
 
function selectYear(yearActual,day) {
  var selectedYear = "";
  
  selectedYear = "<select name='temp1' size='1' onChange='javascript:opener.monthTemp2(self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value,self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value,"+ day+");'>\r\n";
  for (var i=beginYr; i<=finalYr; i++) {
    selectedYear = selectedYear + "  <option value='" + i + "'";
    if (i == yearActual) selectedYear = selectedYear + " selected";
    selectedYear = selectedYear + ">" + i + "</option>\r\n";
  }
  selectedYear = selectedYear + "</select>";
  return selectedYear;
}
 
function createDayTable(year,month,d) {

  var thetable = "<table border='0' cellpadding='2' cellspacing='0' bgcolor='#CFC8FF'>\r\n  <tr>";
  var dateObj = new Date();
  dateObj.setYear(year);
  dateObj.setMonth(month-1);
  dateObj.setDate(1);
  justDay = dateObj.getDay();
  /*thetable = thetable + "\r\n    <td align='center' >Su</td><td align='center'>Mo</td><td align='center'>Tu</td><td align='center'>We</td><td align='center'>Th</td><td align='center'>Fr</td><td align='center'>Sa</td></div>\r\n  <tr>";*****/
  thetable = thetable + "\r\n    <td align='center' >"+Su+"</td><td align='center'>"+Mo+"</td><td align='center'>"+Tu+"</td><td align='center'>"+We+"</td><td align='center'>"+Th+"</td><td align='center'>"+Fr+"</td><td align='center'>"+Sa+"</td></div>\r\n  <tr>";
  for (var j=1; j<=justDay; j++) {
    thetable = thetable + "\r\n    <td></td>";
  }
  
  var cday = fetchDay();

  if (d!= "")
  {
	  cday = d;
  }

  
  for (var i=1; i<10; i++) {
    thetable = thetable + "\r\n    <td"
	
    if (i == cday) thetable = thetable + " bgcolor='#ff0000'";
	else
	thetable = thetable + " bgcolor='#CFC8FF'";
	
    thetable = thetable + "><input type='button' value='0" + i + "' onClick='javascript:opener.temp1=self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value; opener.temp2=self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value; opener.temp3=" + i + "; self.close();'></td>";
    if (((i+justDay) % 7)==0) thetable = thetable + "\r\n  </tr>\r\n\  <tr>";
  }
  for (var i=10; i<=processTemps(year,month); i++) {
    thetable = thetable + "\r\n    <td"
    if (i == cday) thetable = thetable + " bgcolor='#ff0000'";
	else
	thetable = thetable + " bgcolor='#CFC8FF'";

    thetable = thetable + "><input type='button' value='" + i + "' onClick='javascript:opener.temp1=self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value; opener.temp2=self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value; opener.temp3=" + i + "; self.close();'></td>";
    if (((i+justDay) % 7)==0) thetable = thetable + "\r\n  </tr>\r\n\  <tr>";
  }
  thetable = thetable + "\r\n  </tr>\r\n</table>";
  return thetable;
}
 
function monthTemp2(year,month,day) {
  var html = ""; 
  html = html + "<html>\r\n<head>\r\n  <title>" + temp5 + "</title>\r\n</head>\r\n<body bgcolor='#CFC8FF' onUnload='opener.setDateStr();'>\r\n  <div align='center'>\r\n  <form name='Forma1'>\r\n";
  html = html + "<table width = '200'><tr><td  bgcolor = '#CFC8FF'>" + selectMonth(month,day) + "</td><td  bgcolor = '#CFC8FF'>";
  html = html + selectYear(year,day) + "</td></tr></table>";
  html = html + "<br>" + createDayTable(year,month,day);
//  html = html + "<center><p><input type='button' name='hoy' value='today: " + month + "/" + curDay + "/" + year + "' onClick='javascript:opener.temp2 = "+ month + "; opener.temp3 = "+ curDay + "; opener.temp1 = "+ year + "; self.close();'></center>";
  html = html + "\r\n  </form>\r\n  </div>\r\n</body>\r\n</html>\r\n";
  ventana = open("#","calendario","width=550,height=260");
  ventana.document.open();
  ventana.document.writeln(html);
  ventana.document.close();
  ventana.focus();
}
 
function fetchYear() {
  var dtObj = new Date();
  if (navigator.appName == "Netscape") return dtObj.getYear() + 1900
  else return dtObj.getYear();
}
 
function fetchMonth() {
  var dtObj = new Date();
  return dtObj.getMonth()+1;
}
 
function fetchDay() {
  var dtObj = new Date();
  return dtObj.getDate();
}
 
function calculateVal(to,from) {
  temp1 = fetchYear();
  temp2 = fetchMonth();
  temp3 = fetchDay();
  
  temp4 = to;
  temp5 = from;
  
  seldate= to.value;

  if(seldate==null || seldate=='') 
  {
    	curYr = fetchYear();
  	    curMon = fetchMonth();
        curDay = fetchDay();
  }
  else
  {        
   	curMon = seldate.substring(0,seldate.indexOf("/"));
	curDay = seldate.substring(seldate.indexOf("/") + 1,seldate.lastIndexOf("/"));
	curYr= seldate.substring(seldate.lastIndexOf("/") + 1,seldate.length);
  }
  
 
  temp1 = "";
  temp2 = "";
  temp3 = "";
  
  monthTemp2(curYr,curMon,curDay);
}
 
function setDateStr() {
	if (temp2 != "")
	{
	  temp4.value =  temp2 + "/" + temp3 + "/" + temp1;
	}
//  alert(temp4.value );
}
 
