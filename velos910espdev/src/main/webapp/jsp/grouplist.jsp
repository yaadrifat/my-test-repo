<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_MngAcc_MultiGrp%><%-- Manage Account >> Multiple Groups*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
 


<SCRIPT Language="javascript">

 function  validate(formobj){

     

		mycount=0;

	 for(cnt = 0; cnt<5;cnt++){ 
		if((formobj.groupName[cnt].value=="")){
		mycount++;
		}}

		if(mycount==5){
		alert("<%=MC.M_AlteastOne_GrpName%>");/*alert("Please Enter alteast one Group Name");*****/ 
		formobj.groupName[0].focus();
		return false;
		} 

	


	 for(cnt = 0; cnt<5;cnt++){ 
		//Ankit:  Bug-10718 Date- 17-July-2012
		formobj.groupName[cnt].value = formobj.groupName[cnt].value.replace(/^\s+|\s+$/g,""); 
		if((formobj.groupName[cnt].value=="")&&(formobj.groupDesc[cnt].value!="")){
		alert("<%=MC.M_Etr_GrpName%>");/*alert("Please Enter Group Name");*****/ 
		formobj.groupName[cnt].focus();
		return false;
		}
	 }

	
	for(count=0;count<5;count++)
	{
		for(count1=count+1;count1<5;count1++){

		if((formobj.groupName[count].value==formobj.groupName[count1].value)&&(formobj.groupName[count].value!="")){
			alert("<%=MC.M_TwoGrps_CntSimilar%>");/*alert("two groups cannot be similar");*****/
			formobj.groupName[count1].focus();
			return false;
			}
		}

	}	


//	if (!(validate_col('e-Signature',formobj.eSign))) return false

//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect e-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
//   }
   }

</SCRIPT>

 

<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<body>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="2"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 
  <%

	int grpId  =0;

	String grpName ="";

	String grpDesc ="";

	String accId="";

	String mode="";

	HttpSession tSession = request.getSession(true);        

	if (sessionmaint.isValidSession(tSession))

	{


		String uName = (String) tSession.getValue("userName");

	    	grpId = EJBUtil.stringToNum(request.getParameter("grpId"));

		mode = request.getParameter("mode");

		String fromPage = request.getParameter("fromPage");

		int pageRight = 0;

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));		

			UserJB user = (UserJB) (tSession.getValue("currentUser"));
			accId = user.getUserAccountId();

	
	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
	{

%>
    <Form name="group" id="groupFrmId" method="post" action="savegrouplist.jsp" onSubmit="if (validate(document.group)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_Etr_GrpDets%><%-- Please enter Group Details*****--%>: </P>
        </td>
      </tr>
    </table>
    <P class = "sectionHeadings"> 
      <%if (mode.equals("M")) {%>
      <A href="groupRights.jsp?mode=M&srcmenu=<%=src%>&groupId=<%=grpId%>&groupName=<%=grpName%>"><%=LC.L_Assign_GrpRights%><%-- Assign 
      Group Rights*****--%></A> 
      <%}%>
    </P>
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
    	<tr>
		<th width=30%><%=LC.L_Group_Name%><%-- Group Name*****--%> <FONT class="Mandatory">* </FONT> </th>
		<th width=40%><%=LC.L_Description%><%-- Description*****--%></th>
		</tr>

	<tr><td colspan="2">&nbsp </td></tr>
	<% for (int i = 0; i< 5; i++) { %>	
	  <tr> 
	
        <td align="center">
		<%--
		<FONT class="Mandatory">* </FONT>  
		--%>
          <input type="text" name="groupName" size = 35 MAXLENGTH = 50 	value="" onblur="this.value = this.value.replace(/^\s+|\s+$/g,'')">
        </td>
		  <td align="center">	
          <input type="text" name="groupDesc" size = 50 MAXLENGTH = 50 	value="">
	  </td>	
	
      </tr>
	<% } %>
	
      </tr>
      <tr> 
        <td colspan="2"> 
          <input type="hidden" name="groupId" MAXLENGTH = 15 value="<%=grpId%>">
	 	<input type="hidden" name="totrows" value=5>

        </td>
      </tr>
      <tr> 
        <td colspan="2"> 
          <input type="hidden" name="groupAccId" MAXLENGTH = 15 value="<%=accId%>">
        </td>
      </tr>
    </table>
	<table width="98%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" >
	<!--   	<td width="50%" align="right" bgcolor="#cccccc"> <span id="eSignMessage"></span>
				e-Signature <FONT class="Mandatory">* </FONT>
	   </td>

	   <td width="20%" bgcolor="#cccccc">
		<input type="password" name="eSign" id="eSign" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')">
	    </td>	-->
	   
	   
	   <jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="groupFrmId"/>
			<jsp:param name="showDiscard" value="N"/>
	   </jsp:include>
	   
	 	<tr><td>&nbsp;</td></tr>
</table>

    <input type="hidden" name="mode" MAXLENGTH = 15 value="<%=mode%>">
    <input type="hidden" name="src" MAXLENGTH = 15 value="<%=src%>">
    <input type="hidden" name="fromPage" MAXLENGTH = 15 value="<%=fromPage%>">	
    
    <table width="98%" >
      <tr> 
        <td align="right"> 		
         <!-- <input type="Submit" name="submit" value="Submit"> -->
	 <!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="left " border="0">-->	
        </td>
      </tr>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

