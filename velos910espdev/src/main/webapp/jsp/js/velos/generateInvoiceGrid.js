/**
 * AK:Added file for enhancement PCAL-20071
 */
 function includeJS(file)
{

  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;

  document.getElementsByTagName('head').item(0).appendChild(script);

}

/* include any js files here */

// Introduced for localization
includeJS('js/velos/velosUtil.js');
//includeJS('js/yui/build/event-simulate/event-simulate.js');
var CommErr_CnctVelos= M_CommErr_CnctVelos; /*Storing Value from MC.jsp to local var*/
// Define generateInvoiceGrid object. "VELOS" is a namespace so that the object can be called globally
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var PlsEnterEsign=M_PlsEnterEsign;

var myDataTable;
var myDialog = null;
var pgRight=0;
var lastTimeSubmitted = 0;
var calledFrom=null;
var editorSaved = false;
var formatterFail = false;
var formatterFailTwo = false;
var formatterFailThree = false;
var selectedCell = null;
var respJ = null; // response in JSON format
var twoDecimalNumber = {
	//prefix:prefix,
	decimalPlaces:2,
	//decimalSeparator:decimalSeparator,
	thousandsSeparator:','//thousandsSeparator,
	//suffix:suffix
};
var covTypeLegend =''; 

VELOS.generateInvoiceGrid = function (url, args) {
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value);
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; 
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.generateInvoiceGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		this.dataTable = args.dataTable ? args.dataTable : 'generateInvoiceGrid'; // name of the div to hold generateInvoiceGrid
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			alert(CommErr_CnctVelos);/*alert(svrCommErr);*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
		//Getting all common JSOn data for all rows
		var myColumnDefs = [];					
		myColumnDefs = VELOS.generateInvoiceGrid.processColumns(respJ,respJ.colArray);
		
		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);		
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields:respJ.fieldsArray
		};
		var maxWidth = 800;
		var maxHeight = 600;
		var isIe = jQuery.browser.msie;
		
		if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = (isIe==true?1000:1100); }

		if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = (isIe==true?600:670); ; }
		
		//alert('maxWidth:'+maxWidth+' maxHeight:'+maxHeight);

		var calcHeight = respJ.dataArray.length*30 + 40;
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }

		myDataTable = new YAHOO.widget.DataTable(
				this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:"99%",
				height:maxHeight+"px",
				scrollable:true
			}
		);		
		
		covTypeLegend = respJ.covTypeLegend;

		//Formatting Milestone Amount in Desc of Service column
		var tabLength= myDataTable.getRecordSet().getLength();
		var serviceCells= $D.getElementsByClassName('yui-dt-col-descService', 'td');
		for (var iX=0; iX<tabLength; iX++) {
			var el = new YAHOO.util.Element(serviceCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var parentId = parent.get('id');
			
			var serviceText = el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			var tempServiceText = serviceText.substring(0, 
					serviceText.indexOf(L_Amount+": ") + (L_Amount+": ").length);
			var amount = serviceText.substring(tempServiceText.length, serviceText.length);
			amount = YAHOO.util.Number.format(amount, twoDecimalNumber); 
			
			serviceText = tempServiceText + amount;
			el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=serviceText;

			var mileAmountTd = parent.getElementsByClassName('yui-dt-col-mileAmount', 'td')[0];
			mileAmountTd.style.textAlign ='right';
			var prevInvoicedAmountTd = parent.getElementsByClassName('yui-dt-col-prevInvoicedAmount', 'td')[0];
			prevInvoicedAmountTd.style.textAlign ='right';
			var invoiceAmountTd = parent.getElementsByClassName('yui-dt-col-invoiceAmount', 'td')[0];
			invoiceAmountTd.style.textAlign ='right';
		}
			
		//Display button when there are milestone achievements
		if (respJ.dataArray.length > 0){
			var tfoot = myDataTable.getTbodyEl().parentNode.createTFoot();
			var tr = tfoot.insertRow(-1);
			tr.onclick =function(){
				myDataTable.saveCellEditor();
				if(formatterFail){
					myDataTable._oCellEditor.value = '';
					myDataTable.onEventShowCellEditor({target:selectedCell}); 
					if(e) YAHOO.util.Event.stopEvent(e);
					formatterFailTwo=false;
					formatterFailThree=false;
					return false;
				}
			};
			tr.onmouseover = function(){
				return nd();
			}
			var th = tr.appendChild(document.createElement('th'));
			th.colSpan = 10;
			th.innerHTML = "<button type='submit' id='generateInvoice' name='generateInvoice'" 
					+ "onclick='if(f_check_perm_noAlert("+pgRight+",\"N\") || f_check_perm_noAlert("+pgRight+",\"E\")){return VELOS.generateInvoiceGrid.generateInvoice(event);}'" 
					+ " >Generate Invoice</button>";

			var th = tr.appendChild(document.createElement('th'));
			th.colSpan = 2;
			th.innerHTML = '<div class="yui-dt-liner"><b>Total</b></div>';
			th.valign='right';
			th.halign='middle';

			var td = tr.appendChild(document.createElement('th'));
			td.id = 'runningTotalMileTd';
			//td.innerHTML = respJ["totalMileAmount"];
			td.innerHTML = '<div class="yui-dt-liner" style="text-align: right;"><b>'
				+'<span id="runningTotalMileSpan" name="runningTotalMileSpan"></span>'
				+'<input type="hidden" id="runningTotalMileAmount" name ="runningTotalMileAmount" value=""/>'
				+'</b></div>';

			var td = tr.appendChild(document.createElement('th'));
			td.id = 'runningTotalPrevInvoicedTd';
			//td.innerHTML = respJ["totalPrevInvoicedAmount"];
			td.innerHTML = '<div class="yui-dt-liner" style="text-align: right;"><b>'
				+'<span id="runningTotalPrevInvoicedSpan" name="runningTotalPrevInvoicedSpan"></span>'
				+'<input type="hidden" id="runningTotalPrevInvoicedAmount" name ="runningTotalPrevInvoicedAmount" value=""/>'
				+'</b></div>';

			var td = tr.appendChild(document.createElement('th'));
			td.id = 'runningTotalInvoiceTd';
			//td.innerHTML = respJ["totalInvoiceAmount"];
			td.innerHTML = '<div class="yui-dt-liner" style="text-align: right;"><b>'
				+'<span id="runningTotalInvoiceSpan" name="runningTotalInvoiceSpan"></span>'
				+'<input type="hidden" id="runningTotalInvoiceAmount" name ="runningTotalInvoiceAmount" value=""/>'
				+'</b></div>';

			var th = tr.appendChild(document.createElement('th'));
			th.colSpan = 2;
		}
		$j(function() {
			if ($j("input:submit")){
				$j( "input:submit").button();
			}
			if ($j("a:submit")){
				$j( "a:submit").button();
			}
			if ($j("button")){
				$j("button").button();
			}
		});	

		myDataTable.subscribe("editorBlurEvent",function(oArgs) { 
			myDataTable.onEventSaveCellEditor;
			var ev = oArgs.event;
			selectedCell = oArgs.editor.getTdEl();
			if(formatterFail){//Bug#10298 : To focus the cell on clicking other cell
				formatterFailTwo=false;
				formatterFailThree=false;
				myDataTable._oCellEditor.value = '';
				myDataTable.onEventShowCellEditor({target:selectedCell}); 
			}   
		}); 
		myDataTable.subscribe('editorSaveEvent',function(oArgs) {
			editorSaved = true;
			var elCell = oArgs.editor.getTdEl();
			selectedCell = elCell;
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;			
			var ev = oArgs.event;
			var oRecord = this.getRecord(elCell);			
			var column =  this.getColumn(elCell);
				
			if (column.key=="invoiceAmount"){
				var mileAmount = parseFloat(oRecord.getData('mileAmount'));
				var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
				var oldInvoiceAmount = parseFloat(oOldData);
				var newInvoiceAmount = parseFloat(oNewData);
				
				if (newInvoiceAmount < 0){
					alert(M_InvAmtNt_NegVal);
					oRecord.setData(column.key, oOldData);
					this.updateCell(oRecord, column.key, oOldData, false);
					this.showCellEditor(elCell);
					return false;
				}
				
				if (mileAmount < newInvoiceAmount){
					alert(M_InvAmtGt_MstoneAmt);
					oRecord.setData(column.key, oOldData);
					this.updateCell(oRecord, column.key, oOldData, false);
					this.showCellEditor(elCell);
					return false;
				}
				var totalInvoicedAmount = newInvoiceAmount + prevInvoicedAmount;
				if (mileAmount < totalInvoicedAmount){
					alert(M_TotInvAmtGt_MstoneAmt);
					oRecord.setData(column.key, oOldData);
					this.updateCell(oRecord, column.key, oOldData, false);
					this.showCellEditor(elCell);
					return false;
				}

				var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
				if ((isNaN(newInvoiceAmount) || newInvoiceAmount == 0) && (isNaN(oldInvoiceAmount) || oldInvoiceAmount == 0)){
					var runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);
					runningTotalInvoice = isNaN(runningTotalInvoice)?0:runningTotalInvoice;
					
					var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
					if (runningTotalInvoiceSpan){
						runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
					}
				}

				if (oldInvoiceAmount != newInvoiceAmount){
					newInvoiceAmount = (newInvoiceAmount)? newInvoiceAmount:0;
					oldInvoiceAmount = (oldInvoiceAmount)? oldInvoiceAmount:0;

					if (runningTotalInvoiceFld){
						var runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);
						runningTotalInvoice = isNaN(runningTotalInvoice)?0:runningTotalInvoice;
						if (runningTotalInvoice){
							runningTotalInvoice-= oldInvoiceAmount;
							runningTotalInvoice+= newInvoiceAmount;
						} else {
							runningTotalInvoice = newInvoiceAmount;
						}
						runningTotalInvoice = Math.round(runningTotalInvoice*100)/100
						runningTotalInvoiceFld.value = runningTotalInvoice;
						var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
						if (runningTotalInvoiceSpan){
							runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
						}
					}
					var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
					if (runningTotalMilestoneFld){
						var runningTotalMilestone = parseFloat(runningTotalMilestoneFld.value);
						runningTotalMilestone = isNaN(runningTotalMilestone)?0:runningTotalMilestone;
						if (newInvoiceAmount > 0 && oldInvoiceAmount <= 0){
							if (runningTotalMilestone){
								runningTotalMilestone+= mileAmount;
							} else {
								runningTotalMilestone = mileAmount;
							}
						} else { //bug #12256
							if (newInvoiceAmount == 0 && oldInvoiceAmount > 0){
								var runningTotalMilestone = parseFloat(runningTotalMilestoneFld.value);
								if (runningTotalMilestone){
									runningTotalMilestone-= mileAmount;
								}
							}
						}
						runningTotalMilestone = Math.round(runningTotalMilestone*100)/100;
						runningTotalMilestoneFld.value = runningTotalMilestone;
						var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
						if (runningTotalMileSpan){
							runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
						}
					}
					var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
					if (runningTotalPrevInvoiceFld){
						var runningTotalPrevInvoice = parseFloat(runningTotalPrevInvoiceFld.value);
						runningTotalPrevInvoice = isNaN(runningTotalPrevInvoice)?0:runningTotalPrevInvoice;
						if (newInvoiceAmount > 0 && oldInvoiceAmount <= 0){
							if (runningTotalPrevInvoice){
								runningTotalPrevInvoice+= prevInvoicedAmount;
							} else {
								runningTotalPrevInvoice = prevInvoicedAmount;
							}
						}
						runningTotalPrevInvoice = Math.round(runningTotalPrevInvoice*100)/100;
						runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
						var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
						if (runningTotalPrevInvoicedSpan){
							runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
						}
					}
				}
			}
		});

		// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
		myDataTable.subscribe('cellClickEvent',function(oArgs) {
			var target = oArgs.target;
			var column = this.getColumn(target);
			var oRecord = this.getRecord(target);
			var mileAchieveId = parseInt(oRecord.getData('mileAchieveId'));
			if (column.key=="mileAmount" || column.key=="prevInvoicedAmount") return false;
				
			var ev = oArgs.event;
			if(formatterFail){//Bug#10298 : To focus the cell on clicking other cell
				formatterFailTwo=false;
				formatterFailThree=false;
				myDataTable._oCellEditor.value = '';
				myDataTable.onEventShowCellEditor({target:selectedCell}); 
				return false;
			}
			
			switch(column.action){
				case 'calculate':
				if (f_check_perm(pgRight,'N')){
					if (editorSaved){
						editorSaved = false;
						return;
					}
					var mileAmount = parseFloat(oRecord.getData('mileAmount'));
					var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
					var oldInvoiceAmount = parseFloat(oRecord.getData('invoiceAmount'));
					var newInvoiceAmount =  Math.round((mileAmount-prevInvoicedAmount)*100)/100;
			
					if (newInvoiceAmount >= 0){
						if (oldInvoiceAmount != newInvoiceAmount){
							oldInvoiceAmount = (oldInvoiceAmount)? oldInvoiceAmount:0;
							
							oRecord.setData('invoiceAmount',newInvoiceAmount);
							this.updateCell(oRecord, "invoiceAmount", newInvoiceAmount, false);
				
							var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
							if (runningTotalInvoiceFld){
								var runningTotalInvoice = 0, runningTotalMilestone = 0, runningTotalPrevInvoice = 0;
								if (runningTotalInvoiceFld.value != ""){
									runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);	
								}
								if (runningTotalInvoice){
									if (oldInvoiceAmount){
										runningTotalInvoice-= oldInvoiceAmount;
									}
									runningTotalInvoice+= newInvoiceAmount;
								}else{
									runningTotalInvoice = newInvoiceAmount;
								}
								runningTotalInvoice = Math.round(runningTotalInvoice*100)/100;
								runningTotalInvoiceFld.value = runningTotalInvoice;
								var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
								if (runningTotalInvoiceSpan){
									runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
								}
				
								if (newInvoiceAmount > 0 && (!oldInvoiceAmount || oldInvoiceAmount <= 0)){
									//alert('newInvoiceAmount:'+newInvoiceAmount+' oldInvoiceAmount:'+oldInvoiceAmount);
				
									var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
									if (runningTotalMilestoneFld){
										if (runningTotalMilestoneFld.value != ""){
											runningTotalMilestone = parseFloat(runningTotalMilestoneFld.value);	
										}
										runningTotalMilestone+= mileAmount;
										runningTotalMilestone = Math.round(runningTotalMilestone*100)/100;
										runningTotalMilestoneFld.value = runningTotalMilestone;
										var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
										if (runningTotalMileSpan){
											runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
										}
									}
									var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
									if (runningTotalPrevInvoiceFld){
										if (runningTotalPrevInvoiceFld.value != ""){
											runningTotalPrevInvoice = parseFloat(runningTotalPrevInvoiceFld.value);	
										}
										runningTotalPrevInvoice+= prevInvoicedAmount;
										runningTotalPrevInvoice = Math.round(runningTotalPrevInvoice*100)/100;
										runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
										
										var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
										if (runningTotalPrevInvoicedSpan){
											runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
										}
									}
								}
							}
						}
					}
				}
				return false;
				break;
			case 'erase':
				if (f_check_perm(pgRight,'N')){
					var mileAmount = parseFloat(oRecord.getData('mileAmount'));
					var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
					var oldInvoiceAmount = parseFloat(oRecord.getData('invoiceAmount'));
					var newInvoiceAmount = null;
			
					if (oldInvoiceAmount != newInvoiceAmount){
						oldInvoiceAmount = (oldInvoiceAmount)? oldInvoiceAmount:0;

						oRecord.setData('invoiceAmount',newInvoiceAmount);
						this.updateCell(oRecord, "invoiceAmount", newInvoiceAmount, false);
			
						var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
						if (runningTotalInvoiceFld){
							var runningTotalInvoice = 0;
							if (runningTotalInvoiceFld.value != ""){
								runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);	
							}
							if (oldInvoiceAmount > 0){
								if (oldInvoiceAmount > 0){
									runningTotalInvoice-= oldInvoiceAmount;
								}
							
								runningTotalInvoice = Math.round(runningTotalInvoice*100)/100;
								runningTotalInvoiceFld.value = runningTotalInvoice;
								var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
								if (runningTotalInvoiceSpan){
									runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
								}
	
								var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
								if (runningTotalMilestoneFld){
									if (runningTotalMilestoneFld.value != ""){
										runningTotalMilestone = parseFloat(runningTotalMilestoneFld.value);	
									}
									if (runningTotalMilestone > 0){
										runningTotalMilestone-= mileAmount;
									}
									runningTotalMilestone = Math.round(runningTotalMilestone*100)/100;
									runningTotalMilestoneFld.value = runningTotalMilestone;
									var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
									if (runningTotalMileSpan){
										runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
									}
								}
								var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
								if (runningTotalPrevInvoiceFld){
									if (runningTotalPrevInvoiceFld.value != ""){
										runningTotalPrevInvoice = parseFloat(runningTotalPrevInvoiceFld.value);	
									}
									if (runningTotalPrevInvoice > 0){
										runningTotalPrevInvoice-= prevInvoicedAmount;
									}
									runningTotalPrevInvoice = Math.round(runningTotalPrevInvoice*100)/100;
									runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
									
									var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
									if (runningTotalPrevInvoicedSpan){
										runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
									}
								}
							}
						}
					}
				}
				return false;
				break;
			default:
				if (f_check_perm(pgRight,'N')){
					// If no action is given, I try to edit it
					this.onEventShowCellEditor(oArgs);
				}
				return true;
				break;
			}
		});

		myDataTable.subscribe("editorKeydownEvent",function(oArgs) {
			var ed = this._oCellEditor;  // Should be: oArgs.editor, see: http://yuilibrary.com/projects/yui2/ticket/2513909
			var ev = oArgs.event;
			var KEY = YAHOO.util.KeyListener.KEY;
			var Textbox = YAHOO.widget.TextboxCellEditor;
			var Textarea = YAHOO.widget.TextareaCellEditor;
			var cell = ed.getTdEl();
			var oColumn = ed.getColumn();
			var row,rec;
			
			var editNext = function(cell) {
				cell = myDataTable.getNextTdEl(cell);
				while (cell && !myDataTable.getColumn(cell).editor) {
					cell = myDataTable.getNextTdEl(cell);
				}
				if (cell) {
					//Enabling/disabling columns
					var oRecord = myDataTable.getRecord(cell);
					var oColumn = myDataTable.getColumn(cell);
					if (oColumn.key != 'invoiceAmount') {
						editNext(cell);
					}else{
						if(formatterFail) return;
						YAHOO.util.UserAction.click(cell);
					}
				}
			};
			var editPrevious = function(cell) {
				cell = myDataTable.getPreviousTdEl(cell);
				while (cell && !myDataTable.getColumn(cell).editor) {
					cell = myDataTable.getPreviousTdEl(cell);
				}
				if (cell) {
					//Enabling/disabling columns
					var oRecord = myDataTable.getRecord(cell);
					var oColumn = myDataTable.getColumn(cell);
					if (oColumn.key != 'invoiceAmount') {
						editPrevious(cell);
					}else{
						if(formatterFail) return;
						YAHOO.util.UserAction.click(cell);
					}
				}
			};
				
			switch (ev.keyCode) {
				case KEY.TAB:
					YAHOO.util.Event.stopEvent(ev);
					ed.save();
					if (ev.shiftKey) {
						editPrevious(cell);
					} else {
						editNext(cell);
					}
					break;
			}
	   });

		myDataTable.subscribe("cellMouseoverEvent", function(oArgs) {
			var elCell = oArgs.target;
			var oColumn = this.getColumn(oArgs.target);
			var oRecord = this.getRecord(oArgs.target);
			if (oColumn.key == 'coverageType'){
				var oData = oRecord.getData(oColumn.key);
				if (oData && oData != '-')
					return overlib(covTypeLegend,CAPTION,L_Coverage_TypeLegend);
				else 
					return nd();
			}else{
				return nd();
			}
		});
		myDataTable.subscribe("cellMouseoutEvent", function(oArgs) {
			var elCell = oArgs.target;
			var oColumn = this.getColumn(oArgs.target);
			if (oColumn.key == 'coverageType'){
				return nd();
			}
		});
	   myDataTable.focus();
	   hidePanel();
	};
	this.handleFailure = function(o) {
		var paramArray = [o.responseText];
        alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert(svrCommErr+":"+o.responseText);*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}
VELOS.generateInvoiceGrid.calculateAll = function (){
	if (myDataTable._oCellEditor){
		return;
	}
	var recordSet = myDataTable.getRecordSet();
	var tabLength = myDataTable.getRecordSet().getLength();
	var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
	var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
	var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
	var runningTotalInvoice = 0, runningTotalMilestone = 0, runningTotalPrevInvoice = 0;
	
	if (runningTotalInvoiceFld){
		for (var iX=0; iX<tabLength; iX++) {
			var oRecord = recordSet.getRecord(iX);
			var mileAmount = parseFloat(oRecord.getData('mileAmount'));
			var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
			var invoiceAmount = Math.round((mileAmount-prevInvoicedAmount)*100)/100;
			
			if (invoiceAmount >= 0){
				oRecord.setData('invoiceAmount',invoiceAmount);
				myDataTable.updateCell(oRecord, "invoiceAmount", invoiceAmount, false);
				
				runningTotalInvoice+= invoiceAmount;
				if (invoiceAmount > 0){
					runningTotalMilestone+= mileAmount;
					runningTotalPrevInvoice+= prevInvoicedAmount;
				}
			}
		}
		runningTotalInvoice = Math.round(runningTotalInvoice*100)/100;
		runningTotalInvoiceFld.value = runningTotalInvoice;
		var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
		if (runningTotalInvoiceSpan){
			runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
		}
	}
	if (runningTotalMilestoneFld){
		runningTotalMilestone =  Math.round(runningTotalMilestone*100)/100;
		runningTotalMilestoneFld.value = runningTotalMilestone;
		var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
		if (runningTotalMileSpan){
			runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
		}
	}

	if (runningTotalPrevInvoiceFld){
		runningTotalPrevInvoice =  Math.round(runningTotalPrevInvoice*100)/100;
		runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
		var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
		if (runningTotalPrevInvoicedSpan){
			runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
		}
	}
}

VELOS.generateInvoiceGrid.eraseAll = function (){
	var recordSet = myDataTable.getRecordSet();
	var tabLength = myDataTable.getRecordSet().getLength();
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);		
		oRecord.setData('invoiceAmount',null);
		myDataTable.updateCell(oRecord, "invoiceAmount", null, false);
	}
	var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
	if (runningTotalInvoiceFld){
		var runningTotalInvoice = 0;
		runningTotalInvoice = Math.round(runningTotalInvoice*100)/100;
		runningTotalInvoiceFld.value = runningTotalInvoice;
		var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
		if (runningTotalInvoiceSpan){
			runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
		}
	}
	
	var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
	if (runningTotalMilestoneFld){
		var runningTotalMilestone = 0;
		runningTotalMilestone = Math.round(runningTotalMilestone*100)/100;
		runningTotalMilestoneFld.value = runningTotalMilestone;
		var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
		if (runningTotalMileSpan){
			runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
		}
	}
	
	var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
	if (runningTotalPrevInvoiceFld){
		var runningTotalPrevInvoice = 0;
		runningTotalPrevInvoice = Math.round(runningTotalPrevInvoice*100)/100;
		runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
		
		var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
		if (runningTotalPrevInvoicedSpan){
			runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
		}
	}
}

VELOS.generateInvoiceGrid.processColumns = function (respJ,colArray){
	myColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArray) {
		var tempColumnDefs =[];
		var f;
		myColumnDefs = colArray;
		tempColumnDefs = colArray;
		for (var iX=0; iX<myColumnDefs.length; iX++) {			
			myColumnDefs[iX].width = myColumnDefs[iX].key=='descService' ? (isIe==true?180:170):(isIe==true?80:60);
			if (myColumnDefs[iX].key=='coverageType' || myColumnDefs[iX].key=='quantity')
				myColumnDefs[iX].width = (isIe==true?70:50);
			
			VELOS.generateInvoiceGrid.cellEditors(myColumnDefs[iX]);

			if (myColumnDefs[iX].key == 'calculate') { //Ak:Added for enhancement PCAL-20801
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].halign="left";
				myColumnDefs[iX].width = (isIe==true?40:20);
			}
			if (myColumnDefs[iX].key == 'erase') { //Ak:Added for enhancement PCAL-20353
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].halign="left";
				myColumnDefs[iX].width = (isIe==true?45:25);
			}
			if (myColumnDefs[iX].key != 'calculate' && myColumnDefs[iX].key != 'erase') { 
				myColumnDefs[iX].resizeable = true;
				myColumnDefs[iX].sortable = true;
			}
		}
	}
	return myColumnDefs;
	
}

formatTwoDecimalNumber = function(el, oRecord, oColumn, oData) {
	formatterFail = false;

	formatterFailThree=false;
	var formattedData ='';
	if (oData != null){
		formattedData = YAHOO.util.Number.format(oData, twoDecimalNumber);
	}
	el.innerHTML = formattedData;
	oRecord.setData(oColumn.key, oData);
	if (myDataTable && myDataTable._oCellEditor){
		myDataTable._oCellEditor.value = oData;
	}
};

/**
 * JSON to customize the fields.
 */
VELOS.generateInvoiceGrid.types = {
		number: {
			formatter:formatTwoDecimalNumber,//'number',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}([.,]\\d{0,2})?$',
				validator: function(oData){
					var isValid = (oData == null || oData == '')? true 
							: YAHOO.widget.DataTable.validateNumber(oData);
					if (isValid == null || !isValid){
						editorSaved = true;
						alert(M_ValEtrFwg_AmtFmt11Dgt);
						if(myDataTable._oCellEditor){
							var editor = myDataTable._oCellEditor;
							myDataTable._oCellEditor.value = '';
							selectedCell = editor.getTdEl();
							myDataTable.onEventShowCellEditor({target:selectedCell}); 
						}
					}else
						return oData;
				}
			}
		}
};

/**
 * Added method to set the editor for all columns
 */
VELOS.generateInvoiceGrid.cellEditors = function(tempColumnsDef) {	
	if (tempColumnsDef.type!=undefined){
		var type= tempColumnsDef.type;
	 	var typeInfo =VELOS.generateInvoiceGrid.types[type];
	 	if (typeInfo.parser!=undefined){
	 		tempColumnsDef.parser=typeInfo.parser;
	 	}
	 	if(tempColumnsDef.formatter==undefined){
	 	  if (typeInfo.formatter!=undefined)
	 		 tempColumnsDef.formatter=typeInfo.formatter;
	 	}
	 	if (typeInfo.stringer!=undefined)
	 		tempColumnsDef.stringer=typeInfo.stringer;
		if (typeInfo.editorOptions!=undefined){
			tempColumnsDef.editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
		}
	
	}
}
/**
 * Generate Invoice button functionality
 */
VELOS.generateInvoiceGrid.generateInvoice = function(e){
	myDataTable.saveCellEditor();
	if(formatterFail){
		myDataTable._oCellEditor.value = '';
		myDataTable.onEventShowCellEditor({target:selectedCell}); 
		if(e) YAHOO.util.Event.stopEvent(e);
		formatterFailTwo=false;
		formatterFailThree=false;
		return false;
	}
	
	fnSendInvoiceData(myDataTable);
	var myGrid = myDataTable;
	if(!VELOS.generateInvoiceGrid.validate(myGrid)){
		if(e) YAHOO.util.Event.stopEvent(e);
		return false;
	}	
	return true;
}

function fnSendInvoiceData(myDataTable) {
	var recordSet = myDataTable.getRecordSet();
	var tabLength = myDataTable.getRecordSet().getLength();
	var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
	var runningTotalInvoice = 0;
	
	if (runningTotalInvoiceFld){
		if (runningTotalInvoiceFld.value && runningTotalInvoiceFld.value !=""){
			runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);
		}
		var amtInput = document.getElementById("totalInvoiceAmount");
		
		if (!amtInput){
			amtInput = document.createElement('input');
			amtInput.setAttribute('type', "hidden");
			amtInput.setAttribute('id', "totalInvoiceAmount");
			amtInput.setAttribute('name', "totalInvoiceAmount");
			amtInput.setAttribute('value', runningTotalInvoice);
			//amtInput.style.display = "none";
			document.body.appendChild(amtInput);
		} else {
			amtInput.value = runningTotalInvoice;
		}
	}
	
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var mileAchieveId = parseInt(oRecord.getData('mileAchieveId'));
		var milestoneId = parseInt(oRecord.getData('milestoneId'));
		var mileAmount = parseFloat(oRecord.getData('mileAmount'));
		mileAmount = Math.round((mileAmount)*100)/100;

		var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
		prevInvoicedAmount = Math.round((prevInvoicedAmount)*100)/100;

		var invoiceAmount = parseFloat(oRecord.getData('invoiceAmount'));
		invoiceAmount = Math.round((invoiceAmount)*100)/100;
		
		var amtInput = document.getElementById("invAmt"+mileAchieveId+"_mile"+milestoneId);

		var amtJson= {
			mileAchieveId: mileAchieveId,
			milestoneId: milestoneId,
			mileAmount: mileAmount,
			prevInvoicedAmount: prevInvoicedAmount,
			invoiceAmount: invoiceAmount
		};
			
		if (!amtInput){
			if (invoiceAmount > 0){
				amtInput = document.createElement('input');
				amtInput.setAttribute('type', "hidden");
				amtInput.setAttribute('id', "hidden"+mileAchieveId+"_mile"+milestoneId);
				amtInput.setAttribute('name', "invAmt"+mileAchieveId+"_mile"+milestoneId);
				amtInput.setAttribute('value', YAHOO.lang.JSON.stringify(amtJson));
				//amtInput.style.display = "none";
				document.genInvoice.appendChild(amtInput);
			}
		} else {
			amtInput.value = YAHOO.lang.JSON.stringify(amtJson);
		}
	}
}
/**
 * Added method to validate all manadatory fields
 */
VELOS.generateInvoiceGrid.validate= function(myGrid){
	var runningTotalInvoiceFld = document.getElementById('runningTotalInvoiceAmount');
	var runningTotalMilestoneFld = document.getElementById('runningTotalMileAmount');
	var runningTotalPrevInvoiceFld = document.getElementById('runningTotalPrevInvoicedAmount');
	var runningTotalInvoice = 0, runningTotalMilestone = 0, runningTotalPrevInvoice = 0;
	
	if (runningTotalInvoiceFld){
		if(runningTotalInvoiceFld.value != "")
			runningTotalInvoice = parseFloat(runningTotalInvoiceFld.value);
		if (runningTotalInvoice <= 0){
			alert(M_TotInvAmtNt_NegVal);
			return false;
		}
	}
	
	if (runningTotalMilestoneFld){
		if (runningTotalMilestoneFld.value != "")
			runningTotalMilestone = parseFloat(runningTotalMilestoneFld.value);
		var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
		if (runningTotalMileSpan){
			runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
		}
	}
	
	if (runningTotalPrevInvoiceFld){
		if(runningTotalPrevInvoiceFld.value != "")
			runningTotalPrevInvoice = parseFloat(runningTotalPrevInvoiceFld.value);
		
		var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
		if (runningTotalPrevInvoicedSpan){
			runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
		}
	}
	
	var recordSet = myGrid.getRecordSet();
	for(rec=0; rec < recordSet.getLength(); rec++){	
		var oRecord = recordSet.getRecord(rec);
		var mileAmount = parseFloat(oRecord.getData('mileAmount'));
		var prevInvoicedAmount = parseFloat(oRecord.getData('prevInvoicedAmount'));
		var invoiceAmount = parseFloat(oRecord.getData('invoiceAmount'));

		if (invoiceAmount < 0){
			alert(M_InvAmtNt_NegVal);
			oRecord.setData('invoiceAmount', '');
			myGrid.updateCell(oRecord, 'invoiceAmount', '', false);

			runningTotalMilestone-= mileAmount;
			runningTotalInvoice-= invoiceAmount;
			runningTotalPrevInvoice-= prevInvoicedAmount;
			
			runningTotalMilestoneFld.value = runningTotalMilestone;
			var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
			if (runningTotalMileSpan){
				runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
			}
			
			runningTotalInvoiceFld.value = runningTotalInvoice;
			var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
			if (runningTotalInvoiceSpan){
				runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
			}

			runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
			var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
			if (runningTotalPrevInvoicedSpan){
				runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
			}			
			return false;
		}

		if (mileAmount < invoiceAmount){
			alert(M_InvAmtGt_MstoneAmt);
			oRecord.setData('invoiceAmount', '');
			myGrid.updateCell(oRecord, 'invoiceAmount', '', false);

			runningTotalMilestone-= mileAmount;
			runningTotalInvoice-= invoiceAmount;
			runningTotalPrevInvoice-= prevInvoicedAmount;

			runningTotalMilestoneFld.value = runningTotalMilestone;
			var runningTotalMileSpan = document.getElementById('runningTotalMileSpan');
			if (runningTotalMileSpan){
				runningTotalMileSpan.innerHTML = YAHOO.util.Number.format(runningTotalMilestone, twoDecimalNumber);
			}

			runningTotalInvoiceFld.value = runningTotalInvoice;
			var runningTotalInvoiceSpan = document.getElementById('runningTotalInvoiceSpan');
			if (runningTotalInvoiceSpan){
				runningTotalInvoiceSpan.innerHTML = YAHOO.util.Number.format(runningTotalInvoice, twoDecimalNumber);
			}

			runningTotalPrevInvoiceFld.value = runningTotalPrevInvoice;
			var runningTotalPrevInvoicedSpan = document.getElementById('runningTotalPrevInvoicedSpan');
			if (runningTotalPrevInvoicedSpan){
				runningTotalPrevInvoicedSpan.innerHTML = YAHOO.util.Number.format(runningTotalPrevInvoice, twoDecimalNumber);
			}
			return false;
		}
	}
	return true;
}

VELOS.generateInvoiceGrid.fnShowCellEditor= function(rec,colName){
	var columnCells = $D.getElementsByClassName('yui-dt-col-'+colName, 'td', myDataTable.getTableEl());
	for (var iX=0; iX<columnCells.length; iX++) {
		if(rec == iX){
			var el = new YAHOO.util.Element(columnCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var elCell = parent.getElementsByClassName('yui-dt-col-'+colName, 'td')[0];
			myDataTable.showCellEditor(elCell);
			return false;
		}
	}
	return false;
}

VELOS.generateInvoiceGrid.decodeData = function(oData) {
	oData = oData.replace(/[&]lt;/g,"<"); //replace less than
	oData = oData.replace(/[&]gt;/g,">"); //replace greater than
	oData = oData.replace(/[&]amp;/g,"&"); //replace &
	return oData;
}

VELOS.generateInvoiceGrid.encodeData = function(oData) {
	oData = oData.replace(/[&]/g,"&amp;"); //replace &
	oData = oData.replace(/[<]/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]/g,"&gt;"); //replace greater than
	return oData;
}

VELOS.generateInvoiceGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'generateInvoiceGrid'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.generateInvoiceGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.generateInvoiceGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.generateInvoiceGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.generateInvoiceGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.generateInvoiceGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}
//End here



