/**
 * Added file for enhancement INV9.1(Multiple Specimen)
 */
var specimenGridChangeList = [];
var specLabelArray = null;
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var specimensCount = 0;
var formatterFail = false;
var myDataTable;
var pgRight=0;
var myDialog = null;
var specTypeIds = [];
var specTypes  = [];
var specStatIds = [];
var specStats  = [];
var specQUnitIds = [];
var specQUnits  = [];
var studyNames  = [];
var studyIds  = [];
var patientNames  = [];
var patientIds  = [];
//Data Grid name declarated here
var createRows = [];
var studyResult = [];
var patientResult = [];
var myParentGrid, myChildGrid;
var childRecordCount;
var isSpecReadonly;  //Bug#4142 Date:05-June-2012 Ankit
function includeJS(file)
{
  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
includeJS('js/velos/velosUtil.js');
/*var svrCommErr= Svr_CommunicationError; Storing Value from MC.jsp to local var*/
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var varStudy=L_Study;
var Patient_Id=L_Patient_Id;
var ValEtrFwg_Qnty11DgtVal=M_ValEtrFwg_Qnty11DgtVal;
var PlsEnterEsign=M_PlsEnterEsign;
var CldNtCont_InvldIndx=M_CldNtCont_InvldIndx;
var NoRec_ChldGrid=M_NoRec_ChldGrid;
var Add_MultiSpecimens=L_Add_MultiSpecimens;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var PleaseWait_Dots=L_PleaseWait_Dots;
var DupliSpmenIdEtr_PrvdUnqId=M_DupliSpmenIdEtr_PrvdUnqId;
var Delete_Row=L_Delete_Row;
var CnfmSpmen_Chgs=L_CnfmSpmen_Chgs;
var specimenType=L_Specimen_Type;
var lUnit=L_Unit;
var lQuantity=L_Quantity;
// Define multiSpecimensGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.multiSpecimensGrid = function (url, args) {
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updatemultiplespecimens.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.multiSpecimensGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'multiSpecimenGrid'; // name of the div to hold multiSpecimenGrid
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			alert(CommErr_CnctVelos);/*alert(svrCommErr);*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
		specTypeIds = respJ.specTypeId;
		specTypes = respJ.specType;
		specStatIds = respJ.specStatId;
		specStats= respJ.specStat;
		studyNames=respJ.studyNames;
		studyIds=respJ.studyIds;
		patientNames=respJ.patNames;
		patientIds=respJ.patIds;
		isSpecReadonly=respJ.isSpecReadonly;  // Bug#4142 Date:05-June-2012 Ankit
		
	      for(var i=0; i<studyIds.size(); i++) {
	    	  var result = [];
	    	  result['value'] = studyNames[i]; 
	    	  result['id'] = studyIds[i];
	    	  studyResult.push(result);
	       }
	      
	      
	      for(var i=0; i<patientIds.size(); i++) {
	    	  var result = [];
	    	  result['value'] = patientNames[i]; 
	    	  result['id'] = patientIds[i];
	    	  patientResult.push(result);
	       }
	      
	      
		specQUnitIds = respJ.specQUnitId;
		specQUnits= respJ.specQUnit;
		
		var myColumnDefs = [];
		specIdArray = []; 	specSeqArray = []; 		checkItemArray = [];
		specLabelArray = [];	
		delButtonArray = [];
		var maxWidth = 800;
		var maxHeight = 800;
		if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 800; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1000; }

		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }	
		var calcHeight=320;
			createRows = [];
    	for (var iX=0; iX<2; iX++) {
    		var gridNo;
    		if (iX==0){
    			gridNo='Parent';			
				colArray = respJ.parentColArray;
				dataArray = respJ.parentDataArray;	
				calcHeight = respJ.dataArray.length*80 + 80;
			} else if (iX==1){
				gridNo='Child';
				colArray = respJ.colArray;	
				dataArray = respJ.dataArray;
				childRecordCount=respJ.childrecCount;
				calcHeight=320;
			}
    		
    	myColumnDefs = VELOS.multiSpecimensGrid.processColumns(respJ,colArray);
    	
		var myDataSource = new YAHOO.util.DataSource(dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
		   fields:myFieldArray

		};
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }
		myDataTable = new YAHOO.widget.DataTable(
				gridNo +'_'+ this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:"98%",
					//maxWidth+"px",
				height:calcHeight+"px",
				//caption:"DataTable Caption",
				scrollable:false
			}
		);
		
			if(iX==0){
				var elCell = myDataTable.getTableEl();
				var oRecord = myDataTable.getRecord(elCell);		
				var column =  myDataTable.getColumn(elCell);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
				var statuDtTd = parent.getElementsByClassName('yui-dt-col-statDt', 'td')[0];	
				var statuDtEl = new YAHOO.util.Element(statuDtTd);	
			
			 statuDtEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ='<input type="text" name="statusDate" id="statusDate" size="10" align = "left" class = "datefield" READONLY  >';
			 
			var studyIdTd = parent.getElementsByClassName('yui-dt-col-selStudyIds', 'td')[0];	
			var studyIDEl = new YAHOO.util.Element(studyIdTd);		
			
			studyIDEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ='<input type="text" name="selStudy0" id="selStudy0" onFocus="openAuto(\'selStudy0\',\'selStudyIds0\',studyResult,\''+varStudy/*Study*****/+'\');" size="10" align = "left">'+				  					
			'<input type="hidden" name="selStudyIds0" id="selStudyIds0" >';
				
				
			var patIdTd = parent.getElementsByClassName('yui-dt-col-patientIds', 'td')[0];	
			var patIDEl = new YAHOO.util.Element(patIdTd);	
			patIDEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = '<input type="text" id="patientNames0" onFocus="openAuto(\'patientNames0\',\'patientIds0\',patientResult,\''+Patient_Id/*Patient ID*****/+'\');"size="10"  align = "left">'+
			'<input type="hidden" name="patientIds0"  id="patientIds0">';
				
			myParentGrid=myDataTable;
			
			}else if (iX==1){
				myChildGrid=myDataTable;
			}
		
	    	
		if (YAHOO.util.Dom.get("rowCount")){
			YAHOO.util.Dom.get("rowCount").value="0";
			 
		}	
		 myDataTable.subscribe("editorSaveEvent", onCellSave);
		// myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
		// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
		 myDataTable.subscribe('cellClickEvent',function(oArgs) {
				var target = oArgs.target;
				var column = this.getColumn(target);
				var oRecord = this.getRecord(target);
				var specSeq=oRecord.getData('specSeq');
				// Bug#4142 Date:05-June-2012 Ankit
				if(column.key == 'specimenId' && isSpecReadonly == 1){
					return;
				}
							
				// columns with action icons should have an action property
				// The action thus independent of the visual image, if you click on that cell, whatever is displayed in it, the action will happen
				switch (column.action) {
					// for the delete action I request user confirmation and call the ask method of the DataSource
					case 'delete':
							specimenGridChangeList['specSeq'+specSeq] = undefined;	
								myDataTable.deleteRow(target);
						break;
				}
				switch(column.formatter){
				case 'customNumber':
					column.formatter = function(el, oRecord, oColumn, oData) {	
						if (oData != '0'){
							if (oData != '' || oData != null){
								if (/^[0]+/g.test(oData)){
									oData = oData.replace(/^[0]+/g,""); //remove leading zeros
								}
							}
						}
						if (/^\.\d{0,2}?$/.test(oData)){ // If no zero before decimal add one
							oData = "0" + oData;
						}
						if (/^\d{0,11}([.]\d{0,2})?$/.test(oData)){
								el.innerHTML = oData;
								oRecord.setData(oColumn.key, oData);
								if (myDataTable && myDataTable._oCellEditor){
									myDataTable._oCellEditor.value = oData;
								}
						}else{
								oRecord.setData(oColumn.key, '');	
								if (myDataTable && myDataTable._oCellEditor){
									myDataTable._oCellEditor.value = null;
								}		
								alert(ValEtrFwg_Qnty11DgtVal);/*alert("The value entered does not pass the following criteria:\n \n -'Quantity' should be a number.\n -'Quantity' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
					    }
					
					};
					break;
				}
				this.onEventShowCellEditor(oArgs);
			});
	 	 myDataTable.subscribe("rowMouseoutEvent", function(oArgs) {
			 myDataTable.onEventUnhighlightRow(oArgs);
				return nd();
		});
		 
		 myDataTable.focus();
    	}
    	
    
		hidePanel();
		openCal();
		jQuery.extend(jQuery.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});
	};
	this.handleFailure = function(o) {
		var paramArray = [o.responseText];
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert(svrCommErr+":"+o.responseText);*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}
/*
 * Apply attributes to the newly created rows
 */
VELOS.multiSpecimensGrid.applyRowAttribs = function(myDataTable, rCount) {
	var tabLength= myDataTable.getRecordSet().getLength();
	for (var iX=(tabLength-rCount); iX<tabLength; iX++) {
		var delButtonCells= $D.getElementsByClassName('yui-dt-col-delete', 'td');
		var el = new YAHOO.util.Element(delButtonCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var parentId = parent.get('id');

		var specSeqTd = parent.getElementsByClassName('yui-dt-col-specSeq', 'td')[0];
		var specSeqEl = new YAHOO.util.Element(specSeqTd);
		var specSeq = specSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		specSeqArray.push(specSeq);
		specIdArray.push(0);
		specLabelArray.push('');
		
		var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
		var delButtonEl = new YAHOO.util.Element(delButtonTd);
		var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
		= '<div align="center"> <img src="../images/delete.png" align="center" alt="'+Delete_Row/*Delete Row*****/+'" border="0"'
		+ ' name="delete_e'+specSeq+'" id="delete_e'+specSeq+'" /> </div>';	
		
		//Study IDTd
		var studyIdTd = parent.getElementsByClassName('yui-dt-col-selStudyIds', 'td')[0];	
		var studyIDEl = new YAHOO.util.Element(studyIdTd);	
		studyIDEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ='<input type="text" name="selStudy'+specSeq+'" id="selStudy'+specSeq+'" onFocus="openAuto(\'selStudy'+specSeq+'\',\'selStudyIds'+specSeq+'\',studyResult,\''+varStudy/*Study*****/+'\');" size="10" align = "left" >'+  					
		'<input type="hidden"  name="selStudyIds'+specSeq+'" id = "selStudyIds'+specSeq+'" value="">';
	
		//Paient IDTd
		
		var patIdTd = parent.getElementsByClassName('yui-dt-col-patientIds', 'td')[0];	
		var patIDEl = new YAHOO.util.Element(patIdTd);	
		patIDEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = 
		'<input type="text" name="patientNames'+specSeq+'" id="patientNames'+specSeq+'"  onFocus="openAuto(\'patientNames'+specSeq+'\',\'patientIds'+specSeq+'\',patientResult,\''+Patient_Id/*Patient ID*****/+'\');" size="10" align = "left">'+
		'<input type="hidden" name="patientIds'+specSeq+'" id = "patientIds'+specSeq+'" value="">';
		
		
		//StatusDate
			
		var statuDtTd = parent.getElementsByClassName('yui-dt-col-statDt', 'td')[0];	
		var statuDtEl = new YAHOO.util.Element(statuDtTd);			
		statuDtEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = '<input type="text" name="statusDate'+specSeq+'" id="statusDate'+specSeq+'" size="10" align ="left" class = "datefield" READONLY>';
	}
}
/*
 * Method added to cutomize the fields.
 */

VELOS.multiSpecimensGrid.types = {
		'varchar':{
			editor:'textbox',
			editorOptions:{disableBtns:true},
			formatter:'varchar'
		},
		'date': {
			parser:'date',
			formatter:'date',
			editor:'date',
			// I don't want the calendar to have the Ok-Cancel buttons below
			editorOptions:{
				disableBtns:false,
				validator: YAHOO.widget.DataTable.validateDate
			},
			// I want to send the dates to my server in the same format that it sends it to me
			// so I use this stringer to convert data on the way from the client to the server
			stringer: function (date) {
				return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +
					' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
			}
		},
		integer: {
			formatter:'integer',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			// I'm only accepting digits (probably this data type should be called integer)
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}$'
				//validator: YAHOO.widget.DataTable.validateNumber
			}
		},
		number: {
			formatter:'number',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			// I'm only accepting digits (probably this data type should be called integer)
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
				//regExp:'^\\d{11}$',
				//validator: YAHOO.widget.DataTable.validateNumber
			}
		},
		currency: {
			parser:'number',
			formatter:'currency',
			editor:'regexp',
			// for currency I accept numbers with up to two decimals, with dot or comma as a separator
			// It won't accept currency signs and thousands separators really messes things up
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d*([.,]\\d{0,2})?$',
				validator: function (value) {
					return parseFloat(value.replace(',','.'));
				}
			},
			// When I pop up the cell editor, if I don't change the dots into commas, the whole thing will look inconsistent.
			cellEditorFormatter: function(value) {
				return ('' + value).replace('.',',');
			},
			disableBtns:true
		},
		// This is the default, do-nothing data type.
		// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
		string: {
			editor:'textbox'
		}
	};

VELOS.multiSpecimensGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
/*
 * Method to add the new row in child grid(Add new row functionality)
 */

VELOS.multiSpecimensGrid.addRows = function() {
	var count = eval(YAHOO.util.Dom.get("rowCount").value);
	var recordNum=childRecordCount;
	if(YAHOO.lang.isNumber(count)) {
		var myArray = [];
		for(var i=0;i<count;i++) {
			specimensCount++;recordNum++;
			var dataJSON = {"specSeq":specimensCount,"delete":"","specimenId":"","specType":'',"specTypeId":'',"description":"",
					"selStudyIds":"","patientIds":"","specStat":'',"specStatId":'',"statDt":"","quantity":"","recNum":recordNum,
					"unit":"","unitId":"","altId":""};
			var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
			record.row = i;
			myArray.push(record);
			var paramArray = [recordNum];
			specimenGridChangeList['specSeq'+specimensCount] = getLocalizedMessageString("L_Add_Row",paramArray)/*'Added Row #'+recordNum*****/;	
			createRows['specSeq'+specimensCount] = recordNum;
		}
		myDataTable.addRows(myArray);
		if (count>0) {
			VELOS.multiSpecimensGrid.applyRowAttribs(myDataTable,count);
			openCal();
			jQuery.extend(jQuery.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});
		}
		YAHOO.util.Dom.get("rowCount").value = "0";
		childRecordCount=recordNum;
		return;
	}
	YAHOO.log(CldNtCont_InvldIndx);/*YAHOO.log("Could not continue due to invalid index.");*****/
	YAHOO.util.Dom.get("rowCount").value = "0";
}

VELOS.multiSpecimensGrid.decodeData = function(oData) {
	oData = oData.replace(/[&]lt;/g,"<"); //replace less than
	oData = oData.replace(/[&]gt;/g,">"); //replace greater than
	oData = oData.replace(/[&]amp;/g,"&"); //replace &
	return oData;
}

VELOS.multiSpecimensGrid.encodeData = function(oData) {
	oData = oData.replace(/[&]/g,"&amp;"); //replace &
	oData = oData.replace(/[<]/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]/g,"&gt;"); //replace greater than
	return oData;
}

VELOS.multiSpecimensGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'multiSpecimenGrid'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.multiSpecimensGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.multiSpecimensGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.multiSpecimensGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(PleaseWait_Dots);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.multiSpecimensGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.multiSpecimensGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

/*
 * Set to all functionality 
 */

VELOS.multiSpecimensGrid.setToAll = function() {
	var myGrid = myParentGrid;
	var myChildGrid1 = myChildGrid;
	myGrid.saveCellEditor();
	myChildGrid1.saveCellEditor(); 
	var arrRecords = myChildGrid.getRecordSet(); 
	 if(arrRecords.getLength()<1){ //AK:fixed BUG#6686
		 alert(NoRec_ChldGrid);/*alert("No records found in Child Grid");*****/
		 return;
	 }
	 if( VELOS.multiSpecimensGrid.validate(myGrid,true)){	
		VELOS.multiSpecimensGrid.setAllColumnContent(myGrid,myChildGrid);
	 }
}

VELOS.multiSpecimensGrid.validate= function(myGrid,setToAllFlag){
	
	var arrRecords = myGrid.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		oRecord = arrRecords.getRecord(rec);	
		var quanity=oRecord.getData("quantity");
		var unit=oRecord.getData("unit");
		if ((!setToAllFlag) && (!VELOS.multiSpecimensGrid.validateColumnContent(rec, oRecord, 'specType', specimenType/*'Specimen Type'*****/,setToAllFlag))) //AK:Fixed BUG#6795(10thAug11)
				return false;
		if (quanity!=undefined &&  quanity.length != 0 ){
				if (!VELOS.multiSpecimensGrid.validateColumnContent(rec, oRecord, 'unit', lUnit/*'Unit'*****/,setToAllFlag))
				return false;
		}
		 if (unit!=undefined &&  unit.length != 0){
				if (!VELOS.multiSpecimensGrid.validateColumnContent(rec, oRecord, 'quantity',lQuantity /*'Quantity'*****/,setToAllFlag))
					return false;
		}
	}
	return true;
	
}
/*
 * Validate the mandatory fields value
 */

VELOS.multiSpecimensGrid.validateColumnContent = function(recCount, oRecord, columnKey, columnLabel,setToAllFlag) {
	var columnContent = oRecord.getData(columnKey);	
	if(columnContent == undefined || columnContent.length == 0){
		if(setToAllFlag){  //AK:fixed BUG#6686
			var paramArray = [columnLabel];
			alert(getLocalizedMessageString("M_EtrSetAll_PowerBar",paramArray));/*alert("Enter "+columnLabel+" for 'Set To All' power bar");*****/ 
		}else{
			var paramArray = [columnLabel,(recCount+1)];
			alert(getLocalizedMessageString("M_EtrFor_Row",paramArray));/*alert("Enter "+columnLabel+" for row #" + (recCount+1));*****/
		}
		return false;
 	}
	return true;
}

/*
 * Setting up values from parent grid to child grid for 'Set To All' functionality.
 */

VELOS.multiSpecimensGrid.setAllColumnContent = function(myParentGrid,myChildGrid){
	 var childrenCount=0;
	 var statuDate= $('statusDate').value;	
	 var selStudyVal=$('selStudy0').value;
	 var patientNamesVal=$('patientNames0').value;
	 var selStudyIdsVal=$('selStudyIds0').value;
	 var patientIdsVal=$('patientIds0').value;
	 
     childrenCount=childRecordCount;
	 var arrParRecords = myParentGrid.getRecordSet();
	 oParentRecord = arrParRecords.getRecord(0);	
	 var specType=oParentRecord.getData("specType");
	 var specTypeId=oParentRecord.getData("specTypeId");
	 var description=oParentRecord.getData("description");
	 var specStatVal=oParentRecord.getData("specStat");
	 var specStatIdVal=oParentRecord.getData("specStatId");
     var quantity= oParentRecord.getData("quantity");
     if(quantity==null){
    	 quantity="";
     }
     var unitVal= oParentRecord.getData("unit");
     var unitIdVal= oParentRecord.getData("unitId");
     
     var arrRecords = myChildGrid.getRecordSet();
	 for(rec=0; rec < arrRecords.getLength(); rec++){
	  var count=arrRecords.getLength();
	    oRecord = arrRecords.getRecord(rec); 
	    var delButtonCells= $D.getElementsByClassName('yui-dt-col-delete', 'td');
		var el = new YAHOO.util.Element(delButtonCells[rec]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var parentId = parent.get('id');
		
		var specSeqTd = parent.getElementsByClassName('yui-dt-col-specSeq', 'td')[0];
		var specSeqEl = new YAHOO.util.Element(specSeqTd);
		var specSeq=oRecord.getData("specSeq");
		$('statusDate'+(specSeq)).value=statuDate;
		$('selStudy'+(specSeq)).value=selStudyVal;
		$('patientNames'+(specSeq)).value=patientNamesVal;
		$('selStudyIds'+(specSeq)).value=selStudyIdsVal;
		$('patientIds'+(specSeq)).value=patientIdsVal;	
		validateAutoComplete('selStudy'+specSeq,'selStudyIds'+specSeq,studyResult,varStudy,false);
		validateAutoComplete('patientNames'+specSeq,'patientIds'+specSeq,patientResult,Patient_Id,false);
	
		
		
		var descriptionTd = parent.getElementsByClassName('yui-dt-col-description', 'td')[0];
		var descriptionEl = new YAHOO.util.Element(descriptionTd);
		oRecord.setData("description",description);
		descriptionEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=description;
		
		
		var specSeqTd = parent.getElementsByClassName('yui-dt-col-specType', 'td')[0];
		var specSeqEl = new YAHOO.util.Element(specSeqTd);
		oRecord.setData("specType",specType);
		specSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=specType;
		
		var specTypeIdTd = parent.getElementsByClassName('yui-dt-col-specTypeId', 'td')[0];
		var specTypeIdEl = new YAHOO.util.Element(specTypeIdTd);
		oRecord.setData("specTypeId",specTypeId);
		specTypeIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=specTypeId;		

		//Set Status and staus Id of the Specimens
		var tempPatDescTd = parent.getElementsByClassName('yui-dt-col-tempPatientDesc', 'td')[0];	
		var tempPatDescEl = new YAHOO.util.Element(tempPatDescTd);		
		oRecord.setData('tempPatientDesc', patientNamesVal);
		tempPatDescEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = patientNamesVal;	
		
		var specStatTd = parent.getElementsByClassName('yui-dt-col-specStat', 'td')[0];	
		var specStatEl = new YAHOO.util.Element(specStatTd);		
		oRecord.setData('specStat', specStatVal);
		specStatEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = specStatVal;	
		
		
		var specStatIdTd = parent.getElementsByClassName('yui-dt-col-specStatId', 'td')[0];	
		var specStatIdEl = new YAHOO.util.Element(specStatIdTd);		
		oRecord.setData('specStatId', specStatIdVal);
		specStatIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = specStatIdVal;
		
		//Set the quantity 
		var quantityTd = parent.getElementsByClassName('yui-dt-col-quantity', 'td')[0];	
		var quantityEl = new YAHOO.util.Element(quantityTd);		
		oRecord.setData('quantity', quantity);
		quantityEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = quantity;	
		
		//Set Unit 	
		var unitTd = parent.getElementsByClassName('yui-dt-col-unit', 'td')[0];	
		var unitEl = new YAHOO.util.Element(unitTd);		
		oRecord.setData('unit', unitVal);
		unitEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = unitVal;
		
		
		var unitIdTd = parent.getElementsByClassName('yui-dt-col-unitId', 'td')[0];	
		var unitIdEl = new YAHOO.util.Element(unitIdTd);		
		oRecord.setData('unitId', unitIdVal);
		unitIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = unitIdVal;
	
	 }

}

/**
 * Setting up the description and identifiers of study and patient fields into temporary fields.
 *   
 */
VELOS.multiSpecimensGrid.setStudyPatientInfo = function(myChildGrid){	
	var arrRecords = myChildGrid.getRecordSet();
	if(arrRecords.getLength()>0){
		var studyCells= $D.getElementsByClassName('yui-dt-col-tempStudyDesc', 'td');
		var studyIdsCells= $D.getElementsByClassName('yui-dt-col-tempStudyIds', 'td');
		var patientCells= $D.getElementsByClassName('yui-dt-col-tempPatientDesc', 'td');
		var patientIdsCells= $D.getElementsByClassName('yui-dt-col-tempPatientIds', 'td');
		var statusDateCells = $D.getElementsByClassName('yui-dt-col-tempStatDt', 'td');
	}
	for(rec=0; rec < arrRecords.getLength(); rec++){		
		var oRecord = arrRecords.getRecord(rec);	
		var specSeq=oRecord.getData("specSeq");
		var statusDate= $('statusDate'+specSeq).value;	
		var selStudyVal=$('selStudy'+specSeq).value;
		var selStudyIdsVal=$('selStudyIds'+specSeq).value;		
		var patientNamesVal=$('patientNames'+specSeq).value;		 
		var patientIdsVal=$('patientIds'+specSeq).value;		
		var el = new YAHOO.util.Element(studyCells[rec]);			
		el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = selStudyVal;
		oRecord.setData("tempStudyDesc",selStudyVal);

		el = new YAHOO.util.Element(studyIdsCells[rec]);			
		el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = selStudyIdsVal;
		oRecord.setData("tempStudyIds",selStudyIdsVal);

		el = new YAHOO.util.Element(patientCells[rec]);			
		el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = patientNamesVal;
		oRecord.setData("tempPatientDesc",patientNamesVal);

		el = new YAHOO.util.Element(patientIdsCells[rec]);			
		el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = patientIdsVal;
		oRecord.setData("tempPatientIds",patientIdsVal);

		el = new YAHOO.util.Element(statusDateCells[rec]);			
		el.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = statusDate;	
		oRecord.setData("tempStatDt",statusDate);
	}	
}

/**
 * Preview and Save button functionality
 */
VELOS.multiSpecimensGrid.saveDialog = function(){
	var myGrid = myChildGrid;
	var myGridName = Add_MultiSpecimens/*'Add Multiple Specimens '*****/;
	myGrid.saveCellEditor(); 
	VELOS.multiSpecimensGrid.nullifyStudyPatientIdFields(myGrid);//Ak:Fixed BUG#6796
	VELOS.multiSpecimensGrid.setStudyPatientInfo(myGrid);
	if(!VELOS.multiSpecimensGrid.validate(myGrid,false)) //AK:Validate the speciment type for each row
     return false;
	
	if(VELOS.multiSpecimensGrid.validateSpecimentId(myGrid))
	 return false;
	var regularExp =  new RegExp('specSeq[0-9]+$');

	var saveDialog = document.createElement('div');
	if (!$('saveDialog')) {
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfmSpmen_Chgs/*Confirm Specimen Changes*****/+'</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}

	var noChange = true;
	var numChanges = 0;
		var createJSON = "[" ; 
		var specVal ="";
		var rows = 0;
	var maxChangesDisplayedBeforeScroll = 25;
		$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updatemultiplespecimens.jsp?'+incomingUrlParams+'"><div id="changeRowsContent"></div><br/><div id="insertFormContent"></div></form>';
		for (oKey in specimenGridChangeList) {				
				if (oKey.match(regularExp) && specimenGridChangeList[oKey] != undefined) {
					noChange = false;
						//$('changeRowsContent').innerHTML += specimenGridChangeList[oKey]+'<br/>';
						numChanges++;
						if(createRows[oKey] != undefined){
							if(rows != 0){
								createJSON += ",";
							}						
							createJSON += "{\""+rows+"\":\""+createRows[oKey]+"\"}";
							rows ++;
				
					}
				}
			}
		
		createJSON += "]";
	
		if (numChanges > 0){
			var paramArray = [myGridName];
			$('changeRowsContent').innerHTML = '<b>'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ myGridName + '*****/+':</b><br/>' + $('changeRowsContent').innerHTML;			
			specVal = yuiDataTabletoJSON(myGrid);			
			specVal = htmlEncode(specVal);
			createJSON = htmlEncode(createJSON);			
			$('insertFormContent').innerHTML += '<input type="hidden" name="mySpecimenGrid" value="'+specVal+'" />';
			$('insertFormContent').innerHTML += '<input type="hidden" name="createInfo" value="'+createJSON+'" />';			
			
		}
		
		

	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if (noChange) {
		$('insertFormContent').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
		noChange = true;
	} else {
		var paramArray = [numChanges];
		$('insertFormContent').innerHTML += getLocalizedMessageString("L_TotAdd_Rows",paramArray)/*'Total added row(s): '+numChanges*****/+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
				' onkeypress="return VELOS.multiSpecimensGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';
		$('insertFormContent').innerHTML += '<input id="changeCount" name="changeCount" type="hidden" value="'+numChanges+'"/>';
	}
		if($('insertFormContentOne')){
			$('insertFormContentOne').innerHTML =""; 
		}
		
		var myDialog = new YAHOO.widget.Dialog('saveDialog',
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
	        var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ?
		[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
		[
			{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
			{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
		];
	var onSuccess = function(o) {
		hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
			if (respJ.result >= 0) {
				 //alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
				showFlashPanel(respJ.resultMsg);
				//Very Important Fix #6184, 6273, 6274
				//VELOS.milestoneGrid.flushListAndResetRemainder(pMileTypeId);				
					setTimeout('self.close();', 2000); 					
			}
			else if (respJ.result == -3) {
				showFlashPanel(respJ.resultMsg);				
			}
			else {
				var paramArray = [respJ.resultMsg];
				alert(getLocalizedMessageString("L_Error_Cl",paramArray));/*alert("Error: " + respJ.resultMsg);*****/
		}
	};
	var onFailure = function(o) {
		hideTransitPanel();
		var paramArray = [o.status];
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	if (!formatterFail){
		myDialog.show();
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	}

	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel =
				new YAHOO.widget.Panel("flashPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 1500);
	}

	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel =
				new YAHOO.widget.Panel("transitPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}

	var hideTransitPanel = function() {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
	}
			
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
	//}
}
//End of saveDialog method
/* 
 * Resetting the values when 'Select an Option' is selected 
 */
 
VELOS.multiSpecimensGrid.eraseDependent = function (myGrid,elCell,dependent,depValue){
	var oRecord = myGrid.getRecord(elCell);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var depTd = parent.getElementsByClassName('yui-dt-col-'+dependent, 'td')[0];	
	var depEl = new YAHOO.util.Element(depTd);	
	var dep = depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;	
	oRecord.setData(dependent, depValue);
	depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = depValue;
}

/**
 *  validate the Specimen uniqueness
 */
VELOS.multiSpecimensGrid.validateSpecimentId= function(myGrid){
	var arrRecords = myGrid.getRecordSet();
	for(outer=0; outer < arrRecords.getLength(); outer++){
		oRecord = arrRecords.getRecord(outer);	
		var specimenIdOuter=oRecord.getData("specimenId");
	  for(inner = outer + 1; inner<arrRecords.getLength(); inner++){ 
		    oRecord = arrRecords.getRecord(inner);	
			var specimenIdInner=oRecord.getData("specimenId");
		if ( (fnTrimSpaces(specimenIdInner.toLowerCase())==fnTrimSpaces(specimenIdOuter.toLowerCase()) ) && (specimenIdInner!='') && (specimenIdOuter!='')){
			alert(DupliSpmenIdEtr_PrvdUnqId);/*alert("Duplicate Specimen Ids are entered, please provide unique Specimen ID");*****/
			return true;
		}
	  }	
  }
}
/**
 * Added the method to customize the cell on save
 */
var onCellSave = function(oArgs) {			
	var oNewData = oArgs.newData;	
	var elCell = oArgs.editor.getTdEl();
	var oRecord = this.getRecord(elCell);			
	var column =  this.getColumn(elCell);
	if (column.key == 'specType'){					
		VELOS.multiSpecimensGrid.setDropDownId(this,elCell,specTypeIds,specTypes);
	}
	if (column.key == 'specStat'){					
		VELOS.multiSpecimensGrid.setDropDownId(this,elCell,specStatIds,specStats);
	}
	
	if (column.key == 'unit'){					
		VELOS.multiSpecimensGrid.setDropDownId(this,elCell,specQUnitIds,specQUnits);
	}
	
	if(oNewData == L_Select_AnOption){
		VELOS.multiSpecimensGrid.eraseDependent(this,elCell,column.key,'');
	}
}
 /** Function to set Codelist value in corresponding hidden columns.
 */
VELOS.multiSpecimensGrid.setDropDownId = function (myGrid,elCell,myIds,myTypes){		
	var oRecord = myGrid.getRecord(elCell);			
	var oColumn =  myGrid.getColumn(elCell);	

	for (var j=0; j<myIds.length; j++){
		if (myTypes[j] == oRecord.getData(oColumn.key)) break;
	}
	oRecord.setData(oColumn.key +'Id', myIds[j]);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	//getting column(td) element of the row taken above.
	var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Id', 'td')[0];

	var idEl = new YAHOO.util.Element(idTd);

	var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	//Setting the value in the div element under the column(td) taken above.	
	idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myIds[j];	
	
};
/**
 * Method to customize the Grid
 */ 
VELOS.multiSpecimensGrid.processColumns = function (respJ,colArray){
	myColumnDefs = [];	
	if (colArray) {
		var tempColumnDefs =[];
		var f;
		myColumnDefs = colArray;
		tempColumnDefs = colArray;
		for (var iX=0; iX<myColumnDefs.length; iX++) {  //Ak:Removed the code to reduced the width  of few columns.
			myColumnDefs[iX].width = myColumnDefs[iX].key == 'quantity' ? 80:
			myColumnDefs[iX].width = myColumnDefs[iX].key == 'unit' ? 80:100;
			
			myColumnDefs[iX].resizeable = true;
			myColumnDefs[iX].sortable = false;

			if (myColumnDefs[iX].key == 'specType') {
				myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.specType,disableBtns:true});
			}
			if (myColumnDefs[iX].key == 'specStat') {
				myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.specStat,disableBtns:true});
			}	
			if (myColumnDefs[iX].key == 'unit') {
				myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.specQUnit,disableBtns:true});
			}
			if (myColumnDefs[iX].key == 'delete') {
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].width = 50;
			}
			if (!myColumnDefs[iX].madeUp) {
				f = {key:myColumnDefs[iX].key};
			}
			if (myColumnDefs[iX].key == 'statDt') {
				myColumnDefs[iX].editor = new YAHOO.widget.DateCellEditor();
				f.parser = 'date';	
			}
			
			if (myColumnDefs[iX].type!=undefined){
				var type= myColumnDefs[iX].type;
			 	var typeInfo =VELOS.multiSpecimensGrid.types[type];
			 	if (typeInfo.parser!=undefined){
			 		myColumnDefs[iX].parser=typeInfo.parser;
			 		f.parser = typeInfo.parser;	
			 	}
			 	if(myColumnDefs[iX].formatter==undefined){
			 	  if (typeInfo.formatter!=undefined)
			 	   	myColumnDefs[iX].formatter=typeInfo.formatter;
			 	}
			 	if (typeInfo.stringer!=undefined)
			 		myColumnDefs[iX].stringer=typeInfo.stringer;
				if (typeInfo.editorOptions!=undefined){
					myColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
				}
			
			}//parser end	
			
			if (!myColumnDefs[iX].madeUp) {
				myFieldArray.push(f);
			}
			
		}
	}
	return myColumnDefs;
	
}
/**
 * Reset the patient/Study ids fileds when ther desc fileds are blank
 *
 */
VELOS.multiSpecimensGrid.nullifyStudyPatientIdFields=function(myChildGrid){ ////Ak:Fixed BUG#6796
	var arrRecords = myChildGrid.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){		
		var oRecord = arrRecords.getRecord(rec);	
		var specSeq=oRecord.getData("specSeq");
		var selStudyVal=$('selStudy'+specSeq).value;
		if(selStudyVal==null ||selStudyVal==""){
			$('selStudyIds'+specSeq).value="";
		}
		var patientNamesVal=$('patientNames'+specSeq).value;
		if(patientNamesVal==null ||patientNamesVal==""){
			patientIdsVal=$('patientIds'+specSeq).value="";
		}	
  }
}
//Removed the code For BUG#6683(Enhcnacement no more supporting lookup feature for (Study/Patient)fields.

//End here



