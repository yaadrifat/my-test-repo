$j(function(){
   jQuery.validator.addMethod("checkselect", function(value, element) {
		return (true && (parseInt(value) != -1));
	}, "<%=LC.L_PlsSel_Val%>");/*}, "Please Select Value");*****/

	jQuery.validator.addMethod("greaterThanZero", function(value, element) {
	    return this.optional(element) || (parseFloat(value) < 101);
	}, "<%=MC.M_PcentMust_GtZero%>");/*}, "Percentage must be greater than zero");*****/
	
	jQuery.validator.addMethod("checksum", function(value, element) {
		return (true && (CheckSum(value)));
	}, "<%=MC.M_PlsEtr_ValidVal%>");/*}, "Please Enter Valid Value");*****/	

	jQuery.validator.addMethod("checkdbdata", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkdbdata1", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	
	
	jQuery.validator.addMethod("checkdbdata2", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 var flag = validateDbData(url);
		 var flag1 = false;
		 if(!flag)flag1=true;
		 return (true && (flag1));
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checkisbtdata", function(value, element,params) {
		 var url = "getCodeValidate?refnum="+params[0]+"&codeval="+value;
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("checkspecialchar", function(value, element) {
		 return (true && (checkSpecialCharacters(value)));		 
	}, "<%=MC.M_PlsDoNtEtr_SplChar%>");/*}, "Please Do Not Enter Special Characters");*****/

	jQuery.validator.addMethod("checklength", function(value, element) {
		 return (true && (checkMininum(value)));		 
	}, "<%=MC.M_EtrOnly9Dgt_AlphaVal%>");	/*}, "Please Enter Only 9 Digit Alphanumeric Value");	*****/

	jQuery.validator.addMethod("checkformat", function(value, element) {
		 return (true && (checkFormat(value)));		 
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Enter Value in xxxx-xxxx-x format");*****/	

	jQuery.validator.addMethod("checkdbadditionaldata", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#regMaternalId").val();
		 var url1 = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 if(value==""){return true;}else{
		 return (true && (validateDbAdditionalData(url) || validateDbData(url1)));}
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checklocalcbuiddbdata", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata", function(value, element, params) {
		var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#userid").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata1", function(value, element, params) {
		var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#userid").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkisbtdbdata", function(value, element,params) {
		var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("validateValues", function(value, element,params) {
		return (true && (validateValues(params[0],params[1])));		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("validateSameValues", function(value, element,params) {
        var flag = false;
		var val = validateValues(params[0],params[1]);
		if(val==false){
           flag = true;
		}
		return (true && flag);		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("validateRequired", function(value, element,params) {
        var flag = true;
		if($j("#"+params[0]).val()=="" && value==""){
			flag =false;
		}
		return (true && flag);		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("validateRequiredByName", function(value, element,params) {
		//alert($j("input[name="+params[0]+"]").val());
		//alert($j("#"+params[0]).val()+params[0]);
        var flag = true;
        if($j("#"+params[0]).val()==undefined){
			return true;
        }
		if($j("#"+params[0]).val()!="" && value==""){
			flag =false;
		}
		return (true && flag);		 
	}, "Please Enter Data");	
	 
	jQuery.validator.addMethod("checkselectbyname", function(value, element,params) {
		var flag = true;
		//alert($j("#"+params[0]).val()+params[0]);
		if($j("#"+params[0]).val()==undefined){
			return true;
		}
		if($j("#"+params[0]).val()!="" && (parseInt(value) == -1)){
                flag = false;
	    }
		return (true && flag);
	}, "<%=LC.L_PlsSel_Val%>");/*}, "Please Select Value");*****/
	
	jQuery.validator.addMethod("validateAntigen", function(value, element, params) {
		var url = "validateAntigen?entityValue="+$j("#"+params[0]).val();
		 if($j("#"+params[0]).val()=="" && value==""){return true;
		 }else if($j("#"+params[0]).val()=="" && value!=""){
           return false;
		 }else{ return (true && (validateDbData(url)));}	
	}, "<%=MC.M_EtrVld_HlaTyping%>");/*}, "Please Enter Valid HLA Typing");*****/
	
	jQuery.validator.addMethod("checkfundingguidelinesdate", function(value, element,params) {
		var url = "getValidfundingguidelinesdate?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+$j('#datepicker1').val()+"&entityField1="+params[2]+"&entityValue1="+$j('#fkFundCateg').val()+"&entityField2="+params[3]+"&entityValue2="+$j('#fkCbbId').val();
		if(value=="" && $j('#datepicker1').val()==""){return true;}
		 else{ return (true && (validateDbData(url)));}		 
	}, "<%=MC.M_EtrAnother_Date%>");/*}, "Please Enter Another Date");*****/

	jQuery.validator.addMethod("checkDeferMaternalRegistryId", function(value, element, params) {
		var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#deferId").val();
		 if(value==""){return true;}
		 else{ 
			 var val1 = validateDbData(url);			 
             $j("#checkMatRegDefer").val(val1);
			 return true;}	
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checkdbmatreg", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
         
		 var val1 = validateDbData(url);		 
         $j("#checkMatReg").val(val1);
		 return true;
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checkdbreg", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 var val1 = validateDbData(url);		 
         $j("#checkReg").val(val1);
		 return true;
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkDates", function(value, element, params) {
		 return (true && (checkDates(params[0],params[1])));		 
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Check The Date You Entered");*****/	

	jQuery.validator.addMethod("checkDatesProc", function(value, element, params) {
		 return (true && (checkDatesProc(params[0],params[1])));		 
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Check The Date You Entered");*****/
	
	jQuery.validator.addMethod("checkHour", function(value, element, params) {
		 return (true && (checkHour(params[0],params[1])));		 
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Check The Hour You Entered");*****/	

	jQuery.validator.addMethod("checkTimeFormat", function(value, element) {
		if(value==""){
			return true;
		}else{
		 return (true && (dateTimeFormat(value)));		 
		}
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Enter Time format hh:mm ");*****/
});  