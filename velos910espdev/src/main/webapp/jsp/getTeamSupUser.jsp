<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%=LC.L_User_List%><%--User List*****--%></title>
</HEAD>	
<%-- Nicholas: Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%--<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>--%>
<%-- Nicholas: End --%>




<BODY>
<jsp:useBean id="team" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
 HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

	String studyId = request.getParameter("studyId");
	


	TeamDao tDao = new TeamDao();

	tDao = team.getSuperUserTeam(EJBUtil.stringToNum(studyId));

	ArrayList teamUserLastNames 	= tDao.getTeamUserLastNames();
    ArrayList teamUserFirstNames = tDao.getTeamUserFirstNames();
	ArrayList userSiteNames = tDao.getUsrSiteNames();
	 
   String teamUserLastName = null;
   String teamUserFirstName = null;  
   
   String teamSite = null;
   
   int len = teamUserLastNames.size();
   int counter = 0;
   
%>
<div class="popDefault" id="div">
	<BR>
	<P class="sectionHeadings"><%=MC.M_UsrHaving_SuperRgts%><%--Users having Super User access rights*****--%> :</P>
<%-- Nicholas: Start --%>		
	<table class="comPopup" width="100%" border=0>
<%-- Nicholas: End --%>	
		 <tr> 
				  	<th width="20%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
			        <th width="20%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>

	      </tr>
	<%	
		
	
	 for(counter = 0;counter<len;counter++)
		{
		
			teamUserLastName=((teamUserLastNames.get(counter)) == null)?"-":(teamUserLastNames.get(counter)).toString();
			teamUserFirstName=((teamUserFirstNames.get(counter)) == null)?"-":(teamUserFirstNames.get(counter)).toString();
			
			teamSite = (String)userSiteNames.get(counter);
	
			if (teamSite == null){
				teamSite ="-";
			}	
		
			
			if ((counter%2) == 0) { %>
 				 <tr class="browserEvenRow"> 
        	<%
				}else{
			  %>
 			    <tr class="browserOddRow"> 
	         <% }	
		%>
			<td> <%= teamSite%> </td>
	        <td> <%=teamUserFirstName%> <%=teamUserLastName%> </td>
		
	 	</tr>		
		
	<%	
		}
		
	%>
		
		
	</table>	
	
	<P align="center">
		<button onClick="javascript:self.close()"><%=LC.L_Close%></button>
	</P>

</div>
<%
} //end of if for session
else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%
}%>





</BODY>
</HTML>
