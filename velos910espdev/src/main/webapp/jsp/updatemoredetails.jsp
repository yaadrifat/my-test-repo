<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Updt_OtherStdIds%><%--Update Other <%=LC.Std_Study%> IDs*****--%></title>
<%@ page import="com.velos.eres.service.util.LC"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="moredetails" scope="request"  class="com.velos.eres.web.moreDetails.MoreDetailsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.moreDetails.impl.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault">

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>

		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign))
	   {

		  String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			int errorCode=0;
			int rows = 0;

			String recordType="" ;
			String creator="";
			String ipAdd="";
			String accountId="";
			String modId="",modName="";



			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");

			modId = request.getParameter("modId");
			modName = request.getParameter("modName");

			String[] arRecordType = request.getParameterValues("recordType");
			String[] arAlternateId = request.getParameterValues("alternateId");
			String[] arId = request.getParameterValues("id");
			String[] arElementId = request.getParameterValues("elementId");



			MoreDetailsBean mdbMain = new MoreDetailsBean();

			ArrayList alElementData = EJBUtil.strArrToArrayList(arAlternateId) ;

				int len = alElementData.size();

				for ( int j = 0 ; j< len ; j++)
				{
					MoreDetailsBean mdbTemp = new MoreDetailsBean();

					if   ( ( !(EJBUtil.isEmpty (arAlternateId[j])) &&  arRecordType[j].equals("N")) || arRecordType[j].equals("M"))
					{
						mdbTemp.setId(EJBUtil.stringToNum(arId[j]));
						mdbTemp.setModId(modId) ;
						mdbTemp.setModName(modName) ;
						mdbTemp.setModElementId(arElementId[j]);
						mdbTemp.setModElementData(arAlternateId[j]);
						mdbTemp.setRecordType(arRecordType[j]);
						if (arRecordType[j].equals("N"))
						{
							mdbTemp.setCreator(creator);
						}else
						{
							mdbTemp.setModifiedBy(creator);
						}
						mdbTemp.setIpAdd(ipAdd);
						mdbMain.setMoreDetailBeans(mdbTemp);
					}
				}

			errorCode = moredetails.createMultipleMoreDetails(mdbMain,defUserGroup);

		%>
		<br><br><br><br><br><br><br>
		<table "width = 500" align = "center">
			<tr>
				<td width = 500>



		<%


		if ( errorCode == -2  )
		{
%>
			<p class = "successfulmsg" >
	 			<%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%>
			</p>

<%

		} //end of if for Error Code

		else

		{
%>

		<p class = "successfulmsg" >
			<%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%>
		</p>

		</td>
		</tr>
		</table>
		<script>
			//window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			}//end of else of incorrect of esign
     }//end of if body for session

	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>

