<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_New_Sec%><%--New Section*****--%></title>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
checkQuote='N';
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<script>
function  validate(formobj,rows, mode)
 {

 	 
	 flag = true ;
	 mycount = 0;
	 cnt = 0;
	 arr = new Array(rows) ;
	if(rows==1)
	 {
		if(formobj.sectionSequence.value == ""  && formobj.sectionName.value == "" ) {
		mycount++;
	 }
	 }
	else{
		for(count=0; count < rows ; count++)
		 {
			if(formobj.sectionSequence[count].value != "")
			 {
				arr[cnt] = formobj.sectionSequence[count].value;
				cnt++;
			 }
			if(formobj.sectionSequence[count].value == ""  && formobj.sectionName[count].value == "" ) {			
			mycount++;
			}
		 }
	}

len = arr.length;
for(i=0;i<len;i++)
	 {
		for(j=i+1;j<len;j++)
		 {


			if(arr[i]==arr[j])
			 {
				var paramArray = [formobj.sectionSequence[j].value];
				alert(getLocalizedMessageString("M_SeqNumAldy_ChgSeq",paramArray));/*alert("Sequence number "+formobj.sectionSequence[j].value+" is already entered. Please change the sequence");*****/
				formobj.sectionSequence[j].focus();
				return false;
			 }
		 
		 }
	 
	 }
	if(mycount==rows)
	 {
		alert("<%=MC.M_EtrAtleast_OneSec%>");/*alert("Please enter atleast one Section");*****/
		return false;
	 }
	

	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	if (isNaN(formobj.eSign.value) == true) 
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		 return false;
	}
	


	
	if ( mode == "N")
	{
	
		if ( rows > 1) 
		{
			
			for(cnt= 0 ; cnt < rows ; cnt ++)
			{
				
				if ( (formobj.sectionSequence[cnt].value == "" ) && (formobj.sectionName[cnt].value == "") && (formobj.repeatNum[cnt].value =="" ) )
				{
					flag = false ; 
				}
				else 
				{
					flag = true ;
					break ; 
				} 
			}
			
	
			for( cnt = 0; cnt <rows ; cnt ++)
			{
			
			 if(formobj.sectionSequence[cnt].value == ""  && formobj.sectionName[cnt].value != "" ){
				alert("<%=MC.M_PlsEnterSeq%>");/*alert("Please enter Sequence");*****/
				formobj.sectionSequence[cnt].focus();
				return false;
			 }
			 if(formobj.sectionSequence[cnt].value != ""   && formobj.sectionName[cnt].value == "" ){
				alert("<%=MC.M_Etr_SecName%>");/*alert("Please enter Section Name");*****/
				formobj.sectionName[cnt].focus();
				return false;	 
			 }

			  if((formobj.sectionSequence[cnt].value != "" ||  formobj.sectionName[cnt].value != ""  )  && formobj.repeatNum[cnt].value==""){
				alert("<%=MC.M_Etr_RepeatFld%>");/*alert("Please enter Repeat Fields");*****/
				formobj.repeatNum[cnt].focus();
				return false;	 
			 }
	
							
				if(isNaN(formobj.sectionSequence[cnt].value) == true) 
				{
					alert("<%=MC.M_SeqHasNum_ReEtr%>") ;/*alert("Sequence Has to be a number. Please enter again") ;*****/
					formobj.sectionSequence[cnt].focus( ) ;
					 return false  ;
   				}
		
				if(isInteger(formobj.sectionSequence[cnt].value) == false) 
				{
				 	alert("<%=MC.M_DecValNotInSeq%>") ;/*alert("Decimal value not allowed in Sequence. Please enter again") ;*****/
				    formobj.sectionSequence[cnt].focus() ;
				     return false  ;
			    }
			
				if(isNaN(formobj.repeatNum[cnt].value) == true) 
				{
					alert("<%=MC.M_RepeatNum_HasToNum%>");/*alert("Repeat Number Has to be a number. Please enter again");*****/
					formobj.repeatNum[cnt].focus();
					 return false;
			   	}
		
				if(isInteger(formobj.repeatNum[cnt].value) == false) 
				{
					alert("<%=MC.M_DecimalNot_InRepeatNum%>");/*alert("Decimal value not allowed in Repeat Number. Please enter again");*****/
					formobj.repeatNum[cnt].focus();
					 return false;
			   	}
				
				if  (  ( formobj.sectionSequence[cnt].value != "")  &&(  (formobj.sectionName[cnt].value == "") ||  (formobj.repeatNum[cnt].value ==" " )    )   )
				{
					if (formobj.sectionName[cnt].value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please enter a value");*****/
						formobj.sectionName[cnt].focus();
						 return false;
					}
			
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RptNumCntBlank_ReEtr%>");/*alert("Repeat Number cannot be left blank. Please enter again");*****/
						formobj.repeatNum[cnt].focus();
					 return false;
			 		}
				}
		
		
				if  (  ( formobj.sectionName[cnt].value != "")  &&(  (formobj.sectionSequence[cnt].value == "") || (formobj.repeatNum[cnt].value ==" " ) )   )
				{
					if (formobj.sectionSequence[cnt].value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
			    		formobj.sectionSequence[cnt].focus();
						 return false;
					}
			
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
						formobj.repeatNum[cnt].focus();
					 return false;
		 			}
				}
		
		
				if  (  ( formobj.repeatNum[cnt].value != "0" )  &&(  (formobj.sectionSequence[cnt].value == "") || (formobj.sectionName[cnt].value ==" " ) )   )
				{
					if (formobj.sectionSequence[cnt].value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
							formobj.sectionSequence[cnt].focus();
							 return false;
					}
			
					if (formobj.sectionName[cnt].value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*Section Name cannot be left blank. Please  enter a value*****/
						formobj.sectionName[cnt].focus();
					 return false;
			 		}
				}
		
				if (  ( formobj.repeatNum[cnt].value != "0")  && (formobj.sectionName[cnt].value !== "")   )
				{
					if( formobj.sectionSequence[cnt].value==null)
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please  enter a value");*****/
						formobj.sectionSequence[cnt].focus();
						 return false;
					}
				}
		
				if (  ( formobj.repeatNum[cnt].value != "0")  && (formobj.sectionSequence[cnt].value !== "")   )
				{
					if( formobj.sectionName[cnt].value==null)
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Namecannot be left blank. Please enter a value");*****/
						formobj.sectionName[cnt].focus();
						 return false;
					}
				}
		
				if (  ( formobj.sectionSequence[cnt].value != "")  && (formobj.sectionName[cnt].value !== "")   )
				{
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
						formobj.repeatNum[cnt].focus();
						 return false;
					}
				}
				
			}//for loop
		}// end of rows >1 //mode N
		else if (rows ==1)
		{
			
		
		 if(formobj.sectionSequence.value == ""  && formobj.sectionName.value != "" ){
			alert("<%=MC.M_PlsEnterSeq%>");/*alert("Please enter Sequence");*****/
			formobj.sectionSequence.focus();
			return false;
		 }
		 if(formobj.sectionSequence.value != ""   && formobj.sectionName.value == "" ){
			alert("<%=MC.M_Etr_SecName%>");/*alert("Please enter Section Name");*****/
			formobj.sectionName.focus();
			return false;	 
		 }

		  if((formobj.sectionSequence.value != "" ||  formobj.sectionName.value != ""  )  && formobj.repeatNum.value==""){
			alert("<%=MC.M_Etr_RepeatFld%>");/*alert("Please enter Repeat Fields");*****/
			formobj.repeatNum.focus();
			return false;	 
		 }
	
			if ( (formobj.sectionSequence[cnt].value == "" ) && (formobj.sectionName[cnt].value == "") && (formobj.repeatNum[cnt].value ==" ") )
			{
				alert("<%=MC.M_NoValueBlank_EtrData%>") ;/*alert("No value can be left blank in edit mode. Pls enter data for this section.") ;*****/
				formobj.sectionSequence[cnt].focus( ) ;
				 return false  ;
			}
	
			if(isNaN(formobj.sectionSequence.value) == true) 
			{
				alert("<%=MC.M_SeqHasNum_ReEtr%>") ;/*alert("Sequence Has to be a number. Please enter again") ;*****/
				formobj.sectionSequence.focus( ) ;
				 return false  ;
   			}

			if(isInteger(formobj.sectionSequence.value) == false) 
			{
				alert("<%=MC.M_DecValNotInSeq%>") ;/*alert("Decimal value not allowed in Sequence. Please enter again") ;*****/
				formobj.sectionSequence.focus() ;
				 return false  ;
   			}
	
			if(isNaN(formobj.repeatNum.value) == true) 
			{
				alert("<%=MC.M_RepeatNum_HasToNum%>");/*alert("Repeat Number Has to be a number. Please enter again");*****/
				formobj.repeatNum.focus();
				 return false;
		   	}
			
				
			if(isInteger(formobj.repeatNum.value) == false) 
			{
				alert("<%=MC.M_DecimalNot_InRepeatNum%>");/*alert("Decimal value not allowed in Repeat Number. Please enter again");*****/
				formobj.repeatNum.focus();
				return false;
		   	}
		
			if  (  ( formobj.sectionSequence.value != "")  &&(  (formobj.sectionName.value == "") || (formobj.repeatNum.value ==" " ) )   )
			{
					if (formobj.sectionName.value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please enter a value");*****/
						formobj.sectionName.focus();
						 return false;
					}
			
				if (formobj.repeatNum.value == "")
				{
					alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please enter again");*****/
					formobj.repeatNum.focus();
				 return false;
		 		}
			}
		
		
			if  (  ( formobj.sectionName.value != "")  &&(  (formobj.sectionSequence.value == "") || (formobj.repeatNum.value ==" " ) )   )
			{
					if (formobj.sectionSequence.value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
						formobj.sectionSequence.focus();
						 return false;
					}
			
				if (formobj.repeatNum.value == "")
				{
					alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
					formobj.repeatNum.focus();
				 return false;
		 		}
			}
		
		
			if  (  ( formobj.repeatNum.value != "0")  &&(  (formobj.sectionSequence.value == "") || (formobj.sectionName.value ==" " ) )   )
			{
					if (formobj.sectionSequence.value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
						formobj.sectionSequence.focus();
						 return false;
					}
			
				if (formobj.sectionName.value == "")
				{
					alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please  enter a value");*****/
					formobj.sectionName.focus();
				 return false;
		 		}
			}
		
			if (  ( formobj.repeatNum.value != "0")  && (formobj.sectionName.value != "")   )
			{
				if( formobj.sectionSequence.value==null)
				{
					alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please  enter a value");*****/
					formobj.sectionSequence.focus();
					 return false;
				}
			}
		
			if (  ( formobj.repeatNum.value != "0")  && (formobj.sectionSequence.value != "")   )
			{
				if( formobj.sectionName.value==null)
				{
					alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Namecannot be left blank. Please enter a value");*****/
					formobj.sectionName.focus();
					 return false;
				}
			}
		
			if (  ( formobj.sectionSequence.value != "")  && (formobj.sectionName.value !== "")   )
			{
				if (formobj.repeatNum.value == "")
				{
					alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
					formobj.repeatNum.focus();
					 return false;
				}
			}
		
		
		} //else of rows=1
		
		if (  ! flag  )
		{
			alert("<%=MC.M_AllSecs_CntBeBlank%>");/*alert(" All sections cannot be blank.");*****/
			formobj.sectionSequence[0].focus();
		 	return false;
		}
		
	} // mode = 'N'
	
	else if ( mode == "M") 
	{
	
		if ( rows > 1) 
		{
	
			
			
			for( cnt = 0; cnt <rows ; cnt ++)
			{
		
		
			 if(formobj.sectionSequence[cnt].value == ""  && formobj.sectionName[cnt].value != "" ){
				alert("<%=MC.M_PlsEnterSeq%>");/*alert("Please enter Sequence");*****/
				formobj.sectionSequence[cnt].focus();
				return false;
			 }
			 if(formobj.sectionSequence[cnt].value != ""   && formobj.sectionName[cnt].value == "" ){
				alert("<%=MC.M_Etr_SecName%>");/*alert("Please enter Section Name");*****/
				formobj.sectionName[cnt].focus();
				return false;	 
			 }

			  if((formobj.sectionSequence[cnt].value != "" ||  formobj.sectionName[cnt].value != ""  )  && formobj.repeatNum[cnt].value==""){
				alert("<%=MC.M_Etr_RepeatFld%>");/*alert("Please enter Repeat Fields");*****/
				formobj.repeatNum[cnt].focus();
				return false;	 
			 }
	 



				if ( (formobj.sectionSequence[cnt].value == "" ) && (formobj.sectionName[cnt].value == "") && (formobj.repeatNum[cnt].value =="" ) )
				{
					alert("<%=MC.M_NoValueBlank_EtrData%>") ;/*alert("No value can be left blank in edit mode. Pls enter data for this section.") ;*****/
					formobj.sectionSequence[cnt].focus( ) ;
					 return false  ;
				}
				
				
				
				if(isNaN(formobj.sectionSequence[cnt].value) == true) 
				{
					alert("<%=MC.M_SeqHasNum_ReEtr%>") ;/*alert("Sequence Has to be a number. Please enter again") ;*****/
					formobj.sectionSequence[cnt].focus( ) ;
					 return false  ;
   				}
		
				if(isInteger(formobj.sectionSequence[cnt].value) == false) 
				{
					alert("<%=MC.M_DecValNotInSeq%>") ;/*alert("Decimal value not allowed in Sequence. Please enter again") ;*****/
					formobj.sectionSequence[cnt].focus() ;
				 	return false  ;
				}
	
				if(isNaN(formobj.repeatNum[cnt].value) == true) 
				{
					alert("<%=MC.M_RepeatNum_HasToNum%>");/*alert("Repeat Number Has to be a number. Please enter again");*****/
					formobj.repeatNum[cnt].focus();
					 return false;
			   	}
				
				if(isInteger(formobj.repeatNum[cnt].value) == false) 
				{
					alert("<%=MC.M_DecimalNot_InRepeatNum%>");/*alert("Decimal value not allowed in Repeat Number. Please enter again");*****/
					formobj.repeatNum[cnt].focus();
					 return false;
			   	}
			
		
				if  (  ( formobj.sectionSequence[cnt].value != "")  &&(  (formobj.sectionName[cnt].value == "") || (formobj.repeatNum[cnt].value ==" " ) )   )
				{
					if (formobj.sectionName[cnt].value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please enter a value");*****/
						formobj.sectionName[cnt].focus();
						 return false;
					}
			
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please enter again");*****/
						formobj.repeatNum[cnt].focus();
					 return false;
			 		}
				}
		
		
				if  (  ( formobj.sectionName[cnt].value != "")  &&(  (formobj.sectionSequence[cnt].value == "") || (formobj.repeatNum[cnt].value ==" " ) )   )
				{
					if (formobj.sectionSequence[cnt].value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
			    		formobj.sectionSequence[cnt].focus();
						 return false;
					}
			
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
						formobj.repeatNum[cnt].focus();
					 return false;
		 			}
				}
		
		
				if  (  ( formobj.repeatNum[cnt].value != "")  &&(  (formobj.sectionSequence[cnt].value == "") || (formobj.sectionName[cnt].value ==" " ) )   )
				{
					if (formobj.sectionSequence[cnt].value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
							formobj.sectionSequence[cnt].focus();
							 return false;
					}
			
					if (formobj.sectionName[cnt].value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please  enter a value");*****/
						formobj.sectionName[cnt].focus();
					 return false;
			 		}
				}
		
				if (  ( formobj.repeatNum[cnt].value != "")  && (formobj.sectionName[cnt].value !== "")   )
				{
					if( formobj.sectionSequence[cnt].value==null)
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please  enter a value");*****/
						formobj.sectionSequence[cnt].focus();
						 return false;
					}
				}
		
				if (  ( formobj.repeatNum[cnt].value != "")  && (formobj.sectionSequence[cnt].value !== "")   )
				{
					if( formobj.sectionName[cnt].value==null)
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Namecannot be left blank. Please enter a value");*****/
						formobj.sectionName[cnt].focus();
						 return false;
					}
				}
		
				if (  ( formobj.sectionSequence[cnt].value != "")  && (formobj.sectionName[cnt].value !== "")   )
				{
					if (formobj.repeatNum[cnt].value == "")
					{
						alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
						formobj.repeatNum[cnt].focus();
						 return false;
					}
				}
				
			}//for loop
		}// end of rows >1  // mode M
		else if (rows ==1)
		{
	 
		 if(formobj.sectionSequence.value == ""  && formobj.sectionName.value != "" ){
			 alert("<%=MC.M_PlsEnterSeq%>");/*alert("Please enter Sequence");*****/
			formobj.sectionSequence.focus();
			return false;
		 }
		 if(formobj.sectionSequence.value != ""   && formobj.sectionName.value == "" ){
			 alert("<%=MC.M_Etr_SecName%>");/*alert("Please enter Section Name");*****/
			formobj.sectionName.focus();
			return false;	 
		 }

		  if((formobj.sectionSequence.value != "" ||  formobj.sectionName.value != ""  )  && formobj.repeatNum.value==""){
			alert("<%=MC.M_Etr_RepeatFld%>");/*alert("Please enter Repeat Fields");*****/
			formobj.repeatNum.focus();
			return false;	 
		 }
	
			if(isNaN(formobj.sectionSequence.value) == true) 
			{
				alert("<%=MC.M_SeqHasNum_ReEtr%>") ;/*alert("Sequence Has to be a number. Please enter again") ;*****/
				formobj.sectionSequence.focus( ) ;
				 return false  ;
   			}
		
			if(isInteger(formobj.sectionSequence.value) == false) 
			{
				alert("<%=MC.M_DecValNotInSeq%>") ;/*alert("Decimal value not allowed in Sequence. Please enter again") ;*****/
				formobj.sectionSequence.focus() ;
				 return false  ;
   			}
	
			
			if(isNaN(formobj.repeatNum.value) == true) 
			{
				alert("<%=MC.M_RepeatNum_HasToNum%>");/*alert("Repeat Number Has to be a number. Please enter again");*****/
				formobj.repeatNum.focus();
				 return false;
		   	}
			
				
			if(isInteger(formobj.repeatNum.value) == false) 
			{
				alert("<%=MC.M_DecimalNot_InRepeatNum%>");/*alert("Decimal value not allowed in Repeat Number. Please enter again");*****/
				formobj.repeatNum.focus();
				return false;
		   	}			
		
			if  (  ( formobj.sectionSequence.value != "")  &&(  (formobj.sectionName.value == "") || (formobj.repeatNum.value ==" " ) )   )
			{
					if (formobj.sectionName.value == "")
					{
						alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please enter a value");*****/
						formobj.sectionName.focus();
						 return false;
					}
			
				   if (formobj.repeatNum.value == "")
				   {
					   alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please enter again");*****/
					   formobj.repeatNum.focus();
				       return false;
		 		   }
			}
		
		
			if  (  ( formobj.sectionName.value != "")  &&(  (formobj.sectionSequence.value == "") || (formobj.repeatNum.value ==" " ) )   )
			{
					if (formobj.sectionSequence.value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
						formobj.sectionSequence.focus();
						 return false;
					}
			
				if (formobj.repeatNum.value == "")
				{
					alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
					formobj.repeatNum.focus();
				 return false;
		 		}
			}
		
		
			if  (  ( formobj.repeatNum.value != "")  &&(  (formobj.sectionSequence.value == "") || (formobj.sectionName.value ==" " ) )   )
			{
					if (formobj.sectionSequence.value == "")
					{
						alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please enter a value");*****/
						formobj.sectionSequence.focus();
						 return false;
					}
			
				if (formobj.sectionName.value == "")
				{
					alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Name cannot be left blank. Please  enter a value");*****/
					formobj.sectionName.focus();
				 return false;
		 		}
			}
		
			if (  ( formobj.repeatNum.value != "")  && (formobj.sectionName.value !== "")   )
			{
				if( formobj.sectionSequence.value==null)
				{
					alert("<%=MC.M_SecSeqCntBlank_ReEtr%>");/*alert("Section Sequence cannot be left blank. Please  enter a value");*****/
					formobj.sectionSequence.focus();
					 return false;
				}
			}
		
			if (  ( formobj.repeatNum.value != "")  && (formobj.sectionSequence.value !== "")   )
			{
				if( formobj.sectionName.value==null)
				{
					alert("<%=MC.M_SecNameCntBlank_ReEtr%>");/*alert("Section Namecannot be left blank. Please enter a value");*****/
					formobj.sectionName.focus();
					 return false;
				}
			}
		
			if (  ( formobj.sectionSequence.value != "")  && (formobj.sectionName.value !== "")   )
			{
				if (formobj.repeatNum.value == "")
				{
					alert("<%=MC.M_RepeatNum_CntBlank%>");/*alert("Repeat Number cannot be left blank. Please  enter a value");*****/
					formobj.repeatNum.focus();
					 return false;
				}
			}
		
		
		} //else
	} // mode = 'M'
	
	
	
	
	
 }






</script>

<%@ page language = "java"  import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao,com.velos.eres.web.grpRights.GrpRightsJB"%><%@page import="com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="formSecJB" scope="request" class="com.velos.eres.web.formSec.FormSecJB"/>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<DIV class="popDefault" > 

<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>
<% 	
 
	int pageRight = 0;	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
			    
	String mode =request.getParameter("mode");
	String formId =request.getParameter("formId");
	String codeStatus=request.getParameter("codeStatus");		
	if (codeStatus==null)
	{
		codeStatus = "-";
	}
		
	String newMode=mode ;
	String makeReadOnly = ""; 
	String inputClass = "inpDefault"; 
	
	if (pageRight >= 4)
		{
		%>
		<P class="sectionHeadings"> <%=LC.L_Form_Sections%><%--Form >> Sections*****--%> </P>
		<%
		String formSecId= "";
		String formSecName= "";
		String formSecSeq="";
		String formSecRepNo="0";
		String formSecFmt="";	
		String offlineFlag = "";
		
		int rows = 0 ;

		FormSecDao formSecDao = new FormSecDao() ;
		ArrayList formSecIds = new ArrayList () ;  
		ArrayList offlineFlags = new ArrayList();
		
			
		
		if ( mode.equals("N"))
		{
			rows = 5;
			newMode= "N" ;
		}
		else if ( mode.equals("M") ) 
		{
			int formLibId = EJBUtil.stringToNum(formId);
			formSecDao = formSecJB.getAllSectionsOfAForm(formLibId) ;	
			
			offlineFlags = formSecDao.getOfflineFlag();
			
			formSecIds = formSecDao.getFormSecId();
			
			rows=formSecDao.getRows();
			
			newMode = "M" ; 

			
		}
		
%>
<Form  name="newSections" id="newSecFrmId" method="post" action="newSectionSubmit.jsp" onsubmit="if (validate(document.newSections,'<%=rows%>','<%=mode%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name="mode" value="<%=newMode%>">
<input type="hidden" name="formId" value="<%=formId%>">

    <table class=tableDefault width="60%" cellspacing="2" cellpadding="0" border=0 >
      <tr class="browserOddRow"> 
		<th width="10%"> <%=LC.L_Sequence%><%--Sequence*****--%> <FONT class="Mandatory">* </FONT></th>
      	<th width="25%"> <%=LC.L_Sec_Name%><%--Section Name*****--%> <FONT class="Mandatory">* </FONT></th>
        <th width="15%"> <%=LC.L_Repeat_Flds%><%--Repeat Fields*****--%> <FONT class="Mandatory">* </FONT></th>
       	<th width="10%"> <%=LC.L_Format%><%--Format*****--%> </th>
		
         </tr>
<%
	

		for(int i=0;i<rows;i++)
		{
			
			if ( mode.equals("M") )
			{
				
				formSecName= formSecDao.getFormSecName(i).toString() ;
				formSecSeq=formSecDao.getFormSecSeq(i).toString();
				formSecRepNo=formSecDao.getFormSecRepNo(i).toString();
				formSecFmt=formSecDao.getFormSecFmt(i).toString();
				

				formSecId = formSecIds.get(i).toString() ;
				if(offlineFlags.get(i) == null)
				{
					offlineFlag = "0";
				}
				else{
					offlineFlag = offlineFlags.get(i).toString();
				}
				
				
			
				
				if (codeStatus.equals("Offline") && !offlineFlag.equals("0")) {
				
				   makeReadOnly = "ReadOnly";
				   inputClass = "inpGrey";
				}
				else{
					makeReadOnly = ""; 
					inputClass = "inpDefault"; 
				}
				
			} 	
	
			if(i%2==0)
			{
%>
			 <tr >
<%        
			} 
			 else
			{ 
%>
				<tr >
<%
			}
%>
			
			<td><input type="text" name="sectionSequence" size = 10 MAXLENGTH = 4 value="<%=formSecSeq%>"> </td>
		    <td ><input class="inpDefault" type="text" name="sectionName" size =30 MAXLENGTH = 50 value="<%=formSecName%>"  > </td>
			<td ><input class="<%=inputClass%>" type="text" name="repeatNum" size = 15 MAXLENGTH = 4 value="<%=formSecRepNo%>" <%=makeReadOnly%> > </td>
			
<%   	  if (formSecFmt.equals("N"))
		  { 
%>
			<td> 	
			
				<select name="sectionFormat" size="1">
					<option value="N" SELECTED> <%=LC.L_Nontabular%><%--Non-Tabular*****--%></option>
				  	<option value="T"> <%=LC.L_Tabular%><%--Tabular*****--%></option>
				</select>
			</td>		
		   </tr>
<%       }
		if (formSecFmt.equals("T") )
		{
%>			<td>
			 	<select name="sectionFormat" size="1">
				  	<option value="N" > <%=LC.L_Nontabular%><%--Non-Tabular*****--%></option>
				  	<option value="T" SELECTED > <%=LC.L_Tabular%><%--Tabular*****--%></option>
				</select>
			
			</td> 
			</tr>		
		
	<%  }%>		
	<% if ( formSecFmt.equals("")) 
		{ %>
	
				<td><select name="sectionFormat" size="1">
				  	<option value="N" SELECTED> <%=LC.L_Nontabular%><%--Non-Tabular*****--%></option>
				  	<option value="T"> <%=LC.L_Tabular%><%--Tabular*****--%></option> </select>
				</td>		
			   </tr>

		<%}%>			
  	<td><input type="hidden" name="formSecId"  value="<%=formSecId%>"> </td>
  <% }	//for looop
%>
	<tr>
	<td class=tdDefault colspan=5>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="newSecFrmId"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	</td>
	</tr>		
	 </table>		
	</Form>

<%
		}// end of page right
else
{
%>
	 <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
}
  }//end of if body for session
  else
{
%>
	
 <jsp:include page="timeout.html" flush="true"/> 
  <%
}

%>
</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>

