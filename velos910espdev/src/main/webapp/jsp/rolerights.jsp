<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>	
<HEAD>
<%@ page import="java.util.*,java.io.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<Link Rel=STYLESHEET HREF="styles/velos_style.css" type=text/css>

<TITLE><%=LC.L_Role_AccessRights%><%--Role Access Rights*****--%></TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT Language="javascript">

	
	function changeRights(obj,row,formobj)

	{
	  selrow = row ;


	  totrows = formobj.totalrows.value;
	  
	  if (totrows > 1) 		
	  	rights =formobj.rights[selrow].value;

	  else 		
	  	rights =formobj.rights.value;

	  objName = obj.name;

	  if (obj.checked)
   	  {
         
       	if (objName == "newr") 

	{
		rights = parseInt(rights) + 1;

		if (!formobj.view[selrow].checked)

		{ 
			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;
		}

	}


     	if (objName == "edit") 
	{

		rights = parseInt(rights) + 2;

		if (!formobj.view[selrow].checked)

		{ 

			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

	}



     	if (objName == "view") rights = parseInt(rights) + 4;

	if (totrows > 1 )

		formobj.rights[selrow].value = rights;

	else

		formobj.rights.value = rights;

  }else

	{

       	if (objName == "newr") rights = parseInt(rights) - 1;

       	if (objName == "edit") rights = parseInt(rights) - 2;

       	if (objName == "view") 
		{	
			if (formobj.newr[selrow].checked) 
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;		
			}
	
		    else if (formobj.edit[selrow].checked) 
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;		
			}
			else
			{		
			  rights = parseInt(rights) - 4;
			}
		}
		

	if (totrows > 1 ) {

		formobj.rights[selrow].value = rights;
		
    } 
	else {

		formobj.rights.value = rights; 
		

  	 }
	
  }
}

function fnEmpty(formobj){
	
		if (isEmpty(formobj.role.value))
		{	alert("<%=MC.M_Etr_MandatoryFld%>");/*alert("Please enter mandatory fields");*****/	
			formobj.role.focus();
			return false;	 
		} 
		if (isEmpty(formobj.rolesubtype.value)) {	 
			alert("<%=MC.M_Etr_MandatoryFld%>");/*alert("Please enter mandatory fields");*****/		
			formobj.rolesubtype.focus();
			return false;	 
		} 
		return true;
	} 
	
</SCRIPT>



<% 
String src;
src= request.getParameter("srcmenu");
String mode=request.getParameter("mode");
mode=(mode==null)?"":mode;
String role=request.getParameter("role");
role=(role==null)?"":role;
String rolesubtype=request.getParameter("rolesubtype");
rolesubtype=(rolesubtype==null)?"":rolesubtype;
String cid=request.getParameter("cid");

%>

<BODY>

<br>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
	HttpSession tSession = request.getSession(true); 
	int pageRight = 0;

	if (sessionmaint.isValidSession(tSession))

	{
	
%>

  <Form Name="stdRights" method="post" action="updaterolerights.jsp" onsubmit="return fnEmpty(document.stdRights);">
<%

	String userId = request.getParameter("userId");

	ctrl.getControlValues("study_rights");

	int rows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();

	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	int prevSeq =0;
	String str="";
%>
 	
	<TABLE width="400" cellspacing="0" cellpadding="0" colspan="2">
	<tr> <td> <%=LC.L_Role_Name%><%--Role Name*****--%> : <FONT class="Mandatory" color="red">* </FONT></td> <td> <input type="text" name="role" value="<%=role%>" ></td></tr>
	<tr> <td> <%=LC.L_Role_SubType%><%--Role Sub Type*****--%>:<FONT class="Mandatory" color="red">* </FONT></td><td><input type="text" name="rolesubtype" value="<%=rolesubtype%>" ></td></tr> 	
	<tr><td>&nbsp;</td></tr>
	<tr> <td><B> <%=LC.L_Access_Rights%><%--Access Rights*****--%>:</B></td></tr>
	
	<% if (mode.equals("N")){%>
		 <tr><B><%=LC.L_Define_NewRole%><%--Define New Role*****--%></B></tr>
	<% }
	  else if(mode.equals("M"))	 { %>
	 <tr><B><%=LC.L_Edit_RoleDefn%><%--Edit Role Definition*****--%></B></tr>
	 <input type="hidden" name="cid" value=<%=cid%>>
	 <%}%>
	 <Input type="hidden" name="totalrows" value= <%=rows%> >
	 <input type="hidden" name="mode" value=<%=mode%>>
 	<%
 	
	 boolean showRight = false;
	 
	 boolean showNew = true;
	 boolean showEdit = true;
	 boolean showView = true;
	 
	String rights="";
     	String ctrlVal="";
	char c;
     	if (mode.equals("M"))
	{
	CtrlDao ctrlDao=new CtrlDao();
	ctrlVal=ctrlDao.getControlValue(rolesubtype);
	      
	}     
     	for(int count=0;count<rows;count++){ 
     		String ftr = (String) feature.get(count);
			String desc = (String) ftrDesc.get(count);
			Integer seq = (Integer) ftrSeq.get(count);
			showNew = true;
			showEdit = true;
			showView = true;
			
			if ( (ftr.substring(0,2)).equals("H_") )
    		{
    			desc = "<br><b>" + desc + "</b>";
	    		showNew = false;
    			showEdit = false;
    			showView = false;
	    		rights = "0";
    	   	} else if ( (ftr.compareTo("STUDYSEC") == 0) || (ftr.compareTo("STUDYAPNDX") == 0)  || (ftr.compareTo("STUDYEUSR") == 0))
	    	{
    			desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc ;
    		}
			
			
			
		if(mode.equals("N"))
		{
		 rights="0";
		}
			
		if(mode.equals("M"))
		{
			c=ctrlVal.charAt(count);
			rights = Character.toString(c);
		}
	
%>
         <Input type="hidden" name=rights value= <%=rights%> >
	<TR>
		  
        <TD>
		<%= desc%>
		</TD>		 
		</TD>
          <%
     		//check for special rights

	       	if ((ftr.compareTo("STUDYSUM") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  || (ftr.compareTo("STUDYREP") == 0))
			{
			  showNew = false;
			}
			
			if ( (ftr.compareTo("STUDYNOTIFY") == 0)  || (ftr.compareTo("STUDYREP") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  )
			{
			  showEdit = false;
			}
							
			//end of check		

		  
		  if (rights.compareTo("7") == 0){%>
           <TD align=right>
	  	    <%
			if (showNew)
			{
	  		%>
			   <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
		       <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
         	  <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  	<%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("1") == 0){ %>
        <TD align=right>
		 <%
			if (showNew)
			{
	  		%> 
	          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if(rights.compareTo("3") == 0){%>
        <TD align=right>
		 <%
			if (showNew)
			{
	  		%> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
   			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>

	      <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("2") == 0){%>
	   <TD align=right>
	    <%
			if (showNew)
			{
	  		%> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>

          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("4") == 0){%>
        <TD align=right>
		 <%
			if (showNew)
			{
	  		%> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("5") == 0){%>
        <TD align=right> 
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("6") == 0){%>
        <TD align=right>
		 <%
			if (showNew)
			{
	  		%> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
        <%}else{ %>
        <TD align=right>
		 <%
			if (showNew)
			{
	  		%> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
        			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=right>
		  <%
			if (showEdit)
			{
    		%>
 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			} 
			%>   
         </TD>
		 <TD align=right>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			} 
			%>
        </TD>
       
      </TR>
      <%}
	 }//end of show
	  %>
      <TR></tr>
	</table>
		
	<table>
	<tr>
	   
	   <td width="60%">&nbsp;</td>
	
		<td width="40%"><button type="submit"><%=LC.L_Submit%></button></td> 
	</tr>
	</table>	

</Form>

  <%
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
</div>
</BODY>
</HTML>







