<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
	<title> <%=MC.M_VelosEres_Hpage%><%--Velos eResearch >> Homepage*****--%> </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<SCRIPT language="JavaScript1.1">
window.name="formWin";
function openuserwin(userId) {
      windowName = window.open("viewuser.jsp?userId=" + userId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	windowName.focus();
;}
function opentextwin(msgText) {
	  windowName = window.open("viewtext.jsp?msgtext=" + msgText,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}


function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}

function openwinsnap() {
    windowName=window.open("snapshottext.htm","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

/*function waitfor() {
	if (document.all) {
		document.all("plwait").style.visibility="hidden";
	}
	else
	{
		if (navigator.appVersion.substring(0,1) == '5') {
			if (document.getElementById("plwait")) {
				document.getElementById("plwait").style.visibility="hidden";
			}
		}
		else if (window.document.vlogo.document.test) {
			window.document.vlogo.document.test.visibility="hidden";

		}
	}
//	document.all("userNameNew").style.visibility="visible";
	}*/


function setfocus(){
	if (document.forms[0].protocolManagementRightInt == null) { return; }

	protocolManagementRightInt = parseInt(document.forms[0].protocolManagementRightInt.value);

	if (protocolManagementRightInt > 0)
	{
		document.forms[0].searchCriteria.focus()
	}
}
</SCRIPT>


<BODY onLoad="setfocus()" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" style="overflow:hide">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request"	class="com.velos.esch.web.eventassoc.EventAssocJB" />

<%@ page language = "java" import = "com.velos.eres.web.user.UserJB,com.velos.esch.business.common.*"%>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


<jsp:useBean id="msgcntr" scope="session" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>

<jsp:useBean id="sv" scope="request" class="com.velos.eres.web.studyView.StudyViewJB"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
	<jsp:useBean id="studymod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.MsgCntrDao, com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>



<DIV class="browserDefault" id="div1">

	<%HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

	//request.setAttribute("sonia","sahni");
	TimeZone tz = TimeZone.getDefault();

	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
   	if (agent1 != null && agent1.indexOf("MSIE") != -1)
    	ienet = 0; //IE
    else
		ienet = 1;
		String accId = (String) tSession.getValue("accountId");
		UserJB user = (UserJB) tSession.getValue("currentUser");

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);

		int usrId=0;
		int counter = 0;

		usrId = user.getUserId();
		String uName = (String) tSession.getValue("userName");
		%>
		<%
		//grpRights.setId(Integer.parseInt(user.getUserGrpDefault()));
	   //grpRights.getGrpRightsDetails();

	   	int userProtocol  = 0;
		GrpRightsJB groupRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
    	userProtocol = Integer.parseInt(groupRights.getFtrRightsByValue("NPROTOCOL"));

		%><Form action="studybrowserpg.jsp" method="POST">
<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">

<td width="100%">
<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">
<tr height="35" class="searchBg">
	<Input type=hidden  name="protocolManagementRightInt" value="<%=protocolManagementRightInt%>" >

	<% if (protocolManagementRightInt > 0) { %>
		<td class="labelFont" width="10%">
		<%=LC.L_Search_AStd%><%--Search a Study*****--%></td>
 		<td width="18%" align="left"><P><Input class="txtInput" type=text name="searchCriteria" size="25"></P></td>


		<td width="15%" align="left"><P><button type="submit" onmouseover="return overlib('<%=MC.M_EntPartial_CompleteText%><%--Enter partial or complete text to search across <%=LC.Std_Study_Lower%> number, <%=LC.Std_Study_Lower%> title and keyword fields*****--%>',CAPTION,'<%=LC.L_StdSrch_Help%><%--<%=LC.Std_Study%> Search Help*****--%>');" onmouseout="return nd();"><%=LC.L_Search%></button></P></td>

		<td class="strongFont" width="20%" align="left"><A href="advStudysearchpg.jsp"><%=LC.L_Adva_Search%><%--ADVANCED SEARCH*****--%></A></td>

		<% }  %>

<% //check the rights
	 				// To check for the account level rights. Check the Forms module right in account
						String modRight = (String) tSession.getValue("modRight");
						 int formLibSeq = 0;
						 acmod.getControlValues("module");
						 ArrayList acmodfeature =  acmod.getCValue();
						 ArrayList acmodftrSeq = acmod.getCSeq();
						 char formLibAppRight = '0';
						 formLibSeq = acmodfeature.indexOf("MODFORMS");
						 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
						 formLibAppRight = modRight.charAt(formLibSeq - 1);


					 	if (String.valueOf(formLibAppRight).compareTo("1") == 0)
						{  %>

	<td width="30%" align="right">
  <%=LC.L_Acc_Forms%><%--Account Forms*****--%>&nbsp;&nbsp;&nbsp;</td>
	<td class="rhsFont" nowrap="nowrap" width="20%" align="right"><a href="formfilledaccountbrowser.jsp?srcmenu=<%=src%>"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_Edit%><%--Edit*****--%></a></td>

					   <%}%>
		


 		</tr></table>

<br>


<% if (protocolManagementRightInt > 0)
{ %>
			<%
			 studymod.getControlValues("study_rights","STUDYMPAT");
			 ArrayList aRightSeq = studymod.getCSeq();
			 String rightSeq = aRightSeq.get(0).toString();
			 int iRightSeq = EJBUtil.stringToNum(rightSeq);
			Configuration conf = new Configuration();

			String editstudyCount= conf.getEditStudyCount(conf.ERES_HOME +"eresearch.xml");
			String viewstudyCount= conf.getViewStudyCount(conf.ERES_HOME +"eresearch.xml");
			StudyDao studyDao = studyB.getStudyValuesForUsers(String.valueOf(usrId),editstudyCount);
			ArrayList aStudyIds 		   =	studyDao.getStudyIds();
			ArrayList aStudyTitles 		   = 	studyDao.getStudyTitles();
			ArrayList aStudyNumbers 	   = 	studyDao.getStudyNumbers();
			ArrayList aStudyStatuses 	   = 	studyDao.getStudyStatuses();
			ArrayList aStudySubtypes 	   = 	studyDao.getStudySubtypes();
			ArrayList aStudyStatNotes 	   = 	studyDao.getStudyStatNotes();
			ArrayList aStudyStatDates 	   = 	studyDao.getStudyStatDates();
			ArrayList aStudyTeamRights     =    studyDao.getStudyTeamRights();
			ArrayList aStudyActBeginDates  =    studyDao.getStudyActBeginDates();
			//JM: 01.25.2006
			ArrayList aStudySites          =    studyDao.getStudySites();




	   		 int aStudyId = 0;
	   		 int aStudyStatId = 0;
	  		 String aStudyTitle = "";
	 		 String aStudyNumber = "";
			 String aStudyStatus = "";
			 String aStudySubtype = "";
			 String aStudyStatNote = "";
			 String aStudyStatDate = "";
			 String aStudyTeamRight = "";
			 String aStudyActBeginDate = "";

			 String aStudySite = "";

     	   	int aLen = aStudyIds.size();
		   	int aCounter = 0;



			%>

<tr>


<table width="100%"><tr>
     <td class="labelFont" width="50%">

		<%=LC.L_Last_ModifiedStudies%><%--Last Modified <%=LC.Std_Studies%>*****--%>


		</td>
		<%
		if (userProtocol == 4 || userProtocol == 6 || userProtocol == 7 || userProtocol == 5){
		%>
		<td class="rhsFont strongFont" width="50%" align="right">
		<A href="studybrowserpg.jsp"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_View_AllStudies%>"></A>
		</td></tr></table>
		<%
		}
		%>

			<tr>

<table class="outline" width="100%"><tr>

		<th width="9%"><%=LC.L_QuickAccess %><%--Quick Access*****--%></th>
        <th width="24%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> </th>
        <th width="25%"> <%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> </th>
	    <th width="14%" > <%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%> </th>

     </tr>
     <tr>


	<!--KM-->
	<script language=javascript>
	   var varViewTitle =  new Array (<%=aLen%>);
	</script>

   <%String mPatRight ="";
    for(aCounter = 0;aCounter<aLen;aCounter++)

	{
		aStudyId = EJBUtil.stringToNum(((aStudyIds.get(aCounter)) == null)?"-":(aStudyIds.get(aCounter)).toString());
		aStudyTitle = ((aStudyTitles.get(aCounter)) == null)?"-":(aStudyTitles.get(aCounter)).toString();
		aStudyNumber = ((aStudyNumbers.get(aCounter)) == null)?"-":(aStudyNumbers.get(aCounter)).toString();
	    aStudyStatus = ((aStudyStatuses.get(aCounter)) == null)?"-":(aStudyStatuses.get(aCounter)).toString();
	    aStudySubtype = ((aStudySubtypes.get(aCounter)) == null)?"-":(aStudySubtypes.get(aCounter)).toString();
	    aStudyStatNote = ((aStudyStatNotes.get(aCounter)) == null)?"-":(aStudyStatNotes.get(aCounter)).toString();
	    aStudyStatDate = ((aStudyStatDates.get(aCounter)) == null)?"-":(aStudyStatDates.get(aCounter)).toString();
		aStudyTeamRight = ((aStudyTeamRights.get(aCounter)) == null)?"-":(aStudyTeamRights.get(aCounter)).toString();
		aStudyActBeginDate = ((aStudyActBeginDates.get(aCounter)) == null)?"-":(aStudyActBeginDates.get(aCounter)).toString();
 		//JM:
		aStudySite = ((aStudySites.get(aCounter)) == null)?"-":(aStudySites.get(aCounter)).toString();


		//Commented by Manimaran for full title display with mouseover.
		/*if (aStudyTitle.length() > 80)
				{
					aStudyTitle = aStudyTitle.substring(0,80) + "...";
				}
		*/

		//GM 03/14/08 to fix CL7364 . This is to render studytitle which contains special characters like ' < ' and ' > '.
	    //NOTE : we are also aware that this would create problems to render korean characters. We need to find a resolution which fixes BOTH the problems

	    /*if(aStudyTitle!=null){
	    	aStudyTitle = StringUtil.htmlEncode(aStudyTitle);
		}*/
		//aStudyTitle = StringUtil.htmlEncode(aStudyTitle);

		if (aStudyTeamRight.length() >= 11 )
				{
					mPatRight = aStudyTeamRight.substring(iRightSeq - 1, iRightSeq);
				}
		else
				{
					mPatRight = "0";
				}

if((aCounter==0) || (aCounter>0 && aStudyId != EJBUtil.stringToNum((aStudyIds.get(aCounter-1)).toString()))){

		if ((aCounter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}
// If Check used for old data . As it may have a study with statuses having same studystat_date(with no time only date).
  if (userProtocol == 4 || userProtocol == 6 || userProtocol == 7 || userProtocol == 5){
  	aStudyStatNote = aStudyStatNote.replace('\n',' ');
	aStudyStatNote = aStudyStatNote.replace('\r',' ');
	//km-to fix the Bug 2480.
	aStudyStatNote=StringUtil.escapeSpecialCharJS(aStudyStatNote);
  %>
	<td align="center">
	<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"><tr>
	<td width="25%" style="border: 0px"><a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=aStudyId%>"><img src="../images/jpg/study.gif" title="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
	</td>
	<td width="25%" style="border: 0px">
	<%if (!aStudyActBeginDate.equals("-") && EJBUtil.stringToNum(mPatRight) > 0){%>
			<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=aStudyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" title="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
	<%}%></td>
	<td  width="25%" style="border: 0px"><a href="budgetbrowserpg.jsp?srcmenu=tdmenubaritem6&studyId=<%=aStudyId%>&calledFrom=study"><img src="../images/jpg/Budget.gif" title="<%=LC.L_Budgets%><%--Budgets*****--%>" border=0 ></a>
	</td>
	<td  width="25%" style="border: 0px">
		<%
		if (eventassocB.getStudyProts(aStudyId)!=null && eventassocB.getStudyProts(aStudyId).getEvent_ids().size() > 0){
		%>
		<a href="studyprotocols.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=7&studyId=<%=aStudyId%>"><img src="../images/jpg/Calendar.gif" title="<%=LC.L_Calendars%><%--Calendars*****--%>" border=0 ></a>
		<%
		}
		%>
	</td>
	</tr></table>
	</td>
		<!--Start for S-22306 Enhancement : Raviesh -->
        <td><%if(aStudyNumber.length()>50){%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td style="border-color:; border-width: 0;"><%=aStudyNumber.substring(0,50)%>&nbsp;<IMG src ='./images/More.png' class='asIsImage' border=0 onMouseOver="return overlib('<%=aStudyNumber%>',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.L_Study_Number%> Study Number*****--%>');" onMouseOut="return nd();">&nbsp;</IMG></td>
		</tr></table>
		<%} else {%>
		&nbsp;&nbsp;<%=aStudyNumber%>
		<%}%></td>

		<%
		//Modified by Manimaran to display full title in mouseover.
		String viewStudyTitle = StringUtil.escapeSpecialCharJS(aStudyTitle);
		%>

		<script language=javascript>
		   varViewTitle[<%=aCounter%>] = '<%=StringUtil.htmlEncode(viewStudyTitle)%>';
		</script>

		<td><%if(aStudyTitle.length()>50){%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td style="border-color:; border-width: 0;"><%=StringUtil.htmlEncode(aStudyTitle.substring(0,40))%>&nbsp;<IMG src ='./images/More.png' class='asIsImage' border=0 onMouseOver="return overlib(varViewTitle[<%=aCounter%>],CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onMouseOut="return nd();"/></td>
		</tr></table>
		<%}else{%>
		&nbsp;&nbsp;<%=StringUtil.htmlEncode(aStudyTitle)%>
		<%}%></td>
		<!--End for S-22306 Enhancement : Raviesh -->
		 
		<td> <%

		//JM: 01.25.2006 Modified: overlib() passing parameters to have organization name in the tooltip
		if (aStudySubtype.equals("temp_cls")) { %>
		<img src="../images/help.png" border="0"/> 
			<A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=aStudyId%>" onMouseOver="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+aStudyStatNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +aStudySite+"</font></td></tr>"%>',CAPTION,'<%=aStudyStatDate%>');" onMouseOut="return nd();"> <FONT class="Red"><%=aStudyStatus%></FONT> </A>
		<%} else if (aStudySubtype.equals("active")) {%>
			<A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=aStudyId%>" onMouseOver="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+aStudyStatNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +aStudySite+"</font></td></tr>"%>',CAPTION,'<%=aStudyStatDate%>');" onMouseOut="return nd();"> <FONT class="Green"><%=aStudyStatus%></FONT></A>
		<%} else if(aStudySubtype!=null && aStudySubtype.equals("-")){%>
			<!--Added by sudhir for s-22307 enhancement on 12-03-2012-->	
			<div align="center"><A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=aStudyId%>" onMouseOver="return overlib('<%=MC.M_Status_Cannot_Be_Displayed%>',CAPTION,'<%=LC.L_Study_Status%>');" onMouseOut="return nd();"><%//=aStudyStatDate%><img src="../images/help.png" border="0" /> </A></div>											
			<!--Added by sudhir for s-22307 enhancement on 12-03-2012-->		
		<%}else{%>
		 <A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=aStudyId%>" onMouseOver="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+aStudyStatNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +aStudySite+"</font></td></tr>"%>',CAPTION,'<%=aStudyStatDate%>');" onMouseOut="return nd();"><%=aStudyStatus%></A>
		<%} %>
	</td>
     </tr>
      <%
	  }
	}
}
%>


			<%
			if (aLen <=0 )
			{
			%>
			    <P class="blackComments"><%=MC.M_DoNotHaveAces_ToStd%><%--You do not have access to any <%=LC.Std_Study%>*****--%> </P>
		    <%
			}
			%>

<!--		    <P class="defComments"><i>Study Protocols with View access:</i></p> -->
			<%
			java.util.ArrayList svPks;
			java.util.ArrayList svStudyNums;
			java.util.ArrayList svStudyTitles;
			StudyViewDao svd = new StudyViewDao();
			StringBuffer svlist = new StringBuffer();

			svd = sv.getStudyViewByUserId(usrId,EJBUtil.stringToNum(viewstudyCount));
			svPks = svd.getSvPk();
			svStudyNums = svd.getSvStudynum();
			svStudyTitles = svd.getSvStudyTitle();
			int svlen = 0;
			svlen  = svPks.size();
			svlist.append("<Table cellspacing='1' cellpadding='0' border='0' width = '94%'><tr><th width='22%'>"+ LC.L_Study_Number/*Study Number*****/+" </th><th width='51%'>"+LC.L_Study_Title/*Study Title*****/+" </th></tr>");
			String svmodTitle = null;
			for(counter = 0;counter<svlen;counter++)
			{
				//display only first 80 char of the title
				svmodTitle = (String) svStudyTitles.get(counter);
				if (svmodTitle.length() > 80)
				{
					svmodTitle = svmodTitle.substring(0,80) + "...";
				}
				if ((counter%2)==0)
				{
					svlist.append("<tr class = 'browserEvenRow'>");
				}
				else
				{
					svlist.append("<tr class ='browserOddRow'>");
				}
				svlist.append("<td><A HREF = '#' onClick='opensvwin(" + svPks.get(counter) + ")'> [" + svStudyNums.get(counter) + "]</A><br></td><td>"+ svmodTitle +"");
				svlist.append("</td></tr>");
			}
			svlist.append("</Table>");
/*			out.print(svlist.toString());	*/
			if (svlen <=0 )
			{
			%>
<!--			    <P class="blackComments">You do not have View access to any Study </P> -->


<%
			}
			%>


</table>
</td>
      </tr>
</table>


<%
 } //check for protocol management right
%>




  		<%HttpSession thisSession = request.getSession(true); %>
  		<%
		//thisSession.putValue("GRights",grpRights);
		%>
  		<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
  		<%
   		UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByUserId(usrId);
   		ArrayList lnksIds = usrLinkDao.getLnksIds();
   		ArrayList lnksUris = usrLinkDao.getLnksUris();
   		ArrayList lnksDescs = usrLinkDao.getLnksDescs();
   		ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
   		String lnkUri = null;
		String lnkDesc = null;
   		String oldGrp = null;
   		String lnkGrp = null;
   		int len = lnksIds.size();
   		//int counter = 0;
		%>

		<!-- </div> -->

</td></tr></table>

<table width="100%" border="0">
<tr height="10"><td colspan="2"></td></tr>
<tr valign="top">

<td width="50%" align="left" >
    		<Form name="usrlnkbrowser" method="post" action="" onSubmit="">
      			<table class="outline" width="100%" cellspacing="0" cellpadding="0"  height="130">
			<tr><th class="labelFont" width="30%" align="left" height="20"><%=LC.L_My_Links%><%--My Links*****--%></th>
			<th width="70%" class="rhsFont" nowrap="nowrap" align="right"><A href="ulinkBrowser.jsp?user=<%=usrId%>&srcmenu=tdmenubaritem2&selectedTab=2"><img class="headerImage" title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/><%//=LC.L_Edit%><%--Edit*****--%></A></th>
			</tr>
			<%if(len==0){%>
				 <tr class="browserEvenRow"><td></td><td ></td></tr>
			<%} %>
        			<%
				for(counter = 0;counter<len;counter++)
				{
					lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
					lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
					lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
				
				 if (counter%2==0){%>
					        <tr class="browserEvenRow">
				        <%}else{
				        	%>
					        <tr class="browserOddRow">
				     
			        <%} if ( ! lnkGrp.equals(oldGrp)){ %>
						    <td class="lhsFont" width="35%"> <%= lnkGrp %> <br>
				     <%}else{%>
						    <td class="lhsFont" width="65%">&nbsp;<br>
				     <%}%>

				         </td><td class="lhsFont"><A href="<%= lnkUri%>" target="_new"><%= lnkDesc%></A> </td>
			        </tr>
				    <%
					oldGrp = lnkGrp;
		 		}
				%>
				
				</table>
			</Form>
			<%
			usrLinkDao = ulinkB.getULinkValuesByAccountId(EJBUtil.stringToNum(accId),"lnk_gen");
		   	lnksIds = usrLinkDao.getLnksIds();
			lnksUris = usrLinkDao.getLnksUris();
			lnksDescs = usrLinkDao.getLnksDescs();
			lnksGrps = usrLinkDao.getLnksGrpNames();
			len = lnksIds.size();
			counter = 0;
			%>
			</td>

<td width="50%" align="right"  class="plainbg">

			    <Form name="acclnkbrowser" method="post" action="" onSubmit="">
		    		<table class="outline"  width="99%" cellspacing="0" cellpadding="0" height="130">
			        <tr>
			<th class="labelFont" width="30%" align="left" height="20"><%=LC.L_Quick_Links%><%--Quick Links*****--%></th>
			<th width="70%" class="rhsFont" nowrap="nowrap" align="right"><A href="accountlinkbrowser.jsp?srcmenu=tdmenubaritem2&selectedTab=6"><img class="headerImage" title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/><%//=LC.L_Edit%><%--Edit*****--%></A></th>
			</tr>
			<%if(len==0){%>
				 <tr class="browserEvenRow"><td></td><td ></td></tr>
			<%} %>
			      <%
			        oldGrp = null;
				    for(counter = 0;counter<len;counter++)
					{
						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
				        if (counter%2==0){%>
					        <tr class="browserEvenRow">
				        <%}else{
				        	%>
					        <tr class="browserOddRow">
				        <%
				        } if ( ! lnkGrp.equals(oldGrp)){ %>
						        <td class="lhsFont quickLinks_border" width="35%"> <%= lnkGrp %> <br>
						<%}else{%>
						        <td class="lhsFont quickLinks_border" width="70%">&nbsp;<br>
						<%}%>
					         </td><td class="lhsFont quickLinks_border"><A href="<%= lnkUri%>" target="_new"><%= lnkDesc%></A></td>
				        </tr>
				        <%
						oldGrp = lnkGrp;
			 		}
				
				%>
				   </table>
					</Form></td></tr>
<tr><td >&nbsp;</td></tr>
</td></tr></table>


<table width="100%">

<tr>
<td valign="top">


<% java.util.ArrayList ids=msgcntr.getMsgIds(); %>
		    <% String userName="";
			String studyName = "";
		   	String studyId="";
		   	String userX="";
	   		String reqType="";
		    String permission="";
		    String msgcntrId="";
		    String pageStatus=request.getParameter("type");
		    String perGranted="";
		    String msgText="";
		    String subText="";
		    String subUser="";
		    String msgRead="";
		    String msgUnRead="";
		    String msgAcc="";
		    int pkStudyView = 0;
			String svStr = "";
			String msgStudyView = "";
		    if (pageStatus == null) pageStatus = "U";
			%>
			<Input type="hidden" name="type" value="U">
	    	<%MsgCntrDao ctrldao = new MsgCntrDao();
		   	ctrldao= msgcntr.getMsgCount(usrId);
			msgRead=ctrldao.getMsgRead();
			msgUnRead=ctrldao.getMsgUnRead();
			msgAcc=ctrldao.getMsgAcc();
	   		ctrldao= msgcntr.getMsgCntrVelosUser(usrId,pageStatus);
	   		if (msgUnRead == null)
		    {
				msgUnRead = "0";
			}
			if (msgRead == null) {
				msgRead = "0";
			}
		    if (msgAcc == null) {
				msgAcc = "0";
			}
			%>



		  	<Form Name="msgRights" method="post" action="updatemsgcntr.jsp">
				<table  width="100%" cellspacing='0' cellpadding='0' border='0'>



<tr><td  class="labelFont" colspan="6">
					<%=LC.L_Msg_Center%><%--Message Center*****--%>
&nbsp;<a href="myHome.jsp?type=U&srcmenu=<%=src%>"> <%Object[] arguments = {msgUnRead}; %><%=VelosResourceBundle.getLabelString("L_Unrd",arguments)%><%--Unread(<%=msgUnRead%>)*****--%></A>
&nbsp; <a href="myHome.jsp?type=R&srcmenu=<%=src%>"> <%Object[] arguments1 = {msgRead}; %><%=VelosResourceBundle.getLabelString("L_Rd",arguments1)%><%--Read(<%=msgRead%>)*****--%></A>
&nbsp; <a href="myHome.jsp?type=A&srcmenu=<%=src%>"> <%Object[] arguments2 = {msgAcc}; %><%=VelosResourceBundle.getLabelString("L_Ack",arguments2)%><%--Acknowledgements(<%=msgAcc%>)*****--%></A> &nbsp;&nbsp;

				<% ArrayList texts=ctrldao.getMsgcntrTexts() ; %>
		      	<% ArrayList username=ctrldao.getUserFroms() ; %>
		      	<% ArrayList studyname=ctrldao.getStudyNames() ; %>
		      	<% ArrayList reqtype=ctrldao.getMsgcntrReqTypes() ; %>
		      	<% ArrayList studyid=ctrldao.getMsgcntrStudyIds(); %>
		      	<% ArrayList userx=ctrldao.getMsgcntrFromUserIds(); %>
		      	<% ArrayList msgcntrid=ctrldao.getMsgcntrIds(); %>
		      	<% ArrayList Granted=ctrldao.getPermissions();
				  ArrayList PKStudyView=ctrldao.getMsgStudyView();
				   StudyViewDao svdMain = new StudyViewDao();
				   String study_Snapshot = "";
				   study_Snapshot = MC.M_SnapShotBcastUpdateStd;
				 %>

			    <font size = 1 >  [
		      	<% if(pageStatus.equals("R"))
				{%>
					<%=LC.L_Read_Messages%><%--Read Messages*****--%>
	 	       	<%}
				if(pageStatus.equals("U"))
				{%>
					<%=LC.L_Unread_Msgs%><%--Unread Messages*****--%>

		      <%}
				if(pageStatus.equals("A"))
				{%>
			    	<%=LC.L_Acknowledgements%><%--Acknowledgements*****--%>
		      <%}%>
				]</font>

	 </td>
	 		<%
				if(pageStatus.equals("U"))
				{%>
				<td colspan="6" align=right>
				<P class="defComments_txt">
				<%=MC.M_What_IsSnapshot%><%--What is a Snapshot?*****--%>&nbsp; 
				<A href="#"><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(study_Snapshot)%>',CAPTION,'<%=MC.M_What_IsSnapshot%>',LEFT, ABOVE);" onmouseout="return nd();" alt=""></img></A>
				&nbsp;&nbsp;&nbsp; <%=LC.L_Click_On%><%--Click on*****--%> <IMG src ='../images/jpg/snapshot.jpg' border=0> <%=MC.M_ToView_CurrStd%><%--to view the current <%=LC.Std_Study_Lower%> snapshot.*****--%>
				</P></td>
				<% } %>
	 </tr>
			<tr>





<table width="100%"  class="outline">
<tr>


		        	<th width="17%"> <%=LC.L_Name%><%--Name*****--%> </th>
		          	<th width="16%"> <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> </th>
		          	<th width="16%"> <%=LC.L_Text%><%--Text*****--%> </th>
		          	<th width="16%"> <%=LC.L_Request%><%--Request*****--%></th>
		          	<th width="17%"> <%=LC.L_Permission%><%--Permission*****--%></th>
		          	<th width="18%"> <%=LC.L_Snapshot%><%--Snapshot*****--%></th>
		         </tr>



				<input name=count type=hidden value=<%=texts.size()%>>
		        <input name=srcmenu type=hidden value="tdMenuBarItem1">
		        <%
				for(int i=0; i< texts.size(); i++)
				{
					reqType=((reqtype.get(i)) == null)?"-":(reqtype.get(i)).toString();
					reqType=reqType.trim();
					if(reqType.equalsIgnoreCase("v")){
						permission=LC.L_View/*View*****/;
					}
					if(reqType.equalsIgnoreCase("m")){
						permission=LC.L_Modified/*Modify*****/;
				    }
					userName= ((username.get(i)) == null)?"-":(username.get(i)).toString();
					subUser = userName;
					studyName=(((studyname.get(i)) == null)?"-":(studyname.get(i)).toString());
					perGranted=((Granted.get(i)) == null)?"-":(Granted.get(i)).toString();
					studyId=((studyid.get(i)) == null)?"-":(studyid.get(i)).toString();
					userX=((userx.get(i)) == null)?"-":(userx.get(i)).toString();
					msgText=((texts.get(i)) == null)?"-":(texts.get(i)).toString();
					subText = msgText;
					msgcntrId=((msgcntrid.get(i)) == null)?"-":(msgcntrid.get(i)).toString();

					if(pageStatus.equals("U"))
					{
						pkStudyView =  svdMain.getStudyViewIdByStudy(EJBUtil.stringToNum(studyId));
						svStr = "<td><A HREF = '#' onClick='opensvwin(" + pkStudyView  + ")'><IMG src ='../images/jpg/snapshot.jpg' border=0></A> &nbsp;&nbsp; <A href=updatesnapshot.jsp?srcmenu=tdmenubaritem3&calledfrom=H&studyId=" +studyId +">"+LC.L_Update/*Update*****/+"</A></td>";
					}
					else if(pageStatus.equals("R"))
					{
						msgStudyView =  (String) PKStudyView.get(i);

						if (msgStudyView != null && msgStudyView.trim().length() > 0)
						{
						svStr = "<td><A HREF = '#' onClick='opensvwin(" + msgStudyView  + ")'><IMG src ='../images/jpg/snapshot.jpg' border=0></A></td>";
						}
						else
						{
						svStr = "<td>" +msgStudyView+ "</td>";
						}
					}
					else
					{
						svStr = "";
					}


					%>
			       	<input	type= hidden value="<%=msgcntrId%>" name=id<%=i%> >
			       	<input	type= hidden value="<%=studyId%>" name=studyid<%=i%> >
			       	<input	type= hidden value="<%=userX%>" name=userX<%=i%> >
			       	<%	if ((i%2)==0) {
					%>
				      <tr  class="browserEvenRow">
			        <%
					}
					else{
					%>
				        <tr class="browserOddRow">
			         <%
					}
					 %>
					<td >
			        	<% if(userName.length()>10){
			    			subUser=userName.substring(0,10);
				    		subUser+="..";
			    		}
				    	%>
						<a href="#" onClick="openuserwin(<%=userX%>)">
			            <%=subUser %> </A>
					</td>
					<td > <%= studyName %>
					</td>
			        <td>
            			<% if (msgText.length() >10){
							subText=msgText.substring(0,10);
							subText+="...";
						}
						  msgText=msgText.replace(' ','~');
							%>
						<a href="#" onClick="opentextwin('<%=msgText%>')">
			            <%=subText %> </a>
			           <% msgText = msgText.replace('~',' '); %>
					</td>
			        <td align="center"> <%= permission %>
					</td>
					<% if(pageStatus.equals("U"))
					{%>
				        <td>
				            <Input type="radio" value ="view" name=right<%=i%> ><%=LC.L_View%><%--View*****--%>
				            <Input type="radio" value ="modify" name=right<%=i%>><%=LC.L_Modify%><%--Modify*****--%>
				            <Input type="radio" value="deny" name=right<%=i%>><%=LC.L_Deny%><%--Deny*****--%>
							&nbsp;&nbsp;
						</td>
						<%=svStr%>
		          	<%}else{%>
        				<td align="center">
				            <% if(perGranted.equalsIgnoreCase("v")){
								perGranted=LC.L_View/*View*****/;
							}
							if(perGranted.equalsIgnoreCase("m")){
								perGranted=LC.L_Modify/*Modify*****/;
				      		}
				      		if(perGranted.equalsIgnoreCase("d")){
								perGranted=LC.L_Deny/*Deny*****/;
				      		}		%>
			 	            <%= perGranted %>
						</td>
						<% if(pageStatus.equals("R"))
							{%>
				    			<%=svStr%>

		          		<%  }
						}
							%>
        		</tr>
			    <%
				}
				%>
		<% if(texts.size() <= 0 )
			{
			%>
		        <tr>
			        <td colspan=6 align = left>
		    	       	<%=MC.M_NoRecordsFound %>
			       </td>
		        </tr>
		        <%}%>
		        <% if(pageStatus.equals("U") && texts.size() > 0 )
				{
				%>
			        <tr>
				        <td colspan=6 align = right>
			    	        <button type="Submit"><%=LC.L_Submit%></button>
				        &nbsp;&nbsp;&nbsp;</td>
			        </tr>
		        <%}%>
		</tr></table></tr>


</table></form>


</td>
</tr>
</table>



	<%
	} //end of body for session
	else
	{
	%>
		<div id=myName></div>
		<div id=myStudies></div>
		<div id=myLinks></div>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>

	</Form>
	<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</div>


</BODY>

</HTML>