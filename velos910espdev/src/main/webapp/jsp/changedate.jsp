<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Change_ActualDate%><%--Change Actual Date*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>

<script>
function test(formobj) {

	 if (!(validate_col('Actual Date',formobj.actualDt))) return false

	 var fdaVal = document.getElementById("fdaStudy").value;
	 if (fdaVal == "1"){
		if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
	 }

 	 if (!(validate_col('e-Signature',formobj.eSign))) return false

     	if(isNaN(formobj.eSign.value) == true) {
     	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

	if(formobj.acDateFlag.value != 1) {
		if(formobj.prevDate.value==formobj.actualDt.value)
		{
		alert("<%=MC.M_Etr_DiffActualDt%>");/*alert("please enter a different actual date");*****/
		return false;
		}
	}

}

function fshow(obj, val){
/*
if(obj.actualDt.value=="null" || obj.actualDt.value=="")
{
alert("please enter the actual date first");
return false;
}


if(obj.actualDt.value==obj.actualDt.value){
alert("hello world")
}
*/
}

</script>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))	{

 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>

<Form id ="chgdtform" name="ChangeDate" action="UpdateActualDate.jsp" method="post" onSubmit="if (test(document.ChangeDate)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >

<% String prevDate =request.getParameter("prevDate");%>
<% String eventId = request.getParameter("eventId");%>
<% //String eventName = request.getParameter("eventName");

%>
<% String patProtId=request.getParameter("patProtId"); %>
<% String statDesc = request.getParameter("statDesc");%>
<% String statid = request.getParameter("statid"); %>
<% String studyVer = request.getParameter("studyVer"); %>
<% String patientCode = request.getParameter("patientCode");
	String calAssoc=request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;

	String studyId = (String) tSession.getValue("studyId");
	studyId=(studyId==null)?"":studyId;
	studyB.setId(StringUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	String calId=request.getParameter("calId");
	calId=(calId==null)?"":calId;

	String acDateFlag = request.getParameter("acDateFlag");
	//KM
	String org_id = request.getParameter("org_id");
	String calledFrom = request.getParameter("calledFrom");
	String eventName ="";



	 if (calledFrom.equals("P")){

  		  	eventdefB.setEvent_id(EJBUtil.stringToNum(org_id));
			eventdefB.getEventdefDetails();
		    eventName = eventdefB.getName();
	 }else{

		   eventassocB.setEvent_id(EJBUtil.stringToNum(org_id));
		   eventassocB.getEventAssocDetails();
		   eventName = eventassocB.getName();
	 }



	String protocolId=request.getParameter("protocol_id");
	protocolId=(protocolId==null)?"":protocolId;
%>
<%
/*out.println("prevDate is " +prevDate);
out.println("eventId is " +eventId);
out.println("patProtId is " +patProtId);
out.println("statDesc is " +statDesc);
out.println("statid is " +statid);*/


%>
<INPUT NAME="acDateFlag" TYPE=hidden  value=<%=acDateFlag%>>
<INPUT NAME="prevDate" TYPE=hidden  value=<%=prevDate%>>
<INPUT NAME="eventId" TYPE=hidden  value=<%=eventId%>>
<INPUT NAME="patProtId" TYPE=hidden  value=<%=patProtId%>>
<INPUT NAME="statDesc" TYPE=hidden  value=<%=statDesc%>>
<INPUT NAME="statid" TYPE=hidden  value=<%=statid%>>
<INPUT NAME="studyVer" TYPE=hidden  value=<%=studyVer%>>
<INPUT NAME="patientCode" TYPE=hidden  value=<%=patientCode%>>
<INPUT NAME="pkey" TYPE=hidden  value=<%=request.getParameter("pkey")%>>
<INPUT NAME="calassoc" TYPE=hidden  value=<%=calAssoc%>>
<INPUT NAME="studyId" TYPE=hidden  value=<%=studyId%>>
<INPUT NAME="calId" TYPE=hidden  value=<%=calId%>>
<INPUT NAME="protocolCalendar" TYPE=hidden  value=<%=protocolId%>>
<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">

	<BR>

	<table >
	<tr>
	<td colspan = 3>
	<P class="sectionHeadings"> <%=LC.L_Change_ActualDate%><%--Change Actual Date*****--%> </P>

	<td>
	</tr>


	<tr>
<td class=tdDefault><%Object[] arguments = {eventName};%>
<%=VelosResourceBundle.getLabelString("L_Event_Nme",arguments)%><%--Event Name: <%=eventName%>*****--%></td>
</tr>


	<tr>

	<td colspan=2 id="tdComments">


	</td>
	</tr>
	<tr>
	<td class=tdDefault><%=LC.L_Select_ActualDate%><%--Select Actual Date*****--%>:<font class=mandatory>*</font></td>
<%-- INF-20084 Datepicker-- AGodara --%>
	<td class=tdDefault><INPUT type=text name=actualDt class="datefield" size=10 READONLY  ></td>
	</tr>
	</table>
<br>
	<table>
	<tr>
	<td colspan="3"><P class = "defComments" > <%=MC.M_Confirm_ChgDt_EvtSch%><%--You are changing the date of an event scheduled for another day. Would you also like to*****--%>: </P>	</td>


    <tr>
    <td colspan = 2 class=tdDefault>

    <Input type="radio" name="statusflag" value="1"  checked="true"> <%=MC.M_MoveCurEvt_Accrd%><%--Move the current event accordingly.*****--%>
    </td>
	</tr>

    <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="radio" name="statusflag" value="2" > <%=MC.M_MoveEvtSchOnCur_VisitAccrd%><%--Move the events scheduled on current and next visit accordingly.*****--%>


    </td>
  </tr>

    <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="radio" name="statusflag" value="0" >	<%=MC.M_MoveSbqtEvts_Accrd%><%--Move All subsequent events accordingly.*****--%>
    </td>
  </tr>

  <!-- JM: 14Jul2006-->
  <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="radio" name="statusflag" value="3" >	<%=MC.M_MoveEvtsVisit_Accrd%><%--Move All events of this visit accordingly.*****--%>
    </td>
  </tr>

  <tr>
    <td colspan = 2 class=tdDefault>
    <BR><Input type="checkbox" name="synchsuggested" value="1" >	<%=MC.M_MoveDtToInSync_WithActualDt%><%--Move Suggested Date to be in sync with Actual Date*****--%>
    </td>
  </tr>
  <tr>
      <td><%=LC.L_ReasonForChangeFDA%>
      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
      </td>
	  <td><textarea id="remarks" name="remarks" rows="3" cols="30" MAXLENGTH="4000"></textarea></td>
  </tr>
  <tr>
  		<td colspan=2>
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="chgdtform"/>
				<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
		</td>
  </tr>
</table>

</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>

