<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<BODY>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventResB" scope="request" class="com.velos.esch.web.eventresource.EventResourceJB"/>
<%@ page language = "java" import = "com.velos.esch.web.eventuser.EventuserJB,com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,java.util.*"%>
<%@ page import = "com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*"%>
<%@ page import = "com.velos.esch.audit.web.AuditRowEschJB"%>
<!--KM- DateUtil from esch is Obsolete -->
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventStatB" scope="request" class="com.velos.esch.web.eventstat.EventStatJB"/>


<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String eSign = request.getParameter("eSign");

	String oldESign = (String) tSession.getValue("eSign");
	String ipAdd = (String) tSession.getValue("ipAdd");
    String usr = (String) tSession.getValue("userId");

	if(!(oldESign.equals(eSign)))  {
	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {
	String startdt= request.getParameter("actualDt");
	Date st_date1 = DateUtil.stringToDate(startdt,"");
	java.sql.Date protEndDate = DateUtil.dateToSqlDate(st_date1);
	String usrId =request.getParameter("userId");
	String eventId = request.getParameter("eventId");
	String pkey = request.getParameter("pkey");
	String statDesc=request.getParameter("statDesc");
	String notes = request.getParameter("notes");
	String statid= request.getParameter("eventstatus");

	String eventSOS = request.getParameter("ddSos");
	int eventSOSId =0;
	eventSOSId = EJBUtil.stringToNum(eventSOS);

	String eventCType = request.getParameter("ddCoverage");
	int eventCoverType =0;
	eventCoverType = EJBUtil.stringToNum(eventCType);


	String reasonForCoverageChange = request.getParameter("reasonForCoverageChange");
	reasonForCoverageChange = (reasonForCoverageChange==null)?"":reasonForCoverageChange;

	String reasonForCoverageChangeText = request.getParameter("reasonForCoverageChangeTxt");
	reasonForCoverageChangeText = (reasonForCoverageChangeText==null)?"":reasonForCoverageChangeText;


	String enabledDisabled = request.getParameter("attrib_text");
	enabledDisabled = (enabledDisabled==null)?"":enabledDisabled;

	if (reasonForCoverageChange.equals("") && (enabledDisabled.equals("disabled"))){

		reasonForCoverageChange = reasonForCoverageChangeText;

	}

	//JM: 02Jul2008, #3586

	String mode = request.getParameter("mode");
	if(mode.equals("M"))
	{
		statid = request.getParameter("statid");
	}

	String oldStatus=request.getParameter("statid");

	int iEventId = EJBUtil.stringToNum(eventId);
	eventdefB.MarkDone(EJBUtil.stringToNum(eventId),notes,protEndDate,EJBUtil.stringToNum(usrId),EJBUtil.stringToNum(statid),EJBUtil.stringToNum(oldStatus),ipAdd, mode, 
			eventSOSId, eventCoverType, reasonForCoverageChange);

//padding the event id as per table data ...
	String fk_eventId = request.getParameter("eventId");
	int len_evt_id = fk_eventId.length();
	int len_pad = 10 - len_evt_id ;
	String len_pad_str = "";

	for (int k=0; k < len_pad; k++ ){
		len_pad_str = len_pad_str + "0";
	}
	fk_eventId = len_pad_str + fk_eventId;


	if (mode.equals("M")){//only same status possible
		//end date of the event_stat table, you have to make it to null through a procdure
	}else {

	 	eventStatB.setEventStatDate(startdt);
	    eventStatB.setEvtStatNotes(notes);
	    eventStatB.setFkEvtStat(fk_eventId);
	    eventStatB.setEvtStatus(statid);
	    //eventStatB.setEventStatEndDate(evtStatEndDate);
	    eventStatB.setCreator(usr);
	    eventStatB.setIpAdd(ipAdd) ;
	    eventStatB.setEventStatDetails();
	}

	String remarks = request.getParameter("remarks");
	if (!StringUtil.isEmpty(remarks)){
		AuditRowEschJB schAuditJB = new AuditRowEschJB();
		schAuditJB.setReasonForChangeOfScheduleEvent(StringUtil.stringToNum(eventId), 
				StringUtil.stringToNum(usrId), remarks);
		boolean excludeCurrent = (!mode.equals("M"))?true:false;
		schAuditJB.setReasonForChangeOfScheduleEventStatus(StringUtil.stringToNum(eventId), 
					StringUtil.stringToNum(usrId), remarks, excludeCurrent);
	}

//JM: 15Feb2008

	int eventStatusId = 0;

	eventStatusId  = eventResB.getStatusIdOfTheEvent(EJBUtil.stringToNum(eventId));


	String durationStr = null;
	String selRoleId = null;

	ArrayList roleIds = new ArrayList();
	ArrayList evDuration = new ArrayList();

	String[] arDurationDD = request.getParameterValues("durationDD");
	String[] arDurationHH = request.getParameterValues("durationHH");
	String[] arDurationMM = request.getParameterValues("durationMM");
	String[] arDurationSS = request.getParameterValues("durationSS");


	String[] selTrackingIds = request.getParameterValues("trackingIds");
	String selTrackingId ="";

	int deRows = 0;

	if(selTrackingIds != null) {
	   deRows = selTrackingIds.length;
	}

	if (oldStatus.equals(statid)){

		for (int k = 0; k < deRows; k++){
		selTrackingId = selTrackingIds[k];
		//Modified for INF-18183 ::: Ankit
		eventResB.removeEventResources(EJBUtil.stringToNum(selTrackingId), AuditUtils.createArgs(session,"",LC.L_Mng_Pats));
		//eventResB.removeEventResources(EJBUtil.stringToNum(eventId));

		}
	}

	String[] selRole = request.getParameterValues("selRole");

	String ddval = null;
	String hhval = null;
   	String mmval = null;
   	String ssval = null;

	int rows = 0;

	if(selRole != null) {
	   rows = selRole.length;
	}



   for(int i =0; i<rows; i++) {

	selRoleId = selRole[i];

	if (EJBUtil.stringToNum(selRoleId) > 0 )
	{
		ddval = arDurationDD[i];
		hhval = arDurationHH[i];
		mmval = arDurationMM[i];
		ssval = arDurationSS[i];

		if ( ddval.trim().length() == 0 ) ddval = "00";
		if ( hhval.trim().length() == 0 ) hhval = "00";
		if ( mmval.trim().length() == 0 ) mmval = "00";
		if ( ssval.trim().length() == 0 ) ssval = "00";

	    durationStr = ((ddval.trim().length() == 1) ?  "0" + ddval : ddval) + ":" +((hhval.trim().length() == 1) ?  "0" + hhval : hhval) + ":" + ((mmval.trim().length() == 1) ?  "0" + mmval : mmval) + ":" + ((ssval.trim().length() == 1) ?  "0" + ssval : ssval);

		roleIds.add(selRoleId);
		evDuration.add(durationStr);
	}
   }
    eventResB.setEvtStat("" + eventStatusId);
	eventResB.setEvtRoles(roleIds);
	eventResB.setEvtTrackDurations(evDuration);
    eventResB.setCreator(usr);
    eventResB.setIpAdd(ipAdd);
	eventResB.setEventResourceDetails();

//JM: 15Feb2008, added above...
%>

<br><br><br><br>
<p class="successfulmsg" align=center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>

<script>
	window.opener.location.reload();
	setTimeout("self.close()",1000);
</script>


<%}//end of if for eSign
} //end of if for session
else{%>
<jsp:include page="timeout.html" flush="true"/>
<%
}%>

</BODY>
</HTML>
