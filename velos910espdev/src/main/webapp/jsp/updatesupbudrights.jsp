<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<BODY>
<jsp:useBean id="grp" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
int ret = 0;
String budgetId,rights[],cnt,rght="";
String budgetScope="";
int totrows =0,count=0; 

String src;
src=request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");


totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);
  
if (sessionmaint.isValidSession(tSession))  {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%	   
   	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
        String groupId=request.getParameter("groupId");
	int grpId=EJBUtil.stringToNum(groupId);
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

if (totrows > 1){
	rights = request.getParameterValues("rights");

	for (count=0;count<totrows;count++){
		rght = rght + String.valueOf(rights[count]);
	}
}else{

	rght = request.getParameter("rights");
}

grp.setGroupId(grpId);
grp.getGroupDetails();

grp.setGroupSupBudRights(rght);

grp.setModifiedBy(usr);
grp.setIpAdd(ipAdd);

ret =grp.updateGroup();

%>
<br>
<br>
<br>
<br>
<br>
<% if (ret >= 0 ) {%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupbrowserpg.jsp?srcmenu=<%=src%>">
<% 
	} else {
%>
<p class = "successfulmsg" align = center> MC.M_Data_NotSaved<%-- Data not saved*****--%> </p>	
<%
	}
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>






