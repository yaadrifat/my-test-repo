<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>

<TITLE>Appendix Browser</TITLE>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>


<jsp:include page="panel.jsp" flush="true"></jsp:include> 

<body>

<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>

<DIV class="browserDefault" id="div1">


<%


	AppendixDao appendixDao =new AppendixDao();
	
	appendixDao=appendixB.getByStudyIdAndType(21, "url");
	
	ArrayList appendixIds = appendixDao.getAppendixIds(); 
	ArrayList appendixDescriptions = appendixDao.getAppendixDescriptions();
	ArrayList appendixTypes = appendixDao.getAppendixTypes();
	ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();
	ArrayList appendixFiles = appendixDao.getAppendixFiles();
	ArrayList appendixStudyIds = appendixDao.getAppendixStudyIds();
	ArrayList appendixPubFlags =appendixDao.getAppendixPubFlags();

	String appendixId=null;
	String appendixDescription=null;
	String appendixType=null;
	String appendixFile_Uri = null;
	String appendixFile = null;
	String appendixStudy = null;
	String appendixPubFlag = null;	
	
	int len = appendixIds.size();
out.println(len);
   	int counter = 0;

	Object temp;
%>

<br>



<Form name="apendixbrowser" method="post" action="" onsubmit="">

<Input type="hidden" name="selectedTab" value="5">

<table width="100%" >
 <tr >
	<td width = "100%">    

	</td>
 </tr>

	<tr>
		<th width="100%" align="left">
			My Links
	   	</th>
   	</tr>
	<tr height=5>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
		<P class = "defComments">
			You can list your important links. Give full path of your URLs
		</P>			
		</td>
	</tr>	
	<tr>
		<td>
			<P class = "defComments">
			<A href="appendix_url.jsp">Click Here</A> to add a new Link
			</P>
		</td>
	</tr>

	<tr>
		<td>
			<P class = "defComments">
			Select [Edit] against the name of a link to edit its details. Click on the name of the link to open it in a new browser window
			</P>
		</td>
	</tr>
	<tr height=5>
		<td>
		</td>
	</tr>
</table>
	
<table width="100%">
	<%
	for(counter = 0;counter<len;counter++)
	{	
		temp=appendixIds.get(counter);			
		if (!(temp ==null))
			appendixId=temp.toString();
		temp=appendixDescriptions.get(counter);			
		if (!(temp ==null))
			appendixDescription=temp.toString();
		temp=appendixFile_Uris.get(counter);	
		if (!(temp ==null))
			appendixFile_Uri = temp.toString();
		temp=appendixStudyIds.get(counter);
		if (!(temp ==null))	
			appendixStudy = temp.toString();
		temp=appendixPubFlags.get(counter);
		if (!(temp ==null))	
			appendixPubFlag = temp.toString(); 			 
	
		%>
	
		<tr>	
		<td width="80%">
			<UL>
			<LI>
			<A href="<%=appendixFile_Uri%>"><%=appendixDescription%></A>
			</UL>
		</td>
		<td width="20%">
			<A href="appendix_set.jsp">[Edit]</A>
		</td>
		</tr>
			<%			
		
		}
	%>

</table>

<%
	AppendixDao appendixDao1 =new AppendixDao();
	appendixDao1=appendixB.getByStudyIdAndType(21, "file");
	
	ArrayList appendixIds1 = appendixDao1.getAppendixIds(); 
	ArrayList appendixDescriptions1 = appendixDao1.getAppendixDescriptions();
	ArrayList appendixTypes1 = appendixDao1.getAppendixTypes();
	ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();
	ArrayList appendixFiles1 = appendixDao1.getAppendixFiles();
	ArrayList appendixStudyIds1 = appendixDao1.getAppendixStudyIds();
	ArrayList appendixPubFlags1 =appendixDao1.getAppendixPubFlags();

	String appendixId1=null;
	String appendixDescription1=null;
	String appendixType1=null;
	String appendixFile_Uri1 = null;
	String appendixFile1 = null;
	String appendixStudy1 = null;
	String appendixPubFlag1 = null;	
	
	int len1 = appendixIds1.size();

   	int counter1 = 0;

	Object temp1;
%>
<table width="100%" >
	<tr>
		<th width="100%">
			My Files
	   	</th>
   	</tr>
	
	<tr>
		<td>
			You can attach your important documents/forms with this protocol.
		</td>
	</tr>
	
	<tr>
		<td>
			<A href="appendix_set.jsp">Click Here</A>to add a new File
		</td>
	</tr>

	<tr>
		<td>
			Select [Delete] against the name of a file to remove it from appendix. Click on the file name to view it in a new browser window
		</td>
	</tr>

	<%
	for(counter1 = 0;counter1<len1;counter1++)
	{	
		temp1=appendixIds1.get(counter1);			
		if (!(temp1 ==null))
			appendixId1=temp1.toString();
		temp1=appendixDescriptions1.get(counter1);			
		if (!(temp1 ==null))
			appendixDescription1=temp1.toString();
		temp1=appendixFile_Uris1.get(counter1);	
		if (!(temp1 ==null))
			appendixFile_Uri1 = temp1.toString();
		temp1=appendixFiles1.get(counter1);	
		if (!(temp1 ==null))
			appendixFile1 = temp1.toString();	
		temp1=appendixStudyIds1.get(counter1);
		if (!(temp1 ==null))	
			appendixStudy1 = temp1.toString();
		temp1=appendixPubFlags1.get(counter1);
		if (!(temp1 ==null))	
			appendixPubFlag1 = temp1.toString(); 			 
	
		if ((counter1%2)==0) {
		%>
	
		<tr class="browserEvenRow">	
		<%
		}
		else{
 		 %>
		
		<tr class="browserOddRow">	
		 <%
		}
  		%>
		<td>
			<A href="http://127.0.0.1:8080/appendixweb/servlet/Download/"> <%=appendixFile_Uri1%> </A>
		</td>
		</tr>
			<%			

		
		}
	%>

</table>
</FORM>

<div>
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>

</div>

<DIV class="mainmenu" id = "emenu">
  <jsp:include page="menus.htm" flush="true"/>
  
    
</DIV>

</BODY>

</HTML>
