<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<jsp:useBean id="studySiteB" scope="page" class="com.velos.eres.web.studySite.StudySiteJB" />
<jsp:useBean id="studyRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<%@ page import="com.velos.eres.service.util.*"%>
<style type="text/css">
<!--

body{
	overflow: auto;
}


tr.searchRow{
	background-color: #dcdcdc
}
tr.searchRow ul{margin:10px 0px 0px 0px;}
tr.searchRow ul li{
	float:left;
	margin:0px 0px 0px 30px;
}


#mrn_search{

  float: top;
  width: 600px;


}


.searchTable{
  border-style:solid;
  border-width:1px;
  width: 600px;
}


#user_status{
  width: 600px;
  height: 400px;
  border-style:solid;
  border-width:1px;
  position: relative;
  overflow:auto;
  top:0;
  left:0;

}

.search_results{

  left: 3px;
  top: 0;
  position: relative;
  height: auto;
  float: top;
  visibility:visible;
  width: 475px;

}


#dialog_buttons{
  float: top;
  width: 500px;
  padding: 5px;
  position: relative;
}
#ok_button{
  float: right;
}
#cancel_button{
  float: right;
}

#orgDD{
  width: 177px;
}

p.checked {
		background: url(images/checkbox.png) center center no-repeat;
}


-->
</style>

<jsp:include page="include.jsp"/>
<jsp:include page="ui-include.jsp"/>
<script language="JavaScript">toggleNotification('off',"");</script>
<script language="JavaScript">
var SEARCH_PAT_LIST_REMOTE_METHOD = "searchPatList";
var SEARCH_PAT_LIST_LOCAL_METHOD = "searchPatInDB";
var sitesMap = new Object();

var studyId;

var studyName;

var selectedPatient;

//list of sites with the restricted flag turned on
var restrictedSites = new Array();

<%//manage patient rights is being cached here for user convience.
  //it will help determine the workflow, though checks during updates
  //remain the final call on what a user can or cannot do.%>
var hasNewPatientStudyRights = false;
var hasNewPatientGroupRights = false;

$E.addListener(window, "load", function() {
	if (studyId.indexOf(",") > -1){ //"All" studys will have a comma-seperated list of study ids.
		alert("<%=MC.M_AccelEnrl_AllStd%>");/*alert("Accelerated Enrollment cannot be used for \"All\" <%=LC.Std_Studies%>. Please select a <%=LC.Std_Study_Lower%>.");*****/
		window.close();
	}
});

function searchPressed(){
	if (isSitesEmpty()){
		return;
	}


	clearStatusText();

	var patId = document.getElementById("patId").value;
	var orgDD = document.getElementById("orgDD");

	var orgSelectedIndex = orgDD.selectedIndex;
	clearStatusText();
	if (!patId || patId.length == 0){
		appendStatusText("<%=MC.M_PlsEtr_PatID%>");/*appendStatusText("Please enter a <%=LC.Pat_Patient%> ID.");*****/
		return;
	}

	toggleNotification('on',"");
	var facilityName = orgDD.options[orgSelectedIndex].text;
	var facilityId = orgDD.options[orgSelectedIndex].id; //prmary key of the facility
	var facilityAltId = orgDD.options[orgSelectedIndex].value; //altid the id to send to an external EMPI
	try{
		var paramArray = [studyName];
		appendStatusText(getLocalizedMessageString("M_SrchStd_ForPat",paramArray));/*appendStatusText("Searching <%=LC.Std_Study%>: " + studyName + " for <%=LC.Pat_Patient_Lower%>");*****/

		<%//search for patient in study. If found, notify the user take to enrollment details page.%>
	//	var patientFromStudy= searchPatientInStudy(patId, facilityId);
	//	if (patientFromStudy){
	//		appendStatusText("Patient '" + patId + ", " + facilityName + "' Found In Study: " + studyName + ". Click to see enrollment details.");
	//		appendStatusElement(buildPatInStudyTable(patientFromStudy));
	//		return;
	//	}

	//	appendStatusText("Patient '" + patId + ", " + facilityName + "' Not In Study " + studyName + ". Checking if patient is in eResearch.");
		<%//search for patient in eresearch. If found, prompt the user to enroll in study.%>
		var patientFromDB = searchPatientInDB(patId, facilityId);
		if (patientFromDB){
			var paramArray = [patId,facilityName];
			appendStatusText(getLocalizedMessageString("M_PatFnd_Eres",paramArray));/*appendStatusText("<%=LC.Pat_Patient%> '" + patId + ", " + facilityName + "' found in eResearch.");*****/
		//	appendStatusElement(buildPatInDBTable(patientFromDB, facilityId));
		}
		else{
			var paramArray = [patId,facilityName];
			appendStatusText(getLocalizedMessageString("M_PatNtFnd_Eres",paramArray));/*appendStatusText("<%=LC.Pat_Patient%> '" + patId + ", " + facilityName + "' not found in eResearch.");*****/
			<%//if facility is a site restricted site, prompt user to search again, or add the patient manually%>
			if (restrictedSites.indexOf(facilityId) > -1){

				<%	//if hasNewPatientStudyRights, user can add a patient, so can
					//do an EMPI search%>
				if (hasNewPatientGroupRights){
					var addPatient = promptUserToAddPatient();
					if (addPatient){
						addNewPatient(patId, facilityId);
					}
					else{
						appendStatusText("<%=MC.M_Run_SearchAgain%>");/*appendStatusText("Please run search again.");*****/
						return;
					}
				}
				else{
					appendStatusText("<%=MC.M_NoGrpRgtTo_AddPat%>");/*appendStatusText("You do not have Group rights to add <%=LC.Pat_Patients_Lower%>.");*****/
				}
			}
			else {
				if (hasNewPatientGroupRights){
					appendStatusText("<%=LC.L_Checking_Empi%>...");/*appendStatusText("Checking EMPI...");*****/
					<%//search for patient in remote system. If found, switch main window to add patient, and enroll.%>
					var patientFromEMPI = fetchRemotePatList(patId, facilityId, facilityAltId);
					if (!patientFromEMPI){
						appendStatusText("<%=MC.M_PatNotEmpi_ChkPatID%>");/*appendStatusText("<%=LC.Pat_Patient%> not found in EMPI. Please double check that you have entered the correct <%=LC.Pat_Patient%> ID.");*****/
					}
				}
				else{
					appendStatusText("<%=MC.M_YouNot_HaveRightsPat%>");/*appendStatusText("You do not have rights to add <%=LC.Pat_Patients_Lower%>.");*****/
				}
			}
		}

	}
	 catch(e){

	 }
	finally{
		toggleNotification('off',"");
	}


}

function promptUserToAddPatient(){
	var addPatient = confirm("<%=MC.M_PatNotFound_AddNew%>")/*confirm("<%=LC.Pat_Patient%> not found. Add new <%=LC.Pat_Patient_Lower%>?")*****/;
	return addPatient;

}
/*
function searchPatientInStudy(patId, facilityId){
	var options = "&method=searchPatInStudy&" + "patId=" + encodeURIComponent(patId) + "&facility=" + encodeURIComponent(facilityId) + "&studyId=" + studyId;
	var selectPatFromListFunction =  function (oArgs){
		var loadStr;
		var oRecord = this.getRecord(oArgs.target);
		var patient = oRecord.getData();
		var patProtId = patient.patProtMap[studyId];
		loadStr = "enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=M&pkey=" + encodeURIComponent(patient.pk_per) + "&patProtId=" + encodeURIComponent(patProtId) + "&patientCode=" + encodeURIComponent(patient.patId) + "&page=patientEnroll&studyId=" + encodeURIComponent(studyId) + "&statDesc=null&studyVer=";
		window.opener.location.href = loadStr;
		window.close();
	}

	return buildGenericPatTable(options, selectPatFromListFunction, "View");

}
	*/
function searchPatientInDB(patId, facilityId){
	var options = "&method=" + SEARCH_PAT_LIST_LOCAL_METHOD + "&" + "patId=" + encodeURIComponent(patId) + "&facility=" + encodeURIComponent(facilityId) + "&studyId=" + studyId;
	var selectPatFromListFunction =  function (oArgs){
		var loadStr;
		var oRecord = this.getRecord(oArgs.target);
		var patient = oRecord.getData();

		if (patient.onStudy){
			var patProtId = patient.patProtMap[studyId];
			loadStr = "enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=M&pkey=" + encodeURIComponent(patient.pk_per) + "&patProtId=" + encodeURIComponent(patProtId) + "&patientCode=" + encodeURIComponent(patient.patId) + "&page=patientEnroll&studyId=" + encodeURIComponent(studyId) + "&statDesc=null&studyVer=";
		}
		else{
			if (!hasNewPatientStudyRights){
				alert("<%=MC.M_YouNotPat_RgtStd%>");/*alert("You do not have new <%=LC.Pat_Patient_Lower%> right on this <%=LC.Std_Study_Lower%>");*****/
				return;
			}

			loadStr = "enrollpatient.jsp?mode=N&srcmenu=tdmenubaritem5&page=patientEnroll&studyId=" + encodeURIComponent(studyId) + "&pkey=" + patient.pk_per + "&patientCode=" + encodeURIComponent(patId) + "&patProtId=&selectedTab=2&patStudiesEnrollingOrg=" + encodeURIComponent(facilityId);
		}
		window.opener.location.href = loadStr;
		window.close();
	}

	return buildGenericPatTable(options, selectPatFromListFunction);

}

function fetchRemotePatList(patId, facilityId, facilityAltId)
{
	var options = "&method=" + SEARCH_PAT_LIST_REMOTE_METHOD + "&" + "patId=" + encodeURIComponent(patId) + "&facility=" + encodeURIComponent(facilityAltId);
	var buttonClickEvent = function(oArgs){
        var oRecord = this.getRecord(oArgs.target);
        var patRow = oRecord.getData();

    	window.opener.location.href =
    		"patientdetailsquick.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient&remotePatSearchIndex=" + encodeURIComponent(patRow.searchIndex) + "&defaultStudyPK=" + encodeURIComponent(studyId) + "&defaultStudyName=" + encodeURIComponent(studyName) + "&defaultFac=" + encodeURIComponent(facilityId);
    	window.close();
     };
    return buildGenericPatTable(options, buttonClickEvent);

}

function buildGenericPatTable(requestOptionsStr, buttonClickFunction){
	var error;
	var returnPats;
	new Ajax.Request('/velos/jsp/patientSearchWorkflowJSON.jsp?' + requestOptionsStr,{asynchronous:false,
		onSuccess:function(transport)
		{
			var isRemoteSearch = false;
			if (requestOptionsStr.indexOf(SEARCH_PAT_LIST_REMOTE_METHOD) > -1){
				isRemoteSearch = true;
			}

		 	if (transport.status==200)
		 	{
			 	var jsonReturnString = transport.responseText;
			 	var foundPats;
			 	try{
			 		foundPats = YAHOO.lang.JSON.parse(jsonReturnString);
			 	}
			 	catch(e){
			 	}
			 	if (foundPats && foundPats.length > 0){
				 	returnPats = foundPats;
			 		document.getElementById("serverpagination").style.visibility="visible";
		            var selectButtonFormatter = function  (elCell, oRecord, oColumn, oData) {
						if (oRecord.getData().isRemote){
							elCell.innerHTML = "<button><%=LC.L_Add%><%--Add*****--%></button>";
						}

						else if (oRecord.getData().onStudy){
							elCell.innerHTML = "<button><%=LC.L_View%><%--View*****--%></button>";
						}
						else if(hasNewPatientStudyRights){
							elCell.innerHTML = "<button><%=LC.L_Asscociate%><%--Asscociate*****--%></button>";
						}

		           	}

		            var onStudyColumnFormatter = function  (elCell, oRecord, oColumn, oData) {
						var checkboxHTML = "";
		            	switch(oRecord.getData().onStudy){
						case true:
							checkboxHTML = "<p class=\"checked\">&nbsp;</p>";
							break;
						case false:
							break;
						}
		            	elCell.innerHTML = checkboxHTML;
		            }
		           	var siteColumnFormatter = function  (elCell, oRecord, oColumn, oData) {
			           	var siteText = sitesMap[oData];
			           	if (!siteText) siteText = "";
						elCell.innerHTML = siteText;
			        }
				 	//var tableContainerDIV = buildTableDIV();
				 	//var tableContai	nerDIV = document.getElementById("search_results");
					//appendStatusElement(tableContainerDIV);
			 		var myColumnDefs;
			 		if (isRemoteSearch){
				 		myColumnDefs = [
									{key:"button",resizeable:true, label:" ", formatter:selectButtonFormatter},
									{key:"patId", label:"<%=LC.L_Patient_Id%><%--Patient ID*****--%>", sortable:true, resizeable:true},
			 		                {key:"firstName", label:"<%=LC.L_First_Name%><%--First Name*****--%>", sortable:true, resizeable:true},
			 		                {key:"lastName", label:"<%=LC.L_Last_Name%><%--Last Name*****--%>", sortable:true, resizeable:true},
			 		                {key:"DOB", label:"<%=LC.L_Dob%><%--DOB*****--%>", minWidth:100, formatter:yuiTableFormatDate,resizeable:true, sortable:true, sortOptions:{defaultDir:YAHOO.widget.DataTable.CLASS_DESC},resizeable:true}
			 		               ];
			 		}
			 		else{
			 			myColumnDefs = [
										{key:"button",resizeable:true, label:" ", width:120, formatter:selectButtonFormatter},
										{key:"patId", label:"<%=LC.L_Patient_Id%><%--Patient ID*****--%>", sortable:true, resizeable:true},
										{key:"onStudy", label:"<%=LC.L_On_Study%><%--On Study*****--%>", formatter:onStudyColumnFormatter, resizeable:true,sortable:true},
										{key:"patFacilityId", label:"<%=LC.L_PatFacility_ID%><%--Patient Facility ID*****--%>", sortable:true, resizeable:true},
				 		                {key:"firstName", label:"<%=LC.L_First_Name%><%--First Name*****--%>", sortable:true, resizeable:true},
				 		                {key:"lastName", label:"<%=LC.L_Last_Name%><%--Last Name*****--%>", sortable:true, resizeable:true},
				 		                {key:"DOB", label:"<%=LC.L_Dob%><%--DOB*****--%>", minWidth:100, formatter:yuiTableFormatDate,resizeable:true, sortable:true, sortOptions:{defaultDir:YAHOO.widget.DataTable.CLASS_DESC},resizeable:true}
				 		               ];
			 		}

		            var myDataSource = new YAHOO.util.DataSource(returnPats);
		            myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		            myDataSource.responseSchema = {
		            	fields: [
		     	            	"pk_per",
		     	            	"patId",
		     	            	"onStudy",
		     	            	"patFacilityId",
		 		            	"searchIndex",
		 		            	"primaryFacility",
		 		            	"firstName",
		 		            	"lastName",
		 		            	"DOB",
		 		            	"patProtMap",
		 		            	"isRemote"] };

			 		var myDataTable =
				 		new YAHOO.widget.DataTable(
				 				"serverpagination",
						 		myColumnDefs,
						 		myDataSource);

			 		myDataTable.subscribe("buttonClickEvent", buttonClickFunction);
			 	}
		 	}

	 	},
	 	onFailure:function(transport)
		{

	 		error = handleServerException(transport);
		}
	});
	if (error) throw error;
	return returnPats;
}




function reloadOpener(href){
	window.opener.location.href = href;
}


function closePressed(){

	window.close();
}

function buildTableDIV(){

	var div = document.createElement("div");
	div.setAttribute("id", "search_results");

	var div2 = document.createElement("div");
	div2.setAttribute("id", "serverpagination");
	div.appendChild(div2);



	return div;
}

function appendStatusText(newLine){
	<%//fixed 4750 by changing method of adding text element%>
	var pTag = document.createElement("p");
	pTag.setAttribute("class","blackComments");
	pTag.className = "blackComments";
	var textNode = document.createTextNode(newLine);
	pTag.appendChild(textNode);
	appendStatusElement(pTag);

}

function clearStatusText(){
	var node = document.getElementById("search_results");

	if(node.hasChildNodes()) {
		while(node.childNodes.length >= 1 ) {
			node.removeChild(node.firstChild);
		}
	}
	document.getElementById("serverpagination").style.visibility="hidden";
}

function appendStatusElement(element){
	document.getElementById("search_results").appendChild(element);
}




function handleServerException(transport){
	if (transport.statusText=="SESSION_TIME_OUT"){
		appendStatusText("<%=MC.M_SessTimeout_CloseWindow%>");/*appendStatusText("Your session has timed out. Please close this window and log in again.");*****/
	}
	else{
		appendStatusText("<%=MC.M_Err_CnctAdmin%>");/*appendStatusText("An error has occured. Please contact your administrator.");*****/
	}
	return "server error";
}

var yuiTableFormatDate = function (elCell, oRecord, oColumn, oData) {
    var oDate = oData;
    var DOBDate = "";
	if (oDate){
		<%//if dob is restricted, don't attempt to format%>
		if (oDate == "*"){
			DOBDate = "*";
		}
		else{
			try{
				var DOB = new Date(oDate);
				DOBDate = formatDate(DOB, calDateFormat);
			}
			catch(e){
			}
		}
	}
	else{
		DOBDate = "";
	}
    elCell.innerHTML = DOBDate;
};

function addNewPatient(patId, facilityId){
	// + "&defaultPatId=" + patient.MRN + "&defaultFac="
	window.opener.location.href =
		"patientdetailsquick.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient&defaultStudyPK=" + encodeURIComponent(studyId) + "&defaultPatId=" + encodeURIComponent(patId) + "&defaultFac=" + encodeURIComponent(facilityId) + "&defaultStudyName=" + encodeURIComponent(studyName);
	window.close();
}



function deferenceCode(remoteValue, codeType){
	var codeKeyField = openerDocument.getElementById(codeType + "_" + remoteValue);
	if (codeKeyField != null){
		return codeKeyField.value;
	}
	return remoteValue;
}

function isSitesEmpty(){
	if(!document.getElementById("orgDD").options ||
			document.getElementById("orgDD").options.length == 0){
		return true;
	}
	else{
		return false;
	}
}
</script>

<title><%=LC.L_Pat_Search%><%--Patient Search*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<body>
<%@ page
	import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.TeamDao,com.velos.eres.business.common.StudySiteDao,com.velos.eres.service.util.StringUtil,com.velos.remoteservice.demographics.*,java.util.*, com.velos.remoteservice.*, com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.CodeDao"%>

<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<!-- <jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/> -->
<%
	String pstudy=request.getParameter("studyId");
	String studyName = request.getParameter("studyName");
	HttpSession tSession = request.getSession(true);
	int managePatientRights = 0;
	int patientDetailsRights = 0;
	if (sessionmaint.isValidSession(tSession))
	{
		//GET STUDY TEAM RIGHTS
		//code copied from studycentricpatentroll.jsp
		//added to fix 4707 2/11/10 DRM
		int userIdFromSession = EJBUtil.stringToNum((String) tSession.getAttribute("userId"));
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(pstudy), userIdFromSession);
		ArrayList tId = teamDao.getTeamIds();

		if (tId.size() == 0) {
			managePatientRights=0 ;
		}
		else{
			studyRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			studyRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			studyRights.loadStudyRights();

			if ((studyRights.getFtrRights().size()) == 0){
				managePatientRights= 0;
				patientDetailsRights = 0;
			}else{
				managePatientRights = Integer.parseInt(studyRights.getFtrRightsByValue("STUDYMPAT"));
				patientDetailsRights = Integer.parseInt(studyRights.getFtrRightsByValue("STUDYVPDET"));

			}

		}
		boolean hasNewPatientStudyRights = ((managePatientRights & 1) == 1);

		//get user group rights
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    	int groupPatientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
    	boolean hasNewPatientGroupRights = ((groupPatientRights & 1) == 1);

   		//find the remote service
		String accountNumStr = (String)tSession.getAttribute("accountId");
		int accountNo = EJBUtil.stringToNum(accountNumStr);

		//get site list for this user, account and study.
		//added to fix 4665 - 2/1/10 - DRM
		StudySiteDao ssDao = new StudySiteDao();
		ssDao	= studySiteB.
					getSitesForStatAndEnrolledPat(
							EJBUtil.stringToNum(pstudy),
							userIdFromSession,
							accountNo);
		List restrictedSitesList = buildRestrictedList(ssDao);
%>
	<jsp:include page="sessionlogging.jsp" flush="true" />
		<script language="JavaScript">
		<%//transfer variables from java to javascript%>



		studyId = "<%=pstudy%>";
		studyName = "<%=studyName%>";
		hasNewPatientStudyRights = <%=hasNewPatientStudyRights%>;
		hasNewPatientGroupRights = <%=hasNewPatientGroupRights%>;
		<%
			for (int i = 0; i < restrictedSitesList.size(); i++){
		%>
				restrictedSites.push(<%=restrictedSitesList.get(i)%>);
		<%
			}

		%>
		</script>

		<%
		ArrayList arrSiteNames = ssDao.getSiteNames();
		ArrayList altIds = ssDao.getAltIds();
		ArrayList ids = ssDao.getSiteIds();
		//2/1/10 DRM - Bugzilla 4664  - removed onchange event
		StringBuilder dSitesRestricted = new StringBuilder();
		//2/2/10 DRM - Bugzilla 4692 - fixed width of organization dropdown
		dSitesRestricted.append("<select id=\"orgDD\" onchange=\"return searchPressed();\" onkeypress=\"if (event.keyCode == 13) {return searchPressed();}\">") ;
		String siteId;
		String id;
		HashMap javaSiteMap = new HashMap();
		for (int counter = 0; counter < altIds.size() ; counter++) {
			siteId =  (String)altIds.get(counter);
			id = EJBUtil.integerToString((Integer)ids.get(counter));
			dSitesRestricted.append("<option value = \""+ siteId + "\"id=\"" + id +"\">" + arrSiteNames.get(counter)+"</option>");

			javaSiteMap.put(id, arrSiteNames.get(counter));
		}
		// 2/4/2010 - DRM Other option removed to support change in requirement
		//dSitesRestricted.append("<option value='0' >Other</option>");
		dSitesRestricted.append("</select>");



		%>
		<script>
		<%
		Iterator idIt = javaSiteMap.keySet().iterator();
		while(idIt.hasNext()){
			String siteIdStr = (String)idIt.next();
			%>sitesMap["<%=siteIdStr%>"]="<%=javaSiteMap.get(siteIdStr)%>";
			<%
		}
		%>
		</script>
		<div id="mrn_search">
		<%//fixed form submit and enter in patient id field for 4730  2/3/10 DRM for 4662 %>
			<form   name="name_search" METHOD="POST" onsubmit="return false;">
			 <table class="searchTable" cellspacing="0" cellpadding="2">
			 	<tr class="searchRow" >
					<td ><b><%=MC.M_SrchPat_IDOrg%><%--Search for <%=LC.Pat_Patient_Lower%> by ID and Organization*****--%></b></td>
				</tr>
				<tr class="searchRow">
				<%Object[] arguments = {studyName}; %>
					<td ><%=VelosResourceBundle.getMessageString("M_SrchLook_PatStd",arguments)%><%--The search will look for the <%=LC.Pat_Patient_Lower%> in <%=LC.Std_Study_Lower%>*****--%>
					<ul>
						<li><%=MC.M_IfPatInStd_ViewPatEnrl%><%--If <%=LC.Pat_Patient_Lower%> is in the <%=LC.Std_Study_Lower%>, you will be able to view the <%=LC.Pat_Patient_Lower%>'s enrollment.*****--%></li>
						<li><%=VelosResourceBundle.getMessageString("M_PatEres_AbleEnrolPat",arguments)%><%--If <%=LC.Pat_Patient_Lower%> is in the eResearch, you will be able to enroll the <%=LC.Pat_Patient_Lower%> on*****--%>  </li>
						<li><%=VelosResourceBundle.getMessageString("M_PatNtEres_EmpiSysPatAdd",arguments)%><%--If <%=LC.Pat_Patient_Lower%> is not in the eResearch but is in the EMPI system, you will be able to first add the <%=LC.Pat_Patient_Lower%> to eResearch, then add to*****--%> </li>
					</ul>
				</tr>
			 </table>
			 <p/>
			 <table class="searchTable" cellspacing="0" cellpadding="2">

				<tr class="searchRow" >
					<td align="right"> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%></td>
					<td >
						<input type="text" name="patId" id="patId"  size="15" onkeypress="if (event.keyCode == 13) {return searchPressed();}">
					</td>
					<%//changed "Facility" to "Organization" 2/1/10 DRM for 4662 %>
					<td  align="right"> <%=LC.L_Organization%><%--Organization*****--%> </td>
	  				<td  colspan="2"> <%=dSitesRestricted.toString()%> </td>
					<td colspan="2" align="center">
						<!--  	<a id="searchButton" href="javascript:void(0);" onClick=" return searchPressed();">
								<image
									src="../images/jpg/Go.gif"
									style="border-style: none"/>
							</a>
							-->
							<button id="searchButton" onClick=" return searchPressed();"><%=LC.L_Search_ForPat%><%--Search for <%=LC.Pat_Patient%>*****--%></button>
					</td>
				</tr >






			 </table>

			</form>
		</div>
		<div id="user_status" class="tabFormTopN  yui-skin-sam">
			<div id="search_results" style="padding: 2;">

			</div>
			<div id="serverpagination">

			</div>
		</div>
		<div id="dialog_buttons">
			<button onClick=" return closePressed();"><%=LC.L_Close%></button>

		</div>

<%

	}//close is valid session
	else{
		%><jsp:include page="timeout.html" flush="true"/> <%
	}
%>
<script>
if(isSitesEmpty()){

	appendStatusText("<%=MC.M_YouNoAccess_OrgStd%>");/*appendStatusText("You do not have access to any organizations for this <%=LC.Std_Study_Lower%>.");*****/
	document.getElementById("orgDD").disabled=true;
	document.getElementById("patId").disabled=true;
	document.getElementById("searchButton").disabled=true;
}
</script>
</body>

</html>

<%!

private List buildRestrictedList(StudySiteDao ssDao){
	ArrayList restrictedList = new ArrayList();
	ArrayList studySiteIds  = ssDao.getSiteIds();
	ArrayList studySiteRestricteds = ssDao.getIsRestricteds();

	for (int x = 0; x < studySiteIds.size() ; x++){
		String restrictFlag = (String)studySiteRestricteds.get(x);
		if ("y".equalsIgnoreCase(restrictFlag)){
			restrictedList.add(studySiteIds.get(x));
		}
	}
	return restrictedList;
}


%>


