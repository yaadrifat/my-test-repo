<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT Language="JavaScript1.2">
 function fclose(formobj) {
  	if (document.all) {
		window.opener.document.eventmessage.msgUsers.value = formobj.userName.value;
	}
	else
	{

		if(parseInt((navigator.appVersion).substr(0,1)) == 5)
		{
			window.opener.document.eventmessage.msgUsers.value = formobj.userName.value;
		}
		else
		{	
			window.opener.document.div1.document.eventmessage.msgUsers.value = formobj.userName.value;
		}
	}
	
	self.close();
}  
</Script>
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.web.eventuser.EventuserJB"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>

<%

String delusers = request.getParameter("delusers");
String userlist = request.getParameter("userlist");
String eSign = request.getParameter("eSign");	
String eventuserId="";
int val=0;
int prevVal,cnt,len;

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");
String protocolId = request.getParameter("protocolId");   
String calledFrom = request.getParameter("calledFrom");   
String eventId = request.getParameter("eventId");



HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
      	String oldESign = (String) tSession.getValue("eSign");

	String propagationType  = ""; 
	
	if(!oldESign.equals(eSign)) 
	{
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
	{
   %>	
   <form name=usersave METHOD="POST">
	 <%
	 
	 
	 	if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))

			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))

			propagateInEventFlag = "N";

		propagationType = "EVENT_MSG_USER";
	 
		len = delusers.length();
		if (len > 0){
    		val = delusers.indexOf(";");
    		prevVal =0;
    	 	for (cnt=0; val < len; cnt++){
    			//out.print("Value" +cnt +" " +val);
    			eventuserId = delusers.substring(prevVal, val);
    			//out.print("Value" +cnt +" " +eventuserId); 
    			
		    	if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) 
				{
					//sonia - this will delete the main event too
					eventuser.setEventuserId(EJBUtil.stringToNum(eventuserId));
					eventuser.setUserType("S");
					eventuser.setEventId(eventId);
					System.out.println("eventId" + eventId);
					System.out.println("eventuserId" + eventuserId);
			   		eventuser.propagateEventuser(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom, propagationType);  
		
				} else
				{   			
					    // Modified for INF-18183 ::: Raviesh
		    		    eventuser.removeEventuser(EJBUtil.stringToNum(eventuserId),AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
		    	}	    
		    	
    			prevVal= val + 1;
    			if (prevVal >= len) break;
    			val = delusers.indexOf(";", prevVal);
    		}
		}
%>
<Input type=hidden name=userName value="<%=userlist%>">


<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%></p>
<!--<p align=center> <A onClick="fclose(document.usersave)" href="#"><img src="../images/jpg/Go.gif" border="0"> </A></p>-->
<Script>
  	if (document.all) {
		window.opener.document.eventmessage.msgUsers.value = usersave.userName.value;
	}
	else
	{
     formobj = document.usersave;
	
		if(parseInt((navigator.appVersion).substr(0,1)) == 5)
		{
			window.opener.document.eventmessage.msgUsers.value = formobj.userName.value;
		}
		else
		{	
			window.opener.document.div1.document.eventmessage.msgUsers.value = formobj.userName.value;
		}
	}
	
	self.close();
</Script>
</form>
<%
 }//end of esign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>


