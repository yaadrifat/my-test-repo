<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Bgt_Browser%><%--Budget Browser*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<Script language="JavaScript1.1">

function openBudget(formobj,id,bgtType){
src = formobj.srcmenu.value;	
mode =formobj.mode.value;

if(bgtType=="Patient"){
	formobj.action= "patientbudget.jsp?mode=" +mode + "&budgetId=" +id +"&srcmenu="+src + "&selectedTab=1";
}
else{		
	formobj.action= "studybudget.jsp?mode=" +mode + "&budgetId=" +id +"&srcmenu="+src + "&selectedTab=1";	
}
	formobj.submit();
}


function setAction(formObj,src){
	lsearchCriteria=formObj.searchCriteria.value;		
	formObj.action="budgetbrowserns.jsp?srcmenu="+src+"&searchCriteria="+lsearchCriteria;
	formObj.submit();
}


</Script>

<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.group.*,java.util.*,com.velos.eres.business.user.*,com.velos.esch.service.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="budgetDao" scope="request" class="com.velos.esch.business.common.BudgetDao"/>

<%
HttpSession tSession = request.getSession(true); 
%>
<br>
<DIV class="browserDefault" id="div1">
  <%
   if (sessionmaint.isValidSession(tSession))
	{		
	String searchCriteria = request.getParameter("searchCriteria");
	if (searchCriteria==null) {searchCriteria="";}
  %>
  	<P class="sectionHeadings"> <%=LC.L_BgtHome_Open%><%--Budget Home >> Open*****--%></P>  
	<form name="budget" method=post action="budgetbrowserns.jsp">		
	<br>
	<table width=100%>
	<tr>
		<td width=25%> <P class="defComments"><%=LC.L_Search_ABgt%><%--Search a Budget*****--%></P></td>
		<td class=tdDefault width=25%> <Input type=text name="searchCriteria" size="20%"> </td>
		<td width=5% align="left">
		<button type="submit" onClick="setAction(document.budget,'<%=src%>')"><%=LC.L_Search%></button></td>
		<td width=45%><P class="defComments">(<%=MC.M_EtrStdNum_BgtName%><%--Enter a study number or keywords in the study or budget name*****--%>)</P></td>
	</tr>
	

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>		
		<td>
		<A href="budgetbrowserns.jsp?&srcmenu=<%=src%>"><P class="defComments"><%=LC.L_View_AllBgts%><%--View All Budgets--%></P></A>
		</td>
	</tr>
	</table>	
	
	<table width="100%" cellspacing="0" cellpadding="0" border="0" >
	 <tr> 
	  <td width = "50%"> 
	      <P class="defComments"><%=MC.M_Flw_AreSvdBgts%><%--The following are your saved Budgets*****--%>:</P>
	  </td>
	  <td width="50%" align="right"> 
		<p>	
	  	 <A href="budget.jsp?mode=N&srcmenu=<%=src%>&fromPage=budgetbrowser" > <%=MC.M_Create_ANewBgt%><%--Create a new Budget*****--%></A>  				  
		</p>
	  </td>
	 </tr>
	</table>

	
	<input type=hidden name="srcmenu" value=<%=src%>>
	<input type=hidden name=frompage value="budgetbrowser">
	<input type=hidden name=mode value="M">
	<% 
	String acc = (String) tSession.getValue("accountId");
	String uName = (String) tSession.getValue("userName");
	String userId = (String) tSession.getValue("userId");
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	int accountId = EJBUtil.stringToNum(acc);

	ArrayList bgtNames= null; 
	ArrayList bgtVersions= null;
	ArrayList bgtTypes= null;
	ArrayList bgtStats= null;
	ArrayList bgtSites= null; 
	ArrayList bgtStudyTitles= null;
	ArrayList bgtStudyNumbers= null;
	ArrayList bgtIds= null;
	String bgtName = "";
	String bgtVersion = "";
	String bgtType = "";
	String bgtStat = "";
	String bgtSite = "";
	String bgtStudyTitle = "";
	String bgtStudyNumber = "";
	String bgtId = "";

	budgetDao.getBudgetsForUser(EJBUtil.stringToNum(userId),searchCriteria);
	
	bgtNames=budgetDao.getBgtNames(); 
	bgtVersions= budgetDao.getBgtVersions();
	bgtTypes= budgetDao.getBgtTypes();   
	bgtStats= budgetDao.getBgtStats();
	bgtSites= budgetDao.getBgtSites();
	bgtStudyTitles= budgetDao.getBgtStudyTitles();
			 bgtStudyNumbers= budgetDao.getBgtStudyNumbers();
			 bgtIds = budgetDao.getBgtIds();

	
			 int len = bgtVersions.size() ;
			 
	%>
	<table width=98%>
	<tr>
	<th width="14%"> <%=LC.L_Budget_Name%><%--Budget Name*****--%> </th>
	<th width="14%"> <%=LC.L_Version%><%--Version*****--%> </th>
	<th width="14%"> <%=LC.L_Bgt_Type%><%--Budget Type*****--%> </th>
	<th width="14%"> <%=LC.L_Study_Number%><%--Study Number*****--%> </th>
	<th width="14%"> <%=LC.L_Title%><%--Title*****--%> </th>
	<th width="14%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
	<th width="14%"> <%=LC.L_Status%><%--Status*****--%> </th>
	</tr>
	
	<%
	for(int counter = 0;counter<len;counter++){
  
  	bgtName=((bgtNames.get(counter)) == null)?"-":(bgtNames.get(counter)).toString();
	bgtVersion=((bgtVersions.get(counter)) == null)?"-":(bgtVersions.get(counter)).toString();
	bgtType=((bgtTypes.get(counter)) == null)?"-":(bgtTypes.get(counter)).toString();
	bgtStudyNumber=((bgtStudyNumbers.get(counter)) == null)?"-":(bgtStudyNumbers.get(counter)).toString();
	bgtStudyTitle=((bgtStudyTitles.get(counter)) == null)?"-":(bgtStudyTitles.get(counter)).toString();
	bgtSite=((bgtSites.get(counter)) == null)?"-":(bgtSites.get(counter)).toString();
	bgtStat=((bgtStats.get(counter)) == null)?"-":(bgtStats.get(counter)).toString();
	bgtId = ((bgtIds.get(counter)) == null)?"-":(bgtIds.get(counter)).toString();

	if ((counter%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  	<%
		}
	else {
  	%>
      <tr class="browserOddRow"> 
   	<%
		}
   	%>	


	<td><A href="#" onClick="return openBudget(document.budget,'<%=bgtId%>','<%=bgtType%>')"> <%=bgtName%> </A>
	</td>
	<td><%=bgtVersion%></td>
	<td><%=bgtType%></td>
	<td><%=bgtStudyNumber%></td>
	<td><%=bgtStudyTitle%></td>
	<td><%=bgtSite%></td>
	<td><%=bgtStat%></td>

<%
  
	}

	%>
	
	
	
	</table>
	
</form>
		
		
<%
		
	}// end of the session if
	else{
  %>
  	<jsp:include page="timeout.html" flush="true"/>
  	<%
	}
	%>
	<div> 
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</DIV>

<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>
