<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
	<title><%=LC.L_Preparation_Cart%><%--Preparation Cart*****--%></title>
	
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF='./styles/yuilookandfeel/yuilookandfeel.css' type=text/css>
</HEAD>


<BODY leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" style="overflow:auto">

<%@ page language = "java" import = "com.velos.eres.web.user.UserJB,com.velos.esch.business.common.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.web.specimen.*,java.util.List,java.util.ArrayList,java.util.Arrays"%>
<% String src;
src= request.getParameter("srcmenu");
String checkBoxdisplay = request.getParameter("checkboxdisplay");
String selectedTab = request.getParameter("selectedTab");
%>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.MsgCntrDao, com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>





	<%
	
	HttpSession tSession = request.getSession(true);
	
	 ArrayList personPIdList = new ArrayList();	

	 ArrayList studyNumberList=new ArrayList();

	 ArrayList CalendarList=new ArrayList();

	 ArrayList visitList=new ArrayList();

	 ArrayList scheduledDateList=new ArrayList();

	 ArrayList eventList=new ArrayList();

	 ArrayList storageKitList=new ArrayList();

	
 
	if (sessionmaint.isValidSession(tSession))
	{
	String checkName = request.getParameter("checkName");
	String checkedTrue = request.getParameter("checkedTrue");
	
	%>
	
	

<Form name = "prepcart" action="preparationCart.jsp" METHOD="POST">
	
<%
PreparationCartDto preparationCartDto = (PreparationCartDto)tSession.getAttribute("aPreparationCartDto");
if(preparationCartDto !=null){
personPIdList = preparationCartDto.getPersonPIdList();
studyNumberList = preparationCartDto.getStudyNumberList();
CalendarList = preparationCartDto.getCalendarList();
visitList= preparationCartDto.getVisitList();
scheduledDateList = preparationCartDto.getScheduledDateList();
eventList = preparationCartDto.getEventList();
storageKitList = preparationCartDto.getStorageKitList();
}



int length =personPIdList.size();
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablebdr">
 <%
if(length>0) {
	 String normalrowCss="";
     String selectedColCss="";
 %>
 <tr align="center">
    <!-- Ashu:03/10/2011 fixed BUG#5791 -->
    <th  height="20" class="normalcol" style="cursor: auto;"><%=LC.L_Patient_Id%><%--Patient ID*****--%></th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Study_Number%><%--Study Number*****--%></th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Calendar%><%--Calendar*****--%> </th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Visit%><%--Visit*****--%> </th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Scheduled_Date%><%--Scheduled Date*****--%> </th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Event%><%--Event*****--%> </th>
    <th  class="normalcol" style="cursor: auto;"> <%=LC.L_Storage_Kit%><%--Storage Kit*****--%> </th>
      <%if(!StringUtil.isEmpty(checkBoxdisplay) && checkBoxdisplay.equals("Y")){ %>
    <th  class="normalcol" style="cursor: auto;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkallrow" name="checkallrow" onClick="checkAll()" <%=checkedTrue%>><%=checkName%></th>
     <%}
     else{%>    
     <th  class="normalcol" style="cursor: auto;">&nbsp;</th>
  </tr>
<%     
     }
 for(int i=0; i<length;i++ )
 {
 
	 if ((i%2)==0) 
		{
			normalrowCss="normalinfoeven";
			selectedColCss="selectedinfoeven";
		}
		else{
			normalrowCss="normalinfoodd";
		       selectedColCss="selectedinfoodd";
			}
 %>
	 <tr align="center"  class="normalinfo">
	    <td height="25" class="<%=normalrowCss %>"> <%=personPIdList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=studyNumberList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=CalendarList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=visitList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=scheduledDateList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=eventList.get(i) %></td>
	    <td class="<%=normalrowCss %>"><%=storageKitList.get(i) %></td>	    
	   <%if(!StringUtil.isEmpty(checkBoxdisplay) && checkBoxdisplay.equals("Y")){ %>
	    <td class="<%=normalrowCss %>"><input type="checkbox" value="<%=i%>" id="checkRow" name="checkRow" <%=checkedTrue%>></td>
	    <%
	   }
	   else{
	    %>
	    <td class="<%=normalrowCss %>"><input type="checkbox" value="<%=i%>" id="checkRow" name="checkRow" style="display: none" ></td>
	    <%
	    }
	    %>
	</tr>
 <%}
}else {	
	%>
	<tr height="5">
	<td  colspan="8"><center><b><%=MC.M_CartEmpty_AddItemUsingLnk%><%--Cart is Empty! Add item(s) using Add to Preparation Cart link!*****--%></b></center></td>
	</tr>
	<%
}


%>	
	</table>
		</form>


</td>
</tr>
</table>



	<%
	} //end of body for session
	else
	{
	%>
		<div id=myName></div>
		<div id=myStudies></div>
		<div id=myLinks></div>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	
	%>



</BODY>

</HTML>