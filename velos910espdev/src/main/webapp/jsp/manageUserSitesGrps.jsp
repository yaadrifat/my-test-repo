<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_Org%><%--Manage Organizations*****--%></title>
</head>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id ="userSiteGrpJB" scope="request" class="com.velos.eres.web.usrSiteGrp.UsrSiteGrpJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<%@ page language = "java" import="java.util.*,java.io.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.userSite.*,com.velos.eres.web.user.UserJB"%>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function showDD(obJ,count)
{
	var grpPk="#groupPk"+count;
	var select = $j(grpPk).val();
	var selected=select.split(',');
	var ddShow="#userGroups"+count;
	var mulSelect=$j(ddShow);
	for (var i=0;i<selected.length;i++) {
	    var val=selected[i];
	    mulSelect.find('option:[value='+val+']').attr('selected',1);
	}
	if(select.length>0)
	{
		mulSelect.find('option:[value='+''+']').attr('selected',0);
	}
	$j("#"+obJ.id).hide();
	$j(ddShow).show();
	$j(ddShow).focus();
}

function saveValues(obJ,count)
{
	var txtShow="#groupAccess"+count;
	var grpPk="#groupPk"+count;
	$j("#"+obJ.id).hide();
	$j(txtShow).show();	
	var selVal= $j("#"+obJ.id).val();
	if(selVal!=""){
		var selMulti = $j.map($j("#"+obJ.id+" option:selected"), function (el, i) {
			if($j(el).val()!=""){
			return $j(el).text();
			}else{
			   return ;
			}
	  	});
		var selMulPk = $j.map($j("#"+obJ.id+" option:selected"), function (el, i) {
			if($j(el).val()!=""){
			return $j(el).val();
			}else{
			   return ;
			}
		});
		
	   	 $j(txtShow).val(selMulti.join(", "));
	   	 $j(grpPk).val(selMulPk.join(","));
	}else{
		 $j(txtShow).val("");
	   	 $j(grpPk).val("");
	}
}

function goBack()
{
	window.location="managesites.jsp?userId="+document.getElementById("userId").value;
}
</script>
<body>
<div class="popDefault" style="width:480px">
<%		
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>
<%
     int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 	 int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	 
	 String sessUserId =	(String) tSession.getValue("userId");
	 int curUser = EJBUtil.stringToNum(sessUserId);
	 
	 userB.setUserId(userId);
	 userB.getUserDetails(); 
	 
 	 String  userLastName = userB.getUserLastName();
	 String  userFirstName = userB.getUserFirstName();
	 
	 /////////////////////////////
		int pageRightModCdrPf = 0,modCDRPFseq=0;
	 	char cdrpfRight='0';
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		modCtlDao.getControlValues("module");
		ArrayList modCtlDaofeature =  modCtlDao.getCValue();
		ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();
		
		modCDRPFseq = modCtlDaofeature.indexOf("MODCDRPF");
		pageRightModCdrPf = ((Integer) modCtlDaoftrSeq.get(modCDRPFseq)).intValue();
		cdrpfRight = modRight.charAt(pageRightModCdrPf - 1);
		
%>
	<P class="sectionHeadings"><%=MC.M_EditUsrOrgGroupAccess %></P>
<%
	 int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
	 String userSiteFlag = userB.getUserSiteFlag();
	 if (userSiteFlag==null) userSiteFlag="S"; //set this as default
%>   
   <Form name="userOrgGrpSite" id="userOrgGrpSiteForm" method="post" action="updatesUserSiteOrgsGrps.jsp" >
   <input type="hidden" name="userPrimOrg" value=<%=userPrimOrg%>>
   <input type="hidden" name="userId" id="userId" value=<%=userId%>>   
   <br>
   <table width="100%" cellpadding="0" cellspacing="0" border="0" >
   <tr><th style="border-right: 1px solid gray;"><%=LC.L_Organization %></th>
        <th><%=LC.L_Group %></th></tr>
        
<%   
     int userSiteId = 0;
     int userSiteRight = 0;
     String userSiteName = "";

     ArrayList userSiteIds = new ArrayList();		
     ArrayList userSiteRights = new ArrayList();					
     ArrayList userSiteNames = new ArrayList();
     	 
     UserSiteDao userSiteDao = userSiteB.getUserSiteTree(accountId,userId);
     userSiteIds = userSiteDao.getUserSiteIds();
     userSiteNames = userSiteDao.getUserSiteNames();
     userSiteRights = userSiteDao.getUserSiteRights();
 
     ArrayList userSiteGrpPk = new ArrayList();
     ArrayList userSiteGrpSiteId = new ArrayList();
     ArrayList userSiteGrpIds = new ArrayList();
     UserSiteGroupDao userSiteGrpDao = userSiteGrpJB.getUsrSiteGrp(userId);
     userSiteGrpPk = userSiteGrpDao.getPkUsrSiteGrp();
     userSiteGrpSiteId =userSiteGrpDao.getUserSiteIds();
     userSiteGrpIds = userSiteGrpDao.getUserFkGroupIds();
     CodeDao cd1 = new CodeDao();
     cd1.getUserGroups(userId,accountId);
     
     HashMap userSiteGrpdata = new HashMap();
     HashMap userSiteGrpValue = new HashMap();
     HashMap userSiteGrpValuedata = new HashMap();
     HashMap userSiteGrpPkdata = new HashMap();
     int presentDatalen= userSiteGrpPk.size();
     for(int i=0; i<presentDatalen;i++)
     {
    	 int userSiteGrpId= Integer.parseInt((String)userSiteGrpIds.get(i));
    	 ArrayList myArrayDesc = cd1.getCDesc();
    	 ArrayList myArrayId = cd1.getCId();
	 for(int j=0;j<myArrayId.size();j++)
	 {
		 userSiteGrpValue.put(""+myArrayId.get(j),myArrayDesc.get(j)); 
	 }

	 StringBuffer userGrpsIds= new StringBuffer();
    	 StringBuffer userGrpsValues= new StringBuffer();
    	 StringBuffer userGrpsIdPks= new StringBuffer();
    	 if(userSiteGrpdata.containsKey(userSiteGrpSiteId.get(i)))
    	 { 
    	    userGrpsIds.append(userSiteGrpdata.get(userSiteGrpSiteId.get(i))).append(",").append(userSiteGrpIds.get(i));
    	    userSiteGrpdata.put(userSiteGrpSiteId.get(i),userGrpsIds.toString());
    	    
    	    userGrpsValues.append(userSiteGrpValuedata.get(userSiteGrpSiteId.get(i))).append(",").append(userSiteGrpValue.get(String.valueOf(userSiteGrpIds.get(i))));
    	    userSiteGrpValuedata.put(userSiteGrpSiteId.get(i),userGrpsValues.toString());
    	    
    	    userGrpsIdPks.append(userSiteGrpPkdata.get(userSiteGrpSiteId.get(i))).append(",").append(userSiteGrpPk.get(i));
    	    userSiteGrpPkdata.put(userSiteGrpSiteId.get(i),userGrpsIdPks.toString());
    	 }else{
    	    userSiteGrpdata.put(userSiteGrpSiteId.get(i),userSiteGrpIds.get(i));
    	    userSiteGrpValuedata.put(userSiteGrpSiteId.get(i),userSiteGrpValue.get(String.valueOf(userSiteGrpIds.get(i))));
    	    userSiteGrpPkdata.put(userSiteGrpSiteId.get(i),userSiteGrpPk.get(i));
    	 }
     }
    
     String userGroups="";

     int len = userSiteIds.size();
     int count =0; 
     for (int counter=0;counter<len;counter++) {
	 //pkUserSiteId
     	userSiteId=EJBUtil.stringToNum(((userSiteIds.get(counter)) == null)?"-":(userSiteIds.get(counter)).toString().trim());
	//fk_site
     	userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
 	userSiteName = ((userSiteNames.get(counter)) == null)?"-":(userSiteNames.get(counter)).toString();


if (userSiteRight>=4){
  userGroups = cd1.toPullDown("userGroups"+count,0," id=\"userGroups"+count+"\" multiple='multiple' width=\"100%\" style=\"display:none;\" size=\"5\" onblur=\"return saveValues(this,'"+count+"');\"");
    %> 
    <tr>
    <td width="70%" height="25px"> <Input type="hidden" name="rights" value=<%=userSiteRight%> >
         <Input type="hidden" name="pkUserSite" value=<%=userSiteId%> >
         <%=userSiteName%>
   </td>
   <td align="right">
       <input type="hidden" name="userSiteGrpPk" id="userSiteGrpPk" value="<%=userSiteGrpPkdata.get(String.valueOf(userSiteId))==null?"": userSiteGrpPkdata.get(String.valueOf(userSiteId))%>">
       <input type="hidden" name="groupOldPk" id="groupOldPk<%=count%>" value="<%=userSiteGrpdata.get(String.valueOf(userSiteId))==null?"":userSiteGrpdata.get(String.valueOf(userSiteId)) %>">
       <input type="hidden" name="groupPk" id="groupPk<%=count%>" value="<%=userSiteGrpdata.get(String.valueOf(userSiteId))==null?"":userSiteGrpdata.get(String.valueOf(userSiteId)) %>">
       <input type="text" name="groupAccess" id="groupAccess<%=count%>" value="<%=userSiteGrpValuedata.get(String.valueOf(userSiteId))==null?"":userSiteGrpValuedata.get(String.valueOf(userSiteId))  %>" onclick="return showDD(this,'<%=count%>');"> 
       <%=userGroups %>
       </td>
        <%
        count++;        
} %>
    </tr>
    <%} %>
    
   </table>
<br/>	
<%if((String.valueOf(cdrpfRight).compareTo("1") == 0))
{ %>
<tr><td width="100%" height="10"></td></tr>
<tr><td width="100%" colspan="5">
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="userOrgGrpSiteForm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td></tr>
<tr><td><button onClick="return goBack();" type="button"><%=LC.L_Back%></button></td></tr>
<%}%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%

 

} else {  //else of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>

</div>
</body>
</html>
