<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Mstone_PmentBrow%><%-- Milestone >> Payment Browser*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT>
	function AddMilepayment(formObj,windowName,pgRight,mode) 
	{
	if (f_check_perm(pgRight,'N') == true) {     
      windowName= window.open(windowName+mode,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450,top=200,left=200")
      windowName.focus();      
	}else {
		return false;
	}			
	
	}
	
	function EditMilepayment(formObj,windowName,pgRight,mode) 
	{
	if (f_check_perm(pgRight,'E') == true) {     
      windowName= window.open(windowName+mode,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450,top=200,left=200")
      windowName.focus();      
	}else {
		return false;
	}			
	
	}
	
	function openWindow(winurl) 
	{

      windowName = window.open(winurl,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();      
	
	}
	function confirmBox(pageRight,value){
          
         if(!f_check_perm(pageRight,value)) {
	      return false;
	  } 
	   
       /*if (confirm("Do you want to Delete Payments ?" )) {*****/
    	if (confirm("<%=MC.M_WantToDel_Pay%>" )) {
        return true; 
       }	  
      else
      {
	   return false;
	   }
      
}

</SCRIPT>



<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
	
<jsp:useBean id="milepaymentB" scope="request" class="com.velos.eres.web.milepayment.MilepaymentJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<%@ page language = "java" import = "com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.common.MilestoneDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.MilepaymentDao,com.velos.eres.service.util.*"%>

<%
 String selectedTab = request.getParameter("selectedTab"); 

 String srcmenu= request.getParameter("srcmenu");
 String study= request.getParameter("studyId");
 int studyId = EJBUtil.stringToNum(study); 

%>   

<DIV class="BrowserTopn" id="div1">
  <jsp:include page="milestonetabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
  </jsp:include>

</div>
<DIV class="BrowserBotN BrowserBotN_M_2" id="div2">

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{	

	String pageRight= "";	 
	int pgRight = 0;
 
	String userIdFromSession = (String) tSession.getValue("userId");
	
	int mileRight= 0;
	ArrayList teamId ;
	teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		mileRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		mileRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRightsB.loadStudyRights();


		if ((stdRightsB.getFtrRights().size()) == 0){
			mileRight = 0;
		}else{
			mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}
	tSession.setAttribute("mileRight",(new Integer(mileRight)).toString());

 	pageRight = (new Integer(mileRight)).toString();	
	pgRight = EJBUtil.stringToNum(pageRight);

  if (pgRight > 0 ){
  
	String milepaymentId = "";
	String milepaymentDate = "";
	String milepaymentDesc = "";
	String milepaymentAmount = "";
	//String amountNumeric = "";
	//String amountFractional = "";
	//int decPos = 0;
	
	MilepaymentDao milepaymentDao = milepaymentB.getMilepaymentReceived(studyId);
	
	ArrayList milepaymentIds= milepaymentDao.getMilepaymentIds();
	
	ArrayList milepaymentDates= milepaymentDao.getMilepaymentDates();
	ArrayList milepaymentDescs= milepaymentDao.getMilepaymentDescs();
	ArrayList milepaymentAmounts= milepaymentDao.getMilepaymentAmounts();
	ArrayList milepaymenttypes = milepaymentDao.getMilepaymentTypes();

	int len = milepaymentIds.size();
	String hrefVal="milepayments.jsp?studyId="+study+"&mode=";
		
	String studyCurrency = "";
	ArrayList curr = new ArrayList();
	String currency = "";		
	
	studyB.setId(EJBUtil.stringToNum(study));
   	studyB.getStudyDetails();
   	studyCurrency = studyB.getStudyCurrency();	
	SchCodeDao cdDesc = new SchCodeDao();
	cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
	curr =  cdDesc.getCSubType();
	if (curr.size() >0 ) {
		currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());
	}		
		
%>
	<Form name="milepayment" method="post" action="" onsubmit="">
    <Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
    <table width="98%" cellspacing="0" cellpadding="0" border=0 >
	  <tr height="25"> 
        <td width = "78%"> 
		<!-- Modified By parminder singh Bug#10127-->
          <P class="sectionHeadingsFrm"><%=LC.L_Std_Payments%><%-- <%=LC.Std_Study%> Payments*****--%>:</P>
        </td>
		
		<td width = "20%" align="right">
		<A onClick="return AddMilepayment(document.milepayment,'<%=hrefVal%>','<%=pageRight%>','N')" HREF="#"><%=LC.L_Add_New%><%-- ADD NEW*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		
		
      </tr>
      <tr> 
        <td height="5"></td>
        <td height="5"></td>
      </tr>
    </table>


    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign">
      <tr> 
        <th width="15%"> <%=LC.L_Date%><%-- Date*****--%> </th>
        <th width="20%"> <%=LC.L_Payment_Type%><%-- Payment Type*****--%> </th>
        <th width="20%">  <%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_AmtRcvd_In",arguments1)%><%--Amount Received ( in <%=currency%>)*****--%></th>
        <th width="15%"> <%=LC.L_Reconcile%><%-- Reconcile*****--%> </th>
        <th width="30%"> <%=LC.L_Description%><%-- Description*****--%> </th>
        <th width="8%"><%=LC.L_Delete%><%--Delete*****--%> </th>		
      </tr>		
 	  
    <%		

    for(int counter = 0;counter<len;counter++)
	{	
	milepaymentId=(((milepaymentIds.get(counter)) == null)?"-":(milepaymentIds.get(counter)).toString());
	milepaymentDate=(((milepaymentDates.get(counter)) == null)?"-":(milepaymentDates.get(counter)).toString());
	milepaymentDesc=(((milepaymentDescs.get(counter)) == null)?"-":(milepaymentDescs.get(counter)).toString());	
	//milepaymentAmount=(((milepaymentAmounts.get(counter)).equals("null"))?"No Associated Amount":(milepaymentAmounts.get(counter)).toString());
	
	milepaymentAmount=(((milepaymentAmounts.get(counter))==(null))?"No Associated Amount":(milepaymentAmounts.get(counter)).toString()); 
	
		
	
	String hrefVal1="milepayments.jsp?studyId="+study+"&milepaymentId="+milepaymentId+"&mode="; 		
		 
		if(counter%2==0){ 
	%>	
		<TR class="browserEvenRow">	
	<%
	}else{
	%>
		<TR class="browserOddRow">
	<% 
	} 
	%>
	
	<td class=tdDefault align="center"><A href="#" onClick="return EditMilepayment(document.milepayment,'<%=hrefVal1%>','<%=pageRight%>','M')" HREF="#"><%=milepaymentDate%></A></td>
	<td class=tdDefault align="center"><%= (String) milepaymenttypes.get(counter)%></td>
	<td class=tdDefault align="center"><%=milepaymentAmount%></td>
	<td class=tdDefault align="center"><A onClick="openWindow('linkInvPayBrowser.jsp?paymentPk=<%=milepaymentId%>&studyId=<%=studyId%>&pR=<%=pageRight%>')" href="#" ><%=LC.L_Reconcile%><%-- Reconcile*****--%></A></td>
	<td class=tdDefault align="center"><%=milepaymentDesc %></td>
	<td> <A href="milepaymentdelete.jsp?studyId=<%=studyId%>&milepaymentId=<%=milepaymentId%>&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>" onClick="return confirmBox(<%=pageRight%>,'E')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
	</tr>
	
	
	
	
	<%

		
	}
	
	
	%>
	
  </table>	
  </Form>
  <%


	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right


}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

