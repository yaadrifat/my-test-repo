<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.VelosResourceBundle" %>
<div id="summ4Div">
<table>
<tr>
<td width="50%">

<table border="0">
	<tr>
		<td align="right" width="30%" height="22px" style="padding-right:5px; ">
			<%=VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics",LC.L_Type) %>
			<span id="ctrpDraftJB.summ4Type_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td width="1%">&nbsp;</td>
		<td align="left">
		<s:hidden name="ctrpDraftJB.summ4TypeInt" id="ctrpDraftJB.summ4TypeInt" ></s:hidden>
		<s:textfield name="ctrpDraftJB.summ4Type" id="ctrpDraftJB.summ4Type" readonly="true" size="50"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; ">
		<%=VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics",LC.L_Id_Upper) %>
	</td>
	<td width="1%">&nbsp;</td>
	<td align="left"><s:textfield name="ctrpDraftJB.summ4CtrpId" id="ctrpDraftJB.summ4CtrpId" size="50" maxsize="50"></s:textfield>
	</td>
	</tr>
	<tr>
	<td align="right" height="30px" valign="top"><span id="ctrpDraftJB.summ4CtrpId_error" class="errorMessage" style="display:none;"></span>&nbsp;</td>
	<td width="1%"><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
	<td align="left">
	<p class="defComments"><font>
	<%=MC.CTRP_DraftSumm4OrgEnterOrSelect %></font></p>
	</td>
	</tr>
	<tr>
	<td align="right" valign="top" style="padding-right:5px; "><a href="javascript:openSelectOrg(constSum4Org);" ><%=LC.L_Select %></a></td>
	<td><s:hidden name="ctrpDraftJB.summ4FkSite" id="ctrpDraftJB.summ4FkSite" /> 
	<s:hidden name="ctrpDraftJB.summ4FkAdd" id="ctrpDraftJB.summ4FkAdd" /></td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.L_Name%></td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4Name" id="ctrpDraftJB.summ4Name" readonly="true" size="50" cssClass="readonly-input"></s:textfield>&nbsp;
	</td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.L_Street_Address%>
	<span id="ctrpDraftJB.summ4Street_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left" >
	<s:textfield name="ctrpDraftJB.summ4Street" id="ctrpDraftJB.summ4Street" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.L_City%>
	<span id="ctrpDraftJB.summ4City_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4City" id="ctrpDraftJB.summ4City" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.CTRP_DraftStateProvince%>
	<span id="ctrpDraftJB.summ4State_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4State" id="ctrpDraftJB.summ4State" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.L_ZipOrPostal_Code%>
	<span id="ctrpDraftJB.summ4Zip_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4Zip" id="ctrpDraftJB.summ4Zip" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" height="22px" style="padding-right:5px; "><%=LC.L_Country%>
	<span id="ctrpDraftJB.summ4Country_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4Country" id="ctrpDraftJB.summ4Country" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; "><%=LC.L_Email_Addr%>
	<span id="ctrpDraftJB.summ4Email_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td >&nbsp;</td>
	<td align="left">
	<s:textfield name="ctrpDraftJB.summ4Email" id="ctrpDraftJB.summ4Email" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; "><%=LC.L_Phone%></td>
	<td >&nbsp;</td>
	<td align="left"><s:textfield name="ctrpDraftJB.summ4Phone" id="ctrpDraftJB.summ4Phone" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; "><%=LC.L_TTY %></td>
	<td >&nbsp;</td>
	<td align="left"><s:textfield name="ctrpDraftJB.summ4Tty" id="ctrpDraftJB.summ4Tty" size="50"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; "><%=LC.L_Fax%></td>
	<td >&nbsp;</td>
	<td align="left"><s:textfield name="ctrpDraftJB.summ4Fax" id="ctrpDraftJB.summ4Fax" size="50"></s:textfield></td>
	</tr>
	<tr>
	<td align="right" style="padding-right:5px; "><%=LC.L_Url.toUpperCase()%></td>
	<td >&nbsp;</td>
	<td align="left"><s:textfield name="ctrpDraftJB.summ4Url" id="ctrpDraftJB.summ4Url" size="50"></s:textfield></td>
	</tr>
</table>

</td>
<td>
	<table align="left" style="width: 50%;">
		<tr>
			<td>&nbsp;&nbsp;</td>
		</tr>
	</table>
</td>
</tr>
</table>
</div>