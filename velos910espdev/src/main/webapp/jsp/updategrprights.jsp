<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="grp" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="grprights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="session" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="stdRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
int ienet = 2;
String agent1 ;
agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;


int ret = 0;



String grpId,rights[],cnt,rght="0";

int totrows =0,count=0,id;



String src;

src=request.getParameter("src");



grpId = request.getParameter("fk_grp"); //Total Rows



//out.print( "Group" +grpId);

id = Integer.parseInt(grpId);

grprights.setId(id);

totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {

String ipAdd = (String) tSession.getValue("ipAdd");
String usr = (String) tSession.getValue("userId");

String superUserFlag  = request.getParameter("supuser");
String oldsuperUserFlag  = request.getParameter("oldsupuser");
String superBudFlag  = request.getParameter("supbud");

superBudFlag=(superBudFlag==null)?"0":superBudFlag;




String oldSuperUserFlag = "";
String oldSupBudFlag="";
boolean forSuperUser = false,forBudUser=false;
int cntVal = 4;

if (superUserFlag == null )
	superUserFlag = "0";

grp.setGroupId(id);
grp.getGroupDetails();

if (superUserFlag.equals("1"))
{

    oldSuperUserFlag = 	grp.getGroupSuperUserFlag();

    if (oldSuperUserFlag == null)
   		oldSuperUserFlag = "0";

	if (! oldSuperUserFlag.equals("1") )
	{
        ctrl.getControlValues("study_rights");
		int rightsRows = ctrl.getCRows();

   /**bug 2007*/
    stdRights.setId(Integer.parseInt(grpId));
	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();

for(int r=0;r<rightsRows;r++){
	String ftr = (String) feature.get(r);

	String desc = (String) ftrDesc.get(r);
	Integer seq = (Integer) ftrSeq.get(r);
	if ((ftr.compareTo("STUDYSUM") != 0) && (ftr.compareTo("STUDYVPDET") != 0)  && (ftr.compareTo("STUDYREP") != 0))
			  cntVal = cntVal + 1;

	if ( (ftr.compareTo("STUDYNOTIFY") != 0)  && (ftr.compareTo("STUDYREP") != 0) && (ftr.compareTo("STUDYVPDET") != 0)  )
			 cntVal = cntVal + 2;


	 //cntRghts.add(new Integer(cntVal));

     stdRights.setGrSeq(String.valueOf(r));
     stdRights.setFtrRights(String.valueOf(cntVal));
	 stdRights.setModifiedBy(usr);
	 stdRights.setIpAdd(ipAdd);
	 stdRights.updateGrpSupUserStudyRights();
	 cntVal = 4;

}
	/**/

		StringBuffer studyRights = new StringBuffer();
		String studyRightsString = null;

		for(int ct=0;ct<rightsRows;ct++){
			studyRights.append("7");
			}
		studyRightsString = studyRights.toString();

		grp.setGroupSuperUserFlag("1");
		grp.setRoleId("0"); //JM: 22Apr2010: #4852


		grp.setGroupSuperUserRights(studyRightsString);
		//System.out.println("studyRightsString" + studyRightsString);
		//grp.updateGroup();
		forSuperUser = true;

	} // if oldSuperUserFlag is changed
  }
 else
 {
 		grp.setGroupSuperUserFlag("0");
		grp.setRoleId("0");
		//grp.updateGroup();
 }

if (superBudFlag.equals("1"))
{

    oldSupBudFlag =grp.getGroupSupBudFlag();

    if (oldSupBudFlag == null)
   		oldSupBudFlag = "0";

	if (! oldSupBudFlag.equals("1") )
	{
        ctrl.getControlValues("bgt_rights");
	int rightsRows = ctrl.getCRows();
	System.out.println("Budget right erows" +  rightsRows);
		StringBuffer bgtRights = new StringBuffer();
		String bgtRightsString = null;

		for(int ct=0;ct<rightsRows;ct++){
			bgtRights.append("7");
			}
		bgtRightsString = bgtRights.toString();
		System.out.println("Budget right " +  bgtRightsString);
		grp.setGroupSupBudFlag("1");

		grp.setGroupSupBudRights(bgtRightsString);
		//System.out.println("studyRightsString" + studyRightsString);
		//grp.updateGroup();
		forBudUser = true;

	} // if oldSuperUserFlag is changed
  }
 else
 {
 		grp.setGroupSupBudFlag("0");
		//grp.updateGroup();
 }


 grp.updateGroup();

 //remove the rows from setting table for filter
 if (!(superUserFlag.equals("1")))
  {
  String[] keyArray = { "STUDY_TAREA","STUDY_DIVISION","STUDY_DISSITE" };
  commonB.purgeSettings(grpId,"4",new ArrayList(Arrays.asList(keyArray)));

  }
  if (!(superBudFlag.equals("1")))
  {
  String[] keyArray = { "BUD_TAREA","BUD_DIVISION","BUD_DISSITE" };
  commonB.purgeSettings(grpId,"4",new ArrayList(Arrays.asList(keyArray)));

  }

if (totrows > 1){

	rights = request.getParameterValues("rights");

	for (count=0;count<totrows;count++){

		rght = rights[count];

		//out.print("Count" +count +" Rights" +rght);

		cnt = String.valueOf(count);

		grprights.setGrSeq(cnt);

		grprights.setFtrRights(rght);

	}

}else{

	rght = request.getParameter("rights");

	grprights.setGrSeq("1");

	grprights.setFtrRights(rght);

}

grprights.setModifiedBy(usr);
grprights.setIpAdd(ipAdd);

grprights.updateGrpRights();

//out.println("UPDATE Group Rights");

%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
	<% if (forSuperUser)
	{
	%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=supuserstudyrights.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser&budRight=<%=forBudUser%>">
	<%
	}
	if (forBudUser)
	{%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=supbudgetrights.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser">
	<%}
	else
	{
	%>
    <META HTTP-EQUIV=Refresh CONTENT="1; URL=groupbrowserpg.jsp?srcmenu=<%=src%>">

	<%
} // end for for super user

}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





