<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Evt_Lib%><%--Event Library*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<script>
	function f_submit() {
		document.selectDOW.submit();
		return false;
	}
</script>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>



<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<% int accountId=0;  
   int pageRight = 0;
   int rowNo = 0;	
   HttpSession tSession = request.getSession(true); 
	 
   String name = "";
   String param = "event_name";
   String protocolId = request.getParameter("protocolId");
   String duration= request.getParameter("duration");
   String calledFrom = request.getParameter("calledFrom");
   String selectedTab = request.getParameter("selectedTab");
   String mode = request.getParameter("mode");
   String calStatus= request.getParameter("calStatus");
   String displayType=request.getParameter("displayType");
   int pageNo=EJBUtil.stringToNum(request.getParameter("pageNo"));
   int displayDur=EJBUtil.stringToNum(request.getParameter("displayDur"));
   displayDur = displayDur * 7;

%>

<DIV class="browserDefault" id="div1"> 


<%
 if (sessionmaint.isValidSession(tSession))

 {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
    out.println("dur " +duration);
	out.println("disp dur" + displayDur);
	
	String uName = (String) tSession.getValue("userName");
   String acc = (String) tSession.getValue("accountId");
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
//   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
   pageRight=7;

   accountId = EJBUtil.stringToNum(acc);

   if (pageRight > 0 )
	{
%>

<P class = "userName"><%= uName %></p>
<%
   if(mode.equals("N")) {
%>
<P class="sectionHeadings"> <%=LC.L_PcolCal_New%><%--Protocol Calendar >> New*****--%> </P>
<%
   } else if(mode.equals("M")) {
%>
<P class="sectionHeadings"> <%=LC.L_PcolCal_Modify%><%--Protocol Calendar >> Modify*****--%> </P>
<%
   }
%>

<jsp:include page="protocoltabs.jsp" flush="true"> 
</jsp:include>


<Form name="display" method="post" action="selectDOW.jsp?selectedTab=3&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=1">
<select name="displayType">
	<option value="D"><%=LC.L_Days%><%--Days*****--%></option>
	<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
	<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
</select>

<Input type="text" name="displayDur" size=3 value=<%=(displayDur/7)%>> X 7
<button type="submit" border="0"><%=LC.L_Search%></button>
</Form>
<%
	ArrayList eventIds=null; 
	ArrayList protocolIds=null; 
	ArrayList names=null ; 
	ArrayList descriptions= null; 
	ArrayList eventTypes=null; 
	ArrayList eventDurs= null; 
	ArrayList displacements= null; 
	ArrayList orgIds= null; 
	ArrayList costs= null; 
	String eventType="";

    int displacement = 0;

    ctrldao= eventdefB.getAllProtAndEvents(EJBUtil.stringToNum(protocolId));
	eventIds=ctrldao.getEvent_ids() ; 
	protocolIds=ctrldao.getChain_ids() ; 
	names= ctrldao.getNames(); 
	descriptions= ctrldao.getDescriptions(); 
	eventTypes= ctrldao.getEvent_types(); 
	eventDurs= ctrldao.getDurations(); 
	displacements= ctrldao.getDisplacements(); 
	orgIds= ctrldao.getOrg_ids(); 
	costs = ctrldao.getCosts(); 
 %>  	  
<%
	if ((displayType!= null) && displayType.equals("O")) {

		out.println("null");
	}
	else {
 %>


<Form name="selectDOW" method="post" action="saveEventToProto.jsp">

	<%

Integer dur = new Integer(duration);
float flt=dur.intValue();
flt = (float)java.lang.Math.ceil(flt/7);
int weeks=(int)flt;

//number of pages
int pages=(dur.intValue()/displayDur);
%>

<P class="defComments"><%=MC.M_Tot_DurInWeeks%><%--Total Duration in weeks*****--%>: <%=weeks %> </P>

<% for (int i=1;i<=pages;i++) {
	if (i==pageNo) {
%>
	<%=i%>
<%
	}
	else {
%>
	<A href="" onclick="return f_submit()" ><%=i%></A>
<%
	}
}
%>
	<input type="image" src="../images/jpg/Submit.gif" align="absmiddle">
<%
//set weeks to display duration weeks so that in one page only those # of weeks are displayed
  weeks = displayDur/7;
  
  String table_width = weeks*33+"%";
  
  String html="";
  if(weeks<5)
  {
	html = "<P>weeks 1-" + weeks+" |";

  }
  else
  {
	html = "<P>";
	for(int i=1 ;i<=weeks;i+=4)
	{
	  int temp = i+3;
	  if(temp>weeks)
		temp = (int)weeks;		
        html += "weeks";
	  html += i+"-"+temp+ "</a>"+" | ";
	  if(temp<weeks)	
	  html += "<a href= >";	

	}
  }
 %>
<%//=html%>

<P>
<TABLE border=1 cellPadding=1 cellSpacing=1 width=<%=table_width%>>
 <TR>
 <th><%=LC.L_Event%><%--Event*****--%></th>
	 <%for(int j=1;j<=weeks;j++)
	{%>
 	<th><%=LC.L_Week%><%--Week*****--%><%=j%></th>
	<%}%>
 </TR>

<%for(int k=0;k<eventIds.size();k++)  
{  //event loop
	Object[] arguments = {eventIds.size()};
out.println(VelosResourceBundle.getLabelString("L_Event_No",arguments));
	eventType = (String)eventTypes.get(k);
	
	if (eventType.equals("P")) {
		continue;
	}
	
%>	

<INPUT NAME="eventIds" TYPE=hidden  value="<%=eventIds.get(k)%>">
  <%
++rowNo;
if(k%2 == 0)
	{
	%>
      <tr class="browserEvenRow"> 
	  <%
		}
	else {
	  %>
      <tr class="browserOddRow"> 
	   <%
	}
%> 
   <td><%=names.get(k)%></td>
  <TD>
<%for(int m=1;m<=(weeks*7);m++) //week loop
{
	displacement = EJBUtil.stringToNum((String)displacements.get(k));
	if (displacement == m) {
%>
    <INPUT id=checkbox1 name=checkbox<%=rowNo%> type=checkbox checked=true value=<%=m%><%if(m >dur.intValue()){%> disabled = "true"
<% }%>>

    <%
	} else {
	%>
    <INPUT id=checkbox1 name=checkbox<%=rowNo%> type=checkbox value=<%=m%><%if(m >dur.intValue()){%> disabled = "true"
<% }%>>
	
	<%
	}
	if((m%7 == 0) && (m < (weeks*7))) {%>
    </TD>
    <TD>
    <%}%>	 
 <%} //week loop
 %> 
</TR>
<%} //event loop
 %> 
</TABLE></P>
<%}%>
	<INPUT NAME="protocolId" TYPE=hidden value=<%=protocolId%>>
	<INPUT NAME="srcmenu" TYPE=hidden value=<%=src%>>
	<INPUT NAME="calledFrom" TYPE=hidden  value=<%=calledFrom%>>
	<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>
	<INPUT NAME="selectedTab" TYPE=hidden  value=<%=selectedTab%>>
	<INPUT NAME="calStatus" TYPE=hidden  value=<%=calStatus%>>

  <table width="100%" cellspacing="0" cellpadding="0">
      <td align=center> 
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
      </td>
      <td> 
		<button type="submit"><%=LC.L_Next%></button>
      </td> 
      </tr>
  </table>


</Form>
<%
	} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%
 } //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="menus.htm" flush="true"/>
</div>

</body>

</html>
