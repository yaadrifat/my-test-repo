<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Select_Organizations%><%--Select Organizations*****--%></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

function getSiteIds(formobj){
    selIds = new Array(); //array of selected site Ids
	selNames = new Array(); //array of selected site names
	totrows = formobj.totalrows.value;
	checked = false;

	if (totrows==1) {
		if (formobj.chkUserSite.checked) {
			checked=true;
		}else {
			checked=false;
		}
	} else {
		for (i=0;i<totrows;i++) {
			if (formobj.chkUserSite[i].checked) {
				checked=true;
				break;
			} else {
				checked=false;
			}
		}
	}

	if (!checked) {
		alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization.");*****/
		return false;
	}

	if (totrows==1) {
		if (formobj.chkUserSite.checked) {
			selValue = formobj.chkUserSite.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selNames[0] = selValue.substring(pos+1);
		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkUserSite[i].checked) {
				selValue = formobj.chkUserSite[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selNames[j] = selValue.substring(pos+1);
				j++;
			}
		}
	}

if (document.layers) {
    window.opener.document.div1.document.reports.selSiteIds.value = selIds;
    window.opener.document.div1.document.reports.selSiteNames.value = selNames;
} else {
	window.opener.document.reports.selSiteIds.value=selIds;
	window.opener.document.reports.selSiteNames.value=selNames;
}

self.close();
}

</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>


<body style="overflow:auto">
<DIV class="popupDefault" >
<P class="sectionHeadings"> <%=MC.M_RptPatRpt_SelOrg%><%--Reports >> <%=LC.Pat_Patient%> Reports >> Select Organizations*****--%> </P>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	    int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 		int loginUser = EJBUtil.stringToNum((String) tSession.getValue("userId"));
	    ArrayList userSiteIds = null;
	    ArrayList userSiteSiteIds = null;
        ArrayList userSiteNames = null;
        String userSiteId = "";
        String userSiteSiteId = "";
        String userSiteName = "";

	    UserSiteDao userSiteDao = userSiteB.getSitesWithViewRight(accountId,loginUser);
	    userSiteIds = userSiteDao.getUserSiteIds(); //pk_userSite
    	userSiteSiteIds = userSiteDao.getUserSiteSiteIds(); //fk_site
	    userSiteNames = userSiteDao.getUserSiteNames();

	    int len = userSiteIds.size();
%>
  <Form  name="selectSite" method="post" action="" onsubmit="return getSiteIds(document.selectSite)">
    <table width="100%" cellspacing="2" cellpadding="0" border="0" >
      <tr>
		<th width="30%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
        <th width="10%"></th>
      </tr>
<%
for (int counter=0;counter<len;counter++) {
	//pkUserSiteId
     	userSiteId=((userSiteIds.get(counter)) == null)?"-":(userSiteIds.get(counter)).toString();
	//fk_site
     	userSiteSiteId = ((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString();
   		userSiteName = ((userSiteNames.get(counter)) == null)?"-":(userSiteNames.get(counter)).toString();
	if ((counter%2)==0) { %>
     <tr class="browserEvenRow">
  <%}else{  %>
      <tr class="browserOddRow">
   <%}  %>
    	<td width="30%"> <%=userSiteName%></td>
	    <td width="10%">
	  <input type="checkbox" name="chkUserSite" value="<%=userSiteSiteId%>*<%=userSiteName%>" > </td>
    </tr>
<%}%>
<TD><BR></TD>
	<tr>
	<td colspan=2 align="center"><input type="image" src="./images/select.gif" align="absmiddle" border="0"></td>
	</tr>
    </table>

	<Input type="hidden" name="checkedrows" value=0>
	<Input type="hidden" name="totalrows" value=<%=len%> >

</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
</body>

</html>







