<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_FldBwr_FlgSbmt%><%--Field Browser Flag  Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %><%@page import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="fldlibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>



<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
		String studyId= request.getParameter("studyId");
		
	   if(oldESign.equals(eSign)) 
	   {
			
			
			int count=0;
			int lenbrowser=0;
			int pos=0;
			int l=0;
			int j=0;
			int strLen=0;
			int mCount=0;
			int eCount=0;

			String str="";
			String strId="";
			String strTotId="";
			int id=0;
			int tid=0;
			
			ArrayList fieldIds=null;
			int ret=0;
			String mode=request.getParameter("mode");
			String ids=request.getParameter("selFieldIds");
			String notSelIds=request.getParameter("notSelFieldIds");
	
			String calledFrom = request.getParameter("calledFrom");
			String lnkFrom = request.getParameter("lnkFrom");
			String codeStatus = request.getParameter("codeStatus");
			
			if(ids.equals(""))
		   {
				//out.println("inside if 1");
				ids=null;
		   }
		   if(notSelIds.equals(""))
		   {
				//out.println("inside if 2");
				notSelIds=null;
		   }
		
			String formLibId= request.getParameter("formId");
			String recordType="M";

			String modBy=(String) tSession.getAttribute("userId");
			String ipAdd=(String)tSession.getAttribute("ipAdd");
	     	String accountId=(String)tSession.getAttribute("accountId");
			int iModBy= EJBUtil.stringToNum(modBy);
			
			  
			
						
			int iformLibId=EJBUtil.stringToNum(formLibId);
			
			ret = fldlibJB.updateBrowserFlag(ids,notSelIds,iModBy,recordType,ipAdd);
			
		if ( ret == -1 )
		{
%>
  	
<%             %>				
				<br><br><br><br><br><p class = "sectionHeadings" align = center>
	 			<p class = "sectionHeadings" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%></p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		
		<br><br><br><br><br>
		<p class = "sectionHeadings" align = center>
		<%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
			
			
<%
		}//end of else for Error Code Msg Display
		// by salil %>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=fieldBrowser.jsp?formId=<%=formLibId%>&mode=<%=mode%>&flag=0&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>&codeStatus=<%=codeStatus%>"> 
	<%	
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
