<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Pat_Frm %><%-- <%=LC.Pat_Patient%> Form*****--%></title>
<SCRIPT>
function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}

function openQueries(formId,filledFormId) {
	  windowName=window.open("addeditquery.jsp?formId="+formId+"&filledFormId="+filledFormId+"&from=1","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();

}
function openPrintWin(formId,filledFormId,patientId,patientCode,formlibver) {
   windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&pkey="+patientId+"&patientCode="+patientCode+"&linkFrom=P"+"&formLibVerNumber="+formlibver,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
}

//JM: 041306: added newly

function confirmBox(pageRight)
{
	if (f_check_perm(pageRight,'E') == true)
		{
			msg="<%=LC.L_Del_ThisResponse %>";/*msg="Delete this Response?";*****/

			if (confirm(msg))
			{
    			return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
}
//JM: 041306: added newly
function  openReport(formobj,reportId,reportName,repFilledFormId,act,displayMode){

	formobj.action="repRetrieve.jsp?repId="+reportId+"&repName="+reportName+"&repFilledFormId="+repFilledFormId+"&filterType=A";
	if (displayMode != null && displayMode != '') { formobj.action += "&displayMode="+displayMode; }
	formobj.target="_new";
	formobj.submit();
	formobj.action=act;
	formobj.target="";
	}

//Added this method on 1st April for Mahi Enhancement Phase II

function checkPerm(pageRight)
{

	if (f_check_perm(pageRight,'N') == true)
	{
		return true ;

	}
	else
	{
		return false;
	}
}

</SCRIPT>

</head>
 <!--added on 1st April for fixing the Mahi Enhancement Phase II -->
<%
	CodeDao cdao = new CodeDao();
	ArrayList arrSybType = cdao.getCSubType();

	CodeDao cdaoRespStatus = new CodeDao();
	int completeStatus = 0;
	completeStatus = cdaoRespStatus.getCodeId("fillformstat", "complete");

	String ignoreRights = "";
	String eSignRequired = "";

	String formFillDt1 = request.getParameter("formFillDt");
	String selectedTab1 = request.getParameter("selectedTab");
	int pageRight1=0;
	String formId = request.getParameter("formId");
	////////
	String src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
	String patientId = request.getParameter("pkey");
	String entryChar = request.getParameter("entryChar");
	String calledFromForm = "";
    calledFromForm = request.getParameter("calledFromForm");

    //Added on 1st April for fixing the Mahi Enhancement Phase II

	String 	 studyId=request.getParameter("studyId");
	String 	 statDesc=request.getParameter("statDesc");
	String 	 statid=request.getParameter("statid");
	String 	 patProtId=request.getParameter("patProtId");
	String 	 mode=request.getParameter("mode");
	String 	 pkey=request.getParameter("pkey");
	String fldMode = request.getParameter("fldMode");
    String formLibVer = request.getParameter("formLibVer");

  String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	   outputTarget = request.getParameter("outputTarget");

	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }


    if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

    String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }

	if (calledFromForm.equals(""))
   {

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>

 	<%
 }
 %>

<script>
	checkQuote="N";
	</script>

<%@page language = "java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.CodeDao,com.velos.eres.web.user.UserJB,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformsB"scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" /> <!--KM-->



<body style="overflow:auto;">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
int formRights=0;
int personPK = EJBUtil.stringToNum(patientId);
personB.setPersonPKId(personPK);
personB.getPersonDetails();
String patientCode = personB.getPersonPId();
patientCode = (patientCode==null)?"":patientCode;

patientCode = StringUtil.encodeString(patientCode);

String patientDOB=personB.getPersonDob();
patientDOB = (patientDOB==null)?"":patientDOB;

if (calledFromForm.equals(""))
  {
%>

<div class="BrowserTopn" id="div1">
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=patientId%>"/>
<jsp:param name="patientCode" value="<%=patientCode%>"/>
<jsp:param name="page" value="patient"/>
</jsp:include>
</div>

<div class="tabFormBotN_PF_2 tabFormBotN_add BrowserBotN" id="div2">
<%
   }else{%>
<div>
<%}%>

<%
HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
    if (studyId == null || studyId.equals("null")){
    	tSession.setAttribute("studyIdForFormQueries","");	
    }else{
    	tSession.setAttribute("studyIdForFormQueries",studyId);
    }
 	ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;

    int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

	if (grpRights != null)
	{
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
		formRights=Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
	}

	String userId = (String) tSession.getValue("userId");

	int patId;
	SaveFormDao saveDao = new SaveFormDao();
	String statSubType = "";
	patId=EJBUtil.stringToNum(request.getParameter("pkey"));


	String findStr = "";
	int len = 0;
	int pos = 0;
    StringBuffer sbuffer = null;

    //KM-2387
 	int orgRight  = 0;
		 orgRight = userSite.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), patId);

    if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
		}
		System.out.println("ignoreRights" + ignoreRights);

		if (ignoreRights.equals("true") || (! StringUtil.isEmpty(outputTarget) ) )
		{
			pageRight=7;
			formRights=7;
			orgRight = 7;
		}

		session.setAttribute("formQueryRight",""+pageRight);
		
		//Bug#6638
		String accessStatus = "";
		if(null!=grpRights ){
			if(isAccessibleFor(pageRight, 'E')){
				accessStatus="E";
			}else
			if(isAccessibleFor(pageRight, 'N') || isAccessibleFor(pageRight, 'V')){
				accessStatus="V";
			}
			
		}
    	%>

	<!--Added on 1st April for fixing the Mahi Enhancement Phase II, formname dropdown -->

<Form method="post" id="patForm" name="mypatform" action="formfilledpatbrowser.jsp" onsubmit="">
     		<input type="hidden" name="srcmenu" value=<%=src%>>
     		<input type="hidden" name="selectedTab" value="11">
     		<input type="hidden" name="studyId" value=<%=studyId%>>
     		<input type="hidden" name="statDesc" value=<%=statDesc%>>
     		<input type="hidden" name="statid" value=<%=statid%>>
     		<input type="hidden" name="patProtId" value=<%=patProtId%>>
     		<input type="hidden" name="patientCode" value=<%=patientCode%>>
     		<input type="hidden" name="mode" value=<%=mode%>>
     		<input type="hidden" name="pkey" value=<%=pkey%>>
     		<input type="hidden" id="formRights" value=<%=formRights%>>

 <%  
	int filledFormUserAccessInt = lnkformsB.getFilledFormUserAccess(
    		StringUtil.stringToNum(request.getParameter("filledFormId")),
    		StringUtil.stringToNum(request.getParameter("formId")),
    		StringUtil.stringToNum((String)tSession.getAttribute("userId")) );
 
    if ( pageRight >=4 && filledFormUserAccessInt > 0 )
    {


//Added on 1st April for Mahi Enhancement Phase II formname dropdown



	String src1= request.getParameter("srcmenu");
    calledFromForm = request.getParameter("calledFromForm");
	ArrayList arrFrmNames = null;
	ArrayList arrNumEntries = null;
 	ArrayList arrFrmIds = null;
	ArrayList arrEntryChar = null;
	ArrayList arrFrmInfo = new ArrayList();
	String frmName1 = "";
//		 String entryChar1="";
	 String numEntries = "";
	 String frmInfo= "";
	 int formId1 = 0;
	 String firstFormInfo = "";
	 String lfDispInPat = "";

//JM: added 122206

	FormLibDao fd = new FormLibDao();
   	if (mode.equals("N"))
   	{

			formLibVer = fd.getLatestFormLibVersionPK(EJBUtil.stringToNum(formId));

	}

		//end added

	String verNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));




     String strFormId = request.getParameter("formPullDown");
 	 String accId=(String)tSession.getAttribute("accountId");
	 int iaccId=EJBUtil.stringToNum(accId);

     UserJB userB = (UserJB) tSession.getValue("currentUser");
     String siteId = userB.getUserSiteId();


	 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
	 lnkFrmDao = lnkformsB.getPatientForms(iaccId,patId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));

		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();

		 if (strFormId==null) {
		    if (arrFrmIds.size() > 0) {
       		 	formId1 = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
       			entryChar = arrEntryChar.get(0).toString();
       			numEntries = arrNumEntries.get(0).toString();
       			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
			} else {
				  firstFormInfo="lab";
			}
		 }
		 else if (strFormId.equals("lab")) {
		 	 firstFormInfo="lab";
	     }
		 else
		 {
		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		  if (calledFromForm.equals(""))
    	 		 {
		 	     formId1 = EJBUtil.stringToNum(strTokenizer.nextToken());
				 entryChar = strTokenizer.nextToken();
			     numEntries = strTokenizer.nextToken();
				 firstFormInfo = strFormId;
				}
				else
    			 {
    			 	formId1 = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();

    			 	lnkformsB.findByFormId(formId1 );
		   		 	numEntries = lnkformsB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId1 + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;
    			 }
		 }

		 for (int i=0;i<arrFrmIds.size();i++)
		 {  //store the formId, entryChar and num Entries separated with a *
		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
		 	 arrFrmInfo.add(frmInfo);
		 }

		 if (formId1 > 0) {
		 	lnkformsB.findByFormId(formId1);
		 	lfDispInPat = lnkformsB.getLfDispInPat();
		 }

	 	//to add lab entry in the DD
	 	 arrFrmInfo.add("lab");
		 arrFrmNames.add(LC.L_Lab);/* arrFrmNames.add("Lab");*****/

	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
		 StringBuffer formBuffer = new StringBuffer(dformPullDown);
		  if (calledFromForm.equals(""))
		  {
		  %>

			<table width="98%" border="0" >	<tr bgcolor="#dcdcdc">
			<td width="60%" align="right"><%=LC.L_Jump_ToForm %><%-- Jump to Form*****--%>: <%=formBuffer%></td><td>
			<a href="#" onclick="javascript:fn_nextpage(document.mypatform.formPullDown.value) ;">
			<button type="submit"><%=LC.L_Go%></button>
			</A>
			</td>
			</tr>
					<tr height="5"><td></td></tr>
				</table>
		<% } %>
		<script>
				function fn_nextpage(val)
		{
				 window.location.href="patformdetails.jsp?srcmenu=<%=src1%>&selectedTab=<%=selectedTab1%>&formId=<%=formId1%>&mode=N&formDispLocation=PA&pkey=<%=patId%>&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt1%>&formPullDown=<%=firstFormInfo%>&calledFromForm=<%=calledFromForm%>"  ;

				 document.mypatform.submit();
		}
		</script>


<%		 if (calledFromForm.equals(""))
		 {
			 formBuffer.replace(0,7,"<SELECT onChange=\"document.mypatform.submit();\"");
		 }


          formLibB.setFormLibId(EJBUtil.stringToNum(formId));
		  formLibB.getFormLibDetails();



		  String formStatuss = formLibB.getFormLibStatus();
	      cdao.getCodeValuesById(EJBUtil.stringToNum(formStatuss));
	       statSubType = arrSybType.get(0).toString();

		  %>
		  <%


          String filledFormId = request.getParameter("filledFormId");
		  String formFillDt = request.getParameter("formFillDt");
		  String formPullDown = request.getParameter("formPullDown");
     //     String formLibVer = request.getParameter("formLibVer");

          String formHtml = "";

          formLibB.setFormLibId(EJBUtil.stringToNum(formId));
		  formLibB.getFormLibDetails();
		  String formName=formLibB.getFormLibName();
		  String formStatus = formLibB.getFormLibStatus();

	       eSignRequired =formLibB.getEsignRequired();

	        if (StringUtil.isEmpty(eSignRequired))
		   {
		   		eSignRequired = "0";
		   }


			  statSubType = arrSybType.get(0).toString();

		  %>

	<%if (ignoreRights.equals("false"))
	 {
     %>
		<table width="98%" border="0">
		<tr bgcolor="#dcdcdc">
			 <td width="40%"><!--KM-to fix the Bug 2809 -->
			 	 <b> <%=LC.L_Open_FormName %><%-- Open Form Name*****--%>: </b><!--KM-to fix the Bug 2809 -->
			 	 <%=formName%>&nbsp;&nbsp;&nbsp;&nbsp;
			 </td>
			  <%if (mode.equals("M")){%>
			 <td width="10%">
		 		<A onclick="openQueries(<%=formId%>,<%=filledFormId%>)" href=#> <%=LC.L_AddOrEdit_Queries %><%-- Add/Edit Queries*****--%></A>
			 </td>
			 <%}%>
			 <td width="10%">
		 		<A onclick="openPrintWin(<%=formId%>,<%=filledFormId%>,<%=patientId%>,'<%=patientCode%>', '<%=verNumber%>')" href=#><img src="./images/printer.gif" border="0" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>"></img></A>
			 </td>
			 <!--//JM: 041306: Bugzilla issue #2414: Delete and Audit links were missing for Only Once (Editable) Forms.
			 //JM: 042506: modified -->
			 <%
			if (entryChar.equals("E") && (!StringUtil.isEmpty(filledFormId)) ){%>
			 <td width="10%">
				<A href="#" onclick="openReport(document.mypatform,'99','Form Audit Trail','<%=filledFormId%>','formfilledpatbrowser.jsp?srcmenu=tdMenuBarItem5&page=1','')" title="<%=LC.L_ViewAud_Trial%><%--View Audit Trail*****--%>"><%=LC.L_Audit %><%-- Audit*****--%></A>&nbsp;
			</td>
			 <td width="10%">
		 		<A href="#" onclick="openReport(document.mypatform,'99','Track Changes','<%=filledFormId%>','formfilledpatbrowser.jsp?srcmenu=tdMenuBarItem5&page=1','<%=accessStatus%>')" title="<%=LC.L_Track_Changes%><%--Track Changes*****--%>"><%=LC.L_Track_Changes %><%-- Track Changes*****--%></A>&nbsp;
			</td>
			<td width="10%">
				<A onclick="return confirmBox(<%=pageRight%>);" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&pkey=<%=personPK%>&formDispLocation=PA&filledFormId=<%=filledFormId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&outputTarget=<%=outputTarget%>"> <img src="./images/delete.gif" border="0" align="left"/></A>
			</td>
			<%}%>
		</tr>
 		</table>
 	<% } //ignore rights
 	%>

<%		 if (calledFromForm.equals(""))
		 {%>

		 <%}%>
		</form>

<%		 if (calledFromForm.equals(""))
		 {%>

		 <%}


          if (mode.equals("N"))
           {
			formHtml =  lnkformsB.getFormHtmlforPreview(EJBUtil.stringToNum(formId));
           }
           else
           {

			 saveDao =  lnkformsB.getFilledFormHtml(EJBUtil.stringToNum(filledFormId),"PA","Y");
				formHtml =  saveDao.getHtmlStr();
           }


        //Replace Submit image with button
  		findStr ="<input id=\"submit_btn\" border=\"0\" align=\"absmiddle\" src=\"../images/jpg/Submit.gif\" type=\"image\">";
  	    len = findStr.length();
  	    pos = formHtml.lastIndexOf(findStr);

  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			if ( (orgRight < 6) || (pageRight == 4) || (pageRight < 6 && mode.equals("M"))  || (statSubType.equals("L"))
					|| (saveDao.isLocked()) || (pageRight != 5 && pageRight != 7 && mode.equals("N")) ) {
  		    	sbuffer.replace(pos,pos+len,"");
  		    }else{
  		    	sbuffer.replace(pos,pos+len,"<button id=\"submit_btn\" type=\"submit\">"+LC.L_Submit+"</button>");
  		    }
  		    formHtml = sbuffer.toString();
  	    }
  	    
        //in case of view right, remove the submit button
		//in case of lockdown status, remove the submit button


		if ( (orgRight < 6) || (pageRight == 4) || (pageRight < 6 && mode.equals("M"))
				|| (statSubType.equals("L")) || (saveDao.isLocked()) || (pageRight != 5 && pageRight != 7 && mode.equals("N"))) {

       		 // Disable the Form tag for issue 4632
		     formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
			   .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
			   .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");


			 //Added by Manimaran for the issue 2941
			 sbuffer = new StringBuffer(formHtml);
			 sbuffer.append("<table><tr><td>"+VelosResourceBundle.getLabelString("L_Frm_VerNmber"/*Form Version Number*****/,verNumber)+"</td></tr></table>");
			 formHtml = sbuffer.toString();



       	}  else {
		   findStr = "</Form>";

		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
		   StringBuffer paramBuffer = new StringBuffer();
		      //  by salil on 13 oct 2003 to see if the check box type multiple choice exists
     	  //  or not and to handle the case
	     // when no option is selected in a check box type multiple choice the element name does not
	  	// be  carried forward to the next page hence the "chkexists"  variable is used to see
		// if the page has a multiple choice field or not .

		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }

		    paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='PA'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formPatient\" value='"+patientId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formFillMode\" value='"+mode+"'/>");
		    paramBuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"srcmenu\" value='"+src+"'/>");
		   	paramBuffer.append("<input type=\"hidden\" name=\"selectedTab\" value='"+selectedTab+"'/>");
		   	paramBuffer.append("<input type=\"hidden\" name=\"formFillDt\" value='"+formFillDt+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formPullDown\" value='"+formPullDown+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"calledFromForm\" value='"+calledFromForm+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"fldMode\" value='"+fldMode+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"patientDOB\" value='"+patientDOB+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"ignoreRights\" value='"+ignoreRights+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"outputTarget\" value='"+outputTarget+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"+specimenPk+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"eSignRequired\" value='"+eSignRequired+"'/>");

			if (ignoreRights.equals("false"))
	 		{
				paramBuffer.append("<br><table><tr><td>"+VelosResourceBundle.getLabelString("L_Frm_VerNmber"/*Form Version Number*****/,verNumber)+"</td></tr></table>");
			}
			else
			{
				paramBuffer.append("<input type=\"hidden\" name=\"completeStatus\" value='"+completeStatus+"'/>");
			}

			if (!mode.equals("N"))
		    {
		    	paramBuffer.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"+filledFormId+"'/>");
		    	paramBuffer.append("<input type=\"hidden\" name=\"formLibVer\" value='"+formLibVer+"'/>");

			}
			//also append the paramBuffer
		    sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/>"+ paramBuffer.toString() + " </Form>");
		    formHtml = sbuffer.toString();
		}

       %>

       <%=formHtml%>
       <%
	}// end of if for page right
	else
	{

	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	  <%

	} //end of else body for page right

  } //end of if for session

 else
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div>
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
 <%if (calledFromForm.equals(""))
   {
%>

	</div>

	<DIV class="mainMenu" id = "emenu">
	  <jsp:include page="getmenu.jsp" flush="true"/>
	</DIV>
<%
	}
%>
<script>
elementOverride=(document.getElementById("override_count"));
if (!(typeof(elementOverride)=="undefined") && (elementOverride!=null) ) {
if ((document.getElementById("override_count").value)>0) setDisableSubmit('false');
}

<% if (ignoreRights.equals("true"))
	{
%>

	elemeSign = (document.getElementById("eSign"));
	elemeSignLabel = (document.getElementById("eSignLabel"));

	if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
		elemeSign.style.visibility = "hidden";
		elemeSign.value="11";
	}

	if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

		elemeSignLabel.style.visibility = "hidden";
	}

	compStatus = document.forms['er_fillform1'].completeStatus.value;
	formFillMode = document.forms['er_fillform1'].formFillMode.value;

	//set override count to 0 so that query window does not open
	document.forms['er_fillform1'].override_count.value = 0;

	if (formFillMode == 'N')
	{
		document.forms['er_fillform1'].er_def_formstat.value = compStatus;
	}

	elem_def_formstat = (document.getElementById("er_def_formstat"));
	elem_def_form_status_label = (document.getElementById("def_form_status_label"));

	if (!(typeof(elem_def_formstat)=="undefined") && (elem_def_formstat!=null) )
	{
		elem_def_formstat.style.visibility = "hidden";

	}

	if (!(typeof(elem_def_form_status_label)=="undefined") && (elem_def_form_status_label!=null) )
	{
		elem_def_form_status_label.style.visibility = "hidden";

	}



	linkFormSubmit('fillform');
<% }

if (eSignRequired.equals("0") && (!ignoreRights.equals("true")) )
{
  %>

	  	elemeSign = (document.getElementById("eSign"));
		elemeSignLabel = (document.getElementById("eSignLabel"));

		if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
			elemeSign.style.visibility = "hidden";
			elemeSign.value="11";
		}

		if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

			elemeSignLabel.style.visibility = "hidden";
		}
  <%

}


%>


</script>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>