<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%--<TITLE> Registration Complete </TITLE>*****--%>
<TITLE> <%=LC.L_Reg_Complete%></TITLE>
</HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">
<jsp:useBean id="signupB" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/><!--km-->
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.lang.*,java.util.*,java.text.*"%>



<%--
	com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*
--%>
<%

	String userFirstName = request.getParameter("userFirstName");
	userFirstName = userFirstName.trim();

	String userLastName = request.getParameter("userLastName");
	userLastName = userLastName.trim();


	String addPriUser = request.getParameter("addPriUser");

	if(EJBUtil.isEmpty(addPriUser))
		addPriUser = "";


	String addCityUser = request.getParameter("addCityUser");

	if(EJBUtil.isEmpty(addCityUser))
	{
		addCityUser = "";
	}
	else
	{
		addCityUser = 	","  + addCityUser;
	}

	String addStateUser = request.getParameter("addStateUser");

	if(EJBUtil.isEmpty(addStateUser))
		addStateUser = "";


	String addCountryUser = request.getParameter("addCountryUser");

	if(EJBUtil.isEmpty(addCountryUser))
		addCountryUser = "";


	String addZipUser = request.getParameter("addZipUser");
	String addPhoneUser = request.getParameter("addPhoneUser");
	String addEmailUser = request.getParameter("addEmailUser");
	String accType = request.getParameter("accType");
	String accRoleType = request.getParameter("roleType");
	String userCodelstJobtype = request.getParameter("userCodelstJobtype");
	String primarySpeciality = request.getParameter("primarySpeciality");
	String userWrkExp = request.getParameter("userWrkExp");
	String userPhaseInv = request.getParameter("userPhaseInv");
	String siteName = request.getParameter("siteName");
	String sitePerAdd = request.getParameter("sitePerAdd");
	String addCitySite = request.getParameter("addCitySite");
	String addStateSite = request.getParameter("addStateSite");
	String addCountrySite = request.getParameter("addCountrySite");
	String addZipSite = request.getParameter("addZipSite");
	String addPhoneSite = request.getParameter("addPhoneSite");
	String userLogin = request.getParameter("userLogin");

	userLogin = userLogin.trim();

	String userPwd = request.getParameter("userPwd");
	String userConfirmPwd = request.getParameter("userConfirmPwd");
	String userESign = request.getParameter("userESign");
	String timeZone = request.getParameter("timeZone");
	String userConfirmESign = request.getParameter("userConfirmESign");

	String ipAdd = request.getRemoteAddr();

	String userSecQues = request.getParameter("userSecQues");
	String userAnswer = request.getParameter("userAnswer");
	String accMailFlag = request.getParameter("accMailFlag");
	String accPubFlag = request.getParameter("accPubFlag");
	Configuration conf = new Configuration();
	String msgcntrToUserId = conf.getVelosAdminId(conf.ERES_HOME +"eresearch.xml");
	String userStatus = "P";
	String groupName = conf.getDefaultGroup(conf.ERES_HOME +"eresearch.xml");
	String groupDesc = conf.getDefaultDesc(conf.ERES_HOME +"eresearch.xml");

	//JM: 11MAR2009:
	 
	String accStartDate = DateUtil.dateToString(new Date());

	String accStatus = "P";
	String msgcntrStatus = "U";

	String domain = "";
	String messageTo = "";
	String userEmail = "";
	String messageFrom = "";
	String messageText = "";
	String messageSubject = "";
	String messageHeader = "";
	String messageFooter = "";
	String completeMessage = "";
	String accountType="";

	 String newUserMailStatus = "";
	 String mailSentMessage = "";
	 int metaTime = 1;

	int ctrlrows = 0;
	int rows;

	String defModules = "";

	ctrl.getControlValues("module");
	ctrlrows = ctrl.getCRows();

	for(int count=0;count<ctrlrows;count++)
	{
        	defModules = defModules +"0";
    }



	int count = userB.getCount(userLogin);
        if(count > 0) {
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "sectionHeadings" align = center> <%--The Login ID you have entered already
  exists. Please enter a new Login Id.*****--%><%=MC.M_LoginIdExst_EtrNew%> </p>
<form >
  <table align=center>
    <tr width="100%">
      <td width="100%" >
        <input type="button" name="return" value="<%=LC.L_Return%><%--Return*****--%>" onClick="window.history.back()">
      </td>
    </tr>
  </table>
</form>
<%
	}
	else {
	if(accMailFlag == null)
		accMailFlag="N" ;

	if(accPubFlag==null)
		accPubFlag="N" ;

	if(userCodelstJobtype.equals(""))
		userCodelstJobtype = null;

	if(primarySpeciality.equals(""))
		primarySpeciality = null;

	signupB.setUserFirstName(userFirstName);
	signupB.setUserLastName(userLastName);
	signupB.setAddPhoneUser(addPhoneUser);
	signupB.setAddEmailUser(addEmailUser);
	signupB.setAccType(accType);
	signupB.setUserCodelstJobtype(userCodelstJobtype);
	signupB.setUserCodelstSpl(primarySpeciality);
	signupB.setUserWrkExp(userWrkExp);
	signupB.setUserPhaseInv(userPhaseInv);
	signupB.setUserTimeZoneId(timeZone);
	signupB.setSiteName(siteName);
	signupB.setAddPriSite(sitePerAdd);
	signupB.setAddCitySite(addCitySite);
	signupB.setAddStateSite(addStateSite);
	signupB.setAddCountrySite(addCountrySite);
	signupB.setAddZipSite(addZipSite);
	signupB.setAddPhoneSite(addPhoneSite);
	signupB.setUserSecQues(userSecQues);
	signupB.setUserAnswer(userAnswer);
	signupB.setAccMailFlag(accMailFlag);
	signupB.setAccPubFlag(accPubFlag);
	signupB.setMsgcntrToUserId(msgcntrToUserId);
	signupB.setUserStatus(userStatus);
	signupB.setGroupName(groupName);
	signupB.setGroupDesc(groupDesc);

	signupB.setAccStatus(accStatus);
//	signupB.setAccStartDate(accStartDate);
	signupB.setUserSessionTime("15");
	signupB.setUserLoginName(userLogin);
	signupB.setUserPwd(Security.encryptSHA(userPwd));
	signupB.setUserESign(userESign);
	signupB.setUserType("S");
	signupB.setIpAdd(ipAdd);
	Date d = new Date();
	//JM: 12MAR2009:
	 
	d.setDate(d.getDate() + 365);

	//JM: 12MAR2009:
	 
	signupB.setUserPwdExpiryDate(DateUtil.dateToString(d));

	signupB.setUserPwdExpiryDays("365");
	signupB.setUserPwdReminderDate(DateUtil.getFormattedDateString("9999","01","01"));

	//JM: 12MAR2009:
	//signupB.setUserESignExpiryDate(sdf.format(d));
	signupB.setUserESignExpiryDate(DateUtil.dateToString(d));

	signupB.setUserESignExpiryDays("365");
	signupB.setUserESignReminderDate(DateUtil.getFormattedDateString("9999","01","01"));


	signupB.setMsgcntrStatus(msgcntrStatus);

	signupB.setAccModRight(defModules);

///////////////////////////////////////////////Saving account Information
	int accountId= signupB.createAccount();
	String strAccId=accountId+"";

	/* Added for July-August'06 Enhancement (U2) - Default account level settings for User / Patient Timezone, Session Timeout time, Number of days password will expire, Number of days e-Sign will expire. When new account is created, 10 Rows will be inserted by default (with blank settings)
	*/
	if (accountId > 0) {
		SettingsDao settingsDao=new SettingsDao();
		//set the keyword and the value and module information to the settingDao Object
		settingsDao.setSettingKeyword("ACC_USER_TZ");
		settingsDao.setSettingValue(timeZone);
		settingsDao.setSettingKeyword("ACC_USER_TZ_OVERRIDE");
		settingsDao.setSettingValue("1");
		settingsDao.setSettingKeyword("ACC_PAT_TZ");
		settingsDao.setSettingValue(timeZone);
		settingsDao.setSettingKeyword("ACC_PAT_TZ_OVERRIDE");
		settingsDao.setSettingValue("1");
		settingsDao.setSettingKeyword("ACC_SESSION_TIMEOUT");
		settingsDao.setSettingValue("15");
		settingsDao.setSettingKeyword("ACC_SESSION_TIMEOUT_OVERRIDE");
		settingsDao.setSettingValue("1");
		settingsDao.setSettingKeyword("ACC_DAYS_PWD_EXP");
		settingsDao.setSettingValue("365");
		settingsDao.setSettingKeyword("ACC_DAYS_PWD_EXP_OVERRIDE");
		settingsDao.setSettingValue("1");
		settingsDao.setSettingKeyword("ACC_DAYS_ESIGN_EXP");
		settingsDao.setSettingValue("365");
		settingsDao.setSettingKeyword("ACC_DAYS_ESIGN_EXP_OVERRIDE");
		settingsDao.setSettingValue("1");

		for(int i=0;i<=9;i++)
		{
			settingsDao.setSettingsModNum(strAccId);
			settingsDao.setSettingsModName("1");
		}
		commonB.insertSettings(settingsDao);

		// GM 07/17/08 to send an email to the registered user with unencrypted password information

		// get site URL
	  	int ctrlTabRows = 0;
		String siteUrl="";
		CtrlDao urlCtrl = new CtrlDao();
		urlCtrl.getControlValues("site_url");
		ctrlTabRows = urlCtrl.getCRows();
   		if (ctrlTabRows > 0)
	   	{
	   		siteUrl = (String) urlCtrl.getCValue().get(0);
	   	}
		// End

		//get user email and support email
		messageTo = addEmailUser;
    	CtrlDao userCtrl = new CtrlDao();
    	userCtrl.getControlValues("eresuser");
    	rows = userCtrl.getCRows();
    	if (rows > 0)
    	   {
    	   	messageFrom = (String) userCtrl.getCDesc().get(0);
    	   }

		/*messageSubject = "Your New Velos eResearch Account";*****/
		messageSubject = MC.M_YourNew_VeEresAcc;
    	/*messageHeader ="\nDear New Velos eResearch Member," ;*****/
		messageHeader =MC.M_DearNewVelos_EresMbr;
    	/*messageText = "\n\nWelcome to Velos eResearch. Your request for an account is under review. You will receive an activation notice, when approved. The account information that you signed up with is displayed below:\n\nLogin ID : " + userLogin + "\nPassword        : " + userPwd + "\ne-Signature     : " + userESign + "\nApplication URL : " + siteUrl + "\n\n\nThank you for your interest in Velos eResearch.\n\nVelos eResearch Customer Support.\n\nVelos, Inc.\n2201 Walnut Avenue, Suite 208\nFremont, CA. 94538" ;*****/
    	Object[] arguments = {userLogin,userPwd,userESign,siteUrl};
		messageText = VelosResourceBundle.getMessageString("M_WlcmReqApp_AccEresCust",arguments);
    	/*messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;*****/
		messageFooter =MC.M_EmailDocuNotify_SndrDelOrg;
    	completeMessage = messageHeader  + messageText + messageFooter ;
    	try{
       	   	VMailer vm = new VMailer();
        	VMailMessage msgObj = new VMailMessage();

    		msgObj.setMessageFrom(messageFrom);
    		msgObj.setMessageFromDescription(MC.M_VeResCustSrv);
    		msgObj.setMessageTo(messageTo);
    		msgObj.setMessageSubject(messageSubject);
    		msgObj.setMessageSentDate(new Date());
    		msgObj.setMessageText(completeMessage);

    		vm.setVMailMessage(msgObj);
    		newUserMailStatus = vm.sendMail();

    	  }
    	   catch(Exception ex)
            {
         		  newUserMailStatus = ex.toString();
            }

	 	// end send email to user
	}


//////////////code for sending the mail to admin
	if(accType.equals("I")){
	/*accountType="Individual";*****/
	accountType=LC.L_Individual;}
	else if(accType.equals("G")){
	/*accountType="Group";*****/
	accountType=LC.L_Group;}else{
	/*accountType="Evaluation";*****/
	accountType=LC.L_Evaluation;}


//	CtrlDao ctrl=new CtrlDao();

	messageTo = userEmail;
	CtrlDao userCtrl = new CtrlDao();
	userCtrl.getControlValues("eresuser");
	rows = userCtrl.getCRows();
	if (rows > 0)
	   {
	   	messageFrom = (String) userCtrl.getCDesc().get(0);
	   }

	/*messageSubject = "New User Sign Up";*****/
	messageSubject = MC.M_New_UserSignUp;
	/*messageHeader ="\n User Registration Info: \n\n" ;*****/
	messageHeader =MC.M_NewReg_Info ;

	/*messageText = "A New User has Just Signed up with Velos eResearch.\nHere are the brief details.\n\n Name               : " + userLastName +"," + userFirstName  +  "\n Address           : " + addPriUser +  addCityUser + "\n State                : " + addStateUser + "\n Country            : " + addCountryUser + "\n Phone No         : " + addPhoneUser + "\n E-mail  	            : "+ addEmailUser + "\n Organisation     : " + siteName + "\n Account Type  : " + accountType+ "\n\n\n To visit Velos eresearch  click     http://www.veloseresearch.com \n\n To go to Velos admin page click  http://www.veloseresearch.com/eres/jsp/veloshome.jsp";*****/
	Object[] arguments1 = {userLastName,userFirstName,addPriUser,addCityUser,addStateUser,addCountryUser,addPhoneUser,addEmailUser,siteName,accountType};
	messageText = VelosResourceBundle.getMessageString("M_NewUsrCnct_AdmnPgWww",arguments1);
	completeMessage = messageHeader  + messageText + messageFooter ;


	try{
   	   	VMailer vm = new VMailer();
    	VMailMessage msgObj = new VMailMessage();

		msgObj.setMessageFrom(messageFrom);
		/*msgObj.setMessageFromDescription("Velos eResearch Customer Services");*****/
		msgObj.setMessageFromDescription(MC.M_VeResCustSrv);
		msgObj.setMessageTo(messageFrom);
		msgObj.setMessageSubject(messageSubject);
		msgObj.setMessageSentDate(new Date());
		msgObj.setMessageText(completeMessage);

		vm.setVMailMessage(msgObj);
		newUserMailStatus = vm.sendMail();

	  }
	   catch(Exception ex)
        {
     		  newUserMailStatus = ex.toString();
        }
/////////////////////////////////////////////////////// end of mail data

%>



	<table border="0" cellspacing="0" cellpadding="0" width="770">

	 <!-- HEADER OBJECT START-->
	<tr>
	  <td colspan="14"><a href="http://www.velos.com"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres %><%-- Velos eResearch*****--%>" border="0"></a></td>
	</tr>
	 </table>
	 <!-- HEADER OBJECT END -->



	   <table width="770" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
	   		  <tr>

		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">
		      </td>
		   	   <!-- LIBRARY OBJECT END-->

			   <td>
			   <table cellspacing="0" cellpadding="5">
			   <tr>
			  	  <td width="125" valign="top" background="./images/leftsidebg.gif">
				  </td>
			  	  <td align="center" width="750">
				   	 <br><br><br><br><br><br><br><br><br><br><b>

    				  <%
    				  if ( EJBUtil.isEmpty(newUserMailStatus) )
    				  {
    				  	metaTime = 3;
    				  %>
						 <p class = "sectionHeadings" >
					  	 <%--Your registration has been submitted successfully. After verification, Velos will activate your account and send you a notification by email. Thank you for your interest in Velos eResearch.*****--%>
						 <%=MC.M_RegSbmtVerify_AccActv%>
						 </p>

					 <%
					 } else
					 {
					  	metaTime = 10;
    				  %>
						 <p class = "redMessage" >
					  	   <%--Your registration has been submitted successfully but there was an error in sending notification to Velos<br>
						   Please contact Customer Support with the following message:*****--%><%=MC.M_OnErr_ContSupport%> <BR> <%=newUserMailStatus%>.
						  <BR>
						  <%--After verification, Velos will activate your account and send you a notification by email. Thank you for your interest in Velos eResearch.*****--%>
						  <%=MC.M_RegSbmtVerify_AccActv%>.
						  </p>

					 <%

					 }
					 %>

					 <br><br><br><br><br><br><br><br><br><br><br>



		   	   		<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=http://www.velos.com">

				  </td>
			   </tr>
				  </table>
				  </td>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  </tr>
	   </table>






<%
}
%>
</body>
</HTML>

