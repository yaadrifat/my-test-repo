<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studymod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyTeam" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<%
String mode="N";
String selclass;
String studyId="";
String study="";
String studyNo="";
String verNumber = ""  ;
String userId = "";
String studyFromSession = "";
String userIdFromSession = "";
String studyNumber = "";

String tab= request.getParameter("selectedTab");
String from= request.getParameter("from");
mode= request.getParameter("mode");
HttpSession tSession = request.getSession(true);
String acc = (String) tSession.getValue("accountId");

String hideRedmessage = (request.getParameter("hideRedmessage")== null ) ? "0" : "1";
if (sessionmaint.isValidSession(tSession))
{
	tSession.removeAttribute("submissionType");

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	int grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	//int bgtGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));
	//for old data, we may get a '6' where budget 'edit' was allocated before,

	/*if	 (bgtGrpRight == 6)
	{
		bgtGrpRight = 4; //make it only view right	as there is no edit right now
	}
	if (bgtGrpRight == 5)
	{
		bgtGrpRight = 7;
	}*/

	int saveAstemplateRight = 0; //in new mode no right for template

	//end of change for old data

	study = (String) tSession.getValue("studyId");

	studyFromSession = (String) tSession.getValue("studyId");
	userIdFromSession= (String) tSession.getValue("userId");

	studyId = request.getParameter("studyId");

	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	studyNo = (String) tSession.getValue("studyNo");
	verNumber = request.getParameter("verNumber");
	userId = (String) tSession.getValue("userId");

	if(StringUtil.isEmpty(studyId) || (studyId.trim()).equals("null")){
		studyId = studyFromSession;
	}

	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "study_tab");

	if (EJBUtil.stringToNum(studyId) > 0) mode="M";

if (mode == null){
 	mode = "N";
 	stdRights = new StudyRightsJB();
}else if (mode.equals("M")){

	//System.out.println("in tabs" + studyId +"*");

	if (StringUtil.isEmpty(studyFromSession))
	{
	 studyFromSession = "";

	}
	if (StringUtil.isEmpty(studyId))
	{
	 studyId = "";

	}

	if (! studyId.equals(studyFromSession) || stdRights==null)
	{
	    //use the one from parameter and set it in session


	    tSession.setAttribute("studyId",studyId);
		studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();
		tSession.setAttribute("studyNo",studyNumber);
		studyNo = studyNumber;

		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));

		ArrayList tId ;
		tId = new ArrayList();
		tId = teamDao.getTeamIds();

		stdRights = new StudyRightsJB();

		if (tId.size() <=0)
		{
			tSession.putValue("studyRights",stdRights);
		} else {

			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
			tSession.putValue("studyRights",stdRights);
		}
	// Added else part By RA Bug No: 4072(reopen)
	}else{
		studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();
		studyNo = studyNumber;
		}
}
if (EJBUtil.stringToNum(studyId) == 0)
{
	studyId = "";
}

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\"/>");
String uName = (String) tSession.getValue("userName");
%>
<!-- <P class = "userName"><%= uName %></p> -->
<%
if(from.equals("sectionver")){
%>
<%
}else if (from.equals("appendixver")) {
%>
<%
}else if (from.equals("version")) {
%>

<% } else if (from.equals("admin")) { %>

<%
}else if (from.equals("status")) {
%>

<%
}else if (from.equals("calendar")) {
%>
<%
}

else if (from.equals("calenderstatus")) {
%>
<%
}
else if(from.equals("calendarhistory")) {
%>
<%
}
else if (from.equals("report")) {
%>
<%
}else if (from.equals("team")) {
%>
<%
}else if (from.equals("notify")) {
%>
<%
}else if (from.equals("form")) {
%>
<%
}else if (from.equals("studynot")) {
%>
<%
}else if (from.equals("verHistory")) {
%>
<%
}
else if (from.equals("studyTeamHistory")) { //Added by Manimaran for Enhancement S4.
%>
<%
}
else {
%>
<%
}
%>
<DIV>
<!-- Added By Ashu for Enhancement:8.10ReqD-FIN10 -->
<% if (grpRight >0) {
	%>
 <% if (mode.equals("M")) { //Ashu 2Feb11 Updated code for BUG#5772,5775,5774%>
	<table cellspacing="2" cellpadding="0" border="0">
 		<tr>
        <td><P class="defComments_txt"><%=MC.M_YouWorking_OnStd%><%--You are working on <%=LC.Std_Study%>*****--%>: <b><%=studyNo%></b></td>	
		<td>
	<%
	//Added by IA Bug # 2906 make visible the 'p' image when the study is active
	//CodeDao cd1=new CodeDao();

	String  aStudyActBeginDate = "";
	String mPatRight = "0";
	String aStudyTeamRight = "";

	studymod.getControlValues("study_rights","STUDYMPAT");
	ArrayList aRightSeq = studymod.getCSeq();
	String rightSeq = aRightSeq.get(0).toString();
	int iRightSeq = EJBUtil.stringToNum(rightSeq);


	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();

	//activeCodeId = cd1.getCodeId("studystat", "active");
	aStudyActBeginDate = studyB.getStudyActBeginDate();
	aStudyActBeginDate = ((aStudyActBeginDate) == null || aStudyActBeginDate.equals(""))?"-":(aStudyActBeginDate);
	TeamDao teamdao = studyTeam.getTeamRights(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userId));
	ArrayList aStudyTeamRights  = teamdao.getTeamRights();

	if (aStudyTeamRights!= null && aStudyTeamRights.size() >0 )
	{
		aStudyTeamRight = ((aStudyTeamRights.get(0)) == null)?"-":(aStudyTeamRights.get(0)).toString();

		if (aStudyTeamRight.length() >= 11 )
		{
			mPatRight = aStudyTeamRight.substring(iRightSeq - 1, iRightSeq);
		}
		else
		{
			mPatRight = "0";
		}
	}

	if (!aStudyActBeginDate.equals("-") && EJBUtil.stringToNum(mPatRight) > 0){

	%>
	&nbsp;&nbsp;&nbsp;&nbsp;<a href="studypatients.jsp?srcmenu=tdmenubaritem5&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" title="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
	<%} }%>
	</td> 
	</tr>
	</table>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0"> -->
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>
	<% //check the rights
	 	// To check for the account level rights
		String modRight = (String) tSession.getValue("modRight");
		 int patProfileSeq = 0, formLibSeq = 0;
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';

		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();

		 formLibAppRight = modRight.charAt(formLibSeq - 1);

	int pageRight = 0;
	int formRights= 0;

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		if (mode.equals("M")) {
			if ((stdRights.getFtrRights().size()) == 0){
				pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
			}
		}
		else
		{
   			pageRight = grpRight;
		}

		if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
		{ showThisTab = true; }
	}
	else if ("2".equals(settings.getObjSubType())) {
		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
	   		}
		}
		if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
		   showThisTab = true;
		}
	}
	else if ("10".equals(settings.getObjSubType())) {

		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0) {
				   pageRight = 0;
	        } else {
	               pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	        }
	    }

		if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
		   showThisTab = true;
		}
	}
	else if ("7".equals(settings.getObjSubType())) {
		if (mode.equals("N"))
		{
				pageRight= 7;
				formRights = 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			}

			if ((stdRights.getFtrRights().size()) == 0){
			 	formRights= 0;
			}else{
	        	formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		   	}
		}

		  if (((pageRight > 0 ) || (formRights >0)) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))  {
				showThisTab = true;
		  }
	}
	else if ("11".equals(settings.getObjSubType())) {
		if (mode.equals("M")) {
			if ((stdRights.getFtrRights().size()) == 0){
				pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			}
		}

		if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
		{ showThisTab = true; }
	}
	else if ("12".equals(settings.getObjSubType())) {
		int grp_milestone_right = 0;
		grp_milestone_right = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));
		if (grp_milestone_right > 3){
			if (mode.equals("M")) {
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("MILESTONES"));
				}
			}
	
			if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
			{ showThisTab = true; }
		}			
	}
	else if ("9".equals(settings.getObjSubType())) {

		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			}
		}
		if ((pageRight >= 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
		   showThisTab = true;
		}
   }

   	else if ("3".equals(settings.getObjSubType())) {
		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYPTRACK"));
	   	    }
	   	}
		if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
			showThisTab = true;
		}
   }

	else if ("4".equals(settings.getObjSubType())) {
		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			   }else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYREP"));
	   		}
	   	}
		if ((pageRight >= 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
			showThisTab = true;
		}
   }

	else if ("5".equals(settings.getObjSubType())) {
		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{

			if ((stdRights.getFtrRights().size()) == 0){
				  pageRight= 0;
			 }else{
				  pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
			 }
		}
		if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
			showThisTab = true;
		}
   }

	else if ("6".equals(settings.getObjSubType())) {

		if (mode.equals("N"))
		{
				pageRight= 7;
		}
		else
		{
			if ((stdRights.getFtrRights().size()) == 0){
				pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYNOTIFY"));
		   	}
		}
		if ((pageRight > 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
		   showThisTab = true;
		}
   }

	else if ("8".equals(settings.getObjSubType())) {//KM-21Jul09
		int study_acc_form_right = 0;
		int study_team_form_access_right = 0;
		if ((String.valueOf(formLibAppRight).compareTo("1") == 0)) {
			if (mode.equals("N"))
			{
					showThisTab = true;
			}
			else
			{
				study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
				study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
				if (((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))){
			    	if (study_acc_form_right >=4  || study_team_form_access_right>0) { //KM-#4591--refer 4099also.
						showThisTab = true;
					}
				}
			}
		}

   }
   else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; }


	if (tab == null) {
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>

	<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!--    	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt=""/>
					</td> -->
					<td class="<%=selclass%>">

					<%if ("1".equals(settings.getObjSubType())) {%>
					<a href="study.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("2".equals(settings.getObjSubType())) { %>
						<a href="studyVerBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=2&mode=N&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("10".equals(settings.getObjSubType())) {%>
						<a href="studyadmincal.jsp?srcmenu=tdmenubaritem3&selectedTab=10&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("7".equals(settings.getObjSubType())) {%>
						<a href="studyprotocols.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem3&selectedTab=7&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("9".equals(settings.getObjSubType())) {%>
						<a href="studynotification.jsp?srcmenu=tdmenubaritem3&selectedTab=9&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}	else if ("3".equals(settings.getObjSubType())) {%>
						<a href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("4".equals(settings.getObjSubType())) {%>
						<a href="reportsinstudy.jsp?srcmenu=tdmenubaritem3&selectedTab=4&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}else if ("5".equals(settings.getObjSubType())) {%>
						<a href="teamBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=5&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}else if ("6".equals(settings.getObjSubType())) {%>
						<a href="notify.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem3&selectedTab=6&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("8".equals(settings.getObjSubType())) {
						if ((String.valueOf(formLibAppRight).compareTo("1") == 0)) { %>
						<a href="formfilledstudybrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=8&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}}else if ("11".equals(settings.getObjSubType())) {%>
						<a href="combinedBudget.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem3&selectedTab=11&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}else if ("12".equals(settings.getObjSubType())) {%>
						<a href="studyMilestones.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem3&selectedTab=12&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}%>
					</td>
				<!--     <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td> -->
			  	</tr>
		   	</table>
        </td>

	<%}%>
   	</tr>

	<%}%>
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
	<%
	// Commented By RA Bug No: 4072
	//if (!(tab.equals("1")) && mode.equals("M")) {
	
		if (tab.equals("9") && hideRedmessage.equals("0")) //Notifications
	   	{
	   	%>
	   		<P class="redMessage">
	   			<%=MC.M_CalNotfic_NotAplStdAdmCal%><%--<%=LC.Pat_Patient%> Calendar Notifications are not applicable to <%=LC.Std_Study%> Admin Calendars*****--%>
	   		</P>
	   		<%
	   	}
%>
	</DIV>
<%		} //session time out.
			%>
