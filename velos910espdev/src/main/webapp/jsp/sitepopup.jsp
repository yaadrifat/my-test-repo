<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Select_Org%><%--Select Organization*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
 function back(formobj) {
	i=formobj.orgId.options.selectedIndex;
	if (i>-1) {	
	  if (document.all) {
			window.opener.document.reports.orgId.value = formobj.orgId.options[i].value;	
			window.opener.document.reports.selOrg.value = formobj.orgId.options[i].text;
	 } else {
			if (((navigator.appVersion).substring(0,1)) == '5') { 
				window.opener.document.reports.orgId.value = formobj.orgId.options[i].value;	
				window.opener.document.reports.selOrg.value = formobj.orgId.options[i].text;
			} else {
				window.opener.document.div1.document.reports.orgId.value = formobj.orgId.options[i].value;	
				window.opener.document.div1.document.reports.selOrg.value = formobj.orgId.options[i].text;
			}			
		}		 
	} 
	self.close();	 
}

</SCRIPT>

</head>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>

<body>

<Form  name="site" method="post" action="" >

<%

 String studyPk = request.getParameter("studyPk");

 HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{      	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String acc = (String) tSession.getValue("accountId");
	SiteDao siteDao = new SiteDao();
	StringBuffer siteList = new StringBuffer();
		
	if (studyPk.equals("")) { //get organization list irrespective of study
		int siteId=0;
     	siteDao.getSiteValues(EJBUtil.stringToNum(acc));
     	siteList.append("<SELECT NAME=orgId >") ;
     	for (int counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){
     		siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue(); 
     		siteList.append("<OPTION value = "+ siteId +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     	}
     	siteList.append("</SELECT>");

	} else { //get organization list for a study
		siteDao = siteB.getSiteValuesForStudy(EJBUtil.stringToNum(studyPk));
		ArrayList orgIds = new ArrayList();
		orgIds = siteDao.getSiteIds();
		String orgId="";
		ArrayList sites = siteDao.getSiteNames();
	
		int len = orgIds.size();	
		
		siteList.append("<SELECT name=orgId>");

		for(int counter=0;counter<len;counter++){
			orgId = (orgIds.get(counter)).toString();
			siteList.append("<option value="+(orgIds.get(counter)).toString() +" >" +(sites.get(counter)).toString() +"</option>");
		}

		siteList.append("</select>");
	}
%>

<BR>
 <table width="70%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
	<td align="center"><P class = "defComments"><%=MC.M_SelOrg_AndSel%><%--Select Organization and click on Select*****--%></P> <%=siteList%> </td>
	</tr>
	<tr>
	<tr height=30>		
		<td>&nbsp;</td>
	</tr>
	<td align="center">
	 <A onclick="back(document.site)" href="#"> <img src="./images/select.gif"  border="0" > </A>
    </td>
  </tr>
  </table>
</Form>

<%
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%}%>



</body>
</html>
