<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Std_VersionDetails%><%--Study Version Details*****--%></title>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

</head>

<SCRIPT>
function  validate(formobj){
	if (formobj.origVerStatus.value=="F") return false;

  	if (!(validate_col('version number',formobj.verNumber))) return false
  	if (!(validate_date(formobj.versionDate))) return false	//JM
  	if (!(validate_col('Category',formobj.dStudyvercat))) return false//JM



  	if (!(validate_col('e-Signature',formobj.eSign))) return false

	/*if (formobj.verStatus.value=="F") {
    	msg="Selecting 'Freeze' as version status will not allow any further changes and the information cannot be edited then. Would you like to continue?";
		if (confirm(msg)) return true;
  		else return false;
	}*/

  	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
  		return false;
   	}
}
// Added By Rohit for Bug# 4930 to limit the text for TextArea
function limitText(limitField,limit)
{
   if (limitField.value.length > limit) {
       limitField.value = limitField.value.substring(0, limit);
   }
}
</SCRIPT>



<% String srcmenu;
srcmenu= request.getParameter("srcmenu");
%>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<br>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.esch.business.common.*"%>

<%
 String selectedTab = request.getParameter("selectedTab");
 String mode = request.getParameter("mode");
 String studyVerId = "";
 String studyId = "";
if(mode.equals("M"))
    studyVerId = request.getParameter("studyVerId");

else
    studyId = request.getParameter("studyId");


%>



  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>

<%
	int studyVerIdInt = 0;
	String verNumber ="";
	String verStatus = "";
	String verNotes ="";

	String verDate = "";

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

//JM: 11/16/2005: added: Version category dropdown
	String ddStudyvercat = "" ;
	CodeDao cd1=new CodeDao();
	cd1.getCodeValues("studyvercat");

	cd1.setCType("studyvercat");
	cd1.setForGroup(defUserGroup);



	String stdvercat=request.getParameter("dStudyvercat");
	if (stdvercat==null) stdvercat="";

	if (stdvercat.equals("")){
	ddStudyvercat=cd1.toPullDown("dStudyvercat");
	}
	else{
	ddStudyvercat=cd1.toPullDown("dStudyvercat",EJBUtil.stringToNum(stdvercat),true);
	}
//JM: 11/16/2005: added: Version type dropdown
	String ddStudyvertype = "" ;
	CodeDao cd2=new CodeDao();
	cd2.getCodeValues("studyvertype");

	cd2.setCType("studyvertype");
	cd2.setForGroup(defUserGroup);



	String stdvertype=request.getParameter("dStudyvertype");
	if (stdvertype==null) stdvertype="";

	if (stdvertype.equals("")){
	ddStudyvertype=cd2.toPullDown("dStudyvertype");
	}
	else{
	ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(stdvertype),true);
	}


	if(mode.equals("M")){
	studyVerIdInt = EJBUtil.stringToNum(studyVerId);

	studyVerB.setStudyVerId(studyVerIdInt);
	studyVerB.getStudyVerDetails();
	verNumber = studyVerB.getStudyVerNumber();

	verDate =	studyVerB.getVersionDate();

	if (verDate==null) {
		verDate = "";
	}


	stdvercat = studyVerB.getStudyVercat();
	if (stdvercat==null) {
				stdvercat = "";
		}
	ddStudyvercat=cd1.toPullDown("dStudyvercat",EJBUtil.stringToNum(stdvercat),true);

	stdvertype = studyVerB.getStudyVertype();
	if (stdvertype==null) {
				stdvertype = "";
		}
	ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(stdvertype),true);





    verStatus = statHistoryB.getLatestStatus(studyVerIdInt,"er_studyver");

	if(verStatus == null)
		verStatus = "";


   verNotes = studyVerB.getStudyVerNotes();
	if(verNotes == null) {
		verNotes="";
	}
}
	String vcategory=request.getParameter("dStudyvercat");
	if (vcategory==null) vcategory="";
    String userIdFromSession = (String) tSession.getValue("userId");
%>
<DIV class="tableDefault" id="div1">
  <P class="sectionHeadings">  <%=LC.L_Std_VerDetails%><%--Study >> Version Details*****--%> </P>
  <%
	if (verStatus.equals("F")) {
	%>
	 <p class="defComments"><FONT class="Mandatory"><%=MC.M_CntChg_InFreezeVer%><%--You cannot make any changes in a Freeze Version.*****--%></Font> </p>
	 <%}%>


<Form name="version" id="versionForm" method="post" action="updatestudyver.jsp" onsubmit="if (validate(document.version)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <Input type="hidden" name="origVerStatus" value="<%=verStatus%>">
  <Input type="hidden" name="studyVerId" value="<%=studyVerId%>">
    <Input type="hidden" name="studyId" value="<%=studyId%>">
    <Input type="hidden" name="mode" value="<%=mode%>">
    <input type="hidden" name="vcategory" value = "<%=vcategory%>" >


 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl">

  <tr>
  	<td width="15%"> <%=LC.L_Version_Number%><%--Version Number*****--%> <FONT class="Mandatory">*</FONT> </td>
	<td align=left colspan=3>
		<!--<input type=text name=verNumber size=20 maxlength=25 class="leftAlign" value="<%--=verNumber--%>">-->
<!-- Modified by Ganapathy on 05/24/05 -->
		<input type=text name=verNumber size=28 maxlength=100 class="leftAlign" value="<%=verNumber%>">
	</td>
  </tr>
  <tr>
  	<td width="25%"><%=LC.L_Version_Date%><%--Version Date*****--%>
  	</td>
<%-- INF-20084 Datepicker-- AGodara --%>
	<td><input type="text" name="versionDate" class="datefield" size=15   maxlength=20 value='<%=verDate%>'></td>
  </tr>
  <tr><td width="15%"><%=LC.L_Category%><%--Category*****--%><FONT class="Mandatory">* </FONT></td>
  <td ><%=ddStudyvercat%></td>

  </tr>


  <tr><td width="15%"><%=LC.L_Type%><%--Type*****--%></td>
  <td ><%=ddStudyvertype%></td>

  </tr>

  <tr>
    <td width="15%"> <%=LC.L_Notes%><%--Notes*****--%> </td>
	<td align=left colspan=3 >
		<TextArea name="verNotes" rows=3 cols=40 onkeydown="limitText(this.form.verNotes,1999);" onkeyup="limitText(this.form.verNotes,1999);" ><%=verNotes%></TextArea>
	</td>

  </tr>




 </table>
<%
	if ((!(verStatus ==null)) && !(verStatus.equals("F"))) {
%>
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="versionForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
<%
	}
%>


<Input type="hidden" name="mode" value="<%=mode%>">
<Input type="hidden" name="srcmenu" value="<%=srcmenu%>">
<Input type="hidden" name="selectedTab" value="<%=selectedTab%>">


  </Form>

<%

//}


//else

//{

%>
<%--
  <jsp:include page="accessdenied.jsp" flush="true"/>

  --%>
  <%

//} //end of else body for page right



}//end of if body for session

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>

