<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.velos.eres.service.util.StringUtil"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" /></title>
	<div id="nav" style="width:99%;">
	    <tiles:insertAttribute name="localization" />
	    <tiles:insertAttribute name="panel" />
	</div>
	<div id="commoninc">
		<tiles:insertAttribute	name="velos_includes" />
	</div>
<style type="text/css">
ul.errorMessage { list-style-type:none; }
ul.errorMessage li { color:red; height:1.5em; text-align:right; }
span.errorMessage { color:red; height:1.5em; text-align:right; }
</style>
<script>
$j(document).ready(function(){
	if ("Y" == "<%=StringUtil.htmlEncodeXss(request.getParameter("noScroll"))%>") {
		$j("div.BrowserBotN").css({overflow:"hidden"});
	}
});
</script>
<s:head /> 
</head>
<body>
	<div class="BrowserTopN BrowserTopN_RR_1" id="divTab">
			<tiles:insertAttribute	name="tabs" />
	</div>
	<div class="BrowserBotN BrowserBotN_RR_1" style="height:76%;">
		<div id="body">
			<tiles:insertAttribute	name="body" />
		</div>
		<div id="footer" >
			<tiles:insertAttribute	name="footer" />
		</div>
	</div>
</body>
</html>
