<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Bgt_Secs%><%--Budget Sections*****--%></title>


<!-- #5747 02/10/2011 @Ankit --> 
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*, com.velos.esch.business.common.BudgetDao" %>
<jsp:useBean id="budgetB" scope="page"	class="com.velos.esch.web.budget.BudgetJB" />


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<SCRIPT language="javascript">



function confirmBox(eventName,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {

		var paramArray = [decodeString(eventName)];
		if (confirm(getLocalizedMessageString("L_Del_FromPcol",paramArray))) {/*if (confirm("Delete " + decodeString(eventName) + " from Protocol?")) {*****/

		    return true;

		} else {

			return false;

		}

	} else { 

		return false;

	}			

}



</SCRIPT> 







<% String src;

src= request.getParameter("srcmenu");

String budgetTemplate = request.getParameter("budgetTemplate");

String includedIn = request.getParameter("includedIn");
	if(StringUtil.isEmpty(includedIn))
	{
	 includedIn = "";
	}


%>


<% if ("S".equals(budgetTemplate)) { %>
	<jsp:include page="panel.jsp" flush="true"> 

	<jsp:param name="src" value="<%=src%>"/>

	</jsp:include>   

<% } else { 
	%>
<jsp:include page="include.jsp" flush="true"/> 

<% } %>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="bgtSectionB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>

<%@ page language = "java" import = "java.util.*,com.velos.esch.business.common.BgtSectionDao,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>



<% int pageRight = 0;

   HttpSession tSession = request.getSession(true);

   String mode = request.getParameter("mode");        
   
   
	


%>



<BR>

<% 
	String 	bottomdivClass="popDefault";

	if (! (includedIn.equals("P") || 
	        (includedIn.equals("") && !"S".equals(budgetTemplate)))) { 
			bottomdivClass="tabDefBotN";
		%>
	<DIV class="tabDefTopN" id="div1">
	<jsp:include page="budgettabs.jsp" flush="true">
		<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>"/>
		<jsp:param name="mode" value="<%=mode%>"/>	

	</jsp:include>

	</DIV>
<% } %>
		
				
	<div class="<%=bottomdivClass%>" id="div2">	
	<BR>

 <%

 if (sessionmaint.isValidSession(tSession))

 { 
	 String fromRt = request.getParameter("fromRt");
	 if (fromRt==null || fromRt.equals("null")) fromRt="";
	 %>
		<Input type="hidden" name="fromRt" value=<%=fromRt%> />
	 <%
	 if (fromRt.equals("")){
		GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
		pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTDET"));
	 } else{
		pageRight = EJBUtil.stringToNum(fromRt);
	 }

 	String budgetId = request.getParameter("budgetId");

 	String calId = request.getParameter("calId");

	String selectedTab = request.getParameter("selectedTab");

	String bgtStat = request.getParameter("bgtStat");
	// #5747 02/10/2011 @Ankit
	BudgetDao budgetDao = budgetB.getBudgetInfo(EJBUtil.stringToNum(budgetId));
	ArrayList bgtStatDescs = budgetDao.getBgtCodeListStatusesDescs();
	String budgetStatusDesc = "";
	if(bgtStatDescs!=null && bgtStatDescs.size()>0){
		budgetStatusDesc = (String) bgtStatDescs.get(0);
	}
	
   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));



   int counter = 0;

   Integer id;



   ArrayList bgtSectionIds= null; 

   ArrayList bgtSectionNames= null;

   ArrayList bgtSectionTypeDescs= null;

   ArrayList bgtSectionSequences= null;

	ArrayList	bgtSectionFKVisits=null;
            

   BgtSectionDao bgtSectionDao= bgtSectionB.getBgtSections(EJBUtil.stringToNum(calId));

   bgtSectionIds=bgtSectionDao.getBgtSectionIds(); 

   bgtSectionNames= bgtSectionDao.getBgtSectionNames();

   bgtSectionTypeDescs= bgtSectionDao.getBgtSectionTypeDescs();

   bgtSectionSequences= bgtSectionDao.getBgtSectionSequences();

	bgtSectionFKVisits= bgtSectionDao.getArFKVisits();

   int len = bgtSectionIds.size() ;

   String sectionId="";

   String bgtSectionName = ""; 

   String bgtSectionTypeDesc = "";

   String bgtSectionSequence ="";

	String fkVisit="";

 if (pageRight > 0 )

	{

	

 %>

<%if ((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template") )) {
Object[] arguments = {budgetStatusDesc}; %>


<br>
<table width="600" cellspacing="2" cellpadding="2">
	<tr>
	<td><!--#5747 02/10/2011 @Ankit-->
	  	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=budgetStatusDesc%>'. You cannot make any changes to the budget.*****--%></Font></P>
	</td>
	</tr>
</table>
<%}%>


<table width=100%>

<tr>



<td>

<P class = "defComments"><%=MC.M_SecIn_Bgt%><%--Sections in  this budget are*****--%>:</P>

</td>



<td align="right">

<%if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template") )&&(mode.equals("M"))))){%>

<A href="bgtnewsection.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&calId=<%=calId%>&sectionMode=N&bgtSectionId=&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=LC.L_Add_NewSec%><%--Add New Section*****--%></A>

<%}%>

</td>



</tr>

</table>




 <Form name="budgetsections" method="post" action="" onsubmit="">

    <table width="100%" >

     <tr> 
	  <% if (budgetTemplate.equals("P") || budgetTemplate.equals("C") ) {%>
       <th width="20%" align="left"> <%=LC.L_Sec_Type%><%--Section Type*****--%> </th>
	  <%}%>
       <th width="15%" align="left"> <%=LC.L_Sequence%><%--Sequence*****--%> </th>
       <th width="35%" align="left"> <%=LC.L_Section_Name%><%--Section Name*****--%> </th>

		<%if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template"))&&(mode.equals("M"))))){%>	   

    	   <th width="15%" > <%=LC.L_Edit%><%--Edit*****--%> </th>

	       <th width="15%"> <%=LC.L_Delete%><%--Delete*****--%> </th>

		<%}%>

      </tr>

 <%

    for(counter = 0;counter<len;counter++)

	{

	id = (Integer)bgtSectionIds.get(counter);
	fkVisit= (String)bgtSectionFKVisits.get(counter);
	if (StringUtil.isEmpty(fkVisit))
	{
		fkVisit="";
	}
	
	sectionId = id.toString();

	bgtSectionName=((bgtSectionNames.get(counter)) == null)?"-":(bgtSectionNames.get(counter)).toString();

	if (budgetTemplate.equals("P") || budgetTemplate.equals("C")  ) { 
		bgtSectionTypeDesc=((bgtSectionTypeDescs.get(counter)) == null)?"-":(bgtSectionTypeDescs.get(counter)).toString();
	}
	
	bgtSectionSequence=((bgtSectionSequences.get(counter)) == null)?"-":(bgtSectionSequences.get(counter)).toString();

	if ((counter%2)==0) {

	%>

      <tr class="browserEvenRow"> 

  <%

		}

	else {

  %>

      <tr class="browserOddRow"> 

   <%

		}

   %>	

   	<% if (budgetTemplate.equals("P") || budgetTemplate.equals("C")  ) {%>
   	<td><%=bgtSectionTypeDesc%></td>
	<%}%>
	<td><%=bgtSectionSequence%></td>																																					
  	<td><%=bgtSectionName%></td>

	<%if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template"))&&(mode.equals("M"))))){%>
	<% if ( (StringUtil.isEmpty(fkVisit) && includedIn.equals("P"))  ||  StringUtil.isEmpty(includedIn))
		{
		%>
	<td>
		<A href="bgtnewsection.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&calId=<%=calId%>&sectionMode=M&bgtSectionId=<%=sectionId%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit%><%--Edit*****--%> </A>
	</td>		
		<%}%>
	<%}%>

	<%if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template"))&&(mode.equals("M"))))){
		if ( (StringUtil.isEmpty(fkVisit) && includedIn.equals("P"))  ||  StringUtil.isEmpty(includedIn))
		{
		%>
	<td align="center">	
		<A href="budgetsectiondelete.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&bgtSectionId=<%=sectionId%>&delMode=null&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&calId=<%=calId%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&fromRt=<%=fromRt%>" onclick="return confirmBox('<%= StringUtil.encodeString(bgtSectionName) %>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%><%--Delete*****--%>" alt="<%=LC.L_Delete%><%--Delete*****--%>" border="0" align="left"/></A>
	</td>
	<%
		}
			}%>

    </tr>

 <%

		}

%>

    </table>

  <br>

  <br>



  <%

  if(budgetTemplate.equals("P") || budgetTemplate.equals("C") ){

  %>

  

  <table width="100%" cellspacing="0" cellpadding="0">

  <tr>

      <td align=center>

	  	<button onClick="javascript:window.self.close();"><%=LC.L_Close%></button> 

      </td>

      </tr>

  </table>

  <%

  }else{

  %>



    <table width="100%" cellspacing="0" cellpadding="0">

  <tr>

      <td align=center>

	  	<A type="submit" HREF="studybudget.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&bgtcalId=<%=calId%>&pageMode=final&mode=<%=mode%>&selectedTab=<%=selectedTab%>" ><%=LC.L_Back%></A> 

      </td>

      </tr>

  </table>

 

  

  

  

  <%

  }

  %>



  

  </Form>





 <%



 

	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

 <%

 } //end of else body for page right



 

}//end of if body for session

else

{

%>

  <jsp:include page="timeout.html" flush="true"/>

<%

}

%>

 

  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>


<% if (!(includedIn.equals("P") || 
        (includedIn.equals("") && !"S".equals(budgetTemplate)))) { %>
	<div class ="mainMenu" id = "emenu" id = "emenu">
		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
<% } %>


</body>





</html>

