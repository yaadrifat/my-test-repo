<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
	<head>
		<title><%=MC.M_MngAcc_PorAdm%><%--Manage Account >> Portal Admin*****--%></title>
		<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>

	<% String src;
		src= request.getParameter("srcmenu");
	%>  
<jsp:include page="panel.jsp" flush="true" > 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<SCRIPT Language="javascript">

	
        function  redirectManageLogin(portalid,portallevel,portalStat){
	 
	
	 if(portalStat == portalbrowser.codeDesc.value){
	  
	     alert("<%=MC.M_MngLoginCntPerform_Wip%>");/*alert("Manage Logins cannot be performed with the current status as Work in Progress");*****/
	 
	 }
	 else {
	 window.location.href="manageportallogins.jsp?srcmenu=tdmenubaritem5&selectedTab=1&page=1&portalLevel="+portallevel+"&portalId="+portalid+"";
	 
	 }
	    	    
	
	}
	
	function openeditstatus(portalId,statusId,portalName,mode,portalStat) {
               
		 windowname=window.open("editPortalStatus.jsp?moduleTable=er_portal&portalId="+portalId+"&statusId="+statusId+"&portalName="+portalName+"&mode="+mode+"&portalStat="+portalStat,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=650,height=275 top=120,left=200 "); 
		windowname.focus();
	}
	
	function openPreview(portalId) {
               
		windowname=window.open("previewPortal.jsp?portalId="+portalId,"Portal","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=750 top=40,left=80 "); 
		windowname.focus();
	}
	
	function openDesign(portalId, studyid,portalSelfLogout ) {	               
		windowname=window.open("portalDesign.jsp?portalId="+portalId + "&studyId="+studyid + "&selfLogout=" + portalSelfLogout  ,"PortalDesign","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=780 top=50,left=130 "); 
		windowname.focus();
	}
	

//JM: 14Jun2007
	function confirmBox(portalName,pgRight) {	
	
	if ( pgRight>=6 ) {	
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [portalName];
		if (confirm(getLocalizedMessageString("M_DoYouWant_Del",paramArray))) {/*if (confirm("Do you want to delete "+portalName+"?")) {*****/
		    return true;
		} else {
			return false;
		}
	} else { 
		return false;
	}			
	} else{
		alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit Permission denied");*****/
		return false;
	}			
   }   
   
	/*
	function confirmBox(portalName) {
		msg="Do you want to delete "+portalName+"?";		
		if (confirm(msg)) {
			return true;
		}else{
			return false;
		}	
	}
	*/
	
	function f_createNew(pgRight){
	
		if (pgRight == 4 || pgRight == 6){
			alert("<%=LC.L_New_PermissionDenied%>");/*alert("New permission denied");*****/
			return false;
		}
	
	}

</SCRIPT>


<body>
<jsp:useBean id="portalB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<DIV class="tabDefTopN" id="divTab"> 
		<jsp:include page="accounttabs.jsp" flush="true"> 
		<jsp:param name="selectedTab" value="6"/>
		</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 
<Form name="portalbrowser" method="post" action="portalbrowser.jsp" onsubmit="" flush="true">
	<%
		HttpSession tSession = request.getSession(true); 
		if (sessionmaint.isValidSession(tSession)) {	
			String uName = (String) tSession.getValue("userName");
			String userId = (String) tSession.getValue("userId");
	 		int pageRight = 0;
	   		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
   			//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
   			//JM: 13Jun2007
   			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));   			
   			
   			
 		  //if (pageRight > 0 ) {
 			if (pageRight >=4 ) {
 			
	   		int accountId=0;   
			String orderBy="";
			orderBy=request.getParameter("orderBy");
			if (orderBy==null) orderBy="PORTAL_NAME";
				String orderType = "";
				orderType = request.getParameter("orderType");
				if (orderType == null) {
				orderType = "asc";
			} 

	   	String acc = (String) tSession.getValue("accountId");
	   	accountId = EJBUtil.stringToNum(acc);

		//PortalDao portalDao = new PortalDao();
//JM: 03JAN2010: Added the extra parameter userId: #Enh-PP2.1		
		PortalDao portalDao = portalB.getPortalValues(accountId,EJBUtil.stringToNum(userId));
	   	ArrayList portalIds = portalDao.getPortalIds(); 
	   	ArrayList portalFkStudyIds = portalDao.getFkStudyIds();
	   	ArrayList portalNames = portalDao.getPortalNames();
	   	ArrayList createdBy = portalDao.getCreatedBy();
	   	ArrayList portalDescs = portalDao.getPortalDescs();
	   	ArrayList portalLevels = portalDao.getPortalLevels();
	   	ArrayList portalBgColors = portalDao.getPortalBgColors();
		ArrayList portalTxtColors = portalDao.getPortalTxtColors();
		ArrayList portalHeaders = portalDao.getPortalHeaders();
		ArrayList portalFooters = portalDao.getPortalFooters();
		ArrayList notificationFlags = portalDao.getNotificationFlags();
		ArrayList portalStats = portalDao.getPortalStats();
		ArrayList createdOns = portalDao.getCreatedOns();
                ArrayList lastModifiedDates = portalDao.getLastModifiedDates();  
                ArrayList creators = portalDao.getCreator();
                ArrayList ipAdds = portalDao.getIpAdd();
		ArrayList lastModifiedBys = portalDao.getLastModifiedBy();
		ArrayList histStatIds = portalDao.getHistStatIds();
		
		ArrayList arPortalSelfLogouts = portalDao.getArPortalSelfLogout();
		
		int len = portalIds.size();
	   	int counter = 0;
		String portalName ="";
		String portalDesc ="";
		String portalLevel = "";
                int portalId=0;
		String createBy ="";
		String portalStat ="";
		int notificationFlag =0;
                String creator ="";
	        String createdOn ="";
                String lastModifiedDate ="";
		String lastModifiedBy ="";
                String ipAdd ="";
		int histStatId =0;
	
		String portalFkStudyId = "";//JM:
		String portalSelfLogout;
		
		CodeDao codeDao = new CodeDao();
		int res = codeDao.getCodeId("portalstatus","W");
		String statusCodeDesc = codeDao.getCodeDescription();
		
		
	%>

	<input type="hidden" name="orderType" value="<%=orderType%>"> 
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="orderBy" value="<%=orderBy%>">
	<input type="hidden" name="codeDesc" value="<%=statusCodeDesc%>">

	<table width="98%" cellspacing="0" cellpadding="0" border=0 >
      	<tr valign="center"> 
      		<td width = "70%"> 
          		<P class = "defComments"><%=MC.M_ExistPortal_Are%><%--Existing Portal(s) are*****--%>:</P>
        	</td>     		       		
        	<td width = "30%" valign="center" align="right"> 
           		<A  href="portaldetails.jsp?mode=N&srcmenu=<%=src%>" onClick="return f_createNew(<%=pageRight%>);"><%=LC.L_Create_NewPortal%><%--Create New Portal*****--%></A>  
           	</td>
         </tr>         			
    	</table>
    	<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0" >
      		<tr><!--KM- 3036--> 
        		<th width="15%"> <%=LC.L_Portal_Name%><%--Portal Name*****--%> </th>
				<th width="15%"> <%=LC.L_Description%><%--Description*****--%> </th>
        		<th width="15%"> <%=LC.L_Status%><%--Status*****--%> </th>
        		<th width="10%"> <%=LC.L_Logins%><%--Logins*****--%> </th>
			<th width="10%"> <%=LC.L_Designer%><%--Designer*****--%> </th>
			<th width="6%"> <%=LC.L_Preview%><%--Preview*****--%></th>			
			<th width="6%"> <%=LC.L_Delete%><%--Delete*****--%></th>
			<th width="5%"> <%=LC.L_Info%><%--Info*****--%></th>
      		</tr>
      	<%
		for(counter = 0;counter<len;counter++)	{

            portalId=((Integer)portalIds.get(counter)).intValue();
            portalFkStudyId = ((portalFkStudyIds.get(counter))==null)?"0":(portalFkStudyIds.get(counter)).toString();//JM
		    portalName =  ((portalNames.get(counter)) == null)?"-":(portalNames.get(counter)).toString();
		    createBy =  ((createdBy.get(counter)) == null)?"-":(createdBy.get(counter)).toString();	
		    portalDesc =  ((portalDescs.get(counter)) == null)?"-":(portalDescs.get(counter)).toString();
		    	
		    portalLevel =  ((portalLevels.get(counter)) == null)?"-":(portalLevels.get(counter)).toString();	
		    portalStat =  ((portalStats.get(counter)) == null)?"-":(portalStats.get(counter)).toString();	 
		    notificationFlag =  ((Integer)notificationFlags.get(counter)).intValue();

		    createdOn =  ((createdOns.get(counter)) == null)?"-":(createdOns.get(counter)).toString();
		    creator =  ((creators.get(counter)) == null)?"-":(creators.get(counter)).toString();
		    lastModifiedDate =  ((lastModifiedDates.get(counter)) == null)?"-":(lastModifiedDates.get(counter)).toString();
		    ipAdd =  ((ipAdds.get(counter)) == null)?"-":(ipAdds.get(counter)).toString();
		    lastModifiedBy =  ((lastModifiedBys.get(counter)) == null)?"-":(lastModifiedBys.get(counter)).toString();  
		    histStatId=((Integer)histStatIds.get(counter)).intValue();	
		    
		    portalSelfLogout = ((arPortalSelfLogouts.get(counter)) == null)?"0":(arPortalSelfLogouts.get(counter)).toString();
			   
			if (portalDesc.length() > 100) {
				portalDesc = portalDesc.substring(0,100) + "...";    				
			}
			if (portalName.length() > 40) {
				portalName = portalName.substring(0,40) + "...";    				
			}
		
                 
		if ((counter%2)==0) {  %>
		<tr class="browserEvenRow"> 
		<% } else { %>
		<tr class="browserOddRow"> 
        	<% } %>
        		
			<td> 
			<a href="portaldetails.jsp?mode=M&portalId=<%=portalId%>&portalStat=<%=portalStat%>&histStatId=<%=histStatId%>"><%=portalName%></a> </td>
			<td><%= portalDesc%></td>

			<td><!--a href="#" onClick="openeditstatus(<%=portalId%>,<%=histStatId%>,'<%=StringUtil.encodeString(portalName)%>','M','<%=portalStat%>')"> <%=portalStat%></a-->

			<A href="#" onClick="openeditstatus(<%=portalId%>,<%=histStatId%>,'<%=StringUtil.encodeString(portalName)%>','M	','<%=portalStat%>')"><%=portalStat%></a>&nbsp;&nbsp;		
			<a href="showPortalHistory.jsp?modulePk=<%=portalId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=6&page=<%=page%>&from=portalHistory&fromjsp=showPortalHistory.jsp&portalName=<%=StringUtil.encodeString(portalName)%>&currentStatId=<%=histStatId%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif"/></a></td>
			<%if(pageRight>=4){ %>
 			<td> 
 			<a href="#" onClick="redirectManageLogin(<%=portalId%>,'<%=portalLevel%>','<%=portalStat%>')"><%=LC.L_Manage_Logins%><%--Manage Logins*****--%></a>
 			</td>
 
 			<td align="center"> <a href="#" onClick="openDesign(<%=portalId%>, <%=portalFkStudyId%>,<%=portalSelfLogout%>)"><img src="../images/jpg/DesignPortal.gif" title="<%=LC.L_Design%>" border="0"/></a> 
 
 			</td>
 			<%}%>

 			<td  align="center"><A href="#" onClick="openPreview(<%=portalId%>)"> <img src="../images/jpg/preview.gif" title="<%=LC.L_Preview%>" border="0"/></A></td>
 			<!-- <td><A href="deleteportal.jsp?selectedTab=6&portalId=<%=portalId%>&srcmenu=<%=src%>&portalName=<%=StringUtil.encodeString(portalName)%>"><img src="./images/delete.gif" onclick="return confirmBox('<%=portalName%>')" border="0" align="left"/></A></td> -->
			<td  align="center"><A href="deleteportal.jsp?selectedTab=6&portalId=<%=portalId%>&srcmenu=<%=src%>&portalName=<%=StringUtil.encodeString(portalName)%>"><img src="../images/delete.png" title="<%=LC.L_Delete%>" onclick="return confirmBox('<%=portalName%>',<%=pageRight%>)" border="0"/></A></td>
			
			<td  align="center">
				<A><img src="../images/jpg/help.jpg" border="0"  onmouseover="return overlib('<%="<tr><td><font size=2><b>"+LC.L_Creator/*Creator*****/+" : </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(creator)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Created_On/*Created On*****/+": </b></font><font size=1>"+createdOn+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedBy/*Last Modified By*****/+": </b></font><font size=1>"+ StringUtil.escapeSpecialCharJS(lastModifiedBy)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedOn/*Last Modified On*****/+" : </b></font><font size=1>"+lastModifiedDate+"</font></td></tr>"%>',CAPTION,'<%=LC.L_Portal_Upper%>',RIGHT,ABOVE)"; onmouseout="return nd();"></A>
				</td>
      		</tr>

	   	<% } %>
    	</table>
 </Form>
 		<% } //end of if body for page right
		else { %>
  			<jsp:include page="accessdenied.jsp" flush="true"/>
  		<% } //end of else body for page right
		}//end of if body for session
		else { %>
  			<jsp:include page="timeout.html" flush="true"/>
  		<% } %>
	<div> 
			<jsp:include page="bottompanel.jsp" flush="true"/>
  	</div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

