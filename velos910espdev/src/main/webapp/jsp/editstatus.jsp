<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Status_Dets%><%-- Status Details*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT>
function  validate(formobj){

  	if (!(validate_col('Status',formobj.status))) return false
  	if (!(validate_col('Status Date',formobj.statusDate))) return false
  	if (!(validate_col('e-Signature',formobj.eSign))) return false
		//-----------------Added by Ganapathy  on 05/25/05 for May Enhancement-2
   if (!(validate_date(formobj.statusDate))) return false;
 }



</SCRIPT>


<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.CodeDao,com.velos.eres.service.util.*,com.velos.eres.business.studyVer.impl.StudyVerBean"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
	%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
	<%

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String mode="";
	//Added by Ganapathy on 04-15-05
	String strStatus="";
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("modulePk");
	String statusCodeType = request.getParameter("statusCodeType");
	String pageRight = request.getParameter("pageRight");
	String statusId = request.getParameter("statusId");
	String sectionHeading = request.getParameter("sectionHeading");
	String 	studyNo = (String) tSession.getValue("studyNo");
    
	//JM: 05Mar2008
	if (studyNo==null || studyNo=="") {
		studyNo = request.getParameter("studyNumber");
	    //KM:15Oct2008
		studyNo = StringUtil.decodeString(studyNo);
	}
	
	if (studyNo==null ) studyNo="";

	String 	invNumber = request.getParameter("invNumber");



	//String verNumber = request.getParameter("verNumber");//JM: 031006
	//String verNumber = StringUtil.decodeString(request.getParameter("verNumber"));//JM: 031006
	//JM: 031006: for getting the version number
	//JM: 22Feb2008
	String verNumber ="";

	studyVerB.setStudyVerId(EJBUtil.stringToNum(modulePk));
	StudyVerBean  svsk = new StudyVerBean();
	svsk = studyVerB.getStudyVerDetails();
	if (svsk !=null){
	verNumber = svsk.getStudyVerNumber();
	}


	if (EJBUtil.isEmpty(sectionHeading))
		sectionHeading = LC.L_Status_Dets;/*sectionHeading = "Status Details";*****/

	if (EJBUtil.isEmpty(pageRight))
		pageRight = "7";

	String notes = "";
	String statusDD = "";
	String statusCode = "";
	String statusDate = "";

	mode = request.getParameter("mode");
	if( (mode == null)  || (mode.equals("null")))
	{
		 mode="N";
	}
	if (EJBUtil.isEmpty(statusCodeType))
		statusCodeType = "";


	CodeDao cdStatus = new CodeDao();
	
	cdStatus.getCodeValues(statusCodeType);
	
	cdStatus.setCType(statusCodeType);
	cdStatus.setForGroup(defUserGroup);
	        
	ArrayList cDesc = new ArrayList();
		cDesc = cdStatus.getCDesc();
	ArrayList arCid = new ArrayList();
		arCid = cdStatus.getCId();


	if(mode.equals("M"))
	{
	 		statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
			statusCode = statusB.getStatusCodelstId();

			notes = statusB.getStatusNotes();

			if (EJBUtil.isEmpty(notes))
			notes = "";
			statusDate = statusB.getStatusStartDate();


	}
	StringBuffer mainStr = new StringBuffer();
		int pcounter = 0;
		Integer codeIdTemp = null;
		Integer codeIdSelTemp = null;


		 if(mode.equals("M")){
			 //codeIdSelTemp = Integer.valueOf(statusCode);
			 //mainStr.append("<SELECT NAME='status'  disabled>") ;
			 
			 statusDD =cdStatus.toPullDown("status",EJBUtil.stringToNum(statusCode), "disabled");
			 
			}
		 else{
			//mainStr.append("<SELECT NAME='status'  >") ;
			statusDD =cdStatus.toPullDown("status");

		 }

		/*for (pcounter = 0; pcounter  <= cDesc.size() -1 ; pcounter++){

			codeIdTemp = (Integer) arCid.get(pcounter);
			if (codeIdSelTemp!=null && codeIdSelTemp.intValue() == codeIdTemp.intValue())
			{
				mainStr.append("<OPTION value = "+ arCid.get(pcounter)+" SELECTED  >" + cDesc.get(pcounter)+ "</OPTION>");
				//Added by Ganapathy on 04-15-05
				strStatus=cDesc.get(pcounter).toString();
			}
			else
			{
				mainStr.append("<OPTION value = "+ arCid.get(pcounter)+">" + cDesc.get(pcounter)+ "</OPTION>");
			}

		}
		mainStr.append("</SELECT>");


		statusDD = mainStr.toString(); */

	//statusDD = cdStatus.toPullDown("status",EJBUtil.stringToNum(statusCode));


  %>
<!--<DIV class="popDefault" id="div1">
<form name="status" action="updatestatus.jsp"  onsubmit="return validate(document.status)" >

		<input name="moduleTable" type=hidden value=<%--=moduleTable--%>>
		<input name="modulePk" type=hidden value=<%--=modulePk--%>>
		<input name="mode" type=hidden value=<%--=mode--%>>
		<input name="statusId" type=hidden value=<%--=statusId--%>>
	   <input name="statCode" type=hidden value=<%--=statusCode--%>>
<p class="sectionHeadings"><%--=sectionHeading--%></p>		-->
		 <!--Change by Ganapathy on 04-15-05-->
<!--<table width="90%" cellspacing="0" cellpadding="0">
	 <tr>
		 <td>STATUS <FONT class="Mandatory">*</FONT></td>
		 <td><%--=statusDD--%></td>

	 </tr>
	 <tr>
	  <td >DATE <FONT class="Mandatory">* </FONT></td>
	  <td ><input name="statusDate" type=text value=<%--=statusDate--%>>
	  &nbsp&nbsp&nbsp<A href=# onClick="return fnShowCalendar(document.status.statusDate)"> <input type="image" src="./images/calendar.jpg" align="absmiddle" border="0"></A>
	  </td>
	 </tr>
	  <tr>
	  <td >NOTES</td>
	  <td><TEXTAREA name="notes" rows=3 cols=30><%--=notes--%></TEXTAREA></td></td>
	  </tr>
  </table>-->

  <%if(EJBUtil.stringToNum(pageRight) >= 6){%>
  <DIV class="popDefault" id="div1">

	<%  //Added by Ganapathy on 04-15-05
	//JM: 11/18/2005: Study no and Study version number added for display
	if(strStatus.equals("Freeze") && mode.equals("M"))
	{
	out.println("<font class='Mandatory'>"+MC.M_CntChg_InFreezeVer/* You cannot make any changes in a Freeze Version.*****/+"</font>");
	%>
	<p class="sectionHeadings"><%=sectionHeading%></p>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
	<tr >

		<td colspan=2><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNo%></td>
	</tr>
	 <tr>
		<td colspan=2><%=LC.L_Version_Number%><%-- Version Number*****--%>: <%=verNumber%></td>
	</tr>
	 <tr>
		<td width="15%"> <%=LC.L_Status%><%-- Status*****--%>  <FONT class="Mandatory">*</FONT></td>
		<td ><%=statusDD%></td>
	 </tr>
	 <tr>
	  <td><%=LC.L_Date%><%-- Date*****--%> <FONT class="Mandatory">* </FONT></td>
	  <td><input name="statusDate" type="text"  value=<%=statusDate%>></td>
	 </tr>
	  <tr>
	  <td ><%=LC.L_Notes%><%-- Notes*****--%></td>
	  <td><TEXTAREA name="notes"  rows=3 cols=30><%=notes%></TEXTAREA></td>
	  </tr>
	  </table>
	  <%
	  }else{
	  %>

<form name="status" id = "statusfrm" method="POST" action="updatestatus.jsp"  onsubmit="if (validate(document.status)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<input name="moduleTable" type=hidden value=<%=moduleTable%>>
	<input name="modulePk" type=hidden value=<%=modulePk%>>
	<input name="mode" type=hidden value=<%=mode%>>
	<input name="statusId" type=hidden value=<%=statusId%>>
	<input name="statCode" type=hidden value=<%=statusCode%>>
	<p class="sectionHeadings"><%=sectionHeading%></p>

	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl">	
	<% if (moduleTable.equals("er_invoice")){%>
	 <tr>                                <!-- Modified By Parminder Singh Bug#10127-->
	 <%-- Bug#11732-- Raviesh --%>
		<td colspan=2><%=LC.L_Inv_Number%><%-- Invoice Number*****--%>: <%=invNumber%></td>
	</tr>
	<%}%>
	<% if (moduleTable.equals("er_studyver")){%>
	 <tr >
		<td colspan=2><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNo%></td>
	</tr>
		 <tr>
		<td colspan=2><%=LC.L_Version_Number%><%-- Version Number*****--%>: <%=verNumber%></td>
	</tr>
	<%}%>
	 <tr>
		 <td><%=LC.L_Status%><%-- Status*****--%> <FONT class="Mandatory">*</FONT></td>
		 <td><%=statusDD%></td>

	 </tr>
<%-- INF-20084 Datepicker-- AGodara --%>
	 <tr>
	  <td><%=LC.L_Date%><%-- Date*****--%> <FONT class="Mandatory">* </FONT></td>
	  <td><input name="statusDate" type=text class="datefield" value= "<%=statusDate%>">
	
	  <!--//JM: 01.27.2006 for #2488: to solve "Please enter data in all mandatory fields" which was coming on clicking the calendar icon -->
	  <!--input type="image" src="./images/calendar.jpg" align="absmiddle" border="0"--></A>
	  </td>
	 </tr>
	  <tr>
	  <td ><%=LC.L_Notes%><%-- Notes*****--%></td>
	  <td><TEXTAREA name="notes" rows=3 cols=30><%=notes%></TEXTAREA></td>
	  </tr>
	<tr><td colspan=2>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="statusfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
   	</td>
	</tr>
    <%}%>

   </table>
    </form>

   <%}//////////////page right
   %>


</div>
<%
  }//end of if body for session

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>