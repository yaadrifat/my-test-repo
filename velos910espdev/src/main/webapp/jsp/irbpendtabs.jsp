<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studymod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyTeam" scope="request" class="com.velos.eres.web.team.TeamJB"/>

<%!
  private String getHrefBySubtype(String subtype, String mode) {
      if (subtype == null) { return "#"; }
      if ("pend_menu".equals(subtype)) {
          return "irbpenditems.jsp?selectedTab=irb_act_tab&mode=N";
      }
      if ("irb_act_tab".equals(subtype)) {
          return "irbpenditems.jsp?selectedTab=irb_act_tab&mode=N";
      }
      if ("irb_items_tab".equals(subtype)) {
          return "irbpenditems.jsp?selectedTab=irb_items_tab&mode=N";
      }
      if ("irb_saved_tab".equals(subtype)) {
          return "irbpenditems.jsp?selectedTab=irb_saved_tab&mode=N";
      }
      if ("irb_appr_tab".equals(subtype)) {
          return "irbpenditems.jsp?selectedTab=irb_appr_tab&mode=N";
      }
      return "#";
  }
%>

<%
String mode="N";
String selclass;
 
String study="";	
String studyNo="";
String verNumber = ""  ; 
String userId = "";
String studyFromSession = "";
String userIdFromSession = "";
String accId = "";

String tab= request.getParameter("selectedTab");
String from= request.getParameter("from");
mode= request.getParameter("mode");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
 	userIdFromSession= (String) tSession.getValue("userId");
	accId = (String) tSession.getValue("accountId");
	
	int pendingGrpRight = 0;
	int irbActionReqGroupRight=  0;
	int irbPendingRevGroupRight  = 0;
	
			  
	//copy a new study is controlled by Manage Protocols new group right
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pendingGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_APP"));

	irbActionReqGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_AR"));
	irbPendingRevGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_PR"));
	
	
	studyNo = (String) tSession.getValue("studyNo");
	verNumber = request.getParameter("verNumber");
	userId = (String) tSession.getValue("userId");

 
if (mode == null){
 	mode = "N";
}else if (mode.equals("M")){
		 
 	 
	
	
}
 

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");
String uName = (String) tSession.getValue("userName");
%>
<!-- <P class = "userName"><%= uName %></p> -->
<%
 

ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_pend_tab");

%>
<DIV>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">   --> 
<table cellspacing="0" cellpadding="0" border="0"> 
	<tr>
    <%
    // check the rights
 	// To check for the account level rights
	String modRight = (String) tSession.getValue("modRight");
	 int patProfileSeq = 0, formLibSeq = 0;					
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 
	 

	 for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	    // If this tab is configured as invisible in DB, skip it
	    if ("0".equals(settings.getObjVisible())) {
	        continue;
	    }
	    // Figure out the access rights
        boolean showThisTab = false;
        
         
			
		if ("form_tab".equals(settings.getObjSubType())) {
		    if (String.valueOf(formLibAppRight).compareTo("1") == 0) {
		        showThisTab = true;
		    }
		} 
		else if ("irb_saved_tab".equals(settings.getObjSubType()) && pendingGrpRight >=4 )
		{
			showThisTab = true;
		}
		else if ("irb_act_tab".equals(settings.getObjSubType()) && irbActionReqGroupRight >=4 &&
		        pendingGrpRight >=4)
		{
			showThisTab = true;
		}
		else if ("irb_items_tab".equals(settings.getObjSubType()) && irbPendingRevGroupRight >= 4 &&
		        pendingGrpRight >=4)
		{
			showThisTab = true;
		}
		else if ("irb_appr_tab".equals(settings.getObjSubType()) && pendingGrpRight >=4)
		{
			showThisTab = true;
		}
		 
	    if (!showThisTab) { continue; } // no access right; skip this tab
	    // Figure out the selected tab position
        if (tab == null) { 
            selclass = "unselectedTab";
	    } else if (tab.equals(settings.getObjSubType())) {
            selclass = "selectedTab";
        } else {
            selclass = "unselectedTab";
        }
    %>
		<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				 <!-- <td class="<%=selclass%>" rowspan="3" valign="top" >  
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td> --> 
					<td class="<%=selclass%>">
						<a href="<%=getHrefBySubtype(settings.getObjSubType(),mode)%>"><%=settings.getObjDispTxt()%></a>
					</td> 
				   <!--  <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>  --> 
			  	</tr> 
		   	</table> 
        </td>
    <%
     } // End of tabList loop
    %>
		
   	</tr>
 <!--   <tr>
     <td colspan=10 height=10></td>
  </tr>  --> 
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
<%		
} //session time out.
%>
</DIV> 
