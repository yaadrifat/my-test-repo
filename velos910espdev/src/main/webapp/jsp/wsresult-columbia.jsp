<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>

<%@page import="java.util.ArrayList"%><HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>

<%@ page import="com.velos.eres.business.common.CtrlDao"%>
<%@ page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id="wsLookupProxy" scope="session" class="com.ngs.eot.ws.EotWSLookupProxy" />
<%
String ctrlKey="'WSUSER','WSPASSWORD','WSURL'";
String wsUrl="",wsPwd="",wsUser="",showMessage="";
CtrlDao ctlDao=new CtrlDao();
ctlDao.getControlValues(ctrlKey);
ArrayList wsValues=ctlDao.getCValue();
ArrayList wsKeys=ctlDao.getCKey();
wsUser=(String)wsValues.get(wsKeys.indexOf("WSUSER"));
wsPwd=(String)wsValues.get(wsKeys.indexOf("WSPASSWORD"));
wsUrl=(String)wsValues.get(wsKeys.indexOf("WSURL"));
wsLookupProxy.setEndpoint(wsUrl);

String eotProfileName="UI 4.x Prod";
%>

<%

try {
        String root=  request.getParameter("hCFacility");

        String description_15id=  request.getParameter("description129");
            java.lang.String description_15idTemp = null;

        %>
        <jsp:useBean id="wsInstanceIdentifierStatus" scope="session" class="com.ngs.eot.ws.InstanceIdentifierStatus" />
        <%

        String assigningAuthorityName= "";

        String extension=  request.getParameter("MRN");
        extension=(extension==null)?"":extension;
        %>
        <jsp:useBean id="wsInstanceIdentifier" scope="session" class="com.ngs.eot.ws.InstanceIdentifier" />
        <%
        wsInstanceIdentifier.setRoot(root);
        wsInstanceIdentifier.setStatus(wsInstanceIdentifierStatus);
        wsInstanceIdentifier.setAssigningAuthorityName(assigningAuthorityName);
        wsInstanceIdentifier.setExtension(extension);
        com.ngs.eot.ws.ObjectDetail objectDetail = wsLookupProxy.getObjectDetail(wsUser,eotProfileName,wsInstanceIdentifier);

%>
<TABLE>
<TR>
<TD WIDTH="5%"></TD>
<%
String firstName="",lastName="",dobStr="",race="",eth="",sex="",personId="";
String add1 = "",add2 = "",city = "",state = "",zip = "", country = "",county = "",bphone = "", hphone= "", email = "";
java.util.Date dob=null;
boolean _ret=false;

if(objectDetail != null){
com.ngs.eot.ws.Person perObject=objectDetail.getPerson();

if(perObject != null){
_ret=true;
com.ngs.eot.ws.Name[] nameArr=perObject.getName();
com.ngs.eot.ws.CodedWithEquivalents[] raceArr=perObject.getRaceCode();
com.ngs.eot.ws.CodedWithEquivalents[] ethArr=perObject.getEthnicGroupCode();
com.ngs.eot.ws.CodedWithEquivalents genderObj=perObject.getAdministrativeGenderCode();
com.ngs.eot.ws.InstanceIdentifier[] idObj=perObject.getId();
com.ngs.eot.ws.Address[] addArr=perObject.getAddr();

java.util.Calendar cal= perObject.getBirthTime();
dob=	cal.getTime();

java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM/dd/yyyy");

try {
    dobStr = sdf.format(dob);
} catch (Exception e) {
    e.printStackTrace();
}

if ((nameArr!=null) && (nameArr.length>0)) {
	com.ngs.eot.ws.Name nameObj=(com.ngs.eot.ws.Name) nameArr[0];

	if ((nameObj.getGivenName()!=null) && (nameObj.getGivenName().length>0))
	 firstName=nameObj.getGivenName(0);
	if ((nameObj.getFamilyName()!=null) && (nameObj.getFamilyName().length>0))
	 lastName=nameObj.getFamilyName(0);
 }

if ((raceArr!=null) && (raceArr.length>0)) {
		com.ngs.eot.ws.CodedWithEquivalents raceObj=(com.ngs.eot.ws.CodedWithEquivalents) raceArr[0];
	  race=raceObj.getCode();
 }
if ((ethArr!=null) && (ethArr.length>0)) {
	com.ngs.eot.ws.CodedWithEquivalents ethObj=(com.ngs.eot.ws.CodedWithEquivalents) ethArr[0];
	  eth=ethObj.getCode();
 }

if (genderObj!=null) sex=genderObj.getCode();

if ((idObj!=null) && (idObj.length>0))
{
	com.ngs.eot.ws.InstanceIdentifier id=(com.ngs.eot.ws.InstanceIdentifier) idObj[0];
	personId=id.getExtension();
}

if ((addArr!=null) && (addArr.length>0))
{
	com.ngs.eot.ws.Address add=(com.ngs.eot.ws.Address) addArr[0];
	if (add!=null)
	{
		String[] addArray= (String[])add.getStreetAddressLine();
		if (addArray!=null) {
		if (addArray.length >0) add1 = add.getStreetAddressLine(0);
		if (addArray.length>1) add2 = add.getStreetAddressLine(1);
		}
		city = add.getCity();
		state = add.getState();
		zip = add.getPostalCode();
		country = add.getCountry();
		county = add.getCounty();

	}
}








}
eth=(eth==null)?"":eth;
race=(race==null)?"":race;
personId=(personId==null)?"":personId;

}else {
	showMessage="No "+LC.Pat_Patient+" Match Found for the Specified MRN and Organization.";
}



   %>
<table width="100%" style="Font:arial;Font-size:12;font-weight:bold">
<% if (showMessage.length()>0) { %>
<tr width="100%">
<td colspan="3"><%=showMessage%></td>
</tr>
<%} if (personId.length()>0){ %>
<tr width="100%">
<td width="33%">Given Name:<%=firstName%></td><td align="center" width="33%">Last Name:<%=lastName %></td><td width="33%" align="right">Date Of Bith:<%=dobStr%></td>
</tr>
<tr width="100%">
<td width="33%">Gender:<%=sex%></td><td align="center" width="33%">Race:<%=race%></td><td width="33%" align="right">Blood Type:</td>
</tr>
<tr width="100%">
<td width="33%">Birth City:</td><td align="center" width="33%">Birth State:</td><td width="33%" align="right">Nationality:</td>
</tr>
<tr width="100%">
<td width="33%">Address 1:<%=add1%></td><td align="center" width="33%">Address 2:<%=add2%></td><td width="33%" align="right">City:<%=city%></td>
</tr>
<tr width="100%">
<td width="33%">County:<%=county%></td><td align="center" width="33%">State:<%=state%></td><td width="33%" align="right">Country:<%=country%></td>
</tr>
<tr width="100%">
<td width="33%">Zip:<%=zip%></td><td align="center" width="33%"></td><td width="33%" align="right"></td>
</tr>
<%} %>
</table>

<TD>
<input type="hidden" id="wsDOB" value=<%=dobStr%>>
<input type="hidden" id="wsgivenName" value="<%=firstName%>">
<input type="hidden" id="wslastName" value="<%=lastName%>">
<input type="hidden" id="wssex" value="<%=sex%>">
<input type="hidden" id="wsbirthCity" value="">
<input type="hidden" id="wsbirthState" value="">
<input type="hidden" id="wsnatnlty" value="">
<input type="hidden" id="wsrace" value="<%=race%>">
<input type="hidden" id="wseth" value="<%=eth%>">
<input type="hidden" id="wsdeathIndicate" value="">
<input type="hidden" id="wsbloodType" value="">
<input type="hidden" id="wspersonId" value="<%=personId%>">
<input type="hidden" id="wsadd1"  size = 30  MAXLENGTH = 100 value="<%=add1%>">
<input type="hidden" id="wsadd2"  size = 30  MAXLENGTH = 100 value="<%=add2%>">
<input type="hidden" id="wscity"  size = 20 value="<%=city%>" >
<input type="hidden" id="wsstate"  value="<%=state%>">
<input type="hidden" id="wscounty" size = 20 value="<%=county%>">
<input type="hidden" id="wszip" value="<%=zip%>" >
<input type="hidden" id="wscountry"  size = 30 value="<%=country%>">
<input type="hidden" id="wshphone"  size = 30 >
<input type="hidden" id="wsbphone"  size = 30 >
<input type="hidden" id="wsemail"  size = 30 >

</TD></TR>
</TABLE>
<%


} catch (Exception e) {
%>
exception: <%= e %>
<%
return;
}

%>
</BODY>
</HTML>