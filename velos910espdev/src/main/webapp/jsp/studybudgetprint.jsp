<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Std_Bgt%><%--<%=LC.Std_Study%> Budget*****--%></title>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.budgetcal.BudgetcalStateKeeper,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>


<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" style = "overflow:auto">

<%
HttpSession tSession = request.getSession(true);

String userIdFromSession = (String) tSession.getValue("userId");
String acc = (String) tSession.getValue("accountId");

if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
	String calId = request.getParameter("bgtcalId");
	int bgtcalId = EJBUtil.stringToNum(calId);

	String type = request.getParameter("type");
	SchCodeDao schCodeDao = new SchCodeDao();

	BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
	ArrayList bgtNames = budgetDao.getBgtNames();
	ArrayList bgtStats = budgetDao.getBgtStats();
	ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
	ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
	ArrayList bgtSites = budgetDao.getBgtSites();
	ArrayList bgtCurrs = budgetDao.getBgtCurrs();

	boolean sectionFlag = true;

	String bgtName = (String) bgtNames.get(0);
	String bgtStat = (String) bgtStats.get(0);
	String bgtStudyTitle = (String) bgtStudyTitles.get(0);
	String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
	String bgtSite = (String) bgtSites.get(0);
	String bgtCurr = (String) bgtCurrs.get(0);

	SchCodeDao schDao = new SchCodeDao();
	bgtCurr = schDao.getCodeDescription(EJBUtil.stringToNum(bgtCurr));

	bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
	bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
	if(bgtStat.equals("F")) {
		bgtStat = LC.L_Freeze/*Freeze*****/;
	} else if(bgtStat.equals("W")) {
		bgtStat = LC.L_Work_InProgress/*Work in Progress*****/;
	} else if(bgtStat.equals("T")) {
		bgtStat = LC.L_Template/*Template*****/;
	}

	bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
	bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
	bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;

	BudgetcalDao budgetcalDao = budgetcalB.getBgtcalDetail(bgtcalId);
	ArrayList bgtcalNames = budgetcalDao.getBgtcalProtNames();
	String calName = (String) bgtcalNames.get(0);

	LineitemDao lineitemDao = lineitemB.getLineitemsOfBgtcal(bgtcalId);
	ArrayList lineitemIds = lineitemDao.getLineitemIds();
	ArrayList lineitemNames = lineitemDao.getLineitemNames();
	ArrayList lineitemDescs = lineitemDao.getLineitemDescs();
	ArrayList lineitemSponsorUnits = lineitemDao.getLineitemSponsorUnits();
	ArrayList lineitemClinicNOfUnits = lineitemDao.getLineitemClinicNOfUnits();
	ArrayList lineitemDelFlags = lineitemDao.getLineitemDelFlags();
	ArrayList lineitemCategories = lineitemDao.getLineitemCategories();
	ArrayList bgtSectionIds = lineitemDao.getBgtSectionIds();
	ArrayList bgtSectionIntervals = lineitemDao.getBgtSectionNames();
	ArrayList bgtSectionVisits = lineitemDao.getBgtSectionVisits();
	ArrayList bgtSectionDelFlags = lineitemDao.getBgtSectionDelFlags();
	ArrayList bgtSectionSequences = lineitemDao.getBgtSectionSequences();





	int rows = 0;
	for(int i=0;i<lineitemDao.getBRows();i++) {
		if(lineitemNames.get(i) != null && !lineitemNames.get(i).equals("")){
			rows++;
			}
		}

	int totalrows = lineitemDao.getBRows();
	int lineitemId = 0;
	String lineitemName = "";
	String lineitemDesc = "";
	float lineitemSponsorUnit = 0;
	float lineitemClinicNOfUnit = 0;
	float lineitemOtherCost = 0;
	String lineitemDelFlag = "";
	int bgtSectionId = 0;
	int bgtSectionIdOld = 0;
	String bgtSectionInterval = "";
	String bgtSectionVisit = "";
	String bgtSectionDelFlag = "";
	String bgtSectionSequence = "";
	String categoryDesc = "";
	String categoryId =  "";
	int iCatId = 0;
	int sectionRowsIndex = 0;

	float totalCost = 0, subTotal = 0, netTotal = 0, indirectCost = 0, grandTotal = 0;

	budgetcalB.setBudgetcalId(bgtcalId);
	BudgetcalStateKeeper bcsk = budgetcalB.getBudgetcalDetails();
	String indirectCostPer = budgetcalB.getIndirectCost();

	indirectCostPer = (indirectCostPer == null)?"0.0":indirectCostPer;



	budgetcalB.setBudgetcalId(bgtcalId);
	calName = (calName == null)?"-":calName;

	StringBuffer htmlString=new StringBuffer();

	htmlString.append("<BR>");
	htmlString.append("<Form name='budget' method='post' action=''>");
	htmlString.append("<table width='100%' cellspacing='0' cellpadding='0'>");
	htmlString.append("<tr>");
	htmlString.append("<td class=tdDefault width='50%'>");
	htmlString.append("<B>");
	htmlString.append(LC.L_Budget_Name/*Budget Name*****/+":</B>&nbsp;&nbsp;" +bgtName + "</td>");
	htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Organization/*Organization*****/+":</B>&nbsp;&nbsp;"+bgtSite+"</td>");
	htmlString.append("</tr>");
	htmlString.append("<tr>");
	htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Study_Number/*LC.Std_Study+" Number*****/+":</B>&nbsp;&nbsp;"+bgtStudyNumber+"</td>");
	htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Bgt_Status/*Budget Status*****/+":</B>&nbsp;&nbsp;"+bgtStat+"</td>");
	htmlString.append("</tr>");
	htmlString.append("<tr>");
	htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Study_Title/*LC.Std_Study+" Title*****/+":</B>&nbsp;&nbsp;"+bgtStudyTitle+"</td>");
	htmlString.append("</tr>");
	htmlString.append("</table>");
	htmlString.append("</FORM>");

	htmlString.append("<Form name='bgtprot' method=post action=''>");
	htmlString.append("<input type=hidden name=rows value="+rows+">");
	htmlString.append("<table width='100%' cellspacing='3' cellpadding='0' border=0>");
	htmlString.append("<tr>");
	htmlString.append("<th width=20% align =center>"+LC.L_Type/*Type*****/+"</th>");
	htmlString.append("<th width=15% align =center>"+LC.L_Category/*Category*****/+"</th>");
	htmlString.append("<th width=15% align =center>"+LC.L_Description/*Description*****/+"</th>");
	htmlString.append("<th width=15% align =right>"+LC.L_Unit_Cost/*Unit Cost*****/+"</th>");
	htmlString.append("<th width=15% align =right>"+LC.L_NumOfUnits/*No. of Units*****/+"</th>");
	htmlString.append("<th width=20% align =right>"+LC.L_Total_Cost/*Total Cost*****/+"</th>");
	htmlString.append("</tr>");



	bgtSectionIdOld = ((Integer) lineitemIds.get(0)).intValue();
	for(int i=0;i<totalrows;i++) {
		lineitemId = ((Integer) lineitemIds.get(i)).intValue();
		lineitemName = (String) lineitemNames.get(i);
		lineitemDesc = (String) lineitemDescs.get(i);
		lineitemSponsorUnit = EJBUtil.stringToFloat((String) lineitemSponsorUnits.get(i));
		lineitemClinicNOfUnit = EJBUtil.stringToFloat((String) lineitemClinicNOfUnits.get(i));
		categoryId = (String) lineitemCategories.get(i);
		iCatId = EJBUtil.stringToNum(categoryId);

		totalCost = (lineitemSponsorUnit * lineitemClinicNOfUnit);

		bgtSectionId = ((Integer) bgtSectionIds.get(i)).intValue();
		bgtSectionInterval = (String) bgtSectionIntervals.get(i);
		bgtSectionVisit = (String) bgtSectionVisits.get(i);
		bgtSectionDelFlag = (String) bgtSectionDelFlags.get(i);
		bgtSectionSequence = (String) bgtSectionSequences.get(i);

   lineitemDesc = (lineitemDesc == null)?"-":lineitemDesc;
   bgtSectionInterval = (bgtSectionInterval == null)?"-":bgtSectionInterval;
   bgtSectionVisit = (bgtSectionVisit == null)?"-":bgtSectionVisit;







		if(bgtSectionIdOld != bgtSectionId) {
			if(i>0 && sectionFlag == true){
				sectionFlag = false;


			htmlString.append("<td class=tdDefault align=right>");
			htmlString.append("<B>"+LC.L_Sub_Total/*Sub-Total*****/+"</B>");
			htmlString.append("</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td class=tdDefault align=right>" + bgtCurr + " " + EJBUtil.format(EJBUtil.floatToString(new Float(subTotal)),2,".") + "</td>");
			subTotal = 0;
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("</tr>");
			}

			htmlString.append("<tr>");
			htmlString.append("<td class=tdDefault><B>"+bgtSectionInterval+",&nbsp;"+bgtSectionVisit+"</B>");
			htmlString.append("</td>");
			htmlString.append("<td colspan=3>&nbsp;");
			htmlString.append("</td>");
			htmlString.append("</tr>");
		}
		if(lineitemName != null && !lineitemName.equals("")) {
			categoryDesc = schCodeDao.getCodeDescription(EJBUtil.stringToNum(categoryId));
			if(categoryDesc == null)
					categoryDesc = "-";
			htmlString.append("<tr>");
			htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+lineitemName+"</td>");
			htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+categoryDesc+"</td>");
			htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+lineitemDesc+"</td>");
			htmlString.append("<td class=tdDefault align=right>"+ EJBUtil.format(EJBUtil.floatToString(new Float(lineitemSponsorUnit)),2,".") +"</td>");
			htmlString.append("<input type=hidden name=sponsorCost size=5 maxlength=10 value="+ lineitemSponsorUnit+">");
			htmlString.append("<td class=tdDefault align=right>"+ EJBUtil.format(EJBUtil.floatToString(new Float(lineitemClinicNOfUnit)),2,".") +"</td>");
			htmlString.append("<input type=hidden name=clinicCost size=5 maxlength=10 value="+lineitemClinicNOfUnit+">");
			htmlString.append("<td class=tdDefault align=right>"+ EJBUtil.format(EJBUtil.floatToString(new Float(totalCost)),2,".") +"</td>");
			htmlString.append("</tr>");

			sectionFlag = true;
			subTotal = subTotal + totalCost;
			netTotal = netTotal + totalCost;
		}
			bgtSectionIdOld = bgtSectionId;
	} //for loop totalrows
	if(sectionFlag == true){
				sectionFlag = false;



			htmlString.append("<td class=tdDefault align=right>");
			htmlString.append("<B>"+LC.L_Sub_Total/*Sub-Total*****/+"</B>");
			htmlString.append("</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("<td class=tdDefault align=right>" + bgtCurr + " " + EJBUtil.format(EJBUtil.floatToString(new Float(subTotal)),2,".") + "</td>");
			subTotal = 0;
			htmlString.append("<td>&nbsp;</td>");
			htmlString.append("</tr>");
			}




htmlString.append("<tr>");
htmlString.append("<td class=tdDefault width=20% align =right><B>"+LC.L_Total/*Total*****/+"</B></td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td class=tdDefault align=right>" + bgtCurr + " " + EJBUtil.format(EJBUtil.floatToString(new Float(netTotal)),2,".") + "</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("</tr>");



indirectCost = (new Double((netTotal * Float.parseFloat(indirectCostPer)) / 100.0)).floatValue();

htmlString.append("<tr>");
htmlString.append("<td class=tdDefault align =right><B>"+LC.L_Indirect_Costs/*Indirect Costs*****/+"</B></td>");
htmlString.append("<td class=tdDefault align=left>&nbsp;&nbsp;" + EJBUtil.format(EJBUtil.floatToString(new Float(indirectCostPer)),2,".") + "% "+LC.L_Of_Total/*of Total*****/+"</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td class=tdDefault align=right>" + bgtCurr + " " + EJBUtil.format(EJBUtil.floatToString(new Float(indirectCost)),2,".") + "</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("</tr>");

grandTotal = netTotal + indirectCost;

htmlString.append("<tr>");
htmlString.append("<td class=tdDefault width=20% align =right><B>"+LC.L_Grand_Total/*Grand Total*****/+"</B></td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("<td class=tdDefault align=right>" + bgtCurr + " " + EJBUtil.format(EJBUtil.floatToString(new Float(grandTotal)),2,".") + "</td>");
htmlString.append("<td>&nbsp;</td>");
htmlString.append("</tr>");

htmlString.append("</table>");
htmlString.append("</form>");
%>


<%
String contents = null;
String str = htmlString.toString();
contents = ReportIO.saveReportToDoc(str,"xls","budgetexcel");
if(type.equals("E")) {
%>

<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>

<form name="loc" method="POST" >
	<input type=hidden name="contents" value=<%=contents%> />

		<script>
			self.location = window.document.loc.contents.value ;
		</script>
</form>
<%}%>
<%=htmlString%>
<%
} else {
%>
	<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
	}
%>
</body>

</html>





