<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<title> <%=LC.L_Add_MultiSpecimens%><%--Add Multiple Specimens*****--%></title>
<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*"%><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<!--  it has to be taken to stylesheet -->
<style>
 	.ui-autocomplete {
		max-height: 150px;
		overflow-y: auto;
		height:150px;
		position: absolute; cursor: default;
		background-color:#FFFFFF;
	}
	</style>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<script Language="javascript">
//Added by AK for INV9.1 enhancement:
function validateNumber(evt){
	if(evt.shiftKey) return false;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	//delete, backspace & arrow keys
	if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
		return true;

	if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
		return false;
	return true;
}
</script>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>


<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<%
HttpSession tSession = request.getSession(true);
StringBuffer urlParamSB = new StringBuffer();
if (sess.isValidSession(tSession)){

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	

%>
<script type="text/javascript">

//Added to support YUI feature for MultiSpecimens functionlity ...
function reloadMultiSpecimenGrid() {
	
	YAHOO.example.multiSpecimensGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "multiSpecimenGrid"
		};
	
    	myMultiSpecimensGrid = new VELOS.multiSpecimensGrid('fetchMultiSpecimensJSON.jsp', args);
		myMultiSpecimensGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadMultiSpecimenGrid();
});
</script>

<p class = "sectionHeadings" ><%=LC.L_Add_MultiSpecimens%><%--Add Multiple Specimens*****--%></P>
<DIV class="popDefault" id="div1" style="overflow:auto">
<table width="100%" >
	<tr>
		<td colspan="3" align = "left" ><p class="defComments"><%=MC.M_LveSpmenIdBlank_ToAutoId%><%--Please leave 'Specimen ID' blank to auto generate the ID*****--%>
		</p></td>
    </tr>
	<tr>
		<td>
			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
			<button id="save_changes" name="save_changes" onclick="VELOS.multiSpecimensGrid.saveDialog();"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>
		</td>
	</tr>
</table>
<form name="specimenHeader" id="specimenHeader" METHOD="POST">
<table width="98%" cellspacing="0" cellpadding="0" >
	 <tr>
		<td>
			<div id="Parent_multiSpecimenGrid"></div>
		</td>
    </tr>
</table>
<br/>
<table width="98%" cellspacing="0" cellpadding="0" >     
	<tr>
		<td align="left">
  		<button id="setToAll" type="button" name="setToAll" onclick="VELOS.multiSpecimensGrid.setToAll();"><%=LC.L_Set_ToAll%><%--Set To All*****--%></button>
        </td>
        <td align="right">
	  		<input id="rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>
			<span>
				<img id="addRows" name="addRows" title="<%=LC.L_Add%>" src="../images/add.png"
		    	onclick="if(eval(document.getElementById('rowCount').value) > 20){ alert('<%=MC.M_AddMax_20Rows%>')<%--alert('Please add maximum 20 rows at a time.')*****--%>; return;} if(eval(document.getElementById('rowCount').value) > 0){ VELOS.multiSpecimensGrid.addRows(); }"/> <%=LC.L_AddRow_S%><%--Add Row(s)*****--%>
			</span> 
			<span width="50%">&nbsp;</span>
	  	</td>
    </tr>
</table>
	</form>
	<form name="specimenChild" id="specimenChild" METHOD="POST">
	<table width="98%" cellspacing="0" cellpadding="0" >
	  <tr>
		<td>
			<div id="Child_multiSpecimenGrid"></div>
		</td>
	  </tr>
	</table>
	</form>
<div id="multiSpecimenGrid"></div>
</DIV>
<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</BODY>

</html>