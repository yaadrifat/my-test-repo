<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*, com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB" %>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<title><%=LC.L_Pwd_Expired%><%--Password Expired*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(btn)

   {
	
     formobj=document.pwd

     if (btn.id=='logout'){
        formobj.action="logout.jsp";
 		return true; 		
 	}
  	
     if(formobj.checkPass.value == "on") {


    //JM: 17OCT2006: added
	formobj.daysExp.value = fnTrimSpaces(formobj.daysExp.value);

	if (parseInt(formobj.daysExp.value) < 1 ){
			alert("<%=MC.M_PwdExp_Less1Day%>");/*alert("Password Expires can not be less than 1 day");*****/
			formobj.daysExp.focus();
			return false;
	}


	//Added by Manimaran to fix the Bug #2532
    	formobj.newPass.value=fnTrimSpaces(formobj.newPass.value);
      	formobj.oldPass.value=fnTrimSpaces(formobj.oldPass.value);
        formobj.confPass.value=fnTrimSpaces(formobj.confPass.value);

        if (!(validate_col('session',formobj.oldPass))) return false
	if (!(validate_col('session',formobj.newPass))) return false
	if (!(validate_col('session',formobj.confPass))) return false

	/*if( formobj.oldPass.value != fnTrimSpaces(formobj.userOldPwd.value)) {
	      alert("You have not entered the correct old password. Please try again. ");
	      formobj.oldPass.focus();
              return false;
	} */

	//Added by Manimaran for the issue #3993
	loginName = formobj.loginName.value;
	newPass = formobj.newPass.value;
	if (loginName==newPass) {
		alert("<%=MC.M_PwdCnt_PlsEtr%>");/*alert("Password cannot be same as User Name. Please enter again.");*****/
		formobj.newPass.focus();
		return false;
	}


	if( formobj.newPass.value.length < 8)
		{
			alert("<%=MC.M_NewPwd_Atleast8CharsLong%>");/*alert("The New Password should be atleast 8 characters long. Please enter again.");*****/
			formobj.newPass.focus();
			return false;
		}

	if( formobj.newPass.value != formobj.confPass.value)

		{

			alert("<%=MC.M_NewConfirm_PwdSame%>");/*alert("Values in 'New Password' and 'Confirm Password' are not same. Please enter again.");*****/
			formobj.newPass.focus();
			return false;

		}

	if( isNaN(formobj.daysExp.value) == true)
	{
	 alert("<%=MC.M_ExpDayMustNum_ReEtr%>");/*alert("Expiration days must be numeric. Please enter again.");*****/
	 formobj.daysExp.focus();
	 return false;
	}
	//JM: commented
	//if (formobj.daysExp.value==0) formobj.daysExp.value=365;
      }

     if(formobj.checkESign.value == "on") {

     	//Added by Manimaran to fix the Bug #2532
     	formobj.oldESign.value=fnTrimSpaces(formobj.oldESign.value);
     	formobj.newESign.value=fnTrimSpaces(formobj.newESign.value);


        if (!(validate_col('session',formobj.oldESign))) return false
	if (!(validate_col('session',formobj.newESign))) return false
	if (!(validate_col('session',formobj.confESign))) return false


	/*if( formobj.oldESign.value != fnTrimSpaces(formobj.userOldESign.value)) {
	      alert("You have not entered the correct old e-Signature. Please try again. ");
	      formobj.oldESign.focus();
              return false;
	} */

	if( isNaN(formobj.newESign.value) == true)
	{
	 alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("e-Signature must be numeric. Please enter again.");*****/
	 formobj.newESign.focus();
	 return false;
	}


	if( formobj.newESign.value.length < 4)
		{
			alert("<%=MC.M_NewEsign_Atleast4Chars%>");/*alert("The New e-Signature should be atleast 4 characters long. Please enter again.");*****/
			formobj.newESign.focus();
			return false;
		}


	if( formobj.newESign.value != fnTrimSpaces(formobj.confESign.value))

		{
			alert("<%=MC.M_NewConfirm_EsignSame%>");/*alert("Values in 'New e-Signature' and 'Confirm e-Signature' are not same. Please enter again.");*****/
			formobj.newESign.focus();
			return false;

		}


	//JM: 17OCT2006: added the segment
	formobj.daysExpESign.value = fnTrimSpaces(formobj.daysExpESign.value);

	if (parseInt(formobj.daysExpESign.value) < 1 ){
			alert("<%=MC.M_EsignExp_CntLess1Day%>");/*alert("e-Signature Expires can not be less than 1 day");*****/
			formobj.daysExpESign.focus();
			return false;
	}


	if( isNaN(formobj.daysExpESign.value) == true)
	{
	 alert("<%=MC.M_ExpDayMustNum_ReEtr%>");/*alert("Expiration days must be numeric. Please enter again.");*****/
	 formobj.daysExpESign.focus();
	 return false;
	}


      }


      if ((formobj.checkESign.value == "on") && (formobj.checkPass.value == "on")) {
      if( formobj.newESign.value==formobj.newPass.value)
		{
			alert("<%=MC.M_EsignCnt_AsPwd%>");/*alert("e-signature cannot be same as password. Please enter again.");*****/
			formobj.newESign.focus();
			return false;
		}
		}






     }

</SCRIPT>
<body>

<% String srcMenu;
srcMenu= request.getParameter("src");
%>

<DIV class="formDefault" id="div1">

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="com.velos.eres.web.user.UserJB"%>

  <%

int pageRight = 5; //all users can change their password

	HttpSession tSession = request.getSession(true);
	String strUserOldPwd = null;
	String strUserOldESign = null;
	ArrayList setvalueovers = null;

if (sessionmaint.isValidSession(tSession))

 {

//JM:
  	 String accId=(String)tSession.getValue("accountId");
   String uName = (String) tSession.getValue("userName");
   UserJB userB = (UserJB) tSession.getValue("currentUser");
   UserJB userB1 = (UserJB) tSession.getValue("currentUser");
  
	String accName = (String) tSession.getValue("accName");
   	accName=(accName==null)?"default":accName;
   	String accSkinId = "";
   	String usersSkinId = "";
   	String userSkin = "";
   	String accSkin = "";
   	String skin = "default";
   	accSkinId = 	(String) tSession.getValue("accSkin");
  
   	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );


	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){

		accSkin = "accSkin";

	}
	else{


		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){

		skin = userSkin;

	}
	%>
	<script> whichcss_skin("<%=skin%>");</script>
	<%

   //Date eSignExpDate = new Date(userB.getUserESignExpiryDate().substring(6) + "/" + userB.getUserESignExpiryDate().substring(0,2) + "/" + userB.getUserESignExpiryDate().substring(3,5));
   Date eSignExpDate = DateUtil.stringToDate(userB.getUserESignExpiryDate(),null);


   //Date pwdExpDate = new Date(userB.getUserPwdExpiryDate().substring(6) + "/" + userB.getUserPwdExpiryDate().substring(0,2) + "/" + userB.getUserPwdExpiryDate().substring(3,5));
   Date pwdExpDate = DateUtil.stringToDate(userB.getUserPwdExpiryDate(),null);


   Date d = new Date();


	 strUserOldPwd = userB.getUserPwd();
	 strUserOldESign = userB.getUserESign();
	 //String daysExpPass = userB.getUserPwdExpiryDays();
	 //String daysExpESign = userB.getUserESignExpiryDays();

   String loginName = userB.getUserLoginName();	//KM

   String checkPass = "off";
   String checkESign = "off";

   pageRight = 5;


    if (pageRight > 0 )

	{

	    String uSession  = (String) tSession.getValue("userSession");



%>
<BR>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=LC.L_PwdOrEsign_Exp%><%--Password/e-Signature Expired*****--%> </P>
  <Form name = "pwd" method="post" action="updatepwd.jsp?from=pwdexpired">
    <input type="hidden" name="userOldPwd" size = 20 value = "<%=strUserOldPwd%>">
    <input type="hidden" name="userOldESign" size = 20 value = "<%=strUserOldESign%>">
    <input type="hidden" name="loginName" size = 20 value = "<%=loginName%>">



<%

  String setvalueover = null;
  int modname = 1;
  String keyword = null;


  String daysExpPass = null;
  String daysExpESign = null;
  SettingsDao settingsDao=commonB.getSettingsInstance();

  if(d.after(pwdExpDate)) {
	checkPass = "on";
%>

    <table width="600" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <P class = "defComments">
            <%=MC.M_YourPwd_Expired%><%--Your Password has expired. Please enter the following details to create a new Password.*****--%>	</P>
        </td>
      </tr>
	<tr><td>&nbsp;</td></tr>
    </table>
    <table width="500" cellspacing="0" cellpadding="0">
      <tr>
        <td width="300"> <%=LC.L_Expiring_Pwd%><%--Expiring Password*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="password" name="oldPass" size = 15 MAXLENGTH = 20>
        </td>
      </tr>
      <tr>
        <td width="300"> <%=LC.L_New_Pwd%><%--New Password*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="password" name="newPass" size = 15 MAXLENGTH = 20>
        </td>
      </tr>
      <tr>
        <td width="300"> <%=LC.L_Confirm_NewPwd%><%--Confirm New Password*****--%> <FONT class="Mandatory">* </FONT>
        </td>
        <td>
          <input type="password" name="confPass" size = 15 MAXLENGTH = 20>
        </td>
      </tr>

		<!--KM-Message changed as per the new requirement(3993) -->
      <tr><td colspan=3><FONT class="Mandatory"><%=MC.M_PwdLoginName_Diff%><%--Password: Minimum 8 characters, cannot be same as Login Name,cannot be same as your old password*****--%></FONT></td></tr>
      <tr>
	<td width="300"> <%=MC.M_PwdExpires_AfterDays%><%--Number of days after which password expires*****--%></FONT>
        </td>

        <%  //JM: 102306

		setvalueover="1";

		keyword ="ACC_DAYS_PWD_EXP";
		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
		ArrayList pwsExpDays =settingsDao.getSettingValue();
		if (pwsExpDays.size() > 0)
		daysExpPass=(pwsExpDays.get(0)==null)?"":(pwsExpDays.get(0)).toString();

		keyword="ACC_DAYS_PWD_EXP_OVERRIDE";
		settingsDao.reset();
		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
		setvalueovers=settingsDao.getSettingValue();
		if(setvalueovers.size()>0)
			setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
		if(EJBUtil.stringToNum(setvalueover)==0){
		%>
	<td>
	  <input type="input" name="daysExp" size=15 MAXLENGTH=3 readonly value=<%=daysExpPass%>>
	</td>
	<%
	//JM:
	}else{%>
	<td>
	  <input type="input" name="daysExp" size=15 MAXLENGTH=3 value=<%=daysExpPass%>>
	</td>
	<%}%>
      </tr>
    </table>
<%
   } //end of if for d.after(pwdExpDate)
  if(d.after(eSignExpDate)) {
	checkESign = "on";
%>
<br>
    <table width="600" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <P class = "defComments">
            <%=MC.M_YourEsign_Expired%><%--Your e-Signature has expired. Please enter the following details to create a new e-Signature.*****--%>	</P>
        </td>
      </tr>
	<tr><td>&nbsp;</td></tr>
    </table>

    <table width="500" cellspacing="0" cellpadding="0">
      <tr>
        <td width="300"> <%=LC.L_Expiring_Esign%><%--Expiring e-Signature*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="password" name="oldESign" size = 15 MAXLENGTH = 8>
        </td>
      </tr>
      <tr>
        <td width="300"> <%=LC.L_New_Esign%><%--New e-Signature*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="password" name="newESign" size = 15 MAXLENGTH = 8>
        </td>
      </tr>
      <tr>
        <td width="300"> <%=LC.L_Confirm_NewEsign%><%--Confirm New e-Signature*****--%> <FONT class="Mandatory">* </FONT>
        </td>
        <td>
          <input type="password" name="confESign" size = 15 MAXLENGTH = 8>
        </td>
      </tr>
      <tr>
	<td width="300"> <%=MC.M_DaysAfter_EsignExp%><%--Number of days after which e-Signature expires*****--%> <!--For e-Signature never expires, enter 0.--></FONT>
        </td>


	<%

		settingsDao.reset();
		keyword ="ACC_DAYS_ESIGN_EXP";
		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
		ArrayList esignExpDays =settingsDao.getSettingValue();
		if (esignExpDays.size() > 0)
		daysExpESign=(esignExpDays.get(0)==null)?"":(esignExpDays.get(0)).toString();


		setvalueover="0";
		settingsDao.reset();
		keyword="ACC_DAYS_ESIGN_EXP_OVERRIDE";

		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
		setvalueovers = settingsDao.getSettingValue();
		if(setvalueovers.size()>0)
		setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
		  if(EJBUtil.stringToNum(setvalueover)==0){

		%>

	<td>
	  <input type="input" name="daysExpESign" size=15 MAXLENGTH=3 readonly value=<%=daysExpESign%>>
	</td>

	<%}else{%>
	<td>
	  <input type="input" name="daysExpESign" size=15 MAXLENGTH=3 value=<%=daysExpESign%>>
	</td>
	<%}%>
      </tr>
    </table>
<%
   } //end of if for d.after(eSignExpDate)
%>

	   <input type="hidden" name="checkPass" value=<%=checkPass%>>
	   <input type="hidden" name="checkESign" value=<%=checkESign%>>

    <br><br></br>
    <table width="500" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <%

    if (pageRight > 4 )

	{
%>
          <!--<input type="Submit" name="submit" value="Submit">-->
	  <input type="submit" class="ui-button" onClick = "return validate(this)" value="<%=LC.L_Submit%>"/>
	  <input type="submit" class="ui-button" id="logout" onClick = "return validate(this)" value="<%=LC.L_Logout%>"/> 	   
      <%

	}

else

	{

%>
          <!--<input type="Submit" name="submit" value="Submit" DISABLED>-->
          <%

	}

%>
        </td>
      </tr>
    </table>

  </Form>
  <%

	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class=login id="emenu">
</div>

</body>

</html>


