<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<HTML>

<HEAD><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<P>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY >
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*,com.velos.esch.web.eventresource.EventResourceJB"%>
<%@ page language = "java" import= "com.velos.eres.service.util.StringUtil" %>
<%@ page language = "java" import="com.velos.esch.audit.web.AuditRowEschJB" %>
<%
 int iret = -1;
 HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign))
	   {
		   String userId = (String) tSession.getValue("userId");
		   String ipAdd =  (String) tSession.getValue("ipAdd");
		   String calledFrom = request.getParameter("calledFrom");
		   if (calledFrom==null) calledFrom = "-";

		   String cmbView =request.getParameter("cmbView");

		   String[] statusId =request.getParameterValues("eventstatus");
		   String[] oldStatusId= request.getParameterValues("oldStatusId");
		   String[] eventSOSId =request.getParameterValues("ddSiteService");
		   String[] eventCoverage =request.getParameterValues("ddCoverageType");

		   String[] reasonForCoverageChng = request.getParameterValues("reasonForCoverageChange");//JM: 14Jun2010: #enh-D-FIN23



		   String[] note=request.getParameterValues("note");
		   String[] exeon=request.getParameterValues("exeon");
		   String enrollId = request.getParameter("patProtId");
	       String arrCheck= request.getParameter("arrCheck");

	       String studyId=request.getParameter("studyId");
	       studyId=(studyId==null)?"":studyId;

	       String calAssoc=request.getParameter("calassoc");
	       calAssoc=(calAssoc==null)?"":calAssoc;

		   String[] chkOldStatus = new String[10000];
		   String[] chkStatusId =new String[10000];
		   String[] chkEventIds = new String[10000];
		   String[] chkSOSId =new String[10000];
		   String[] chkCoverageId =new String[10000];
		   int viewLen =0;


		   int len = oldStatusId.length;

		   int ret=0;
		   int j=0;
		   String[] strChk = new String[len];
		   StringTokenizer saCheck = new StringTokenizer(arrCheck,",");
		   int chkNum = saCheck.countTokens();


			 for(int count=0;count<chkNum;count++)
			 {
				  strChk[j]= saCheck.nextToken();
				  j++;
			 }



		  if(cmbView.equals("visit") || cmbView.equals("")  ){
			String[]  chkVst = request.getParameterValues("chkVst");
			String[] eveIds = request.getParameterValues("eveIds");

		   //int viewLen = chkVst.length;//blocked
		   viewLen = chkVst.length;
			String[] strEventIds = new String[viewLen];
			int chkIds=0;
			int totalIds=0;
		    int index=0;
			int m=0;
			for(int i=0 ; i<chkNum ; i++)
			 {
				if(strChk[i].equals("1"))
				 {
					strEventIds[index] = eveIds[i];
					StringTokenizer saIds = new StringTokenizer(strEventIds[index],":");
					chkIds = saIds.countTokens();
					for(m=0;m<chkIds;m++)
					 {
						totalIds++;
					 }

				}
			 }
		 //String[] chkStatusId =new String[totalIds];
		 chkStatusId =new String[totalIds];
		 chkSOSId = new String[totalIds];
		 chkCoverageId = new String[totalIds];

		 String[] chkReasonForCoverageChng=new String[totalIds];

		 String[] chkNote=new String[totalIds];
		 String[] chkExeon=new String[totalIds];

		 //String[] chkOldStatus = new String[totalIds];
		   chkOldStatus = new String[totalIds];
		 //String[] chkEventIds = new String[totalIds];
		 chkEventIds = new String[totalIds];
		 int cnt=0;
	    int indx=0;
	 for(int i=0 ; i<chkNum ; i++)
	 {
		if(strChk[i].equals("1"))
		 {

			strEventIds[cnt] = eveIds[i];
			StringTokenizer saIds = new StringTokenizer(strEventIds[cnt],":");
			chkIds = saIds.countTokens();
			for(m=0;m<chkIds;m++)
			 {
				chkEventIds[indx] = saIds.nextToken();
				chkStatusId[indx] = statusId[i];
				chkExeon[indx]= exeon[i];

				if (!calAssoc.equals("S")){
					chkSOSId[indx] = eventSOSId[i];
					chkCoverageId[indx] = eventCoverage[i];
					//JM: 06Aug2010: #5062
					if (reasonForCoverageChng !=null)
					chkReasonForCoverageChng[indx]= reasonForCoverageChng[i];
					else
					chkReasonForCoverageChng[indx]= "";


				}else{

					chkSOSId[indx] = "";
					chkCoverageId[indx] = "";
					chkReasonForCoverageChng[indx]= "";
				}

				chkNote[indx] = note[i];
			    chkOldStatus[indx] = oldStatusId[i];

				indx++;
			 }
			cnt++;
		}
	 }

		if (calAssoc.equals("S"))
		    ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId),chkEventIds,chkStatusId,chkOldStatus,chkExeon,ipAdd,calAssoc);
		else
		ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(enrollId),EJBUtil.stringToNum(userId),chkEventIds,chkStatusId,chkOldStatus,chkExeon,ipAdd);
		
		String remarks = request.getParameter("remarks");
		if (!StringUtil.isEmpty(remarks)){
			for(int i=0 ; i<chkNum ; i++)
			 {
				if(strChk[i].equals("1"))
				 {
					String statId = chkStatusId[i];
					String oldStatId = chkOldStatus[i];
					
					StringTokenizer saIds = new StringTokenizer(eveIds[i],":");
					chkIds = saIds.countTokens();
					for(m=0;m<chkIds;m++)
					 {
						int evtId = StringUtil.stringToNum(saIds.nextToken());
						AuditRowEschJB schAuditJB = new AuditRowEschJB();
						schAuditJB.setReasonForChangeOfScheduleEvent(evtId, StringUtil.stringToNum(userId), remarks);
						
						boolean excludeCurrent = (!statId.equals(oldStatId))?true:false;
						schAuditJB.setReasonForChangeOfScheduleEventStatus(evtId, StringUtil.stringToNum(userId), 
							remarks, excludeCurrent);
					 }
				}
			 }
		 }
	}

	else if( cmbView.equals("event")){
		String[] chkEvent =request.getParameterValues("chkEvent");
		//int viewLen = chkEvent.length;//blocked
		viewLen = chkEvent.length;
		 String[] strStatusId =new String[viewLen];
		 String[] strNote=new String[viewLen];
		 String[] strSos=new String[viewLen];
		 String[] strCoverage=new String[viewLen];

		 String[] strExeon=new String[viewLen];
		 String[] strOldStatus = new String[viewLen];
		 String[] strEventIds = new String[viewLen];

		 String[] strReasonForCoverageChng=new String[viewLen];




	int cnt=0;
	 for(int i=0 ; i<chkNum ; i++)
	 {
		if(strChk[i].equals("1"))
		 {
			strStatusId[cnt] = statusId[i];
			strExeon[cnt] = exeon[i];
			strNote[cnt] = note[i];

			if (!calAssoc.equals("S")){
				strSos[cnt] = eventSOSId[i];
				strCoverage[cnt] = eventCoverage[i];


		    	//JM: 06Aug2010: #5062

					if (reasonForCoverageChng[i] !=null) {
						strReasonForCoverageChng[cnt] = reasonForCoverageChng[i];
					}else{
						strReasonForCoverageChng[cnt] = "";
					}

		    }else{
		    	strSos[cnt] = "";
				strCoverage[cnt] = "";
		    	strReasonForCoverageChng[cnt] = "";
		    }


		    strOldStatus[cnt] = oldStatusId[i];


			cnt++;

		}
	 }


		if (calAssoc.equals("S"))
		    ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId),chkEvent,strStatusId,strOldStatus,strExeon,strNote,strSos,strCoverage,strReasonForCoverageChng,ipAdd,calAssoc);
		else
		ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(enrollId),EJBUtil.stringToNum(userId),chkEvent,strStatusId,strOldStatus,strExeon,strNote,strSos,strCoverage, strReasonForCoverageChng,ipAdd);
		
		int index =0;
		String remarks = request.getParameter("remarks");
		if (!StringUtil.isEmpty(remarks)){
			for(int i=0 ; i<chkNum ; i++)
			 {
				if(strChk[i].equals("1"))
				 {
					AuditRowEschJB schAuditJB = new AuditRowEschJB();
					schAuditJB.setReasonForChangeOfScheduleEvent(StringUtil.stringToNum(chkEvent[index]), 
							StringUtil.stringToNum(userId), remarks);
					index++;
				}
			 }
		 }
	}



//JM: 06Aug2008 #3319

String[] chkEvent_res =request.getParameterValues("chkEvent");

	String eventId = "";
	int len_general = 0;

	if( cmbView.equals("event")){
		len_general = chkEvent_res.length;
	}else{
		len_general = chkEventIds.length;
	}


 for(int g=0 ; g<len_general; g++)	 { //for loop ---------------1 //how many events

	if( cmbView.equals("event")){
		eventId = chkEvent_res[g];
	}else{
	 	eventId = chkEventIds[g];
	}


	ArrayList evtStats = null;
    ArrayList cDescs = null;
    ArrayList cIds = null;
    ArrayList cDuration = null;

    String evtStat = "";
    String cDesc = "";
	int cId = 0;
	String cRoleDur = null;


	ArrayList evDuration = new ArrayList();
	String selTrackingId ="";
	String durationStr = null;
	String evtResTrackId = "";
	int eventStatusId = 0;

	EventResourceJB eventResB = new EventResourceJB();
	EventResourceDao evResDao = new EventResourceDao();

		evResDao = eventResB.getAllRolesForEvent(EJBUtil.stringToNum(eventId));


	   ArrayList evtResTrackIds = null;

	   evtResTrackIds = evResDao.getEvtResTrackIds();
	   evtStats = evResDao.getEvtStats();
	   cIds = evResDao.getEvtRoles();
	   cDuration = evResDao.getEvtTrackDurations();
	   cDescs = evResDao.getResourceNames();
	   int cRows = cIds.size();


		int fidx = 0;
		int midx = 0;
		int lidx = 0;




	for (int f=0; f<cRows; f++){ //how many roles associated-----loop2


		String ddval = "00";
		String hhval = "00";
   		String mmval = "00";
   		String ssval = "00";

		cDesc = (String) cDescs.get(f);
    	cId =  EJBUtil.stringToNum(((String) cIds.get(f)));
		cRoleDur = (String) cDuration.get(f);
		evtResTrackId = (String) evtResTrackIds.get(f);

		//avoid duplicate roles.
		
		//Modified for INF-18183 ::: Akshi
        eventResB.removeEventResources(EJBUtil.stringToNum(evtResTrackId),AuditUtils.createArgs(session,"",LC.L_Mng_Pats));

	   	if (!(cRoleDur == null) && ! (cRoleDur.equals("null")) && cRoleDur.trim().length() > 0){

			// get the dd, hh, mm, ss values

			fidx = 	cRoleDur.indexOf(":");
			midx = 	cRoleDur.indexOf(":",fidx+1);
			lidx = 	cRoleDur.lastIndexOf(":");

			ddval = cRoleDur.substring(0,fidx);
			hhval = cRoleDur.substring(fidx+1,midx);
			mmval = cRoleDur.substring(midx + 1,lidx);
			ssval = cRoleDur.substring(lidx+1);

			if ( ddval.trim().length() == 0 ) ddval = "00";
			if ( hhval.trim().length() == 0 ) hhval = "00";
			if ( mmval.trim().length() == 0 ) mmval = "00";
			if ( ssval.trim().length() == 0 ) ssval = "00";

		    durationStr = ((ddval.trim().length() == 1) ?  "0" + ddval : ddval) + ":" +((hhval.trim().length() == 1) ?  "0" + hhval : hhval) + ":" + ((mmval.trim().length() == 1) ?  "0" + mmval : mmval) + ":" + ((ssval.trim().length() == 1) ?  "0" + ssval : ssval);
			evDuration.add(durationStr);

		}

	}//end of for loop---2


	eventStatusId  = eventResB.getStatusIdOfTheEvent(EJBUtil.stringToNum(eventId));


	eventResB.setEvtStat("" + eventStatusId);
	eventResB.setEvtRoles(cIds);
	eventResB.setEvtTrackDurations(evDuration);
    eventResB.setCreator(userId);
    eventResB.setIpAdd(ipAdd);
	eventResB.setEventResourceDetails();


 } //end of for loop -----1




	if ( ret!= 0)  {

				%>
				<br><br><br><br><br>
					<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%>
					</p>
					<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
				<% return;
				}

			else {%>
				<br><br><br><br><br><p class = "successfulmsg" align = center>
				<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>
				<script>
			 		window.opener.location.reload();
					setTimeout("self.close()",1000);
				</script>

			<%}



	   }
		else {

	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
		}
	}

	else
	{
	%>
	  <jsp:include page="timeout.html" flush="true"/>
	  <%
	}
	%>

	</BODY>

	</HTML>

