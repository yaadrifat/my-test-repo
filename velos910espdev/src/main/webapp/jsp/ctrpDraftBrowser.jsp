<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>

<%@page import="com.velos.eres.business.common.*,java.util.*"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="ui-include.jsp" flush="true"/> 
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<%
int ctrpRights=0;
try {
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	ctrpRights=Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
	if (!isAccessibleFor(ctrpRights, 'V')){
		out.println("<div><p>&nbsp;</p><p class='sectionHeadings' align='center'>"+LC.L_Access_Forbidden+"</p></div>");
		return;
	}
	
	CodeDao cdDraftStatus = new CodeDao();
	String readyDraftStatus = cdDraftStatus.getCodeDescription(
			cdDraftStatus.getCodeId("ctrpDraftStatus","readySubmission"));
%>
<script >
var readyDraftStatus = "<%=readyDraftStatus%>";
var L_Access_Rights = "<%=LC.L_Access_Rights%>";
var L_Valid_Esign ="<%=LC.L_Valid_Esign%>";
var L_Invalid_Esign ="<%=LC.L_Invalid_Esign%>";
var CTRP_DraftTrialSubmCategory = "<%=LC.CTRP_DraftTrialSubmCategory%>";
var L_Create="<%=LC.L_Create%>";

function checkSearch()
{ 
	
	document.getElementById('studyIds').value="";
	document.getElementById('ctrpUnmark').value="";
	paginate_study.runFilter();
	$j("#messages").delay(800).fadeOut('slow');
	arrayDownload = [];
	checkedDraftIds="";
	
	
}
var studyIds="";

//Modified for S-22306 Enhancement : Raviesh
studypatLink = function(elCell, oRecord, oColumn, oData) {
	var data=oData;
		var record=oRecord;
 		var htmlStr="";
 		var studyNumber=record.getData("STUDY_NUMBER");
 		if(studyNumber.length>50){
 			studyNumber=studyNumber.substring(0,50);
 			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" style=\"border-style: none;\" onmouseover=\"return overlib(\'"+record.getData("STUDY_NUMBER")+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"/>"; 			
		}else{
			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber;
			}
 		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
 		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
 		var seq=parseInt($('rightSeq').value);
		if (!seq) seq=0;
		var mPatRight=0;
		if (sTeamRight.length>seq)
		{
 		mPatRight=parseInt(sTeamRight.substring(seq-1,seq));
 		mPatRight=((mPatRight==null) || (!mPatRight) )?"":mPatRight;
		}
		elCell.innerHTML =htmlStr;
};

draftStatusLink = function(elCell, oRecord, oColumn, oData) {
	    var data=oData;
	    var record=oRecord;
	    var status=record.getData("FK_CODELST_STAT")==null?"":record.getData("FK_CODELST_STAT");
	   if(status=="")
	   {
		   $j('#ctrpStudyWithOutDraft').show();
		   $j('#ctrpStudyWithDraft').hide();
	   }else{
		   $j('#ctrpStudyWithOutDraft').hide();
		   $j('#ctrpStudyWithDraft').show();
	   }
	    var statusSubType=record.getData("FK_CODELST_STAT_SUBTYPE")==null?"":record.getData("FK_CODELST_STAT_SUBTYPE");
	    var draftType=record.getData("CTRP_DRAFT_TYPE")==null?"":record.getData("CTRP_DRAFT_TYPE");
	    var researchType=record.getData("CTRP_DRAFT_TYPE_STAT")==null?"":record.getData("CTRP_DRAFT_TYPE_STAT");
	    var draftPK=record.getData("PK_CTRP_DRAFT")==null?"":record.getData("PK_CTRP_DRAFT");
	    var studyPK=record.getData("PK_STUDY")==null?"":record.getData("PK_STUDY");

	    var htmlStr="";
	    var imagStr="";

	    if(<%=isAccessibleFor(ctrpRights, 'V')%>) 
	    {
			switch(draftType){
			case "1" ://Industrial
				if (researchType != "indus"){
					imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
				   	htmlStr="<a href=\"javascript:void(0);\" onClick=\"return alert('Page under construction'); return false;\" style=\"text-decoration:none;\" onmouseover=\"return overlib('Conflict found between Study Summary &gt;&gt;Research Type and Trial Submission Category',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a>";
				}else{
					if(statusSubType=="readySubmission"){
				    	imagStr="<img src=\"./images/ctrp_images/success.png\" style=\"border-style: none;\"/>&nbsp;"+status;
				    	htmlStr="<a href=\"ctrpDraftIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }else {
			    		imagStr="<img src=\"./images/ctrp_images/exclaimation.png\" style=\"border-style: none;\"/>&nbsp;"+status;
						htmlStr="<a href=\"ctrpDraftIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }
				}
				break;
			case "0" ://Non-Industrial
				if (researchType != "other" && researchType != "insti" && researchType != "national"){
					imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
				   	htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\"onmouseover=\"return overlib('Conflict found between Study Summary &gt;&gt; Research Type and Trial Submission Category',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a>";
				}else{
					if(statusSubType=="readySubmission"){
				    	imagStr="<img src=\"./images/ctrp_images/success.png\" style=\"border-style: none;\"/>&nbsp;"+status;
				    	htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }else {
			    		imagStr="<img src=\"./images/ctrp_images/exclaimation.png\" style=\"border-style: none;\"/>&nbsp;"+status;
						htmlStr="<a href=\"ctrpDraftNonIndustrial?draftId="+draftPK+"\"  return false;\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a>";
				    }
				}
				break;
			case "-1"://New Draft
				imagStr="<img src=\"./images/ctrp_images/newDraft.png\" title='"+L_Create+"' style=\"border-style: none;\"/>&nbsp;"+status;
				<%if (!isAccessibleFor(ctrpRights, 'N')){%>
					htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ L_Access_Rights +"');\" onmouseout=\"return nd();\">"+imagStr+"</a></div>";
				<%} else {%>
				if (researchType == "other" || researchType == "insti" || researchType == "national"){
					htmlStr="<div align=\"center\" ><a href=\"ctrpDraftNonIndustrial?studyId="+studyPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a></div>";
			    }else{
				    if (researchType == "indus"){
				    	htmlStr="<div align=\"center\" ><a href=\"ctrpDraftIndustrial?studyId="+studyPK+"\" style=\"text-decoration:none;\">&nbsp;"+imagStr+"</a></div>";
				    }else{
				    	imagStr ="&nbsp;<img src=\"./images/ctrp_images/redExlamation.png\" style=\"border-style: none;\" />&nbsp;"+status;
						htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_TrialCatNat_ExtrIndlStdPg+"',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">"+imagStr+"</a></div>";
				    }
			    }
			    <%}%>
				break;
			}
	    }else{
	    	imagStr="<img src=\"./images/ctrp_images/Frozen.png\" style=\"border-style: none;\" />&nbsp;"+status;
		    htmlStr="<div align=\"center\" ><a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ L_Access_Rights +"');\" onmouseout=\"return nd();\">"+imagStr+"</a></div>";
        }
	   
	    elCell.innerHTML =htmlStr;
};

var arrayDownload = [];
function setDownloadArray(pkDraft, fkStudy, draftTypeSubType, draftStatSubType){
	var arraySize = arrayDownload.length;
	arrayDownload[arraySize] = 'pkDraft:'+pkDraft+'fkStudy:'+fkStudy;

	arrayDownload[arraySize] += 'draftTypeSubType:';
	if (draftTypeSubType != undefined && draftTypeSubType.length > 0){
		arrayDownload[arraySize] += draftTypeSubType;
	}
	arrayDownload[arraySize] += 'draftStatSubType:';
	if (draftStatSubType != undefined && draftStatSubType.length > 0){
		arrayDownload[arraySize] += draftStatSubType;
	}
}
//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
var checkedDraftIds="";
function selectedDraftIds(status,checkdraftId,draftTypeSubType,draftStatSubType){
		if(status.checked){
			if(checkedDraftIds==""){
				checkedDraftIds=$j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			}else{
				checkedDraftIds=$j.trim(checkedDraftIds)+$j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			}
		}
		else{
			var replaceStr = $j.trim(checkdraftId)+"~"+$j.trim(draftTypeSubType)+"~"+$j.trim(draftStatSubType)+",";
			checkedDraftIds = replaceCheckedDraftIds($j.trim(checkedDraftIds), replaceStr, '');
			$j.trim(checkedDraftIds);
		}
	}
//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
function replaceCheckedDraftIds(allSelectedDraftIds, replace, replaceWithStr) {
	  return allSelectedDraftIds.replace(new RegExp(replace, 'g'),replaceWithStr);
}

studyCheckBox = function(elCell, oRecord, oColumn, oData) {
		var data=oData;
    		var record=oRecord;
    		var htmlStr="";

    		var pkDraft=oRecord.getData("PK_CTRP_DRAFT");
    		var draftTypeSubType=oRecord.getData("CTRP_DRAFT_TYPE_STAT");
    		var draftStatSubType=oRecord.getData("FK_CODELST_STAT_SUBTYPE");
    		var fkStudy=oRecord.getData("PK_STUDY");
    		setDownloadArray(pkDraft, fkStudy, draftTypeSubType, draftStatSubType);
    		
    		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
     		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
    		var seq=parseInt($('rightSeq').value);
    		if (!seq) seq=0;
    		var mPatRight=0;
    		if (sTeamRight.length>seq)
    		{
     		mProtRight=parseInt(sTeamRight.substring(seq-1,seq));
     		mProtRight=((mProtRight==null) || (!mProtRight) )?"":mProtRight;
    		}
    		if (f_check_perm_noAlert(mProtRight,'E'))
    		{
        		 htmlStr="<input type='checkbox' name='studycheckboxes' id='studycheckboxes' onclick=\"selectedDraftIds(this,'"+pkDraft+"','"+draftTypeSubType+"','"+draftStatSubType+"')\" value='"+record.getData("PK_STUDY")+"'>";
    		}else
    		{
    		 htmlStr="<a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">&nbsp;<img src=\"./images/ctrp_images/Frozen.png\" style=\"border-style: none;\" /></a>";
    		 htmlStr +="<input type='checkbox' disabled name='studycheckboxes' id='studycheckboxes' value='"+record.getData("PK_STUDY")+"'>";
        		}

    		
    		elCell.innerHTML =htmlStr;
};

statusLink  =
	 function(elCell, oRecord, oColumn, oData)
	  {
		var record=oRecord;
      		var htmlStr="";
      		var cl="";
    		var status=record.getData("STATUS"); 
    		var sub=record.getData("STATUS_SUBTYPE"); 
    		  sub=((sub==null) || (!sub) )?"":sub;
      		if (sub=="temp_cls") cl="Red"; 
      		if (sub=="active") cl="Green";
      		var nt=record.getData("STUDYSTAT_NOTE");
      		nt=((nt==null) || (!nt) )?"":nt;
      		nt=htmlEncode(nt);
      		nt = escapeSingleQuoteEtc(nt);
      		var tt="<tr><td><font size=2><%=LC.L_Note%>:</font><font size=1>"+nt+"</font></td></tr><tr><td><font size=2><%=LC.L_Organization%>:</font><font size=1> " +record.getData("SITE_NAME")    <%--Fixed bug#7913:Akshi--%>
      		+"</font></td></tr>";
      		if(status!="...")
      		{
          		htmlStr="<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")   
      		+ "\" onmouseover=\"return overlib('"+tt+"',CAPTION,'"+record.getData("STUDYSTAT_DATE_DATESORT")+"');\" onmouseout=\"return nd();\"><FONT class=\""+cl+"\">"+record.getData("STATUS")+"</FONT> </A>";
      		}else{
      			htmlStr=status;
          	}
      		elCell.innerHTML=htmlStr;
		
    }
//Modified for S-22306 Enhancement : Raviesh    
titleLink = 
	function(elCell, oRecord, oColumn, oData)
	  {
		var record=oRecord;
        		var htmlStr="";
		var title = record.getData("STUDY_TITLE"); 
		title = ((title==null) || (!title) )? "-":title;
		title = htmlEncode(title);
		title = escapeSingleQuoteEtc(title);
		if(title.length>50)
		{
		var studyTitle=title.substring(0,50);
		htmlStr = studyTitle+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" border=\"0\" onmouseover=\"return overlib(\'"+title+"\',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\">";
		}else{
			htmlStr=title;
		}
		elCell.innerHTML = htmlStr;
     }
resType=	function(elCell, oRecord, oColumn, oData)
{
	var record=oRecord;
		var htmlStr="";
	var title = record.getData("RESEARCH_TYPE"); 
	var researchType=record.getData("CTRP_DRAFT_TYPE_STAT")==null?"":record.getData("CTRP_DRAFT_TYPE_STAT");
	title = ((title==null) || (!title) )? "-":title;
	title = htmlEncode(title);
	title = escapeSingleQuoteEtc(title);
	var color;
	//Bug#7936
	if (researchType == "other" || researchType == "insti" || researchType == "national"){
	color="green";			
	}else if(researchType == "indus"){
		color="blue";
	}else{
	 color="red";
	 }
	htmlStr = "<font color="+color+">"+title+"</font>"; 
	elCell.innerHTML = htmlStr;
}
$E.addListener(window, "load", function() {  
	clearValues();
	paginate_study=new VELOS.Paginator('ctrpDraftBrowser',{
					rowsPerPage  : 10,
	 				sortDir:"asc", 
					sortKey:"STUDY_NUMBER",
					defaultParam:"userId,accountId,grpId,superuserRights,pmtclsId",
					filterParam:"searchCriteria,ctrpDraftSearch",				
					dataTable:'serverpagination',
					rowSelection:[10,15,20,50],
					navigation: true,
					rowsPerPage:10
					});
	checkSearch();
	checkedDraftIds=""; 
	}

)  

function postQuery(type)
{	
	var checkBoxes =document.getElementsByName("studycheckboxes").length;
	var checkedStudy = new Array();
	var checkedDraft = new Array();

	var indusFlag = false;
	var nonIndusFlag = false;

	var strDownload = '';
	var statDelimiter = 'draftStatSubType:';
	var typeDelimiter = 'draftTypeSubType:';
	var pkDraftDelimiter='pkDraft:';
	var fkStudyDelimiter ='fkStudy:';
	
	var startPt = 0;
	var endPt = 0;
	
	var cS=0;

	var confirmFlag = false;
	//Modified on 15/Feb/2012 For Bug# 8464 :Yogendra
	var finalCheckedDraftIds="";
	var checkedDraftIdsArr = new Array();
	if(type=="download"){
		if(checkedDraftIds.length>0){
			checkedDraftIdsArr = checkedDraftIds.split(",");
			for(var i=0; i<checkedDraftIdsArr.length-1;i++ ){
				var checkIdData  = checkedDraftIdsArr[i];
				var checkIdDataArray = new Array();
				checkIdDataArray = checkIdData.split("~")
					if(finalCheckedDraftIds=="")	 
						finalCheckedDraftIds = checkIdDataArray[0];
					else
						finalCheckedDraftIds = finalCheckedDraftIds+","+checkIdDataArray[0];
		
					if(checkIdDataArray[2] != 'readySubmission'){
						if(confirmFlag==false)
							confirmFlag =true;
					}
					if(checkIdDataArray[1] == "indus"){
						indusFlag = true;
					}
					if(checkIdDataArray[1] == "other" || checkIdDataArray[1] == "insti" || checkIdDataArray[1] == "national"){
						nonIndusFlag = true;
					}
					if (indusFlag && nonIndusFlag){
						alert(CTRP_IndNonIndDraftsDownload);
						return;
					}
			} 
		}else{
			alert(M_PlsSel_OneStd);
			return false;
		}
		if(confirmFlag){
			if(!confirm(CTRP_ConfirmDraftDownload))
				return;
		}
	}else{
		for (var i=0;i<checkBoxes;i++)
		{   
			if(document.getElementsByName("studycheckboxes")[i].checked)
			{
				//Modified for Bug#8464 | Date: 20 FEB 2012 |By :YPS
				checkedStudy[cS]=document.getElementsByName("studycheckboxes")[i].value;
				cS++;
			}
		}
		
		if(checkedStudy.length<=0)
		{
			alert(M_PlsSel_OneStd);
			return false;
		}
	}
	if(type=="download"){
		var flagValue =0;
		if(nonIndusFlag){
			flagValue=1;
		}
		document.getElementById('studyIds').value=checkedStudy;
		document.getElementById('draftIds').value=finalCheckedDraftIds;
		//Modified For Bug#8464 | Date:28 Feb 2012 | By:Yogendra Pratap Singh
		var width  = 400;
 		var height = 200;
 		var left   = (screen.width  - width)/2;
 		var top    = (screen.height - height)/2;
 		var params = 'width='+width+', height='+height;
 		 params += ', top='+top+', left='+left;
 		 params += ', directories=no';
 		 params += ', location=no';
 		 params += ', menubar=no';
 		 params += ', resizable=no';
 		 params += ', scrollbars=no';
 		 params += ', status=yes';
 		 params += ', toolbar=no';
		var win = window.open("ctrpDraftExport?studyIds="+checkedStudy+"&industryFlag="+indusFlag+"&nonIndustryFlag="+flagValue+"&selectedDraftIds="+finalCheckedDraftIds,'',params);
		clearValues();
		document.getElementById('searchCriteria').value="";
		paginate_study.runFilter();
		win.focus();
	}else{
		if(type=="unmark"){
			$j("#confirmMessage").html("<%=MC.CTRP_DraftConfmReportUnmark%>");
			document.getElementById('ctrpUnmark').value='1';
		}else if(type=="del"){
			 var proceed = confirm(M_SureWantDel_SelDrfts);	
			 if (!proceed){
				return false;
			 }
			 $j("#confirmMessage").html("<%=MC.CTRP_DraftConfmDraftDelete%>");
			 document.getElementById('draftDelete').value='1';
		}
		document.getElementById('studyIds').value=checkedStudy;
		openMessageDialog();
	}
} 

function submitData()
{
	var eSign=$j("#eSign").val();
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSign").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessage").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSign").focus();
		return false;
	}

document.getDraftBrowser.submit();
}
function openMessageDialog()
{
/*YK: Added for bug #7932*/
	var myDialog = new YAHOO.widget.Dialog('enterEsign', 
		{
			visible:false, fixedcenter:true, modal:true, resizeable:true,
			draggable:"true", autofillheight:"body", constraintoviewport:false
			
		});
	var handleCancel = function() {
	     clearValues();
	     myDialog.cancel();
	};
	var handleSubmit = function() {
		//Bug #8253- check for valid e-sign
		var eSignMessage=$j("#eSignMessage").html();
		if(eSignMessage!=L_Valid_Esign)
		{
			alert(M_IncorrEsign_EtrAgain);
			$j("#eSign").focus();
			return false;
		}
	     showTransitPanel(PleaseWait_Dots);
	     submitData();
	};
	var myButtons =[
	    		{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
	    		{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
	    	     ];
		myDialog.cfg.queueProperty("buttons", myButtons);
		myDialog.render(document.body);
		myDialog.show();
		$j("#enterEsign").show();
		$j(".container-close").click(function() {
			clearValues();
		});
var showTransitPanel = function(msg) {
if (!VELOS.transitPanel) {
	VELOS.transitPanel = 
		new YAHOO.widget.Panel("transitPanel",  
			{ width:"240px", 
			  fixedcenter:true, 
			  close:false, 
			  draggable:false, 
			  zindex:4,
			  modal:true,
			  visible:false
			} 
		);
}
VELOS.transitPanel.setHeader("");
VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;">'+msg+'</td></tr><table>');
VELOS.transitPanel.render(document.body);
VELOS.transitPanel.show();
}
$j(function() {
	if ($j("input:submit")){
		$j( "input:submit").button();
	}
	if ($j("a:submit")){
		$j( "a:submit").button();
	}
	if ($j("button")){
		$j("button").button();
	}
});	
}
function clearValues()
{
	$j("#eSign").val("");
	ajaxvalidate('misc:eSign',4,'eSignMessage',L_Valid_Esign,L_Invalid_Esign,'sessUserId');
	document.getElementById('studyIds').value="";
	document.getElementById('ctrpUnmark').value="";
	document.getElementById('draftDelete').value="";
	checkedDraftIds="";
	finalCheckedDraftIds="";
}

<!-- Modified By Parminder Singh Bug#10391 -->
var fnOnceEnterKeyPress = function(e) {
	var eSign=$j("#eSign").val();
	 if (e.keyCode == 13 || e.keyCode == 10) {
	if (eSign.length<=0)
	{
		alert(M_PlsEnterEsign);
		$j("#eSign").focus();
		return false;
	}
	var eSignMessage=$j("#eSignMessage").html();
	if(eSignMessage!=L_Valid_Esign)
	{
		alert(M_IncorrEsign_EtrAgain);
		$j("#eSign").focus();
		return false;
	}
document.getDraftBrowser.submit();
}
}

</script>
<body  class="yui-skin-sam" >

<div style="margin-top: 10px;">
<s:form method="post" action="ctrpDraftBrowser" name="getDraftBrowser" id="getDraftBrowser" onsubmit="return false;">

<table width="98%" cellspacing="0"  cellpadding="0" border="0" align="center">
<tr class="searchBg" height="55" >
<td class="labelFont" width="15%" valign="top">
<%=LC.L_Search_By%><br><br>
<%=MC.M_StdTitle_Kword%>:
</td>
<td width="25%" id="lookupid" valign="middle">
<br><s:textfield id="searchCriteria" name="searchCriteria" ></s:textfield>
</td>
<td width="5%" valign="middle" align="center"><br><%=LC.L_In_Lower %></td>
<td width="30%" valign="middle">
<br><s:property escape="false" value="ctrpDraftJB.studyDraftsMenu" /></td>
<td valign="middle">
<br>&nbsp;&nbsp;&nbsp;&nbsp; 
<button type="submit" onclick="checkSearch();"><%=LC.L_Search %></button>
</td>
</tr> 
</table> 
<div style="margin-top: 10px;" id="messages" >
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
<tr align="center"><td align="center">
<s:label cssClass="defComments"><s:property value="%{#request.message}"/></s:label>
</td></tr> </table>
</div>
<div id="ctrpStudyWithDraft" style="display: none; margin-left: 10px;">
<%if (isAccessibleFor(ctrpRights, 'N') || isAccessibleFor(ctrpRights, 'E')){ %>
	<%=LC.L_To_Selected %>
	<%if (isAccessibleFor(ctrpRights, 'E')){ %> 
	<a href="javascript:void(0)" onclick="postQuery('del');"><%=LC.L_Delete_Draft %></a>&nbsp;&nbsp;&nbsp;
	<%} %>
	<%if (isAccessibleFor(ctrpRights, 'N')){ %>
	<a href="javascript:void(0)" onclick="postQuery('download');"><%=LC.L_Download%></a>
	<%} %>
<%} %>
</div>
<div id="ctrpStudyWithOutDraft" style="display: none; margin-left: 10px;">
<%=LC.L_To_Selected %> <a href="javascript:void(0)" onclick="postQuery('unmark');"><%=LC.L_UnmarkCtrp_Reoprtable %></a>
</div>

<br>
<s:hidden id="accountId" name="accountId" value="%{ctrpDraftJB.accountId}"></s:hidden>
<s:hidden id="studyIds" name="studyIds" ></s:hidden>
<s:hidden id="draftIds" name="draftIds" ></s:hidden>
<s:hidden id="ctrpUnmark"  name="ctrpUnmark" ></s:hidden>
<s:hidden id="draftDelete"  name="draftDelete" ></s:hidden>
<s:hidden id="userId" name="userId" value="%{ctrpDraftJB.userIdS}"></s:hidden>
<s:hidden id="grpId" name="grpId" value="%{ctrpDraftJB.grpId}"></s:hidden>
<s:hidden id="groupName" name="groupName" value="%{ctrpDraftJB.groupName}"></s:hidden>
<s:hidden id="rightSeq" name="rightSeq" value="%{ctrpDraftJB.rightSeq}"></s:hidden>
<s:hidden id="superuserRights" name="superuserRights" value="%{ctrpDraftJB.superuserRights}"></s:hidden>
<s:hidden id="pmtclsId" name="pmtclsId" value="%{ctrpDraftJB.pmtclsId}"></s:hidden>

<div id="serverpagination" style="margin-top: 10px; margin-left: 5px; " >
</div>
<br/>
<div id="enterEsign" style="display: none; background-color:#f2f2f2; width:400px;">
<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width:400px;">
<span id="confirmMessage"></span>
&nbsp;
</div>
<div class="bd" id="insertEsign" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; overflow: visible; width:400px;">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td colspan="4" align="left" class="black"><%=MC.M_Etr_Esign_ToProc %></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
	<td width="25%" id="eSignLabel" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;" ><%=LC.L_Esignature%><FONT class="Mandatory">*</FONT>&nbsp;</td>
	<td width="20%"  >
	<!-- Modified By Parminder Singh Bug#10391 -->
	<input type="password" name="eSign" id="eSign" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt; vertical-align: bottom;" maxlength="8" 
	 onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign%>','<%=LC.L_Invalid_Esign%>','sessUserId')"  onkeypress="return fnOnceEnterKeyPress(event);"></input></td>
	 <td width="30%" align="center" ><%--<button name="submit" onclick="return submitData();" ><%=LC.L_Submit %></button>--%></td>
	</tr>
	<tr><td colspan="4" style="margin-top: 10px; height: 40px;" align="center">&nbsp;<span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td></tr>
	</table>
   </div>
	<div class="ft" style="width:400px;"></div>
</div>
</s:form>
</div>
</body>
<%} catch(Exception e) { return; }
%> 