<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_FldEdt_BoxSubmit%><%--Field -Edit Box Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="fldValidateJB" scope="request"  class="com.velos.eres.web.fldValidate.FldValidateJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.fldValidate.impl.FldValidateBean"%>

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
	
		
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			String fldCategory="";
			String fldName="";
			String fldNameFormat="";
			String fldExpLabel = "";
			String fldUniqId="";
			String fldKeyword="";
			String fldInstructions="";
			String fldDesc="";
			String fldType="";
			String fldDataType="";
			String fldIsRO="";
		
		
			String fldLength="";
			String fldLinesNo="";
			String fldCharsNo="";
			String fldDefResp="";
			String accountId="";
			String fldLibFlag="L"; // received from session in future 
			Style aStyle = new Style();
		
		
			String creator="";
			String ipAdd="";
			String numformat ="";

			String fldValId="";
			String Glt1 = "";
			String Glt2 = "";
			String val1 = "";
			String val2 = "";
			String andOr = "";

			String dateCheck="",defaultDate="";
		
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			mode=request.getParameter("mode");
			String recordType=mode;  
			
	
	
			fldCategory=request.getParameter("category");
			fldName=request.getParameter("name");
			
			fldNameFormat=request.getParameter("nameta");
			fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
			
			fldUniqId=request.getParameter("uniqueId");
			fldKeyword=request.getParameter("keyword");
			fldInstructions=request.getParameter("instructions");
			fldDesc=request.getParameter("description");
			fldType="E"; //edit box type to be entered by us 
			fldDataType=request.getParameter("editBoxType");
			fldLength=request.getParameter("lengthOf");
			fldCharsNo=request.getParameter("characters");
			fldLinesNo=request.getParameter("lines");
			dateCheck = request.getParameter("hdndatefld");
			
			defaultDate=request.getParameter("hdndefaultdate");
			defaultDate=(defaultDate==null)?"0":defaultDate;
			
			fldValId=request.getParameter("fldValId");
			Glt1 = request.getParameter("greaterless1");
			Glt2 = request.getParameter("greaterless2");
			val1 = request.getParameter("val1");
			val2 = request.getParameter("val2");
			andOr = request.getParameter("andor");
			numformat = request.getParameter("numformat");
			fldExpLabel = request.getParameter("expLabel");
			
			
			
			String dateChk = request.getParameter("hdnOverRideDate");			
				if(dateChk == null || dateChk.equals(""))
				dateChk = "0";
			String formatChk = request.getParameter("hdnOverRideFormat");
				if(formatChk == null || formatChk.equals(""))
				formatChk = "0";
			String rangeChk = request.getParameter("hdnOverRideRange");
				if(rangeChk == null || rangeChk.equals(""))
				rangeChk = "0";

			if(numformat!=null)
			{
				numformat = numformat.trim();
			}
			else
		    {
				numformat="";
		    }
		   if(Glt1==null)
				Glt1="";
			if(Glt2==null)
				Glt2="";
			if(val1==null)
				val1="";
			if(val2==null)
				val2="";
			if(andOr==null)
				andOr="";
			if(numformat==null)
				numformat="";
			if(dateCheck==null)
				dateCheck="";

			
			if (fldDataType.equals("ED"))
			{
				//fldDefResp=request.getParameter("defResponse2");
			//	fldDefResp = "";
			if (defaultDate.equals("1"))
			   fldDefResp="[VELSYSTEMDATE]";
				fieldLibJB.setFldIsRO("0") ;
			}
			else
			{
				fldDefResp=request.getParameter("defResponse1");
				fldDefResp=(fldDefResp==null)?"":fldDefResp;
			}
			
			
			
			if (!(fldDataType.equals("ED")) && (fldDefResp.equals("[VELSYSTEMDATE]")))
			{
				fldDefResp="";
			}
	
	
			if (mode.equals("M"))
			{
				fieldLibId=request.getParameter("fieldLibId");
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId));
				fieldLibJB.getFieldLibDetails();
			}
			
			if(fldExpLabel==null){
				
				fldExpLabel="0";
			}
			else
		   {
				fldExpLabel="1";
		   }
			
			fieldLibJB.setCatLibId(fldCategory);
			fieldLibJB.setFldName(fldName);
			fieldLibJB.setFldNameFormat(fldNameFormat);
			fieldLibJB.setFldUniqId(fldUniqId);
			fieldLibJB.setFldKeyword(fldKeyword);
			fieldLibJB.setFldInstructions(fldInstructions);
			fieldLibJB.setFldDesc(fldDesc);
			fieldLibJB.setFldType(fldType);
			fieldLibJB.setFldDataType(fldDataType);
			fieldLibJB.setFldLength(fldLength);
			fieldLibJB.setFldLinesNo(fldLinesNo);
			fieldLibJB.setFldCharsNo(fldCharsNo);
			fieldLibJB.setFldDefResp(fldDefResp);
			fieldLibJB.setIpAdd(ipAdd);
			fieldLibJB.setRecordType(recordType);
			fieldLibJB.setAccountId(accountId);
			fieldLibJB.setLibFlag(fldLibFlag);
			fieldLibJB.setTodayCheck(dateCheck);
			fieldLibJB.setFldFormat(numformat);
			fieldLibJB.setExpLabel(fldExpLabel);
			fieldLibJB.setFldIsVisible("0");
			fieldLibJB.setOverRideFormat(formatChk);
			fieldLibJB.setOverRideRange(rangeChk);
			fieldLibJB.setOverRideDate(dateChk);
			fieldLibJB.setAStyle(aStyle);
			
			FldValidateBean fldValsk = new FldValidateBean();

				fldValsk.setFldLibId(fieldLibId);
				fldValsk.setFldValidateOp1(Glt1);
				fldValsk.setFldValidateOp1(Glt1);
				fldValsk.setFldValidateVal1(val1);
				fldValsk.setFldValidateLogOp1(andOr);
				fldValsk.setFldValidateOp2(Glt2);
				fldValsk.setFldValidateVal2(val2);
				fldValsk.setRecordType(recordType); 
				fldValsk.setIpAdd(ipAdd);
				fieldLibJB.setFldValidateSk(fldValsk);
			
			
			if (mode.equals("N"))
			{
			fldValsk.setCreator(creator);
			}
			

			// Set the form Notify State Keeper
			if (mode.equals("N"))
			{
				
				fieldLibJB.setCreator(creator);
				errorCode = fieldLibJB.setFieldLibDetails();
			}

			else if (mode.equals("M"))
			{
				
				
				aStyle= new Style();	
				fieldLibJB.setCatLibId(fldCategory);
				fieldLibJB.setFldUniqId(fldUniqId);
				fieldLibJB.setFldKeyword(fldKeyword);
				fieldLibJB.setFldInstructions(fldInstructions);
				fieldLibJB.setFldType(fldType);
				fieldLibJB.setFldDataType(fldDataType);
				fieldLibJB.setFldLength(fldLength);
				fieldLibJB.setFldLinesNo(fldLinesNo);
				fieldLibJB.setFldCharsNo(fldCharsNo);
				fieldLibJB.setFldDefResp(fldDefResp);
				fieldLibJB.setModifiedBy(creator);
				fieldLibJB.setIpAdd(ipAdd);
				fieldLibJB.setRecordType(recordType);
				fieldLibJB.setLibFlag(fldLibFlag);
				fieldLibJB.setTodayCheck(dateCheck);
				fieldLibJB.setAccountId(accountId);
				fieldLibJB.setExpLabel(fldExpLabel);
				fieldLibJB.setFldIsVisible("0");
				fieldLibJB.setOverRideFormat(formatChk);
				fieldLibJB.setOverRideRange(rangeChk);
				fieldLibJB.setOverRideDate(dateChk);
				fieldLibJB.setAStyle(aStyle);
				fieldLibJB.setFldValidateSk(fldValsk);
				fieldLibJB.setFldFormat(numformat);

				fldValsk.setFldValidateId(EJBUtil.stringToNum(fldValId));
				//KM-#3634
				fldValsk.setModifiedBy(creator);

				errorCode=fieldLibJB.updateFieldLib();
							
		}
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>
  	
<%             if (errorCode == -3 )
				{
%>
				<br><br><br><br><br>
				<p class = "successfulmsg" > <%=MC.M_FldIdExst_EtrDiff%><%--The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button type="button" tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfullyl*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		
		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
 	 <script>
 		    //Added to resolve Bug#7595 : Raviesh					
		    window.opener.location.href = 'fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4';	
		    setTimeout("self.close()",1000);
		</script>	

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
