<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head>

<title><%=MC.M_Ajax_StdData%><%-- Ajax <%=LC.Std_Study%> Data*****--%></title>



<%@ page import="com.velos.eres.business.common.*,java.sql.*,com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<head>

<SCRIPT LANGUAGE="JavaScript">
	function viewInvoice(inv)
	{
      windowName= window.open("viewInvoice.jsp?invPk=" + inv,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();

	}
	function openFilledFormWin(perId, personCode, patprotId, studyId) {
	   	windowName=window.open("formfilledstdpatbrowser.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=8&pkey="+perId+"&patProtId="+patprotId+"&studyId="+studyId+"&patientCode="+personCode+"","Form","scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	   	windowName.focus();
	}
</SCRIPT>
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil"%>
<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<%
	//Declarations
	String src="tdMenuBarItem3";
	String studySql ="";
	String countSql;

	long rowsPerPage=0;
	long totalPages=0;
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;
	long firstRec = 0;
	long lastRec = 0;
	int openCount = 0;
	int resolvedCount = 0;
	int reopenCount = 0;
	int closedCount = 0;
	int enrolled = 0;
	String stdTeamRight = "";

	String studyNumber = null;
	String studyTitle = null;
	String studyStatus = null;
	String studyActualDt = null;
	String studyIRB = null;
	String studyIRB2 = null;
	String studySite = null;
	String studyPI = null;

	int studyId=0;
	boolean hasMore = false;
	boolean hasPrevious = false;
	CodeDao  cd = new CodeDao();
	String codeLst= cd.getValForSpecificSubType();
	HttpSession tSession = request.getSession(true);
	String userId= (String) tSession.getValue("userId");

	String pagenum = "";
	int curPage = 0;
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");

	BrowserRows pr;
	BrowserRows br;

	long startPage = 1;
	long cntr = 0;
	String pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	int curPageView = EJBUtil.stringToNum(pagenumView);

	long startPageView = 1;
	long cntrView = 0;
	String defGroup;
	String groupName;
	String rightSeq;

	String orderByView = "";
	orderByView = request.getParameter("orderByView");

	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");

	int usrId;
	String dProtocolStatus = "" ;
	CodeDao cd1 = new CodeDao();
	
	GrpRightsJB grpRights;
	ArrayList aRightSeq ;
	int iRightSeq ;

	int nodeNo = EJBUtil.stringToNum(request.getParameter("nodeNo"));
	int snodeNo = EJBUtil.stringToNum(request.getParameter("snodeNo"));
	String accountId= (String) tSession.getValue("accountId");
%>
<%
	cd1.getCodeValues("studystat");
	dProtocolStatus = cd1.toPullDown("protocolStatus");
	startPage = 1;
	cntr = 0;
	pagenumView = "";
	curPageView = 0;
	startPageView = 1;
	cntrView = 0;

	pagenum = request.getParameter("page");
	if (pagenum == null) {pagenum = "1";}
	curPage = EJBUtil.stringToNum(pagenum);

	orderBy = "";
	orderBy = request.getParameter("orderBy");

	if (EJBUtil.isEmpty(orderBy)) {orderBy = "4";}
	orderType = "";
	orderType = request.getParameter("orderType");

	if (orderType == null)	{orderType = "asc";}
	pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	curPageView = EJBUtil.stringToNum(pagenumView);

	orderByView = "";
	orderByView = request.getParameter("orderByView");
	if (EJBUtil.isEmpty(orderByView)) { orderByView = "4";}

	orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	if (orderTypeView == null) {orderTypeView = "asc";}

	if (sessionmaint.isValidSession(tSession)) {
		userId = (String) tSession.getValue("userId");

		//Added by Manimaran to give access right for default admin group to the delete link
		usrId = EJBUtil.stringToNum(userId);
		userB.setUserId(usrId);
		userB.getUserDetails();
		defGroup = userB.getUserGrpDefault();
		groupB.setGroupId(EJBUtil.stringToNum(defGroup));
		groupB.getGroupDetails();
		groupName = groupB.getGroupName();

		int pageRight =0;
		grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

		acmod.getControlValues("study_rights","STUDYMPAT");
		aRightSeq = acmod.getCSeq();
		rightSeq = aRightSeq.get(0).toString();
		iRightSeq = EJBUtil.stringToNum(rightSeq);

		if (pageRight > 0 )	{
			cd = new CodeDao();
			codeLst = cd.getValForSpecificSubType();

			/*nodeNo = (nodeNo==null)?"":nodeNo;
			snodeNo = (snodeNo==null)?"":snodeNo;*/

			//System.out.println("nodeNo " + nodeNo);
			String superuserRights = "";
			superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);	
			switch (nodeNo){
			case 0:
				//Query Summary -- KM--studyteam rights
				studySql = "select pk_study,STUDY_ACTUALDT, study_number,case when length(study_title) > 80 then substr(study_title,1,40) || '...'  else study_title  end as study_title, (nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights,"
				+ "(select usr_firstname || ' ' || usr_lastname from er_user where pk_user = study_prinv) as pi,"
				+ "(select (select codelst_desc from er_codelst where pk_codelst = aa.fk_codelst_studystat) from er_studystat aa where aa.fk_study = pk_study and aa.pk_studystat =  ( "
				+ "CASE "
				+ "WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study "
				+ "AND a.fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 "
				+ "THEN (SELECT MAX(b.pk_studystat) FROM ER_STUDYSTAT b WHERE b.fk_study = pk_study "
				+ "AND b.fk_site=(select fk_siteid from er_user where pk_user="+userId+") AND b.studystat_date = (SELECT MAX(c.studystat_date) "
				+ "FROM ER_STUDYSTAT c WHERE c.fk_study = pk_study "
				+ "AND c.fk_site=(select fk_siteid from er_user where pk_user="+userId+"))) "
				+ "WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study "
				+ "AND a.fk_site=(select fk_siteid from er_user where pk_user="+userId+") )>0 "
				+ "THEN(SELECT MAX(b.pk_studystat) FROM ER_STUDYSTAT b WHERE b.fk_study = pk_study "
				+ "AND b.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and b.studystat_date = (SELECT MAX(c.studystat_date) "
				+ "FROM ER_STUDYSTAT c WHERE c.fk_study = pk_study and c.fk_site=(select fk_siteid from er_user where pk_user="+userId+"))) "
				+ "ELSE (SELECT MAX(a.pk_studystat) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study) END) "
				+ ") as status, "
				+ "(select to_char(bb.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat bb where bb.fk_study = pk_study and bb.pk_studystat =  ( "
				+ "CASE "
				+ "WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study "
				+ "AND a.fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 "
				+ "THEN (SELECT MAX(b.pk_studystat) FROM ER_STUDYSTAT b WHERE b.fk_study = pk_study "
				+ "AND b.fk_site=(select fk_siteid from er_user where pk_user="+userId+") AND b.studystat_date = (SELECT MAX(c.studystat_date) "
				+ "FROM ER_STUDYSTAT c WHERE c.fk_study = pk_study "
				+ "AND c.fk_site=(select fk_siteid from er_user where pk_user="+userId+"))) "
				+ "WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study "
				+ "AND a.fk_site=(select fk_siteid from er_user where pk_user="+userId+") )>0 "
				+ "THEN(SELECT MAX(b.pk_studystat) FROM ER_STUDYSTAT b WHERE b.fk_study = pk_study "
				+ "AND b.fk_site=(select fk_siteid from er_user where pk_user="+userId+") AND b.studystat_date=(SELECT MAX(c.studystat_date) "
				+ "FROM ER_STUDYSTAT c WHERE c.fk_study = pk_study and c.fk_site=(select fk_siteid from er_user where pk_user="+userId+"))) "
				+ "ELSE (SELECT MAX(a.pk_studystat) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study) END) "
				+ ") as IRB, "
				+ "(select count(*) from er_patprot where pk_study = fk_study and patprot_stat = 1 and patprot_enroldt is not null) as enrolled,"
				+ "open_count,resolved_count,reopen_count,closed_count "
				+ "from er_study, "
				+ "(SELECT fk_study, "
				+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'open'),1,0)) AS open_count, "
				+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
				+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
				+ "SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'closed'),1,0)) AS closed_count "
				+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g "
				+ "WHERE b.fk_formquery = a.pk_formquery "
				+ "AND a.fk_field = c.fk_field "
				+ "AND d.pk_formsec = c.fk_formsec "
				+ "AND e.pk_formlib = d.fk_formlib "
				+ "AND pk_formlib = f.fk_formlib "
				+ "AND pk_patforms = fk_querymodule "
				+ "AND pk_patprot = fk_patprot "
				+ "AND b.pk_formquerystatus = (SELECT max(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
				+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
				+ "AND trunc(b.entered_on) = (SELECT max(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
				+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
				+ "AND f.RECORD_TYPE <> 'D' "
				+ "GROUP BY g.fk_study) aa "
				+ "where pk_study = aa.fk_study(+) "
				+ "AND (EXISTS(select * from er_studyteam h where pk_study = h.fk_study and h.fk_user = "+userId+" and nvl(studyteam_status,'Active') = 'Active')"
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) ";
				System.out.println("studySql " + studySql) ;

				break;
			case 1:
				//Query Activity
				//Today
				studySql = "SELECT pk_study, pk_patprot, study_number, fk_per, (SELECT per_code FROM er_per WHERE Pk_per = fk_per) as personCode, patprot_patstdid, pk_formlib, form_name,(nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "SUM(open_count) AS open_count, SUM(resolved_count) AS resolved_count, SUM(reopen_count) AS reopen_count, SUM(closed_count) AS closed_count, dt "
				+ "FROM( "
				+ "SELECT pk_study, pk_patprot, study_number, g.fk_per as fk_per, patprot_patstdid, pk_formlib, form_name, "
 				+ "SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open'),1,0)) AS open_count, "
 				+ "SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved'),1,0)) AS resolved_count, "
 				+ "SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 're-opened'),1,0)) AS reopen_count, "
 				+ "SUM(DECODE(fk_codelst_querystatus,(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 'closed'),1,0)) AS closed_count, "
 				+ "CASE WHEN TRUNC(b.entered_on) = TRUNC(SYSDATE) THEN TRUNC(b.entered_on) END AS dt "
 				+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,ER_STUDY z "
 				+ "WHERE b.fk_formquery = a.pk_formquery "
 				+ "AND a.fk_field = c.fk_field "
 				+ "AND d.pk_formsec = c.fk_formsec "
 				+ "AND e.pk_formlib = d.fk_formlib "
 				+ "AND pk_formlib = f.fk_formlib "
 				+ "AND pk_patforms = fk_querymodule "
 				+ "AND pk_patprot = fk_patprot "
 				+ "AND pk_study = g.fk_study(+) "
 				+ "AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
 				+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
 				+ "AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
 				+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
 				+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM h WHERE pk_study = h.fk_study AND h.fk_user = "+userId+" AND NVL(studyteam_status,'Active') = 'Active') "
 				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
  				+ "AND (TRUNC(b.entered_on)=TRUNC(SYSDATE)) "
  				+ "AND f.RECORD_TYPE <> 'D' "
 				+ "GROUP BY pk_study, pk_patprot, study_number, g.fk_per, patprot_patstdid, pk_formlib, form_name,b.entered_on "
 				+ ") GROUP BY pk_study, pk_patprot, study_number, fk_per, patprot_patstdid, pk_formlib, form_name,dt";

				break;
			case 2:
				//Query Activity
				//All Open
				studySql = "SELECT DISTINCT open_days, pk_study, pk_patprot, study_number, fk_per, (SELECT per_code FROM er_per WHERE Pk_per = fk_per) as personCode, patprot_patstdid, pk_formlib, form_name,(nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "CASE WHEN open_days=5 THEN 'Today' "
				+ "	 WHEN open_days=4 THEN 'Last 7 Days' "
				+ "	 WHEN open_days=3 THEN 'Last 15 Days' "
				+ "	 WHEN open_days=2 THEN 'Last 30 Days' "
				+ "	 WHEN open_days=1 THEN '> 30 Days' "
				+ "END AS open_when, count(*) AS queryCNT "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, g.fk_per, g.patprot_patstdid, pk_formlib, e.form_name, TRUNC(b.entered_on) AS entered_on, "
				+ "CASE WHEN b.entered_on = TRUNC(SYSDATE) THEN 5 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) AND b.entered_on >= TRUNC(SYSDATE) -7) THEN 4 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -7 AND b.entered_on >= TRUNC(SYSDATE) -15) THEN 3 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -15 AND b.entered_on >= TRUNC(SYSDATE) -30) THEN 2 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -30) THEN 1 "
				+ "END AS open_days "
				+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,ER_STUDY z "
				+ "WHERE b.fk_formquery = a.pk_formquery "
				+ "AND a.fk_field = c.fk_field "
				+ "AND d.pk_formsec = c.fk_formsec "
				+ "AND e.pk_formlib = d.fk_formlib "
				+ "AND pk_formlib = f.fk_formlib "
				+ "AND pk_patforms = fk_querymodule "
				+ "AND pk_patprot = fk_patprot "
				+ "AND pk_study = g.fk_study(+) "
				+ "AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
				+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
				+ "AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
				+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
				+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
				+ "AND f.RECORD_TYPE <> 'D' "
				+ "AND b.fk_codelst_querystatus = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 'open') "
				+ "AND trunc(b.entered_on) <= TRUNC(SYSDATE)) "
				+ "GROUP BY open_days, pk_study, pk_patprot, study_number, fk_per, patprot_patstdid, pk_formlib, form_name "
				+ "ORDER BY open_days ";
				//System.out.println("studySql " + studySql) ;
				break;
			case 3:
				//Query Activity
				//All Resolved
				studySql = "SELECT DISTINCT resolved_days, pk_study, pk_patprot, study_number, fk_per, (SELECT per_code FROM er_per WHERE Pk_per = fk_per) as personCode, patprot_patstdid, pk_formlib, form_name,(nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "CASE WHEN resolved_days=5 THEN 'Today' "
				+ "	 WHEN resolved_days=4 THEN 'Last 7 Days' "
				+ "	 WHEN resolved_days=3 THEN 'Last 15 Days' "
				+ "	 WHEN resolved_days=2 THEN 'Last 30 Days' "
				+ "	 WHEN resolved_days=1 THEN '> 30 Days' "
				+ "END AS resolved_when, count(*) AS queryCNT "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, g.fk_per, g.patprot_patstdid, pk_formlib, e.form_name, TRUNC(b.entered_on) AS entered_on, "
				+ "CASE WHEN b.entered_on = TRUNC(SYSDATE) THEN 5 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) AND b.entered_on >= TRUNC(SYSDATE) -7) THEN 4 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -7 AND b.entered_on >= TRUNC(SYSDATE) -15) THEN 3 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -15 AND b.entered_on >= TRUNC(SYSDATE) -30) THEN 2 "
				+ "	 WHEN (b.entered_on < TRUNC(SYSDATE) -30) THEN 1 "
				+ "END AS resolved_days "
				+ "FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g, ER_STUDY z "
				+ "WHERE b.fk_formquery = a.pk_formquery "
				+ "AND a.fk_field = c.fk_field "
				+ "AND d.pk_formsec = c.fk_formsec "
				+ "AND e.pk_formlib = d.fk_formlib "
				+ "AND pk_formlib = f.fk_formlib "
				+ "AND pk_patforms = fk_querymodule "
				+ "AND pk_patprot = fk_patprot "
				+ "AND pk_study = fk_study(+) "
				+ "AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery "
				+ "AND x.fk_codelst_querystatus = b.fk_codelst_querystatus "
				+ "AND trunc(b.entered_on) = (SELECT MAX(trunc(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery "
				+ "AND y.fk_codelst_querystatus = b.fk_codelst_querystatus)) "
				+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
				+ "AND f.RECORD_TYPE <> 'D' "
				+ "AND b.fk_codelst_querystatus = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'query_status' AND codelst_subtyp = 'resolved') "
				+ "AND trunc(b.entered_on) <= TRUNC(SYSDATE)) "
				+ "GROUP BY resolved_days, pk_study, pk_patprot, study_number, fk_per, patprot_patstdid, pk_formlib, form_name "
				+ "ORDER BY resolved_days";
				break;
			case 4:
				//Form Expectancy
				studySql = "SELECT pk_study, pk_patprot, study_number, (nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights,"
				+ "CASE WHEN period = 0 THEN 'Today' "
				+ "	 WHEN period > 0 AND period <= 7 THEN '1-7 Days' "
				+ "	 WHEN period > 7 AND period <= 15 THEN '8-15 Days' "
				+ "	 WHEN period > 15 AND period <= 30 THEN '16-30 Days' "
				+ "END AS due_when, fk_per, personCode, patprot_patstdid, fk_form, form_name "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) AS period, flag, fuzzy_duration_aft, "
				+ "fk_per, personCode, patprot_patstdid, fk_form, form_name "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, "
				+ "CASE "
				+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
				+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
				+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
				+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
				+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
				+ " END AS fuzzy_duration_aft,"
				+ " (SELECT 'X' FROM dual WHERE TRUNC(SYSDATE) > "
				+ "	  (CASE "
				+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
				+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
				+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
				+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
				+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time)"
				+ " END)) AS flag, fk_per, (SELECT per_code FROM er_per WHERE Pk_per = fk_per) as personCode, patprot_patstdid, fk_form, "
				+ " CASE "
				+ "  	  WHEN fk_form = 0 THEN other_links "
				+ " 	  ELSE (SELECT form_name FROM er_formlib WHERE pk_formlib = fk_form) "
				+ " END AS form_name "
				+ "FROM esch.sch_events1 a, ER_PATPROT b, esch.sch_event_crf c, event_assoc e, ER_STUDY f "
				+ "WHERE a.fk_assoc = c.fk_event "
				+ "AND e.event_id = a.fk_assoc "
				+ "AND pk_patprot = a.fk_patprot "
				+ "AND a.status = 0 AND b.patprot_stat = 1 "
				+ "AND f.pk_study = b.fk_study AND fk_account = "+ accountId +" "
				+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
 				+ "AND fk_form > 0 "
				+ ") WHERE ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) >= 0) "
				+ "WHERE period <= 30 "
				+ "ORDER BY period ";
				break;
			case 5:
				//Overdue Forms
				studySql = "SELECT pk_study, pk_patprot, study_number, (nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "CASE WHEN period < 0 AND period >= -7 THEN '<= 7 Days' "
				+ "	 WHEN period < -7 AND period >= -15 THEN '8-15 Days' "
				+ "	 WHEN period < -15 AND period >= -30 THEN '16-30 Days' "
				+ "	 WHEN period < -30 THEN '>30 Days' "
				+ "END AS overdue_for, fk_per, personCode, patprot_patstdid, Start_Date_Time, fk_form, form_name "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, trim(Start_Date_Time) as Start_Date_Time, ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) AS period, flag, fuzzy_duration_aft, "
				+ "fk_per, personCode, patprot_patstdid, fk_form, form_name "
				+ "FROM ( "
				+ "SELECT pk_study, pk_patprot, study_number, "
				+ "CASE "
				+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
				+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
				+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
				+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
				+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time) "
				+ "END AS fuzzy_duration_aft, "
				+ "(SELECT 'X' FROM dual WHERE TRUNC(SYSDATE) > "
				+ "	  (CASE "
				+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1)"
				+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1"
				+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7)"
				+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30)"
				+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time) "
				+ " END)) AS flag, fk_per, (SELECT per_code FROM er_per WHERE Pk_per = fk_per) as personCode, patprot_patstdid, Start_Date_Time, fk_form, "
				+ "CASE "
				+ "  	  WHEN fk_form = 0 THEN other_links "
				+ " 	  ELSE (SELECT form_name FROM er_formlib WHERE pk_formlib = fk_form) "
				+ "END AS form_name "
				+ "FROM esch.sch_events1 a, ER_PATPROT b, esch.sch_event_crf c, event_assoc e, ER_STUDY f "
				+ "WHERE a.fk_assoc = c.fk_event "
				+ "AND e.event_id = a.fk_assoc "
				+ "AND pk_patprot = a.fk_patprot "
				+ "AND f.pk_study = b.fk_study AND fk_account = "+ accountId +" "
				+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
 				+ "AND b.patprot_stat = 1  "
				+ "AND NOT EXISTS (SELECT * FROM er_patforms d WHERE fk_patprot = pk_patprot AND d.fk_per = b.fk_per AND d.fk_formlib = c.fk_form AND d.fk_sch_events1(+) = a.event_id AND d.RECORD_TYPE <> 'D' "
				+ ")) WHERE ROUND(fuzzy_duration_aft - TRUNC(SYSDATE)) <= 0) "
				+ "WHERE period < 0 AND FK_FORM > 0 "
				+ "ORDER BY period asc ";
				//System.out.println("studySql " + studySql) ;
				break;
			case 6:
				//Adverse Events
				studySql = "SELECT pk_study, study_number, pk_per, patprot_patstdid, personCode, (nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "SUM(aeCnt) AS aeCnt, SUM(saeCnt) AS saeCnt "
				+ "FROM ( "
				+ "SELECT pk_study, study_number, pk_per, patprot_patstdid, AdvTYPE, personCode, "
				+ "CASE WHEN AdvTYPE = 'Adverse Event' THEN ae ELSE 0 END AS aeCnt, "
				+ "CASE WHEN AdvTYPE = 'Serious Adverse Event' THEN ae ELSE 0 END AS saeCnt "
				+ "FROM ( "
				+ "SELECT SUM(cnt) AS ae, pk_study, study_number, pk_per,patprot_patstdid,AdvType, personCode "
				+ "FROM ("
				+ "SELECT COUNT(*) AS Cnt, pk_study, study_number, pk_per, per_code as personCode, "
				+ "(select distinct patprot_patstdid from er_patprot aa where aa.fk_study = pk_study and aa.fk_per = pk_per) as patprot_patstdid, "
				+ "'Adverse Event'  AS AdvType "
				+ "FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
				+ "WHERE pk_study = fk_study AND "
				+ "pk_per = fk_per AND "
				+ "ER_PER.fk_account =  "+ accountId +" "
				+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
 				+ "AND to_date(AE_STDATE) BETWEEN trunc(SYSDATE)-7 AND trunc(SYSDATE)-1 "
				+ "AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Adverse Event' "
				+ "GROUP BY ae_stdate, pk_study, study_number, pk_per, per_code) "
				+ "GROUP BY pk_study, study_number, pk_per, personCode, patprot_patstdid, AdvType "
				+ "UNION "
				+ "SELECT SUM(cnt) AS ae, pk_study, study_number, pk_per,patprot_patstdid,AdvType, personCode "
				+ "FROM ("
				+ "SELECT COUNT(*) AS Cnt, pk_study, study_number, pk_per, per_code as personCode, "
				+ "(select distinct patprot_patstdid from er_patprot where fk_study = pk_study and fk_per = pk_per) as patprot_patstdid,"
 				+ "'Serious Adverse Event' AS AdvType "
				+ "FROM ESCH.sch_adverseve, ER_STUDY, ER_PER "
				+ "WHERE pk_study = fk_study AND "
				+ "pk_per = fk_per AND "
				+ "ER_PER.fk_account = "+ accountId +" "
				+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
 				+ "AND to_date(AE_STDATE) BETWEEN trunc(SYSDATE)-7 AND trunc(SYSDATE)-1 "
				+ "AND (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) = 'Serious Adverse Event' "
				+ "GROUP BY  ae_stdate, pk_study, study_number, pk_per, per_code) "
				+ "GROUP BY pk_study, study_number, pk_per, personCode, patprot_patstdid, AdvType) "
				+ ") GROUP BY pk_study, study_number, pk_per, personCode, patprot_patstdid ";

				break;
			case 7:
				//Visit Related
				studySql = "SELECT DISTINCT pk_study, study_number, fk_per, patprot_patstdid, (nvl((select study_team_rights from er_studyteam where fk_user = "+userId+" and  fk_study = pk_study  and study_team_usr_type = 'D'),"+superuserRights+" ) ) as study_team_rights, "
				+ "SUM(CASE WHEN EVENT_STATUS = 'Done' THEN 1 END) AS doneCnt, "
				+ "SUM(CASE WHEN EVENT_STATUS = 'Past Scheduled Date' THEN 1 END) AS notDoneCnt "
				+ "FROM ( "
				+ "SELECT pk_study, study_number, fk_per, patprot_patstdid, PTSCH_ACTSCH_DATE, EVENT_STATUS, "
				+ "CASE WHEN eventstat_date > fuzzy_duration_aft OR eventstat_date < fuzzy_duration_bef THEN 'Yes' ELSE 'No' END AS out_of_range "
				+ "FROM ( "
				+ "SELECT c.FK_STUDY AS pk_study, (SELECT STUDY_NUMBER FROM ERES.ER_STUDY WHERE PK_STUDY = c.FK_STUDY) STUDY_NUMBER, "
				+ "fk_per, PERSON_CODE, PATPROT_PATSTDID, "
				+ "(SELECT NAME FROM EVENT_ASSOC WHERE EVENT_ASSOC.EVENT_ID = a.SESSION_ID) PTSCH_CALENDAR, "
				+ "(SELECT VISIT_NAME FROM SCH_PROTOCOL_VISIT WHERE PK_PROTOCOL_VISIT = a.FK_VISIT) PTSCH_VISIT, "
				+ "a.DESCRIPTION PTSCH_EVENT, Start_Date_Time PTSCH_ACTSCH_DATE, "
				+ " CASE "
				+ "  	  WHEN event_durationbefore ='D' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) * 1) "
				+ " 	  WHEN event_durationbefore ='H' THEN (Start_Date_Time) - 1 "
				+ " 	  WHEN event_durationbefore ='W' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) * 7) "
				+ " 	  WHEN event_durationbefore ='M' THEN (Start_Date_Time) - (TO_NUMBER(fuzzy_period) *30) "
				+ " 	  WHEN event_durationbefore IS NULL THEN (Start_Date_Time) "
				+ " END AS fuzzy_duration_bef, "
				+ " CASE "
				+ "  	  WHEN event_durationafter ='D' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 1) "
				+ " 	  WHEN event_durationafter ='H' THEN (Start_Date_Time) + 1 "
				+ " 	  WHEN event_durationafter ='W' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) * 7) "
				+ " 	  WHEN event_durationafter ='M' THEN (Start_Date_Time) + (TO_NUMBER(event_fuzzyafter) *30) "
				+ " 	  WHEN event_durationafter IS NULL THEN (Start_Date_Time) "
				+ " END AS fuzzy_duration_aft, "
				+ "CASE "
				+ "         WHEN Start_Date_Time < TRUNC(SYSDATE) AND (SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst = isconfirmed) = 'ev_notdone' THEN 'Past Scheduled Date' "
				+ "         ELSE (SELECT CODELST_DESC FROM SCH_CODELST WHERE ISCONFIRMED = PK_CODELST) "
				+ "END AS EVENT_STATUS, "
				+ "EVENT_EXEON EVENTSTAT_DATE, a.notes EVENTSTAT_NOTES "
				+ "FROM SCH_EVENTS1 a,person b,ER_PATPROT c, EVENT_ASSOC d "
				+ "WHERE pk_person = c.fk_per AND "
				+ "pk_patprot = fk_patprot AND "
				+ "d.EVENT_ID = a.fk_assoc AND "
				+ "Start_Date_Time < TRUNC(SYSDATE) AND "
				+ "b.fk_ACCOUNT = "+ accountId +" "
				+ "AND (EXISTS (SELECT * FROM er_studyteam h WHERE c.fk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", c.fk_study) = 1)) "
 				+ "WHERE (EVENT_STATUS = 'Past Scheduled Date' OR (EVENT_STATUS = 'Done' AND (eventstat_date > fuzzy_duration_aft OR eventstat_date < fuzzy_duration_bef)))"
				+ ") "
				+ "WHERE (PTSCH_ACTSCH_DATE <= TRUNC(SYSDATE) AND "
				+ "PTSCH_ACTSCH_DATE >= TRUNC(SYSDATE-7) "
				+ ") "
				+ "GROUP BY pk_study, study_number, fk_per, patprot_patstdid";
				System.out.println("studySql " + studySql) ;
				break;
			}

			countSql = "select count(*) from (" + studySql + ")";
			rowsPerPage=0;
			totalPages=0;
			rowsReturned = 0;
			showPages = 0;
			totalRows = 0;
			firstRec = 0;
			lastRec = 0;
			openCount = 0;
			resolvedCount = 0;
			reopenCount = 0;
			closedCount = 0;
			enrolled = 0;

			studyNumber = null;
			studyTitle = null;
			studyStatus = null;
			studyActualDt = null;
			studyIRB = null;
			studyIRB2 = null;
			studySite = null;
			studyPI = null;

			studyId=0;
			hasMore = false;
			hasPrevious = false;

			//		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			rowsPerPage =  1000;
			totalPages =Configuration.PAGEPERBROWSER ;
			br = new BrowserRows();

			br.getPageRows(curPage,rowsPerPage,studySql,totalPages,countSql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();
		%>

		<Input type="hidden" name="srcmenu" value="<%=src%>">
		<Input type="hidden" name="page" value="<%=curPage%>">
		<Input type="hidden" name="orderBy" value="<%=orderBy%>">
		<Input type="hidden" name="orderType" value="<%=orderType%>">
		<BR>
		<%
		String nodeName ="";
		
		switch (nodeNo){
	    	case 0:
	    		nodeName=LC.L_Query_Summary;/*nodeName="Query Summary";*****/
				break;
	    	case 1:
	    		nodeName=LC.L_QryAct_Today;/*nodeName="Query Activity- Today";*****/
				break;
    	 	case 2:
    	 		nodeName=MC.M_QryAct_AllOpen;/*nodeName="Query Activity- All Open";*****/
				break;
    	 	case 3:
    	 		nodeName=MC.M_QryAct_AllRslvd;/*nodeName="Query Activity- All Resolved";*****/
				break;
			case 4:
				nodeName=LC.L_Form_Expectancy ;/*nodeName="Form Expectancy";*****/
				break;
			case 5:
				nodeName=LC.L_Overdue_Forms;/*nodeName="Overdue Forms";*****/
				break;
			case 6:
				nodeName=MC.M_PcolDvat_AdvEvt;/*nodeName="Protocol Deviations- Adverse Events";*****/
				break;
    		case 7:
    			nodeName=MC.M_PcolDvat_VstRtd;/*nodeName="Protocol Deviations- Visit related";*****/
				break;
		}%>
		<Table width="100%">
			<tr bgcolor="#dcdcdc" height="30">
				<td><b><%=nodeName%></b> </td>
			</tr>
		</Table>
		<br>
	    <table width="100%" >
	    	<tr> <%
	    	int perId = 0;
	    	
	    	switch (nodeNo){
	    	case 0:
	    		//Query Summary%>
	    		<th width="8%"></th>
	        	<th width="8%" onClick="setOrder(document.studybrowserpg,4)"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
				<th width="12%"><%=LC.L_PI%><%--PI*****--%></th>
				<th width="13%"> <%=LC.L_Status%><%--Status*****--%></th>
	        	<th width="8%"> <%=LC.L_Accrual%><%--Accrual*****--%></th>
	 	    	<th width="8%"> <%=LC.L_Open%><%--Open*****--%></th>
	 	    	<th width="8%"> <%=LC.L_Resolved%><%--Resolved*****--%></th>
	 	    	<th width="8%"> <%=LC.L_Reopened%><%--Reopened*****--%></th>
	 	    	<th width="8%"> <%=LC.L_Closed%><%--Closed*****--%></th>
	    	    <!--th width="6%"></th-->
			</tr>
				<%

				String mPatRight ="";
		    	for(int counter = 1;counter<=rowsReturned;counter++){
					studyId=EJBUtil.stringToNum(br.getBValues(counter,"pk_study")) ;
					studyNumber=br.getBValues(counter,"study_number");
					studyStatus=br.getBValues(counter,"status");
				    studyStatus = (  studyStatus ==null )?"-":( studyStatus) ;
					studyIRB=br.getBValues(counter,"IRB");
				    studyIRB = (  studyIRB ==null )?"-":( studyIRB) ;
					studyPI=br.getBValues(counter,"pi");
				    studyPI = (  studyPI ==null )?"-":( studyPI) ;
					studyActualDt = br.getBValues(counter,"STUDY_ACTUALDT");
				    openCount = EJBUtil.stringToNum(br.getBValues(counter,"open_count"));
				    resolvedCount = EJBUtil.stringToNum(br.getBValues(counter,"resolved_count"));
				    reopenCount = EJBUtil.stringToNum(br.getBValues(counter,"reopen_count"));
				    closedCount = EJBUtil.stringToNum(br.getBValues(counter,"closed_count"));
				    enrolled = EJBUtil.stringToNum(br.getBValues(counter,"enrolled"));
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}


				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  			<%if(studyActualDt!=null && EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%-- <%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
					<%}%>
				</td>
				<td><%=studyNumber%></td>
				<td> <%=studyPI%> </td>
				<td> <%=studyStatus%> </td>
				<td ALIGN="CENTER"><%=enrolled%></td>
					<%if (openCount == 0){%>
				<td ALIGN="CENTER" BGCOLOR="E0D185"> <%=openCount%> </td>
					<%} else{%>
				<td ALIGN="CENTER" BGCOLOR="cd5c5c"> <%=openCount%> </td>
					<%}
					if (resolvedCount == 0) {%>
				<td ALIGN="CENTER" BGCOLOR="E0D185"> <%=resolvedCount%> </td>
					<%}	else{%>
				<td ALIGN="CENTER" BGCOLOR="A9D355"> <%=resolvedCount%> </td>
					<%}
					if (reopenCount == 0){%>
				<td ALIGN="CENTER" BGCOLOR="E0D185"> <%=reopenCount%> </td>
					<%}else	{%>
				<td ALIGN="CENTER" BGCOLOR="cd5c5c"> <%=reopenCount%> </td>
					<%}
					if (closedCount == 0) {%>
				<td ALIGN="CENTER" BGCOLOR="E0D185"> <%=closedCount%> </td>
					<%}else{%>
				<td ALIGN="CENTER" BGCOLOR="A9D355"> <%=closedCount%> </td>
					<%}%>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 1:
	    		//Today%>
	    		<th width="2%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	        	<th width="3%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	        	<th width="8%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
	        	<th width="2%"><%=LC.L_Open%><%--Open*****--%></th>
	        	<th width="2%"><%=LC.L_Resolved%><%--Resolved*****--%></th>
	        	<th width="2%"><%=LC.L_Reopened%><%--Reopened*****--%></th>
	        	<th width="2%"><%=LC.L_Closed%><%--Closed*****--%></th>
	        	<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}

				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
						<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%-- <%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a> <%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"form_name")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"open_count")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"resolved_count")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"reopen_count")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"closed_count")%> </td>
		    	<td align="center"> 
					<% 
					perId = EJBUtil.stringToNum(br.getBValues(counter,"fk_per"));
					
					//KM-#4099	
					if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>	
					<A onclick="openFilledFormWin(<%=perId%>,<%=personCode%>,<%=patprotId%>,<%=studyId%>)" href=#> <%=LC.L_View_Form%><%--View Form*****--%> </A>
					<%}%>	
					
					</td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 2:
	    		//All Open%>
	    		<th width="2%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="5%"><%=LC.L_Open_For%><%--Open For*****--%></th>
	    		<th width="3%"><%=LC.L_Query_Count%><%--Query Count*****--%></th>
	        	<th width="3%"><%=LC.L_Patient_StudyId%><%-- <%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	        	<th width="5%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}

				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a> <%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td> <%=br.getBValues(counter,"open_when")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"queryCNT")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"form_name")%> </td>
				<td align="center">
					<% 
					perId = EJBUtil.stringToNum(br.getBValues(counter,"fk_per"));
					
					//KM-#4099	
					if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<A onclick="openFilledFormWin(<%=perId%>,<%=personCode%>,<%=patprotId%>,<%=studyId%>)" href=#> <%=LC.L_View_Form%><%--View Form*****--%> </A>
					<%}%>

					</td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 3:
	    		//All Resolved%>
	    		<th width="2%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="5%"><%=LC.L_Resolved%><%--Resolved*****--%></th>
	    		<th width="3%"><%=LC.L_Query_Count%><%--Query Count*****--%></th>
	        	<th width="3%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	        	<th width="5%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a> <%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td> <%=br.getBValues(counter,"resolved_when")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"queryCNT")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"form_name")%> </td>
				<td align="center"> 
					<% 
					perId = EJBUtil.stringToNum(br.getBValues(counter,"fk_per"));
					//KM-#4099	
					if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<A onclick="openFilledFormWin(<%=perId%>,<%=personCode%>,<%=patprotId%>,<%=studyId%>)" href=#> <%=LC.L_View_Form%><%--View Form*****--%> </A>		
					<%}%>

					</td>
			</tr>
				<%}%>
		</table>
	      		<%break;
    		case 4:
	    		//Form Expectancy%>
	    		<th width="3%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="3%"><%=LC.L_Due%><%--Due*****--%></th>
	        	<th width="3%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	        	<th width="6%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
	    		<th width="3%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}

				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a> <%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td> <%=br.getBValues(counter,"due_when")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"form_name")%> </td>
				<td align="center"> <A href='patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=M&pkey=<%=br.getBValues(counter,"fk_per")%>&patProtId=<%=patprotId%>&patientCode=<%=personCode%>&page=patientEnroll&studyId=<%=studyId%>&generate=N'> <%=LC.L_View_Form%><%--View Form*****--%> </A></td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 5:
	    		//Overdue Forms%>
	    		<th width="3%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="3%"><%=LC.L_Overdue_For%><%--Overdue For*****--%></th>
	        	<th width="2%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	        	<th width="2%"><%=LC.L_ActualScheduled_Dt%><%--Actual Scheduled Date*****--%></th>
	        	<th width="5%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
	    		<th width="3%"></th>
			</tr>
				<%
		    	//System.out.println("rowsReturned " + rowsReturned) ;
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a>
						<%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td> <%=br.getBValues(counter,"overdue_for")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"Start_Date_Time")%> </td>
				<td> <%=br.getBValues(counter,"form_name")%> </td>
				<td align="center"> <A href='patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=M&pkey=<%=br.getBValues(counter,"fk_per")%>&patProtId=<%=patprotId%>&patientCode=<%=personCode%>&page=patientEnroll&studyId=<%=studyId%>&generate=N'> <%=LC.L_View_Form%><%--View Form*****--%> </A></td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 6:
	    		//Adverse Events%>
	    		<th width="2%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="3%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	    		<th width="3%"><%=LC.L_Adverse_Events%><%--Adverse Events*****--%></th>
	    		<th width="3%"><%=LC.L_Serious_AdverseEvents%><%--Serious Adverse Events *****--%> </th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a>
					<%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td> <%=br.getBValues(counter,"aeCnt")%> </td>
				<td> <%=br.getBValues(counter,"saeCnt")%> </td>
				<td align="center"> <A href='adveventbrowser.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&pkey=<%=br.getBValues(counter,"pk_per")%>&studyId=<%=studyId%>'> <%=LC.L_View_Ae %><%-- View AE*****--%> </A></td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	case 7:
	    		//Visit Related%>
	    		<th width="2%"></th>
	    		<th width="3%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="3%"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%></th>
	    		<th width="4%"><%=LC.L_Done_Count%><%--Done Count*****--%></th>
	    		<th width="4%"><%=LC.L_Not_DoneCount%><%--Not Done Count*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int patprotId = EJBUtil.stringToNum(br.getBValues(counter,"pk_patprot"));
		    		String personCode = br.getBValues(counter,"personCode");
					stdTeamRight = br.getBValues(counter,"study_team_rights");
					
					if (stdTeamRight.length() >= 11 )
					{
						mPatRight = stdTeamRight.substring(iRightSeq - 1, iRightSeq);
					}
					else
					{
						mPatRight = "0";
					}
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow">
		    		<%}	else{%>
		  	<tr class="browserOddRow">
		    		<%}%>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%-- <%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
					
					<% if (EJBUtil.stringToNum(mPatRight) > 0 ) {%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patient%>*****--%>" border=0 ></a>
					<%}%>
				</td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"patprot_patstdid")%> </td>
				<td align="center"> <%=(br.getBValues(counter,"doneCnt")==null )?"-":(br.getBValues(counter,"doneCnt"))%> </td>
				<td align="center"> <%=(br.getBValues(counter,"notDoneCnt")==null )?"-":(br.getBValues(counter,"notDoneCnt"))%> </td>
				<td align="center"> <A href='patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=M&pkey=<%=br.getBValues(counter,"fk_per")%>&page=patientEnroll&patProtId=<%=patprotId%>&patientCode=<%=personCode%>&page=patientEnroll&generate=N&studyId=<%=studyId%>'> <%=LC.L_View_Schedule%><%--View Schedule*****--%> </A></td>
			</tr>
				<%}%>
		</table>
	      		<%break;
	    	}%>

		<table>
			<tr>
				<td>
				<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
				<%} else {%>
					<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
				<%}%>
				</td>
			</tr>
		</table>
		<%} //end of if body for page right
		else{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%} //end of else body for page right
	} //end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>

</body>



</html>