<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) { 
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=MC.M_StdAppdx_LnkDets%><%--<%=LC.Std_Study%> Appendix >> Link Details*****--%></title>
<% } %>





<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</HEAD>



<% String src;

src= request.getParameter("srcmenu");
String from  = "appendixver";
%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<SCRIPT Language="javascript">

 function  validate(formobj){

     formobj=document.upload

     if (!(validate_col('File',formobj.name))) return false

     if (!(validate_col('Description',formobj.desc))) return false
       if(formobj.desc.value.length>500){  
	     alert(" <%=MC.M_SrtDesc_MaxChar%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
	     formobj.desc.focus();
	     return false;
     } 
     if (!(checkquote(formobj.desc.value))) return false
	
//     if (!(validate_col('Esign',formobj.eSign))) return false
//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect e-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
 //  }


 }

	function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<DIV class="BrowserTopn" id="div1"> 
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  </jsp:include>
  </DIV>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
 
  <%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
{
	String uName = (String) tSession.getValue("userName");
	String sessStudyId = (String) tSession.getValue("studyId");
	String studyNo = (String) tSession.getValue("studyNo");
	String tab = request.getParameter("selectedTab");
	String studyVerId = request.getParameter("studyVerId");
	int stId=EJBUtil.stringToNum(sessStudyId);
%>

   <form name=upload id = "updloadForm" METHOD="POST" action="appendix_url_insert.jsp" onsubmit = "if (validate(document.savecopy)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <input type="hidden" name="studyVerId" value=<%=studyVerId%>>	
    <TABLE width="98%">
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <b><%=MC.M_AddLkTo_StdAppx%><%--Add Links to your <%=LC.Std_Study%>'s Appendix*****--%> </b></P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right">  <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=text name=name MAXLENGTH=255 size=50>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com' 
            or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <TextArea type=text name=desc row=3 cols=50 > </TextArea>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_NameToLnk_500CharMax%><%--Give a friendly name to your link. (500 char 
            max.)*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td  colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available 
          to the public?*****--%> </td>
      </tr>
      <tr> 
        <td colspan=2> 
          <input type="Radio" name="pubflag" value=Y>
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N CHECKED>
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_PublicVsNotPublic_Info%><%--What 
          is Public vs Not Public Information?*****--%></A> </td>
      </tr>
    </table>
    <input type="hidden" name="type" value='url'>
    <input type="hidden" name="study" value=<%=stId%>>
    <input type="hidden" name="studyId" value=<%=stId%>>
    <input type="hidden" name="srcmenu" value=<%=src%>>	
    <BR>

		
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="updloadForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

	
  </form>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
