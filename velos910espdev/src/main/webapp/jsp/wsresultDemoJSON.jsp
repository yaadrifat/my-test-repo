<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>

<html>


<body>
<%@ page import="org.json.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*, com.velos.service.*, com.velos.remoteservice.*, com.velos.remoteservice.demographics.*, com.velos.eres.business.common.CodeDao, com.velos.eres.business.common.CommonDAO" %>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="commonB" scope="page"
	class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="accB" scope="request"
	class="com.velos.eres.web.account.AccountJB" />
<%
	String returnString = null;
	String errorString = "";
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		String mode = request.getParameter("mode");
		if (mode == null) mode = "";
		/*
		We support two modes of search:
			"list" is a name-based search that returns minimal
			data for a list of patients.

			"single" is a search that returns
			detailed information about a single patient.
		*/
		String lastName = request.getParameter("lastName");
		String middleName = request.getParameter("middleName");
		String firstName = request.getParameter("firstName");
		String gender = request.getParameter("gender");
		Date DOB = DateUtil.stringToDate(request.getParameter("dob"), "MM/dd/yyyy");
		Map searchParameters = new HashMap();
		searchParameters.put("firstName", firstName);
		searchParameters.put("middleName", middleName);
		searchParameters.put("lastName", lastName);
		searchParameters.put("dob", DOB);
		searchParameters.put("gender", gender);

		if (mode.equalsIgnoreCase("list")){

			try{
				//Gender comes to us in the dropdown as the primary in er_codelst.
				//de-reference it here in the CodeDao
				//CodeDao cd1 = new CodeDao();
				//cd1.getCodeValues("gender");
				//String gender = cd1.getCodeDesc(genderPK);

				//find the remote service
				String accountNumStr = (String)tSession.getAttribute("accountId");
				int accountNo = EJBUtil.stringToNum(accountNumStr);
				IDemographicsService demographicsService =
					DemographicsServiceFactory.getService(accountNo);

				if (demographicsService == null){
					throw new RuntimeException("Could not find remote demographics service.");
				}

				List patients = null;
				//invoke the remote service


				try {
					patients = demographicsService.getPatientDemographics(searchParameters);
				} catch (PatientNotFoundException e) {
					e.printStackTrace();

				}
				if (patients.size() == 0){
					errorString = "No "+LC.Pat_Patients+" found.";
				}
				else{

					JSONArray jsPatList = new JSONArray();
					for (int patCnt = 0; patCnt < patients.size(); patCnt++){
						IPatientDemographics patient = (IPatientDemographics)patients.get(patCnt);

						JSONObject jsPatient = new JSONObject(patient);
						jsPatient.put("searchIndex", patCnt);
						jsPatList.put(jsPatient);

					}
					returnString = jsPatList.toString();

				}
			}
			catch(Throwable t){
				t.printStackTrace();
				errorString = "Oops. An error has occurred talking to the "+LC.Pat_Patient_Lower+" search service. Please contact your system administrator.";

			}
		}

		//return detailed information about an individual patient
		else{

			try{
			  //find the remote service
			  String accountNumStr = (String)tSession.getAttribute("accountId");
			  int accountNo = EJBUtil.stringToNum(accountNumStr);
			  IDemographicsService demographicsService = DemographicsServiceFactory.getService(accountNo);
			  IPatientDemographics patient = null;
			  JSONObject jsPatient = null;
			  try {
					patient = (IPatientDemographics)demographicsService.getPatientDemographics(searchParameters).get(0);
					jsPatient = new JSONObject(patient);
					returnString = jsPatient.toString();

			   } catch (PatientNotFoundException e) {
				    errorString = LC.Pat_Patient+" Not Found.";
			  		e.printStackTrace();
			   }

			   if (patient == null){
				   errorString = LC.Pat_Patient+" not found.";
			   }

			}
			catch(Throwable t){
				errorString = "Oops. An error has occurred talking to the "+LC.Pat_Patient_Lower+" search service. Please contact your system administrator.";
				t.printStackTrace();
			}
%>


<%
		}
	}


%>
<div style="visibility: hidden;" id="jsonReturn"><%=returnString%></div>
<div style="visibility: hidden;" id="jsonError"><%=errorString%></div>
</body>


</html>

<%!
public String clean(String input){
	if (input == null){
		return "";
	}
	return input;
}

public String getCodePK(
		String codeType,
		String remoteCode,
		ArrayList keyList,
		ArrayList remoteCodeList,
		ArrayList typeList){

	for (int x = 0; x < keyList.size(); x++){
		String type = (String)typeList.get(x);
		if (type.equalsIgnoreCase(codeType)){
			String testRemoteCode = (String)remoteCodeList.get(x);
			if (remoteCode.equalsIgnoreCase(testRemoteCode)){
				return (String)keyList.get(x);
			}
		}
	}
	return null;
}

%>