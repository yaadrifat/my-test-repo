<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<head>

</head>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<script>
function callOverlib(id,name,subtype,status) {
	return overlib(
			'<tr><td><font size=2><%=LC.L_Storage_Id%><%--Storage ID*****--%>: </font></td>'+
            '<td><font size=2>'+id+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Name%><%--Name*****--%>: </font></td>'+
            '<td><font size=2>'+name+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Type%><%--Type*****--%>: </font></td>'+
            '<td><font size=2>'+subtype+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Status%><%--Status*****--%>: </font></td>'+
            '<td><font size=2>'+status+'</font></td></tr>');
}
function handleKeyPress(e,formobj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
		{
			// handle Enter key
			// return false; here to cancel the event
			callAjaxGetStorages(formobj)
		}
}

	function onSubmit()
	{
	 return false;
	}

	function selectStorage(locName,cx,cy,pk)
	{

	frmName = document.search;

	if(frmName.callingform.value != "")
	{
		genOpenerFormName = frmName.callingform.value;

		locationFldName = frmName.locationFldName.value;

		coordXFldName = frmName.coordXFldName.value;

		coordYFldName = frmName.coordYFldName.value;

		storagePKFldName = frmName.storagePKFldName.value;


		window.opener.document.forms[genOpenerFormName].elements[locationFldName].value = locName;
		window.opener.document.forms[genOpenerFormName].elements[coordXFldName].value = cx;
		window.opener.document.forms[genOpenerFormName].elements[coordYFldName].value = cy;

		window.opener.document.forms[genOpenerFormName].elements[storagePKFldName].value = pk;

		self.close();
		return;
	}
	//
	}

	function selectStorage4Specimen(locName,pk)
	{

	frmName = document.search;

	if(frmName.callingform.value != "")
	{
		genOpenerFormName = frmName.callingform.value;

		locationFldName = frmName.locationFldName.value;


		storagePKFldName = frmName.storagePKFldName.value;


		window.opener.document.forms[genOpenerFormName].elements[locationFldName].value = locName;

		window.opener.document.forms[genOpenerFormName].elements[storagePKFldName].value = pk;

		self.close();
		return;
	}
	//
	}

	function callAjaxGetStorages(formobj){
	    name = formobj.storageName.value;
	    id = formobj.storageID.value;
	    gridFor = formobj.gridFor.value;
	    storageType = formobj.storageType.value;
	    srchLocFlag = formobj.srchLocFlag.value;

 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
   		urlData:"dispType=B&storageId="+id+"&name="+name+"&gridFor="+gridFor+"&storageType="+storageType
   		       +"&pkStorage="+formobj.pkStorage.value+"&srchLocFlag="+srchLocFlag,
		   reqType:"POST",
		   outputElement: "span_storage" }

   ).startRequest();

    storage_path.innerHTML = "";
    span_storagechild.innerHTML = "";
    formobj.childspanname.value="";

       if (formobj.pkStorage.value == "0" ) { return; }
       callAjaxGetStorageParents(formobj);
  	  var elem = document.getElementById("span_storagechild");
   }

   function callAjaxGetStorageParents(formobj) {
       var childspanname = "span_storagechild";
 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
 	   		urlData:"child="+formobj.pkStorage.value+"&dispType=G&explorer=1&gridFor="+formobj.gridFor.value,
 			   reqType:"POST",
 			   outputElement: childspanname }
 	       ).startRequest();
   }

   function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row){
	   callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
			   topStorage,topStorageName,level,column,row, null);
   }
   function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row, explorer){
       // alert('parent='+parent+' gridFor='+gridFor+' grandparent='+grandparent+' topStorage='+topStorage+' topStorageName='+topStorageName+
       //        ' level='+level+' column='+column+' explorer='+explorer);
	   var curdivs;
       for (var iY=(level+1); iY<702; iY++) {
	       var elem = document.getElementById("span_storagechild"+iY);
	       if (elem == null) { break; }
		   elem.innerHTML = "";
       }
       var childspanname = "span_storagechild" + (level+1);

	   if (parent == topStorage) {
		   storage_path.innerHTML = "";
		   span_storagechild.innerHTML = "";
       }
       oldchHTML = span_storagechild.innerHTML;
 	   // span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;position:relative;margin-left: -20px;\"></span>";
	   span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;\"></span>";

	   srchLocFlag = document.search.srchLocFlag.value;
	   var explorerParameter = explorer == null ? "" : "&explorer="+explorer;
 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
   		urlData:"parent="+parent+"&dispType=G&gridFor="+gridFor+"&parentName="+parentname+
   		        "&pkTopStorage="+topStorage+"&topStorageName="+topStorageName+explorerParameter+"&srchLocFlag="+srchLocFlag,
		   reqType:"POST",
		   outputElement: childspanname }
       ).startRequest();
 	   var eachTd = null;
       for (var iY=1; iY<702; iY++) {
 	       eachTd = document.getElementById("td_level"+level+"_col1_row"+iY);
 	       if (eachTd == null) { break; }
           for (var iX=1; iX<702; iX++) {
     	       eachTd = document.getElementById("td_level"+level+"_col"+iX+"_row"+iY);
     	       if (eachTd == null) { break; }
     	       if (iY %2 == 1) eachTd.style.backgroundColor = "#DADADA";
     	       else eachTd.style.backgroundColor = "#F7F7F7";
           }
       }
       if (row == null) row = 1;
 	   var td = document.getElementById("td_level"+level+"_col"+column+"_row"+row);
 	   if (td != null) {
 	      td.style.backgroundColor = "#FDFDDE";
 	   }
   }
   function goToDetailInParent(pkStorage) {
	   window.opener.location.href = 'storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=M&pkStorage='+pkStorage;
	   setTimeout('self.close()',1000);
   }

</script>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>




<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<%
	String gridFor = "";
	String formName = "";
	String locationFldName="";
	String coordXFldName="";
	String coordYFldName="";
	String storagePKFldName="";
	String storageTyp = null;
	String dstorageType = null;

	String spSrchLocFlag = request.getParameter("srchLocFlag");
	spSrchLocFlag = (spSrchLocFlag==null)?"":spSrchLocFlag;

	gridFor = request.getParameter("gridFor");

	if (StringUtil.isEmpty(gridFor))
	{
		gridFor = "ST";
	}

	formName = request.getParameter("form");

	if (StringUtil.isEmpty(formName))
	{
	 formName = "";
	}

	locationFldName = request.getParameter("locationName");

	if (StringUtil.isEmpty(locationFldName))
	{
	 locationFldName = "";
	}

	coordXFldName= request.getParameter("coordx");

	if (StringUtil.isEmpty(coordXFldName))
	{
	 coordXFldName = "";
	}

	coordYFldName = request.getParameter("coordy");
	if (StringUtil.isEmpty(coordYFldName))
	{
	 coordYFldName = "";
	}

	storagePKFldName = request.getParameter("storagepk");

	if (StringUtil.isEmpty(storagePKFldName))
	{
	 storagePKFldName = "";
	}

	String pkStorage = request.getParameter("pkStorage");
	if (StringUtil.isEmpty(pkStorage)) { pkStorage = "0"; }

	CodeDao cd = new CodeDao();

//JM: 28Sep2009: #4302
	//cd.getCodeValues("store_type");
	  cd.getCodeValuesSaveKit("store_type");

    storageTyp = request.getParameter("storageType");
    if (StringUtil.isEmpty(storageTyp)) { storageTyp = ""; }
    if (storageTyp.equals("")) {
        dstorageType=cd.toPullDown("storageType");
    } else {
        dstorageType=cd.toPullDown("storageType",EJBUtil.stringToNum(storageTyp),true);
    }


    boolean isHierarchyView = false;
%>
<% if ("0".equals(pkStorage)) { %>
<title><%=LC.L_Search_StorageLocation%><%--Search Storage Location*****--%></title>
<% } else {
       isHierarchyView = true;
%>
<title><%=LC.L_Storage_HierarchyView%><%--Storage Hierarchy View*****--%></title>
<% }  %>
<body>
<form name="search" method="POST">
<table id="searchBar" width="100%" cellspacing="0" cellpadding="0" border="0"  class="basetbl outline midalign">

<tr>
	<td><%=LC.L_Storage_Type%><%--Storage Type*****--%></td><td><%=dstorageType%>
	</td>
	<td><%=LC.L_Storage_Id%><%--Storage ID*****--%></td><td><Input type = "text" name="storageID"  onkeypress="return handleKeyPress(event,document.search);" value = "" size=15 align = right>
	</td>
	<td><%=LC.L_Storage_Name%><%--Storage Name*****--%></td><td><Input type = "text" name="storageName" onkeypress="return handleKeyPress(event,document.search);" value = "" size=15 align = right>
	</td>
	<td><button type="submit" onclick="callAjaxGetStorages(document.search);"><%=LC.L_Search%></button></td>
	</tr>
</table>
<% if (!"0".equals(pkStorage)) { %>
    <script>
    document.getElementById('searchBar').style.display="none";
    </script>
<% }  %>

<Input type = "hidden" name="gridFor"  value = "<%=gridFor%>" size=15 align = right>
<Input id="textpath" type = "hidden" name="childspanname"  value = "" size=115 align = right>

<Input  type = "hidden" name="callingform"  value = "<%=formName%>" size=115 align = right>
<Input  type = "hidden" name="locationFldName"  value = "<%=locationFldName%>" size=115 align = right>
<Input  type = "hidden" name="coordXFldName"  value = "<%=coordXFldName%>" size=115 align = right>
<Input  type = "hidden" name="coordYFldName"  value = "<%=coordYFldName%>" size=115 align = right>
<Input  type = "hidden" name="storagePKFldName"  value = "<%=storagePKFldName%>" size=115 align = right>
<Input  type = "hidden" name="pkStorage"  value = "<%=pkStorage%>" size=115 align = right>
<Input  type = "hidden" name="srchLocFlag" value="<%=spSrchLocFlag%>">




<BR><BR>
<span ID="storage_path" ></span>


<% if(!isHierarchyView) { %>
<div ID="span_storage" style="float:left;overflow:auto;width:100%;height:150%;" ></div>
<% }  %>

<span ID="span_storagechild" style="overflow:auto;float:left;width:100%;height:150%;"></span>

</form>
<div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
  <script>

  	callAjaxGetStorages(window.document.search);
  </script>
</body>