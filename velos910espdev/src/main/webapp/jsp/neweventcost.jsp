<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLib_CostSpecCost%><%--Event Library >> Event Cost >> Specify Cost*****--%> </title>
	<%
	} else {%>
		<title><%=LC.L_Evt_CostSpecify%><%--Event Cost Specify*****--%> </title>	
	<%
	}
%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.*"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao,com.velos.esch.business.common.*"%>

<jsp:useBean id="eventcostB" scope="request" class="com.velos.esch.web.eventcost.EventcostJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>
 <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>	
 <%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
	%>

<SCRIPT Language="javascript">



 function  validate(formobj){

	totRows = 0;
	myCount = 0;

	for(count = 0; count<5;count++){ 
		if((formobj.costDesc[count].value == "")  && (formobj.costNum[count].value== "" || formobj.costFrac[count].value== ""))
			 {
				myCount++;
			 }
	}
			if(myCount==5){
			alert("<%=MC.M_AtleastOne_EvtCost%>");/*alert("Please enter atleast one event cost");*****/
			return false;
		 }

	 for(cnt = 0; cnt<5;cnt++){ 



	 	 formobj.cost[cnt].value = formobj.costNum[cnt].value + "." + formobj.costFrac[cnt].value;

		
//Comment by Ganapathy on 05/11/05

//	     if ((formobj.costDesc[cnt].value != "") && (formobj.cur[cnt].value == "")){

//			 if (!(validate_col('Currency',formobj.cur[cnt]))) return false;
	     if ((formobj.costDesc[cnt].value != "") ){
			 if(formobj.cost[cnt].value=='' || formobj.cost[cnt].value==null || formobj.cost[cnt].value=='.'){

                alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/

                formobj.costNum[cnt].focus()

                return false;

			}
			if(formobj.costFrac[cnt].value=='' || formobj.costFrac[cnt].value==null || formobj.costFrac[cnt].value=='.'){
				formobj.costFrac[cnt].value = '00';
			}
	     }
//Comment by Ganapathy on 05/11/05
		 //if(formobj.costDesc[cnt].value != "" && formobj.cur[cnt].value != "" && (formobj.costNum[cnt].value =="" && formobj.costFrac[cnt].value=="" ) )
//Comment by Ganapathy on 05/18/05		 
		/* if(formobj.costDesc[cnt].value != ""  && (formobj.costNum[cnt].value =="" && formobj.costFrac[cnt].value=="" ) )
		 {
			alert("Please enter data in all mandatory fields");
			 formobj.costNum[cnt].focus();
			return false;
		 }
*/
		 
//Comment by Ganapathy on 05/11/05
//  if ((formobj.cur[cnt].value != "") && (formobj.costDesc[cnt].value == "")){
	   
  /*if (formobj.costDesc[cnt].value == ""){
			  if (!(validate_col('CostDesc',formobj.costDesc[cnt]))) return false;

			 if(formobj.cost[cnt].value=='' || formobj.cost[cnt].value==null || formobj.cost[cnt].value=='.'){

                alert("Please enter data in all mandatory fields")

                formobj.costNum[cnt].focus()

                return false;

			}

	     }*/



		  if (formobj.cost[cnt].value != "."){

 			  if (!(validate_col('Description',formobj.costDesc[cnt]))) return false;

			  //Comment by Ganapathy on 05/11/05
  			  //if (!(validate_col('Currency',formobj.cur[cnt]))) return false;

		  }



		  if (formobj.cost[cnt].value != "." && !(isDecimal(formobj.cost[cnt].value))){

	 		alert("<%=LC.L_Invalid_Cost%>");/*alert("Invalid Cost");*****/

			formobj.costNum[cnt].focus();

		 	 return false;

		 }

		/*  if (formobj.cur[cnt].value != ""){

 			  if (!(validate_col('Description',formobj.cur[cnt]))) return false;

		  }*/

		  

	     if ((formobj.costDesc[cnt].value != "") && (formobj.cost[cnt].value != "")) totRows =totRows + 1;  

	  }

	  formobj.totrows.value = 5;


	if (!(validate_col('e-Signature',formobj.eSign))) return false



	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

	   }




}





</SCRIPT>





<% String src;

src= request.getParameter("srcmenu");
//SV, commented 10/28/04, event name handling cal-enh-doc-req09b
String eventName= request.getParameter("eventName");

%>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
<jsp:include page="include.jsp" flush="true"/>
<%    
}
 else{

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<%}%>





<body>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV   id="div1"> 
		<br>
	<%	} 

else { %>

<DIV class="formDefault" id="div1"> 

<%}%>	

<%

	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   

	String eventId = request.getParameter("eventId");

	String costmode = request.getParameter("costmode");

	String mode = request.getParameter("mode");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");		
		String calassoc = request.getParameter("calassoc");	   
	

	String cost = "";

	String costDesc = "";

	String costId = "";

	String evName="";

	

	HttpSession tSession = request.getSession(true); 



	if (sessionmaint.isValidSession(tSession))

	{

	   String uName = (String) tSession.getValue("userName");

	   String sAcc = (String) tSession.getValue("accountId");
	   
	   String userId=(String)tSession.getAttribute("userId");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")  )
	{
	%>
	<!-- P class="sectionHeadings"> Event Library >> Event Cost >> Specify Cost </P-->
	<%
	}else{
		String calName = (String) tSession.getValue("protocolname");
	%>
	<P class="sectionHeadings"> <%Object[] arguments1 = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtCstSpec",arguments1)%><%--Protocol Calendar [ <%=calName%> ] >> Event Cost >> Event Cost Specify*****--%> </P>
	<%}%>

<jsp:include page="eventtabs.jsp" flush="true">

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="calassoc" value="<%=calassoc%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>



<form name="addcost" id="addcostfrm" METHOD=POST action="multicostsave.jsp?costmode=<%=costmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>" onsubmit="if (validate(document.addcost)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<INPUT NAME="calassoc" TYPE="hidden" value="<%=calassoc%>" SIZE=20/>


<%

//get dropdowns



	String cd1, cd2, cd3, cd4, cd5, sd1, sd2, sd3, sd4, sd5;

		

	SchCodeDao cd = new SchCodeDao();



    cd.getCodeValues("currency", EJBUtil.stringToNum(sAcc)); 



    cd1 = cd.toPullDown("cur");	

	String str1=cd1;

//	out.println(cd11);
//    cd2 = cd.toPullDown("cur2");

  //  cd3 = cd.toPullDown("cur3");

    //cd4 = cd.toPullDown("cur4");

//    cd5 = cd.toPullDown("cur5");



	//@Ankit: 14-Jul-2011, #FIN-CostType-1
    String studyId = "";
    String roleCodePk="";
	SchCodeDao cdesc = new SchCodeDao();

   if (calledFrom.equals("P") || calledFrom.equals("L")) {					

	   cdesc.getCodeValues("cost_desc", EJBUtil.stringToNum(sAcc)); 
   }
   if (calledFrom.equals("S")) { //Protocol called from Study
		
	    eventassocB.setEvent_id(StringUtil.stringToNum(protocolId));
		eventassocB.getEventAssocDetails();
		String eventType = eventassocB.getEvent_type();
		studyId =(eventType!=null && eventType.equalsIgnoreCase("P")) ? eventassocB.getChain_id() : "";
		
		if (! StringUtil.isEmpty(studyId) && StringUtil.stringToNum(studyId) > 0){
				roleCodePk = eventassocB.getStudyUserRole(studyId,userId);
		} 
		else{
			
		  roleCodePk ="";
		}
		cdesc.getCodeValuesForStudyRole("cost_desc",roleCodePk);	
   }
   sd1 = cdesc.toPullDown("costDesc");

%>

<input type=hidden name=totrows>

<input type=hidden name=mode value=<%=mode%>>

<input type=hidden name=eventId value=<%=eventId%>>



<TABLE width="90%" cellpadding="0" cellspacing="0" border="0" class="basetbl outline">

<th width=45% ><%=LC.L_Cost_Type%><%--Cost Type*****--%> <FONT class="Mandatory">* </FONT></th>

<th width=10%><%=LC.L_Unit%><%--Unit*****--%><!-- Comment by Ganapathy on 05/11/05 
 <FONT class="Mandatory">* </FONT>--></th>

<th width=35%><%=LC.L_Cost%><%--Cost*****--%> <FONT class="Mandatory">* </FONT></th>



<%

for (int i = 0; i< 5; i++) 

{ %>



    <tr>

    <td width=45%> 

    <%= sd1%>

    </td>

	<td width=10%><%= cd1%></td> 

    <td width=35%>

    <INPUT NAME="cost" TYPE=hidden SIZE=20>

	<INPUT NAME="costNum" TYPE="text" SIZE=9 MAXLENGTH=10>

	<B>.</B>

	<INPUT NAME="costFrac" TYPE="text" SIZE=2 MAXLENGTH=2> 

    </td></tr>

<%

}

%>	

	</table>

<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="addcost"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
	
</jsp:include>   



<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addcostfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>



</form>



<%



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}

else {

%>

</div>



<div class ="mainMenu" id = "emenu">



<jsp:include page="getmenu.jsp" flush="true"/>   



</div>

	<% }%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</body>

<%if ((request.getParameter("fromPage")).equals("selectEvent")){ %>	

<button onclick="window.history.back();"><%=LC.L_Back%></button> <%}%>

</html>





