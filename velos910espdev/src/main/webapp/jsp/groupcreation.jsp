<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=MC.M_Grp_CreationInEres%><%--Group Creation in eResearch*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<body>

<jsp:include page="panel.htm" flush="true"/>   

<DIV class="formDefault">

<Form method="post" action="updategroup.jsp">

<table width="400" cellspacing="0" cellpadding="0">
<tr><td>
<P class = "defComments">
<%=MC.M_Etr_TheGrpDets%><%--Please Enter the Group Details*****--%>
</P></td></tr>
</table>

<P class = "sectionHeadings">
<%=LC.L_Grp_Dets%><%--Group Details*****--%>
</P>

<table width="300" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="300">
       <%=LC.L_Group_Name%><%--Group Name*****--%> 
    </td>
    <td>
	<input type="text" name="groupName" size = 15 MAXLENGTH = 50>
    </td>
  </tr>
  <tr> 
    <td width="300">
       <%=LC.L_Group_Desc%><%--Group Description*****--%>
    </td>
    <td>
	<input type="text" name="groupDesc" size = 20 MAXLENGTH = 200>
    </td>
  </tr>
  <tr> 
    <td>
	<input type="hidden" name="groupId" MAXLENGTH = 15>
    </td>
  </tr>		
  <tr> 
    <td>
	<input type="hidden" name="groupAccId" MAXLENGTH = 15>
    </td>
  </tr>	
</table>
<br>

<table width="150" >
<tr>
<td align="right">
<input type="Submit" name="submit" value="<%=LC.L_Submit%><%--Submit--%>">
</td>
</tr>
</table>

</Form>
<div>
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>

</div>

</body>
</html>
