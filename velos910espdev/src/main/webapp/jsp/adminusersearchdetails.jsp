<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=LC.L_User_Search%><%--User Search*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="Javascript1.2">

	
function back(id,name,frmName) {

	if (document.layers) { 
	
	if(frmName.from.value == "adminauditreports") {
	//alert("Check");
	window.opener.document.div1.document.report.user.value = id;
	window.opener.document.div1.document.report.username.value = name;
	}
	}
	else
	{
	if(frmName.from.value == "adminauditreports") {
	window.opener.document.report.user.value = id;
	window.opener.document.report.username.value = name;
	} 
	}
	self.close();
}

</SCRIPT>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.LC"%>


<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<body style="overflow:scroll;">
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)){
   	String from=request.getParameter("from") ;
	String accId = "";
		accId = request.getParameter("accountId");
	if(accId == null){
		accId = (String) tSession.getValue("accountId");
	}
	

		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));	
	
	
	
%>
<BR>
<P class="defComments"><%=LC.L_Search_Criteria%><%--Search Criteria*****--%></P>
<br>
<Form  name="search1" method="post" action="adminusersearchdetails.jsp">
  <input type="hidden" name=from value=<%=from%>>
  <Input type="hidden" name="selectedTab" value="1">
  <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=20%> <%=LC.L_User_Search%><%--User Search*****--%></td>
      <td class=tdDefault width=30%> <%=LC.L_User_FirstName%><%--User First Name*****--%>: 
        <Input type=text name="fname">
      </td>
      <td class=tdDefault width=30%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: 
        <Input type=text name="lname">
      </td>
	  
	  
	  	 <td class=tdDefault width=25%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%> 
	  </td>
	  <td> &nbsp;&nbsp</td>
	  <td class=tdDefault width=25%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:
         <%dAccSites = cd2.toPullDown("accsites");%>
		 <%=dAccSites%> 
	  </td>
	  
	  
	  
	  
	  
	  
      <td class=tdDefault width=10%> 
     <!--   <Input type="submit" name="submit" value="Go"> -->
	 <button type="submit"><%=LC.L_Search%></button>
      </td>
    </tr>
  </table>
</Form>
<DIV > 
  <%
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	
	String jobTyp = "";
	String orgName ="";

	String ufname= request.getParameter("fname") ;
	String ulname=request.getParameter("lname") ;
	jobTyp=request.getParameter("jobType") ;
	orgName=request.getParameter("accsites") ;
	
	if ((ufname.equals("")) && (ulname.equals("")) && (jobTyp == null) && (orgName == null)){%>
  <P class="defComments"> <%=MC.M_PlsSrchCrit_ClkGo%><%--Please specify the Search criteria and then click on 
    'Search' to view the User List*****--%></P>
  <%}else{
		UserDao userDao = new UserDao();
		userDao.searchAccountUsers(EJBUtil.stringToNum(accId),ulname,jobTyp,orgName,ufname);
	//	out.print(EJBUtil.stringToNum(accId) +ulname +ufname);
	      ArrayList usrLastNames;
      	ArrayList usrFirstNames=null;
	      ArrayList usrMidNames=null;
      	ArrayList siteNames=null;
	      ArrayList jobTypes=null;
      	ArrayList usrIds=null;	
	      ArrayList grpNames=null;
		ArrayList  usrLogNames = null;
		  	
      	String usrLastName = null;
	      String grpName = null;
		String usrFirstName = null;
	      String usrMidName = null;
      	String siteName = null;
	      String jobType = null;
      	String usrId = null;
		String usrLogName = null; 
			
		int counter = 0;
		String oldGrp = null;
		String usrName= "";
		String orgNam = "";
%>
  <br>
  <Form name="accountBrowser" method="post">
    <table class=tableDefault width="100%" border=0>
      <tr> 
      <%
		usrLastNames = userDao.getUsrLastNames();
		usrFirstNames = userDao.getUsrFirstNames();
		usrIds = userDao.getUsrIds();
		siteNames = userDao.getUsrSiteNames();
		usrLogNames = userDao.getUsrLogNames(); 
		int i;
		int lenUsers = usrLastNames.size();
	//	out.print(lenUsers);
	%>
	  <% if (lenUsers > 0){ %>
        <th width=35% align =left> <%=LC.L_User_Name%><%--User Name*****--%> </th>
		<th width=35% align =left> <%=LC.L_User_LoginHypenId%><%--User Login-Id*****--%> </th>
	<%}else{%>
	        <th width=35% align =center><P class=defcomments> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></P></th>
	<%}%>
      </tr>

      <%

		for(i = 0 ; i < lenUsers ; i++)
	  	{
			usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
			usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
			usrId = ((Integer)usrIds.get(i)).toString();
			
			
			
			orgNam = (String)siteNames.get(i);
	
			if (orgNam == null){
				orgNam ="-";
			}	
			
			
			
			usrLogName = (String) usrLogNames.get(i);
			if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
        <%
			}else{
		  %>
      <tr class="browserOddRow"> 
        <% } usrName = usrFirstName + " " +usrLastName; 
		   %>
        <td width =35%> <A href = "#" onClick="back('<%=usrId%>','<%=usrName%>',document.forms[0]);"><%= usrFirstName%>&nbsp; 
          <%= usrLastName%></A> </td>
		  <td width=35%><%=usrLogName%></td>
      </tr>
      <%}%>
    </table>
  </Form>
  <%}//end of Parameter check
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
</div>
<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>
