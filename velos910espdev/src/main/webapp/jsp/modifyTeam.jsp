<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
 
<head>
<%
boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewStdPers%><%--Research Compliance >> New Application >> <%=LC.Std_Study%> Personnel*****--%></title>
<% } else {  %>
<title><%=LC.L_Std_TeamDets%><%--<%=LC.Std_Study%> >> Team Details*****--%></title>
<% }  %>




<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT Language="javascript">

function f_check_user(pright,mode,userType)
{
 if (f_check_perm(pright,mode) == false)
 	{
 		return false;
 	}
 
 //KM-to fix the Bug 2556(Not to allow deleted Non-System Study Team User to edit access rights.)
 //KM-Modified for Enh.#S17
 if(userType == 'N' || userType=='X')  //KM-Dont allow Non-System user to see access rights.
 {
   alert("<%=MC.M_NonSysUsr_NotAces%>");/*alert("This user is a Non-System user and therefore does not have any access to the system");****/
   return false;
 }

}

function  validate(formobj){

 // #5371 01/28/2011 @Ankit
 if (!(validate_col('e-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}

</SCRIPT>

<% String src;
String from = "team";
src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<body>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="BrowserTopn" id="div1"> 
<% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>  
  </jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"> 
  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

	String study = (String) tSession.getValue("studyId");

	String acc = (String) tSession.getValue("accountId");

	String rights = request.getParameter("right");

	String tab = request.getParameter("selectedTab");



      int pageRight = EJBUtil.stringToNum(rights);

    // GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

	// pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

%>
  <%-- IN THE ABOVE LINE OF CODE THE Id (MSITES) FOR SITES NEED TO REPLACED BY THAT OF TEAM--%>
  <%

 if (pageRight > 0 )

	{



	   int studyId = 0;   

	   studyId = EJBUtil.stringToNum(study);
	   

	   TeamDao teamDao = teamB.getTeamValues(studyId,EJBUtil.stringToNum(acc));


	   ArrayList teamIds 		= teamDao.getTeamIds(); 

	   ArrayList teamRoles 		= teamDao.getTeamRoleIds();

	   ArrayList teamUserLastNames 	= teamDao.getTeamUserLastNames();

	   ArrayList teamUserFirstNames = teamDao.getTeamUserFirstNames();

	   ArrayList teamRights 	= teamDao.getTeamRights();
	   
	   ArrayList teamUserTypes = teamDao.getTeamUserTypes();
	   
	   ArrayList userIds = teamDao.getUserIds();


	   String teamRole = null;

	   String teamUserLastName = null;

	   String teamUserFirstName = null;

	   String teamRight = null;

	   String teamUserType = null;
	   
	   String userType = null;
	   

	   int teamId = 0;
	   int userId = 0;



	   int len = teamIds.size();

	   int counter = 0;

	   CodeDao cdRole = new CodeDao();   

   	   cdRole.getCodeValues("role");


//	    JM: 28April05 
		 
				 

		String  site="";
		String group="";

		int siteId=0;
		int groupId = 0;

		String dAccSites ="";
		String dAccGroups = "";
		String dJobType= "";
		String userJobType="";
			

			
	
//		 JM: 28April05; get drop downs for site, group and job type		
		 CodeDao cd2 = new CodeDao();
		 cd2.getAccountSites(EJBUtil.stringToNum(acc));
		 String orgName=request.getParameter("accsites");
		 
		 
		 if (orgName==null) orgName="";
		 if (orgName.equals("")){
		 dAccSites=cd2.toPullDown("accsites");
		 }
		 else{
			 
			 dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(orgName),true);
			}

		 CodeDao cd3 = new CodeDao();
		 cd3.getAccountGroups(EJBUtil.stringToNum(acc));
	         String groupName=request.getParameter("accgroup");
		 if (groupName==null) groupName="";
		 if (groupName.equals("")){
		 dAccGroups = cd3.toPullDown("accgroup");
		 }
		 else{
		 dAccGroups = cd3.toPullDown("accgroup",EJBUtil.stringToNum(groupName),true);
		 }

		
		CodeDao cd = new CodeDao();
		cd.getCodeValues("job_type");
		
		String jobTyp=request.getParameter("jobType"); 
		if (jobTyp==null) jobTyp="";
		if (jobTyp.equals(""))			
		   dJobType=cd.toPullDown("jobType");
		 
		else
		dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobTyp),true);


%>
 
<!--  <Form  name="search" method="post" action="assignToTeam.jsp" onsubmit="return f_check_perm(<%=pageRight%>,'N');">-->
	<Form  name="search" method="post" action="assignToTeam.jsp">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <Input type="hidden" name="right" value="<%=rights%>">


 <table class="tableDefault" width="98%" border=0 cellspacing="0" cellpadding="0">
	<tr height="20">
	<td colspan="6" bgcolor="#dcdcdc"> <b> <%=MC.M_AddUser_TeamSrch%><%--To add a new User to the Team, Search By*****--%></b></td>
	</tr>
	<tr>
	<td width="10%" bgcolor="#dcdcdc"> <%=LC.L_First_Name%><%--First Name*****--%>:</td>
	 <td width="20%" bgcolor="#dcdcdc"> <Input type=text name="fname">  </td>
     <td width="10%" bgcolor="#dcdcdc" align="right"> <%=LC.L_Last_Name%><%--Last Name*****--%>:&nbsp; </td>
     <td width="20%" bgcolor="#dcdcdc"> <Input type=text name="lname"> </td>
	<td width="10%" bgcolor="#dcdcdc" align="right"> <%=LC.L_Organization%><%--Organization*****--%>: &nbsp; </td>
      <td width="28%" bgcolor="#dcdcdc">  <%=dAccSites%>  </td>
     </tr>
	<tr>
	<td  bgcolor="#dcdcdc"> <%=LC.L_Group%><%--Group*****--%>:</td>
	 <td  bgcolor="#dcdcdc">  <%=dAccGroups%>    </td>
	<td bgcolor="#dcdcdc" align="right"> <%=LC.L_Job_Type%><%--Job Type*****--%>:&nbsp; </td>
     <td  bgcolor="#dcdcdc">   <%=dJobType%>  </td>
  	<td colspan="2" align="center" bgcolor="#dcdcdc"> 
			<% if (pageRight == 5 || pageRight == 7)
				{
				%>
	          <!--<Input type="submit" name="submit" value="Go">-->
			   <button type="submit"><%=LC.L_Search%></button>
		  <%
		  	}
		  %>
        </td>
      </tr>
    </table>
    <Input type=hidden name="srcmenu" value="<%=src%>">
  </Form>
  
  
<!--  <Form name="team" method="post" action="updateTeam.jsp" onsubmit="return f_check_perm(<%=pageRight%>,'E');">-->
<!-- #5371 01/28/2011 @Ankit -->
<Form name="team" id="teamFrmId" method="post" action="updateTeam.jsp" onsubmit="if (validate(document.team)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <table width="98%" cellspacing="0" cellpadding="0" border=0 >
      <tr > 
        <td width = "100%"> 
          <P class = "defcomments">
          <% String Hyper_Link = "<A href=\"userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2\">"+LC.L_Add_NewUser+"</a>";
          Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_UnableToFindUsr_AddUsr",arguments) %>
           <%--If you are unable to find a user in the existing user list, you may <A href="userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2">Add New User </a> here*****--%></P> 
           <br><br>
            <p class = "sectionheadings"><%=MC.M_Modify_StdTeamDets%><%--Modify <%=LC.Std_Study%> Team Details*****--%> </P>
        </td>
      </tr>
      <tr> 
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>
    <table width="98%" >
      <tr> 
        <th width="40%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>
        <th width="30%"> <%=LC.L_Role%><%--Role*****--%> </th>
        <th width="30%"> <%=LC.L_Access_Rights%><%--Access Rights*****--%> </th>
      </tr>
      <%
	String selName = null;

	
    for(counter = 0;counter<len;counter++)

	{	teamId=EJBUtil.stringToNum(((teamIds.get(counter)) == null)?"-":(teamIds.get(counter)).toString());



		teamRole=((teamRoles.get(counter)) == null)?"-":(teamRoles.get(counter)).toString();

		teamUserLastName=((teamUserLastNames.get(counter)) == null)?"-":(teamUserLastNames.get(counter)).toString();

		teamUserFirstName=((teamUserFirstNames.get(counter)) == null)?"-":(teamUserFirstNames.get(counter)).toString();

		teamRight=((teamRights.get(counter)) ==null)?"-":(teamRights.get(counter)).toString();

		selName =  teamUserFirstName+" "+teamUserLastName; 
		
		userId = ((Integer)userIds.get(counter)).intValue(); 
			
		userB.setUserId(userId);
    	userB.getUserDetails();
		userType = userB.getUserType();

		teamUserType = (String) teamUserTypes.get(counter);

		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

	%>
        <input type="hidden" name="teamId" Value="<%=teamId%>">
        <td> <%= teamUserFirstName%> <%= teamUserLastName%></td>
        <td> 
          <%String role = 	cdRole.toPullDown("role",EJBUtil.stringToNum(teamRole));%>
          <%=role%> </td>
	    <!-- Bug#9933 28-May-2012 -Sudhir-->     
        <td width="30%"> <A href="studyrights.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return f_check_user(<%=pageRight%>,'E','<%=userType%>');">
        	<img border="0" src="./images/AccessRights.gif" title="<%=LC.L_Access_Rights%><%--Access Rights*****--%>"></A> </td>
      </tr>
      <%

	 }



%>
      <input type="hidden" name="src" Value="<%=src%>">
      <input type="hidden" name="totrows" Value="<%=len%>">
	  </table>
	  	<% if (pageRight == 6 || pageRight == 7)
				{
				%>
        
	  	<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="teamFrmId"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
  <% }%>
	  <table>
      <tr> 
        <td align="right"> 
			<% if (pageRight == 6 || pageRight == 7)
				{
				%>
          <!--<Input type="submit" value="Submit" name="submit">-->
		  <!--<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" onClick = "return validate()" border="0">-->
		  <% }%>
        </td>
      </tr>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>



