<%-- propagateEventOptions.jsp --%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<SCRIPT>
	
//SV, 9/16/04, added to set the propagate flags to be used while saving event information. part of cal-enh-05
   function setPropagate(chkBox, flagObj) 
	{
		flagObj.value = "N";

		if (chkBox.checked) {
			flagObj.value = "Y";
		}
		else {
			flagObj.value = "N";
		}
	 	return flagObj.value;  
	}
   function setPropagate(formobj) 
	{
		if (formobj.chkPropagate[0].checked) {
			formobj.propagateInVisitFlag.value = "Y";
		}
		else {
			formobj.propagateInVisitFlag.value = "N";
		}
	   
		if (formobj.chkPropagate[1].checked) {
			formobj.propagateInEventFlag.value = "Y";
		}
		else {
			formobj.propagateInEventFlag.value = "N";
		}
		
/*DEBUG		alert(formobj.propagateInVisitFlag.value);
		alert(formobj.propagateInEventFlag.value);
*/	
	}

</SCRIPT>

<%  
String fromPage= request.getParameter("fromPage");
String formName = request.getParameter("formName");
String eventName = request.getParameter("eventName");
eventName=(eventName==null)?"":eventName;
eventName=(StringUtil.decodeString(eventName));
  HttpSession tSession = request.getSession(true); 

  if (sessionmaint.isValidSession(tSession))

  {

	 boolean propagateInVisit = false;
	 boolean propagateInEvent = false;
	%>

<BR>
<%-- SV, 9/16/04, Show the checkboxes only if we are coming from the event customization page. When coming from
event browser page for the new enhancement make sure the progagate flags are  set to "Y" and 
set these flags to "N" if coming from any other screen. The hidden fields are added for the purpose of save functionality--%>
<%if (fromPage.equals("fetchProt")) { %>
<table>

	<tr><td class=tdDefault> <Input type="checkbox" name="chkPropagate" Value="Visit" onClick="return(setPropagate(document.<%=formName%>))"> <%=MC.M_ApplyToAll_EvtsInVisit%><%--Apply to all Events in this Visit*****--%></Input></td></tr>
	<tr><td class=tdDefault> <Input type="checkbox" name="chkPropagate" Value="Event" onClick="return(setPropagate(document.<%=formName%>))"> <%=LC.L_Apply_All%><%--Apply to all*****--%> <B> <%=eventName%> </B> <%=MC.M_Evt_InThisCal%><%--Events in this Calendar*****--%></Input></td></tr>
	<tr><td> <Input type="hidden" name="propagateInVisitFlag" ></Input></td></tr>
	<tr><td> <Input type="hidden" name="propagateInEventFlag" ></Input></td></tr>
</table>
<BR>

<%}
/* changed the visit_fag to N as it doesn't make sense on the second screen, where we deal with records with displacement = 0 */
else if (fromPage.equals("eventbrowser")) { %>
	<tr><td> <Input type="hidden" name="propagateInVisitFlag" Value="N"></Input></td></tr>
	<tr><td> <Input type="hidden" name="propagateInEventFlag" Value="Y"></Input></td></tr>
<%}
else { // for all other cases 
%>
	<tr><td> <Input type="hidden" name="propagateInVisitFlag" Value="N"></Input></td></tr>
	<tr><td> <Input type="hidden" name="propagateInEventFlag" Value="N"></Input></td></tr>
<%}
  
 }
  	  
%>
