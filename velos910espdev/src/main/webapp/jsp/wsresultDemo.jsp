<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>

<html>


<body>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*, com.velos.service.*, com.velos.remoteservice.*, com.velos.remoteservice.demographics.*, com.velos.eres.business.common.CodeDao, com.velos.eres.business.common.CommonDAO" %>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="commonB" scope="page"
	class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="accB" scope="request"
	class="com.velos.eres.web.account.AccountJB" />
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		String mode = request.getParameter("mode");
		if (mode == null) mode = "";

		/*
		We support two modes of search:
			"list" is a name-based search that returns minimal
			data for a list of patients.

			"id" is an MRN-based search that returns
			detailed information about a single patient.
		*/
		if (mode.equalsIgnoreCase("list")){
			String lastName = request.getParameter("lastName");
			String firstName = request.getParameter("firstName");
			int genderPK = EJBUtil.stringToNum(request.getParameter("gender"));
			try{
				//Gender comes to us in the dropdown as the primary in er_codelst.
				//de-reference it here in the CodeDao
				CodeDao cd1 = new CodeDao();
				cd1.getCodeValues("gender");
				String gender = cd1.getCodeDesc(genderPK);

				//find the remote service
				IDemographicsService demographicsService =
					DemographicsServiceFactory.getService();

				if (demographicsService == null){
					throw new RuntimeException("Could not find remote demographics service.");
				}

				List patients = null;
				//invoke the remote service

				patients =
					demographicsService.getPatientDemographics(
							lastName,
							firstName,
							gender);

				if (patients.size() == 0){
					%>No <%=LC.Pat_Patients%> found.<%
				}
				else{
					%>
					<table>
						<tr>
							<th><%=LC.Pat_Patient%> ID</th>
							<th>First</th>
							<th>Last</th>
							<th>DOB</th>

						</tr>
					<%
					for (int patCnt = 0; patCnt < patients.size(); patCnt++){
						IPatientDemographics patient = (IPatientDemographics)patients.get(patCnt);

						String browserClass = "browserOddRow";
						if ((patCnt%2)==0){
							browserClass = "browserEvenRow";
						}
						%>
						<tr class="<%=browserClass%>">
							<td><a href="javascript:void(0);" onClick=" return selectPatFromList('<%=clean(patient.getMRN())%>');"><%=clean(patient.getMRN())%></a></td>
							<td><%=clean(patient.getFirstName())%></td>
							<td><%=clean(patient.getLastName())%></td>
							<td><%=clean(DateUtil.dateToString(patient.getDOB())) %></td>


						</tr>
						<%
					}
					%>
					</table>
					<%
				}
			}
			catch(Throwable t){
				t.printStackTrace();
				%>Oops. An error has occurred talking to the <%=LC.Pat_Patient_Lower%> search service. Please contact your system administrator.<%

			}
		}

		//return detailed information about an individual patient
		else{

			String patientId=  request.getParameter("MRN");
			String responseCode = null;
			String responseMessage = null;
			String givenName = null;
			String middleName = null;
			String lastName = null;
			String DOB = null;
			String ethnicity = null;
			String ethnicityPK = null;
			String race = null;
			String racePK = null;
			String survivalStatus = null;
			String maritalStatusPK = null;
			String maritalStatus = null;
			String bloodGroup = null;
			String bloodGroupPK = null;
			String ssn = null;
			String address1 = null;
			String address2 = null;
			String city = null;
			String county = null;
			String state = null;
			String zip = null;
			String country = null;
			String homePhones = null;
			String workPhones = null;
			String email = null;
			String primaryFacility = null;
			String genderPK = null;
			String sex = null;


			//specify cause added for NYU...generally applicable, but for NYU will actually transplant organ
			String causeOfDeathOther = null;
			String causeOfDeath = null;
			String showMessage = "";

			try{


			  IDemographicsService demographicsService = DemographicsServiceFactory.getService();
			  HashMap requestParams = new HashMap();
			  requestParams.put("patId", patientId);
			  List patients = null;
			  try{
				patients = demographicsService.getPatientDemographics(requestParams);
			  }
			  catch(PatientNotFoundException e){
				  showMessage = LC.Pat_Patient+ " not found for id: " + patientId;
			  }
			  if (patients == null  || patients.size() == 0){
				  showMessage = LC.Pat_Patient+ " not found for id: " + patientId;
			  }
			  else{
				IPatientDemographics patient = (IPatientDemographics)patients.get(0);
				String acc = (String) tSession.getValue("accountId");

				accB.setAccId(EJBUtil.stringToNum(acc));
				accB.getAccountDetails();
				String accCode = "";
				accCode = accB.getAccName();
				accCode = (accCode == null) ? "" : accCode;
				String types = "\'race\',\'gender\',\'ethnicity\',\'marital_st\',\'bloodgr\'";
				CommonDAO commonDao = commonB.getCommonHandle();

				commonDao.getMappingData(types, accCode);
				ArrayList mappingType = commonDao.getMapping_type();
				ArrayList mappingSubTyp = commonDao.getMapping_subTypList();
				ArrayList mappingLMapList = commonDao.getMapping_lMapList();
				ArrayList mappingRMapList = commonDao.getMapping_rMapList();

				sex = patient.getGender();

				genderPK = getCodePK(
						"gender",
						sex,
						mappingLMapList,
						mappingRMapList,
						mappingType);


				maritalStatus = patient.getMaritalStatus();
				maritalStatusPK = getCodePK(
						"marital_st",
						maritalStatus,
						mappingLMapList,
						mappingRMapList,
						mappingType);



				DOB = DateUtil.dateToString(patient.getDOB());
				if (DOB == null) DOB = "";

				givenName = clean(patient.getFirstName());
				middleName = clean(patient.getMiddleName());
				lastName = clean(patient.getLastName());
				sex = clean(patient.getGender());

				ethnicity = clean(patient.getEthnicity());
				ethnicityPK = getCodePK(
						"ethnicity",
						ethnicity,
						mappingLMapList,
						mappingRMapList,
						mappingType);

				race = clean(patient.getRace());
				racePK = getCodePK(
						"race",
						race,
						mappingLMapList,
						mappingRMapList,
						mappingType);

				survivalStatus = clean(patient.getSurvivalStatus());
				causeOfDeath = clean(patient.getCauseOfDeath());
				causeOfDeathOther = clean(patient.getCauseOfDeathOther());

				maritalStatus = clean(patient.getMaritalStatus());
				bloodGroup = clean(patient.getBloodGroup());
				bloodGroupPK = getCodePK(
						"bloodgr",
						bloodGroup,
						mappingLMapList,
						mappingRMapList,
						mappingType);

				ssn = clean(patient.getSSN());
				address1 = clean(patient.getAddress1());
				address2 = clean(patient.getAddress2());
				city = clean(patient.getCity());
				county = clean(patient.getCounty());
				state = clean(patient.getState());
				zip = clean(patient.getZip());
				country = clean(patient.getCountry());
				homePhones = clean(patient.getHomePhones());
				workPhones = clean(patient.getWorkPhones());
				email = clean(patient.getEmail());

				primaryFacility = clean(patient.getPrimaryFacility());

			  }

			}
			catch(Throwable t){
				showMessage = "Oops. An error has occurred talking to the "+LC.Pat_Patient_Lower+" search service. Please contact your system administrator.";
				t.printStackTrace();
			}
%>
		<input type="hidden" id="wsPatId" value="<%=patientId%>"/>
		<input type="hidden" id="wsPatFName" value="<%=givenName%>"/>
		<input type="hidden" id="wsPatMName" value="<%=middleName%>"/>
		<input type="hidden" id="wsPatLName" value="<%=lastName%>"/>
		<input type="hidden" id="wsPatDOB" value="<%=DOB%>"/>
		<input type="hidden" id="wsPatEthnicity" value="<%=ethnicity%>"/>
		<input type="hidden" id="wsPatRace" value="<%=race%>"/>
		<input type="hidden" id="wsPatSurvivalStatus" value="<%=survivalStatus%>"/>
		<input type="hidden" id="wsPatMaritalStatus" value="<%=maritalStatusPK%>" name="<%=maritalStatus%>"/>
		<input type="hidden" id="wsPatBloodGroup" value="<%=bloodGroup%>"/>
		<input type="hidden" id="wsPatSSN" value="<%=ssn%>"/>
		<input type="hidden" id="wsPatEmail" value="<%=email%>"/>
		<input type="hidden" id="wsPatAddress1" value="<%=address1%>"/>
		<input type="hidden" id="wsPatAddress2" value="<%=address2%>"/>
		<input type="hidden" id="wsPatCity" value="<%=city%>"/>
		<input type="hidden" id="wsPatState" value="<%=state%>"/>
		<input type="hidden" id="wsPatCounty" value="<%=county%>"/>
		<input type="hidden" id="wsPatZip" value="<%=zip%>"/>
		<input type="hidden" id="wsPatCountry" value="<%=country%>"/>
		<input type="hidden" id="wsPatHomePhones" value="<%=homePhones%>"/>
		<input type="hidden" id="wsPatWorkPhones" value="<%=workPhones%>"/>
		<input type="hidden" id="wsPatPrimaryFacility" value="<%=primaryFacility%>"/>
		<input type="hidden" id="wsPatSex" value="<%=genderPK%>" name="<%=sex%>"/>
		<input type="hidden" id="wsPatCauseOfDeath" value="<%=causeOfDeath%>"/>
		<input type="hidden" id="wsPatCauseOfDeathOther" value="<%=causeOfDeathOther%>"/>

		<div>

			<% if (showMessage.length()>0) { %>
				<table>
					<tr>
					<td colspan="3"><%=showMessage%></td>
					</tr>
				</table>

			<%}
			else if (patientId.length()>0){ %>
			<table>
			<tr  class="browserOddRow">
			<td><%=LC.Pat_Patient%> Id:</td>
				<td><%=patientId%></td>
			</tr>
			<tr class="browserEvenRow">
				<td>First Name:</td>
				<td><%=givenName%></td>
			</tr>
			<tr class="browserOddRow">
				<td>Last Name:</td>
				<td><%=lastName%></td>
			</tr>
			<tr class="browserEvenRow">
				<td>Date Of Birth:</td>
				<td><%=DOB%></td>
			</tr>
			<tr class="browserOddRow">
				<td>Gender:</td>
				<td><%=sex%></td>
			</tr>
			<tr class="browserEvenRow">
				<td>Race:</td>
				<td><%=race%></td>
			</tr>

			<tr class="browserEvenRow">
				<td>Ethnicity:</td>
				<td><%=ethnicity%></td>
			</tr>

			<tr class="browserOddRow">
				<td>Survival Status:</td>
				<td><%=survivalStatus%></td>
			</tr>
			<tr class="browserEvenRow">
				<td>Cause of Death:</td>
				<td><%=causeOfDeath%></td>
			</tr>
			<tr class="browserOddRow">
				<td>Other Cause of Death:</td>
				<td><%=causeOfDeathOther%></td>
			</tr>

			</table>

			<%} %>


		</div>
<%
		}
	}


%>

</body>


</html>

<%!
public String clean(String input){
	if (input == null){
		return "";
	}
	return input;
}

public String getCodePK(
		String codeType,
		String remoteCode,
		ArrayList keyList,
		ArrayList remoteCodeList,
		ArrayList typeList){

	for (int x = 0; x < keyList.size(); x++){
		String type = (String)typeList.get(x);
		if (type.equalsIgnoreCase(codeType)){
			String testRemoteCode = (String)remoteCodeList.get(x);
			if (remoteCode.equalsIgnoreCase(testRemoteCode)){
				return (String)keyList.get(x);
			}
		}
	}
	return null;
}

%>