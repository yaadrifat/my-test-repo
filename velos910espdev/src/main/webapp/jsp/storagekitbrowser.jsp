<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=MC.M_MngInv_StrgKitSrch%><%--Manage Inventory >> Storage Kit Search*****--%></title>
</head>
<SCRIPT language="javascript">
// Added By Amarnadh for Study search Nov 16th 07
function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=storage&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="storagekitbrowser.jsp";
	void(0);
}


function setFilterText(form) {

       form.storageId.value=form.storageID.value;
       form.storageName.value = form.storageName.value;
}

function setOrder(formObj,orderBy,pgRight) { //orderBy column number

    var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;

	formObj.action="storagekitbrowser.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	setFilterText(formObj)
	formObj.submit();
}
function openwindow(formobj,pgright){

	if (! f_check_perm(pgright,'N') )
	{
		return false;
	}
     windowName =window.open('addmultiplestorage.jsp?srcmenu=tdmenubaritem6&selectedTab=1' , 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1100,height=500');
     windowName.focus();

}


function checkAll(formobj){
    act="check";
 	totcount=formobj.totcount.value;
    if (formobj.All.value=="checked")
		act="uncheck" ;
    if (totcount==1){
       if (act=="uncheck")
		   formobj.Del.checked =false ;
       else
		   formobj.Del.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck")
				 formobj.Del[i].checked=false;
             else
				 formobj.Del[i].checked=true;
         }
    }


    if (act=="uncheck")
		formobj.All.value="unchecked";
    else
		formobj.All.value="checked";
}



	 function selectStorages(formobj,selVal,pgright, from){


	 if (! f_check_perm(pgright,'E') )
	{
		return false;
	}

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu = formobj.srcmenu.value;

	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelStorKits_ToDel%>");/*alert("Please select Storage Kit(s) to be Deleted");*****/

	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			    msg="<%=MC.M_ActWillDel_StrgKitCont%>"/*"This action will also delete all the child storage kits linked with the selected storage kit(s). Are you sure you want to continue?"*****/;

			 if (confirm(msg))
			{
			   cnt++;


			     // window.open("deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value,"_self");
			     formobj.action="deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value+"&fromPg="+from,"_self";
			     formobj.submit();

		    }
		    else
				{
					return false;
				}
		 }

	 }else{
	  for(i=0;i<totcount;i++){

	  if (formobj.Del[i].checked){
		  selStrs[j] = formobj.Del[i].value;
		  j++;
		  cnt++;
			}
		}
		if(cnt>0){

			msg="<%=MC.M_ActWillDel_StrgKitCont%>"/*"This action will also delete all the child storage kits linked with the selected storage kit(s). Are you sure you want to continue?"*****/;

		if(confirm(msg)) {

			formobj.action="deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+selStrs+"&fromPg="+from,"_self";
			formobj.submit();

	    }
			  else
				{
				  return false;
				}
		}

	  }
	  if(cnt==0)
		 {


		  alert("<%=MC.M_SelStorKits_ToDel%>");/*alert("Please select Storage Kit(s) to be Deleted");*****/
		   	   return false;
		 }

	  formobj.selStrs.value=selStrs;

 }



</SCRIPT>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="tdmenubaritem6"/>
</jsp:include>
<body>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/> <!--Amarnadh -->


<%
 String selectedTab = request.getParameter("selectedTab");

%>

<DIV class="tabDefTopN" id="div1">
<jsp:include page="inventorytabs.jsp" flush="true">
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
</jsp:include>
</DIV>
<%
    HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

	 if (pageRight > 0 )

	{

		String accId = (String) tSession.getValue("accountId");
		String src               = request.getParameter("srcmenu");
		String storageId         = request.getParameter("storageId");
		String storageStat       = request.getParameter("storageStatus");

		String storageNme        = request.getParameter("storageName");
		String specimenTyp       = request.getParameter("specimenType");

	    String dstudytxt		 =  "" ;

		//String storageLocation   = request.getParameter("storageLoc");
		//String selectedTab       = request.getParameter("selectedTab") ;
		String searchFrom        = request.getParameter("searchFrom");
		String orderBy           = request.getParameter("orderBy");
		String orderType         = request.getParameter("orderType");
		String pagenum 	         = request.getParameter("page");

		String dspecimenType     = "";
		String dstorageStatus    = "";
		String where             = "where ";
		String strCoordinateX = "";
		String strCoordinateY = "";
		String mainFKStorage = "";


		strCoordinateX = request.getParameter("strCoordinateX");

		strCoordinateY = request.getParameter("strCoordinateY");
		mainFKStorage= request.getParameter("mainFKStorage");

		if (StringUtil.isEmpty(mainFKStorage))
		{
			mainFKStorage = "";

		}

		if (StringUtil.isEmpty(strCoordinateX))
		{
			strCoordinateX = "";

		}

		if (StringUtil.isEmpty(strCoordinateY))
		{
			strCoordinateY = "";

		}


		String study =  request.getParameter("dStudy");
	    if (( request.getParameter("dstudytxt"))!= null &&  (request.getParameter("dstudytxt").length() > 0))dstudytxt= request.getParameter("dstudytxt");

		CodeDao cd = new CodeDao();

		if(storageId == null )
	    	   storageId = "";

		if(study == null)
			study = "";

		if(storageNme == null )
	  	   storageNme = "";


		cd.getCodeValues("store_type");


		if(specimenTyp ==null)
		   specimenTyp="";




		  cd = new CodeDao();
		  cd.getCodeValues("specimen_type");



		if (specimenTyp.equals(""))
	 	    dspecimenType=cd.toPullDown("specimenType");
		else
	       dspecimenType=cd.toPullDown("specimenType",EJBUtil.stringToNum(specimenTyp),true);


		cd = new CodeDao();
		cd.getCodeValues("storage_stat");


		if (storageStat==null)
		    storageStat="";

		if (storageStat.equals(""))
	 	    dstorageStatus=cd.toPullDown("storageStatus");

		else
	        dstorageStatus=cd.toPullDown("storageStatus",EJBUtil.stringToNum(storageStat),true);


	    // Added By Amarnadh for Study search Nov 16th 07
		 String studyId = request.getParameter("selStudyIds");
			if (studyId==null) studyId="";

		 String studyNumber = request.getParameter("selStudy");
			if (studyNumber==null) studyNumber="";

		String tableName = "ER_STORAGE a,ER_STORAGE_STATUS b " ;
		       if(specimenTyp  != "") {
		            tableName =  tableName + ", ER_ALLOWED_ITEMS ";
		        }


		// Amarnadh Nov 16th
		String studyIdStr = "";
		studyIdStr = request.getParameter("selStudyIds");
		studyIdStr=(studyIdStr==null)?"":studyIdStr;

		//IH-Latest status fetched.
		StringBuffer sqlBuffer   = new StringBuffer();

		    sqlBuffer.append("SELECT distinct STORAGE_ID,(select codelst_desc from er_codelst where pk_codelst = a.fk_codelst_storage_type)storage_type,STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,	 ");
		    sqlBuffer.append(" nvl((select codelst_desc from er_codelst where pk_codelst = ");
		    sqlBuffer.append("   nvl((select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and ");
		    sqlBuffer.append("   pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and ");
		    sqlBuffer.append("   ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE))),0) ) ");
		    sqlBuffer.append(" ,' ') as STORAGE_STATUS, ");
		    sqlBuffer.append(" STORAGE_NAME,STORAGE_CAP_NUMBER, (select codelst_desc from er_codelst where pk_codelst = fk_codelst_capacity_units) as capacity_units,PK_STORAGE, ");
		    sqlBuffer.append(" nvl((SELECT codelst_desc FROM er_codelst WHERE pk_codelst = storage_kit_category ),' ')  as kit_category, ");
		    sqlBuffer.append(" STORAGE_KIT_CATEGORY, (select c.storage_name ");
		    sqlBuffer.append(" from er_storage c  where c.pk_storage = a.fk_storage) parent_storage FROM ").append(tableName).append(" ");

		StringBuffer whereBuffer = new StringBuffer(where);

		// Modified By Amarnadh for Bugzilla issue #3199
	    boolean whereFlag = true;//KM-Account Id added for new requirement.
	            whereBuffer.append(" a.pk_storage = b.fk_storage AND a.FK_CODELST_STORAGE_TYPE = (select pk_codelst from er_codelst where codelst_type='store_type' and codelst_subtyp='Kit') and a.fk_storage is null");

	 	 if(storageId != "" ){
	  	   whereBuffer.append(" AND LOWER(STORAGE_ID) LIKE LOWER(");
		   whereBuffer.append("'%"+storageId.trim()+"%')");
	           whereFlag = true;
	     }



	     if( EJBUtil.stringToNum(mainFKStorage) > 0 ){
	  	   whereBuffer.append(" AND a.fk_storage = "+mainFKStorage );
		         whereFlag = true;
	     }




	     //Modified By  Amarnadh for Bugzilla issue #3199
 	     if(studyIdStr != "" ){
	            whereBuffer.append(" AND FK_STUDY = "+studyIdStr);
	     }
	     else{
	            whereBuffer.append(" and FK_ACCOUNT = '").append(accId).append("'");
	     }

		 if(storageStat != "" ){
	          if(whereFlag){
	            whereBuffer.append(" AND b.FK_CODELST_STORAGE_STATUS = ")
		                    .append("'"+storageStat+"'");

	       }
	        else
	         whereBuffer.append(" b.FK_CODELST_STORAGE_STATUS = ")
		                .append("'"+storageStat+"'");
		       whereFlag = true;
	       }
	      if(storageNme != "" ){
	       if(whereFlag){
	        whereBuffer.append(" AND  LOWER(STORAGE_NAME) LIKE LOWER(")
		               .append("'%"+storageNme.trim()+"%')");

	      }
	      else
	       whereBuffer.append("  LOWER(STORAGE_NAME) LIKE LOWER(")
		              .append("'%"+storageNme.trim()+"%')");
		    whereFlag = true;
	      }


	    if(specimenTyp  != ""){
	       if(whereFlag){
	          whereBuffer.append(" AND FK_CODELST_SPECIMEN_TYPE  = " )
		      .append(specimenTyp)
			  .append(" AND a.PK_STORAGE = ER_ALLOWED_ITEMS.FK_STORAGE");

	      }
	       else{
	          whereBuffer.append(" FK_CODELST_SPECIMEN_TYPE  = ")
		      .append(specimenTyp)
			  .append(" AND a.PK_STORAGE = ER_ALLOWED_ITEMS.FK_STORAGE");

	        }
	        whereFlag = true;

	    }


		if(whereFlag)
	        sqlBuffer.append(whereBuffer);

	     String sql = sqlBuffer.toString();


		long rowsPerPage    = 0;
		long totalPages     = 0;
		long rowsReturned   = 0;
		long showPages      = 0;
		long totalRows      = 0;
		long firstRec       = 0;
		long lastRec        = 0;
		boolean hasMore     = false;
		boolean hasPrevious = false;


		String storageTypTxtDesc = "";
		String countsql = " Select count(*) from ( " + sql + " )";

		   if (EJBUtil.isEmpty(orderBy))
			orderBy = "lower(STORAGE_ID)";

		   if (orderType == null)
		        orderType = "asc";


		   if (orderBy.equals("LOWER(CAPACITY_UNITS)"))
			   orderType = orderType + ", storage_cap_number " +orderType;

		   rowsPerPage =   Configuration.ROWSPERBROWSERPAGE ;
		   totalPages  =   Configuration.PAGEPERBROWSER ;
		   int curPage = 0;
		   long startPage = 1;
		   long cntr = 0;
		   //if (pagenum == null)
		   //    pagenum = "1";

		  curPage        = EJBUtil.stringToNum(pagenum);
//JM: 23Jul2009: issue no-4118(Comment #5)
		  curPage = (curPage==0)?1:curPage;

		  BrowserRows br = new BrowserRows();

		  if (pagenum != null) {
	  	      br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
		  }


		  rowsReturned   = br.getRowReturned();
		  showPages      = br.getShowPages();
		  startPage      = br.getStartPage();
		  hasMore        = br.getHasMore();
		  hasPrevious    = br.getHasPrevious();
		  totalRows      = br.getTotalRows();
		  firstRec       = br.getFirstRec();
		  lastRec        = br.getLastRec();

		  if(orderType.indexOf(",")!= -1) {
			  int val = orderType.indexOf(",");
			  orderType = orderType.substring(0,val);
		  }

	%>

	<form name="storage" method="post" action="storagekitbrowser.jsp?page=1" onSubmit="return    setFilterText(document.storage)">
	<input type="hidden" name="searchFrom" Value="search">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" Value="<%=orderBy%>">
	<Input type="hidden" name="orderType" Value="<%=orderType%>">
	<input type="hidden" name="storageId">
	<input type="hidden" name="dstudytxt">

	<input type="hidden" name="specimenTyp">
	<input type="hidden" name="storageStat">
	<input type="hidden" name="srcmenu" Value="<%=src%>">
	<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
	<input type="hidden" name="totcount" Value="<%=rowsReturned%>">
	<input type="hidden" name="selStrs" >

	<DIV class="tabFormTopN"  id="div2">

	<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr class="tmpHeight plainBG"></tr>
	<tr >
		<td><%=LC.L_Storage_Id%><%--Storage ID*****--%>:</td><td><Input type = "text" name="storageID"  value = "<%=storageId%>" size=15 align = right>
		</td>
		<td></td>


		<!--
		<td>Location:</td><td><Input readonly type = "text" name="storageLoc" value =  "<%--=storageLocation--%>" size=15 align = right>

			<Input type = "hidden" name="mainFKStorage" value = "<%=mainFKStorage%>" size=25 MAXLENGTH = 250 align = right>

			<Input type = "hidden" name="strCoordinateX" value = "<%=strCoordinateX%>" size=25 MAXLENGTH = 250 align = right>
			<Input type = "hidden" name="strCoordinateY" value = "<%=strCoordinateY%>" size=25 MAXLENGTH = 250 align = right>

			<A  href="javascript:void(0);" onClick="return openLocLookup(document.storage)" >Select</A>&nbsp;
			<A href="#" onClick="setValue(document.storage)"> Remove</A>

		</td>
		-->
	

	
		<td><%=LC.L_Name%><%--Name*****--%>:</td><td><Input type = "text" name="storageName" value = "<%=storageNme%>" size=15>
		</td>

	    	 <td colspan="2" align="left">

		   	&nbsp;<button type="submit"><%=LC.L_Search%></button>
		 </td>

	</tr>
	</table>
	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="searchBg">
    <tr height="35">
	<!--
	<td width=24%>
    Barcode:&nbsp;
    <input id="barcode" type="text" size=12>
	</td>
	-->
	<br/>
	<td width=13%>
	<A href="addstoragekit.jsp?srcmenu=tdmenubaritem6&selectedTab=3&mode=N" onClick="return f_check_perm(<%=pageRight%>,'N') ;"><%=LC.L_Add_NewKit%><%--Add New Kit*****--%></A>
	</td>

	<td width=13%>
	<A href="#" onClick ="return selectStorages(document.storage,'D',<%=pageRight%>, 'Kit')" ><%=LC.L_Delete%><%--DELETE*****--%></A>
	</td>


	</table>

	</DIV>
	<DIV class="tabFormBotN tabFormBotN_MI_2" id="div3">




	<%

	   if(rowsReturned > 0) {
	%>



	<table width="100%" cellspacing="0" cellpadding="0" border="0">

	<TR> 
	     <th width=10% onClick="setOrder(document.storage,'LOWER(STORAGE_ID)')" align =center><%=LC.L_Id_Upper%><%--ID*****--%> &loz;</th>
	     <th width=14% onClick="setOrder(document.storage,'LOWER(STORAGE_NAME)')" align =center><%=LC.L_Name%><%--Name*****--%>  &loz;</th>
	     <th width=14% onClick="setOrder(document.storage,'LOWER(kit_category)')"align =center><%=LC.L_Category%><%--Category*****--%>  &loz;</th>
 	     <!-- removed status column<th width=12% onClick="setOrder(document.storage,'LOWER(STORAGE_STATUS)')" salign =center >Status  &loz;</th>-->
	     <th width=10%  align =center ><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="All" value="" onClick="checkAll(document.storage)"></th>
	</TR>


<%
	   String pkStorage;

	   String strId ="";
	   String strName = "";
	   String studynum = ""; //Amar
	   String strType ="";
	   String strCO_ORDINATE_X ="";
	   String strCO_ORDINATE_Y ="";
	   String strCapacityNumber = "";
	   String strCapacityUnits = " ";
	   String strStatus = "";
	   String rNum = "";

	   String strTypeDesc ="";
	   //String strStatusDesc = "";
	   String template ="";
	   CodeDao cdDesc = new CodeDao();
	   for(int i = 1 ; i <=rowsReturned ; i++) {

	    pkStorage = br.getBValues(i,"PK_STORAGE");
		template = br.getBValues(i,"STORAGE_ISTEMPLATE");

	    strId     = br.getBValues(i,"STORAGE_ID");
	    if(strId.length() > 35)
	      strId = strId.substring(0,35) + "...";

		studynum = br.getBValues(i,"studynum");
	    studynum=(studynum==null)?"":studynum;

		strTypeDesc   = br.getBValues(i,"kit_category");


		strCO_ORDINATE_X = br.getBValues(i,"STORAGE_COORDINATE_X");
	    strCO_ORDINATE_Y = br.getBValues(i,"STORAGE_COORDINATE_Y");


		//strStatusDesc = br.getBValues(i,"STORAGE_STATUS");


	      strName   = br.getBValues(i,"STORAGE_NAME");
	      if(strName.length() > 35)
	      strName = strName.substring(0,35) + "...";
	      strCapacityNumber = br.getBValues(i,"STORAGE_CAP_NUMBER");

		  strCapacityUnits  = br.getBValues(i,"CAPACITY_UNITS");


		 String strCapacityNumberAndUnits = "";


	    if(strCapacityUnits == null || strCapacityNumber == null)
	      strCapacityNumberAndUnits = "";

	      else
	      strCapacityNumberAndUnits = strCapacityNumber+" "+strCapacityUnits ;



		//strlocation =  br.getBValues(i,"parent_storage");

		/*if (StringUtil.isEmpty(strlocation))
		{
			strlocation= "";
		}*/

		rNum = br.getBValues(i,"RNUM");


	    if ((i%2)!=0) {
	    %>

	      <tr class="browserEvenRow">

	   <%

	   }
	   else {

	   %>

	      <tr class="browserOddRow">

	 <%
	    }
	 %>
	    <td width =10%><A href="editstoragekit.jsp?srcmenu=tdmenubaritem6&selectedTab=3&pkstoragekit=<%=pkStorage%>&mode=M"><%=strId%></A></td>
	     <td width =14%><%=strName%> </td>
	     <td width =14%><%=strTypeDesc%></td>

	   <!--removed status coluumn  <td width =12%><%//=strStatusDesc%></td> -->
	     <!-- <td width =10% align="center"><A href="#"> <img src="./images/printer.gif" border="0" align="center"/></A></td> -->
	     <td width =10% align="center"><input type="checkbox" name="Del" value="<%=pkStorage%>"></td>


	 <%
	  }
	%>
	</table>

	<P class="defComments_txt"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

	        <%
		if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
		%>
		<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>



		 <%
		if(storageNme.equals("All") || storageNme == null || storageNme.equals("")) {
		%>
		<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=storageNme%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>

		</P>

	<table class="tableDefault" align=left  >
			<tr valign="top"><td>

		<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} %>	</td></tr></table>

		<table class="tableDefault" align=center height="50px">
			<tr valign="top">

	      <%
	       if (curPage==1) startPage=1;

	        for (int count = 1; count <= showPages;count++) {
	        cntr = (startPage - 1) + count;
	        if ((count == 1) && (hasPrevious)) {

	       %>

	  	<td> <A href="storagekitbrowser.jsp?selectedTab=<%=selectedTab%>&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&specimenType=<%=specimenTyp%>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<%
	  	}
		%>
		<td>
		<%

		 if (curPage  == cntr)
		 {
	        %>
			<FONT class = "pageNumber"><%= cntr %></Font>
	       <%
	       }
	      else
	        {
	       %>
		<A href="storagekitbrowser.jsp?selectedTab=<%=selectedTab%>&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&specimenType=<%=specimenTyp%>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&orderBy=<%=orderBy%>&page=<%=cntr%>&orderType=<%=orderType%>"><%=cntr%></A>
	       <%
	    	}
		 %>
		</td>
		<%
		  }

		if (hasMore)
		{

	     %>
	     <td colspan = 3 align = center>
	  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="storagekitbrowser.jsp?selectedTab=<%=selectedTab%>&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&specimenType=<%=specimenTyp%>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&orderBy=<%=orderBy%>&page=<%=cntr+1%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A></td>
		<%
	  	}
		%>
	   </tr>
	  </table>
	 <%
	   }
	   else {
	     %>
	      <P class="defComments_txt"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

	        <%
		if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
		%>
		<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>



		 <%
		if(storageNme.equals("All") || storageNme == null || storageNme.equals("")) {
		%>
		<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=storageNme%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		</P>
	 <% if(pagenum != null) { // When the page opens the first time, do no search and don't display the following msg %>
		<P class="defComments_txt" align="center"><b><%=MC.M_NoRec_MdfCrit%><%--No matching records found. Please modify your criteria and try again.*****--%></b></P>
     <% } %>
	       <!-- <table>
	<tr>
	<td width=30%>
	</td>
	<td width=20%>
	</td>
	<td >
	<td width=10%>
	<A href="storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=N" >Add new</A>
	</td>
	<td width=11%>
	<A href="#" onClick="openwindow(document.storage);" >Add Multiple</A>
	</td>
	</tr>
	</table>-->
	       <%
	  }
	%>


	</DIV>
	</form>
<%
 } //pageright
 else
 {
 	%>
 	<DIV class="BrowserBotN" id="div3" style="overflow:hidden">
  		<jsp:include page="accessdenied.jsp" flush="true"/>
  	</DIV>
  <%
 }%>
<%}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>

<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>


</DIV>
</body>

</html>

