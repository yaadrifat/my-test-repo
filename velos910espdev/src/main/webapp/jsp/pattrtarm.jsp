<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Pats_TreatmentArm%><%--<%=LC.Pat_Patient%>'s Treatment Arm*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>



<SCRIPT Language="javascript">
 window.name="pattrtarm";

 function funcChange(formobj)
 {


 formobj.seltxid.value=formobj.stdtxtarm.value;
 formobj.stdTXId.value = formobj.stdtxtarm.value;
 formobj.patProtId.value=formobj.patProtId.value;
 formobj.patTXArmId.value=formobj.patTXArmId.value;
 formobj.studyId.value=formobj.studyId.value;

 formobj.action="pattrtarm.jsp" ;
	formobj.submit();
 }

 function  validate(formobj)
 {

 	if (   !(validate_col ('Treatment', formobj.stdtxtarm)  )  )  return false

	 if (!(validate_date(formobj.startDate))) return false
	 if (!(validate_date(formobj.endDate))) return false

	  if (formobj.startDate.value != null && formobj.startDate.value != '' && formobj.endDate.value != null && formobj.endDate.value != '')
	 {
			 if (CompareDates(formobj.startDate.value,formobj.endDate.value,'>'))
		 {
		  	alert ("<%=MC.M_EndDtNot_LtStDt%>");/*alert ("End Date should not be less than Start Date");*****/
			return false;
		 }
	 }

 	var mVal = document.getElementById("mode").value;
 	if (mVal){
 	 	var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "M"){
			if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
		}
 	}

     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false

	if(isNaN(formobj.eSign.value) == true)
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again.");*****/
		formobj.eSign.focus();
		 return false;
   	}

  }



</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patTXArmJB" scope="request"  class="com.velos.eres.web.patTXArm.PatTXArmJB"/>
<jsp:useBean id="stdTXArmJB" scope="request"  class="com.velos.eres.web.studyTXArm.StudyTXArmJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page language = "java" import = "com.velos.eres.web.study.StudyJB"%>

<body>
<DIV class="popDefault" id="div1">
 <%

	HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%

	int pageRight = 4;
	if (pageRight >= 4)
	{

		String mode=request.getParameter("mode");
		String patientCode = request.getParameter("patientCode");
		patientCode = StringUtil.decodeString(patientCode);
		String studyNumber = request.getParameter("studyNumber");
		if(mode == null)
		  mode = "";
		studyNumber = StringUtil.decodeString(studyNumber);

		 String seltxid = request.getParameter("seltxid");
		 if(seltxid==null)
		 seltxid="";


		String patProtId= request.getParameter("patProtId");
		String stdTXId = request.getParameter("stdTXId");
		String patTXId = "";
		String drugInfo="";
		String startDate="";
		String endDate="";
		String notes ="";

		if(stdTXId==null)
		stdTXId ="";
		if(mode.equals("N") && !stdTXId.equals(""))
		{
		  stdTXArmJB.setStudyTXArmId(EJBUtil.stringToNum(stdTXId));
		  stdTXArmJB.getStudyTXArmDetails();
		  drugInfo = stdTXArmJB.getTXDrugInfo();

		}



		String accountId=(String)tSession.getAttribute("accountId");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		String studyId=request.getParameter("studyId");

		StudyJB studyB = new StudyJB();
		studyB.setId(StringUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

		if (mode.equals("M"))
		{
				patTXId = request.getParameter("patTXArmId");
				patTXArmJB.setPatTXArmId(EJBUtil.stringToNum(patTXId));
				patTXArmJB.getPatTXArmDetails();
				stdTXId = patTXArmJB.getStudyTXArmId();
				drugInfo = patTXArmJB.getTXDrugInfo();

				startDate = patTXArmJB.getStartDate();
				endDate = patTXArmJB.getEndDate();
				notes = patTXArmJB.getNotes();

		}

		if(drugInfo == null ) drugInfo ="";
		if(notes == null) notes = "";
		StringBuffer dtxarms = new StringBuffer();
		int tx = EJBUtil.stringToNum(stdTXId);
		StudyTXArmDao tDao = new StudyTXArmDao();
	 tDao = stdTXArmJB.getStudyTrtmtArms(EJBUtil.stringToNum(studyId));

	  ArrayList txIds = tDao.getStudyTXArmIds();
	  ArrayList txNames = tDao.getTXNames();
  int txId =0;
  int len = txIds.size();
   dtxarms.append("<SELECT NAME=stdtxtarm ");
   //KM-Modified
   if(mode.equals("N"))
    dtxarms.append(" onChange='funcChange(document.pattrtarm)'") ;
	dtxarms.append(">");
   dtxarms.append("<OPTION value =''> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;
	for (int counter = 0; counter <= len-1 ; counter++) {
		txId =  ((Integer)txIds.get(counter)).intValue();
		if (txId==tx) {
			dtxarms.append("<OPTION value = "+ txId +" selected>" + txNames.get(counter)+"</OPTION>");
		} else {
			dtxarms.append("<OPTION value = "+ txId +">" + txNames.get(counter)+"</OPTION>");
		}
	}

%>

    <Form name="pattrtarm" id="pattrtarmfrm" method="post" action="pattrtarmsubmit.jsp" onsubmit="if (validate(document.pattrtarm)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 <input type=hidden name="seltxid" >
 <table width="80%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadingsFrm"> <%=LC.L_Pats_TreatmentArm%><%--<%=LC.Pat_Patient%>'s Treatment Arm*****--%> </P> <br></td>
	  </tr>

	 </table>
 <table width="400" cellspacing="0" cellpadding="0">
	<tr>
	  <td><P> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>: &nbsp;&nbsp;<%=patientCode%> &nbsp;&nbsp;&nbsp; <%=LC.L_Study_Number %><%-- <%=LC.Std_Study%> Number*****--%>: &nbsp;&nbsp;<%=studyNumber%></P>

     </td>

    </tr>

    </table>
 <P class = "defComments"> <%=MC.M_Etr_TreatArmDet%><%--Please enter Treatment Arm details*****--%> </P>


	<Input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>" >
	<Input type="hidden" id="mode" name="mode" value=<%=mode%> >
	<Input type="hidden" name="studyId" value=<%=studyId%> >
	<Input type="hidden" name="patProtId" value=<%=patProtId%> >
	<Input type="hidden" name="stdTXArmId" value=<%=stdTXId%> >
	<Input type="hidden" name="patTXArmId" value=<%=patTXId%> >
	<Input type="hidden" name="stdTXId" value=<%=stdTXId%> >
	<Input type="hidden" name="patientCode" value='<%=patientCode%>' ><!-- Mukul:Bug No #3999: Patient code was truncated after space -->
		<Input type="hidden" name="studyNumber" value='<%=studyNumber%>' >





	<table width="80%" cellspacing="0" cellpadding="0" border="0"  >
		<tr>
      <td><%=LC.L_Treatment%><%--Treatment*****--%> <FONT class="Mandatory">* </FONT></td>
       <td><%=dtxarms%></td>
	  </tr>

	  <tr>
      <td ><%=LC.L_Drug_Info%><%--Drug information*****--%></td>
        <td><input type="text" name="drugInfo" value="<%=drugInfo%>" maxlength=250></td>
	  </tr>
	 <%-- INF-20084 Datepicker-- AGodara --%>
	  <tr>
	  	<td><%=LC.L_Start_Date%><%--Start Date*****--%></td>
	  	<td><input type="text" name="startDate" class="datefield" size = 15 MAXLENGTH = 50 value="<%=startDate%>" ></td>
	  </tr>
	  <tr>
	  	<td><%=LC.L_End_Date%><%--End Date*****--%></td>
	  	<td><input type="text" name="endDate" class="datefield" size = 15 MAXLENGTH = 50	value="<%=endDate%>" ></td>
	  </tr>
	  
	  <tr>
      <td><%=LC.L_Notes%><%--Notes*****--%></td>
	  <td><textarea name="notes" maxlength=4000><%=notes%></textarea></td>
	  </tr>
	<% if ("M".equals(mode)){%>
	<br>
	<tr>
      <td><%=LC.L_ReasonForChangeFDA%>
      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
      </td>
	  <td><textarea id="remarks" name="remarks" rows="3" cols="30" MAXLENGTH="4000"></textarea></td>
  	</tr>
	<%} %>
	</table>	  
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="pattrtarmfrm"/>
		<jsp:param name="showDiscard" value="N"/>
  </jsp:include>

  </Form>

<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel">
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>
















