<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<link href="styles/login.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<form name="loginerror" method="post" action="ereslogin.jsp" onSubmit="window.self.close();">
<br><br>
<table width="800" height="200"><tr><td>
<p class = "sectionHeadings">
<%=MC.M_SorryNotLogged_SomeReason %><%-- We are Sorry, you can not be logged on due to any of the following reasons*****--%>
</p>

<br>

<p class = "defComments"> 

<%=MC.M_InvldusrAcc_CheckAdmin %><%--1.  You have provided invalid user name or password.
<br> 2.  Your account is not yet activated.
<br> 3. Your account is expired.Please check with your Account Administrator.*****--%>
</p>

<br><%--Modified By Tarun Kumar : Bug#10301 --%>
<button type="submit" style="text-align: center;" ><font color="white"><%=LC.L_Try_Again%></font></button>

</td></tr></table>
<!--<A href="#" onClick="window.self.close();"><img src="../images/jpg/tryagain.jpg" border=0></A>-->
</form>
