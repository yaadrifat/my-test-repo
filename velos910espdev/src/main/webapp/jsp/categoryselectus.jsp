<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Category%><%--Category*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.category
//start
		 
	mycount=0;

	 for(cnt = 0; cnt<5;cnt++){ 
		if((formobj.categoryName[cnt].value=="")){
		mycount++;
		}}

		if(mycount==5){
		alert("<%=MC.M_AlteastOne_CatName%>");/*alert("Please Enter alteast one Category Name");*****/  
		formobj.categoryName[0].focus();
		return false;
		} 

	for(cnt = 0; cnt<5;cnt++){ 
		if((formobj.categoryName[cnt].value=="")&&(formobj.desc[cnt].value!="")){
		alert("<%=MC.M_Etr_CatVal%>");/*alert("Please Enter the Category Value");*****/  
		formobj.categoryName[cnt].focus();
		return false;
		}
	 }

	
	for(count=0;count<5;count++)
	{
		for(count1=count+1;count1<5;count1++){

		if((formobj.categoryName[count].value==formobj.categoryName[count1].value)&&(formobj.categoryName[count].value!="")){
			alert("<%=MC.M_CatNames_CntSimilar%>");/*alert("Two Category names cannot be similar");*****/
			formobj.categoryName[count1].focus();
			return false;
			}
		}

	}	

		 
		 
//end
     	 
     if (!(validate_col('e-Signature',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}

   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>


<% String src;

src= request.getParameter("srcmenu");

%>




<body>
<DIV  id="div1"> 

  <%

	String categoryId  ="";
	String categoryName ="";

	String desc ="";
    for (Enumeration e = request.getParameterNames() ; e.hasMoreElements() ;) {
         System.out.println(e.nextElement());
     }

	String mode="";
	String catMode="";
     String catId=request.getParameter("catId");
     String catName=request.getParameter("catName");
	String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String cost = request.getParameter("cost");
	String calStatus=request.getParameter("calStatus");

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
		String uName = (String) tSession.getValue("userName");

		mode = request.getParameter("mode");
		catMode = request.getParameter("catMode");

		int pageRight = 7;

	//	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
//        	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));

		if (catMode.equals("M")) {
			categoryId = request.getParameter("categoryId");
			eventdefB.setEvent_id(EJBUtil.stringToNum(categoryId));
		
			eventdefB.getEventdefDetails();
			categoryName = eventdefB.getName(); 

			desc = eventdefB.getDescription();

   		}
%>


  <%

	if ((catMode.equals("M") && pageRight >=6) || (catMode.equals("N") && (pageRight == 5 || pageRight == 7 )) )


	{

%>
  <br>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=LC.L_PcolCal_Cat%><%--Protocol Calendar >> Category*****--%> </P>
  <br>
  <br>
  <P class="defComments"> <%=MC.M_Etr_CatDets%><%--Please enter Category details*****--%></P>

  <Form name="category" method="post" action="updatecategoryselectus.jsp?catId=<%=catId%>&catName=<%=catName%>&mode=<%=mode%>&cost=<%=cost%>&calStatus=<%=calStatus%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>" onsubmit="return validate()">
    <input type="hidden" name="srcmenu" value="<%=src%>">
    <input type="hidden" name="mode" size = 20  value ="<%=mode%>" >
    <input type="hidden" name="catMode" size = 20  value ="<%=catMode%>" >
    <input type="hidden" name="categoryId" size = 20  value ="<%=categoryId%>" >

    <table width="100%" >
    <tr class = "popupHeader"> 
         <th width="40%"><%=LC.L_Cat_Name%><%--Category Name*****--%></th>
         <th width="60%"><%=LC.L_Cat_Desc%><%--Category Description*****--%></th>
    </tr> 	
  <%    for (int i=0;i<5;i++){   
    if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  <%
		}
	else {
  %>
      <tr class="browserOddRow"> 
   <%
		}
	%> 
    
         <td width="40%"> 
          <input type="text" name="categoryName" size = 20 MAXLENGTH = 50 	value="<%=categoryName%>">
       </td>
       <td width="60%"> 
	   <input type="text" name="desc" size = 40 MAXLENGTH = 50 value="<%=desc%>">
      </td>
      </tr>
      <% }%>
      	  
        
  </table>
  		  

<table width="100%">
<tr>
	   <td>
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8">
		</td><td width="38%">
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >
        
	     </td>
</tr>
</table>

    <br>

  <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 
	<!--	<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >-->
      </td> 
      </tr>
  </table>
   
  </Form>
 
<table><tr><td>
	<!--<A href="#" onclick="window.history.back()" type="submit"><%=LC.L_Back%></A>-->
	<button onclick="window.history.back()"><%=LC.L_Back%></button>
	</td></tr></table> 	  
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  
</body>

</html>


