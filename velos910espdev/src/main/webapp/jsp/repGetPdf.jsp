<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<body>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.lowagie.text.Document, com.lowagie.text.pdf.PdfWriter,com.lowagie.text.pdf.codec.Base64,com.lowagie.text.html.simpleparser.StyleSheet,com.lowagie.text.html.simpleparser.HTMLWorker,com.lowagie.text.*,com.lowagie.*,java.io.*,java.util.ArrayList,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
	
<%
	HttpSession tSession = request.getSession(true);
 	if (sessionmaint.isValidSession(tSession))
	{
		String repDate="";
		String uName =(String) tSession.getValue("userName");	 
		Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
		com.aithent.file.uploadDownload.Configuration.readSettings("eres");
		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");	
		String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
		Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
		String filePath = Configuration.REPDWNLDPATH;
		Calendar now = Calendar.getInstance();
		repDate = DateUtil.getCurrentDate() ;
		String fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
		String htmlFile = filePath + "/"+fileName;

		
		int repId = StringUtil.stringToNum(request.getParameter("repId"));
		String repName = request.getParameter("repName");
		ReportDaoNew rD =new ReportDaoNew();
		rD=repB.getRepXml(repId,0,"");

		ArrayList repXml = rD.getRepXml();

		rD=repB.getRepXsl(repId);
		ArrayList repXsl = rD.getXsls();

		Object temp;
		String xml=null;
		String xsl=null;

		temp=repXml.get(0);				

		if (!(temp == null)) 
		{
			xml=temp.toString();
			//replace the encoded characters
			xml = StringUtil.replace(xml,"&amp;#","&#");
			
		}
		else
		{
			out.println(MC.M_ErrIn_GetXml);/*out.println("Error in getting XML");*****/
			Rlog.fatal("Report","ERROR in getting XML");			
			return;
		}

		try{	
			temp=repXsl.get(0);
		} catch (Exception e) {
			out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
			Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
			return;
		}	

		if (!(temp == null))	{
			xsl=temp.toString();
		}
		else
		{
			out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
			Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
			return;
		}

		if ((xsl.length())==0) {
			out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
			Rlog.fatal("Report","ERROR in getting XSL. Empty XSL found in database for report " +repId);
			return;
		} 

		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml); 
		Reader sR1=new StringReader(xsl); 
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);



		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		transformer1.setParameter("repName",repName);
		transformer1.setOutputProperty("encoding", "UTF-8");
		transformer1.setParameter("repBy",uName);
		transformer1.setParameter("repDate",repDate);

		FileOutputStream fhtml=new FileOutputStream(htmlFile);
		transformer1.transform(xmlSource1, new StreamResult(fhtml));
		fhtml.close();

	Calendar now1 = Calendar.getInstance();
	String pdfFileName="reportpdf"+"["+ System.currentTimeMillis() + "].pdf" ;
	String pdfFile=filePath + "/" + pdfFileName ;


	try
	{	
		File fin=new File(htmlFile);
		Document pdfDocument = new Document(PageSize.A3, 25, 25, 50, 50);
        Reader htmlreader = new BufferedReader(new InputStreamReader(
                                 new FileInputStream(fin)
                                ));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter.getInstance(pdfDocument, baos);
        pdfDocument.open();
        StyleSheet styles = new StyleSheet();
        //styles.loadTagStyle("body", "font", "Bitstream Vera Sans");
        ArrayList arrayElementList = HTMLWorker.parseToList(htmlreader, styles);
        for (int i1 = 0; i1 < arrayElementList.size(); ++i1) {
            Element e = (Element) arrayElementList.get(i1);
            pdfDocument.add(e);
        }

 
        pdfDocument.close();
        byte[] bs = baos.toByteArray();
        String pdfBase64 = Base64.encodeBytes(bs); //output
        FileOutputStream outPdf = new FileOutputStream(pdfFile);
        outPdf.write(bs);
        outPdf.close();
		}
		catch (Exception e)
		{
			System.out.println("Error in Creation of PDF file" + e + e.getMessage());
		}
	
		String path=fileDnPath + "?file=" + pdfFileName +"&mod=R";
		
%>

	<form name="dummy" method="post" action="<%=path%>" target="_self">
		<input type ="hidden" name="d" />
	</form>

<script>
	document.dummy.submit();
</script>
	
	<% } //session
		%> 
	
</body>
</html>